/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import fa.gs.utils.misc.errors.Errors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "TipoEmailBaseGenerator", mappedName = "TipoEmailBaseGenerator")
@LocalBean
public class TipoEmailBaseGenerator extends LogicBean {

    @EJB
    private JpaUtils jpaUtils;

    @PostConstruct
    public void init() {
        try {
            if (!existe()) {
                crear();
            }
        } catch (Throwable thr) {
            log.warning(thr, "Ocurrió un error generando tipo de email base.");
            throw Errors.illegalState(thr, "Ocurrió un error generando tipo de email base.");
        }
    }

    private boolean existe() throws Throwable {
        String sql = " select count(*) as \"total\" from info.tipo_email where id = 0 ";
        Integer total = jpaUtils.getTotal(sql);
        return total > 0;
    }

    private void crear() throws Throwable {
        String sql = " insert into info.tipo_email(id, descripcion) values(0, '') ";
        jpaUtils.executeUpdate(sql);
    }

}
