/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.text.Strings;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.enums.TipoTelefonoEnum;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "TipoTelefonosGenerator", mappedName = "TipoTelefonosGenerator")
@LocalBean
public class TipoTelefonosGenerator extends LogicBean {

    @EJB
    private JpaUtils jpaUtils;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        try {
            for (TipoTelefonoEnum tipo : TipoTelefonoEnum.values()) {
                if (!existe(tipo)) {
                    crear(tipo);
                }
            }
        } catch (Throwable thr) {
            log.warning(thr, "Ocurrió un error generando tipos de teléfono.");
            throw Errors.illegalState(thr, "Ocurrió un error generando tipos de teléfono.");
        }
    }

    private boolean existe(TipoTelefonoEnum tipo) throws Throwable {
        String sql = Strings.format(" select count(*) as \"total\" from info.tipo_telefono where tipo = '%s' ", tipo.descripcion());
        Integer total = jpaUtils.getTotal(sql);
        return total > 0;
    }

    private void crear(TipoTelefonoEnum tipo) throws Throwable {
        String sql = Strings.format(" insert into info.tipo_telefono(tipo) values('%s') ", tipo.descripcion());
        jpaUtils.executeUpdate(sql);
    }

}
