/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import fa.gs.utils.misc.Assertions;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.entities.info.ContactoEmail;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Emails;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "SepsaInfoGenerator", mappedName = "SepsaInfoGenerator")
@LocalBean
public class SepsaInfoGenerator implements Serializable {

    @EJB
    private Personas personas;

    @EJB
    private Emails emails;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        Persona sepsa = obtenerOCrearSepsa();
        obtenerOCrearEmail(sepsa);
    }

    private Persona obtenerOCrearSepsa() {
        Persona sepsa = personas.obtenerPorRuc("80049048-7").value(null);
        if (sepsa == null) {
            sepsa = personas.registrarPersonaJuridica("SISTEMAS ELECTRÓNICOS DEL PARAGUAY S.A.", "SEPSA", "80049048-7").value(null);
        }
        return sepsa;
    }

    private void obtenerOCrearEmail(Persona sepsa) {
        if (sepsa == null) {
            return;
        }

        List<ContactoEmail> elementos = emails.obtenerContactosEmailDisponibles(sepsa.getId());
        if (Assertions.isNullOrEmpty(elementos)) {
            emails.registrarEmail(sepsa.getId(), "ecobro@sepsa.com.py");
        }
    }

}
