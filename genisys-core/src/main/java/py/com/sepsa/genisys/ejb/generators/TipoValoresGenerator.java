/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoValor;
import py.com.sepsa.genisys.ejb.enums.TipoValorEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.TipoValorFacade;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.DbFiller;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "TipoValoresGenerator", mappedName = "TipoValoresGenerator")
@LocalBean
public class TipoValoresGenerator implements Serializable {

    @EJB
    private TipoValorFacade facade;

    /**
     * Valores fijos para base de datos.
     */
    private ValoresFijos valores;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        this.valores = new ValoresFijos(facade);
        this.valores.fill();
    }

    private static class ValoresFijos extends DbFiller.EntityFacadeBacked<TipoValor, TipoValorEnum> {

        private final TipoValorFacade facade;

        private ValoresFijos(TipoValorFacade facade) {
            this.facade = facade;
        }

        @Override
        public Collection<TipoValorEnum> obtenerValoresFijosDisponibles() {
            return Arrays.asList(TipoValorEnum.values());
        }

        @Override
        public boolean equals(TipoValor valor, TipoValorEnum valorFijo) {
            return Objects.equals(valorFijo.descripcion(), valor.getDescripcion());
        }

        @Override
        public AbstractFacade<TipoValor> getFacade() {
            return facade;
        }

        @Override
        public TipoValor obtenerValor(AbstractFacade<TipoValor> facade, TipoValorEnum valorFijo) {
            return ((TipoValorFacade) facade).findByDescripcion(valorFijo.descripcion());
        }

        @Override
        public TipoValor crearValor(AbstractFacade<TipoValor> facade, TipoValorEnum valorFijo) throws Throwable {
            TipoValor tipoValor = new TipoValor();
            tipoValor.setDescripcion(valorFijo.descripcion());
            facade.create(tipoValor);
            return tipoValor;
        }

    }

}
