/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.PerfilFacade;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.DbFiller;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "PerfilesGenerator", mappedName = "PerfilesGenerator")
@LocalBean
public class PerfilesGenerator extends LogicBean {

    @EJB
    private PerfilFacade facade;

    /**
     * Valores fijos para base de datos.
     */
    private ValoresFijos valores;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        this.valores = new ValoresFijos(facade);
        this.valores.fill();
    }

    private static class ValoresFijos extends DbFiller.EntityFacadeBacked<Perfil, PerfilEnum> {

        private final PerfilFacade facade;

        private ValoresFijos(PerfilFacade facade) {
            this.facade = facade;
        }

        @Override
        public Collection<PerfilEnum> obtenerValoresFijosDisponibles() {
            return Arrays.asList(PerfilEnum.values());
        }

        @Override
        public boolean equals(Perfil valor, PerfilEnum valorFijo) {
            return Objects.equals(valorFijo.descripcion(), valor.getDescripcion());
        }

        @Override
        public AbstractFacade<Perfil> getFacade() {
            return facade;
        }

        @Override
        public Perfil obtenerValor(AbstractFacade<Perfil> facade, PerfilEnum valorFijo) {
            return ((PerfilFacade) facade).findByDescripcion(valorFijo.descripcion());
        }

        @Override
        public Perfil crearValor(AbstractFacade<Perfil> facade, PerfilEnum valorFijo) throws Throwable {
            Perfil perfil = new Perfil();
            perfil.setDescripcion(valorFijo.descripcion());
            perfil.setEstado('A');
            facade.create(perfil);
            return perfil;
        }

    }

}
