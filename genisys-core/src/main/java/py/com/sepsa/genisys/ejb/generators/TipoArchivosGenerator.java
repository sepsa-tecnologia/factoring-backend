/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoArchivo;
import py.com.sepsa.genisys.ejb.enums.TipoArchivoEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.TipoArchivosFacade;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.DbFiller;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "TipoArchivosGenerator", mappedName = "TipoArchivosGenerator")
@LocalBean
public class TipoArchivosGenerator implements Serializable {

    @EJB
    private TipoArchivosFacade facade;

    /**
     * Valores fijos para base de datos.
     */
    private ValoresFijos valores;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        this.valores = new ValoresFijos(facade);
        this.valores.fill();
    }

    private static class ValoresFijos extends DbFiller.EntityFacadeBacked<TipoArchivo, TipoArchivoEnum> {

        private final TipoArchivosFacade facade;

        private ValoresFijos(TipoArchivosFacade facade) {
            this.facade = facade;
        }

        @Override
        public Collection<TipoArchivoEnum> obtenerValoresFijosDisponibles() {
            return Arrays.asList(TipoArchivoEnum.values());
        }

        @Override
        public boolean equals(TipoArchivo valor, TipoArchivoEnum valorFijo) {
            return Objects.equals(valorFijo.descripcion(), valor.getDescripcion());
        }

        @Override
        public AbstractFacade<TipoArchivo> getFacade() {
            return facade;
        }

        @Override
        public TipoArchivo obtenerValor(AbstractFacade<TipoArchivo> facade, TipoArchivoEnum valorFijo) {
            return ((TipoArchivosFacade) facade).findByDescripcion(valorFijo.descripcion());
        }

        @Override
        public TipoArchivo crearValor(AbstractFacade<TipoArchivo> facade, TipoArchivoEnum valorFijo) throws Throwable {
            TipoArchivo tipoValor = new TipoArchivo();
            tipoValor.setDescripcion(valorFijo.descripcion());
            facade.create(tipoValor);
            return tipoValor;
        }

    }

}
