/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import fa.gs.utils.collections.Lists;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.entities.info.TipoContacto;
import py.com.sepsa.genisys.ejb.facades.info.TipoContactoFacade;
import py.com.sepsa.genisys.ejb.utils.Constantes;
import py.com.sepsa.genisys.ejb.utils.DbFiller;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "TipoContactoGenerator", mappedName = "TipoContactoGenerator")
@LocalBean
public class TipoContactoGenerator implements Serializable {

    @EJB
    private TipoContactoFacade facade;

    /**
     * Valores fijos para base de datos.
     */
    private ValoresFijos valores;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        this.valores = new ValoresFijos(facade);
        this.valores.fill();
    }

    private static class ValoresFijos extends DbFiller<TipoContacto, Integer> {

        private final TipoContactoFacade facade;

        private ValoresFijos(TipoContactoFacade facade) {
            this.facade = facade;
        }

        @Override
        public TipoContacto obtenerValor(Integer valorFijo) {
            return facade.findById(valorFijo);
        }

        @Override
        public TipoContacto crearValor(Integer valorFijo) throws Throwable {
            TipoContacto instance = new TipoContacto();
            instance.setId(valorFijo);
            instance.setDescripcion("Genérico");
            facade.create(instance);
            return instance;
        }

        @Override
        public Collection<Integer> obtenerValoresFijosDisponibles() {
            List<Integer> ids = Lists.empty();
            ids.add(Constantes.ID_TIPO_CONTACTO_GENERICO);
            return ids;
        }

        @Override
        public boolean equals(TipoContacto valor, Integer valorFijo) {
            return Objects.equals(valor.getId(), valorFijo);
        }

    }

}
