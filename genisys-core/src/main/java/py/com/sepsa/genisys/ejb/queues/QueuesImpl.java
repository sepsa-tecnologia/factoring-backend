/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.queues;

import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.errors.Errors;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoring;
import py.com.sepsa.genisys.ejb.logic.impl.Queues;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.TxWrapperCore;
import py.com.sepsa.genisys.integraciones.banco_rio.BancoRioTipoPeticionEnum;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.WsTrackInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "Queues", mappedName = "Queues")
@Remote(Queues.class)
public class QueuesImpl implements Queues {

    private final String QUEUE_NOTIFICACIONES = "queue.notificaciones";

    private final String QUEUE_BANCO_RIO_TRACKING_PETICIONES = "queue.banco_rio.tracking_peticiones";

    private AppLogger log;

    @Resource
    private ManagedScheduledExecutorService executor;

    private ScheduledFuture<?> dispatcherHandler;

    private Map<String, Queue> queues;

    private Map<String, QueueConsumer> consumers;

    @PostConstruct
    public void init() {
        this.log = AppLoggerFactory.core();
        this.queues = Maps.empty();
        this.consumers = Maps.empty();
        this.dispatcherHandler = executor.schedule(this::dispatch, 30, TimeUnit.SECONDS);
        initConsumerNotificaciones();
        initConsumerBancoRio1();
    }

    @PreDestroy
    public void finish() {
        try {
            dispatcherHandler.cancel(true);
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrio un error deteniendo trabajador en segundo plano.");
        }
    }

    private void initConsumerNotificaciones() {
        queues.put(QUEUE_NOTIFICACIONES, new ConcurrentLinkedDeque<>());
        QueueConsumer consumer = new QueueConsumer_Notificaciones();
        consumers.put(QUEUE_NOTIFICACIONES, consumer);
    }

    private void initConsumerBancoRio1() {
        queues.put(QUEUE_BANCO_RIO_TRACKING_PETICIONES, new ConcurrentLinkedDeque<>());
        QueueConsumer consumer = new QueueConsumer_BancoRio1();
        consumers.put(QUEUE_BANCO_RIO_TRACKING_PETICIONES, consumer);
    }

    private void dispatch() {
        while (true) {
            // Control.
            boolean a = Thread.currentThread().isAlive() == false;
            boolean b = Thread.currentThread().isInterrupted() == true;
            if (a || b) {
                return;
            }

            // Procesar colas.
            int s = 0;
            do {
                for (Map.Entry<String, Queue> entry : queues.entrySet()) {
                    String queueName = entry.getKey();
                    Queue queue = entry.getValue();

                    Object data = queue.poll();
                    if (data != null) {
                        log.debug("cosumiendo cola '%s' ...", queueName);
                        TxWrapperCore tx = Injection.lookup(TxWrapperCore.class);
                        Result<?> result = tx.execute(() -> {
                            QueueConsumer consumer = consumers.get(queueName);
                            consumer.consume(data);
                            return null;
                        });
                        log.debug("cosumiendo cola '%s': %s", queueName, result.isFailure() ? "OK" : "KO");
                    }

                    s += queue.size();
                }
            } while (s != 0);

            // Dormir.
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public void post(NotificacionEmailFactoring cabecera, Integer[] idFacturas) {
        QueueConsumer_Notificaciones.Data data = new QueueConsumer_Notificaciones.Data();
        data.setCabecera(cabecera);
        data.setIdFacturas(idFacturas);
        queues.get(QUEUE_NOTIFICACIONES).add(data);
    }

    @Override
    public void post(BancoRioTipoPeticionEnum tipoPeticion, WsTrackInfo info) {
        QueueConsumer_BancoRio1.Data data = new QueueConsumer_BancoRio1.Data();
        data.setTipoPeticion(tipoPeticion);
        data.setInfo(info);
        queues.get(QUEUE_BANCO_RIO_TRACKING_PETICIONES).add(data);
    }

}
