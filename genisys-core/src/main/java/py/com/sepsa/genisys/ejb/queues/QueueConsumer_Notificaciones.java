/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.queues;

import fa.gs.utils.misc.Assertions;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoring;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoringFactura;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class QueueConsumer_Notificaciones implements QueueConsumer {

    @Override
    public void consume(Object data0) throws Throwable {
        if (data0 instanceof Data) {
            Data data = (Data) data0;

            // Inyectar beans.
            JpaUtils jpaUtils = Injection.lookup(JpaUtils.class);

            // Se genera la notificacion (inicialmente inactiva) para el contacto que habilita las notificaciones de factoring.
            data.cabecera.setEstado('I');
            data.cabecera.setLeido('n');
            NotificacionEmailFactoring n = jpaUtils.create(data.cabecera);

            // Se generan los detalles, si hubieren.
            if (!Assertions.isNullOrEmpty(data.idFacturas)) {
                for (Integer idFactura : data.idFacturas) {
                    NotificacionEmailFactoringFactura nf = new NotificacionEmailFactoringFactura(n.getId(), idFactura);
                    jpaUtils.create(nf);
                }
            }

            // Se edita el estado de la notificacion para que pueda ser procesado.
            n.setEstado('P');
            jpaUtils.edit(n);
        }
    }

    @lombok.Data
    public static class Data {

        private NotificacionEmailFactoring cabecera;
        private Integer[] idFacturas;
    }

}
