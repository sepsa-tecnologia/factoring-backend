/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoCargo;
import py.com.sepsa.genisys.ejb.enums.TipoCargoEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.TipoCargoFacade;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.DbFiller;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "TipoCargosGenerator", mappedName = "TipoCargosGenerator")
@LocalBean
public class TipoCargosGenerator extends LogicBean {

    @EJB
    private TipoCargoFacade facade;

    /**
     * Valores fijos para base de datos.
     */
    private ValoresFijos valores;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        this.valores = new ValoresFijos(facade);
        this.valores.fill();
    }

    private static class ValoresFijos extends DbFiller.EntityFacadeBacked<TipoCargo, TipoCargoEnum> {

        private TipoCargoFacade facade;

        private ValoresFijos(TipoCargoFacade facade) {
            this.facade = facade;
        }

        @Override
        public Collection<TipoCargoEnum> obtenerValoresFijosDisponibles() {
            return Arrays.asList(TipoCargoEnum.values());
        }

        @Override
        public boolean equals(TipoCargo valor, TipoCargoEnum valorFijo) {
            return Objects.equals(valorFijo.descripcion(), valor.getDescripcion());
        }

        @Override
        public AbstractFacade<TipoCargo> getFacade() {
            return facade;
        }

        @Override
        public TipoCargo obtenerValor(AbstractFacade<TipoCargo> facade, TipoCargoEnum valorFijo) {
            return ((TipoCargoFacade) facade).findByDescripcion(valorFijo.descripcion());
        }

        @Override
        public TipoCargo crearValor(AbstractFacade<TipoCargo> facade, TipoCargoEnum valorFijo) throws Throwable {
            TipoCargo tipoCargo = new TipoCargo();
            tipoCargo.setDescripcion(valorFijo.descripcion());
            facade.create(tipoCargo);
            return tipoCargo;
        }

    }
}
