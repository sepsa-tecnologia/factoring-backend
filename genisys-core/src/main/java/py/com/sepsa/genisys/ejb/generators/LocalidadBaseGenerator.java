/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import fa.gs.utils.misc.errors.Errors;
import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "LocalidadBaseGenerator", mappedName = "LocalidadBaseGenerator")
@LocalBean
@DependsOn({"DptoGeograficoBaseGenerator"})
public class LocalidadBaseGenerator extends LogicBean {

    @EJB
    private JpaUtils jpaUtils;

    @PostConstruct
    public void init() {
        try {
            if (!existe()) {
                crear();
            }
        } catch (Throwable thr) {
            log.warning(thr, "Ocurrió un error generando departamento geográfico base.");
            throw Errors.illegalState(thr, "Ocurrió un error generando departamengo geográfico base.");
        }
    }

    private boolean existe() throws Throwable {
        String sql = " select count(*) as \"total\" from info.localidad where id = 0 ";
        Integer total = jpaUtils.getTotal(sql);
        return total > 0;
    }

    private void crear() throws Throwable {
        String sql = " insert into info.localidad(id, id_dpto_geografico, descripcion) values(0, 0, 'BASE') ";
        jpaUtils.executeUpdate(sql);
    }

}
