/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.batchlets;

import fa.gs.utils.collections.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.logic.impl.Batchlets;
import py.com.sepsa.genisys.ejb.utils.JobStamp;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "Batchlets", mappedName = "Batchlets")
@Remote(Batchlets.class)
public class BatchletsImpl implements Batchlets {

    private Map<String, JobStamp> stamps;

    @PostConstruct
    public void init() {
        this.stamps = new ConcurrentHashMap<>();
    }

    @Override
    public void stamp(String name) {
        long now = System.currentTimeMillis();
        JobStamp stamp;
        if (!stamps.containsKey(name)) {
            stamp = new JobStamp();
            stamp.setName(name);
            stamp.setLastRun(now);
            stamp.setRuns(0L);
            stamp.setEllapsed(0L);
            stamps.put(name, stamp);
        } else {
            stamp = stamps.get(name);
            stamp.setLastRun(now);
        }
    }

    @Override
    public void ellapsed(String name, long millis) {
        JobStamp stamp = stamps.get(name);
        if (stamp != null) {
            stamp.setRuns(stamp.getRuns() + 1);
            stamp.setEllapsed(stamp.getEllapsed() + millis);
        }
    }

    @Override
    public void sleep(String name, long period) {
        JobStamp stamp = stamps.get(name);
        if (stamp != null) {
            stamp.setLastPeriod(period);
        }
    }

    @Override
    public JobStamp[] list() {
        Collection<JobStamp> values = this.stamps.values();
        return Arrays.unwrap(values, JobStamp.class);
    }

}
