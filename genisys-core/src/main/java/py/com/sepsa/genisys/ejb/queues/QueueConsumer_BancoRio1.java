/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.queues;

import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.BancoRioTipoPeticionEnum;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.WsTrackInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
public class QueueConsumer_BancoRio1 implements QueueConsumer {

    @Override
    public void consume(Object data0) throws Throwable {
        if (data0 instanceof Data) {
            Data data = (Data) data0;
            BancoRio bancoRio = Injection.lookup(BancoRio.class);
            bancoRio.registrarPeticion(data.tipoPeticion, data.info);
        }
    }

    @lombok.Data
    public static class Data {

        private BancoRioTipoPeticionEnum tipoPeticion;
        private WsTrackInfo info;
    }

}
