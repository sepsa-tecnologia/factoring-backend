/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.entities.notificacion.Plantilla;
import py.com.sepsa.genisys.ejb.enums.TipoPlantilla;
import py.com.sepsa.genisys.ejb.facades.notificacion.PlantillaFacade;
import py.com.sepsa.genisys.ejb.logic.impl.Plantillas;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.DbFiller;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "TipoPlantillasGenerator", mappedName = "TipoPlantillasGenerator")
@LocalBean
public class TipoPlantillasGenerator implements Serializable {

    @EJB
    private PlantillaFacade facade;

    /**
     * Valores fijos para base de datos.
     */
    private ValoresFijos valores;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        this.valores = new ValoresFijos(facade);
        this.valores.fill();
    }

    private static class ValoresFijos extends DbFiller.EntityFacadeBacked<Plantilla, TipoPlantilla> {

        private final PlantillaFacade facade;

        private ValoresFijos(PlantillaFacade facade) {
            this.facade = facade;
        }

        @Override
        public Collection<TipoPlantilla> obtenerValoresFijosDisponibles() {
            return Arrays.asList(TipoPlantilla.values());
        }

        @Override
        public boolean equals(Plantilla valor, TipoPlantilla valorFijo) {
            boolean a = Objects.equals(valor.getAsunto(), valorFijo.getAsunto());
            boolean b = Objects.equals(valor.getCuerpo(), valorFijo.getCuerpo());
            return a && b;
        }

        @Override
        public AbstractFacade<Plantilla> getFacade() {
            return facade;
        }

        @Override
        public Plantilla obtenerValor(AbstractFacade<Plantilla> facade, TipoPlantilla valorFijo) {
            return ((PlantillaFacade) facade).findByAsuntoCuerpo(Plantillas.ASUNTO, valorFijo.getCuerpo());
        }

        @Override
        public Plantilla crearValor(AbstractFacade<Plantilla> facade, TipoPlantilla valorFijo) throws Throwable {
            // NOTE: El identificador para momento de notificacion es identico al valor en mercurio.
            // NOTE: El identificador para tipo de documento es identico al valor en mercurio.
            // NOTE: El identificador para tipo de notificacion es identico al valor en mercurio.
            Plantilla plantilla = new Plantilla();
            plantilla.setAsunto(Plantillas.ASUNTO);
            plantilla.setCuerpo(valorFijo.getCuerpo());
            plantilla.setIdMomentoNotificacion(1);
            plantilla.setIdTipoDocumento(5);
            plantilla.setIdTipoNotificacion(1);
            facade.create(plantilla);
            return plantilla;
        }

    }

}
