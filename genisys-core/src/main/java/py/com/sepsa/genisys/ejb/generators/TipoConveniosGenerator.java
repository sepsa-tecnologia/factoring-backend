/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoConvenio;
import py.com.sepsa.genisys.ejb.enums.TipoConvenioEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.TipoConvenioFacade;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.DbFiller;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "TipoConveniosGenerator", mappedName = "TipoConveniosGenerator")
@LocalBean
public class TipoConveniosGenerator implements Serializable {

    @EJB
    private TipoConvenioFacade facade;

    /**
     * Valores fijos para base de datos.
     */
    private ValoresFijos valores;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        this.valores = new ValoresFijos(facade);
        this.valores.fill();
    }

    private static class ValoresFijos extends DbFiller.EntityFacadeBacked<TipoConvenio, TipoConvenioEnum> {

        private final TipoConvenioFacade facade;

        private ValoresFijos(TipoConvenioFacade facade) {
            this.facade = facade;
        }

        @Override
        public Collection<TipoConvenioEnum> obtenerValoresFijosDisponibles() {
            return Arrays.asList(TipoConvenioEnum.values());
        }

        @Override
        public boolean equals(TipoConvenio valor, TipoConvenioEnum valorFijo) {
            return Objects.equals(valorFijo.descripcion(), valor.getDescripcion());
        }

        @Override
        public AbstractFacade<TipoConvenio> getFacade() {
            return facade;
        }

        @Override
        public TipoConvenio obtenerValor(AbstractFacade<TipoConvenio> facade, TipoConvenioEnum valorFijo) {
            return ((TipoConvenioFacade) facade).findByDescripcion(valorFijo.descripcion());
        }

        @Override
        public TipoConvenio crearValor(AbstractFacade<TipoConvenio> facade, TipoConvenioEnum valorFijo) throws Throwable {
            TipoConvenio tipoValor = new TipoConvenio();
            tipoValor.setDescripcion(valorFijo.descripcion());
            facade.create(tipoValor);
            return tipoValor;
        }

    }

}
