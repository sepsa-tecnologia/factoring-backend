/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import fa.gs.utils.collections.Lists;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.entities.info.Cargo;
import py.com.sepsa.genisys.ejb.facades.info.CargoFacade;
import py.com.sepsa.genisys.ejb.utils.Constantes;
import py.com.sepsa.genisys.ejb.utils.DbFiller;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "CargoGenerator", mappedName = "CargoGenerator")
@LocalBean
public class CargoGenerator implements Serializable {

    @EJB
    private CargoFacade facade;

    /**
     * Valores fijos para base de datos.
     */
    private ValoresFijos valores;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        this.valores = new ValoresFijos(facade);
        this.valores.fill();
    }

    private static class ValoresFijos extends DbFiller<Cargo, Integer> {

        private final CargoFacade facade;

        private ValoresFijos(CargoFacade facade) {
            this.facade = facade;
        }

        @Override
        public Cargo obtenerValor(Integer valorFijo) {
            return facade.findById(valorFijo);
        }

        @Override
        public Cargo crearValor(Integer valorFijo) throws Throwable {
            Cargo instance = new Cargo();
            instance.setId(valorFijo);
            instance.setDescripcion("Genérico");
            facade.create(instance);
            return instance;
        }

        @Override
        public Collection<Integer> obtenerValoresFijosDisponibles() {
            List<Integer> ids = Lists.empty();
            ids.add(Constantes.ID_CARGO_GENERICO);
            return ids;
        }

        @Override
        public boolean equals(Cargo valor, Integer valorFijo) {
            return Objects.equals(valor.getId(), valorFijo);
        }

    }

}
