/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.generators;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import py.com.sepsa.genisys.ejb.entities.factoring.Estado;
import py.com.sepsa.genisys.ejb.facades.factoring.EstadoFacade;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.DbFiller;

/**
 *
 * @author Fabio A. González Sosa
 */
@Startup
@Singleton(name = "EstadosFacturaGenerator", mappedName = "EstadosFacturaGenerator")
@LocalBean
public class EstadosFacturaGenerator extends LogicBean {

    @EJB
    private EstadoFacade facade;

    @EJB
    private Estados estados;

    /**
     * Valores fijos para base de datos.
     */
    private ValoresFijos valores;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        this.valores = new ValoresFijos(facade, estados);
        this.valores.fill();
    }

    private static class ValoresFijos extends DbFiller.EntityFacadeBacked<Estado, Estados.EstadoFacturaEnum> {

        private final Estados estados;

        private final EstadoFacade facade;

        private ValoresFijos(EstadoFacade facade, Estados estados) {
            this.facade = facade;
            this.estados = estados;
        }

        @Override
        public Collection<Estados.EstadoFacturaEnum> obtenerValoresFijosDisponibles() {
            return Arrays.asList(Estados.EstadoFacturaEnum.values());
        }

        @Override
        public boolean equals(Estado valor, Estados.EstadoFacturaEnum valorFijo) {
            return Objects.equals(valorFijo.getId(), valor.getId());
        }

        @Override
        public AbstractFacade<Estado> getFacade() {
            return facade;
        }

        @Override
        public Estado obtenerValor(AbstractFacade<Estado> facade, Estados.EstadoFacturaEnum valorFijo) {
            return estados.findById(valorFijo.getId());
        }

        @Override
        public Estado crearValor(AbstractFacade<Estado> facade, Estados.EstadoFacturaEnum valorFijo) throws Throwable {
            Estado instance = new Estado();
            instance.setId(valorFijo.getId());
            instance.setDescripcion(valorFijo.getDescripcion());
            instance.setTipo(valorFijo.getTipo());
            facade.create(instance);
            return instance;
        }

    }

}
