/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.info;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class CuentaEntidadFinanciera implements Serializable {

    private Integer idCuenta;
    private Integer idEntidadFinanciera;
    private String razonSocialEntidadFinanciera;
    private Integer idPersona;
    private String nroCuenta;
    private Date fechaAlta;

    @Override
    public String toString() {
        return nroCuenta;
    }

}
