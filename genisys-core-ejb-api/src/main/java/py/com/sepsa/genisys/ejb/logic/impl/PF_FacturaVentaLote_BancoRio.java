/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.misc.Numeric;
import fa.gs.misc.fechas.Fechas;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java8.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioBanco;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioPersona;
import py.com.sepsa.genisys.ejb.entities.factoring.Convenio;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaConformada;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.info.ContactoEmail;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.FacturaConformadaFacade;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculoDesembolsoFacturaInfo;
import py.com.sepsa.genisys.ejb.logic.pojos.SolicitudDesembolsoResultadoInfo;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.integraciones.banco_rio.CalculoNetoFacturasInput;
import py.com.sepsa.genisys.integraciones.banco_rio.CalculoNetoFacturasOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.RespuestaCalculoNetoFacturas;
import py.com.sepsa.genisys.integraciones.banco_rio.SolicitudDesembolsoInput;
import py.com.sepsa.genisys.integraciones.banco_rio.SolicitudDesembolsoOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.CuentaInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_FacturaVentaLote_BancoRio", mappedName = "PF_FacturaVentaLote_BancoRio")
@LocalBean
public class PF_FacturaVentaLote_BancoRio extends PF_FacturaVentaLote_Base {

    @EJB
    private Facturas facturas;

    @EJB
    private BancoRio integracionBancoRio;

    @EJB
    private FacturaConformadaFacade facturaConformadaFacade;

    @EJB
    private Convenios convenios;

    @EJB
    private Emails emails;

    public Collection<CalculoDesembolsoFacturaInfo> calcularDesembolsos(Usuario usuario, Perfil perfil, List<FacturaInfo> facturas, Persona entidadFinanciera, Date fechaAdelanto) throws Throwable {
        final Collection<CalculoDesembolsoFacturaInfo> calculos = Lists.empty();

        // Realizar calculos por grupo de compradores.
        Map<Integer, List<FacturaInfo>> facturasPorComprador = this.facturas.agruparPorComprador(facturas);
        for (Map.Entry<Integer, List<FacturaInfo>> entry : facturasPorComprador.entrySet()) {
            // Datos de grupo de facturas.
            facturas = entry.getValue();
            Integer idEntidadFinanciera = entidadFinanciera.getId();
            Integer idComprador = entry.getKey();
            Persona comprador = personas.obtenerPorId(idComprador).value();
            Integer idProveedor = fa.gs.utils.collections.Lists.first(facturas).getProveedorId();
            Persona proveedor = personas.obtenerPorId(idProveedor).value();

            if (!Assertions.isNullOrEmpty(facturas)) {
                // Buscar convenio.
                Result<Collection<Convenio>> resConvenios = convenios.obtenerConvenios(idComprador, idProveedor, idEntidadFinanciera);
                Collection<Convenio> convenios0 = resConvenios.value(Lists.empty());

                // Control.
                if (Assertions.isNullOrEmpty(convenios0)) {
                    throw Errors.illegalState("No se encontraron datos de convenio para operar con el comprador '%s'.", Text.razonSocial(comprador));
                }

                // Control.
                if (convenios0.size() != 1) {
                    throw Errors.illegalState("No existe un único convenio para operar con el comprador '%s'.", Text.razonSocial(comprador));
                }

                // Adaptar datos de entrada.
                CalculoNetoFacturasInput input = new CalculoNetoFacturasInput();
                input.setDocumentoComprador(integracionBancoRio.resolveDocumento(comprador));
                input.setDocumentoProveedor(integracionBancoRio.resolveDocumento(proveedor));
                input.setPorcentajeUsoPlataforma(convenios.obtenerPorcentajeUsoPlataforma());
                input.setFacturas(Lists.empty());
                for (FacturaInfo factura0 : facturas) {
                    py.com.sepsa.genisys.integraciones.banco_rio.pojos.FacturaInfo facturaInfo0 = new py.com.sepsa.genisys.integraciones.banco_rio.pojos.FacturaInfo();
                    facturaInfo0.setNumero(factura0.getNumero());
                    facturaInfo0.setFechaVencimiento(factura0.getFechaPago());
                    facturaInfo0.setMonto(factura0.getMontoTotalConformado());
                    input.getFacturas().add(facturaInfo0);
                }

                // Consumir servicio.
                Integer idPersona = usuario.getIdPersona();
                Integer idPerfil = perfil.getId();
                Result<CalculoNetoFacturasOutput> resOutput = integracionBancoRio.calculoNetoFacturas(idPersona, idPerfil, idEntidadFinanciera, input);
                resOutput.raise();

                // Control.
                CalculoNetoFacturasOutput output = resOutput.value();
                if (!output.getRespuesta().isOk()) {
                    String msg = integracionBancoRio.resolveErrorMessage(output.getTrack(), output.getRespuesta());
                    throw Errors.illegalState(msg);
                }

                // Adaptar datos de salida.
                for (RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura detalle : output.getRespuesta().getDetalles()) {
                    CalculoDesembolsoFacturaInfo calculo = new CalculoDesembolsoFacturaInfo();
                    calculo.setOk(detalle.getMensaje().equals("OK"));
                    calculo.setMessage(detalle.getMensaje());
                    calculo.setFactura(buscarFactura(facturas, detalle));
                    calculo.setConvenio(null);
                    calculo.setDiasAnticipo(detalle.getDiasInteres());
                    calculo.setPorcentajeDescuento(obtenerPorcentajeDescuento(detalle));
                    calculo.setMontoDescuento(obtenerMontoDescuento(detalle));
                    calculo.setMontoDistribucion(detalle.getUsoPlataforma());
                    calculo.setMontoDesembolso(detalle.getNeto());
                    calculo.setExtras(detalle);

                    // Agregar a datos.
                    calculos.add(calculo);
                }
            }
        }

        return calculos;
    }

    public BigDecimal obtenerPorcentajeDescuento(RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura detalle) {
        BigDecimal montoConformado = detalle.getMonto();
        BigDecimal montoDescontado = obtenerMontoDescuento(detalle);
        BigDecimal porcentajeDescuento = Numeric.mul(Numeric.div(montoDescontado, montoConformado), Numeric.CIEN);
        porcentajeDescuento = Numeric.round(porcentajeDescuento, 2);
        return porcentajeDescuento;
    }

    public BigDecimal obtenerMontoDescuento(RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura detalle) {
        BigDecimal montoDescontado = Numeric.sum(detalle.getGastosAdministrativos(), detalle.getInteres(), detalle.getIvaGastosAdministrativos(), detalle.getIvaSeguro(), detalle.getSeguro(), detalle.getUsoPlataforma());
        return montoDescontado;
    }

    private FacturaInfo buscarFactura(Collection<FacturaInfo> facturas, RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura detalle) {
        for (FacturaInfo factura : facturas) {
            if (factura.getNumero().equals(detalle.getNroFactura())) {
                Date a = Fechas.inicioDia(factura.getFechaPago());
                Date b = Fechas.inicioDia(detalle.getFechaVto());
                if (a.compareTo(b) == 0) {
                    return factura;
                }
            }
        }
        return null;
    }

    public SolicitudDesembolsoResultadoInfo vender(Usuario usuario, Perfil perfil, Integer idLote, Collection<CalculoDesembolsoFacturaInfo> calculos, Persona entidadFinanciera) {
        // Datos iniciales.
        Integer idPerfilComprador = perfiles.obtenerPerfil(PerfilEnum.COMPRADOR).getId();
        Integer idPerfilProveedor = perfiles.obtenerPerfil(PerfilEnum.PROVEEDOR).getId();
        Integer idEntidadFinanciera = entidadFinanciera.getId();
        BigDecimal usoPlataforma = convenios.obtenerPorcentajeUsoPlataforma();

        // Agrupar facturas por comprador.
        List<FacturaInfo> facturas = Lists.stream(calculos).map(c -> c.getFactura()).collect(Collectors.toList());
        Map<Integer, List<FacturaInfo>> grupos = this.facturas.agruparPorComprador(facturas);
        for (Map.Entry<Integer, List<FacturaInfo>> grupo : grupos.entrySet()) {
            // Datos de grupo de facturas.
            facturas = grupo.getValue();
            Integer idComprador = grupo.getKey();
            Persona comprador = personas.obtenerPorId(idComprador).value();
            Integer idProveedor = fa.gs.utils.collections.Lists.first(facturas).getProveedorId();
            Persona proveedor = personas.obtenerPorId(idProveedor).value();

            if (!Assertions.isNullOrEmpty(facturas)) {
                // Persona Comprador.
                Result<BancoRioPersona> resPersonaComprador = integracionBancoRio.obtenerPersona(idComprador, idPerfilComprador);
                BancoRioPersona personaComprador = resPersonaComprador.value(null);
                if (personaComprador == null) {
                    return venderKo("El comprador '%s' no está adherido a la entidad financiera seleccionada.", Text.ruc(comprador));
                }

                // Persona Proveedor.
                Result<BancoRioPersona> resPersonaProveedor = integracionBancoRio.obtenerPersona(idProveedor, idPerfilProveedor);
                BancoRioPersona personaProveedor = resPersonaProveedor.value(null);
                if (personaProveedor == null) {
                    return venderKo("El proveedor '%s' no está adherido a la entidad financiera seleccionada.", Text.ruc(proveedor));
                }

                // Banco de proveedor.
                Result<BancoRioBanco> resBancoProveedor = integracionBancoRio.obtenerBanco(personaProveedor.getIdBanco());
                BancoRioBanco bancoProveedor = resBancoProveedor.value(null);
                if (bancoProveedor == null) {
                    return venderKo("El proveedor '%s' no registra ningún banco.", Text.ruc(proveedor));
                }

                // Adaptar datos de entrada.
                SolicitudDesembolsoInput input = new SolicitudDesembolsoInput();
                input.setPorcentajeUsoPlataforma(usoPlataforma);
                input.setComprador(comprador);
                input.setCompradorDocumento(integracionBancoRio.resolveDocumento(comprador));
                input.setCompradorCodCliente(personaComprador.getCodigoCliente());
                input.setCompradorEmailNotificacion(resolveEmail(comprador));
                input.setProveedor(proveedor);
                input.setProveedorDocumento(integracionBancoRio.resolveDocumento(proveedor));
                input.setProveedorCodCliente(personaProveedor.getCodigoCliente());
                input.setProveedorEmailNotificacion(resolveEmail(proveedor));
                input.setProveedorCuentaAcreditacion(new CuentaInfo());
                input.getProveedorCuentaAcreditacion().setNroCuenta(personaProveedor.getNroCuenta());
                input.getProveedorCuentaAcreditacion().setBanco(bancoProveedor);
                input.setFacturas(Lists.empty());
                for (FacturaInfo factura0 : facturas) {
                    py.com.sepsa.genisys.integraciones.banco_rio.pojos.FacturaInfo facturaInfo0 = new py.com.sepsa.genisys.integraciones.banco_rio.pojos.FacturaInfo();
                    facturaInfo0.setNumero(factura0.getNumero());
                    facturaInfo0.setFechaVencimiento(factura0.getFechaPago());
                    facturaInfo0.setMonto(factura0.getMontoTotalConformado());
                    input.getFacturas().add(facturaInfo0);
                }

                // Consumir servicio.
                Integer idPersona = usuario.getIdPersona();
                Integer idPerfil = perfil.getId();
                Result<SolicitudDesembolsoOutput> resOutput = integracionBancoRio.solicitudDesembolso(idPersona, idPerfil, idEntidadFinanciera, input);
                if (resOutput.isFailure()) {
                    log.error(resOutput.failure().cause(), resOutput.failure().cause().getMessage());
                    return venderKo("Ocurrió un error solicitando desembolso para facturas de comprador '%s'.", Text.razonSocial(comprador));
                }

                // Control.
                SolicitudDesembolsoOutput output = resOutput.value();
                if (!output.getRespuesta().isOk()) {
                    String msg = integracionBancoRio.resolveErrorMessage(output.getTrack(), output.getRespuesta());
                    return venderKo(msg);
                }

                // Registrar seguimiento pendiente para lote.
                String idSolicitud = output.getRespuesta().getIdSolicitud();
                String nroOperacion = output.getRespuesta().getNroOperacion();
                Result<?> resRegistroSeguimiento = integracionBancoRio.registrarSolicitudDesembolso(idComprador, idProveedor, idLote, idSolicitud, nroOperacion);
                if (resRegistroSeguimiento.isFailure()) {
                    log.error(resRegistroSeguimiento.failure().cause(), resRegistroSeguimiento.failure().cause().getMessage());
                    return venderKo("Ocurrió un error registrando seguimiento de desembolso para facturas de comprador '%s'.", Text.razonSocial(comprador));
                }

                // Modificar datos de facturas.
                for (FacturaInfo factura : facturas) {
                    // Cambiar estado de factura.
                    Integer idFactura = factura.getId();
                    Result<?> resCambioEstado = cambiarEstadoFactura(usuario.getId(), idFactura, Estados.EstadoFacturaEnum.EN_ESTUDIO_POR_ENTIDAD, "");
                    if (resCambioEstado.isFailure()) {
                        String msg = fa.gs.utils.misc.text.Text.select(resCambioEstado.failure().message(), resCambioEstado.failure().cause().getMessage());
                        return venderKo("Ocurrió un error modificando estado para factura '%s' de comprador '%s'. %s.", factura.getNumero(), Text.razonSocial(comprador), msg);
                    }

                    // Consolidar valores de venta.
                    CalculoDesembolsoFacturaInfo calculo = buscarCalculo(calculos, factura);
                    Result<?> resModificacionFacturaConformada = modificarFacturaConformadaAsociada(calculo);
                    if (resModificacionFacturaConformada.isFailure()) {
                        String msg = fa.gs.utils.misc.text.Text.select(resModificacionFacturaConformada.failure().message(), resModificacionFacturaConformada.failure().cause().getMessage());
                        return venderKo("Ocurrió un error modificando valores de desembolso para factura '%s' de comprador '%s'. %s.", factura.getNumero(), Text.razonSocial(comprador), msg);
                    }
                }

                // Emitir notificaciones.
                notificaciones.notificarVentaDeFacturas(facturas);
            }
        }

        // Ok.
        SolicitudDesembolsoResultadoInfo instance = new SolicitudDesembolsoResultadoInfo();
        instance.setOk(true);
        instance.setErr(null);
        return instance;
    }

    private String resolveEmail(Persona persona) {
        try {
            List<ContactoEmail> contactos = emails.obtenerContactosEmailDisponibles(persona.getId());
            ContactoEmail contacto = Lists.first(contactos);
            return contacto.getEmail();
        } catch (Throwable thr) {
            return "";
        }
    }

    private Result<Void> modificarFacturaConformadaAsociada(CalculoDesembolsoFacturaInfo info) {
        Result<Void> result;

        try {
            FacturaConformada facturaConformada = facturaConformadaFacade.findByIdFactura(info.getFactura().getId());
            facturaConformada.setIdEntidadFinanciera(integracionBancoRio.obtenerIdEntidadBancoRio());
            facturaConformada.setIdConvenio(info.getConvenio());
            facturaConformada.setFechaVenta(py.com.sepsa.genisys.ejb.utils.Fechas.ahora());
            facturaConformada.setPorcentajeComision(info.getPorcentajeDescuento());
            facturaConformada.setDiasAnticipo(info.getDiasAnticipo());
            facturaConformada.setMontoNeto(info.getMontoDesembolso());
            facturaConformada.setMontoImponibleComision(info.getMontoDescuento());
            facturaConformada.setMontoImpuestoComision(fa.gs.utils.misc.numeric.Numeric.CERO);
            facturaConformada.setMontoTotalComision(info.getMontoDescuento());
            facturaConformada.setMontoDistribucion(info.getMontoDistribucion());
            facturaConformada.setMontoImponibleGastosOperativos(fa.gs.utils.misc.numeric.Numeric.CERO);
            facturaConformada.setMontoImpuestoGastosOperativos(fa.gs.utils.misc.numeric.Numeric.CERO);
            facturaConformada.setMontoTotalGastosOperativos(fa.gs.utils.misc.numeric.Numeric.CERO);
            facturaConformadaFacade.edit(facturaConformada);
            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error modificando registro de factura conformada para factura id='%s'", info.getFactura().getId()).build();
        }

        return result;
    }

}
