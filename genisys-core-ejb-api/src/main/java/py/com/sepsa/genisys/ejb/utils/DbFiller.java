/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import fa.gs.utils.misc.errors.Errors;
import java.util.Collection;
import java.util.LinkedList;

/**
 *
 * @author Fabio A. González Sosa
 */
public abstract class DbFiller<T, V> {

    /**
     * Coleccion de valores concretos que representan a los valores fijos dentro
     * de algun medio de persistencia, idealmente la base de datos.
     */
    private final Collection<T> valores;

    /**
     * Constructor.
     */
    public DbFiller() {
        this.valores = new LinkedList<>();
    }

    /**
     * Mapea una representacion estatica de un valor fijo a un valor concreto.
     *
     * @param valorFijo Valor fijo.
     * @return Valor concreto.
     */
    public abstract T obtenerValor(V valorFijo);

    /**
     * Permite crear un valor concreto a partir de su representacio de valor
     * fijo.
     *
     * @param valorFijo Valor fijo.
     * @return Valor concreto.
     * @throws Throwable Error producido.
     */
    public abstract T crearValor(V valorFijo) throws Throwable;

    /**
     * Obtiene la coleccion de valores fijos disponibles.
     *
     * @return Coleccion de valores fijos.
     */
    public abstract Collection<V> obtenerValoresFijosDisponibles();

    /**
     * Determina si un valor concreto es equivalente a una representacion de
     * valor fijo, y viceversa.
     *
     * @param valor Valor concreto.
     * @param valorFijo Valor fijo.
     * @return {@code true} si son equivalentes, caso contrario {@code false}.
     */
    public abstract boolean equals(T valor, V valorFijo);

    /**
     * Permite obtener o crear los valores concretos necesarios de acuerdo a la
     * coleccion de valores fijos disponibles.
     */
    public final void fill() {
        for (V valorFijo : obtenerValoresFijosDisponibles()) {
            T valor = obtenerOCrearValor(valorFijo);
            if (valor != null) {
                valores.add(valor);
            }
        }
    }

    /**
     * Obtiene o crea un valor concreto para un valor fijo dado.
     *
     * @param valorFijo Valor fijo.
     * @return Valor concreto.
     */
    private T obtenerOCrearValor(V valorFijo) {
        // Verificar si ya existe el valor concreto.
        T valor = obtenerValor(valorFijo);
        if (valor != null) {
            return valor;
        }

        // Crear el valor concreto.
        try {
            valor = crearValor(valorFijo);
            return valor;
        } catch (Throwable thr) {
            Errors.dump(System.err, thr);
            return null;
        }
    }

    /**
     * Obtiene la coleccion de valores concretos inicializados.
     *
     * @return Coleccion de valores concretos.
     */
    public Collection<T> obtenerValores() {
        return valores;
    }

    /**
     * Mapea un valor concreto a un valor fijo.
     *
     * @param valor Valor concreto.
     * @return Valor fijo.
     */
    public V mapValor(T valor) {
        if (valor == null) {
            return null;
        } else {
            for (V valorFijo : obtenerValoresFijosDisponibles()) {
                if (equals(valor, valorFijo)) {
                    return valorFijo;
                }
            }
            return null;
        }
    }

    /**
     * Mapea un valor fijo a un valor concreto.
     *
     * @param valorFijo Valor fijo.
     * @return Valor concreto.
     */
    public T mapValorFijo(V valorFijo) {
        for (T valor : valores) {
            if (equals(valor, valorFijo)) {
                return valor;
            }
        }
        return null;
    }

    /**
     * Subclase que permite manipular valores concretos desde base de datos.
     *
     * @param <T> Tipo de valor concreto.
     * @param <V> Tipo de valor fijo.
     */
    public abstract static class EntityFacadeBacked<T, V> extends DbFiller<T, V> {

        /**
         * Inicializar el facade a utilizar.
         *
         * @return Facade.
         */
        protected abstract AbstractFacade<T> getFacade();

        @Override
        public final T obtenerValor(V valorFijo) {
            return obtenerValor(getFacade(), valorFijo);
        }

        /**
         * Permite crear un valor concreto a partir de su representacio de valor
         * fijo.
         *
         * @param facade Facade.
         * @param valorFijo Valor fijo.
         * @return Valor concreto.
         */
        public abstract T obtenerValor(AbstractFacade<T> facade, V valorFijo);

        @Override
        public final T crearValor(V valorFijo) throws Throwable {
            return crearValor(getFacade(), valorFijo);
        }

        /**
         * Permite crear un valor concreto a partir de su representacio de valor
         * fijo.
         *
         * @param facade Facade.
         * @param valorFijo Valor fijo.
         * @return Valor concreto.
         * @throws Throwable Error producido.
         */
        public abstract T crearValor(AbstractFacade<T> facade, V valorFijo) throws Throwable;

    }

}
