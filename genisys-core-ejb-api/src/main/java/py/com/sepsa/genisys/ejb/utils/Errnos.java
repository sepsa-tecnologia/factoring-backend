/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Errnos {

    public static int ERRNO_ERROR = -10000;
    public static int ERRNO_LINEA_CREDITO_INSUFICIENTE = ERRNO_ERROR - 1;
    public static int ERRNO_LINEA_CREDITO_CORRUPTA = ERRNO_ERROR - 2;
    public static int ERRNO_LINEA_CREDITO_NO_EXISTE = ERRNO_ERROR - 3;
    public static int ERRNO_PROVEEDOR_NO_EXISTE = ERRNO_ERROR - 4;
    public static int ERRNO_CREDENCIALES_INVALIDAS = ERRNO_ERROR - 5;

}
