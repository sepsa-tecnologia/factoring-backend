/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import fa.gs.utils.misc.text.Strings;
import java.util.Iterator;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Injection {

    /**
     * Obtiene el valor de nombre actualmente asignado al modulo deployado en el
     * contenedor.
     *
     * @return Nombre de modulo.
     */
    public static String appName() {
        String appName = lookup("java:app/AppName");
        return appName;
    }

    /**
     * Realiza una búsqueda manual para la inyección de beans a través de JNDI.
     *
     * @param <T> Tipo esperado del bean a inyectar.
     * @param jndi Nombre JNDI del bean a inyectar.
     * @return Bean asociado al nombre dado. Caso contrario, {@code null}.
     */
    public static <T> T lookup(String jndi) {
        try {
            Context c = new InitialContext();
            Object obj = c.lookup(jndi);
            if (obj == null) {
                return null;
            } else {
                return (T) obj;
            }
        } catch (Throwable thr) {
            thr.printStackTrace(System.err);
            return null;
        }
    }

    /**
     * Realiza una busqueda para la inyeccion de beans EJB.
     *
     * @param <T> Tipo esperado del bean a inyectar.
     * @param klass Clase del bean a inyectar.
     * @return Instancia del bean indicado, si hubiere. Caso contrario
     * {@code null}.
     */
    public static <T> T lookup(Class<T> klass) {
        String jndi = Strings.format("java:app/%s/%s", appName(), klass.getSimpleName());
        return Injection.lookup(jndi);
    }

    /**
     * Realiza una busqueda para la inyeccion de beans CDI.
     *
     * @param <T> Tipo esperado del bean a inyectar.
     * @param cdiBeanClass Clase del bean CDI a inyectar.
     * @return Instancia del bean indicado, si hubiere. Caso contrario
     * {@code null}.
     */
    public static <T> T lookupCdiBean(Class<T> cdiBeanClass) {
        try {
            //CDI.current().getBeanManager().se
            Object bean = CDI.current().select(cdiBeanClass).get();
            return (bean != null) ? cdiBeanClass.cast(bean) : null;
        } catch (Throwable thr) {
            thr.printStackTrace(System.err);
            return null;
        }
    }

    /**
     * Realiza una busqueda para la inyeccion de beans CDI.
     * <br/>
     * Fuente:
     * <a href="https://github.com/erny/activiti/blob/master/modules/activiti-cdi/src/main/java/org/activiti/cdi/impl/util/ProgrammaticBeanLookup.java">erny/activiti</a>
     *
     * @param <T> Tipo esperado del bean a inyectar.
     * @param cdiBeanClass Clase del bean CDI a inyectar.
     * @param manager Manejador de beans utilizado por algun contexto CDI.
     * @return Instancia del bean indicado, si hubiere. Caso contrario
     * {@code null}.
     */
    public static <T> T lookupCdiBean(Class<T> cdiBeanClass, BeanManager manager) {
        try {
            Iterator<Bean<?>> iter = manager.getBeans(cdiBeanClass).iterator();
            Bean<T> bean = (Bean<T>) iter.next();
            CreationalContext<T> ctx = manager.createCreationalContext(bean);
            Object instance = manager.getReference(bean, cdiBeanClass, ctx);
            return (instance != null) ? cdiBeanClass.cast(instance) : null;
        } catch (Throwable thr) {
            thr.printStackTrace(System.err);
            return null;
        }
    }

    /**
     * Realiza una búsqueda manual para la inyección de managed beans.
     *
     * @param managedBeanClass Clase del bean a inyectar.
     * @return Manged Bean asociado a la clase indicada. Caso contrario,
     * {@code null}.
     */
    public static <T> T lookupManagedBean(Class<T> managedBeanClass) {
        try {
            BeanManager bm = Injection.lookup("java:comp/BeanManager");
            Bean<T> mbean = (Bean<T>) bm.getBeans(managedBeanClass).iterator().next();
            CreationalContext<T> ctx = bm.createCreationalContext(mbean);
            return (T) (bm.getReference(mbean, managedBeanClass, ctx));
        } catch (Throwable thr) {
            thr.printStackTrace(System.err);
            return null;
        }
    }

}
