/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic;

import fa.gs.utils.logging.app.AppLogger;
import java.io.Serializable;
import javax.ejb.EJB;
import py.com.sepsa.genisys.ejb.entities.factoring.Estado;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
public class LogicBean implements Serializable {

    /**
     * Referencia a todos los bens de logica disponibles.
     */
    @EJB
    protected LogicBeans beans;

    /**
     * Log de datos.
     */
    protected AppLogger log;
    //</editor-fold>

    /**
     * Constructor.
     */
    public LogicBean() {
        this.log = AppLoggerFactory.core();
    }

    /**
     * Permite analizar la traza de una excepcion o fallo producido para
     * determinar si la causa del mismo es una excepcion de determinada clase.
     *
     * @param throwable Excepcion producida durante alguna operacion.
     * @param klass Clase de la excepcion que se desea encontrar.
     * @return Excepcion que figura como causante, si hubiere.
     */
    protected Throwable getFailureCause(Throwable throwable, Class<? extends Throwable> klass) {
        Throwable cause = throwable.getCause();
        while (cause != null) {
            if (klass.isInstance(cause)) {
                return cause;
            }
            cause = cause.getCause();
        }
        return null;
    }

    /**
     * Permite reducir la cantidad de codigo necesario para obtener un estado
     * dado.
     *
     * @param estadoEnum Enumeracion de estado.
     * @return Entidad estado correspondiente a la enumeracion indicada.
     */
    protected Estado getEstado(Estados.EstadoEnum estadoEnum) {
        return beans.getEstados().find(estadoEnum);
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public LogicBeans getBeans() {
        return beans;
    }

    public void setBeans(LogicBeans beans) {
        this.beans = beans;
    }
    //</editor-fold>

}
