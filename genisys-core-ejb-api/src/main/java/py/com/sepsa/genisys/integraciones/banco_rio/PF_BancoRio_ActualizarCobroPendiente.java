/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.fechas.Fechas;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.TxWrapperCore;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_ActualizarCobroPendiente", mappedName = "PF_BancoRio_ActualizarCobroPendiente")
@LocalBean
public class PF_BancoRio_ActualizarCobroPendiente implements Serializable {

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Facturas facturas;

    @EJB
    private Personas personas;

    @EJB
    private Perfiles perfiles;

    @EJB
    private Notificaciones notificaciones;

    private AppLogger log;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    public void work(Integer idFactura) throws Throwable {
        // Obtener identificador de usuario para ingestion de datos.
        Integer idUsuarioIngestion = bancoRio.obtenerIdUsuarioIngestion();
        if (idUsuarioIngestion <= 0) {
            throw Errors.illegalState("El usuario para la ingestión no está establecido");
        }

        // Dato de factura.
        Result<FacturaInfo> resFactura = facturas.buscarFactura(idFactura);
        resFactura.raise();
        FacturaInfo factura = resFactura.value();

        // Comentario.
        String comentario = "INGESTIÓN MANUAL";

        // Procesar factura.
        ingestFactura(idUsuarioIngestion, factura, comentario);
    }

    public void work(Integer idUsuarioIngestion, Integer diasTolerancia, Persona comprador) throws Throwable {
        // Fecha actual.
        Date hoy = Fechas.now();
        Date fechaHasta = Fechas.finDia(hoy);
        Date fecahDesde = Fechas.addDias(fechaHasta, -1 * diasTolerancia);

        // Preparar datos de entrada.
        ConsultaFacturasLiquidablesInput input = new ConsultaFacturasLiquidablesInput();
        input.setComprador(comprador);
        input.setCompradorDocumento(bancoRio.resolveDocumento(comprador));
        input.setFechaDesde(fecahDesde);
        input.setFechaHasta(fechaHasta);

        // Consultar facturas liquidables.
        log.debug("consultando facturas para liquidar id_comprador=%s", comprador.getId());
        Result<ConsultaFacturasLiquidablesOutput> resRespuesta = bancoRio.consultarFacturasLiquidables(input);
        if (resRespuesta.isFailure()) {
            log.error()
                    .cause(resRespuesta.failure().cause())
                    .message("Ocurrió un error consultando facturas liquidables")
                    .tag("id_comprador", comprador.getId())
                    .log();
            throw Errors.illegalState(resRespuesta.failure().cause(), "Ocurrió un error consultando facturas liquidables");
        }

        // Control.
        ConsultaFacturasLiquidablesOutput output = resRespuesta.value();
        if (!output.getRespuesta().isOk()) {
            String msg = bancoRio.resolveErrorMessage(output.getTrack(), output.getRespuesta());
            log.warning()
                    .cause(resRespuesta.failure().cause())
                    .message("Ocurrió un error consultando facturas liquidables. %s", msg)
                    .tag("id_comprador", comprador.getId())
                    .log();
            throw Errors.illegalState(resRespuesta.failure().cause(), "Ocurrió un error consultando facturas liquidables (%s)", msg);
        }

        // Procesar facturas.
        if (output.getRespuesta().isOk()) {
            Collection<RespuestaConsultaFacturasLiquidables.FacturaALiquidar> facturas = output.getRespuesta().getFacturas();
            if (!Assertions.isNullOrEmpty(facturas)) {
                for (RespuestaConsultaFacturasLiquidables.FacturaALiquidar factura : facturas) {
                    // Obtener datos de factura.
                    // NOTE: ANTERIORMENTE SE INCLUIA LA FECHA DE VENCIMIENTO EN LA BUSQUEDA, PERO LA FECHA RETORNADA POR EL SERVICIO DEL BANCO DIFIERE CON EL VALOR INGRESADO POR EL PROVEEDOR.
                    Result<FacturaInfo> resFactura = this.facturas.buscarFactura(comprador.getId(), factura.getNroFactura(), factura.getMonto());
                    FacturaInfo facturaInfo = resFactura.value(null);
                    if (facturaInfo == null) {
                        continue;
                    }

                    // Verificar que la factura este desembolsada.
                    boolean ok = this.facturas.checkFacturaEnEstado(facturaInfo, Estados.EstadoFacturaEnum.PAGADA);
                    if (!ok) {
                        continue;
                    }

                    // Procesar factura.
                    TxWrapperCore tx = Injection.lookup(TxWrapperCore.class);
                    Result<?> result = tx.execute(() -> ingestFactura(idUsuarioIngestion, facturaInfo, output.getRespuesta().getMensaje()));
                    if (result.isFailure()) {
                        notificaciones.notificarSepsa(result.failure().cause(), "Ocurrió un error procesando actualización de estado de cobro para factura id=%s", facturaInfo.getId());
                    }
                }
            }
        }
    }

    private Void ingestFactura(Integer idUsuarioIngestion, FacturaInfo facturaInfo, String comentario) throws Throwable {
        try {
            // Nuevo estado de factura.
            Estados.EstadoFacturaEnum estado = Estados.EstadoFacturaEnum.COBRADA;

            // Cambiar estado de factura, si hubiere.
            Result<Factura> resCambioEstado = null;
            resCambioEstado = facturas.cambiarEstado(idUsuarioIngestion, facturaInfo.getId(), estado);
            resCambioEstado.raise();

            // Agregar historico, si hubiere.
            comentario = comentario.trim();
            String msg = "RESPUESTA AUTOMATICA DE ENTIDAD" + (Assertions.stringNullOrEmpty(comentario) ? "" : (": " + comentario));
            Result<HistoricoFactura> resHistorico = facturas.agregarHistorico(idUsuarioIngestion, resCambioEstado.value(), estado, msg);
            resHistorico.raise();

            return null;
        } catch (Throwable thr) {
            throw Errors.illegalState(thr, "No se pudo procesar la factura id=%s", facturaInfo.getId());
        }
    }

}
