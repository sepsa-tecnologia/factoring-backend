/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.sepsa;

import fa.gs.utils.result.simple.Result;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;

/**
 *
 * @author Fabio A. González Sosa
 */
public interface ClienteEdiServer {

    Result<String> obtenerTokenAutenticacion();

    Result<RegistrarLiquidacionOutput> registrarLiquidacion(String token, RegistrarLiquidacionInput input);

    Result<Integer> obtenerOCrearIdClienteSepsa(String token, String ruc, String razonSocial, Personas.TipoPersonaEnum tipoPersona);

}
