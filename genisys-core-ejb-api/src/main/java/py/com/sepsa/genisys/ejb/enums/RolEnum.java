/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.enums;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum RolEnum {
    COMPRADOR(1),
    PROVEEDOR(2);
    private Integer idRol;

    RolEnum(Integer idRol) {
        this.idRol = idRol;
    }

    public Integer idRol() {
        return idRol;
    }

}
