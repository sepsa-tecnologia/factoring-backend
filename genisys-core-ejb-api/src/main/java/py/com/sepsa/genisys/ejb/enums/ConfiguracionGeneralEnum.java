/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.enums;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum ConfiguracionGeneralEnum {
    SYS_DIRECTORIO_ARCHIVOS_SUBIDOS("SYS_DIRECTORIO_ARCHIVOS_SUBIDOS"),
    SYS_URL_PUBLICA("SYS_URL_PUBLICA"),
    /**
     * Indica el porcentaje de uso de plataforma a aplicar sobre calculo de
     * comisiones, si es necesario.
     */
    SYS_PORCENTAJE_USO_PLATAFORMA("core.porcentaje_uso_plataforma"),
    /**
     * Identificador de entidad dentro de base de datos SEPSA.
     */
    INTEGRACION_BANCO_RIO_ID_ENTIDAD("integracion.banco_rio.id_entidad_financiera"),
    /**
     * Datos de conexion/acceso a web service de registro de datos de
     * desembolso.
     */
    INTEGRACION_BANCO_RIO_WEB_SERVICE_URL("integracion.banco_rio.ws.url"),
    INTEGRACION_BANCO_RIO_WEB_SERVICE_USUARIO("integracion.banco_rio.ws.usuario"),
    INTEGRACION_BANCO_RIO_WEB_SERVICE_PASSWORD("integracion.banco_rio.ws.password"),
    INTEGRACION_BANCO_RIO_WEB_SERVICE_CLAVE_PUBLICA("integracion.banco_rio.ws.clave_publica"),
    INTEGRACION_BANCO_RIO_LIQUIDACION_CUENTA_SEPSA("integracion.banco_rio.liquidacion.cuenta_sepsa"),
    /**
     * Identificador de usuario para registro de eventos que involucren
     * facturas.
     */
    INTEGRACION_BANCO_RIO_INGESTION_ID_USUARIO("integracion.banco_rio.id_usuario_ingestion"),
    /**
     * Intervalo en milisegundos para ejecucion de consulta de facturas
     * desembolsadas.
     */
    INTEGRACION_BANCO_RIO_INGESTION_INTERVALO("integracion.banco_rio.intervalo_ingestion"),
    /**
     * Cantidad máxima de dias de tolerancia para considerar la consulta de
     * estado de desembolso para facturas.
     */
    INTEGRACION_BANCO_RIO_INGESTION_DIAS_TOLERANCIA("integracion.banco_rio.dias_tolerancia"),
    /**
     * Intervalo en milisegundos para ejecucion de notificación de facturas
     * conformadas a proveedor.
     */
    NOTIFICACION_FACTURA_CONFORMADA_PROVEEDOR_INTERVALO("notificacion.facturas_conformadas.intervalo_notificacion"),
    /**
     * Intervalo en dias para envío de notificaciones de facturas conformadas a
     * un proveedor
     */
    NOTIFICACION_FACTURA_CONFORMADA_PROVEEDOR_ESPERA("notificacion.facturas_conformadas.espera_notificacion"),
    /**
     * Intervalo en milisegundos para ejecucion de notificación de facturas
     * conformadas a proveedor.
     */
    NOTIFICACION_FACTURA_POR_VENCER_PROVEEDOR_INTERVALO("notificacion.facturas_por_vencer.intervalo_notificacion"),
    /**
     * Intervalo en dias para envío de notificaciones de facturas conformadas a
     * un proveedor
     */
    NOTIFICACION_FACTURA_POR_VENCER_PROVEEDOR_ESPERA("notificacion.facturas_por_vencer.espera_notificacion"),
    /**
     * Días de anticipo para notificaciones de facturas por vencer
     */
    NOTIFICACION_FACTURA_POR_VENCER_DIAS_ANTICIPO("notificacion.facturas_por_vencer.anticipo"),
    /**
     * Identificador de persona dentro de base de datos SEPSA.
     */
    INTEGRACION_BIGGIE_ID_PERSONA("integracion.biggie.id_persona"),
    /**
     * Identificador de usuario para registro de eventos que involucren
     * facturas.
     */
    INTEGRACION_BIGGIE_ID_USUARIO("integracion.biggie.id_usuario_ingestion"),
    /**
     * Intervalo en milisegundos para ejecucion de consulta de facturas
     * desembolsadas.
     */
    INTEGRACION_BIGGIE_INGESTION_INTERVALO("integracion.biggie.intervalo_ingestion"),
    /**
     * Parametros de gestion para notificador de correo.
     */
    NOTIFICADOR_SMTP_HOST("notificador.smtp.host"),
    NOTIFICADOR_SMTP_PORT("notificador.smtp.port"),
    NOTIFICADOR_SMTP_USERNAME("notificador.smtp.username"),
    NOTIFICADOR_SMTP_PASSWORD("notificador.smtp.password"),;
    private final String nombre;

    private ConfiguracionGeneralEnum(String nombre) {
        this.nombre = nombre;
    }

    public String nombre() {
        return nombre;
    }
}
