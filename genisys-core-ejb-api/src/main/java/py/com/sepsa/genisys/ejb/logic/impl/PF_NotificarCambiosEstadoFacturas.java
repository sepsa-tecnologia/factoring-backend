/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.result.simple.Result;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Todo;
import java.io.Serializable;
import java.util.List;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import py.com.sepsa.genisys.ejb.entities.info.Email;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoring;
import py.com.sepsa.genisys.ejb.entities.notificacion.Plantilla;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.facades.info.EmailFacade;
import py.com.sepsa.genisys.ejb.facades.notificacion.PlantillaFacade;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.email.EmailContent;
import py.com.sepsa.genisys.ejb.utils.email.EmailContentType;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_NotificarCambiosEstadoFacturas", mappedName = "PF_NotificarCambiosEstadoFacturas")
@LocalBean
public class PF_NotificarCambiosEstadoFacturas implements Serializable {

    private AppLogger log;

    @EJB
    private Notificaciones notificaciones;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.core();
    }

    public void work() throws Throwable {
        // Obtener sesion con servidor de correo.
        SmtpAuthentication authenticaion = getSmtpAuthentication();
        Session session = getSmtpSession(authenticaion);

        // Obtener notificaciones pendientes.
        List<NotificacionEmailFactoring> pendientes = notificaciones.obtenerNotificacionesPendientes();
        int ok = 0;
        int ko = 0;
        for (int i = 0; i < pendientes.size(); i++) {
            NotificacionEmailFactoring pendiente = pendientes.get(i);

            // Control.
            // TODO: Controlar fecha actual contra pendiente.getFechaEnviar().
            Todo.TODO();

            try {
                // Generar contenido apto para correo.
                EmailContent contenido = obtenerContenido(pendiente);

                // Enviar mensaje a destinatario/s.
                if (!fa.gs.misc.Assertions.isNullOrEmpty(contenido.getDestinatarios())) {
                    for (String destinatario : contenido.getDestinatarios()) {
                        try {
                            // Enviar mensaje.
                            MimeMessage message = new MimeMessage(session);
                            message.setFrom(new InternetAddress(authenticaion.username));
                            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario));
                            message.setSubject(contenido.getAsunto());
                            message.setContent(contenido.getCuerpo(), contenido.getContentType().mimeType());
                            Transport.send(message);

                            // Cambiar estado de notificacion.
                            notificaciones.marcarNotificacionComoEnviada(pendiente.getId());

                            ok = ok + 1;
                        } catch (Throwable thr0) {
                            // Ko.
                            ko = ko + 1;
                            log.warning()
                                    .cause(thr0)
                                    .message("Ocurrio un error creando notificacion")
                                    .tag("notificacion_email_factoring.id", pendiente.getId())
                                    .log();
                        }
                    }
                }
            } catch (Throwable thr) {
                log.error()
                        .cause(thr)
                        .message("Ocurrio un error procesando notificaciones")
                        .log();
            }
        }

        // Ok.
        log.info("Notificaciones procesadas. TOTAL=%s OK=%s KO=%s", pendientes.size(), ok, ko);
    }

    private EmailContent obtenerContenido(NotificacionEmailFactoring notificacion) throws Throwable {
        // Obtenemos el email del contacto.
        EmailFacade emailFacade = Injection.lookup(EmailFacade.class);
        Email email = emailFacade.find(notificacion.getIdEmail());

        // Obtener plantilla de notificacion.
        PlantillaFacade plantillaFacade = Injection.lookup(PlantillaFacade.class);
        Plantilla plantilla = plantillaFacade.find(notificacion.getIdPlantilla().getId());

        // Contenito plano.
        String contenido = notificacion.getContenido();
        if (!fa.gs.misc.Assertions.stringNullOrEmpty(contenido)) {
            EmailContent content = new EmailContent();
            content.setDestinatarios(notificaciones.splitDestinatarios(email.getEmail()));
            content.setAsunto(plantilla.getAsunto());
            content.setContentType(EmailContentType.TEXT);
            content.setCuerpo(contenido);
            return content;
        }

        // Contenido de operaciones con facturas.
        return notificaciones.generarContenidoNotificacionEmail(notificacion, false);
    }

    private SmtpAuthentication getSmtpAuthentication() {
        /**
         * Credenciales de autenticacion.
         * ====================================================================
         * En caso de un fallo de autenticación, verificar si esta habilitada la
         * opción de uso externo por aplicaciones inseguras:
         * https://www.google.com/settings/security/lesssecureapps
         */
        Configuraciones configuraciones = Injection.lookup(Configuraciones.class);
        String username = getSmtpParam(configuraciones, ConfiguracionGeneralEnum.NOTIFICADOR_SMTP_USERNAME);
        String password = getSmtpParam(configuraciones, ConfiguracionGeneralEnum.NOTIFICADOR_SMTP_PASSWORD);

        SmtpAuthentication instance = new SmtpAuthentication();
        instance.username = username;
        instance.password = password;
        return instance;
    }

    private Session getSmtpSession(final SmtpAuthentication auth) {
        // Parametros de conexion.
        Configuraciones configuraciones = Injection.lookup(Configuraciones.class);
        Properties prop = new Properties();
        prop.put("mail.smtp.host", getSmtpParam(configuraciones, ConfiguracionGeneralEnum.NOTIFICADOR_SMTP_HOST));
        prop.put("mail.smtp.port", getSmtpParam(configuraciones, ConfiguracionGeneralEnum.NOTIFICADOR_SMTP_PORT));
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");

        // Iniciar sesion.
        Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(auth.username, auth.password);
            }
        });

        return session;
    }

    private String getSmtpParam(Configuraciones configuraciones, ConfiguracionGeneralEnum param) {
        Result<String> resConfig = configuraciones.buscarValorParametro(param);
        String value = resConfig.value();
        return value;
    }

    private static class SmtpAuthentication {

        String username;
        String password;
    }
}
