/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.comercial;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "cliente", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c"),
    @NamedQuery(name = "Cliente.findByIdCliente", query = "SELECT c FROM Cliente c WHERE c.idCliente = :idCliente"),
    @NamedQuery(name = "Cliente.findByIdComercial", query = "SELECT c FROM Cliente c WHERE c.idComercial = :idComercial"),
    @NamedQuery(name = "Cliente.findByIdGrupo", query = "SELECT c FROM Cliente c WHERE c.idGrupo = :idGrupo"),
    @NamedQuery(name = "Cliente.findByIdGrupoImpresion", query = "SELECT c FROM Cliente c WHERE c.idGrupoImpresion = :idGrupoImpresion"),
    @NamedQuery(name = "Cliente.findBySaldo", query = "SELECT c FROM Cliente c WHERE c.saldo = :saldo"),
    @NamedQuery(name = "Cliente.findByPorcentajeRetencion", query = "SELECT c FROM Cliente c WHERE c.porcentajeRetencion = :porcentajeRetencion"),
    @NamedQuery(name = "Cliente.findByEstado", query = "SELECT c FROM Cliente c WHERE c.estado = :estado"),
    @NamedQuery(name = "Cliente.findByNroPlanCuenta", query = "SELECT c FROM Cliente c WHERE c.nroPlanCuenta = :nroPlanCuenta")})
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Column(name = "id_comercial")
    private Integer idComercial;
    @Column(name = "id_grupo")
    private Integer idGrupo;
    @Column(name = "id_grupo_impresion")
    private Integer idGrupoImpresion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "saldo")
    private BigInteger saldo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_retencion")
    private int porcentajeRetencion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private Character estado;
    @Size(max = 2147483647)
    @Column(name = "nro_plan_cuenta")
    private String nroPlanCuenta;

    public Cliente() {
    }

    public Cliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Cliente(Integer idCliente, BigInteger saldo, int porcentajeRetencion, Character estado) {
        this.idCliente = idCliente;
        this.saldo = saldo;
        this.porcentajeRetencion = porcentajeRetencion;
        this.estado = estado;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdComercial() {
        return idComercial;
    }

    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }

    public Integer getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Integer idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Integer getIdGrupoImpresion() {
        return idGrupoImpresion;
    }

    public void setIdGrupoImpresion(Integer idGrupoImpresion) {
        this.idGrupoImpresion = idGrupoImpresion;
    }

    public BigInteger getSaldo() {
        return saldo;
    }

    public void setSaldo(BigInteger saldo) {
        this.saldo = saldo;
    }

    public int getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(int porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getNroPlanCuenta() {
        return nroPlanCuenta;
    }

    public void setNroPlanCuenta(String nroPlanCuenta) {
        this.nroPlanCuenta = nroPlanCuenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCliente != null ? idCliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.idCliente == null && other.idCliente != null) || (this.idCliente != null && !this.idCliente.equals(other.idCliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.comercial.Cliente[ idCliente=" + idCliente + " ]";
    }

}
