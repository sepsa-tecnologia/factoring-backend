/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import fa.gs.misc.errors.AppErrorException;
import fa.gs.result.simple.Result;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.batch.api.Batchlet;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Batchlets;
import py.com.sepsa.genisys.ejb.logic.impl.Configuraciones;

/**
 *
 * @author Fabio A. González Sosa
 */
public abstract class AbstractBatchlet implements Batchlet {

    protected Batchlets batchlets;

    private final AtomicBoolean toStop;

    private Throwable lastFailure;

    public AbstractBatchlet() {
        this.batchlets = Batchlets.inject();
        this.toStop = new AtomicBoolean(false);
    }

    protected abstract String getName();

    protected abstract long getPeriod();

    protected abstract boolean continueOnFailure();

    protected abstract void work() throws Throwable;

    protected long readPeriodConfig(ConfiguracionGeneralEnum config, long fallback) {
        try {
            Configuraciones configuraciones = Injection.lookup(Configuraciones.class);
            Result<String> resConfig = configuraciones.buscarValorParametro(config);
            resConfig.raise();
            String value = resConfig.value();
            return Long.valueOf(value);
        } catch (AppErrorException | NumberFormatException thr) {
            return fallback;
        }
    }

    @Override
    public String process() throws Exception {
        while (true) {
            // Control.
            if (toStop.get()) {
                return "STOP";
            }

            // Realizar trabajo.
            long a = System.currentTimeMillis();
            try {
                batchlets.stamp(getName());
                work();
            } catch (Throwable thr) {
                lastFailure = thr;
                if (thr instanceof InterruptedException) {
                    Thread.currentThread().interrupt();
                    return "FAILED";
                }
            } finally {
                long b = System.currentTimeMillis();
                batchlets.ellapsed(getName(), b - a);
            }

            // Control.
            if (lastFailure != null && !continueOnFailure()) {
                return "FAILED";
            }

            // Esperar siguiente iteracion.
            try {
                long period = getPeriod();
                batchlets.sleep(getName(), period);
                Thread.sleep(period);
            } catch (InterruptedException ie) {
                return "STOP";
            }
        }
    }

    @Override
    public void stop() throws Exception {
        toStop.set(true);
    }

}
