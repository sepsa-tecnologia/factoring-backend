package py.com.sepsa.genisys.ejb.facades.info;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 * Fachada de la entidad persona
 *
 * @author Daniel F. Escauriza Arza
 */
@Stateless(name = "PersonaFacade", mappedName = "PersonaFacade")
@LocalBean
public class PersonaFacade extends AbstractFacade<Persona> {

    /**
     * Constructor de PersonaFacade
     */
    public PersonaFacade() {
        super(Persona.class);
    }

}
