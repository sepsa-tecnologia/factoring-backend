/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.info;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
@Entity
@Table(name = "persona", schema = "info")
@XmlRootElement
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "id_tipo_persona")
    @Basic(optional = false)
    private Integer idTipoPersona;

    @Column(name = "id_direccion")
    private Integer idDireccion;

    @Column(name = "estado")
    @Basic(optional = false)
    @NotNull
    private Character estado;

    @Size(max = 2147483647)
    @Column(name = "ruc")
    private String ruc;

    @Column(name = "dv_ruc")
    private BigDecimal dvRuc;

    @Transient
    private String razonSocial;

    @Transient
    private String pjRazonSocial;

    @Transient
    private String pjNombreFantasia;

    @Transient
    private String pfNombres;

    @Transient
    private String pfApellidos;

    @Transient
    private String pfCi;

    public Persona() {
        ;
    }

    public Persona(Integer id) {
        this.id = id;
    }

    public Persona(Integer id, Character estado) {
        this.id = id;
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return razonSocial;
    }

}
