/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.misc.Assertions;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.misc.text.Text;
import fa.gs.utils.misc.xml.Xml;
import fa.gs.utils.misc.xml.XmlPrefixInfoList;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import javax.xml.xpath.XPath;
import lombok.Data;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import py.com.sepsa.genisys.ejb.utils.Fechas;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class RespuestaConsultaFacturasLiquidables implements Respuesta {

    String codigo;
    String mensaje;
    Collection<RespuestaConsultaFacturasLiquidables.FacturaALiquidar> facturas;

    public static RespuestaConsultaFacturasLiquidables parse(String xmlSoap) throws Throwable {
        // Eliminar el namespace que no utiliza ningun prefijo.
        xmlSoap = Text.replaceAll(xmlSoap, "xmlns=\"http://tempuri.org/\"", "");

        // Cargar XML de respuesta SOAP.
        Document xml = Xml.parse(xmlSoap);

        // Obtener prefijos y uris.
        XmlPrefixInfoList prefixes = Xml.readPrefixes(xml);

        // Preparar consultas XPath.
        XPath xpath = Xml.xpath(prefixes);
        String xCodigo = ".//CodigoError";
        String xMensaje = ".//Mensaje";
        String xInfoFacturaALiquidar = ".//InfoFacturaALiquidar";
        String xDNroOperacion = ".//NroOperacion";
        String xDIdSolicitud = ".//IdSolicitud";
        String xDNroFactura = ".//NroFactura";
        String xDFechaVto = ".//FechaVto";
        String xDMonto = ".//Monto";

        // Obtener valor.
        String codigo = Xml.readNodeTexContent(xpath, xCodigo, xml);
        String mensaje = Xml.readNodeTexContent(xpath, xMensaje, xml);
        NodeList facturas0 = Xml.readNodes(xpath, xInfoFacturaALiquidar, xml);
        Collection<RespuestaConsultaFacturasLiquidables.FacturaALiquidar> facturas = Lists.empty();
        for (int i = 0; i < facturas0.getLength(); i++) {
            Node detalle0 = facturas0.item(i);
            FacturaALiquidar factura = new FacturaALiquidar();
            factura.nroOperacion = Xml.readNodeTexContent(xpath, xDNroOperacion, detalle0);
            factura.idSolicitud = Xml.readNodeTexContent(xpath, xDIdSolicitud, detalle0);
            factura.nroFactura = Xml.readNodeTexContent(xpath, xDNroFactura, detalle0);
            factura.monto = Numeric.adaptAsBigDecimal(Xml.readNodeTexContent(xpath, xDMonto, detalle0));
            factura.fechaVto = Fechas.parse(Xml.readNodeTexContent(xpath, xDFechaVto, detalle0), "yyyy-MM-dd'T'HH:mm:ss");
            facturas.add(factura);
        }

        // Procesar valores.
        RespuestaConsultaFacturasLiquidables respuesta = new RespuestaConsultaFacturasLiquidables();
        respuesta.codigo = codigo;
        respuesta.mensaje = mensaje;
        respuesta.facturas = facturas;
        return respuesta;
    }

    public static RespuestaConsultaFacturasLiquidables instance(String codigo, String mensaje) {
        RespuestaConsultaFacturasLiquidables instance = new RespuestaConsultaFacturasLiquidables();
        instance.codigo = codigo;
        instance.mensaje = mensaje;
        return instance;
    }

    @Override
    public boolean isOk() {
        return Assertions.stringNullOrEmpty(codigo) == false && Objects.equals(codigo, "0");
    }

    @Data
    public static class FacturaALiquidar {

        private String nroOperacion;
        private String idSolicitud;
        private String nroFactura;
        private Date fechaVto;
        private BigDecimal monto;
    }

}
