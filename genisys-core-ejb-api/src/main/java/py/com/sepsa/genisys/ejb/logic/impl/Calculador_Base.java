/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.utils.misc.numeric.Numeric;
import java.math.BigDecimal;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.Convenio;
import py.com.sepsa.genisys.ejb.enums.TipoCargoEnum;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraInput;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public abstract class Calculador_Base implements Calculador {

    protected String validarDatosBase() {
        CalculadoraInput input = getCalculadoraInput();

        // Proveedor.
        if (input.getProveedor() == null) {
            return "Se debe seleccionar un proveedor.";
        }

        // Comprador.
        if (input.getComprador() == null) {
            return "Se debe seleccionar un comprador.";
        }

        // Entidad Financiera.
        if (input.getEntidadFinanciera() == null) {
            return "Se debe seleccionar una entidad financiera.";
        }

        // Monto de Factura.
        if (input.getMontoFactura() == null) {
            return "Se debe ingresar un monto de factura para realizar el cálculo.";
        }

        // Monto de Factura.
        if (Numeric.menorIgual(input.getMontoFactura(), Numeric.CERO)) {
            return "El monto de factura no puede ser menor o igual a cero.";
        }

        // Fecha de Pago.
        if (input.getFechaPagoFactura() == null) {
            return "La fecha de pago de factura no puede ser vacía.";
        }

        // Fecha de Pago de hoy en adelante.
        int diff = Fechas.diasDiferencia(input.getFechaPagoFactura());
        if (diff < 0) {
            return "La fecha de pago para la factura no puede ser anterior al día de hoy.";
        }

        // Fecha de Descuento.
        if (input.getFechaAdelantoFactura() == null) {
            return "La fecha de descuento de factura no puede ser vacía.";
        }

        // Fecha de Descuento de hoy en adelante.
        diff = Fechas.diasDiferencia(input.getFechaAdelantoFactura());
        if (diff < 0) {
            return "La fecha de pago para la factura no puede ser anterior al día de hoy.";
        }

        // Fecha de Descuento no puede ser mayor que fecha de pago.
        diff = Fechas.diasDiferencia(input.getFechaAdelantoFactura(), input.getFechaPagoFactura());
        if (diff < 0) {
            return "La fecha de descuento no puede ser posterior a la fecha de pago de la factura.";
        }

        return null;
    }

    protected BigDecimal mulDiv100(BigDecimal monto, Convenio convenio, TipoCargoEnum tipoCargoEnum) {
        Convenios bConvenios = Injection.lookup(Convenios.class);
        BigDecimal cargo = bConvenios.obtenerConfigCargo(convenio, tipoCargoEnum, Numeric.CERO);
        return mulDiv100(monto, cargo);
    }

    protected BigDecimal mulDiv100(BigDecimal a, BigDecimal b) {
        return Numeric.div(Numeric.mul(a, b), Numeric.CIEN);
    }

}
