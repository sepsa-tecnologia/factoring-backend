/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
@Entity
@Table(name = "factura_seguimiento_liquidacion", schema = "factoring")
@Data
public class FacturaSeguimientoLiquidacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_factura")
    private Integer idFactura;

    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private String estado;

    @Basic(optional = true)
    @Column(name = "cod_liq_comprador")
    private String codLiqComprador;

    @Basic(optional = true)
    @Column(name = "cod_liq_proveedor")
    private String codLiqProveedor;

    @Basic(optional = true)
    @Column(name = "cod_liq_entidad")
    private String codLiqEntidad;

    @Basic(optional = true)
    @Column(name = "cod_liq_sepsa")
    private String codLiqSepsa;

    @Override
    public String toString() {
        return id.toString();
    }

}
