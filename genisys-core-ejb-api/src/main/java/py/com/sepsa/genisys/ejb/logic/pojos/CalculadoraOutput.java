/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.pojos;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
public interface CalculadoraOutput extends Serializable {

    Integer getDiasAdelantados();

    void setDiasAdelantados(Integer value);

    BigDecimal getPorcentajeTotalComision();

    void setPorcentajeTotalComision(BigDecimal value);

    BigDecimal getMontoTotalComision();

    void setMontoTotalComision(BigDecimal value);

    BigDecimal getMontoTotalDesembolso();

    void setMontoTotalDesembolso(BigDecimal value);

    Object getExtras();

    void setExtras(Object value);

    @Data
    public static class Impl implements CalculadoraOutput {

        /**
         * Dias de adelanto.
         */
        private Integer diasAdelantados;

        /**
         * Porcentaje total de descuento.
         */
        private BigDecimal porcentajeTotalComision;

        /**
         * Monto descontado.
         */
        private BigDecimal montoTotalComision;

        /**
         * Monto final a desembolsar.
         */
        private BigDecimal montoTotalDesembolso;

        /**
         * Datos adicionales de calculo.
         */
        private Object extras;
    }

}
