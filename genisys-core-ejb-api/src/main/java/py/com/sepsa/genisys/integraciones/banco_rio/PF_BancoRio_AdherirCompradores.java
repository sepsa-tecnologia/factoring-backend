/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.jboss.ejb3.annotation.TransactionTimeout;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.utils.Constantes;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.TxWrapperCore;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.CompradorInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_AdherirCompradores", mappedName = "PF_BancoRio_AdherirCompradores")
@LocalBean
public class PF_BancoRio_AdherirCompradores implements Serializable {

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Notificaciones notificaciones;

    @EJB
    private PF_BancoRio_AdherirComprador adherirComprador;

    @TransactionTimeout(value = Constantes.TX_TIMEOUT_VALUE)
    public void adherirCompradores() throws Throwable {
        // Consumir servicio.
        Result<ConsultaCompradoresOutput> resCompradores = bancoRio.consultarCompradores();
        if (resCompradores.isFailure()) {
            throw Errors.illegalState(resCompradores.failure().cause(), resCompradores.failure().message());
        }

        // Procesar respuesta.
        ConsultaCompradoresOutput output = resCompradores.value();
        if (!output.getRespuesta().isOk()) {
            String msg = bancoRio.resolveErrorMessage(output.getTrack(), output.getRespuesta());
            throw Errors.illegalState(msg);
        }

        // Obtener compradores.
        Collection<CompradorInfo> compradores = output.getRespuesta().getCompradores();
        if (Assertions.isNullOrEmpty(compradores)) {
            return;
        }

        // Procesar compradores.
        for (CompradorInfo comprador : compradores) {
            TxWrapperCore tx = Injection.lookup(TxWrapperCore.class);
            Result<?> result = tx.execute(() -> work0(comprador));
            if (result.isFailure()) {
                notificaciones.notificarSepsa(result.failure().cause(), "Ocurrió un error registrando datos de nuevo comprador razon_social='%s' nro_documento='%s'", comprador.getRazonSocial(), comprador.getNroDocumento());
            }
        }
    }

    private Void work0(CompradorInfo comprador) throws Throwable {
        adherirComprador.work(comprador);
        return null;
    }

}
