/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.notificacion;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoringFactura;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author descauriza
 */
@Stateless(name = "NotificacionEmailFactoringFacturaFacade", mappedName = "NotificacionEmailFactoringFacturaFacade")
@LocalBean
public class NotificacionEmailFactoringFacturaFacade extends AbstractFacade<NotificacionEmailFactoringFactura> {

    public NotificacionEmailFactoringFacturaFacade() {
        super(NotificacionEmailFactoringFactura.class);
    }

}
