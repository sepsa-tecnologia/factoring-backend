/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.enums;

/**
 * Enumeracion para los tipos de valores posibles en la configuracion de un
 * cargo.
 */
public enum TipoTelefonoEnum {
    LINEA_BAJA("LÍNEA BAJA"),
    MOVIL("MÓVIL");
    private final String descripcion;

    TipoTelefonoEnum(String value) {
        this.descripcion = value;
    }

    public String descripcion() {
        return descripcion;
    }

}
