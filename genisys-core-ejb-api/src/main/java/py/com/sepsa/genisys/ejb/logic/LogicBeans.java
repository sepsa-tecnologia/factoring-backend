/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import lombok.Data;
import py.com.sepsa.genisys.ejb.logic.impl.Archivos;
import py.com.sepsa.genisys.ejb.logic.impl.Cargos;
import py.com.sepsa.genisys.ejb.logic.impl.Configuraciones;
import py.com.sepsa.genisys.ejb.logic.impl.Convenios;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facades;
import py.com.sepsa.genisys.ejb.logic.impl.LineasCredito;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.TipoArchivos;
import py.com.sepsa.genisys.ejb.logic.impl.TipoCargos;
import py.com.sepsa.genisys.ejb.logic.impl.TipoTasasComision;
import py.com.sepsa.genisys.ejb.logic.impl.TipoValores;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "LogicBeans", mappedName = "LogicBeans")
@LocalBean
@Data
public class LogicBeans implements Serializable {

    @EJB
    private Estados estados;

    @EJB
    private Facades facades;

    @EJB
    private Perfiles perfiles;

    @EJB
    private Archivos archivos;

    @EJB
    private TipoArchivos tipoArchivos;

    @EJB
    private LineasCredito lineasCredito;

    @EJB
    private Configuraciones configuraciones;

    @EJB
    private Cargos cargos;

    @EJB
    private Convenios convenios;

    @EJB
    private TipoCargos tipoCargos;

    @EJB
    private TipoValores tipoValores;

    @EJB
    private TipoTasasComision tipoTasasComision;

}
