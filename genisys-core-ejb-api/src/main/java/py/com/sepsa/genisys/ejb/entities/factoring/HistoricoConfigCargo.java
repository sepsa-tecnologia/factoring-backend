/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "historico_config_cargo", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistoricoConfigCargo.findAll", query = "SELECT h FROM HistoricoConfigCargo h"),
    @NamedQuery(name = "HistoricoConfigCargo.findById", query = "SELECT h FROM HistoricoConfigCargo h WHERE h.id = :id"),
    @NamedQuery(name = "HistoricoConfigCargo.findByIdUsuario", query = "SELECT h FROM HistoricoConfigCargo h WHERE h.idUsuario = :idUsuario"),
    @NamedQuery(name = "HistoricoConfigCargo.findByFechaOperacion", query = "SELECT h FROM HistoricoConfigCargo h WHERE h.fechaOperacion = :fechaOperacion"),
    @NamedQuery(name = "HistoricoConfigCargo.findByValor", query = "SELECT h FROM HistoricoConfigCargo h WHERE h.valor = :valor"),
    @NamedQuery(name = "HistoricoConfigCargo.findByComentario", query = "SELECT h FROM HistoricoConfigCargo h WHERE h.comentario = :comentario"),
    @NamedQuery(name = "HistoricoConfigCargo.findByFechaInicio", query = "SELECT h FROM HistoricoConfigCargo h WHERE h.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "HistoricoConfigCargo.findByFechaFin", query = "SELECT h FROM HistoricoConfigCargo h WHERE h.fechaFin = :fechaFin"),
    @NamedQuery(name = "HistoricoConfigCargo.findByHoraInicio", query = "SELECT h FROM HistoricoConfigCargo h WHERE h.horaInicio = :horaInicio"),
    @NamedQuery(name = "HistoricoConfigCargo.findByHoraFin", query = "SELECT h FROM HistoricoConfigCargo h WHERE h.horaFin = :horaFin"),
    @NamedQuery(name = "HistoricoConfigCargo.findByRenovacionAutomatica", query = "SELECT h FROM HistoricoConfigCargo h WHERE h.renovacionAutomatica = :renovacionAutomatica")})
public class HistoricoConfigCargo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_operacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaOperacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor")
    private BigDecimal valor;
    @Size(max = 2147483647)
    @Column(name = "comentario")
    private String comentario;
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Column(name = "hora_inicio")
    @Temporal(TemporalType.TIME)
    private Date horaInicio;
    @Column(name = "hora_fin")
    @Temporal(TemporalType.TIME)
    private Date horaFin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "renovacion_automatica")
    private Character renovacionAutomatica;
    @Column(name = "id_convenio")
    private Integer idConvenio;
    @JoinColumn(name = "id_tipo_cargo", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private TipoCargo idTipoCargo;
    @JoinColumn(name = "id_tipo_valor", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private TipoValor idTipoValor;

    // Campos adicionales.
    @JoinColumn(name = "id_convenio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Convenio convenio;

    public HistoricoConfigCargo() {
    }

    public HistoricoConfigCargo(Integer id) {
        this.id = id;
    }

    public HistoricoConfigCargo(Integer id, Date fechaOperacion, BigDecimal valor, Character renovacionAutomatica) {
        this.id = id;
        this.fechaOperacion = fechaOperacion;
        this.valor = valor;
        this.renovacionAutomatica = renovacionAutomatica;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(Date fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Date horaFin) {
        this.horaFin = horaFin;
    }

    public Character getRenovacionAutomatica() {
        return renovacionAutomatica;
    }

    public void setRenovacionAutomatica(Character renovacionAutomatica) {
        this.renovacionAutomatica = renovacionAutomatica;
    }

    public Integer getIdConvenio() {
        return idConvenio;
    }

    public void setIdConvenio(Integer idConvenio) {
        this.idConvenio = idConvenio;
    }

    public Convenio getConvenio() {
        return convenio;
    }

    public void setIdConvenio(Convenio Convenio) {
        this.convenio = Convenio;
    }

    public TipoCargo getIdTipoCargo() {
        return idTipoCargo;
    }

    public void setIdTipoCargo(TipoCargo idTipoCargo) {
        this.idTipoCargo = idTipoCargo;
    }

    public TipoValor getIdTipoValor() {
        return idTipoValor;
    }

    public void setIdTipoValor(TipoValor idTipoValor) {
        this.idTipoValor = idTipoValor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof HistoricoConfigCargo)) {
            return false;
        }
        HistoricoConfigCargo other = (HistoricoConfigCargo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.HistoricoConfigCargo[ id=" + id + " ]";
    }

}
