/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.numeric.Numeric;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.sepsa.genisys.ejb.entities.factoring.Convenio;
import py.com.sepsa.genisys.ejb.enums.TipoCargoEnum;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraInput;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraOutput;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Calculador_Sepsa extends Calculador_Base {

    private CalculadoraInput calculadoraInput;
    private CalculadoraOutput calculadoraOutput;

    public Calculador_Sepsa() {
        this.calculadoraOutput = null;
    }

    @Override
    public String validar() {
        // Proveedor.
        if (calculadoraInput.getProveedor() == null) {
            return "Debe seleccionar un proveedor.";
        }

        // Comprador.
        if (calculadoraInput.getComprador() == null) {
            return "Debe seleccionar un comprador.";
        }

        // Entidad Financiera.
        if (calculadoraInput.getEntidadFinanciera() == null) {
            return "Debe seleccionar una entidad financiera.";
        }

        // Monto de Factura.
        if (calculadoraInput.getMontoFactura() == null) {
            return "Debe ingresar un monto de factura para realizar el cálculo.";
        }

        // Monto de Factura.
        if (Numeric.menorIgual(calculadoraInput.getMontoFactura(), Numeric.CERO)) {
            return "El monto de factura no puede ser menor o igual a cero.";
        }

        // Fecha de Pago.
        if (calculadoraInput.getFechaPagoFactura() == null) {
            return "La fecha de pago de factura no puede ser vacía.";
        }

        // Fecha de Pago de hoy en adelante.
        int diff = Fechas.diasDiferencia(calculadoraInput.getFechaPagoFactura());
        if (diff < 0) {
            return "La fecha de pago para la factura no puede ser anterior al día de hoy.";
        }

        // Fecha de Descuento.
        if (calculadoraInput.getFechaAdelantoFactura() == null) {
            return "La fecha de descuento de factura no puede ser vacía.";
        }

        // Fecha de Descuento de hoy en adelante.
        diff = Fechas.diasDiferencia(calculadoraInput.getFechaAdelantoFactura());
        if (diff < 0) {
            return "La fecha de pago para la factura no puede ser anterior al día de hoy.";
        }

        // Fecha de Descuento no puede ser mayor que fecha de pago.
        diff = Fechas.diasDiferencia(calculadoraInput.getFechaAdelantoFactura(), calculadoraInput.getFechaPagoFactura());
        if (diff < 0) {
            return "La fecha de descuento no puede ser posterior a la fecha de pago de la factura";
        }

        // Debe existir un único convenio entre los participantes.
        List<Convenio> convenios = obtenerConvenios(calculadoraInput);
        if (convenios.isEmpty()) {
            return "No existe un convenio válido para calcular los cargos a aplicar.";
        } else if (convenios.size() > 1) {
            return "No existe un único convenio para calcular los cargos a aplicar.";
        }

        return null;
    }

    /**
     * Obtiene los convenios disponibles entre los participantes indicados en la
     * vista.
     *
     * @return Convenios disponibles.
     */
    private List<Convenio> obtenerConvenios(CalculadoraInput input) {
        Convenios bConvenios = Injection.lookup(Convenios.class);
        Integer idComprador = input.getComprador().getId();
        Integer idProveedor = input.getProveedor().getId();
        Integer idEntidadFinanciera = input.getEntidadFinanciera().getId();
        Result<Collection<Convenio>> resConvenios = bConvenios.obtenerConvenios(idComprador, idProveedor, idEntidadFinanciera);
        List<Convenio> convenios = new LinkedList<>();
        convenios.addAll(resConvenios.value(Collections.EMPTY_LIST));
        return convenios;
    }

    /**
     * Obtiene los convenios disponibles entre los participantes indicados en la
     * vista.
     *
     * @return Convenios disponibles.
     */
    private List<Convenio> obtenerConvenios() {
        Convenios bConvenios = Injection.lookup(Convenios.class);
        Integer idComprador = calculadoraInput.getComprador().getId();
        Integer idProveedor = calculadoraInput.getProveedor().getId();
        Integer idEntidadFinanciera = calculadoraInput.getEntidadFinanciera().getId();
        Result<Collection<Convenio>> resConvenios = bConvenios.obtenerConvenios(idComprador, idProveedor, idEntidadFinanciera);
        List<Convenio> convenios = new LinkedList<>();
        convenios.addAll(resConvenios.value(Collections.EMPTY_LIST));
        return convenios;
    }

    @Override
    public void calcular() throws Throwable {
        // Obtener convenio.
        Convenio convenio = Lists.first(obtenerConvenios());

        // Obtener version de cargos en convenio.
        Convenios bConvenios = Injection.lookup(Convenios.class);
        String version = bConvenios.obtenerConfigCargo(convenio.getId(), TipoCargoEnum.VERSION_CARGOS, "1");
        if (version.equals("2")) {
            calcular_v2(convenio);
        } else {
            throw Errors.illegalState("Versión de cargos '%s' para convenio no soportada.", version);
        }
    }

    private void calcular_v2(Convenio convenio) throws Throwable {
        // Inyectar beans.
        Convenios bConvenios = Injection.lookup(Convenios.class);

        // Calcular dias de adelanto.
        BigDecimal diasAdelantados = Numeric.wrap(Fechas.diasDiferencia(calculadoraInput.getFechaAdelantoFactura(), calculadoraInput.getFechaPagoFactura()));

        // Calcular porcentaje de impuesto sobre comision.
        BigDecimal porcentajeImpuestoComision = bConvenios.obtenerConfigCargo(convenio, TipoCargoEnum.PORCENTAJE_IMPUESTO_COMISION, Numeric.CERO);

        // Calcular comision de sepsa e impuesto.
        BigDecimal porcentajeComisionSepsa = bConvenios.obtenerConfigCargo(convenio, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_SEPSA, Numeric.CERO);
        BigDecimal porcentajeTotalComisionSepsa = Numeric.mul(porcentajeComisionSepsa, diasAdelantados);
        BigDecimal montoImponibleComisionSepsa = mulDiv100(calculadoraInput.getMontoFactura(), porcentajeTotalComisionSepsa);
        BigDecimal montoImpuestoComisionSepsa = mulDiv100(montoImponibleComisionSepsa, porcentajeImpuestoComision);
        BigDecimal montoTotalComisionSepsa = Numeric.sum(montoImponibleComisionSepsa, montoImpuestoComisionSepsa);

        // Calcular comision de comprador e impuesto.
        BigDecimal porcentajeComisionComprador = bConvenios.obtenerConfigCargo(convenio, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_COMPRADOR, Numeric.CERO);
        BigDecimal porcentajeTotalComisionComprador = Numeric.mul(porcentajeComisionComprador, diasAdelantados);
        BigDecimal montoImponibleComisionComprador = mulDiv100(calculadoraInput.getMontoFactura(), porcentajeTotalComisionComprador);
        BigDecimal montoImpuestoComisionComprador = mulDiv100(montoImponibleComisionComprador, porcentajeImpuestoComision);
        BigDecimal montoTotalComisionComprador = Numeric.sum(montoImponibleComisionComprador, montoImpuestoComisionComprador);

        // Calcular comision de entidad financiera e impuesto.
        BigDecimal porcentajeComisionEntidadFinanciera = bConvenios.obtenerConfigCargo(convenio, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_ENTIDAD_FINANCIERA, Numeric.CERO);
        BigDecimal porcentajeTotalComisionEntidadFinanciera = Numeric.mul(porcentajeComisionEntidadFinanciera, diasAdelantados);
        BigDecimal montoImponibleComisionEntidadFinanciera = mulDiv100(calculadoraInput.getMontoFactura(), porcentajeTotalComisionEntidadFinanciera);
        BigDecimal montoImpuestoComisionEntidadFinanciera = mulDiv100(montoImponibleComisionEntidadFinanciera, porcentajeImpuestoComision);
        BigDecimal montoTotalComisionEntidadFinanciera = Numeric.sum(montoImponibleComisionEntidadFinanciera, montoImpuestoComisionEntidadFinanciera);

        // Calcular porcentaje diario y total de comision imponible.
        BigDecimal porcentajeInteresDiarioComision = Numeric.sum(porcentajeComisionSepsa, porcentajeComisionComprador, porcentajeComisionEntidadFinanciera);
        BigDecimal porcentajeTotalInteresDiarioComision = Numeric.sum(porcentajeTotalComisionSepsa, porcentajeTotalComisionComprador, porcentajeTotalComisionEntidadFinanciera);

        // Calcular monto total imponible de comision e impuesto del mismo, si hubiere.
        BigDecimal montoImponibleComision = Numeric.sum(montoImponibleComisionSepsa, montoImponibleComisionComprador, montoImponibleComisionEntidadFinanciera);
        BigDecimal montoImpuestoComision = Numeric.sum(montoImpuestoComisionSepsa, montoImpuestoComisionComprador, montoImpuestoComisionEntidadFinanciera);
        BigDecimal montoTotalComision = Numeric.sum(montoTotalComisionSepsa, montoTotalComisionComprador, montoTotalComisionEntidadFinanciera);

        // Calcular gastos operativos e impuesto del mismo, si hubiere.
        BigDecimal montoImponibleGastosOperativos = mulDiv100(calculadoraInput.getMontoFactura(), convenio, TipoCargoEnum.PORCENTAJE_IMPONIBLE_GASTOS_OPERATIVOS);
        BigDecimal montoImpuestoGastosOperativos = mulDiv100(montoImponibleGastosOperativos, convenio, TipoCargoEnum.PORCENTAJE_IMPUESTO_GASTOS_OPERATIVOS);
        BigDecimal montoTotalGastosOperativos = Numeric.sum(montoImponibleGastosOperativos, montoImpuestoGastosOperativos);

        // Calcular monto total a desembolsar.
        BigDecimal montoTotalDescuento = Numeric.sum(montoTotalComision, montoTotalGastosOperativos);
        BigDecimal montoTotalDesembolso = Numeric.sub(calculadoraInput.getMontoFactura(), montoTotalDescuento);

        // Detalles extras.
        Calculador_Sepsa.Extras extras = new Calculador_Sepsa.Extras();
        extras.setConvenio(convenio);
        extras.setPorcentajeTotalComisionSepsa(porcentajeTotalComisionSepsa);
        extras.setMontoTotalComisionSepsa(Numeric.round(montoTotalComisionSepsa));
        extras.setPorcentajeTotalComisionComprador(porcentajeTotalComisionComprador);
        extras.setMontoTotalComisionComprador(Numeric.round(montoTotalComisionComprador));
        extras.setPorcentajeTotalComisionEntidadFinanciera(porcentajeTotalComisionEntidadFinanciera);
        extras.setMontoTotalComisionEntidadFinanciera(Numeric.round(montoTotalComisionEntidadFinanciera));
        extras.setMontoImponibleComision(Numeric.round(montoImponibleComision));
        extras.setMontoImpuestoComision(Numeric.round(montoImpuestoComision));
        extras.setMontoTotalComision(Numeric.round(montoTotalComision));
        extras.setMontoImponibleGastosOperativos(Numeric.round(montoImponibleGastosOperativos));
        extras.setMontoImpuestoGastosOperativos(Numeric.round(montoImpuestoGastosOperativos));
        extras.setMontoTotalGastosOperativos(Numeric.round(montoTotalGastosOperativos));

        // Adaptar datos de salida.
        this.calculadoraOutput = new CalculadoraOutput.Impl();
        this.calculadoraOutput.setDiasAdelantados(diasAdelantados.intValue());
        this.calculadoraOutput.setPorcentajeTotalComision(porcentajeTotalInteresDiarioComision);
        this.calculadoraOutput.setMontoTotalComision(Numeric.round(montoTotalDescuento));
        this.calculadoraOutput.setMontoTotalDesembolso(Numeric.round(montoTotalDesembolso));
        this.calculadoraOutput.setExtras(extras);
    }

    @Data
    public static class Extras implements Serializable {

        private Convenio convenio;
        private BigDecimal porcentajeTotalComisionSepsa;
        private BigDecimal montoTotalComisionSepsa;
        private BigDecimal porcentajeTotalComisionComprador;
        private BigDecimal montoTotalComisionComprador;
        private BigDecimal porcentajeTotalComisionEntidadFinanciera;
        private BigDecimal montoTotalComisionEntidadFinanciera;
        private BigDecimal montoImponibleComision;
        private BigDecimal montoImpuestoComision;
        private BigDecimal montoTotalComision;
        private BigDecimal montoImponibleGastosOperativos;
        private BigDecimal montoImpuestoGastosOperativos;
        private BigDecimal montoTotalGastosOperativos;
    }
}
