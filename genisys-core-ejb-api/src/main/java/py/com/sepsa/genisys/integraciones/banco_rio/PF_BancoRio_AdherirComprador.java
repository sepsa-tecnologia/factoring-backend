/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioBanco;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioPersona;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoDocumento;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.PersonaRol;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.CompradorInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_AdherirComprador", mappedName = "PF_BancoRio_AdherirComprador")
@LocalBean
public class PF_BancoRio_AdherirComprador implements Serializable {

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Personas personas;

    @EJB
    private Perfiles perfiles;

    public void work(CompradorInfo comprador) throws Throwable {
        // Perfil comprador.
        Perfil perfil = perfiles.obtenerPerfil(PerfilEnum.COMPRADOR);
        Integer idPerfil = perfil.getId();

        // Entidad financiera.
        Integer idEntidadFinanciera = bancoRio.obtenerIdEntidadBancoRio();

        // Eliminar ceros a la izquierda de numero de documento.
        String nroDocumento = comprador.getNroDocumento();
        while (nroDocumento.startsWith("0")) {
            nroDocumento = nroDocumento.substring(1);
        }

        // Obtener tipo de documento.
        Result<BancoRioTipoDocumento> resTipoDocumento = bancoRio.obtenerTipoDocumentoPorSigla(comprador.getTipoDocumento());
        resTipoDocumento.raise();
        BancoRioTipoDocumento tipoDocumento = resTipoDocumento.value();

        // Verificar si ya existe persona.
        Result<Persona> resPersona = personas.obtenerPorRuc(nroDocumento);
        resPersona.raise();
        Persona persona = resPersona.value();
        if (persona == null) {
            agregarPersona(comprador, idPerfil, idEntidadFinanciera, nroDocumento, tipoDocumento);
        } else {
            editarPersona(comprador, persona, idPerfil, idEntidadFinanciera, nroDocumento, tipoDocumento);
        }

    }

    private void agregarPersona(CompradorInfo comprador, Integer idPerfil, Integer idEntidadFinanciera, String nroDocumento, BancoRioTipoDocumento tipoDocumento) throws Throwable {
        // Registrar persona nueva.
        Result<Persona> resPersona = personas.registrarPersonaJuridica(comprador.getRazonSocial(), comprador.getRazonSocial(), nroDocumento);
        resPersona.raise();
        Persona persona = resPersona.value();
        Integer idPersona = persona.getId();

        // Agregar rol de comprador.
        Result<PersonaRol> resAgregarRol = personas.agregarRol(idPersona, PerfilEnum.COMPRADOR);
        resAgregarRol.raise();

        // Actualizar persona para entidad.
        Result<BancoRioPersona> resBancoRioPersona = bancoRio.registrarComprador(idPersona, idPerfil, idEntidadFinanciera, comprador.getCodCliente(), tipoDocumento.getId());
        resBancoRioPersona.raise();

        // Actualizar nro de cuenta.
        // NOTE: VERIFICAR CON ENTIDAD CUAL BANCO ES EL CORRECTO.
        Result<BancoRioBanco> resBancoRio = bancoRio.obtenerBancoPorCodigo("BISAPYPE");
        resBancoRio.raise();
        BancoRioBanco bancoRio0 = resBancoRio.value();
        bancoRio.actualizarNroCuenta(idPersona, idPerfil, bancoRio0.getId(), comprador.getNroCuentaCredito());
    }

    private void editarPersona(CompradorInfo comprador, Persona persona, Integer idPerfil, Integer idEntidadFinanciera, String nroDocumento, BancoRioTipoDocumento tipoDocumento) throws Throwable {
        Integer idPersona = persona.getId();

        Result<BancoRioPersona> resBancoRioPersona = bancoRio.obtenerPersona(persona.getId(), idPerfil);
        resBancoRioPersona.raise();

        // Actualizar nro de cliente.
        bancoRio.actualizarCodCliente(idPersona, idPerfil, comprador.getCodCliente());

        // Actualizar tipo de documento.
        bancoRio.actualizarTipoDocumento(idPersona, idPerfil, tipoDocumento.getId());

        // Actualizar nro de cuenta.
        // NOTE: VERIFICAR CON ENTIDAD CUAL BANCO ES EL CORRECTO.
        Result<BancoRioBanco> resBancoRio = bancoRio.obtenerBancoPorCodigo("BISAPYPE");
        resBancoRio.raise();
        BancoRioBanco bancoRio0 = resBancoRio.value();
        bancoRio.actualizarNroCuenta(idPersona, idPerfil, bancoRio0.getId(), comprador.getNroCuentaCredito());
    }
}
