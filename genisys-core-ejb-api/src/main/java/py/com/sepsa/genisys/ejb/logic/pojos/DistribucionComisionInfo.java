/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.pojos;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class DistribucionComisionInfo implements Serializable {

    private Integer idConvenio;
    private BigDecimal porcentajeComisionComprador;
    private BigDecimal montoComisionComprador;
    private BigDecimal porcentajeComisionSepsa;
    private BigDecimal montoComisionSepsa;
    private BigDecimal porcentajeComisionEntidad;
    private BigDecimal montoComisionEntidad;
}
