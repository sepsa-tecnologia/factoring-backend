/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import java.util.Collection;
import java.util.Objects;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoCargo;
import py.com.sepsa.genisys.ejb.enums.TipoCargoEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.TipoCargoFacade;
import py.com.sepsa.genisys.ejb.logic.LogicBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "TipoCargos", mappedName = "TipoCargos")
@LocalBean
public class TipoCargos extends LogicBean {

    @EJB
    private TipoCargoFacade facade;

    /**
     * Obtiene una enumeracion de tipo de cargo en base a una entidad de tipo de
     * cargo.
     *
     * @param tipoCargo Entidad tipo de cargo.
     * @return Enumeracion de tipo de cargo.
     */
    public TipoCargoEnum obtenerTipoCargo(TipoCargo tipoCargo) {
        for (TipoCargoEnum tipoCargoEnum : TipoCargoEnum.values()) {
            if (Objects.equals(tipoCargoEnum.descripcion(), tipoCargo.getDescripcion())) {
                return tipoCargoEnum;
            }
        }
        return null;
    }

    /**
     * Obtiene un tipo de cargo en base a una enumeracion de tipo de cargo.
     *
     * @param tipoCargo Enumeracion de tipo de cargo.
     * @return Entidad tipo de cargo.
     */
    public TipoCargo obtenerTipoCargo(TipoCargoEnum tipoCargo) {
        return facade.findByDescripcion(tipoCargo.descripcion());
    }

    /**
     * Obtiene todos los tipos de cargo disponibles.
     *
     * @return Coleccion de tipos de cargo disponibles.
     */
    public Collection<TipoCargo> obtenerTipoCargos() {
        return facade.findAll();
    }

}
