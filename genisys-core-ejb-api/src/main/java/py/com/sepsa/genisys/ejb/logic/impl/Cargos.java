/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.ConfigCargo;
import py.com.sepsa.genisys.ejb.enums.TipoCargoEnum;
import py.com.sepsa.genisys.ejb.enums.TipoValorEnum;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.Fechas;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Cargos", mappedName = "Cargos")
@LocalBean
public class Cargos extends LogicBean {

    /**
     * Persiste una nueva configuracion de cargo dentro de un convenio dado.
     *
     * @param idConvenio Identificador de convenio.
     * @param valor Valor numero del cargo.
     * @param valorTexto Valor textual del cargo, si hubiere.
     * @param tipoCargo Tipo de cargo.
     * @param tipoValor Tipo de valor.
     * @return Configuracion de cargo.
     */
    public Result<ConfigCargo> registrarConfigCargo(Integer idConvenio, BigDecimal valor, String valorTexto, TipoCargoEnum tipoCargo, TipoValorEnum tipoValor) {
        Result<ConfigCargo> result;

        try {
            // Calculos auxiliares.
            Integer anho = Calendar.getInstance().get(Calendar.YEAR);
            Date fechaInicio = Fechas.buildFecha(anho, 1, 1);
            Date horaInicio = Fechas.buildHora(0, 0, 0);
            Date fechaFin = Fechas.buildFecha(anho, 12, 31);
            Date horaFin = Fechas.buildHora(23, 59, 59);

            // Creacion parcial de una configuracion de cargo.
            ConfigCargo cargo = new ConfigCargo();
            cargo.setIdTipoCargo(beans.getTipoCargos().obtenerTipoCargo(tipoCargo));
            cargo.setIdTipoValor(beans.getTipoValores().obtenerTipoValor(tipoValor));
            cargo.setValor(valor);
            cargo.setValorTexto(valorTexto);
            cargo.setFechaInicio(fechaInicio);
            cargo.setFechaFin(fechaFin);
            cargo.setHoraInicio(horaInicio);
            cargo.setHoraFin(horaFin);
            cargo.setRenovacionAutomatica('S');
            cargo.setIdConvenio(idConvenio);

            // Persistir la configuracion de cargo.
            beans.getFacades().getConfigCargoFacade().create(cargo);
            result = Results.ok().value(cargo).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error creando configuración de cargo para convenio id=%s", idConvenio).build();
        }
        return result;
    }

    /**
     * Obtiene el valor numerico configurado en un cargo.
     *
     * @param configCargo Configuracion de cargo.
     * @param fallback Valor alternativo en caso de que no se pueda utilizar el
     * valor configurado en el cargo.
     * @return Valor numerico del cargo.
     */
    public BigDecimal obtenerValor(ConfigCargo configCargo, BigDecimal fallback) {
        return (configCargo != null) ? configCargo.getValor() : fallback;
    }

    /**
     * Obtiene el valor textual configurado en un cargo.
     *
     * @param configCargo Configuracion de cargo.
     * @param fallback Valor alternativo en caso de que no se pueda utilizar el
     * valor configurado en el cargo.
     * @return Valor textual del cargo.
     */
    public String obtenerValor(ConfigCargo configCargo, String fallback) {
        return (configCargo != null) ? configCargo.getValorTexto() : fallback;
    }

    /**
     * Clase que permite desplegar informacion de cargos en la vista.
     */
    public static class ConfigCargoView {

        //<editor-fold defaultstate="collapsed" desc="Atributos">
        /**
         * Descripción corta, utilizada como label en un formulario.
         */
        private String descripcionCorta;
        /**
         * Descripción larga, utilizada como texto de ayuda en un formulario.
         */
        private String descripcionLarga;
        /**
         * Valor formateao del formulario, utilizado como campo de información.
         */
        private String valor;
        //</editor-fold>

        /**
         * Constructor.
         *
         * @param descripcionCorta Descripcion corta.
         * @param descripcionLarga Descripcion larga.
         * @param valor Valor formateado.
         */
        public ConfigCargoView(String descripcionCorta, String descripcionLarga, String valor) {
            this.descripcionCorta = descripcionCorta;
            this.descripcionLarga = descripcionLarga;
            this.valor = valor;
        }

        //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
        public String getDescripcionCorta() {
            return descripcionCorta;
        }

        public void setDescripcionCorta(String descripcionCorta) {
            this.descripcionCorta = descripcionCorta;
        }

        public String getDescripcionLarga() {
            return descripcionLarga;
        }

        public void setDescripcionLarga(String descripcionLarga) {
            this.descripcionLarga = descripcionLarga;
        }

        public String getValor() {
            return valor;
        }

        public void setValor(String valor) {
            this.valor = valor;
        }
        //</editor-fold>

    }

}
