/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoConvenio;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "TipoConvenioFacade", mappedName = "TipoConvenioFacade")
@LocalBean
public class TipoConvenioFacade extends AbstractFacade<TipoConvenio> {

    /**
     * Constructor.
     */
    public TipoConvenioFacade() {
        super(TipoConvenio.class);
    }

    /**
     * Permite obtener un tipo de convenio en base a su descripcion.
     *
     * @param descripcion Descripcion del tipo de convenio.
     * @return Tipo de convenio que concuerda con la descripcion, null en caso
     * contrario.
     */
    public TipoConvenio findByDescripcion(String descripcion) {
        Query q = getEntityManager().createNamedQuery("TipoConvenio.findByDescripcion");
        q.setParameter("descripcion", descripcion);
        return JpaUtils.getFirst(q);
    }

}
