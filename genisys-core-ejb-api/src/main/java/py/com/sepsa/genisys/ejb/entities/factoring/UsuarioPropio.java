/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
@Entity
@Table(name = "usuario_propio", schema = "factoring")
@NamedQueries({
    @NamedQuery(name = "UsuarioPropio.findByAuthId", query = "SELECT u FROM UsuarioPropio u WHERE u.authId = :authId"),
    @NamedQuery(name = "UsuarioPropio.findByPersonaAlias", query = "SELECT u FROM UsuarioPropio u WHERE u.idPersona = :idPersona and u.alias = :alias")
})
public class UsuarioPropio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "id_usuario", nullable = false)
    @Basic(optional = false)
    private Integer idUsuario;

    @Column(name = "id_persona", nullable = false)
    @Basic(optional = false)
    private Integer idPersona;

    @Column(name = "alias", nullable = false)
    @Basic(optional = false)
    private String alias;

    @Column(name = "auth_id", nullable = false)
    @Basic(optional = false)
    private String authId;

    @Column(name = "auth_secret", nullable = false)
    @Basic(optional = false)
    private String authSecret;

}
