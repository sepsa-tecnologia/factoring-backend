/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import java.io.Serializable;
import javax.ejb.Remote;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoring;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.BancoRioTipoPeticionEnum;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.WsTrackInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Remote
public interface Queues extends Serializable {

    public static Queues inject() {
        String jndi = "java:global/genisys-core-0.0.1/Queues!py.com.sepsa.genisys.ejb.logic.impl.Queues";
        return (Queues) Injection.lookup(jndi);
    }

    void post(NotificacionEmailFactoring cabecera, Integer[] idFacturas);

    void post(BancoRioTipoPeticionEnum tipoPeticion, WsTrackInfo info);

}
