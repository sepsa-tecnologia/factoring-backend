/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "TxWrapperCore", mappedName = "TxWrapperCore")
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class TxWrapperCore implements Serializable {

    public static final AppLogger log = AppLoggerFactory.core();

    @Resource
    private UserTransaction tx;

    /**
     * Ejecuta un bloque de ejecución arbitrario.
     *
     * @param <T> Tipo de resultado primario manejado dentro del bloque de
     * ejecución.
     * @param executable Abstracción del bloque a ejecutar.
     * @return Resultado de la operacion.
     */
    public <T> Result<T> execute(TxWrapperCore.Executable<T> executable) {
        // Control de seguridad.
        if (executable == null) {
            return Results.ko()
                    .cause(Errors.illegalArgument("El codigo de ejecucion no puede ser nulo"))
                    .build();
        }

        try {
            // Iniciar transaccion.
            log.debug("iniciando transaccion");
            tx.setTransactionTimeout((int) Constantes.TX_TIMEOUT_UNIT.toSeconds(Constantes.TX_TIMEOUT_VALUE));
            tx.begin();
            try {
                // Ejectura bloque de codigo.
                log.debug("ejecutando transaccion");
                T result = executable.execute();

                // Confirmar (commitear) cambios realizados.
                log.debug("finalizando transaccion");
                tx.commit();

                return Results.ok()
                        .value(result)
                        .nullable(true)
                        .build();
            } catch (Throwable thr) {
                // Desechar (rollback) cambios realizados.
                log.error("desechando transaccion");
                tx.rollback();

                return Results.ko()
                        .cause(thr)
                        .message("Ocurrió un error durante ejecucion de bloque de código")
                        .build();
            }
        } catch (Throwable thr) {
            // No se pudo iniciar la transaccion.
            log.fatal(thr, "desechando ejecucion");
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error durante ejecucion de transacción")
                    .build();
        }
    }

    public interface Executable<T> {

        T execute() throws Throwable;

    }

}
