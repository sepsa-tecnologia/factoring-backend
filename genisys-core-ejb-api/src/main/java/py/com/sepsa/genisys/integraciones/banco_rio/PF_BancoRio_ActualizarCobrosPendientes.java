/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.jboss.ejb3.annotation.TransactionTimeout;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioPersona;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Constantes;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.TxWrapperCore;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_ActualizarCobrosPendientes", mappedName = "PF_BancoRio_ActualizarCobrosPendientes")
@LocalBean
public class PF_BancoRio_ActualizarCobrosPendientes implements Serializable {

    private AppLogger log;

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Personas personas;

    @EJB
    private Perfiles perfiles;

    @EJB
    private Notificaciones notificaciones;

    @EJB
    private PF_BancoRio_ActualizarCobroPendiente actualizarCobroPendiente;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    @TransactionTimeout(value = Constantes.TX_TIMEOUT_VALUE)
    public void work() throws Throwable {
        // Obtener identificador de usuario para ingestion de datos.
        log.debug("Obteniendo usuario de ingestión ...");
        Integer idUsuarioIngestion = bancoRio.obtenerIdUsuarioIngestion();
        if (idUsuarioIngestion <= 0) {
            throw Errors.illegalState("El usuario para la ingestión no está establecido");
        }

        // Obtener cantidad de dias de tolerancia.
        log.debug("Obteniendo dias de tolerancia ...");
        Integer diasTolerancia = bancoRio.obtenerDiasTolerancia();

        // Perfil comprador.
        Perfil perfil = perfiles.obtenerPerfil(PerfilEnum.COMPRADOR);
        Integer idPerfil = perfil.getId();

        // Obtener compradores disponibles.
        log.debug("Obteniendo compradores activos ...");
        Result<Collection<Persona>> resCompradores = personas.obtenerCompradoresDisponibles();
        Collection<Persona> compradores = resCompradores.value(Lists.empty());
        log.debug("Compradores activos: %s", Lists.size(compradores));
        for (Persona comprador : compradores) {
            TxWrapperCore tx = Injection.lookup(TxWrapperCore.class);
            Result<?> result = tx.execute(() -> work0(idUsuarioIngestion, diasTolerancia, comprador, idPerfil));
            if (result.isFailure()) {
                notificaciones.notificarSepsa(result.failure().cause(), "Ocurrió un error procesando facturas cobradas por entidad para comprador id=%s", comprador.getId());
            }
        }
    }

    private Void work0(Integer idUsuarioIngestion, Integer diasTolerancia, Persona comprador, Integer idPerfil) throws Throwable {
        // Control.
        Result<BancoRioPersona> resPersonaComprador = bancoRio.obtenerPersona(comprador.getId(), idPerfil);
        BancoRioPersona personaComprador = resPersonaComprador.value(null);
        if (personaComprador == null) {
            return null;
        }

        // Procesar facturas a liquidar.
        actualizarCobroPendiente.work(idUsuarioIngestion, diasTolerancia, comprador);
        return null;
    }

}
