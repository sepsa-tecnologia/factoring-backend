/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.misc.Type;
import fa.gs.misc.collections.Maps;
import fa.gs.misc.fechas.Fechas;
import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.jpa.Jpa;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.numeric.Numeric;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.com.sepsa.genisys.ejb.entities.factoring.ConfigCargo;
import py.com.sepsa.genisys.ejb.entities.factoring.Convenio;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.TipoCargoEnum;
import py.com.sepsa.genisys.ejb.logic.pojos.DistribucionComisionInfo;
import py.com.sepsa.genisys.ejb.logic.pojos.UsuarioInfo;
import py.com.sepsa.genisys.ejb.utils.Maps2;
import py.com.sepsa.genisys.ejb.utils.Persistence;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.integraciones.sepsa.ClienteEdiServer;
import py.com.sepsa.genisys.integraciones.sepsa.ClienteEdiServerImpl;
import py.com.sepsa.genisys.integraciones.sepsa.ClienteEdiServerImplDummy;
import py.com.sepsa.genisys.integraciones.sepsa.RegistrarLiquidacionInput;
import py.com.sepsa.genisys.integraciones.sepsa.RegistrarLiquidacionOutput;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Sepsa", mappedName = "Sepsa")
@LocalBean
public class Sepsa implements Serializable {

    @PersistenceContext(unitName = Persistence.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @EJB
    private Configuraciones configuraciones;

    @EJB
    private Convenios convenios;

    @EJB
    private Cargos cargos;

    @EJB
    private Personas personas;

    public Collection<UsuarioInfo> obtenerUsuariosActivos() throws Throwable {
        final Collection<UsuarioInfo> infos = Lists.empty();
        Collection<Map<String, Object>> rows = Jpa.select(buildQueryUsuariosActivos(), em);
        if (!Assertions.isNullOrEmpty(rows)) {
            for (Map<String, Object> row : rows) {
                // Adaptar perfiles.
                String perfiles = Maps.get(row, "perfiles", Type.STRING);
                perfiles = perfiles.replaceAll(";", ",").replaceAll("ROLE_", "").replaceAll("_", " ");

                UsuarioInfo info = new UsuarioInfo();
                info.setRazonSocial(Maps2.string(row, "razon_social"));
                info.setRuc(Maps2.string(row, "ruc"));
                info.setDvRuc(String.valueOf(Numeric.adaptAsInteger(Maps2.get(row, "dv_ruc"))));
                info.setUsuario(Maps2.string(row, "usuario"));
                info.setTxtPerfilAgg(perfiles);
                infos.add(info);
            }
        }
        return infos;
    }

    private String buildQueryUsuariosActivos() {
        StringBuilder builder = new StringBuilder();
        builder.append(" select ");
        builder.append("  coalesce(p0.nombre || ' ' || p0.apellido, p1.razon_social, p2.razon_social) as \"razon_social\", ");
        builder.append("  p.ruc, ");
        builder.append("  p.dv_ruc, ");
        builder.append("  t2.usuario, ");
        builder.append("  t3.perfiles ");
        builder.append(" from info.persona p  ");
        builder.append(" left join lateral (select * from info.persona_fisica pf where pf.id = p.id) as p0 on true ");
        builder.append(" left join lateral (select * from info.persona_juridica pj where pj.id = p.id) as p1 on true ");
        builder.append(" left join lateral (select * from info.entidad_financiera ef left join info.persona_juridica pj2 on pj2.id = ef.id_entidad_financiera where ef.id_entidad_financiera = p.id) as p2 on true ");
        builder.append(" left join lateral (select * from trans.persona_rol pr where pr.estado = 'A' and pr.id_persona = p.id and pr.id_rol = 1) as t0 on true ");
        builder.append(" left join lateral (select * from trans.persona_rol pr where pr.estado = 'A' and pr.id_persona = p.id and pr.id_rol = 2) as t1 on true ");
        builder.append(" left join lateral (select id, usuario from trans.usuario u where u.estado = 'A' and u.id_persona = p.id) as t2 on true ");
        builder.append(" left join lateral (select string_agg(p.descripcion, ';') as \"perfiles\"  from factoring.usuario_perfil up left join factoring.perfil p on p.id = up.id_perfil where up.id_usuario = t2.id) as t3 on true ");
        builder.append(" where ");
        builder.append("   1 = 1 ");
        builder.append("   and t2.usuario is not null ");
        builder.append(" order by 1 ");
        builder.append(" 	; ");
        return builder.toString();
    }

    public ClienteEdiServer obtenerClienteEdiServer() {
        String value = configuraciones.buscarValorParametro("edi.client").value("dummy");
        if (Objects.equals(value, "dummy")) {
            return ClienteEdiServerImplDummy.instance();
        } else if (Objects.equals(value, "prod")) {
            return ClienteEdiServerImpl.prod();
        } else {
            return ClienteEdiServerImpl.dev();
        }
    }

    public DistribucionComisionInfo calcularDistribucionComisiones(FacturaInfo factura, Integer idEntidadFinanciera) throws Throwable {
        // Obtener datos de convenio.
        Integer idConvenio = factura.getConvenioId();
        if (idConvenio == null) {
            // Buscar convenio.
            Result<Collection<Convenio>> resConvenios = convenios.obtenerConvenios(factura.getCompradorId(), factura.getProveedorId(), idEntidadFinanciera);
            Collection<Convenio> convenios0 = resConvenios.value(Lists.empty());
            if (Assertions.isNullOrEmpty(convenios0) || convenios0.size() != 1) {
                throw Errors.illegalState("No se encontraron datos de convenio para liquidación de factura (Entidad id=%s, Comprador id=%s, Factura id=%s)", idEntidadFinanciera, factura.getCompradorId(), factura.getId());
            }
            idConvenio = Lists.first(convenios0).getId();
            if (idConvenio == null) {
                throw Errors.illegalState("No se encontraron datos de convenio para liquidación de factura (Entidad id=%s, Comprador id=%s, Factura id=%s)", idEntidadFinanciera, factura.getCompradorId(), factura.getId());
            }
        }

        // Obtener configuracion de cargo.
        BigDecimal porcentajeParteComisionComprador = obtenerConfigCargoParteComision(idConvenio, TipoCargoEnum.PORCENTAJE_TOTAL_PARTE_COMISION_COMPRADOR);
        if (porcentajeParteComisionComprador == null) {
            throw Errors.illegalState("No se encontró una configuración de porcentaje de distribución de comisión (comprador) para liquidación de factura (Entidad id=%s, Comprador id=%s, Factura id=%s)", idEntidadFinanciera, factura.getCompradorId(), factura.getId());
        }

        // Obtener configuracion de cargo.
        BigDecimal porcentajeParteComisionSepsa = obtenerConfigCargoParteComision(idConvenio, TipoCargoEnum.PORCENTAJE_TOTAL_PARTE_COMISION_SEPSA);
        if (porcentajeParteComisionSepsa == null) {
            throw Errors.illegalState("No se encontró una configuración de porcentaje de distribución de comisión (SEPSA) para liquidación de factura (Entidad id=%s, Comprador id=%s, Factura id=%s)", idEntidadFinanciera, factura.getCompradorId(), factura.getId());
        }

        // Obtener ccnfiguracion de cargo.
        BigDecimal porcentajeParteComisionEntidad = obtenerConfigCargoParteComision(idConvenio, TipoCargoEnum.PORCENTAJE_TOTAL_PARTE_COMISION_ENTIDAD_FINANCIERA);
        if (porcentajeParteComisionEntidad == null) {
            throw Errors.illegalState("No se encontró una configuración de porcentaje de distribución de comisión (entidad financiera) para liquidación de factura (Entidad id=%s, Comprador id=%s, Factura id=%s)", idEntidadFinanciera, factura.getCompradorId(), factura.getId());
        }

        // Monto a considerar para la distribucion de comisiones.
        BigDecimal montoDistribucion = factura.getMontoTotalDistribucion();
        if (montoDistribucion == null || Numeric.menor(montoDistribucion, Numeric.CERO)) {
            throw Errors.illegalState("No se encontró un monto de distribución para liquidación de factura (Entidad id=%s, Comprador id=%s, Factura id=%s)", idEntidadFinanciera, factura.getCompradorId(), factura.getId());
        }

        // Calculo de monto para cada porcentaje.
        BigDecimal montoParteComisionComprador = Numeric.montoDePorcentaje(montoDistribucion, porcentajeParteComisionComprador);
        BigDecimal montoParteComisionSepsa = Numeric.montoDePorcentaje(montoDistribucion, porcentajeParteComisionSepsa);
        BigDecimal montoParteComisionEntidad = Numeric.montoDePorcentaje(montoDistribucion, porcentajeParteComisionEntidad);

        DistribucionComisionInfo facturaALiquidarBancoRio = new DistribucionComisionInfo();
        facturaALiquidarBancoRio.setIdConvenio(idConvenio);
        facturaALiquidarBancoRio.setPorcentajeComisionComprador(porcentajeParteComisionComprador);
        facturaALiquidarBancoRio.setMontoComisionComprador(montoParteComisionComprador);
        facturaALiquidarBancoRio.setPorcentajeComisionSepsa(porcentajeParteComisionSepsa);
        facturaALiquidarBancoRio.setMontoComisionSepsa(montoParteComisionSepsa);
        facturaALiquidarBancoRio.setPorcentajeComisionEntidad(porcentajeParteComisionEntidad);
        facturaALiquidarBancoRio.setMontoComisionEntidad(montoParteComisionEntidad);
        return facturaALiquidarBancoRio;
    }

    private BigDecimal obtenerConfigCargoParteComision(Integer idConvenio, TipoCargoEnum tipoCargo) {
        ConfigCargo configCargo = convenios.obtenerConfigCargo(idConvenio, tipoCargo).value(null);
        if (configCargo == null) {
            return null;
        }
        return cargos.obtenerValor(configCargo, (BigDecimal) null);
    }

    public void registrarLiquidacionComprador(Integer idComprador, BigDecimal montoLiquidacion, String codigo) throws Throwable {
        // Obtener token de autenticacion.
        ClienteEdiServer client = obtenerClienteEdiServer();
        fa.gs.utils.result.simple.Result<String> resToken = client.obtenerTokenAutenticacion();
        resToken.raise();
        String token = resToken.value();

        // Obtener o crear datos de comprador, como cliente de SEPSA.
        Persona comprador = personas.obtenerPorId(idComprador).value();
        fa.gs.utils.result.simple.Result<Integer> resIdCliente = client.obtenerOCrearIdClienteSepsa(token, Text.ruc(comprador), Text.razonSocial(comprador), Personas.TipoPersonaEnum.fromIdTipoPersona(comprador.getIdTipoPersona()));
        resIdCliente.raise();

        // Registrar liquidacion.
        RegistrarLiquidacionInput inputLiquidacionComprador = new RegistrarLiquidacionInput();
        inputLiquidacionComprador.setCodigoIsoMoneda("PYG");
        inputLiquidacionComprador.setCodigoProducto("E-COBRO");
        inputLiquidacionComprador.setCodigoEstado("PENDIENTE");
        inputLiquidacionComprador.setCodigo(codigo);
        inputLiquidacionComprador.setFechaEvento(Fechas.now());
        inputLiquidacionComprador.setIdCliente(resIdCliente.value());
        inputLiquidacionComprador.setCodigoTipoRegistroLiquidacion("GASTO_ADMINISTRATIVO");
        inputLiquidacionComprador.setValor(montoLiquidacion);
        fa.gs.utils.result.simple.Result<RegistrarLiquidacionOutput> resLiquidacion = client.registrarLiquidacion(token, inputLiquidacionComprador);
        resLiquidacion.raise();
    }

    public void registrarLiquidacionSepsa(BigDecimal montoLiquidacion, String codigo) throws Throwable {
        // Obtener token de autenticacion.
        ClienteEdiServer client = obtenerClienteEdiServer();
        fa.gs.utils.result.simple.Result<String> resToken = client.obtenerTokenAutenticacion();
        resToken.raise();
        String token = resToken.value();

        // Obtener o crear datos de SEPSA, como cliente de SEPSA.
        Result<Persona> resSepsa = personas.obtenerPorRuc("80049048-7");
        Persona sepsa = resSepsa.value();
        fa.gs.utils.result.simple.Result<Integer> resIdCliente = client.obtenerOCrearIdClienteSepsa(token, Text.ruc(sepsa), Text.razonSocial(sepsa), Personas.TipoPersonaEnum.fromIdTipoPersona(sepsa.getIdTipoPersona()));
        resIdCliente.raise();

        // Registrar liquidacion.
        RegistrarLiquidacionInput inputLiquidacionComprador = new RegistrarLiquidacionInput();
        inputLiquidacionComprador.setCodigoIsoMoneda("PYG");
        inputLiquidacionComprador.setCodigoProducto("E-COBRO");
        inputLiquidacionComprador.setCodigoEstado("PENDIENTE");
        inputLiquidacionComprador.setCodigo(codigo);
        inputLiquidacionComprador.setFechaEvento(Fechas.now());
        inputLiquidacionComprador.setIdCliente(resIdCliente.value());
        inputLiquidacionComprador.setCodigoTipoRegistroLiquidacion("GASTO_ADMINISTRATIVO");
        inputLiquidacionComprador.setValor(montoLiquidacion);
        fa.gs.utils.result.simple.Result<RegistrarLiquidacionOutput> resLiquidacion = client.registrarLiquidacion(token, inputLiquidacionComprador);
        resLiquidacion.raise();
    }

}
