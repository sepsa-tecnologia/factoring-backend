/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.notificacion;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "momento_notificacion", schema = "notificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MomentoNotificacion.findAll", query = "SELECT m FROM MomentoNotificacion m"),
    @NamedQuery(name = "MomentoNotificacion.findById", query = "SELECT m FROM MomentoNotificacion m WHERE m.id = :id"),
    @NamedQuery(name = "MomentoNotificacion.findByDescripcion", query = "SELECT m FROM MomentoNotificacion m WHERE m.descripcion = :descripcion")})
public class MomentoNotificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "momentoNotificacion")
    private Collection<ContactoMomentoNotificacion> contactoMomentoNotificacionCollection;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "idMomentoNotificacion")
    private Collection<Plantilla> plantillaCollection;

    public MomentoNotificacion() {
    }

    public MomentoNotificacion(Integer id) {
        this.id = id;
    }

    public MomentoNotificacion(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<ContactoMomentoNotificacion> getContactoMomentoNotificacionCollection() {
        return contactoMomentoNotificacionCollection;
    }

    public void setContactoMomentoNotificacionCollection(Collection<ContactoMomentoNotificacion> contactoMomentoNotificacionCollection) {
        this.contactoMomentoNotificacionCollection = contactoMomentoNotificacionCollection;
    }

    @XmlTransient
    public Collection<Plantilla> getPlantillaCollection() {
        return plantillaCollection;
    }

    public void setPlantillaCollection(Collection<Plantilla> plantillaCollection) {
        this.plantillaCollection = plantillaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof MomentoNotificacion)) {
            return false;
        }
        MomentoNotificacion other = (MomentoNotificacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.notificacion.MomentoNotificacion[ id=" + id + " ]";
    }

}
