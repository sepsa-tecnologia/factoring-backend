/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Arrays;
import fa.gs.utils.misc.Assertions;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.jboss.ejb3.annotation.TransactionTimeout;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.utils.Constantes;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.TxWrapperCore;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_SolicitarLiquidacionFacturas", mappedName = "PF_BancoRio_SolicitarLiquidacionFacturas")
@LocalBean
public class PF_BancoRio_SolicitarLiquidacionFacturas implements Serializable {

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Facturas facturas;

    @EJB
    private Notificaciones notificaciones;

    @EJB
    private PF_BancoRio_SolicitarLiquidacionFactura solicitarLiquidacionFactura;

    @TransactionTimeout(value = Constantes.TX_TIMEOUT_VALUE)
    public void work() throws Throwable {
        // Obtener identificadores de facturas pendientes de liquidacion.
        Result<List<Integer>> resIdFacturas = facturas.obtenerIdFacturasPendientesALiquidar();
        resIdFacturas.raise();
        List<Integer> idFacturas = resIdFacturas.value();

        if (!Assertions.isNullOrEmpty(idFacturas)) {
            // Obtener datos de facturas.
            Result<List<FacturaInfo>> resListaFacturas = facturas.buscarFacturas(Arrays.unwrap(idFacturas, Integer.class));
            resListaFacturas.raise();
            List<FacturaInfo> listaFacturas = resListaFacturas.value();

            // Agrupar facturas por entidad financiera.
            Map<Integer, List<FacturaInfo>> facturasPorEntidadFinanciera = facturas.agruparPorEntidadFinanciera(listaFacturas);
            for (Map.Entry<Integer, List<FacturaInfo>> entry : facturasPorEntidadFinanciera.entrySet()) {
                work0(entry.getKey(), entry.getValue());
            }
        }
    }

    private void work0(Integer idEntidadFinanciera, List<FacturaInfo> listaFacturas) throws Throwable {
        // Control.
        if (!bancoRio.esBancoRio(idEntidadFinanciera)) {
            return;
        }

        // Agrupar facturas por comprador.
        Map<Integer, List<FacturaInfo>> facturasPorComprador = facturas.agruparPorComprador(listaFacturas);
        for (Map.Entry<Integer, List<FacturaInfo>> entry : facturasPorComprador.entrySet()) {
            List<FacturaInfo> listaFacturas0 = entry.getValue();
            if (!Assertions.isNullOrEmpty(listaFacturas0)) {
                for (FacturaInfo factura : listaFacturas0) {
                    TxWrapperCore tx = Injection.lookup(TxWrapperCore.class);
                    Result<?> result = tx.execute(() -> {
                        solicitarLiquidacionFactura.solicitarLiquidacionFactura(idEntidadFinanciera, entry.getKey(), factura);
                        return null;
                    });
                    if (result.isFailure()) {
                        notificaciones.notificarSepsa(result.failure().cause(), "Ocurrió un error solicitando liquidación para factura id=%s", factura.getId());
                    }
                }
            }
        }
    }

}
