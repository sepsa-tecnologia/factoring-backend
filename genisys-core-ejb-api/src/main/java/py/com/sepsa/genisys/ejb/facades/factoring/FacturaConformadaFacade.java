/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaConformada;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "FacturaConformadaFacade", mappedName = "FacturaConformadaFacade")
@LocalBean
public class FacturaConformadaFacade extends AbstractFacade<FacturaConformada> {

    // <editor-fold desc="Facades.">
    @EJB
    private FacturaFacade facturaFacade;

    @EJB
    private NotaCreditoFacade notaCreditoFacade;

    @EJB
    private RetencionFacade retencionFacade;
    // </editor-fold>

    /**
     * Constructor.
     */
    public FacturaConformadaFacade() {
        super(FacturaConformada.class);
    }

    public FacturaConformada findByIdFactura(Integer idFactura) {
        Query q = getEntityManager().createNamedQuery("FacturaConformada.findById");
        q.setParameter("id", idFactura);
        return JpaUtils.getFirst(q);
    }

}
