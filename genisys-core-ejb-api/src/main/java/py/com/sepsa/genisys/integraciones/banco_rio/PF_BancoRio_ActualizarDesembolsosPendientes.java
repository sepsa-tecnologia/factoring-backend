/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.misc.fechas.Fechas;
import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.jboss.ejb3.annotation.TransactionTimeout;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioSeguimientoDesembolso;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Constantes;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.TxWrapperCore;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_ActualizarDesembolsosPendientes", mappedName = "PF_BancoRio_ActualizarDesembolsosPendientes")
@LocalBean
public class PF_BancoRio_ActualizarDesembolsosPendientes implements Serializable {

    private AppLogger log;

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Notificaciones notificaciones;

    @EJB
    private PF_BancoRio_ActualizarDesembolsoPendiente actualizarDesembolsoPendiente;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    @TransactionTimeout(value = Constantes.TX_TIMEOUT_VALUE)
    public void work() throws Throwable {
        // Obtener identificador de usuario para ingestion de datos.
        log.debug("Obteniendo usuario de ingestión ...");
        Integer idUsuarioIngestion = bancoRio.obtenerIdUsuarioIngestion();
        if (idUsuarioIngestion <= 0) {
            throw Errors.illegalState("El usuario para la ingestión no está establecido");
        }

        // Obtener cantidad de dias de tolerancia.
        log.debug("Obteniendo dias de tolerancia ...");
        Integer diasTolerancia = bancoRio.obtenerDiasTolerancia();

        // Obtener operaciones de desembolsos pendientes.
        log.debug("Obteniendo desembolsos pendientes ...");
        Collection<BancoRioSeguimientoDesembolso> solicitudesDesembolso = obtenerSolicitudesDesembolsoPendientes(diasTolerancia);
        log.debug("Consultas pendientes: %s", Lists.size(solicitudesDesembolso));
        for (BancoRioSeguimientoDesembolso solicitudDesembolso : solicitudesDesembolso) {
            TxWrapperCore tx = Injection.lookup(TxWrapperCore.class);
            Result<?> result = tx.execute(() -> work0(idUsuarioIngestion, solicitudDesembolso));
            if (result.isFailure()) {
                notificaciones.notificarSepsa(result.failure().cause(), "Ocurrió un error procesando solicitud de desembolso id=%s", solicitudDesembolso.getId());
            }
        }
    }

    /**
     * Obtiene la lista de consultas pendientes para facturas vendidas o en
     * analisis.
     *
     * @param diasTolerancia Cantidad de dias atras a considerar. Es decir, la
     * tolerancia en dias para considerar la consulta de estado para facturas.
     * @return Lista de consultas pendientes.
     * @throws Throwable Si ocurre un error.
     */
    private Collection<BancoRioSeguimientoDesembolso> obtenerSolicitudesDesembolsoPendientes(int diasTolerancia) throws Throwable {
        Date fecha = Fechas.addValue(Fechas.now(), Calendar.DAY_OF_MONTH, -1 * Math.abs(diasTolerancia));
        Result<Collection<BancoRioSeguimientoDesembolso>> result = bancoRio.obtenerSolicitudesDesembolsoPendientes(Fechas.inicioDia(fecha));
        result.raise();
        return result.value(Lists.empty());
    }

    private Void work0(Integer idUsuarioIngestion, BancoRioSeguimientoDesembolso solicitudDesembolso) throws Throwable {
        actualizarDesembolsoPendiente.work(idUsuarioIngestion, solicitudDesembolso);
        return null;
    }

}
