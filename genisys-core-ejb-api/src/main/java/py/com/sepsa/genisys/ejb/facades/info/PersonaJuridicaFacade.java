/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.info;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.info.PersonaJuridica;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author descauriza
 */
@Stateless(name = "PersonaJuridicaFacade", mappedName = "PersonaJuridicaFacade")
@LocalBean
public class PersonaJuridicaFacade extends AbstractFacade<PersonaJuridica> {

    public PersonaJuridicaFacade() {
        super(PersonaJuridica.class);
    }

}
