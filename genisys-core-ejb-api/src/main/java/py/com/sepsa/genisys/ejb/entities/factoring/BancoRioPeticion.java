/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "banco_rio_peticion", schema = "factoring")
@Data
public class BancoRioPeticion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "referencia")
    private String referencia;

    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo")
    private String tipo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private String estado;

    @Basic(optional = true)
    @Column(name = "request")
    private String request;

    @Basic(optional = true)
    @Column(name = "request_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestInicio;

    @Basic(optional = true)
    @Column(name = "request_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestFin;

    @Basic(optional = true)
    @Column(name = "response")
    private String response;

}
