/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "HistoricoFacturaFacade", mappedName = "HistoricoFacturaFacade")
@LocalBean
public class HistoricoFacturaFacade extends AbstractFacade<HistoricoFactura> {

    // <editor-fold desc="Facades.">
    @EJB
    private EstadoFacade estadoFacade;

    @EJB
    private FacturaFacade facturaFacade;
    // <editor-fold>

    /**
     * Constructor.
     */
    public HistoricoFacturaFacade() {
        super(HistoricoFactura.class);
    }

    /**
     * Permite obtener la lista de registros historicos relacionados a una
     * factura.
     *
     * @param idFactura Identificador de factura.
     * @return Lista de registros historicoas relacionados a una factura.
     */
    public List<HistoricoFactura> findByIdFactura(Integer idFactura) {
        Query q = getEntityManager().createNamedQuery("HistoricoFactura.findByIdFactura");
        q.setParameter("idFactura", idFactura);
        return JpaUtils.getAll(q);
    }

}
