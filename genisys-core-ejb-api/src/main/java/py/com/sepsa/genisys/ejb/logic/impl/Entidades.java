/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.criteria.query.Order;
import fa.gs.misc.text.Joiner;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java8.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.PersonaEntidad;
import py.com.sepsa.genisys.ejb.entities.factoring.PersonaEntidadSolicitud;
import py.com.sepsa.genisys.ejb.entities.info.PersonaEntidadInfo;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;
import py.com.sepsa.genisys.ejb.utils.Persistence;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Entidades", mappedName = "Entidades")
@LocalBean
public class Entidades extends LogicBean {

    @PersistenceContext(unitName = Persistence.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @EJB
    private JpaUtils jpaUtils;

    public boolean tieneEstado(PersonaEntidadInfo item, EstadoAdhesionEntidad estado) {
        EstadoAdhesionEntidad estado0 = EstadoAdhesionEntidad.fromEstado(item.getEstado());
        return Objects.equals(estado0, estado);
    }

    public Result<Void> actualizarEstadoAdhesionEntidad(Integer idPersonaEntidad, EstadoAdhesionEntidad estado) {
        try {
            String query = Strings.format(" update factoring.persona_entidad set estado = '%s' where id = %s ", estado.codigo, idPersonaEntidad);
            jpaUtils.executeUpdate(query);

            return Results.ok()
                    .nullable(true)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error actualizando estado de adhesion con entidad")
                    .tag("persona_entidad.id", idPersonaEntidad)
                    .build();
        }
    }

    public Result<EstadoAdhesionEntidad> obtenerEstadoAdhesion(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera) {
        try {
            EstadoAdhesionEntidad estado;
            PersonaEntidad instance = obtener(idPersona, idPerfil, idEntidadFinanciera);
            if (instance == null) {
                estado = null;
            } else {
                estado = EstadoAdhesionEntidad.fromEstado(instance.getEstado());
            }

            return Results.ok()
                    .nullable(true)
                    .value(estado)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message(thr.getMessage())
                    .build();
        }
    }

    public Result<List<PersonaEntidad>> obtenerSolicitudesAdhesionPendientes() {
        try {
            Query q = em.createNamedQuery("PersonaEntidad.findByEstado");
            q.setParameter("estado", "P");
            List<PersonaEntidad> rows = q.getResultList();
            return Results.ok()
                    .value(rows)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrio un error obteniendo solicitudes pendientes")
                    .build();
        }
    }

    public Result<PersonaEntidad> obtenerOCrearAdhesionConEntidad(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera) {
        try {
            // Obtener datos.
            PersonaEntidad instance = obtener(idPersona, idPerfil, idEntidadFinanciera);
            if (instance == null) {
                instance = crear(idPersona, idPerfil, idEntidadFinanciera, "P");
            }

            return Results.ok()
                    .value(instance)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .build();
        }
    }

    private PersonaEntidad obtener(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera) throws Throwable {
        try {
            Query q = em.createNamedQuery("PersonaEntidad.findByIds");
            q.setParameter("idPersona", idPersona);
            q.setParameter("idPerfil", idPerfil);
            q.setParameter("idEntidadFinanciera", idEntidadFinanciera);
            Object obj = q.getSingleResult();
            return PersonaEntidad.class.cast(obj);
        } catch (NoResultException nre) {
            return null;
        }
    }

    private PersonaEntidad crear(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, String estado) throws Throwable {
        PersonaEntidad instance = new PersonaEntidad();
        instance.setIdPersona(idPersona);
        instance.setIdPerfil(idPerfil);
        instance.setIdEntidadFinanciera(idEntidadFinanciera);
        instance.setEstado(estado);
        instance = jpaUtils.create(instance);
        return instance;
    }

    public Result<PersonaEntidadSolicitud> crearSolicitudAdhesionEntidad(Integer idPersonaEntidad, Date fechaSolicitud, Date fechaRespuesta, String respuesta) {
        try {
            PersonaEntidadSolicitud instance = new PersonaEntidadSolicitud();
            instance.setIdPersonaEntidad(idPersonaEntidad);
            instance.setFechaSolicitud(fechaSolicitud);
            instance.setFechaRespuesta(fechaRespuesta);
            instance.setRespuestaEntidad(respuesta);
            instance = jpaUtils.create(instance);

            return Results.ok()
                    .value(instance)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error registrando solicitud de adhesion para entidad financiera.")
                    .tag("persona_entidad.id", idPersonaEntidad)
                    .build();
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Obtener Relaciones Persona-Entidad">
    public List<PersonaEntidadInfo> selectPersonaEntidades(Integer idPersona, Integer idPerfil, Integer limit, Integer offset, Map<String, Object> filterBy, Map<String, Order> orderBy) {
        List<PersonaEntidadInfo> personas = Lists.empty();

        try {
            // Ejecutar query.
            String query = queryPersonaEntidades(idPersona, idPerfil, false, limit, offset, filterBy, orderBy);
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Agregar registros.
            if (!Assertions.isNullOrEmpty(rows)) {
                rows.stream()
                        .map(this::mapPersonaEntidadInfo)
                        .forEach(personas::add);
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando facturas");
        }

        return personas;
    }

    private PersonaEntidadInfo mapPersonaEntidadInfo(Map<String, Object> row) {
        PersonaEntidadInfo instance = new PersonaEntidadInfo();
        instance.setId(Maps.integer(row, "id"));
        instance.setIdPersona(Maps.integer(row, "id_persona"));
        instance.setIdPerfil(Maps.integer(row, "id_perfil"));
        instance.setIdEntidadFinanciera(Maps.integer(row, "id_entidad_financiera"));
        instance.setRazonSocialEntidadFinanciera(Maps.string(row, "razon_social_entidad_financiera"));
        instance.setRucEntidadFinanciera(Maps.string(row, "ruc_entidad_financiera"));
        instance.setEstado(Maps.character(row, "estado").toString());
        return instance;
    }

    public Integer countPersonaEntidades(Integer idPersona, Integer idPerfil, Map<String, Object> filterBy) {
        try {
            // Ejecutar query.
            String query = queryPersonaEntidades(idPersona, idPerfil, true, null, null, filterBy, null);
            Map<String, Object> row = jpaUtils.getFirst(query);

            BigInteger count = Maps.biginteger(row, "total");
            return count.intValue();
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando entidades");
            return -1;
        }
    }

    private String queryPersonaEntidades(Integer idPersona, Integer idPerfil, boolean forCount, Integer limit, Integer offset, Map<String, Object> filterBy, Map<String, Order> orderBy) {
        // Construir query.
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");

        // Proyecciones.
        if (forCount) {
            builder.append("  cast(count(*) as int8) as \"total\" ");
        } else {
            builder.append("  pe.id as \"id\", ");
            builder.append("  pe.id_persona as \"id_persona\", ");
            builder.append("  pe.id_perfil as \"id_perfil\", ");
            builder.append("  pe.id_entidad_financiera as \"id_entidad_financiera\", ");
            builder.append("  pj.razon_social as \"razon_social_entidad_financiera\", ");
            builder.append("  p.ruc || '-' || cast(p.dv_ruc as text) as \"ruc_entidad_financiera\", ");
            builder.append("  pe.estado as \"estado\" ");
        }

        // Origen de datos.
        builder.append(" from factoring.persona_entidad as pe ");
        builder.append(" left join info.persona as p on p.id = pe.id_entidad_financiera ");
        builder.append(" left join info.persona_juridica as pj on pj.id = p.id ");

        // Filtros.
        builder.append(" where ");
        builder.append("   1 = 1 ");
        builder.append("   and pe.id_persona = %s ", idPersona);
        builder.append("   and pe.id_perfil = %s ", idPerfil);
        if (!Assertions.isNullOrEmpty(filterBy)) {
            filterBy.entrySet().stream().forEach(e -> {
                builder.append(" and %s ", String.valueOf(e.getValue()));
            });
        }

        // Ordenacion.
        if (forCount == false) {
            if (!Assertions.isNullOrEmpty(orderBy)) {
                String[] clauses = orderBy.entrySet().stream()
                        .map(e -> Strings.format("%s %s", e.getKey(), e.getValue()))
                        .distinct()
                        .toArray(String[]::new);
                String clause = Joiner.of(clauses).separator(",").join();

                builder.append(" order by ");
                builder.append(" %s ", clause);
            }
        }

        // Paginacion.
        if (forCount == false) {
            if (limit != null) {
                builder.append(" limit %s ", limit);
            }
            if (offset != null) {
                builder.append(" offset %s ", offset);
            }
        }

        return builder.toString();
    }
    //</editor-fold>

    public Result<Collection<PersonaEntidadSolicitud>> obtenerSolicitudesAdhesionConEntidad(Integer idPersonaEntidad) {
        try {
            // Ejecutar consulta.
            String sql = Strings.format(" select * from factoring.persona_entidad_solicitud where id_persona_entidad = %s order by fecha_solicitud desc, fecha_respuesta desc ", idPersonaEntidad);
            Collection<Map<String, Object>> rows = jpaUtils.getAll(sql);

            // Extraer datos-
            Collection<PersonaEntidadSolicitud> items = Lists.stream(rows)
                    .map(this::map2PersonaEntidadSolicitud)
                    .collect(Collectors.toList());

            return Results.ok()
                    .value(items)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo solicitudes de adhesión con entidad financiera")
                    .tag("persona_entidad.id", idPersonaEntidad)
                    .build();
        }
    }

    private PersonaEntidadSolicitud map2PersonaEntidadSolicitud(Map<String, Object> row) {
        PersonaEntidadSolicitud instance = new PersonaEntidadSolicitud();
        instance.setId(Maps.integer(row, "id"));
        instance.setIdPersonaEntidad(Maps.integer(row, "id_persona_entidad"));
        instance.setFechaSolicitud(Maps.date(row, "fecha_solicitud"));
        instance.setFechaRespuesta(Maps.date(row, "fecha_respuesta"));
        instance.setRespuestaEntidad(Maps.string(row, "respuesta_entidad"));
        return instance;
    }

    public enum EstadoAdhesionEntidad {
        INDEFINIDO("-"),
        PENDIENTE("P"),
        ACTIVO("A"),
        RECHAZADO("R");
        private final String codigo;

        private EstadoAdhesionEntidad(String codigo) {
            this.codigo = codigo;
        }

        public static EstadoAdhesionEntidad fromEstado(String estado) {
            for (EstadoAdhesionEntidad estado0 : EstadoAdhesionEntidad.values()) {
                if (Objects.equals(estado, estado0.codigo)) {
                    return estado0;
                }
            }
            return null;
        }

        public String codigo() {
            return codigo;
        }

    }

}
