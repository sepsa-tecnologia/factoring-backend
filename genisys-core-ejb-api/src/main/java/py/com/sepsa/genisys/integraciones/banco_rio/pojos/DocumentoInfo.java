/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio.pojos;

import java.io.Serializable;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoDocumento;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class DocumentoInfo implements Serializable {

    private String nroDocumento;
    private BancoRioTipoDocumento tipoDocumento;
    private byte[] imagen;
}
