/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.notificacion;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "notificacion_contacto", schema = "notificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotificacionContacto.findAll", query = "SELECT n FROM NotificacionContacto n"),
    @NamedQuery(name = "NotificacionContacto.findByIdNotificacion", query = "SELECT n FROM NotificacionContacto n WHERE n.notificacionContactoPK.idNotificacion = :idNotificacion"),
    @NamedQuery(name = "NotificacionContacto.findByIdContacto", query = "SELECT n FROM NotificacionContacto n WHERE n.notificacionContactoPK.idContacto = :idContacto")})
public class NotificacionContacto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NotificacionContactoPK notificacionContactoPK;
    @JoinColumn(name = "id_notificacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ConfigNotificacion configNotificacion;

    public NotificacionContacto() {
    }

    public NotificacionContacto(NotificacionContactoPK notificacionContactoPK) {
        this.notificacionContactoPK = notificacionContactoPK;
    }

    public NotificacionContacto(int idNotificacion, int idContacto) {
        this.notificacionContactoPK = new NotificacionContactoPK(idNotificacion, idContacto);
    }

    public NotificacionContactoPK getNotificacionContactoPK() {
        return notificacionContactoPK;
    }

    public void setNotificacionContactoPK(NotificacionContactoPK notificacionContactoPK) {
        this.notificacionContactoPK = notificacionContactoPK;
    }

    public ConfigNotificacion getConfigNotificacion() {
        return configNotificacion;
    }

    public void setConfigNotificacion(ConfigNotificacion configNotificacion) {
        this.configNotificacion = configNotificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notificacionContactoPK != null ? notificacionContactoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof NotificacionContacto)) {
            return false;
        }
        NotificacionContacto other = (NotificacionContacto) object;
        if ((this.notificacionContactoPK == null && other.notificacionContactoPK != null) || (this.notificacionContactoPK != null && !this.notificacionContactoPK.equals(other.notificacionContactoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.notificacion.NotificacionContacto[ notificacionContactoPK=" + notificacionContactoPK + " ]";
    }

}
