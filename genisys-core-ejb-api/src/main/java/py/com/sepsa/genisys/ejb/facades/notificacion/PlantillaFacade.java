/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.notificacion;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.notificacion.Plantilla;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author descauriza
 */
@Stateless(name = "PlantillaFacade", mappedName = "PlantillaFacade")
@LocalBean
public class PlantillaFacade extends AbstractFacade<Plantilla> {

    public PlantillaFacade() {
        super(Plantilla.class);
    }

    /**
     * Obtiene una plantilla de notificacion en base a un asunto especifico.
     *
     * @param asunto Asunto utilizado como criterio de busqueda.
     * @return Plantilla de notificacion, si hubiere. Null caso contrario.
     */
    public Plantilla findByAsunto(String asunto) {
        Query q = getEntityManager().createNamedQuery("Plantilla.findByAsunto");
        q.setParameter("asunto", asunto);
        return JpaUtils.getFirst(q);
    }

    /**
     * Obtiene una plantilla de notificacion en base a un asunto y cuerpo
     * especifico.
     *
     * @param asunto Asunto utilizado como criterio de busqueda.
     * @param cuerpo Cuerpo utilizado como criterio de busqueda.
     * @return Plantilla de notificacion, si hubiere. Null caso contrario.
     */
    public Plantilla findByAsuntoCuerpo(String asunto, String cuerpo) {
        Query q = getEntityManager().createNamedQuery("Plantilla.findByAsuntoCuerpo");
        q.setParameter("asunto", asunto);
        q.setParameter("cuerpo", cuerpo);
        return JpaUtils.getFirst(q);
    }

}
