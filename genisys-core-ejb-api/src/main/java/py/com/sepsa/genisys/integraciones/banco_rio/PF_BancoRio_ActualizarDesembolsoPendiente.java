/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.Units;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioSeguimientoDesembolso;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.BancoRioEstadoSeguimientoSolicitudDesembolsoEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_ActualizarDesembolsoPendiente", mappedName = "PF_BancoRio_ActualizarDesembolsoPendiente")
@LocalBean
public class PF_BancoRio_ActualizarDesembolsoPendiente implements Serializable {

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Personas personas;

    @EJB
    private Facturas facturas;

    @EJB
    private JpaUtils jpaUtils;

    @EJB
    private Notificaciones notificaciones;

    private AppLogger log;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    public void work(Integer idUsuarioIngestion, BancoRioSeguimientoDesembolso solicitudDesembolso) throws Throwable {
        // Verificar que solicitud este pendiente.
        if (solicitudDesembolso != null) {
            BancoRioEstadoSeguimientoSolicitudDesembolsoEnum estado = BancoRioEstadoSeguimientoSolicitudDesembolsoEnum.fromCodigo(solicitudDesembolso.getEstado());
            if (estado != BancoRioEstadoSeguimientoSolicitudDesembolsoEnum.PENDIENTE) {
                throw Errors.illegalState("La solicitud de desembolso ya presenta el estado '%s'", estado);
            }
        }

        // Obtener datos de comprador.
        Result<Persona> resPersona = personas.obtenerPorId(solicitudDesembolso.getIdComprador());
        Persona comprador = resPersona.value();

        // Preparar datos de entrada.
        ConsultaEstadoDesembolsoInput input = new ConsultaEstadoDesembolsoInput();
        input.setComprador(comprador);
        input.setCompradorDocumento(bancoRio.resolveDocumento(comprador));
        input.setIdSolicitud(solicitudDesembolso.getIdSolicitud());

        // Consultar estado de desembolso.
        log.debug("consultando estado desembolso id_lote=%s id_solicitud=%s", solicitudDesembolso.getIdLote(), solicitudDesembolso.getIdSolicitud());
        Result<ConsultaEstadoDesembolsoOutput> resRespuesta = bancoRio.consultarEstadoDesembolso(null, null, null, input);
        if (resRespuesta.isFailure()) {
            log.error()
                    .cause(resRespuesta.failure().cause())
                    .message("Ocurrió un error consultando estado de desembolso")
                    .tag("id_lote", solicitudDesembolso.getIdLote())
                    .tag("id_solicitud", solicitudDesembolso.getIdSolicitud())
                    .log();
            throw Errors.illegalState(resRespuesta.failure().cause(), "Ocurrió un error consultando estado de desembolso");
        }

        // Control.
        ConsultaEstadoDesembolsoOutput output = resRespuesta.value();
        if (!output.getRespuesta().isOk()) {
            String msg = bancoRio.resolveErrorMessage(output.getTrack(), output.getRespuesta());
            log.warning()
                    .message("Ocurrió un error consultando estado de desembolso. %s", msg)
                    .tag("id_lote", solicitudDesembolso.getIdLote())
                    .tag("id_solicitud", solicitudDesembolso.getIdSolicitud())
                    .log();
            throw Errors.illegalState("Ocurrió un error consultando estado de desembolso (%s)", msg);
        }

        // Procesar facturas.
        if (output.getRespuesta().isOk()) {
            // Determinar estado de facturas.
            Estados.EstadoFacturaEnum estadoFacturas = resolveEstadoFacturas(output.getRespuesta());
            String decisionFacturas = resolveDecisionFacturas(output.getRespuesta());

            // Obtener facturas en lote.
            List<FacturaInfo> facturasOk = Lists.empty();
            Collection<FacturaInfo> facturas0 = obtenerFacturas(solicitudDesembolso.getIdLote());

            // Modificar estado de facturas.
            for (FacturaInfo factura0 : facturas0) {
                Result<?> resIngestion = ingestFactura(idUsuarioIngestion, factura0, estadoFacturas, decisionFacturas);
                if (resIngestion.isFailure()) {
                    log.warning()
                            .message("Ocurrió un error cambiando estado de factura luego de solicitud de desembolso.")
                            .tag("solicitud.id_solicitud", solicitudDesembolso.getIdSolicitud())
                            .tag("lote.id", solicitudDesembolso.getIdLote())
                            .tag("factura.id", factura0.getId())
                            .tag("factura.estado", estadoFacturas)
                            .log();
                } else {
                    // Agregar factura a coleccion de facturas modificadas.
                    facturasOk.add(factura0);
                }
            }

            // Modificar estado de solicitud de desembolso.
            Result<?> resIngestion = ingestSolicitudDesembolso(solicitudDesembolso, estadoFacturas);
            if (resIngestion.isFailure()) {
                log.warning()
                        .cause(resIngestion.failure().cause())
                        .message("Ocurrió un error cambiando estado de datos de seguimiento de desembolso.")
                        .tag("solicitud.id_solicitud", solicitudDesembolso.getIdSolicitud())
                        .tag("lote.id", solicitudDesembolso.getIdLote())
                        .log();
                throw Errors.illegalState(resIngestion.failure().cause(), "Ocurrió un error consultando estado de desembolso");
            }

            // Notificar modificacion de facturas..
            notificarModificacionFacturas(facturasOk, estadoFacturas);
        }
    }

    private Estados.EstadoFacturaEnum resolveEstadoFacturas(RespuestaConsultaEstadoDesembolso respuesta) {
        // Valor de estado.
        String estado = Units.execute("PENDIENTE", () -> respuesta.getEstado());
        estado = estado.toUpperCase();

        // Control.
        if (estado.equals("APROBADA") || estado.equals("APROBADO")) {
            return Estados.EstadoFacturaEnum.PAGADA;
        }

        // Control.
        if (estado.equals("RECHAZADO") || estado.equals("RECHAZADA")) {
            return Estados.EstadoFacturaEnum.RECHAZADA_POR_ENTIDAD;
        }

        // Control.
        if (estado.equals("ERROR")) {
            return Estados.EstadoFacturaEnum.RECHAZADA_POR_ENTIDAD;
        }

        return Estados.EstadoFacturaEnum.EN_ESTUDIO_POR_ENTIDAD;
    }

    private String resolveDecisionFacturas(RespuestaConsultaEstadoDesembolso respuesta) {
        String estado = respuesta.getEstado();
        String decision = respuesta.getDecision();
        return Strings.format("%s -- %s", estado, decision);
    }

    private Collection<FacturaInfo> obtenerFacturas(Integer idLote) throws Throwable {
        Result<List<FacturaInfo>> result = facturas.buscarFacturasEnLote(idLote);
        result.raise();
        return result.value(Lists.empty());
    }

    private Result<Void> ingestFactura(Integer idUsuario, FacturaInfo factura, Estados.EstadoFacturaEnum estado, String comentario) {
        try {
            // Cambiar estado de factura, si hubiere.
            Result<Factura> resCambioEstado = null;
            if (estado != null && factura.getEstadoDescripcion().equals(estado.getDescripcion()) == false) {
                resCambioEstado = facturas.cambiarEstado(idUsuario, factura.getId(), estado);
                resCambioEstado.raise();
            }

            // Agregar historico, si hubiere.
            Result<HistoricoFactura> resHistorico = null;
            if (resCambioEstado != null) {
                comentario = comentario.trim();
                String msg = "RESPUESTA AUTOMATICA DE ENTIDAD" + (Assertions.stringNullOrEmpty(comentario) ? "" : (": " + comentario));
                resHistorico = facturas.agregarHistorico(idUsuario, resCambioEstado.value(), estado, Strings.format(msg));
                resHistorico.raise();
            }

            return Results.ok()
                    .nullable(true)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error procesando operacion de seguimiento de desembolso")
                    .build();
        }
    }

    private Result<Void> ingestSolicitudDesembolso(BancoRioSeguimientoDesembolso seguimiento, Estados.EstadoFacturaEnum estado) {
        try {
            // Determinar estado de seguimiento de desembolso.
            BancoRioEstadoSeguimientoSolicitudDesembolsoEnum estadoSeguimientoDesembolso = null;
            if (estado == Estados.EstadoFacturaEnum.VENDIDA || estado == Estados.EstadoFacturaEnum.PAGADA || estado == Estados.EstadoFacturaEnum.RECHAZADA_POR_ENTIDAD) {
                estadoSeguimientoDesembolso = BancoRioEstadoSeguimientoSolicitudDesembolsoEnum.RESUELTA;
            } else {
                estadoSeguimientoDesembolso = BancoRioEstadoSeguimientoSolicitudDesembolsoEnum.PENDIENTE;
            }

            // Modificar estado de registro de seguimiento.
            String query = " update factoring.banco_rio_seguimiento_desembolso set estado = '%s', fecha_respuesta = now() where id = %s ";
            query = Strings.format(query, estadoSeguimientoDesembolso.codigo(), seguimiento.getId());
            int c = jpaUtils.executeUpdate(query);
            if (c != 1) {
                throw Errors.illegalState();
            }

            return Results.ok()
                    .nullable(true)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message(thr.getMessage())
                    .build();
        }
    }

    private void notificarModificacionFacturas(List<FacturaInfo> facturas, Estados.EstadoFacturaEnum estado) {
        if (estado == Estados.EstadoFacturaEnum.RECHAZADA_POR_ENTIDAD) {
            notificaciones.notificarRechazoDeFacturasPorEntidad(facturas);
        } else if (estado == Estados.EstadoFacturaEnum.PAGADA) {
            notificaciones.notificarDesembolsoDeFacturas(facturas);
        }
    }

}
