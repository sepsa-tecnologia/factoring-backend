/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.database.query.expressions.literals.SQLStringLiterals;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.Units;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.fechas.Fechas;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioSeguimientoLiquidacion;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.FacturaEstadoSeguimientoLiquidacionEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.impl.Sepsa;
import py.com.sepsa.genisys.ejb.logic.pojos.DistribucionComisionInfo;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_ActualizarLiquidacionPendiente", mappedName = "PF_BancoRio_ActualizarLiquidacionPendiente")
@LocalBean
public class PF_BancoRio_ActualizarLiquidacionPendiente implements Serializable {

    private AppLogger log;

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Sepsa sepsa;

    @EJB
    private Personas personas;

    @EJB
    private Facturas facturas;

    @EJB
    private JpaUtils jpaUtils;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    public void work(Integer idUsuarioIngestion, BancoRioSeguimientoLiquidacion solicitudLiquidacion) throws Throwable {
        // Verificar que solicitud este pendiente.
        if (solicitudLiquidacion != null) {
            FacturaEstadoSeguimientoLiquidacionEnum estado = FacturaEstadoSeguimientoLiquidacionEnum.fromCodigo(solicitudLiquidacion.getEstado());
            if (estado != FacturaEstadoSeguimientoLiquidacionEnum.PENDIENTE) {
                throw Errors.illegalState("La solicitud de liquidación ya presenta el estado '%s'", estado);
            }
        }

        // Obtener datos de factura.
        Result<FacturaInfo> resFactura = facturas.buscarFactura(solicitudLiquidacion.getIdFactura());
        FacturaInfo factura = resFactura.value();

        // Obtener datos de comprador.
        Result<Persona> resPersona = personas.obtenerPorId(factura.getCompradorId());
        Persona comprador = resPersona.value();

        // Preparar datos de entrada.
        ConsultaEstadoLiquidacionInput input = new ConsultaEstadoLiquidacionInput();
        input.setComprador(comprador);
        input.setCompradorDocumento(bancoRio.resolveDocumento(comprador));
        input.setIdLiquidacion(solicitudLiquidacion.getIdLiquidacion());

        // Consultar estado de desembolso.
        log.debug("consultando estado liquidacion id_factura=%s", solicitudLiquidacion.getIdFactura());
        Result<ConsultaEstadoLiquidacionOutput> resRespuesta = bancoRio.consultarEstadoLiquidacion(null, null, null, input);

        // Actualizar fecha de respuesta.
        String query = "update factoring.banco_rio_seguimiento_liquidacion set fecha_respuesta = %s";
        query = Strings.format(query, SQLStringLiterals.fechaHora(Fechas.now()));
        jpaUtils.executeUpdate(query);

        // Control.
        if (resRespuesta.isFailure()) {
            log.error()
                    .cause(resRespuesta.failure().cause())
                    .message("Ocurrió un error consultando estado de liquidación")
                    .tag("id_factura", solicitudLiquidacion.getIdFactura())
                    .tag("id_liquidacion", solicitudLiquidacion.getIdLiquidacion())
                    .log();
            resRespuesta.raise();
        }

        // Control.
        ConsultaEstadoLiquidacionOutput output = resRespuesta.value();
        if (!output.getRespuesta().isOk()) {
            String msg = bancoRio.resolveErrorMessage(output.getTrack(), output.getRespuesta());
            log.warning()
                    .message("Ocurrió un error consultando estado de liquidación. %s", msg)
                    .tag("id_factura", solicitudLiquidacion.getIdFactura())
                    .tag("id_liquidacion", solicitudLiquidacion.getIdLiquidacion())
                    .log();
            throw Errors.illegalState("Ocurrió un error consultando estado de liquidación (%s)", msg);
        }

        // Procesar respuesta.
        if (output.getRespuesta().isOk()) {
            // Determinar estado de liquidacion.
            FacturaEstadoSeguimientoLiquidacionEnum estadoLiquidacion = resolveEstadoLiquidacion(output.getRespuesta());

            // Modificar estado de solicitud - SEPSA.
            query = " update factoring.factura_seguimiento_liquidacion set estado = '%s' where id_factura = %s ";
            query = Strings.format(query, estadoLiquidacion.codigo(), factura.getId());
            jpaUtils.executeUpdate(query);

            // Modificar estado de solicitud - ENTIDAD.
            query = "update factoring.banco_rio_seguimiento_liquidacion set estado = '%s' where id_factura = %s and id_liquidacion = '%s'";
            query = Strings.format(query, estadoLiquidacion.codigo(), factura.getId(), solicitudLiquidacion.getIdLiquidacion());
            jpaUtils.executeUpdate(query);

            // Modificar estado de factura - LIQUIDADO.
            if (estadoLiquidacion == FacturaEstadoSeguimientoLiquidacionEnum.LIQUIDADA) {
                // Nuevo estado de factura.
                Estados.EstadoFacturaEnum estado = Estados.EstadoFacturaEnum.LIQUIDADA;

                // Cambiar estado de factura, si hubiere.
                Result<Factura> resCambioEstado = null;
                resCambioEstado = facturas.cambiarEstado(idUsuarioIngestion, factura.getId(), estado);
                resCambioEstado.raise();

                // Agregar historico, si hubiere.
                String comentario = output.getRespuesta().getMensaje().trim();
                String msg = "RESPUESTA AUTOMATICA DE ENTIDAD (LIQUIDACIÓN)" + (Assertions.stringNullOrEmpty(comentario) ? "" : (": " + comentario));
                Result<HistoricoFactura> resHistorico = facturas.agregarHistorico(idUsuarioIngestion, resCambioEstado.value(), estado, msg);
                resHistorico.raise();

                // Liquidar via Sepsa, si estado lo permite.
                registrarLiquidacionEnSepsa(factura);
            }

            // Modificar estado de factura - RECHAZADO.
            if (estadoLiquidacion == FacturaEstadoSeguimientoLiquidacionEnum.RECHAZADA) {
                // Nuevo estado de factura.
                Estados.EstadoFacturaEnum estado = Estados.EstadoFacturaEnum.RECHAZADA_POR_ENTIDAD;

                // Cambiar estado de factura, si hubiere.
                Result<Factura> resCambioEstado = null;
                resCambioEstado = facturas.cambiarEstado(idUsuarioIngestion, factura.getId(), estado);
                resCambioEstado.raise();

                // Agregar historico, si hubiere.
                String comentario = output.getRespuesta().getDecision().trim();
                String msg = "RESPUESTA AUTOMATICA DE ENTIDAD (LIQUIDACIÓN)" + (Assertions.stringNullOrEmpty(comentario) ? ": --" : (": " + comentario));
                Result<HistoricoFactura> resHistorico = facturas.agregarHistorico(idUsuarioIngestion, resCambioEstado.value(), estado, msg);
                resHistorico.raise();
            }
        }
    }

    private FacturaEstadoSeguimientoLiquidacionEnum resolveEstadoLiquidacion(RespuestaConsultaEstadoLiquidacion respuesta) {
        // Valor de estado.
        String estado = Units.execute("SOLICITADO", () -> respuesta.getEstado());
        estado = estado.toUpperCase();

        // Control.
        if (estado.equals("ERROR")) {
            return FacturaEstadoSeguimientoLiquidacionEnum.RECHAZADA;
        }

        // Control.
        if (estado.equals("APROBADA") || estado.equals("APROBADO")) {
            return FacturaEstadoSeguimientoLiquidacionEnum.LIQUIDADA;
        }

        // Control.
        if (estado.equals("RECHAZADO") || estado.equals("RECHAZADA")) {
            return FacturaEstadoSeguimientoLiquidacionEnum.RECHAZADA;
        }

        return FacturaEstadoSeguimientoLiquidacionEnum.PENDIENTE;
    }

    private void registrarLiquidacionEnSepsa(FacturaInfo factura) throws Throwable {
        String query;

        // Calcular distribuciones de comision.
        DistribucionComisionInfo distribucion = sepsa.calcularDistribucionComisiones(factura, bancoRio.obtenerIdEntidadBancoRio());

        // Codigo de liquidacion.
        long codLiquidacion = System.currentTimeMillis();

        // Registrar liquidacion de comprador.
        String codLiqComprador = String.valueOf(codLiquidacion) + "C";
        sepsa.registrarLiquidacionComprador(factura.getCompradorId(), distribucion.getMontoComisionComprador(), codLiqComprador);
        query = " update factoring.factura_seguimiento_liquidacion set cod_liq_comprador = '%s' where id_factura = %s ";
        query = Strings.format(query, codLiqComprador, factura.getId());
        jpaUtils.executeUpdate(query);

        // Registrar liquidacion de sepsa.
        String codLiqSepsa = String.valueOf(codLiquidacion) + "S";
        sepsa.registrarLiquidacionSepsa(distribucion.getMontoComisionSepsa(), codLiqSepsa);
        query = " update factoring.factura_seguimiento_liquidacion set cod_liq_sepsa = '%s' where id_factura = %s ";
        query = Strings.format(query, codLiqSepsa, factura.getId());
        jpaUtils.executeUpdate(query);
    }

}
