/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.info;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.info.Email;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author descauriza
 */
@Stateless(name = "EmailFacade", mappedName = "EmailFacade")
@LocalBean
public class EmailFacade extends AbstractFacade<Email> {

    public EmailFacade() {
        super(Email.class);
    }

}
