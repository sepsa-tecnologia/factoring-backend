/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import fa.gs.utils.logging.Logger;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.logging.log4j2.Log4j2LoggerFactory;
import java.io.InputStream;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author Fabio A. González Sosa
 */
public class AppLoggerFactory extends Log4j2LoggerFactory {

    static {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream is = loader.getResourceAsStream("py/com/sepsa/genisys/core/utils/logging/Log.xml");
        Log4j2LoggerFactory.configure(is);
    }

    private static AppLogger logger(String name) {
        org.apache.logging.log4j.core.Logger impl = (org.apache.logging.log4j.core.Logger) LogManager.getLogger(name);
        return new AppLogger(impl);
    }

    public static AppLogger core() {
        AppLogger logger = logger("genisys.core.logger");
        logger.tag("origen", "genisys.core");
        return logger;
    }

    public static AppLogger web() {
        AppLogger logger = logger("genisys.web.logger");
        logger.tag("origen", "genisys.web");
        return logger;
    }

    public static AppLogger rest() {
        AppLogger logger = logger("genisys.rest.logger");
        logger.tag("origen", "genisys.rest");
        return logger;
    }

    public static AppLogger notificador() {
        AppLogger logger = logger("genisys.notificador.logger");
        logger.tag("origen", "genisys.notificador");
        return logger;
    }

    public static AppLogger ingest(String name) {
        AppLogger logger = logger("genisys.ingest.logger");
        logger.tag("origen", "genisys.ingest");
        logger.tag("ingest", name);
        return logger;
    }

    @Override
    public Logger getLogger(String name) {
        return AppLoggerFactory.logger(name);
    }

}
