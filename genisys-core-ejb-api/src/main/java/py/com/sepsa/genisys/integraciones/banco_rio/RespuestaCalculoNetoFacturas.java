/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.misc.Assertions;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.misc.text.Text;
import fa.gs.utils.misc.xml.Xml;
import fa.gs.utils.misc.xml.XmlPrefixInfoList;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import javax.xml.xpath.XPath;
import lombok.Data;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import py.com.sepsa.genisys.ejb.utils.Fechas;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class RespuestaCalculoNetoFacturas implements Respuesta {

    String codigo;
    String mensaje;
    Collection<RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura> detalles;

    public static RespuestaCalculoNetoFacturas parse(String xmlSoap) throws Throwable {
        // Eliminar el namespace que no utiliza ningun prefijo.
        xmlSoap = Text.replaceAll(xmlSoap, "xmlns=\"http://tempuri.org/\"", "");

        // Cargar XML de respuesta SOAP.
        Document xml = Xml.parse(xmlSoap);

        // Obtener prefijos y uris.
        XmlPrefixInfoList prefixes = Xml.readPrefixes(xml);

        // Preparar consultas XPath.
        XPath xpath = Xml.xpath(prefixes);
        String xCodigo = ".//CodigoError";
        String xMensaje = ".//Mensaje";
        String xDetalles = ".//SimuladorCalculo[1]/DetalleDeCalculo[1]/SimuladorCalculoDetalle";
        String xDNroFactura = ".//NroFactura";
        String xDMonto = ".//Monto";
        String xDFechaVto = ".//FechaVto";
        String xDUsoPlataforma = ".//UsoPlataforma";
        String xDDiasInteres = ".//DiasDeInteres";
        String xDInteres = ".//Interes";
        String xDSeguro = ".//Seguro";
        String xDIvaSeguro = ".//IvaSeguro";
        String xDGastosAdm = ".//GastosAdm";
        String xDIvaGastosAdm = ".//IvaGastosAdm";
        String xDTasaAnualNominal = ".//TasaAnualNominal";
        String xDTasaAnualEfectiva = ".//TasaAnualEfectiva";
        String xDNeto = ".//Neto";
        String xDEstado = ".//Estado";
        String xDMensaje = ".//Mensaje";

        // Obtener valor.
        String codigo = Xml.readNodeTexContent(xpath, xCodigo, xml);
        String mensaje = Xml.readNodeTexContent(xpath, xMensaje, xml);
        NodeList detalles0 = Xml.readNodes(xpath, xDetalles, xml);
        Collection<RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura> detalles = Lists.empty();
        for (int i = 0; i < detalles0.getLength(); i++) {
            Node detalle0 = detalles0.item(i);
            DetalleCalculoNetoFactura detalle = new DetalleCalculoNetoFactura();
            detalle.estado = Xml.readNodeTexContent(xpath, xDEstado, detalle0);
            detalle.mensaje = Xml.readNodeTexContent(xpath, xDMensaje, detalle0);
            detalle.nroFactura = Xml.readNodeTexContent(xpath, xDNroFactura, detalle0);
            detalle.monto = Numeric.adaptAsBigDecimal(Xml.readNodeTexContent(xpath, xDMonto, detalle0));
            detalle.fechaVto = Fechas.parse(Xml.readNodeTexContent(xpath, xDFechaVto, detalle0), "yyyy-MM-dd'T'HH:mm:ss");
            detalle.usoPlataforma = Numeric.adaptAsBigDecimal(Xml.readNodeTexContent(xpath, xDUsoPlataforma, detalle0, "0"));
            detalle.diasInteres = Numeric.adaptAsInteger(Xml.readNodeTexContent(xpath, xDDiasInteres, detalle0, "0"));
            detalle.interes = Numeric.adaptAsBigDecimal(Xml.readNodeTexContent(xpath, xDInteres, detalle0, "0"));
            detalle.seguro = Numeric.adaptAsBigDecimal(Xml.readNodeTexContent(xpath, xDSeguro, detalle0, "0"));
            detalle.ivaSeguro = Numeric.adaptAsBigDecimal(Xml.readNodeTexContent(xpath, xDIvaSeguro, detalle0, "0"));
            detalle.gastosAdministrativos = Numeric.adaptAsBigDecimal(Xml.readNodeTexContent(xpath, xDGastosAdm, detalle0, "0"));
            detalle.ivaGastosAdministrativos = Numeric.adaptAsBigDecimal(Xml.readNodeTexContent(xpath, xDIvaGastosAdm, detalle0, "0"));
            detalle.tasaAnualNominal = Numeric.adaptAsBigDecimal(Xml.readNodeTexContent(xpath, xDTasaAnualNominal, detalle0, "0"));
            detalle.tasaAnualEfectiva = Numeric.adaptAsBigDecimal(Xml.readNodeTexContent(xpath, xDTasaAnualEfectiva, detalle0, "0"));
            detalle.neto = Numeric.adaptAsBigDecimal(Xml.readNodeTexContent(xpath, xDNeto, detalle0, "0"));
            detalles.add(detalle);
        }

        // Procesar valores.
        RespuestaCalculoNetoFacturas respuesta = new RespuestaCalculoNetoFacturas();
        respuesta.codigo = codigo;
        respuesta.mensaje = mensaje;
        respuesta.detalles = detalles;
        return respuesta;
    }

    public static RespuestaCalculoNetoFacturas instance(String codigo, String mensaje) {
        RespuestaCalculoNetoFacturas instance = new RespuestaCalculoNetoFacturas();
        instance.codigo = codigo;
        instance.mensaje = mensaje;
        return instance;
    }

    @Override
    public boolean isOk() {
        return Assertions.stringNullOrEmpty(codigo) == false && Objects.equals(codigo, "0");
    }

    @Data
    public static class DetalleCalculoNetoFactura {

        private String estado;
        private String mensaje;
        private String nroFactura;
        private BigDecimal monto;
        private Date fechaVto;
        private BigDecimal usoPlataforma;
        private Integer diasInteres;
        private BigDecimal interes;
        private BigDecimal seguro;
        private BigDecimal ivaSeguro;
        private BigDecimal gastosAdministrativos;
        private BigDecimal ivaGastosAdministrativos;
        private BigDecimal tasaAnualNominal;
        private BigDecimal tasaAnualEfectiva;
        private BigDecimal neto;
    }

}
