/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.criteria.query.Operators;
import fa.gs.criteria.query.Order;
import fa.gs.criteria.query.QueryCriteria;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.database.query.expressions.literals.CollectionLiteral;
import fa.gs.utils.database.query.expressions.literals.SQLStringLiterals;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.Ids;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.misc.text.Joiner;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.Estado;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaConformada;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaSeguimientoLiquidacion;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.entities.factoring.Lote;
import py.com.sepsa.genisys.ejb.entities.factoring.NotaCredito;
import py.com.sepsa.genisys.ejb.entities.factoring.Retencion;
import py.com.sepsa.genisys.ejb.entities.factoring.Transaccion;
import py.com.sepsa.genisys.ejb.entities.info.CuentaEntidadFinanciera;
import py.com.sepsa.genisys.ejb.entities.info.FacturaEstadoInfo;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.FacturaEstadoSeguimientoLiquidacionEnum;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;
import py.com.sepsa.genisys.ejb.utils.Text;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Facturas", mappedName = "Facturas")
@LocalBean
public class Facturas extends LogicBean {

    @EJB
    private JpaUtils jpaUtils;

    public Result<Lote> buscarLote(Integer idLote) {
        Result<Lote> result;

        try {
            Lote lote = beans.getFacades().getLoteFacade().findById(idLote);
            result = Results.ok().value(lote).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).build();
        }

        return result;
    }

    public Result<Lote> crearLoteFacturas() {
        Result<Lote> result;

        try {
            // Generar codigo de validacion de lote.
            String id = Ids.randomUuid();

            // Crear lote.
            Lote instance = new Lote();
            instance.setCodValidacion(id);
            instance = jpaUtils.create(instance);

            result = Results.ok().value(instance).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error creando nuevo lote.").build();
        }

        return result;
    }

    public Result<Void> agregarALoteFacturas(Lote lote, List<FacturaInfo> facturas) {
        Result<Void> result;

        try {
            if (!Assertions.isNullOrEmpty(facturas)) {
                for (FacturaInfo factura : facturas) {
                    String query = Strings.format(" update factoring.factura set id_lote = %s where id = %s ", lote.getId(), factura.getId());
                    int ret = jpaUtils.executeUpdate(query);
                    if (ret != 1) {
                        throw Errors.builder().message("No se pudo modificar factura").build();
                    }
                }
            }

            result = Results.ok()
                    .nullable(true)
                    .value(null)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error agregando facturas a lote.")
                    .tag("lote.id", lote.getId())
                    .build();
        }

        return result;
    }

    /**
     * Agrega un registro de historico de factura indicando un cambio de estado
     * sobre una factura.
     *
     * @param idUsuario Identificador del suario que efectua el cambio de estado
     * sobre la factura.
     * @param factura Factura que sufrio algun cambio.
     * @param estadoEnum Nuevo estado de la factura.
     * @param comentario Comentario opcional que describe el cambio de estado.
     * @return Resultado de la operacion.
     */
    public Result<HistoricoFactura> agregarHistorico(Integer idUsuario, Factura factura, Estados.EstadoEnum estadoEnum, String comentario) {
        Result<HistoricoFactura> result;

        try {
            // Crear registro historico.
            HistoricoFactura historico = new HistoricoFactura();
            historico.setComentario(comentario);
            historico.setIdEstado(beans.getEstados().find(estadoEnum));
            historico.setIdFactura(factura);
            historico.setIdUsuario(idUsuario);
            historico.setFecha(Fechas.ahora());
            beans.getFacades().getHistoricoFacturaFacade().create(historico);

            result = Results.ok().value(historico).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error agregando registro histórico para factura.").build();
        }
        return result;
    }

    public Result<List<FacturaInfo>> buscarFacturasEnLote(Integer idLote) {
        Result<List<FacturaInfo>> result;

        try {
            // Filtros.
            Map<String, Object> filterBy = Maps.empty();
            filterBy.put("a", Strings.format("f.id_lote = %s", idLote));

            // Obtener facturas.
            List<FacturaInfo> facturas = selectFacturas(null, null, filterBy, null);

            result = Results.ok()
                    .value(facturas)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo facturas")
                    .build();
        }

        return result;
    }

    /**
     * Permite obtener una factura mediante su identificador.
     *
     * @param idFactura Identificador de factura.
     * @return Resultado de la operacion.
     */
    @Deprecated
    private Result<Factura> buscarFactura0(Integer idFactura) {
        Result<Factura> result;
        try {
            QueryCriteria criteria = QueryCriteria.instance();
            criteria.where("id", Operators.EQUALS, idFactura);
            Factura factura = beans.getFacades().getFacturaFacade().findFirst(criteria);
            result = Results.ok().value(factura).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error obteniendo factura id='%s'", idFactura).build();
        }
        return result;
    }

    public Result<FacturaInfo> buscarFactura(Integer idFactura) {
        Result<FacturaInfo> result;

        try {
            Integer[] idFacturas = new Integer[]{idFactura};
            Result<List<FacturaInfo>> resFacturas = buscarFacturas(idFacturas);
            resFacturas.raise();

            Collection<FacturaInfo> facturas = resFacturas.value();
            FacturaInfo factura = Lists.first(facturas);

            result = Results.ok()
                    .value(factura)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo factura")
                    .tag("factura.id", idFactura)
                    .build();
        }

        return result;
    }

    public Result<List<FacturaInfo>> buscarFacturas(Integer[] idFacturas) {
        Result<List<FacturaInfo>> result;

        try {
            // Filtros.
            Map<String, Object> filterBy = Maps.empty();
            filterBy.put("a", Strings.format("f.id in %s", CollectionLiteral.instance(idFacturas).stringify(null)));

            // Obtener facturas.
            List<FacturaInfo> facturas = selectFacturas(null, null, filterBy, null);

            result = Results.ok()
                    .value(facturas)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo facturas")
                    .build();
        }

        return result;
    }

    public Result<FacturaInfo> buscarFactura(String nroFactura, String rucProveedor, String rucComprador, Date fechaEmision) {
        Result<FacturaInfo> result;

        try {
            // Obtener estado eliminado.
            Estado ELIMINADO = beans.getEstados().find(Estados.EstadoFacturaEnum.ELIMINADA);

            // Construir consulta.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     f.id as \"id\" ");
            builder.append(" from factoring.factura as f ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and f.nro_factura = '%s' ", nroFactura);
            builder.append("     and f.ruc_comprador = '%s' ", rucComprador);
            builder.append("     and f.ruc_proveedor = '%s' ", rucProveedor);
            builder.append("     and cast(f.fecha as date) = %s ", SQLStringLiterals.fecha(fechaEmision));
            builder.append("     and f.id_estado != %s ", ELIMINADO.getId());
            builder.append(" limit 1 ");
            builder.append(" ; ");

            // Ejecutar consulta.
            String query = builder.toString();
            Map<String, Object> row = jpaUtils.getFirst(query);
            if (Assertions.isNullOrEmpty(row)) {
                throw Errors.illegalState("No se pudo determinar la factura.");
            }

            // Extraer datos.
            Integer idFactura = Maps.integer(row, "id");
            return buscarFactura(idFactura);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error verificando datos de factura")
                    .build();
        }

        return result;
    }

    public Result<FacturaInfo> buscarFactura(Integer idComprador, String nroFactura, BigDecimal monto) {
        Result<FacturaInfo> result;

        try {
            // Obtener estado eliminado.
            Estado ELIMINADO = beans.getEstados().find(Estados.EstadoFacturaEnum.ELIMINADA);

            // Construir consulta.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     f.id as \"id\" ");
            builder.append(" from factoring.factura as f ");
            builder.append(" left join factoring.factura_conformada as fc on fc.id = f.id ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and f.id_persona_destino = %s ", idComprador);
            builder.append("     and f.nro_factura = '%s' ", nroFactura);
            builder.append("     and fc.monto_conformado = %s ", monto.toPlainString());
            builder.append("     and f.id_estado != %s ", ELIMINADO.getId());
            builder.append(" limit 1 ");
            builder.append(" ; ");

            // Ejecutar consulta.
            String query = builder.toString();
            Map<String, Object> row = jpaUtils.getFirst(query);
            if (Assertions.isNullOrEmpty(row)) {
                throw Errors.illegalState("No se pudo determinar la factura.");
            }

            // Extraer datos.
            Integer idFactura = Maps.integer(row, "id");
            return buscarFactura(idFactura);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error verificando datos de factura")
                    .build();
        }

        return result;
    }

    public boolean facturaExiste(String nroFactura, String rucProveedor, String rucComprador, Date fechaEmision, Date fechaVencimiento, BigDecimal monto, Integer idMoneda) {
        try {
            // Obtener estado eliminado.
            Estado ELIMINADO = beans.getEstados().find(Estados.EstadoFacturaEnum.ELIMINADA);

            // Construir consulta.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     f.id ");
            builder.append(" from factoring.factura as f ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and f.nro_factura = '%s' ", nroFactura);
            builder.append("     and f.ruc_comprador = '%s' ", rucComprador);
            builder.append("     and f.ruc_proveedor = '%s' ", rucProveedor);
            builder.append("     and cast(f.fecha as date) = %s ", SQLStringLiterals.fecha(fechaEmision));
            builder.append("     and cast(f.fecha_pago as date) = %s ", SQLStringLiterals.fecha(fechaVencimiento));
            builder.append("     and f.monto_total_factura = %s ", monto);
            builder.append("     and f.id_moneda = %s ", idMoneda);
            builder.append("     and f.id_estado != %s ", ELIMINADO.getId());
            builder.append(" limit 1 ");
            builder.append(" ; ");

            // Ejecutar consulta.
            String query = builder.toString();
            Map<String, Object> rows = jpaUtils.getFirst(query);
            return (rows != null && rows.isEmpty() == false);
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error verificando si factura existe");
            return true;
        }
    }

    /**
     * Permite obtener un registro de conformacion/venta asociado a una factura
     * dada.
     *
     * @param idFactura Identificador de factura.
     * @return Resultado de la operacion.
     */
    public Result<FacturaConformada> buscarFacturaConformada(Integer idFactura) {
        Result<FacturaConformada> result;
        try {
            FacturaConformada facturaConformada = beans.getFacades().getFacturaConformadaFacade().findByIdFactura(idFactura);
            result = Results.ok().value(facturaConformada).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error obteniendo factura conformada id='%s'", idFactura).build();
        }
        return result;
    }

    /**
     * Permite obtener un registro de nota de credito asociado a una factura
     * dada.
     *
     * @param idFactura Identificador de factura.
     * @return Resultado de la operacion.
     */
    public Result<NotaCredito> buscarNotaCredito(Integer idFactura) {
        Result<NotaCredito> result;
        try {
            NotaCredito notaCredito = beans.getFacades().getNotaCreditoFacade().findByIdFactura(idFactura);
            result = Results.ok().value(notaCredito).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error obteniendo nota de credito para factura id='%s'", idFactura).build();
        }
        return result;
    }

    /**
     * Permite obtener un registro de retencion asociado a una factura dada.
     *
     * @param idFactura Identificador de factura.
     * @return Resultado de la operacion.
     */
    public Result<Retencion> buscarRetencion(Integer idFactura) {
        Result<Retencion> result;
        try {
            Retencion retencion = beans.getFacades().getRetencionFacade().findByIdFactura(idFactura);
            result = Results.ok().value(retencion).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error obteniendo retencion para factura id='%s'", idFactura).build();
        }
        return result;
    }

    /**
     * Crea un registro de transaccion que describe el movimiento de dinero
     * realizado entre dos cuentas involucradas en el desembolso de una factura
     * en particular.
     *
     * @param fecha Fecha de la operacion.
     * @param origen Cuenta de origen del desembolso.
     * @param destino Cuenta de destino del desembolso.
     * @param factura Factura a desembolsar.
     * @param monto Monto desembolsado.
     * @return
     */
    public Result<Transaccion> registrarTransaccionDesembolso(Date fecha, CuentaEntidadFinanciera origen, CuentaEntidadFinanciera destino, FacturaInfo factura, BigDecimal monto) {
        Result<Transaccion> result;
        try {
            // Registrar transaccion de desembolso.
            // TODO: CORREGIR.
            Transaccion tx = new Transaccion();
            tx.setFecha(fecha);
            tx.setIdCuentaOri(origen.getIdCuenta());
            tx.setIdCuentaDest(destino.getIdCuenta());
            tx.setIdDocumento(null);
            tx.setIdTipoDocumento(null);
            tx.setIdFacturaConformada(factura.getId());
            tx.setMonto(monto);
            tx.setPorcentaje(Numeric.wrap(-1));
            tx.setEstado('A');
            beans.getFacades().getTransaccionFacade().create(tx);

            result = Results.ok().value(tx).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr)
                    .message("Ocurrió un error registrando transacción de desembolso")
                    .tag("factura.id", factura.getId())
                    .tag("cuenta.origen", origen.getIdCuenta())
                    .tag("cuenta.destino", destino.getIdCuenta())
                    .build();
        }

        return result;
    }

    /**
     * Permite registrar una factura.
     *
     * @param idUsuario Identificador de usuario que registra la factura.
     * @param factura Factura.
     * @return Resultado de la operacion.
     */
    public Result<Factura> registrarFactura(Integer idUsuario, Factura factura) {
        Result<Factura> result;
        try {
            // Persistir factura.
            beans.getFacades().getFacturaFacade().create(factura);

            // Agregar registro historico.
            HistoricoFactura historico = new HistoricoFactura();
            historico.setIdUsuario(idUsuario);
            historico.setIdEstado(factura.getIdEstado());
            historico.setIdFactura(factura);
            historico.setFecha(Fechas.ahora());
            historico.setComentario("Factura ingresada manualmente");
            beans.getFacades().getHistoricoFacturaFacade().create(historico);

            result = Results.ok().value(factura).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error persistiendo factura").build();
        }
        return result;
    }

    /**
     * Permite registrar una factura.
     *
     * @param idUsuario Identificador de usuario que registra la factura.
     * @param comprador Comprador al que es emitida la factura.
     * @param proveedor Proveedor que emite la factura.
     * @param nroFactura Numero de comprobante de factura.
     * @param fechaEmision Fecha de emision de factura.
     * @param fechaPago Fecha de vencimiento/pago de factura.
     * @param montoTotal Monto total de factura.
     * @param moneda Moneda en la que se expresa el monto de factura.
     * @return Resultado de la operacion.
     */
    public Result<Factura> registrarFactura(Integer idUsuario, Persona comprador, Persona proveedor, String nroFactura, Date fechaEmision, Date fechaPago, BigDecimal montoTotal, Moneda moneda) {
        Result<Factura> result;

        try {
            // Nuevo objeto Factura
            Factura factura = new Factura();

            // Datos seteados desde el input para una nueva factura.
            factura.setIdEstado(beans.getEstados().find(Estados.EstadoFacturaEnum.NO_CONFORMADA));
            factura.setNroFactura(nroFactura);
            factura.setIdPersonaOrigen(proveedor.getId());
            factura.setRazonSocialProveedor(Text.razonSocial(proveedor));
            factura.setRucProveedor(Text.ruc(proveedor));
            factura.setIdPersonaDestino(comprador.getId());
            factura.setRazonSocialComprador(Text.razonSocial(comprador));
            factura.setRucComprador(Text.ruc(comprador));
            factura.setFecha(fechaEmision);
            factura.setFechaPago(fechaPago);
            factura.setDiasCredito(Fechas.diasDiferencia(fechaEmision, fechaPago));
            factura.setNroTimbrado("");
            factura.setVtoTimbrado("");
            factura.setIdMoneda(moneda.getId());
            factura.setMontoTotal5(Numeric.CERO);
            factura.setMontoTotal10(Numeric.CERO);
            factura.setMontoTotalExento(Numeric.CERO);
            factura.setMontoIva5(Numeric.CERO);
            factura.setMontoImponible5(Numeric.CERO);
            factura.setMontoIva10(Numeric.CERO);
            factura.setMontoImponible10(Numeric.CERO);
            factura.setMontoIvaTotal(Numeric.CERO);
            factura.setMontoImponibleTotal(Numeric.CERO);
            factura.setMontoTotalFactura(montoTotal);

            // Persistir factura.
            beans.getFacades().getFacturaFacade().create(factura);
            result = Results.ok().value(factura).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error persistiendo factura").build();
        }
        return result;
    }

    /**
     * Permite la conformacion de una factura.
     *
     * @param idFactura Identificador de factura.
     * @param montoTotalFactura Monto inicial de factura.
     * @param montoNotaCredito Monto de notas de credito, si hubieren.
     * @param montoRetencion Monto de retenciones, si hubieren.
     * @param montoOtros Monto de otros gatos, si hubieren.
     * @param montoTotalConformado Monto total conformado.
     * @return Factura conformada.
     */
    public Result<FacturaConformada> registrarFacturaConformada(Integer idFactura, BigDecimal montoTotalFactura, BigDecimal montoNotaCredito, BigDecimal montoRetencion, BigDecimal montoOtros, BigDecimal montoTotalConformado) {
        Result<FacturaConformada> result;

        try {
            // Establecer datos iniciales de factura conformada.
            FacturaConformada instance = new FacturaConformada();
            instance.setId(idFactura);
            instance.setMontoFactura(montoTotalFactura);
            instance.setMontoNotaCredito(montoNotaCredito);
            instance.setMontoRetencion(montoRetencion);
            instance.setMontoOtrosCargos(montoOtros);
            instance.setMontoConformado(montoTotalConformado);

            // Inicializar campos relacionados a la venta de una factura, pero que no son utilizados durante la conformacio.
            instance.setPorcentajeComision(Numeric.CERO);
            instance.setMontoImponibleComision(Numeric.CERO);
            instance.setMontoImpuestoComision(Numeric.CERO);
            instance.setMontoTotalComision(Numeric.CERO);
            instance.setMontoImponibleGastosOperativos(Numeric.CERO);
            instance.setMontoImpuestoGastosOperativos(Numeric.CERO);
            instance.setMontoTotalGastosOperativos(Numeric.CERO);
            instance.setMontoNeto(Numeric.CERO);

            instance = jpaUtils.create(instance);
            result = Results.ok()
                    .value(instance)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error conformando factura")
                    .tag("factura.id", idFactura)
                    .build();
        }

        return result;
    }

    /**
     * Realiza el cambio de estado para una factura. Al mismo tiempo, registra
     * este cambio de estado dentro del historico de facturas.
     *
     * @param idFactura Identificador de factura.
     * @param nuevoEstado Enumeracion de nuevo estado para factura.
     * @param idUsuario Identificador de usuario que realiza el cambio.
     * @return Resultado de la operacion.
     */
    public Result<Factura> cambiarEstado(Integer idUsuario, Integer idFactura, Estados.EstadoFacturaEnum nuevoEstado) {
        Result<Factura> result;

        try {
            Result<Estado> resEstado = beans.getEstados().buscarEstado(nuevoEstado);
            resEstado.raise();

            Result<Factura> resFactura = buscarFactura0(idFactura);
            resFactura.raise();

            Estado estado = resEstado.value();
            Factura factura = resFactura.value();
            factura.setIdEstado(estado);
            beans.getFacades().getFacturaFacade().edit(factura);

            result = Results.ok().value(factura).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr)
                    .message("Ocurrió un error cambiando estado de factura id='%s' a '%s'", idFactura, nuevoEstado.getDescripcion())
                    .build();
        }

        return result;
    }

    /**
     * Obtiene informacion relativa al estado actual de una factura.
     *
     * @param idFactura Identificador de factura.
     * @return Resultado de operacion.
     */
    public Result<FacturaEstadoInfo> obtenerEstado(Integer idFactura) {
        Result<FacturaEstadoInfo> result;

        try {
            // Construir consulta.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("   f.id as \"facturaId\", ");
            builder.append("   f.nro_factura as \"facturaNumero\", ");
            builder.append("   f.fecha as \"facturaFechaEmision\", ");
            builder.append("   f.ruc_proveedor as \"proveedorRuc\", ");
            builder.append("   e.descripcion as \"estadoDescripcion\", ");
            builder.append("   hf.fecha as \"estadoFechaCambio\", ");
            builder.append("   hf.comentario as \"observaciones\" ");
            builder.append(" from factoring.factura as f  ");
            builder.append(" left join factoring.historico_factura as hf on f.id = hf.id_factura and f.id_estado = hf.id_estado ");
            builder.append(" left join factoring.estado as e on f.id_estado = e.id ");
            builder.append(" where ");
            builder.append("   1 = 1 ");
            builder.append("   and f.id = %s ", idFactura);
            builder.append(" limit 1 ");
            builder.append(" ; ");

            // Ejecutar consulta.
            String query = builder.toString();
            Map<String, Object> row = jpaUtils.getFirst(query);
            if (Assertions.isNullOrEmpty(row)) {
                throw Errors.illegalState("Factura no cuenta con ningun estado.");
            }

            // Preparar datos de salida.
            FacturaEstadoInfo info = FacturaEstadoInfo.instance(row);
            result = Results.ok()
                    .value(info)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr)
                    .message("Ocurrió un error obteniendo estado de factura")
                    .build();
        }

        return result;
    }

    /**
     * Verifica que una factura presente al menos uno de los estados
     * especificados.
     *
     * @param factura Factura.
     * @param estados Estados a validar
     * @return {@code true} si y solo si la factura tiene al menos uno de los
     * estados. Caso contrario, {@code false}.
     */
    public boolean checkFacturaEnEstado(FacturaInfo factura, Estados.EstadoFacturaEnum... estados) {
        for (Estados.EstadoFacturaEnum estado : estados) {
            if (factura.getEstadoId() != null && Objects.equals(factura.getEstadoId(), estado.getId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica que una coleccion de facturas presenten el mismo estado.
     *
     * @param facturas Lista de facturas.
     * @param estados Estados a validar
     * @return {@code true} si las facturas estan en el mismo estado. Caso
     * contrario, {@code false}.
     */
    public boolean checkFacturasEnEstado(List<FacturaInfo> facturas, Estados.EstadoFacturaEnum... estados) {
        for (FacturaInfo factura : facturas) {
            if (!checkFacturaEnEstado(factura, estados)) {
                return false;
            }
        }
        return true;
    }

    public List<FacturaInfo> selectFacturas(Integer limit, Integer offset, Map<String, Object> filterBy, Map<String, Order> orderBy) {
        List<FacturaInfo> facturas = Lists.empty();

        try {
            // Ejecutar query.
            String query = queryFacturas(false, limit, offset, filterBy, orderBy);
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Agregar registros.
            if (!Assertions.isNullOrEmpty(rows)) {
                rows.stream()
                        .map(FacturaInfo::instance)
                        .forEach(facturas::add);
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando facturas");
        }

        return facturas;
    }

    public Integer countFacturas(Map<String, Object> filterBy) {
        try {
            // Ejecutar query.
            String query = queryFacturas(true, null, null, filterBy, null);
            Map<String, Object> row = jpaUtils.getFirst(query);

            BigInteger count = Maps.biginteger(row, "total");
            return count.intValue();
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando facturas");
            return -1;
        }
    }

    private String queryFacturas(boolean forCount, Integer limit, Integer offset, Map<String, Object> filterBy, Map<String, Order> orderBy) {
        // Construir query.
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");

        // Proyecciones.
        if (forCount) {
            builder.append("   cast(count(*) as int8) as \"total\" ");
        } else {
            builder.append("   f.id as \"id\", ");
            builder.append("   f.id_lote as \"loteId\", ");
            builder.append("   p1.id as \"compradorId\", ");
            builder.append("   coalesce(f.razon_social_comprador, pj1.razon_social, pf1.nombre || ' ' || pf1.apellido) as \"compradorRazonSocial\", ");
            builder.append("   coalesce(f.ruc_comprador, p1.ruc || '-' || p1.dv_ruc) as \"compradorRuc\", ");
            builder.append("   p0.id as \"proveedorId\", ");
            builder.append("   coalesce(f.razon_social_proveedor, pj0.razon_social, pf0.nombre || ' ' || pf0.apellido) as \"proveedorRazonSocial\", ");
            builder.append("   coalesce(f.ruc_proveedor, p0.ruc || '-' || p0.dv_ruc) as \"proveedorRuc\", ");
            builder.append("   p2.id as \"entidadFinancieraId\", ");
            builder.append("   coalesce(pj2.razon_social, pf2.nombre || ' ' || pf2.apellido) as \"entidadFinancieraRazonSocial\", ");
            builder.append("   (p2.ruc || '-' || p2.dv_ruc) as \"entidadFinancieraRuc\", ");
            builder.append("   f.nro_factura as \"numero\", ");
            builder.append("   f.nro_timbrado as \"timbradoNumero\", ");
            builder.append("   f.vto_timbrado as \"timbradoVencimiento\", ");
            builder.append("   f.id_documento as \"documentoId\", ");
            builder.append("   coalesce(f.id_tipo_documento, 5) as \"documentoTipoId\", ");
            builder.append("   f.fecha as \"fechaEmision\", ");
            builder.append("   f.fecha_pago as \"fechaPago\", ");
            builder.append("   f.id_moneda as \"monedaId\", ");
            builder.append("   f.monto_total_5 as \"montoTotalIva5\", ");
            builder.append("   f.monto_total_10 as \"montoTotalIva10\", ");
            builder.append("   f.monto_iva_total as \"montoTotalIva\", ");
            builder.append("   f.monto_total_exento as \"montoTotalExento\", ");
            builder.append("   f.monto_imponible_total as \"montoTotalImponible\", ");
            builder.append("   f.monto_total_factura as \"montoTotal\", ");
            builder.append("   fc.monto_conformado as \"montoTotalConformado\", ");
            builder.append("   fc.monto_total_comision as \"montoTotalDescuento\", ");
            builder.append("   fc.porcentaje_comision as \"porcentajeDescuento\", ");
            builder.append("   (f.monto_total_factura - fc.monto_total_comision) as \"montoTotalDesembolso\", ");
            builder.append("   fc.monto_distribucion as \"montoTotalDistribucion\", ");
            builder.append("   f.dias_credito as \"diasPlazo\", ");
            builder.append("   fc.dias_anticipo as \"diasAnticipo\", ");
            builder.append("   f.id_documento is not null as \"esFacturaEdi\", ");
            builder.append("   e.id as \"estadoId\", ");
            builder.append("   e.descripcion as \"estadoDescripcion\", ");
            builder.append("   c.id as \"convenioId\", ");
            builder.append("   tc.descripcion as \"convenioDescripcion\" ");
        }

        // Origen de datos.
        builder.append(" from factoring.factura as f ");
        builder.append(" left join factoring.factura_conformada as fc on fc.id = f.id ");
        builder.append(" left join info.persona as p0 on p0.id = f.id_persona_origen ");
        builder.append(" left join info.persona_fisica as pf0 on pf0.id = f.id_persona_origen ");
        builder.append(" left join info.persona_juridica as pj0 on pj0.id = f.id_persona_origen  ");
        builder.append(" left join info.persona as p1 on p1.id = f.id_persona_destino  ");
        builder.append(" left join info.persona_fisica as pf1 on pf1.id = f.id_persona_destino ");
        builder.append(" left join info.persona_juridica as pj1 on pj1.id = f.id_persona_destino ");
        builder.append(" left join factoring.estado as e on f.id_estado = e.id ");
        builder.append(" left join factoring.convenio as c on fc.id_convenio = c.id ");
        builder.append(" left join factoring.tipo_convenio as tc on tc.id = c.id_tipo_convenio ");
        builder.append(" left join info.persona as p2 on p2.id = fc.id_entidad_financiera ");
        builder.append(" left join info.persona_fisica as pf2 on pf2.id = fc.id_entidad_financiera ");
        builder.append(" left join info.persona_juridica as pj2 on pj2.id = fc.id_entidad_financiera ");

        // Filtros.
        builder.append(" where ");
        builder.append("     1 = 1 ");
        if (!Assertions.isNullOrEmpty(filterBy)) {
            filterBy.entrySet().stream().forEach(e -> {
                String v = String.valueOf(e.getValue());
                if (!Assertions.stringNullOrEmpty(v)) {
                    builder.append(" and %s ", v);
                }
            });
        }

        // Ordenacion.
        if (forCount == false) {
            if (!Assertions.isNullOrEmpty(orderBy)) {
                String[] clauses = orderBy.entrySet().stream()
                        .map(e -> Strings.format("%s %s", e.getKey(), e.getValue()))
                        .distinct()
                        .toArray(String[]::new);
                String clause = Joiner.of(clauses).separator(",").join();

                builder.append(" order by ");
                builder.append(" %s ", clause);
            }
        }

        // Paginacion.
        if (forCount == false) {
            if (limit != null) {
                builder.append(" limit %s ", limit);
            }
            if (offset != null) {
                builder.append(" offset %s ", offset);
            }
        }

        builder.append(" ; ");

        return builder.toString();
    }

    /**
     * Agrupa una lista de facturas por el identificador del comprador.
     *
     * @param facturas Lista de facturas.
     * @return Grupos de facturas.
     */
    public Map<Integer, List<FacturaInfo>> agruparPorComprador(List<FacturaInfo> facturas) {
        Map<Integer, List<FacturaInfo>> map = Maps.empty();
        for (FacturaInfo factura : facturas) {
            if (!map.containsKey(factura.getCompradorId())) {
                map.put(factura.getCompradorId(), fa.gs.utils.collections.Lists.empty());
            }
            map.get(factura.getCompradorId()).add(factura);
        }
        return map;
    }

    /**
     * Agrupa una lista de facturas por el identificador de la entidad
     * financiera a la que fueron vendidas.
     *
     * @param facturas Lista de facturas.
     * @return Grupos de facturas.
     */
    public Map<Integer, List<FacturaInfo>> agruparPorEntidadFinanciera(List<FacturaInfo> facturas) {
        Map<Integer, List<FacturaInfo>> map = Maps.empty();
        for (FacturaInfo factura : facturas) {
            if (factura.getEntidadFinancieraId() != null) {
                if (!map.containsKey(factura.getEntidadFinancieraId())) {
                    map.put(factura.getEntidadFinancieraId(), fa.gs.utils.collections.Lists.empty());
                }
                map.get(factura.getEntidadFinancieraId()).add(factura);
            }
        }
        return map;
    }

    /**
     * Agrupa una lista de facturas por el identificador de lote en el que se
     * incluyen.
     *
     * @param facturas Lista de facturas.
     * @return Grupos de facturas.
     */
    public Map<Integer, List<FacturaInfo>> agruparPorLote(List<FacturaInfo> facturas) {
        Map<Integer, List<FacturaInfo>> map = Maps.empty();
        for (FacturaInfo factura : facturas) {
            if (factura.getIdLote() != null) {
                if (!map.containsKey(factura.getIdLote())) {
                    map.put(factura.getIdLote(), fa.gs.utils.collections.Lists.empty());
                }
                map.get(factura.getIdLote()).add(factura);
            }
        }
        return map;
    }

    /**
     * Obtiene los identificadores de facturas cuyo cambio de estado se haya
     * realizado el dia de hoy.
     *
     * @param idEstado Identificador del estado de las facturas
     * @return Lista de identificadores de facturas
     */
    public Result<List<Integer>> obtenerIdFacturasCambioEstadoHoy(Integer idEstado) {
        Date hoy = fa.gs.misc.fechas.Fechas.now();
        Date fechaDesde = fa.gs.misc.fechas.Fechas.inicioDia(hoy);
        Date fechaHasta = fa.gs.misc.fechas.Fechas.finDia(hoy);
        return obtenerIdFacturasCambioEstado(idEstado, fechaDesde, fechaHasta);
    }

    /**
     * Obtiene los identificadores de facturas cuyo cambio de estado se haya
     * realizado en el rango de fechas especificado.
     *
     * @param idEstado Identificador de estado.
     * @param fechaDesde Fecha desde.
     * @param fechaHasta Fecha hasta.
     * @return Lista de identificadores de facturas.
     */
    public Result<List<Integer>> obtenerIdFacturasCambioEstado(Integer idEstado, Date fechaDesde, Date fechaHasta) {
        Result<List<Integer>> result;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sssz");
        try {
            // Construir consulta.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("   hf.id_factura as \"facturaId\" ");
            builder.append(" from factoring.historico_factura as hf  ");
            builder.append(" where ");
            builder.append("   1 = 1 ");
            builder.append("   and hf.id_estado = %s ", idEstado);
            builder.append("   and hf.fecha >= '%s' ", sdf.format(fechaDesde));
            builder.append("   and hf.fecha <= '%s' ", sdf.format(fechaHasta));
            builder.append(" ; ");

            // Ejecutar consulta.
            String query = builder.toString();
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Preparar datos de salida.
            List<Integer> listaIds = Lists.empty();
            rows.forEach(row -> {
                listaIds.add(Maps.integer(row, "facturaId"));
            });
            result = Results.ok()
                    .value(listaIds)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo identificadores de factura pendientes")
                    .build();
        }

        return result;
    }

    public Result<Integer> obtenerIdEntidadFinancieraVenta(Integer idFactura) {
        try {
            // Construir query.
            String query = " select fc.id_entidad_financiera as \"id\" from factoring.factura_conformada as fc where id = %s ";
            query = Strings.format(query, idFactura);
            Map<String, Object> row = jpaUtils.getFirst(query);

            // Control.
            if (row == null) {
                throw Errors.illegalState("Factura no está conformada");
            }

            Integer id = Maps.integer(row, "id");
            return Results.ok()
                    .value(id)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo identificador de entidad financiera")
                    .build();
        }
    }

    public Result<FacturaSeguimientoLiquidacion> registrarFacturaSeguimientoLiquidacion(Integer idFactura) {
        Result<FacturaSeguimientoLiquidacion> result;
        try {
            FacturaSeguimientoLiquidacion instance = new FacturaSeguimientoLiquidacion();
            instance.setIdFactura(idFactura);
            instance.setEstado(FacturaEstadoSeguimientoLiquidacionEnum.PENDIENTE.codigo());

            instance = jpaUtils.create(instance);

            result = Results.ok()
                    .value(instance)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr)
                    .message("Ocurrió un error registrando factura seguimiento liquidacion")
                    .build();
        }
        return result;
    }

    public Result<Boolean> existeNroFacturaEnFacturaSeguimientoLiquidacion(Integer idFactura) {
        Result<Boolean> result;

        try {
            // Construir consulta.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     cast(count(*) as int8) as \"total\" ");
            builder.append(" from factoring.factura_seguimiento_liquidacion as fsl  ");
            builder.append(" where ");
            builder.append("   1 = 1 ");
            builder.append("   and fsl.id_factura = %s ", idFactura);
            builder.append(" ; ");

            // Ejecutar consulta.
            String query = builder.toString();
            Integer total = jpaUtils.getTotal(query);

            result = Results.ok()
                    .value(total > 0)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr)
                    .message("Ocurrió un error obteniendo facturas en seguimiento registradas")
                    .build();
        }

        return result;
    }

    /**
     * Obtener identificador de facturas de hoy con el estado especificado
     *
     * @return Lista de identificadores de facturas
     */
    public Result<List<Integer>> obtenerIdFacturasPendientesALiquidar() {
        Result<List<Integer>> result;
        try {
            // Construir consulta.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("   fsl.id_factura as \"facturaId\" ");
            builder.append(" from factoring.factura_seguimiento_liquidacion as fsl  ");
            builder.append(" where ");
            builder.append("   1 = 1 ");
            builder.append("   and fsl.estado = '%s' ", FacturaEstadoSeguimientoLiquidacionEnum.PENDIENTE.codigo());
            builder.append(" ; ");

            // Ejecutar consulta.
            String query = builder.toString();
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Preparar datos de salida.
            List<Integer> listaIds = Lists.empty();
            rows.forEach(row -> {
                listaIds.add(Maps.integer(row, "facturaId"));
            });
            result = Results.ok()
                    .value(listaIds)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr)
                    .message("Ocurrió un error obteniendo identificadores de facturas pendientes")
                    .build();
        }

        return result;
    }

    public Result<List<FacturaInfo>> obtenerFacturasConformadasVigentes(Integer idProveedor) {
        Result<List<FacturaInfo>> result;

        try {
            // Obtener estado conformado.
            Estado CONFORMADA = beans.getEstados().find(Estados.EstadoFacturaEnum.CONFORMADA);

            // Fecha de hoy.
            Date hoy = fa.gs.misc.fechas.Fechas.inicioDia(new Date());

            // Construir consulta.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     f.id as \"id\", ");
            builder.append("     p0.id as \"proveedorId\", ");
            builder.append("     fc.monto_conformado as \"montoTotalConformado\" ");
            builder.append(" from factoring.factura as f ");
            builder.append(" left join info.persona as p0 on p0.id = f.id_persona_origen ");
            builder.append(" left join factoring.factura_conformada as fc on fc.id = f.id ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and f.id_persona_origen = %s ", idProveedor);
            builder.append("     and f.id_estado = %s ", CONFORMADA.getId());
            builder.append("     and fecha_pago >= %s", SQLStringLiterals.fecha(hoy));
            builder.append(" ; ");

            // Ejecutar consulta.
            String query = builder.toString();
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Agregar registros.
            List<FacturaInfo> facturas = Lists.empty();
            if (!Assertions.isNullOrEmpty(rows)) {
                rows.stream()
                        .map(FacturaInfo::instance)
                        .forEach(facturas::add);
            }
            result = Results.ok()
                    .value(facturas)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error verificando datos de factura")
                    .build();
        }

        return result;
    }

    public Result<List<FacturaInfo>> obtenerFacturasPorVencer(Integer idComprador, Integer diasAnticipo) {
        Result<List<FacturaInfo>> result;

        Date fechaAnticipo = fa.gs.utils.misc.fechas.Fechas.addDias(Fechas.ahora(), diasAnticipo);
        Date hoy = fa.gs.utils.misc.fechas.Fechas.inicioDia(Fechas.ahora());

        try {
            // Obtener estado conformado.
            Estado PAGADA = beans.getEstados().find(Estados.EstadoFacturaEnum.PAGADA);

            // Construir consulta.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     f.id as \"id\", ");
            builder.append("     p0.id as \"proveedorId\", ");
            builder.append("     fc.monto_conformado as \"montoTotalConformado\" ");
            builder.append(" from factoring.factura as f ");
            builder.append(" left join info.persona as p0 on p0.id = f.id_persona_origen ");
            builder.append(" left join factoring.factura_conformada as fc on fc.id = f.id ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and f.id_persona_destino = %s ", idComprador);
            builder.append("     and f.id_estado != %s ", PAGADA.getId());
            builder.append("     and f.fecha_pago >= '%s'", Fechas.format(hoy, "yyyy-MM-dd HH:mm:ss.SSSZ"));
            builder.append("     and f.fecha_pago < '%s'", Fechas.format(fechaAnticipo, "yyyy-MM-dd HH:mm:ss.SSSZ"));
            builder.append(" ; ");

            // Ejecutar consulta.
            String query = builder.toString();
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Agregar registros.
            List<FacturaInfo> facturas = Lists.empty();
            if (!Assertions.isNullOrEmpty(rows)) {
                rows.stream()
                        .map(FacturaInfo::instance)
                        .forEach(facturas::add);
            }
            result = Results.ok()
                    .value(facturas)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error verificando datos de factura")
                    .build();
        }

        return result;
    }

}
