/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.info;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.info.ContactoEmail;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author descauriza
 */
@Stateless(name = "ContactoEmailFacade", mappedName = "ContactoEmailFacade")
@LocalBean
public class ContactoEmailFacade extends AbstractFacade<ContactoEmail> {

    public ContactoEmailFacade() {
        super(ContactoEmail.class);
    }

}
