/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.security;

import fa.gs.misc.collections.Lists;
import fa.gs.utils.authentication.user.BaseAuthenticationInfo;
import fa.gs.utils.authentication.user.PermisoInfo;
import fa.gs.utils.authentication.user.UsuarioInfo;
import java.util.Collection;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class UserAuthenticationInfo extends BaseAuthenticationInfo {

    private Usuario mUsuario;

    private Collection<Perfil> mPerfiles;

    public void refresh() {
        setUsuario(toUsuarioInfo(mUsuario));
        setPermisos(toPermisoInfo(mPerfiles));
    }

    private UsuarioInfo toUsuarioInfo(Usuario usuario) {
        return new UsuarioInfo0(usuario);
    }

    private Collection<PermisoInfo> toPermisoInfo(Collection<Perfil> perfiles) {
        Collection<PermisoInfo> permisos = Lists.empty();
        for (Perfil perfil : perfiles) {
            PermisoInfo permiso = new PerfilInfo0(perfil);
            permisos.add(permiso);
        }
        return permisos;
    }

    private static class UsuarioInfo0 implements UsuarioInfo {

        private final Usuario usuario;

        UsuarioInfo0(Usuario usuario) {
            this.usuario = usuario;
        }

        @Override
        public Number id() {
            return usuario.getId();
        }

        @Override
        public String username() {
            return usuario.getUsuario();
        }

        @Override
        public String password() {
            return usuario.getContrasena();
        }

    }

    private static class PerfilInfo0 implements PermisoInfo {

        private final Perfil perfil;

        PerfilInfo0(Perfil perfil) {
            this.perfil = perfil;
        }

        @Override
        public Number id() {
            return perfil.getId();
        }

        @Override
        public String name() {
            return perfil.getDescripcion();
        }

    }

}
