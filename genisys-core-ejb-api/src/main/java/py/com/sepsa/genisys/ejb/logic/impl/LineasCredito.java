/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.criteria.query.Operators;
import fa.gs.criteria.query.Order;
import fa.gs.criteria.query.QueryCriteria;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.misc.numeric.Numeric;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.OptimisticLockException;
import py.com.sepsa.genisys.ejb.entities.factoring.LineaCredito;
import py.com.sepsa.genisys.ejb.entities.factoring.LineaCreditoHistorico;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.Errnos;
import py.com.sepsa.genisys.ejb.utils.Fechas;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "LineasCredito", mappedName = "LineasCredito")
@LocalBean
public class LineasCredito extends LogicBean {

    /**
     * Permite obtener el registro de la línea de crédito de una empresa
     * (persona) con una entidad financiera dada.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @param idPersona Identificador de persona (empresa).
     * @return Resultado de la operacion.
     */
    public Result<LineaCredito> buscarLineaCredito(Integer idEntidadFinanciera, Integer idPersona) {
        Result<LineaCredito> result;
        try {
            QueryCriteria criteria = QueryCriteria.instance();
            criteria.where("idEntidadFinanciera", Operators.EQUALS, idEntidadFinanciera);
            criteria.where("idPersona", Operators.EQUALS, idPersona);
            LineaCredito linea = beans.getFacades().getLineaCreditoFacade().findFirst(criteria);
            result = Results.ok().value(linea).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error obteniendo línea de crédito de persona id='%s' con entidad id='%s'", idPersona, idEntidadFinanciera).build();
        }

        return result;
    }

    /**
     * Permite obtener los movimientos historicos de una linea de credito
     * ordenados descendentemente.
     *
     * @param idLineaCredito Identificador de linea de credito.
     * @return Resultado de la operacion.
     */
    public Result<Collection<LineaCreditoHistorico>> buscarHistoricoLineaCredito(Integer idLineaCredito) {
        Result<Collection<LineaCreditoHistorico>> result;
        try {
            QueryCriteria criteria = QueryCriteria.instance();
            criteria.where("idLineaCredito.id", Operators.EQUALS, idLineaCredito);
            criteria.order("fecha", Order.DESCENDING);
            Collection<LineaCreditoHistorico> historico = beans.getFacades().getLineaCreditoHistoricoFacade().find(criteria);
            result = Results.ok().value(historico).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error obteniendo histórico de la línea de crédito id='%s'", idLineaCredito).build();
        }
        return result;
    }

    /**
     * Registra una nueva linea de credito para un cliente dado.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @param idPersona Identificador de cliente al que se asocia la linea.
     * @param idUsuario Usuario que realiza la operacion de registro de linea.
     * @param idMoneda Identificador de moneda.
     * @param monto Monto tope, inicial, de la linea de credito.
     * @return Resultado de la operacion.
     */
    public Result<LineaCredito> registrarLineaCredito(Integer idEntidadFinanciera, Integer idPersona, Integer idUsuario, Integer idMoneda, BigDecimal monto) {
        Result<LineaCredito> result;
        try {
            // Crear registro de linea de credito.
            LineaCredito linea = new LineaCredito();
            linea.setIdEntidadFinanciera(idEntidadFinanciera);
            linea.setIdPersona(idPersona);
            linea.setIdMoneda(idMoneda);
            linea.setMonto(monto);
            linea.setMontoTope(monto);
            linea.setVersion(0);
            linea.setFecha(Fechas.ahora());
            beans.getFacades().getLineaCreditoFacade().create(linea);

            // Crear registro historico.
            Result<LineaCreditoHistorico> resRegistroHistorico = registrarOperacionEnHistorico(linea, idUsuario, monto, TipoOperacionEnum.CREACION);
            resRegistroHistorico.raise();

            result = Results.ok().value(linea).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error registrando nueva línea de crédito para persona id='%s' con entidad id='%s'", idPersona, idEntidadFinanciera).build();
        }
        return result;
    }

    /**
     * Permite modificar una linea de credito para aumentar su monto disponible.
     *
     * @param lineaCredito Linea de credito a modificar.
     * @param idUsuario Identificador de usuario que realiza la operacion.
     * @param monto Monto de incremento.
     * @return Resultado de la operacion.
     */
    public Result<LineaCredito> acreditarLineaCredito(LineaCredito lineaCredito, Integer idUsuario, BigDecimal monto) {
        return acreditarLineaCredito(lineaCredito.getIdEntidadFinanciera(), lineaCredito.getIdPersona(), idUsuario, monto);
    }

    /**
     * Permite modificar una linea de credito para aumentar su monto disponible.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @param idPersona Identificador de cliente dueño de la linea.
     * @param idUsuario Identificador de usuario que realiza la operacion.
     * @param monto Monto de incremento.
     * @return Resultado de la operacion.
     */
    public Result<LineaCredito> acreditarLineaCredito(Integer idEntidadFinanciera, Integer idPersona, Integer idUsuario, BigDecimal monto) {
        Result<LineaCredito> result;
        try {
            // Obtener referencia de linea de credito.
            result = buscarLineaCredito(idEntidadFinanciera, idPersona);
            result.raise();

            // Calcular nuevo monto luego de la acreditacion.
            LineaCredito lineaCredito = result.value();
            BigDecimal actual = lineaCredito.getMonto();
            BigDecimal nuevo = Numeric.sum(actual, monto);

            // Aplicar cambios a la linea de credito.
            Result<Boolean> resModificacion = modificarLineaDeCredito(lineaCredito, idUsuario, nuevo, monto, TipoOperacionEnum.CREDITO);
            resModificacion.raise();

            // Retornar linea de credito actualizada.
            result = buscarLineaCredito(idEntidadFinanciera, idPersona);
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error tratando de acreditar la línea de crédito de persona id='%s' con entidad id='%s'", idPersona, idEntidadFinanciera).build();
        }
        return result;
    }

    /**
     * Permite modificar una linea de credito para decrementar su monto
     * disponible.
     *
     * @param lineaCredito Linea de credito a modificar.
     * @param idUsuario Identificador de usuario que realiza la operacion.
     * @param monto Monto de incremento.
     * @return Resultado de la operacion.
     */
    public Result<LineaCredito> descontarLineaCredito(LineaCredito lineaCredito, Integer idUsuario, BigDecimal monto) {
        return descontarLineaCredito(lineaCredito.getIdEntidadFinanciera(), lineaCredito.getIdPersona(), idUsuario, monto);
    }

    /**
     * Permite modificar una linea de credito para decrementar su monto
     * disponible.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @param idPersona Identificador de cliente dueño de la linea.
     * @param idUsuario Identificador de usuario que realiza la operacion.
     * @param monto Monto de incremento.
     * @return Resultado de la operacion.
     */
    public Result<LineaCredito> descontarLineaCredito(Integer idEntidadFinanciera, Integer idPersona, Integer idUsuario, BigDecimal monto) {
        Result<LineaCredito> result;
        try {
            // Obtener referencia de linea de credito.
            result = buscarLineaCredito(idEntidadFinanciera, idPersona);
            result.raise();

            // Calcular nuevo monto luego del descuento.
            LineaCredito lineaCredito = result.value();
            BigDecimal actual = lineaCredito.getMonto();
            BigDecimal nuevo = Numeric.sub(actual, monto);

            // Controlar que la linea de credito sea suficiente.
            if (Numeric.menor(actual, monto)) {
                return Results.ko().errno(Errnos.ERRNO_LINEA_CREDITO_INSUFICIENTE).message("La línea de crédito es insuficiente para el desembolso con la entidad financiera indicada.").build();
            }

            // Aplicar cambios a la linea de credito.
            Result<Boolean> resModificacion = modificarLineaDeCredito(lineaCredito, idUsuario, nuevo, monto, TipoOperacionEnum.DEBITO);
            resModificacion.raise();

            // Retornar linea de credito actualizada.
            result = buscarLineaCredito(idEntidadFinanciera, idPersona);
        } catch (Throwable thr) {
            int errno = Errnos.ERRNO_ERROR;
            Throwable oex = getFailureCause(thr, OptimisticLockException.class);
            if (oex != null) {
                errno = Errnos.ERRNO_LINEA_CREDITO_CORRUPTA;
            }
            result = Results.ko().cause(thr).errno(errno).message("Ocurrió un error tratando de descontar la línea de crédito de persona id='%s' con entidad id='%s'", idPersona, idEntidadFinanciera).build();
        }
        return result;
    }

    /**
     * Abstrae el proceso de modificacion de una linea de credito ya sea para
     * aumentar o decrementar el monto disponible de la misma.
     *
     * @param linea Linea de credito.
     * @param idUsuario Identificador de usuario que realiza la operacion.
     * @param nuevo Monto final que queda como disponible para la linea.
     * @param diff Monto que fue sumado o restado al valor original.
     * @param operacion Tipo de operacion realizada.
     * @return Resultado de la operacion.
     */
    private Result<Boolean> modificarLineaDeCredito(LineaCredito linea, Integer idUsuario, BigDecimal nuevo, BigDecimal diff, TipoOperacionEnum operacion) {
        Result<Boolean> result;
        try {
            // Editar la linea de credito estableciendo el nuevo monto.
            linea.setMonto(nuevo);
            beans.getFacades().getLineaCreditoFacade().edit(linea);

            // Registrar operacion en historico de linea de credito.
            Result<LineaCreditoHistorico> resRegistroHistorico = registrarOperacionEnHistorico(linea, idUsuario, diff, operacion);
            resRegistroHistorico.raise();

            result = Results.ok().value(Boolean.TRUE).build();
        } catch (Throwable thr) {
            // TODO: CONTROLAR SI FUE POR SINCRONIZACION.
            result = Results.ko().cause(thr).message("Ocurrió un error modificando monto de línea de crédito id='%s' (%s -> %s)", linea.getId(), nuevo.toString(), diff.toString()).build();
        }

        return result;
    }

    /**
     * Registra una operacion en el historico de movimientos asociado a una
     * linea de credito.
     *
     * @param linea Linea de credito.
     * @param idUsuario Identificador de usuario que realizo el movimiento.
     * @param monto Monto del movimiento.
     * @param operacion Tipo de operacion.
     * @return Resultado de la operacion.
     */
    private Result<LineaCreditoHistorico> registrarOperacionEnHistorico(LineaCredito linea, Integer idUsuario, BigDecimal monto, TipoOperacionEnum operacion) {
        Result<LineaCreditoHistorico> result;
        try {
            LineaCreditoHistorico historico = new LineaCreditoHistorico();
            historico.setIdLineaCredito(linea);
            historico.setIdMoneda(linea.getIdMoneda());
            historico.setIdUsuario(idUsuario);
            historico.setMonto(monto);
            historico.setOperacion(operacion.value());
            historico.setFecha(Fechas.ahora());
            beans.getFacades().getLineaCreditoHistoricoFacade().create(historico);

            result = Results.ok().value(historico).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error registrando operación en histórico de línea de crédito id='%s' (%c/%s)", linea.getId(), operacion.value(), monto.toString()).build();
        }
        return result;
    }

    public enum TipoOperacionEnum {
        CREACION('I', "CREACIÓN"),
        CREDITO('C', "HABER"),
        DEBITO('D', "DEBE");

        //<editor-fold defaultstate="collapsed" desc="Atributos">
        private final Character value;
        private final String descripcion;
        //</editor-fold>

        private TipoOperacionEnum(Character value, String descripcion) {
            this.value = value;
            this.descripcion = descripcion;
        }

        public static TipoOperacionEnum from(Character value) {
            for (TipoOperacionEnum tipo : TipoOperacionEnum.values()) {
                if (Objects.equals(tipo.value, value)) {
                    return tipo;
                }
            }
            return null;
        }

        public String descripcion() {
            return descripcion;
        }

        private Character value() {
            return value;
        }
    }
}
