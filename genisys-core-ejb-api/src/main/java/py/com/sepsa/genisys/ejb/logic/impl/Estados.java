/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.Estado;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Estados", mappedName = "Estados")
@LocalBean
public class Estados implements Serializable {

    /**
     * Fachadas remotas.
     */
    @EJB
    private Facades facades;

    /**
     * Lista de estados cacheados.
     */
    private List<Estado> estados;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        List<EstadoEnum> enums = new LinkedList<>();
        enums.addAll(Arrays.asList(EstadoFacturaEnum.values()));

        estados = enums
                .stream()
                .map((ee) -> getOrCreateEstado(ee.getId(), ee.getDescripcion(), ee.getTipo()))
                .filter((e) -> e != null)
                .collect(Collectors.toList());
    }

    /**
     * El unico proposito de este metodo es la de ser llamado por otros beans
     * que deseen iniciar explicitamente el bean (eager) en lugar de ser
     * utilizado en algun momento arbitrario (lazy).
     */
    public void activate() {
        ;
    }

    /**
     * Permite mapear una coleccion de enumeraciones de estado a sus
     * correspondientes identificadores en base de datos.
     *
     * @param estados Coleccion de enumeraciones de estado, de posibles
     * diferentes tipos.
     * @return Coleccion de identificadores asociados a cada esnumeracion de
     * estado valido.
     */
    public Integer[] obtenerIds(Estados.EstadoEnum... estados) {
        return Arrays.asList(estados)
                .stream()
                .map((e) -> find(e))
                .filter((e) -> e != null)
                .map((e) -> e.getId())
                .toArray(Integer[]::new);
    }

    /**
     * Permite obtener la entidad estado asociada a una enumeracion de estado.
     *
     * @param estado Enumeracion de estado.
     * @return Resultado de la operacion.
     */
    public Result<Estado> buscarEstado(Estados.EstadoEnum estado) {
        Result<Estado> result;
        try {
            Estado estado_ = find(estado);
            result = Results.ok().value(estado_).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error obteniendo estado '%s'", estado.getDescripcion()).build();
        }
        return result;
    }

    /**
     * Permite obtener la entidad estado asociada a una enumeracion de estado.
     *
     * @param estado Enumeracion de estado.
     * @return Estado asociado a la enumeracion.
     */
    public Estado find(Estados.EstadoEnum estado) {
        String descripcion = estado.getDescripcion();
        Character tipo = estado.getTipo();
        return find(descripcion, tipo);
    }

    /**
     * Obtiene un objeto estado en base a su descripcion y tipo.
     *
     * @param descripcion Descripcion del estado.
     * @param tipo Tipo de estado.
     * @return Estado, si la descripcion y tipo estan presentes en base de
     * datos. Caso contrario {@code null}.
     */
    private Estado find(String descripcion, Character tipo) {
        for (Estado estado : estados) {
            if (estado.getDescripcion().equals(descripcion) && estado.getTipo().equals(tipo)) {
                return estado;
            }
        }
        return null;
    }

    /**
     * Obtiene un estado desde base de datos, especificado por su descripcion y
     * tipo. Si no existe, el estado es creado.
     *
     * @param descripcion Descripcion del estado.
     * @param tipo Tipo del estado.
     * @return Estado encontrado o creado. Si no es posible crear un nuevo
     * estado, se retorna {@code null}.
     */
    private Estado getOrCreateEstado(Integer id, String descripcion, Character tipo) {
        // Verificar si estado ya existe.
        Estado estado = facades.getEstadoFacade().findByDescripcionTipo(descripcion, tipo);
        if (estado != null) {
            return estado;
        }

        // Si estado no existe, crear el mismo.
        try {
            estado = new Estado();
            if (id != null) {
                estado.setId(id);
            }
            estado.setDescripcion(descripcion);
            estado.setTipo(tipo);
            facades.getEstadoFacade().create(estado);
            return estado;
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return null;
        }
    }

    /**
     * Retorna un objeto si el mismo esta presente en la base de datos.
     *
     * @param id Identificador del estado.
     * @return Estado obtenido desde la cache, nulo caso contrario.
     */
    public Estado findById(Integer id) {
        for (Estado estado : estados) {
            if (Objects.equals(estado.getId(), id)) {
                return estado;
            }
        }
        return null;
    }

    /**
     * Obtiene todos los estados (en cache) que concuerden con el tipo dado.
     *
     * @param tipo Tipo de estado a buscar.
     * @return Coleccion de estados.
     */
    public List<Estado> findByTipo(String tipo) {
        List<Estado> filtrados = new LinkedList<>();
        for (Estado estado : estados) {
            if (Objects.equals(estado.getTipo(), tipo)) {
                filtrados.add(estado);
            }
        }
        return filtrados;
    }

    public List<Estado> getEstados() {
        return estados;
    }

    /**
     * Interface general para abstraer los estados posibles dentro del sistema.
     */
    public static interface EstadoEnum {

        Integer getId();

        String getDescripcion();

        Character getTipo();
    }

    public enum EstadoFacturaEnum implements EstadoEnum {
        // Cada valor entero corresponde a un ID dentro de la BD. Excepto el valor 0.
        DESCONOCIDO(0, "--"),
        NO_CONFORMADA(1, "NO_CONFORMADA"),
        CONFORMADA(2, "CONFORMADA"),
        RECHAZADA_POR_PROVEEDOR(3, "RECHAZADA_POR_PROVEEDOR"),
        VENDIDA(4, "VENDIDA"), // Factura vendida a entidad (solicitud de adelanto aprobado).
        PAGADA(5, "PAGADA"), // Factura desembolsada.
        RECHAZADA_POR_ENTIDAD(6, "RECHAZADA_POR_ENTIDAD"),
        COBRADA(7, "COBRADA"), // Factura cobrada por entidad, al proveedor.
        CONFORMACION_SOLICITADA(8, "CONFORMACION_SOLICITADA"),
        RECHAZADA_POR_COMPRADOR(9, "RECHAZADA_POR_COMPRADOR"),
        EN_ESTUDIO_POR_ENTIDAD(10, "EN_ESTUDIO_POR_ENTIDAD"), // Factura vendida a entidad (solicitud de adelanto pendiente).
        ELIMINADA(11, "ELIMINADA"),
        LIQUIDADA(12, "LIQUIDADA");

        //<editor-fold defaultstate="collapsed" desc="Atributos">
        private final Integer id;
        private final String descripcion;
        private final Character tipo;
        //</editor-fold>

        EstadoFacturaEnum(Integer id, String descripcion) {
            this.id = id;
            this.descripcion = descripcion;
            this.tipo = 'F';
        }

        public static EstadoFacturaEnum fromId(Integer id) {
            for (EstadoFacturaEnum estado : values()) {
                if (Objects.equals(estado.id, id)) {
                    return estado;
                }
            }
            return DESCONOCIDO;
        }

        public static EstadoFacturaEnum fromDescripcion(String descripcion) {
            for (EstadoFacturaEnum estado : values()) {
                if (Objects.equals(estado.descripcion, descripcion)) {
                    return estado;
                }
            }
            return DESCONOCIDO;
        }

        @Override
        public Integer getId() {
            return id;
        }

        @Override
        public String getDescripcion() {
            return descripcion;
        }

        @Override
        public Character getTipo() {
            return tipo;
        }
    }

}
