/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.genisys.ejb.entities.info.Persona;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "config_cargo", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConfigCargo.findAll", query = "SELECT c FROM ConfigCargo c"),
    @NamedQuery(name = "ConfigCargo.findById", query = "SELECT c FROM ConfigCargo c WHERE c.id = :id"),
    @NamedQuery(name = "ConfigCargo.findByValor", query = "SELECT c FROM ConfigCargo c WHERE c.valor = :valor"),
    @NamedQuery(name = "ConfigCargo.findByFechaInicio", query = "SELECT c FROM ConfigCargo c WHERE c.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "ConfigCargo.findByFechaFin", query = "SELECT c FROM ConfigCargo c WHERE c.fechaFin = :fechaFin"),
    @NamedQuery(name = "ConfigCargo.findByHoraInicio", query = "SELECT c FROM ConfigCargo c WHERE c.horaInicio = :horaInicio"),
    @NamedQuery(name = "ConfigCargo.findByHoraFin", query = "SELECT c FROM ConfigCargo c WHERE c.horaFin = :horaFin"),
    @NamedQuery(name = "ConfigCargo.findByIdConvenioTipoCargo", query = "SELECT c FROM ConfigCargo c WHERE c.idConvenio = :idConvenio and c.idTipoCargo.id = :idTipoCargo"),
    @NamedQuery(name = "ConfigCargo.findByRenovacionAutomatica", query = "SELECT c FROM ConfigCargo c WHERE c.renovacionAutomatica = :renovacionAutomatica")})
public class ConfigCargo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor")
    private BigDecimal valor;
    @Basic(optional = true)
    @Column(name = "valor_texto")
    private String valorTexto;
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Column(name = "hora_inicio")
    @Temporal(TemporalType.TIME)
    private Date horaInicio;
    @Column(name = "hora_fin")
    @Temporal(TemporalType.TIME)
    private Date horaFin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "renovacion_automatica")
    private Character renovacionAutomatica;
    @Column(name = "id_convenio")
    private Integer idConvenio;
    @JoinColumn(name = "id_tipo_cargo", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private TipoCargo idTipoCargo;
    @JoinColumn(name = "id_tipo_valor", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private TipoValor idTipoValor;

    // Campos adicionales.
    @Transient
    private Persona proveedor = null;
    @Transient
    private Persona comprador = null;
    @Transient
    private Boolean renovacionAutomaticaBool = false;
    @JoinColumn(name = "id_convenio", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Convenio convenio;

    public ConfigCargo() {
    }

    public ConfigCargo(Integer id) {
        this.id = id;
    }

    public ConfigCargo(Integer id, BigDecimal valor, Character renovacionAutomatica) {
        this.id = id;
        this.valor = valor;
        this.renovacionAutomatica = renovacionAutomatica;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getValorTexto() {
        return valorTexto;
    }

    public void setValorTexto(String valorTexto) {
        this.valorTexto = valorTexto;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Date horaFin) {
        this.horaFin = horaFin;
    }

    public Character getRenovacionAutomatica() {
        return renovacionAutomatica;
    }

    public void setRenovacionAutomatica(Character renovacionAutomatica) {
        this.renovacionAutomatica = renovacionAutomatica;
    }

    public Integer getIdConvenio() {
        return idConvenio;
    }

    public void setIdConvenio(Integer idConvenio) {
        this.idConvenio = idConvenio;
    }

    public TipoCargo getIdTipoCargo() {
        return idTipoCargo;
    }

    public void setIdTipoCargo(TipoCargo idTipoCargo) {
        this.idTipoCargo = idTipoCargo;
    }

    public TipoValor getIdTipoValor() {
        return idTipoValor;
    }

    public void setIdTipoValor(TipoValor idTipoValor) {
        this.idTipoValor = idTipoValor;
    }

    public Persona getProveedor() {
        return proveedor;
    }

    public void setProveedor(Persona proveedor) {
        this.proveedor = proveedor;
    }

    public Persona getComprador() {
        return comprador;
    }

    public void setComprador(Persona comprador) {
        this.comprador = comprador;
    }

    public Boolean getRenovacionAutomaticaBool() {
        return renovacionAutomaticaBool;
    }

    public void setRenovacionAutomaticaBool(Boolean renovacionAutomaticaBool) {
        this.renovacionAutomatica = renovacionAutomaticaBool ? 'S' : 'N';
        this.renovacionAutomaticaBool = renovacionAutomaticaBool;
    }

    public Convenio getConvenio() {
        return convenio;
    }

    public void setConvenio(Convenio convenio) {
        this.convenio = convenio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ConfigCargo)) {
            return false;
        }
        ConfigCargo other = (ConfigCargo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String txt = getIdTipoCargo().getDescripcion().trim();
        txt = txt.replace("_", " ");
        return txt;
    }

}
