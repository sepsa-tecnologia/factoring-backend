/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.info;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.info.TipoContacto;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author descauriza
 */
@Stateless(name = "TipoContactoFacade", mappedName = "TipoContactoFacade")
@LocalBean
public class TipoContactoFacade extends AbstractFacade<TipoContacto> {

    public TipoContactoFacade() {
        super(TipoContacto.class);
    }

    public TipoContacto findById(Integer idTipoContacto) {
        Query q = getEntityManager().createNamedQuery("TipoContacto.findById");
        q.setParameter("idTipoContacto", idTipoContacto);
        return JpaUtils.getFirst(q);
    }

}
