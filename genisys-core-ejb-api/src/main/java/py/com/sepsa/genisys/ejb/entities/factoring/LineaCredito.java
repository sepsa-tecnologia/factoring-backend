/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import py.com.sepsa.genisys.ejb.entities.info.EntidadFinanciera;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;
import py.com.sepsa.genisys.ejb.entities.info.Persona;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "linea_credito", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LineaCredito.findAll", query = "SELECT l FROM LineaCredito l"),
    @NamedQuery(name = "LineaCredito.findById", query = "SELECT l FROM LineaCredito l WHERE l.id = :id"),
    @NamedQuery(name = "LineaCredito.findByIdEntidadFinanciera", query = "SELECT l FROM LineaCredito l WHERE l.idEntidadFinanciera = :idEntidadFinanciera"),
    @NamedQuery(name = "LineaCredito.findByIdPersona", query = "SELECT l FROM LineaCredito l WHERE l.idPersona = :idPersona"),
    @NamedQuery(name = "LineaCredito.findByIdMoneda", query = "SELECT l FROM LineaCredito l WHERE l.idMoneda = :idMoneda"),
    @NamedQuery(name = "LineaCredito.findByMonto", query = "SELECT l FROM LineaCredito l WHERE l.monto = :monto"),
    @NamedQuery(name = "LineaCredito.findByVersion", query = "SELECT l FROM LineaCredito l WHERE l.version = :version")})
public class LineaCredito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_entidad_financiera")
    private int idEntidadFinanciera;
    @Column(name = "id_persona")
    @NotNull
    private Integer idPersona;
    @Column(name = "id_moneda")
    @NotNull
    private Integer idMoneda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_tope")
    private BigDecimal montoTope;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto")
    private BigDecimal monto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "version")
    @Version
    private int version;
    @OneToMany(mappedBy = "idLineaCredito", fetch = FetchType.LAZY)
    private Collection<LineaCreditoHistorico> lineaCreditoHistoricoCollection;

    // Campos adicionales.
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_persona", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Persona persona;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_entidad_financiera", referencedColumnName = "id_entidad_financiera", nullable = false, insertable = false, updatable = false)
    private EntidadFinanciera entidadFinanciera;
    @JoinColumn(name = "id_moneda", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Moneda moneda;

    public LineaCredito() {
    }

    public LineaCredito(Integer id) {
        this.id = id;
    }

    public LineaCredito(Integer id, int idEntidadFinanciera, BigDecimal monto, int version) {
        this.id = id;
        this.idEntidadFinanciera = idEntidadFinanciera;
        this.monto = monto;
        this.version = version;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(int idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public BigDecimal getMontoTope() {
        return montoTope;
    }

    public void setMontoTope(BigDecimal montoTope) {
        this.montoTope = montoTope;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public int getVersion() {
        return version;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public EntidadFinanciera getEntidadFinanciera() {
        return entidadFinanciera;
    }

    public void setEntidadFinanciera(EntidadFinanciera entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    @XmlTransient
    public Collection<LineaCreditoHistorico> getLineaCreditoHistoricoCollection() {
        return lineaCreditoHistoricoCollection;
    }

    public void setLineaCreditoHistoricoCollection(Collection<LineaCreditoHistorico> lineaCreditoHistoricoCollection) {
        this.lineaCreditoHistoricoCollection = lineaCreditoHistoricoCollection;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof LineaCredito)) {
            return false;
        }
        LineaCredito other = (LineaCredito) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.LineaCredito[ id=" + id + " ]";
    }

}
