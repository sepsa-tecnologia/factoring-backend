/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.numeric.Currency;
import fa.gs.utils.misc.numeric.Numeric;
import java.io.File;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java8.util.Objects;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Excel {

    /**
     * Verifica si un objeto es nulo.
     *
     * @param obj Objeto.
     * @return {@code true} si el objeto es nulo, caso contrario {@code false}.
     */
    public static boolean isNull(Object obj) {
        return obj == null;
    }

    /**
     * Carga un archivo fisico a la memoria.
     *
     * @param file Archivo Excel.
     * @return Representacion en memoria.
     * @throws Throwable Si no es posible cargar el archivo.
     */
    public static XSSFWorkbook load(File file) throws Throwable {
        XSSFWorkbook wb;
        try (OPCPackage fs = OPCPackage.openOrCreate(file)) {
            wb = new XSSFWorkbook(fs);
        }
        return wb;
    }

    /**
     * Obtiene una hoja especifica de un libro excel.
     *
     * @param workbook Libro excel.
     * @param name Nombre de la hoja.
     * @return Hoja, si hubiere. Caso contrario {@code null}.
     */
    public static XSSFSheet sheet(XSSFWorkbook workbook, String name) {
        return workbook.getSheet(name);
    }

    /**
     * Obtiene una hoja especifica de un libro excel.
     *
     * @param workbook Libro excel.
     * @param index Posicion de la hoja.
     * @return Hoja, si hubiere. Caso contrario {@code null}.
     */
    public static XSSFSheet sheet(XSSFWorkbook workbook, int index) {
        return workbook.getSheetAt(index);
    }

    /**
     * Obtiene una fila especifica de una hoja excel.
     *
     * @param sheet Hoja excel.
     * @param row Numero de fila.
     * @return Fila, si hubiere. Caso contrario {@code null}.
     */
    public static XSSFRow row(XSSFSheet sheet, int row) {
        if (isNull(sheet)) {
            return null;
        }

        return sheet.getRow(row);
    }

    /**
     * Obtiene la cantidad de filas con datos en una hoja excel.
     *
     * @param sheet Hoja excel.
     * @return Cantidad de filas con datos (no vacias).
     */
    public static int rowCount(XSSFSheet sheet) {
        int rowCount = 0;
        Iterator<Row> iter = sheet.rowIterator();
        while (iter.hasNext()) {
            Row row = iter.next();
            if (!isRowBlank(row)) {
                rowCount = row.getRowNum();
            }
        }
        return rowCount;
    }

    private static boolean isRowBlank(Row r) {
        boolean ret = true;
        if (r != null) {
            Iterator<Cell> cellIter = r.cellIterator();
            while (cellIter.hasNext()) {
                Cell cell = cellIter.next();
                if (!isCellBlank(cell)) {
                    ret = false;
                    break;
                }
            }
        }

        return ret;
    }

    private static boolean isCellBlank(Cell cell) {
        if (cell == null) {
            return true;
        }

        boolean a = cell.getCellType() == CellType.BLANK;
        boolean b = cell.getCellType() == CellType.STRING && Assertions.stringNullOrEmpty(cell.getStringCellValue());
        return a || b;
    }

    /**
     * Obtiene una celda especifica de una hoja excel.
     *
     * @param sheet Hoja excel.
     * @param row Numero de fila.
     * @param col Numero de columna (celda).
     * @return Celda, si hubiere. Caso contrario {@code null}.
     */
    public static XSSFCell cell(XSSFSheet sheet, int row, int col) {
        if (isNull(sheet)) {
            return null;
        }

        XSSFRow row_ = row(sheet, row);
        return cell(row_, col);
    }

    /**
     * Obtiene una celda especifica de una fila excel.
     *
     * @param row Fila excel.
     * @param col Numero de columna (celda).
     * @return Celda, si hubiere. Caso contrario {@code null}.
     */
    public static XSSFCell cell(XSSFRow row, int col) {
        if (isNull(row)) {
            return null;
        }

        return row.getCell(col);
    }

    /**
     * Obtiene el valor de fecha en una celda.
     *
     * @param cell Celda excel.
     * @param fallback Valor alternativo en caso de que la celda no contenga un
     * valor correcto.
     * @return Fecha.
     */
    public static Date date(XSSFCell cell, Date fallback) {
        try {
            if (isNull(cell)) {
                return fallback;
            }

            if (cell.getCellType() == CellType.NUMERIC) {
                if (DateUtil.isCellDateFormatted(cell)) {
                    Date date = cell.getDateCellValue();
                    if (date != null) {
                        return date;
                    }
                }
            }

            String text = text(cell, null);
            if (text != null) {
                Date tmp = Fechas.parse(text, Fechas.FORMAT_DD_MM_YYYY);
                if (tmp != null) {
                    return tmp;
                }
            }

            return fallback;
        } catch (Throwable thr) {
            return fallback;
        }
    }

    /**
     * Obtiene el valor numerico en una celda.
     *
     * @param cell Celda excel.
     * @param fallback Valor alternativo en caso de que la celda no contenga un
     * valor correcto.
     * @return Valor numerico.
     */
    public static BigDecimal number(XSSFCell cell, BigDecimal fallback) {
        try {
            if (isNull(cell)) {
                return fallback;
            }

            if (cell.getCellType() == CellType.NUMERIC) {
                double value = cell.getNumericCellValue();
                return Numeric.wrap(value);
            }

            String text = text(cell, null);
            if (text != null) {
                BigDecimal tmp = Currency.parseCurrency(text);
                if (tmp != null) {
                    return tmp;
                }
            }

            return fallback;
        } catch (Throwable thr) {
            return fallback;
        }
    }

    /**
     * Obtiene el valor de texto en una celda.
     *
     * @param cell Celda excel.
     * @param fallback Valor alternativo en caso de que la celda no contenga un
     * valor correcto.
     * @return Texto.
     */
    public static String text(Cell cell, String fallback) {
        try {
            if (isNull(cell)) {
                return fallback;
            }

            if (cell.getCellType() == CellType.STRING) {
                return cell.getStringCellValue();
            }

            return fallback;
        } catch (Throwable thr) {
            return fallback;
        }
    }

    /**
     * Obtiene el valor de bandera en una celda. S=si, N=no.
     *
     * @param cell Celda excel.
     * @param fallback Valor alternativo en caso de que la celda no contenga un
     * valor correcto.
     * @return Booleano.
     */
    public static Boolean bool(Cell cell, Boolean fallback) {
        String text = text(cell, "N");
        if (Objects.equals(text, "S")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verifica si una fila esta completamente vacia.
     *
     * @param row Fila excel.
     * @return {@code true} si la fila esta vacia, caso contrario {@code false}.
     */
    public static boolean isEmpty(XSSFRow row) {
        if (isNull(row)) {
            return true;
        }

        for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
            XSSFCell cell = row.getCell(c);
            if (!isEmpty(cell)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Verifica si una celda esta vacia.
     *
     * @param cell Celda excel.
     * @return {@code true} si la celda esta vacia, caso contrario
     * {@code false}.
     */
    public static boolean isEmpty(XSSFCell cell) {
        try {
            if (cell == null) {
                return true;
            } else if (cell.getCellType() == CellType.BLANK) {
                return true;
            } else if (cell.getCellType() == CellType.STRING && cell.getStringCellValue() != null && cell.getStringCellValue().isEmpty()) {
                return true;
            } else {
                return false;
            }
        } catch (Throwable thr) {
            return false;
        }
    }

}
