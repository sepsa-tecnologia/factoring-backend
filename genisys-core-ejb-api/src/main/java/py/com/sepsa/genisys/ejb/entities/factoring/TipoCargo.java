/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "tipo_cargo", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoCargo.findAll", query = "SELECT t FROM TipoCargo t"),
    @NamedQuery(name = "TipoCargo.findById", query = "SELECT t FROM TipoCargo t WHERE t.id = :id"),
    @NamedQuery(name = "TipoCargo.findByDescripcion", query = "SELECT t FROM TipoCargo t WHERE t.descripcion = :descripcion")})
public class TipoCargo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoCargo", fetch = FetchType.LAZY)
    private Collection<HistoricoConfigCargo> historicoConfigCargoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoCargo", fetch = FetchType.LAZY)
    private Collection<ConfigCargo> configCargoCollection;

    public TipoCargo() {
    }

    public TipoCargo(Integer id) {
        this.id = id;
    }

    public TipoCargo(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<HistoricoConfigCargo> getHistoricoConfigCargoCollection() {
        return historicoConfigCargoCollection;
    }

    public void setHistoricoConfigCargoCollection(Collection<HistoricoConfigCargo> historicoConfigCargoCollection) {
        this.historicoConfigCargoCollection = historicoConfigCargoCollection;
    }

    @XmlTransient
    public Collection<ConfigCargo> getConfigCargoCollection() {
        return configCargoCollection;
    }

    public void setConfigCargoCollection(Collection<ConfigCargo> configCargoCollection) {
        this.configCargoCollection = configCargoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof TipoCargo)) {
            return false;
        }
        TipoCargo other = (TipoCargo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String str = this.descripcion;
        str = str.replace("_", " ");
        return str.trim();
    }

}
