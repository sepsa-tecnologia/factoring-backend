/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

/**
 *
 * @author Fabio A. González Sosa
 */
import fa.gs.misc.Numeric;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.fechas.Formats;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import java8.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.info.ContactoEmail;
import py.com.sepsa.genisys.ejb.entities.info.Email;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.info.PersonaInfo;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoring;
import py.com.sepsa.genisys.ejb.entities.notificacion.Plantilla;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.enums.TipoPlantilla;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.ejb.utils.email.EmailContent;
import py.com.sepsa.genisys.ejb.utils.email.EmailContentType;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Notificaciones", mappedName = "Notificaciones")
@LocalBean
public class Notificaciones extends LogicBean {

    @EJB
    private Plantillas plantillas;

    @EJB
    private Personas personas;

    @EJB
    private Facturas facturas;

    @EJB
    private Emails emails;

    @EJB
    private Configuraciones configuraciones;

    @EJB
    private JpaUtils jpaUtils;

    public void notificarSepsa(String fmt, Object... args) {
        notificarSepsa(null, fmt, args);
    }

    public void notificarSepsa(Throwable thr, String fmt, Object... args) {
        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            // Generar contenido.
            try (PrintStream ps = new PrintStream(os)) {
                Errors.dump(ps, thr, fmt, args);
            }

            // Cuerpo de mensaje.
            String msg = Strings.getString(os.toByteArray());

            // Notificar.
            notificarAlertaSepsa(msg);
        } catch (Throwable thr0) {
            log.error(thr, "Ocurrió un error notificando alerta a sepsa");
        }
    }

    private void notificarAlertaSepsa(String contenido) {
        // Obtener persona SEPSA.
        Persona destino = personas.obtenerPorRuc("80049048-7").value();
        if (destino == null) {
            return;
        }

        // Crear notificacion.
        crearNotificacionAPersona(sepsa(destino), contenido);
    }

    /**
     * Registra notificaciones pendientes por conformacion de facturas.
     *
     * @param facturas Facturas conformadas.
     */
    public void notificarConformacionDeFacturas(List<FacturaInfo> facturas) {
        Map<PersonaInfo, List<FacturaInfo>> grupos = clasificarPorProveedor(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.CONFORMACION);
    }

    /**
     * Registra notificaciones pendientes por solicitud de conformación de
     * facturas.
     *
     * @param facturas Facturas que son solicitadas para conformacion.
     */
    public void notificarSolicitudConformacionDeFacturas(List<FacturaInfo> facturas) {
        Map<PersonaInfo, List<FacturaInfo>> grupos = clasificarPorComprador(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.SOLICITUD_CONFORMACION);
    }

    /**
     * Registra notificaciones pendientes por inicio de analisis de factibilidad
     * de desembolso de facturas.
     *
     * @param facturas Facturas cuyo estudio de factibilidad ha iniciado.
     */
    public void notificarAnalisisDeFacturas(List<FacturaInfo> facturas) {
        Map<PersonaInfo, List<FacturaInfo>> grupos;

        grupos = clasificarPorComprador(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.EN_ESTUDIO);

        grupos = clasificarPorProveedor(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.EN_ESTUDIO);
    }

    /**
     * Registra notificaciones pendientes por rechazo de facturas conformadas
     * por parte del comprador.
     *
     * @param facturas Facturas rechazadas.
     */
    public void notificarRechazoDeFacturasPorComprador(List<FacturaInfo> facturas) {
        Map<PersonaInfo, List<FacturaInfo>> grupos = clasificarPorProveedor(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.RECHAZO_POR_COMPRADOR);
    }

    /**
     * Registra notificaciones pendientes por rechazo de facturas conformadas
     * por parte del proveedor.
     *
     * @param facturas Facturas rechazadas.
     */
    public void notificarRechazoDeFacturasPorProveedor(List<FacturaInfo> facturas) {
        Map<PersonaInfo, List<FacturaInfo>> grupos = clasificarPorComprador(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.RECHAZO_POR_PROVEEDOR);
    }

    /**
     * Registra notificaciones pendientes por venta de facturas conformadas
     * (solicitud de desembolso).
     *
     * @param facturas Facturas vendidas.
     */
    public void notificarVentaDeFacturas(List<FacturaInfo> facturas) {
        Map<PersonaInfo, List<FacturaInfo>> grupos = clasificarPorEntidadFinanciera(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.VENTA);
    }

    /**
     * Registra notificaciones pendientes por rechazo de facturas vendidas por
     * parte de una entidad financiera.
     *
     * @param facturas Facturas rechazadas.
     */
    public void notificarRechazoDeFacturasPorEntidad(List<FacturaInfo> facturas) {
        Map<PersonaInfo, List<FacturaInfo>> grupos;

        grupos = clasificarPorComprador(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.RECHAZO_POR_ENTIDAD);

        grupos = clasificarPorProveedor(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.RECHAZO_POR_ENTIDAD);
    }

    /**
     * Registra notificaciones pendientes por desembolso de facturas vendidas.
     *
     * @param facturas Facturas desembolsadas.
     */
    public void notificarDesembolsoDeFacturas(List<FacturaInfo> facturas) {
        Map<PersonaInfo, List<FacturaInfo>> grupos;

        grupos = clasificarPorComprador(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.PAGO);

        grupos = clasificarPorProveedor(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.PAGO);
    }

    /**
     * Registra notificaciones pendientes por rechazo de facturas conformadas
     * por parte del proveedor.
     *
     * @param facturas Facturas rechazadas.
     */
    public void notificarCobroFacturas(List<FacturaInfo> facturas) {
        Map<PersonaInfo, List<FacturaInfo>> grupos;

        grupos = clasificarPorComprador(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.COBRO);

        grupos = clasificarPorProveedor(facturas);
        crearNotificacionesAGrupoPersonas(grupos, TipoPlantilla.COBRO);
    }

    /**
     * Crea notificaciones de cambio de estado de facturas a un grupo de
     * personas que actuan como destinatarios y poseen una lista individual de
     * facturas.
     *
     * @param grupos Grupos de destinatarios.
     * @param tipoPlantilla Tpo de plantilla de notificacion.
     */
    private void crearNotificacionesAGrupoPersonas(Map<PersonaInfo, List<FacturaInfo>> grupos, TipoPlantilla tipoPlantilla) {
        for (Map.Entry<PersonaInfo, List<FacturaInfo>> grupo : grupos.entrySet()) {
            PersonaInfo destino = grupo.getKey();
            List<FacturaInfo> listaFacturas = grupo.getValue();
            crearNotificacionAPersona(destino, listaFacturas, tipoPlantilla);
        }
    }

    private void crearNotificacionAPersona(PersonaInfo destino, String contenido) {
        Collection<ContactoEmail> contactosEmail = emails.obtenerContactosEmailDisponibles(destino.getId());
        log.debug("[NOTIFICACION] id_persona_destino=%d, plantilla=%s, contactos_email=%d", destino.getId(), TipoPlantilla.TEXTO.toString(), contactosEmail.size());
        for (ContactoEmail contactoEmail : contactosEmail) {
            // Se genera la notificacion (inicialmente inactiva) para el contacto que habilita las notificaciones de factoring.
            NotificacionEmailFactoring n = new NotificacionEmailFactoring();
            n.setFechaInsercion(new Date());
            n.setIdContacto(contactoEmail.getIdContacto());
            n.setIdEmail(contactoEmail.getIdEmail());
            n.setIdPlantilla(plantillas.find(TipoPlantilla.TEXTO));
            n.setContenido(contenido);
            crearNotificacionAPersonaAsync(n, null);
        }
    }

    private void crearNotificacionAPersonaAsync(NotificacionEmailFactoring cabecera, Integer[] idFacturas) {
        Queues queues = Queues.inject();
        if (queues != null) {
            queues.post(cabecera, idFacturas);
        }
    }

    /**
     * Crea una notificacion de cambio de estado de facturas a una persona en
     * particular.
     *
     * @param destino Persona que sera utilizada como destinatario de la
     * notificacion.
     * @param facturas Facturas cuyo cambio de estado se notifica.
     * @param tipoPlantilla Tipo de plantilla de notificacion.
     */
    private void crearNotificacionAPersona(PersonaInfo destino, List<FacturaInfo> facturas, TipoPlantilla tipoPlantilla) {
        try {
            Collection<ContactoEmail> contactosEmail = emails.obtenerContactosEmailDisponibles(destino.getId());
            log.debug("[NOTIFICACION] id_persona_destino=%d, cantidad_facturas=%d, plantilla=%s, contactos_email=%d", destino.getId(), facturas.size(), tipoPlantilla.toString(), contactosEmail.size());
            for (ContactoEmail contactoEmail : contactosEmail) {
                // Se genera la cabecera de notificacion-
                NotificacionEmailFactoring n = new NotificacionEmailFactoring();
                n.setFechaInsercion(new Date());
                n.setIdContacto(contactoEmail.getIdContacto());
                n.setIdEmail(contactoEmail.getIdEmail());
                n.setIdPlantilla(plantillas.find(tipoPlantilla));

                // Se agrega la lista de facturas asociadas a la notificacion de cambio de estado.
                Collection<Integer> idf = Lists.stream(facturas)
                        .map(f -> f.getId())
                        .filter(i -> i != null)
                        .distinct()
                        .collect(Collectors.toList());

                // Se encola el registro de notificaciones.
                crearNotificacionAPersonaAsync(n, fa.gs.utils.collections.Arrays.unwrap(idf, Integer.class));
            }
        } catch (Throwable thr) {
            log.warning(thr, "No se pudo registrar la notificacion (con plantilla '%s') para la persona con id = %d", tipoPlantilla.toString(), destino.getId());
        }
    }

    /**
     * Clasifica un grupo de facturas y las divide en grupos de acuerdo a sus
     * respectivos proveedores.
     *
     * @param facturas Lista de facturas a clasificar.
     * @return Lista de facturas clasificadas por proveedor.
     */
    private Map<PersonaInfo, List<FacturaInfo>> clasificarPorProveedor(List<FacturaInfo> facturas) {
        // Agrupar las facturas por proveedor.
        Map<PersonaInfo, List<FacturaInfo>> grupos = new HashMap<>();
        for (FacturaInfo factura : facturas) {
            PersonaInfo destino = proveedor(factura);
            if (destino != null) {
                if (!grupos.containsKey(destino)) {
                    grupos.put(destino, new LinkedList<>());
                }
                grupos.get(destino).add(factura);
            }
        }
        return grupos;
    }

    /**
     * Clasifica un grupo de facturas y las divide en grupos de acuerdo a sus
     * respectivos compradores.
     *
     * @param facturas Lista de facturas a clasificar.
     * @return Lista de facturas clasificadas por comprador.
     */
    private Map<PersonaInfo, List<FacturaInfo>> clasificarPorComprador(List<FacturaInfo> facturas) {
        // Agrupar las facturas por proveedor.
        Map<PersonaInfo, List<FacturaInfo>> grupos = new HashMap<>();
        for (FacturaInfo factura : facturas) {
            PersonaInfo destino = comprador(factura);
            if (destino != null) {
                if (!grupos.containsKey(destino)) {
                    grupos.put(destino, new LinkedList<>());
                }
                grupos.get(destino).add(factura);
            }
        }
        return grupos;
    }

    /**
     * Clasifica un grupo de facturas y las divide en grupos de acuerdo a sus
     * respectivas entidades financieras.
     *
     * @param facturas Lista de facturas a clasificar.
     * @return Lista de facturas clasificadas por comprador.
     */
    private Map<PersonaInfo, List<FacturaInfo>> clasificarPorEntidadFinanciera(List<FacturaInfo> facturas) {
        // Agrupar las facturas por proveedor.
        Map<PersonaInfo, List<FacturaInfo>> grupos = new HashMap<>();
        for (FacturaInfo factura : facturas) {
            PersonaInfo destino = entidadFinanciera(factura);
            if (destino != null) {
                if (!grupos.containsKey(destino)) {
                    grupos.put(destino, new LinkedList<>());
                }
                grupos.get(destino).add(factura);
            }
        }
        return grupos;
    }

    public List<NotificacionEmailFactoring> obtenerNotificacionesPendientes() {
        try {
            return beans.getFacades().getNotificacionEmailFactoringFacade().findByEstado('P');
        } catch (Throwable thr) {
            return Lists.empty();
        }
    }

    public void marcarNotificacionComoLeida(Integer idNotificacionEmailFactoring) {
        NotificacionEmailFactoring notificacionEmailFactoring = beans.getFacades().getNotificacionEmailFactoringFacade().find(idNotificacionEmailFactoring);
        if (Objects.equals(notificacionEmailFactoring.getLeido(), 'n')) {
            notificacionEmailFactoring.setLeido('s');
            notificacionEmailFactoring.setFechaEnviar(Fechas.ahora());
            beans.getFacades().getNotificacionEmailFactoringFacade().edit(notificacionEmailFactoring);
        }
    }

    public void marcarNotificacionComoEnviada(Integer idNotificacionEmailFactoring) {
        NotificacionEmailFactoring notificacionEmailFactoring = beans.getFacades().getNotificacionEmailFactoringFacade().find(idNotificacionEmailFactoring);
        if (Objects.equals(notificacionEmailFactoring.getLeido(), 'n')) {
            notificacionEmailFactoring.setFechaEnvio(new Date());
            notificacionEmailFactoring.setEstado('E');
            beans.getFacades().getNotificacionEmailFactoringFacade().edit(notificacionEmailFactoring);
        }
    }

    public Result<NotificacionEmailFactoring> obtenerUltimaNotificacionPersona(Integer idPersona, TipoPlantilla tipoPlantilla) {
        Result<NotificacionEmailFactoring> result;

        try {
            // Obtener identificador de la plantilla.
            Plantilla PLANTILLA = plantillas.find(tipoPlantilla);

            // Construir consulta.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     nef.id as \"id\", ");
            builder.append("     nef.id_email as \"idEmail\", ");
            builder.append("     nef.id_contacto as \"idContacto\", ");
            builder.append("     nef.id_plantilla as \"idPlantilla\",");
            builder.append("     nef.fecha_insercion as \"fechaInsercion\" ");
            builder.append(" from notificacion.notificacion_email_factoring as nef ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and nef.id_contacto = %s ", idPersona);
            builder.append("     and nef.id_plantilla = %s ", PLANTILLA.getId());
            builder.append(" order by fecha_insercion desc");
            builder.append(" ; ");

            // Ejecutar consulta.
            String query = builder.toString();
            Map<String, Object> row = jpaUtils.getFirst(query);

            if (!Assertions.isNull(row)) {
                // Extraer datos
                NotificacionEmailFactoring item = new NotificacionEmailFactoring();
                item.setId(Maps.integer(row, "id"));
                item.setIdEmail(Maps.integer(row, "idEmail"));
                item.setIdContacto(Maps.integer(row, "idContacto"));
                item.setIdPlantilla(PLANTILLA);
                item.setFechaInsercion(Maps.date(row, "fechaInsercion"));

                result = Results.ok()
                        .value(item)
                        .build();
            } else {
                result = Results.ok()
                        .value(null)
                        .build();
            }
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error verificando datos de factura")
                    .build();
        }
        return result;
    }

    public Integer obtenerEsperaNotificaciones() throws Throwable {
        try {
            Result<String> resConfig = configuraciones.buscarValorParametro(ConfiguracionGeneralEnum.NOTIFICACION_FACTURA_CONFORMADA_PROVEEDOR_ESPERA);
            String value = resConfig.value("3");
            return Integer.valueOf(value);
        } catch (NumberFormatException thr) {
            return 1;
        }
    }

    public void crearNotificacionFacturasConformadas(List<FacturaInfo> facturas) {
        FacturaInfo factura = facturas.get(0);
        PersonaInfo persona = proveedor(factura);
        crearNotificacionAPersona(persona, facturas, TipoPlantilla.RECORDATORIO_FACTURAS_CONFORMADAS);
    }

    public void crearNotificacionFacturasPorVencer(List<FacturaInfo> facturas) {
        FacturaInfo factura = facturas.get(0);
        PersonaInfo persona = comprador(factura);
        crearNotificacionAPersona(persona, facturas, TipoPlantilla.RECORDATORIO_FACTURAS_POR_VENCER);
    }

    //<editor-fold defaultstate="collapsed" desc="Manipulacion y Generacion de contenido de notificaciones via Email">
    public EmailContent generarContenidoNotificacionEmail(NotificacionEmailFactoring notificacion) throws Exception {
        return generarContenidoNotificacionEmail(notificacion, true);
    }

    public EmailContent generarContenidoNotificacionEmail(NotificacionEmailFactoring notificacion, boolean fullHtml) throws Exception {
        // Obtenemos el email del contacto.
        Email email = beans.getFacades().getEmailFacade().find(notificacion.getIdEmail());

        // Obtenemos los identificadores de las facturas asociadas a la notificado.
        List<FacturaInfo> listaFacturas = getFacturasAsociadas(notificacion.getId());

        // Obtener plantilla de notificacion.
        Plantilla plantilla = beans.getFacades().getPlantillaFacade().find(notificacion.getIdPlantilla().getId());

        // Se forman las partes del correo y se envia el mismo.
        EmailContent content = build(notificacion, email, plantilla, listaFacturas, fullHtml);
        return content;
    }

    /**
     * Obtiene las facturas asociadas a una notificacion.
     *
     * @param idNotificacion Identificador de notificacion.
     * @return Lista de facturas asociadas a la notificacion.
     */
    private List<FacturaInfo> getFacturasAsociadas(Integer idNotificacion) {
        try {
            // Obtener identificadores de facturas asociadas a notificacion.
            Integer[] idFacturas = beans.getFacades().getNotificacionEmailFactoringFacade().findIdFacturas(idNotificacion);

            // Obtener facturas asociadas.
            Result<List<FacturaInfo>> res = facturas.buscarFacturas(idFacturas);
            res.raise();
            return res.value();
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error obteniendo facturas asociadas id_notificacion=%s", idNotificacion);
            return Lists.empty();
        }
    }

    /**
     * Construye el contenido del correo electronico a ser enviado como
     * notificacion.
     *
     * @param notificacion Notificacion pendiente.
     * @param destinatario Destinatario del correo.
     * @param plantilla Plantilla para el cuerpo del correo.
     * @param facturas Lista de facturas con las cuales se genera el contenido.
     * @return Contenido del correo.
     * @throws Exception Error producido durante la generacion del contenido.
     */
    private EmailContent build(NotificacionEmailFactoring notificacion, Email destinatario, Plantilla plantilla, List<FacturaInfo> facturas) throws Exception {
        return build(notificacion, destinatario, plantilla, facturas, true);
    }

    /**
     * Construye el contenido del correo electronico a ser enviado como
     * notificacion.
     *
     * @param notificacion Notificacion pendiente.
     * @param destinatario Destinatario del correo.
     * @param plantilla Plantilla para el cuerpo del correo.
     * @param facturas Lista de facturas con las cuales se genera el contenido.
     * @param fullHtml indica si el template debe generar contenido HTML
     * autocontenido completamente o si debe generar un fraccion que puede ser
     * incluida en otros componentes HTML.
     * @return Contenido del correo.
     * @throws Exception Error producido durante la generacion del contenido.
     */
    private EmailContent build(NotificacionEmailFactoring notificacion, Email destinatario, Plantilla plantilla, List<FacturaInfo> facturas, boolean fullHtml) throws Exception {
        // Variables para el template de correo electronico.
        String templateName;
        Map<String, Object> templateData = new HashMap<>();
        templateData.put("generarFullHtml", fullHtml);
        templateData.put("urlTracking", generarUrlTracking(notificacion));

        // Asunto de la plantilla en base de datos.
        String cuerpoPlantilla = plantilla.getCuerpo();
        if (Objects.equals(cuerpoPlantilla, TipoPlantilla.CONFORMACION.toString())) {
            // Obtenemos una factura para conocer el comprador que conformo las facturas.
            // Para proveedor.
            FacturaInfo factura = facturas.get(0);
            templateName = "cambio_estado_facturas.ftl";
            templateData.put("titulo", TipoPlantilla.CONFORMACION.getAsunto());
            templateData.put("facturas", wrap(facturas));
            templateData.put("usarMontoConformado", true);
            templateData.put("mensajes", Arrays.asList(
                    String.format("El comprador <strong>%s</strong> con RUC %s, ha conformado la siguiente lista de facturas.", factura.getCompradorRazonSocial(), factura.getCompradorRuc())
            ));
        } else if (Objects.equals(cuerpoPlantilla, TipoPlantilla.RECHAZO_POR_COMPRADOR.toString())) {
            // Obtenemos una factura para conocer el comprador que rechazo las facturas.
            // Para proveedor.
            FacturaInfo factura = facturas.get(0);
            templateName = "cambio_estado_facturas.ftl";
            templateData.put("titulo", TipoPlantilla.RECHAZO_POR_COMPRADOR.getAsunto());
            templateData.put("facturas", wrap(facturas));
            templateData.put("usarMontoConformado", false);
            templateData.put("mensajes", Arrays.asList(
                    String.format("El comprador <strong>%s</strong> con RUC %s ha rechazado la siguiente lista de facturas no conformadas o pendientes de conformación.", factura.getCompradorRazonSocial(), factura.getCompradorRuc())
            ));
        } else if (Objects.equals(cuerpoPlantilla, TipoPlantilla.RECHAZO_POR_PROVEEDOR.toString())) {
            // Obtenemos una factura para conocer el proveedor que rechazo las facturas.
            // Para comprador.
            FacturaInfo factura = facturas.get(0);
            templateName = "cambio_estado_facturas.ftl";
            templateData.put("titulo", TipoPlantilla.RECHAZO_POR_PROVEEDOR.getAsunto());
            templateData.put("facturas", wrap(facturas));
            templateData.put("usarMontoConformado", true);
            templateData.put("mensajes", Arrays.asList(
                    String.format("El proveedor <strong>%s</strong> con RUC %s ha rechazado la siguiente lista de facturas previamente conformadas.", factura.getProveedorRazonSocial(), factura.getProveedorRuc())
            ));
        } else if (Objects.equals(cuerpoPlantilla, TipoPlantilla.VENTA.toString())) {
            // Obtenemos una factura para conocer el proveedor que vende las facturas.
            // Para entidad.
            FacturaInfo factura = facturas.get(0);
            templateName = "cambio_estado_facturas.ftl";
            templateData.put("titulo", TipoPlantilla.VENTA.getAsunto());
            templateData.put("facturas", wrap(facturas));
            templateData.put("usarMontoConformado", true);
            templateData.put("mensajes", Arrays.asList(
                    String.format("El proveedor <strong>%s</strong> con RUC %s, ha solicitado el desembolso de la siguiente lista de facturas conformadas.", factura.getProveedorRazonSocial(), factura.getProveedorRuc())
            ));
        } else if (Objects.equals(cuerpoPlantilla, TipoPlantilla.RECHAZO_POR_ENTIDAD.toString())) {
            // Obtenemos algunos datos de la entidad financiera que realizo la accion.
            // Para proveedor/comprador.
            FacturaInfo factura = facturas.get(0);
            PersonaInfo persona = entidadFinanciera(factura);
            templateName = "cambio_estado_facturas.ftl";
            templateData.put("titulo", TipoPlantilla.RECHAZO_POR_ENTIDAD.getAsunto());
            templateData.put("facturas", wrap(facturas));
            templateData.put("usarMontoConformado", true);
            templateData.put("mensajes", Arrays.asList(
                    String.format("La entidad financiera <strong>%s</strong> con RUC %s, ha rechazado la solicitud de desembolso de la siguiente lista de facturas.", persona.getRazonSocial(), persona.getRuc())
            ));
        } else if (Objects.equals(cuerpoPlantilla, TipoPlantilla.PAGO.toString())) {
            // Obtenemos algunos datos de la entidad financiera que realizo la accion.
            FacturaInfo factura = facturas.get(0);
            PersonaInfo persona = entidadFinanciera(factura);
            templateName = "cambio_estado_facturas.ftl";
            templateData.put("titulo", TipoPlantilla.PAGO.getAsunto());
            templateData.put("facturas", wrap(facturas));
            templateData.put("usarMontoConformado", true);
            templateData.put("usarMontosDesembolso", true);
            templateData.put("mensajes", Arrays.asList(
                    String.format("La entidad financiera <strong>%s</strong> con RUC %s, ha registrado el desembolso de la siguiente lista de facturas.", persona.getRazonSocial(), persona.getRuc())
            ));
            // TODO: AGREGAR A LA PLANTILLA LA POSIBILIDAD DE MOSTRAR (CONDICIONALMENTE EN BASE A SU EXISTENCIA) INFORMACION RELACIONADA A LA TRANSACCION.
        } else if (Objects.equals(cuerpoPlantilla, TipoPlantilla.SOLICITUD_CONFORMACION.toString())) {
            // Obtenemos una factura para conocer el proveedor que rechazo las facturas.
            // Para comprador.
            FacturaInfo factura = facturas.get(0);
            PersonaInfo persona = proveedor(factura);
            templateName = "cambio_estado_facturas.ftl";
            templateData.put("titulo", TipoPlantilla.SOLICITUD_CONFORMACION.getAsunto());
            templateData.put("facturas", wrap(facturas));
            templateData.put("usarMontoConformado", false);
            templateData.put("mensajes", Arrays.asList(
                    String.format("El proveedor <strong>%s</strong> con RUC %s ha solicitado la conformación de la siguiente lista de facturas.", persona.getRazonSocial(), persona.getRuc())
            ));
        } else if (Objects.equals(cuerpoPlantilla, TipoPlantilla.EN_ESTUDIO.toString())) {
            // Obtenemos una factura para conocer la entidad financiera que acepto las facturas para su analisis.
            // Para proveedor.
            // Obtenemos algunos datos de la entidad financiera que realizo la accion.
            FacturaInfo factura = facturas.get(0);
            PersonaInfo persona = entidadFinanciera(factura);
            templateName = "cambio_estado_facturas.ftl";
            templateData.put("titulo", TipoPlantilla.EN_ESTUDIO.getAsunto());
            templateData.put("facturas", wrap(facturas));
            templateData.put("usarMontoConformado", false);
            templateData.put("mensajes", Arrays.asList(
                    String.format("La entidad financiera <strong>%s</strong> con RUC %s, ha aceptado la siguiente lista de facturas para su análisis/estudio de factibilidad para desembolso.", persona.getRazonSocial(), persona.getRuc())
            ));
        } else if (Objects.equals(cuerpoPlantilla, TipoPlantilla.COBRO.toString())) {
            // Obtenemos una factura para conocer la entidad financiera que acepto las facturas como cobradas.
            // Para comprador.
            // Obtenemos algunos datos de la entidad financiera que realizo la accion.
            FacturaInfo factura = facturas.get(0);
            PersonaInfo persona = entidadFinanciera(factura);
            templateName = "cambio_estado_facturas.ftl";
            templateData.put("titulo", TipoPlantilla.COBRO.getAsunto());
            templateData.put("facturas", wrap(facturas));
            templateData.put("usarMontoConformado", false);
            templateData.put("mensajes", Arrays.asList(
                    String.format("La entidad financiera <strong>%s</strong> con RUC %s, ha aceptado la siguiente lista de facturas como pagadas, cancelando así cualquier obligación pendiente.", persona.getRazonSocial(), persona.getRuc())
            ));
        } else if (Objects.equals(cuerpoPlantilla, TipoPlantilla.RECORDATORIO_FACTURAS_CONFORMADAS.toString())) {
            templateName = "recordatorio_facturas_conformadas.ftl";
            templateData.put("titulo", TipoPlantilla.RECORDATORIO_FACTURAS_CONFORMADAS.getAsunto());
            // Calcular monto total
            BigDecimal montoTotal = BigDecimal.ZERO;
            for (FacturaInfo facturaConformada : facturas) {
                montoTotal = montoTotal.add(facturaConformada.getMontoTotalConformado() == null ? BigDecimal.ZERO : facturaConformada.getMontoTotalConformado());
            }
            String montoTotalString = Numeric.formatCurrency(montoTotal, "Gs");

            templateData.put("mensajes", Arrays.asList(
                    String.format("Le recordamos que tiene %s facturas conformadas pendientes, por un monto igual a: %s", facturas.size(), montoTotalString)
            ));
        } else if (Objects.equals(cuerpoPlantilla, TipoPlantilla.RECORDATORIO_FACTURAS_POR_VENCER.toString())) {
            templateName = "recordatorio_facturas_conformadas.ftl";
            templateData.put("titulo", TipoPlantilla.RECORDATORIO_FACTURAS_POR_VENCER.getAsunto());
            // Calcular monto total
            BigDecimal montoTotal = BigDecimal.ZERO;
            Date fechaMasCercana = null;
            for (FacturaInfo facturaConformada : facturas) {
                if (fechaMasCercana == null) {
                    fechaMasCercana = facturaConformada.getFechaPago();
                } else {
                    if (fa.gs.utils.misc.fechas.Fechas.mayor(facturaConformada.getFechaPago(), fechaMasCercana)) {
                        fechaMasCercana = facturaConformada.getFechaPago();
                    }
                }
                montoTotal = montoTotal.add(facturaConformada.getMontoTotalConformado() == null ? BigDecimal.ZERO : facturaConformada.getMontoTotalConformado());
            }
            String montoTotalString = Numeric.formatCurrency(montoTotal, "Gs");
            String fechaAVencer = Fechas.format(fechaMasCercana, "dd/MM/yyyy");
            templateData.put("mensajes", Arrays.asList(
                    String.format("Le recordamos que tiene %s facturas por vencer, por un monto igual a: %s, a vencer desde el día: %s", facturas.size(), montoTotalString, fechaAVencer)
            ));
        } else {
            return null;
        }

        // Generar cuerpo del correo utilizando templates.
        Configuration cfg = new Configuration(new Version(2, 3, 23));
        cfg.setClassForTemplateLoading(Notificaciones.class, "/py/com/sepsa/genisys/core/templates");
        cfg.setDefaultEncoding("utf-8");
        cfg.setOutputEncoding("utf-8");
        Template template = cfg.getTemplate(templateName);
        StringWriter writer = new StringWriter();
        template.process(templateData, writer);

        // Generar cuerpo de correo encapsulado.
        EmailContent content = new EmailContent();
        content.setDestinatarios(splitDestinatarios(destinatario.getEmail()));
        content.setCc(null);
        content.setAsunto("Sepsa Factoring");
        content.setCuerpo(writer.toString());
        content.setContentType(EmailContentType.HTML);
        return content;
    }

    public String[] splitDestinatarios(String base) {
        String[] parts = base.split(Pattern.quote(";"));
        if (Assertions.isNullOrEmpty(parts)) {
            return new String[0];
        }

        Collection<String> destinatarios = Lists.empty();
        for (String part : parts) {
            part = part.trim();
            if (!Assertions.stringNullOrEmpty(part)) {
                destinatarios.add(part);
            }
        }

        return fa.gs.utils.collections.Arrays.unwrap(destinatarios, String.class);
    }

    /**
     * Genera una URL especifica que sirve para identificar las notificaciones
     * que fueron convertidas a HTML para lectura, de tal manera a marcar la
     * notificacion en si como leida.
     *
     * @param notificacion Notificacion.
     * @return URL que permite marcar a la notificacion en cuestion como leida.
     */
    private String generarUrlTracking(NotificacionEmailFactoring notificacion) {
        // Obtener valor configurado para enlace publico.
        Result<String> resUrl = beans.getConfiguraciones().buscarValorParametro(ConfiguracionGeneralEnum.SYS_URL_PUBLICA);
        String url = (resUrl.isSuccess()) ? resUrl.value() : "";

        // Enmascarar id de notificacion.
        String id = Ids.mask(notificacion.getId());

        // Generar valor aleatorio para evitar que la peticion sea cacheada.
        String uuid = Ids.randomUuid();

        // Formar url final.
        return String.format("%s%s?id=%s&o=%s", url, "/notificacion/track", id, uuid);
    }
    //</editor-fold>

    static PersonaInfo sepsa(Persona persona0) {
        PersonaInfo persona = new PersonaInfo();
        persona.setId(persona0.getId());
        persona.setRazonSocial(Text.razonSocial(persona0));
        persona.setRuc(Text.ruc(persona0));
        return persona;
    }

    static PersonaInfo comprador(FacturaInfo factura) {
        PersonaInfo persona = new PersonaInfo();
        persona.setId(factura.getCompradorId());
        persona.setRazonSocial(factura.getCompradorRazonSocial());
        persona.setRuc(factura.getCompradorRuc());
        return persona;
    }

    static PersonaInfo proveedor(FacturaInfo factura) {
        PersonaInfo persona = new PersonaInfo();
        persona.setId(factura.getProveedorId());
        persona.setRazonSocial(factura.getProveedorRazonSocial());
        persona.setRuc(factura.getProveedorRuc());
        return persona;
    }

    static PersonaInfo entidadFinanciera(FacturaInfo factura) {
        PersonaInfo persona = new PersonaInfo();
        persona.setId(factura.getEntidadFinancieraId());
        persona.setRazonSocial(factura.getEntidadFinancieraRazonSocial());
        persona.setRuc(factura.getEntidadFinancieraRuc());
        return persona;
    }

    private List<FacturaInfoTxt> wrap(List<FacturaInfo> facturas) {
        List<FacturaInfoTxt> facturasTxt = Lists.empty();
        if (!Assertions.isNullOrEmpty(facturas)) {
            for (FacturaInfo factura : facturas) {
                FacturaInfoTxt facturaTxt = new FacturaInfoTxt();
                facturaTxt.numero = factura.getNumero();
                facturaTxt.fechaEmision = Fechas.format(factura.getFechaEmision(), Formats.DD_MMM_YYYY).toUpperCase();
                facturaTxt.fechaVencimiento = Fechas.format(factura.getFechaPago(), Formats.DD_MMM_YYYY).toUpperCase();
                facturaTxt.montoTotal = Numeric.formatCurrency(factura.getMontoTotal(), "Gs");
                facturaTxt.montoTotalConformado = (factura.getMontoTotalConformado() == null) ? "N/D" : Numeric.formatCurrency(factura.getMontoTotalConformado(), "Gs");
                facturaTxt.montoTotalDescuento = (factura.getMontoTotalDescuento() == null) ? "N/D" : Numeric.formatCurrency(factura.getMontoTotalDescuento(), "Gs");
                facturaTxt.montoTotalDesembolso = (factura.getMontoTotalDesembolso() == null) ? "N/D" : Numeric.formatCurrency(factura.getMontoTotalDesembolso(), "Gs");
                facturasTxt.add(facturaTxt);
            }
        }
        return facturasTxt;
    }

    @Data
    public static class FacturaInfoTxt implements Serializable {

        private String numero;
        private String fechaEmision;
        private String fechaVencimiento;
        private String montoTotal;
        private String montoTotalConformado;
        private String montoTotalDescuento;
        private String montoTotalDesembolso;
    }

}
