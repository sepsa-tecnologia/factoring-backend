/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "pregunta_frecuente", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PreguntaFrecuente.findAll", query = "SELECT p FROM PreguntaFrecuente p"),
    @NamedQuery(name = "PreguntaFrecuente.findAllSorted", query = "SELECT p FROM PreguntaFrecuente p ORDER BY p.orden"),
    @NamedQuery(name = "PreguntaFrecuente.findById", query = "SELECT p FROM PreguntaFrecuente p WHERE p.id = :id"),
    @NamedQuery(name = "PreguntaFrecuente.findByOrden", query = "SELECT p FROM PreguntaFrecuente p WHERE p.orden = :orden"),
    @NamedQuery(name = "PreguntaFrecuente.findByPregunta", query = "SELECT p FROM PreguntaFrecuente p WHERE p.pregunta = :pregunta"),
    @NamedQuery(name = "PreguntaFrecuente.findByRespuesta", query = "SELECT p FROM PreguntaFrecuente p WHERE p.respuesta = :respuesta")})
public class PreguntaFrecuente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "orden")
    private int orden;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "pregunta")
    private String pregunta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "respuesta")
    private String respuesta;

    public PreguntaFrecuente() {
    }

    public PreguntaFrecuente(Integer id) {
        this.id = id;
    }

    public PreguntaFrecuente(Integer id, int orden, String pregunta, String respuesta) {
        this.id = id;
        this.orden = orden;
        this.pregunta = pregunta;
        this.respuesta = respuesta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof PreguntaFrecuente)) {
            return false;
        }
        PreguntaFrecuente other = (PreguntaFrecuente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.PreguntaFrecuente[ id=" + id + " ]";
    }

}
