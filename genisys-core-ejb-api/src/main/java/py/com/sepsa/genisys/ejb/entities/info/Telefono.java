/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
@Entity
@Table(name = "telefono", schema = "info")
@NamedQueries({
    @NamedQuery(name = "Telefono.obtenerPorId", query = "SELECT t from Telefono t WHERE t.id = :idTelefono")
})
public class Telefono implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_prefijo")
    private Integer idPrefijo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tipo")
    private Integer idTipo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "numero")
    private String numero;

    @Basic(optional = false)
    @NotNull
    @Column(name = "principal")
    private String principal;

    @Basic(optional = true)
    @Column(name = "id_cuenta")
    private Integer idCuenta;

}
