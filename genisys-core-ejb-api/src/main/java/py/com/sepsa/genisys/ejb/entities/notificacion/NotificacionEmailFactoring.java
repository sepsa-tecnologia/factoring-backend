/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.notificacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.genisys.ejb.entities.info.Email;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "notificacion_email_factoring", schema = "notificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotificacionEmailFactoring.findAll", query = "SELECT n FROM NotificacionEmailFactoring n"),
    @NamedQuery(name = "NotificacionEmailFactoring.findById", query = "SELECT n FROM NotificacionEmailFactoring n WHERE n.id = :id"),
    @NamedQuery(name = "NotificacionEmailFactoring.findByIdEmail", query = "SELECT n FROM NotificacionEmailFactoring n WHERE n.idEmail = :idEmail"),
    @NamedQuery(name = "NotificacionEmailFactoring.findByIdContacto", query = "SELECT n FROM NotificacionEmailFactoring n WHERE n.idContacto = :idContacto"),
    @NamedQuery(name = "NotificacionEmailFactoring.findByFechaInsercion", query = "SELECT n FROM NotificacionEmailFactoring n WHERE n.fechaInsercion = :fechaInsercion"),
    @NamedQuery(name = "NotificacionEmailFactoring.findByFechaEnvio", query = "SELECT n FROM NotificacionEmailFactoring n WHERE n.fechaEnvio = :fechaEnvio"),
    @NamedQuery(name = "NotificacionEmailFactoring.findByFechaEnviar", query = "SELECT n FROM NotificacionEmailFactoring n WHERE n.fechaEnviar = :fechaEnviar"),
    @NamedQuery(name = "NotificacionEmailFactoring.findByEstado", query = "SELECT n FROM NotificacionEmailFactoring n WHERE n.estado = :estado")})
public class NotificacionEmailFactoring implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_email")
    private int idEmail;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_contacto")
    private int idContacto;

    @Basic(optional = true)
    @Column(name = "contenido")
    private String contenido;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;

    @Column(name = "fecha_envio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;

    @Column(name = "fecha_enviar")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnviar;

    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private Character estado;

    @Basic(optional = false)
    @NotNull
    @Column(name = "leido")
    private Character leido;

    @JoinColumn(name = "id_plantilla", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Plantilla idPlantilla;

    // Campos adicionales.
    @JoinColumn(name = "id_email", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Email email;

    public NotificacionEmailFactoring() {
    }

    public NotificacionEmailFactoring(Integer id) {
        this.id = id;
    }

    public NotificacionEmailFactoring(Integer id, int idEmail, int idContacto, Date fechaInsercion, Character estado) {
        this.id = id;
        this.idEmail = idEmail;
        this.idContacto = idContacto;
        this.fechaInsercion = fechaInsercion;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(int idEmail) {
        this.idEmail = idEmail;
    }

    public int getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public Date getFechaEnviar() {
        return fechaEnviar;
    }

    public void setFechaEnviar(Date fechaEnviar) {
        this.fechaEnviar = fechaEnviar;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getLeido() {
        return leido;
    }

    public void setLeido(Character leido) {
        this.leido = leido;
    }

    public Plantilla getIdPlantilla() {
        return idPlantilla;
    }

    public void setIdPlantilla(Plantilla idPlantilla) {
        this.idPlantilla = idPlantilla;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof NotificacionEmailFactoring)) {
            return false;
        }
        NotificacionEmailFactoring other = (NotificacionEmailFactoring) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.notificacion.NotificacionEmailFactoring[ id=" + id + " ]";
    }

}
