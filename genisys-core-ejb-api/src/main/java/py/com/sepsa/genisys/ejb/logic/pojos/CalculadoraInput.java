/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.pojos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.info.Persona;

/**
 *
 * @author Fabio A. González Sosa
 */
public interface CalculadoraInput extends Serializable {

    Persona getProveedor();

    void setProveedor(Persona value);

    Persona getComprador();

    void setComprador(Persona value);

    Persona getEntidadFinanciera();

    void setEntidadFinanciera(Persona value);

    Date getFechaPagoFactura();

    void setFechaPagoFactura(Date value);

    Date getFechaAdelantoFactura();

    void setFechaAdelantoFactura(Date value);

    BigDecimal getMontoFactura();

    void setMontoFactura(BigDecimal value);

    @Data
    public static class Impl implements CalculadoraInput {

        /**
         * Proveedor seleccionado.
         */
        private Persona proveedor;

        /**
         * Comprador seleccionado.
         */
        private Persona comprador;

        /**
         * Entidad financiera seleccionada.
         */
        private Persona entidadFinanciera;

        /**
         * Fecha de pago de factura.
         */
        private Date fechaPagoFactura;

        /**
         * Fecha en que se realiza el adelanto de la factura.
         */
        private Date fechaAdelantoFactura;

        /**
         * Monto conformado de factura.
         */
        private BigDecimal montoFactura;
    }

}
