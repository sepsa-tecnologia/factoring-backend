/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.misc.Assertions;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java8.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.Convenio;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaConformada;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.FacturaConformadaFacade;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraInput;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraOutput;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculoDesembolsoFacturaInfo;
import py.com.sepsa.genisys.ejb.logic.pojos.SolicitudDesembolsoResultadoInfo;
import py.com.sepsa.genisys.ejb.utils.Text;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_FacturaVentaLote_Sepsa", mappedName = "PF_FacturaVentaLote_Sepsa")
@LocalBean
public class PF_FacturaVentaLote_Sepsa extends PF_FacturaVentaLote_Base {

    @EJB
    private Convenios convenios;

    @EJB
    private FacturaConformadaFacade facturaConformadaFacade;

    public Collection<CalculoDesembolsoFacturaInfo> calcularDesembolsos(Usuario usuario, Perfil perfil, List<FacturaInfo> facturas, Persona entidadFinanciera, Date fechaAdelanto) throws Throwable {
        final Collection<CalculoDesembolsoFacturaInfo> calculos = Lists.empty();

        Calculador calculador = new Calculador_Sepsa();

        // Realizar calculos.
        for (FacturaInfo factura : facturas) {
            // Datos iniciales.
            Integer idEntidadFinanciera = entidadFinanciera.getId();
            Integer idComprador = factura.getCompradorId();
            Persona comprador = personas.obtenerPorId(idComprador).value();
            Integer idProveedor = factura.getProveedorId();
            Persona proveedor = personas.obtenerPorId(idProveedor).value();

            // Validar convenio.
            String err = validarConvenio(idComprador, idProveedor, idEntidadFinanciera);
            if (err != null) {
                CalculoDesembolsoFacturaInfo calculo = new CalculoDesembolsoFacturaInfo();
                calculo.setOk(false);
                calculo.setMessage(err);
                calculo.setFactura(factura);
                calculos.add(calculo);
                continue;
            }

            // Adaptar datos de entrada.
            CalculadoraInput input = new CalculadoraInput.Impl();
            input.setComprador(comprador);
            input.setProveedor(proveedor);
            input.setEntidadFinanciera(entidadFinanciera);
            input.setFechaAdelantoFactura(fechaAdelanto);
            input.setFechaPagoFactura(factura.getFechaPago());
            input.setMontoFactura(factura.getMontoTotalConformado());

            // Calcular.
            calculador.setCalculadoraInput(input);
            calculador.calcular();
            CalculadoraOutput output = calculador.getCalculadoraOutput();
            Calculador_Sepsa.Extras extras = (Calculador_Sepsa.Extras) output.getExtras();

            // Adatpar datos de salida.
            CalculoDesembolsoFacturaInfo calculo = new CalculoDesembolsoFacturaInfo();
            calculo.setOk(true);
            calculo.setMessage("OK");
            calculo.setFactura(factura);
            calculo.setConvenio(extras.getConvenio());
            calculo.setDiasAnticipo(output.getDiasAdelantados());
            calculo.setPorcentajeDescuento(output.getPorcentajeTotalComision());
            calculo.setMontoDescuento(output.getMontoTotalComision());
            calculo.setMontoDistribucion(output.getMontoTotalComision());
            calculo.setMontoDesembolso(output.getMontoTotalDesembolso());
            calculo.setExtras(extras);
            calculos.add(calculo);
        }

        return calculos;
    }

    private String validarConvenio(Integer idComprador, Integer idProveedor, Integer idEntidadFinanciera) {
        // Obtener conenvios.
        Result<Collection<Convenio>> resConvenios = convenios.obtenerConvenios(idComprador, idProveedor, idEntidadFinanciera);
        if (resConvenios.isFailure()) {
            return resConvenios.failure().message();
        }

        // Convenios.
        Collection<Convenio> convenios0 = resConvenios.value(Lists.empty());

        // Control.
        if (Assertions.isNullOrEmpty(convenios0)) {
            return "No existe un convenio disponible con esta entidad para el desembolso de las facturas.";
        }

        // Control.
        if (convenios0.size() > 1) {
            return "Existe más de un convenio aplicable para la solicitud de desembolso de estas facturas.";
        }

        return null;
    }

    public SolicitudDesembolsoResultadoInfo vender(Usuario usuario, Perfil perfil, Integer idLote, Collection<CalculoDesembolsoFacturaInfo> calculos, Persona entidadFinanciera) {
        // Datos iniciales.
        Integer idPerfilComprador = perfiles.obtenerPerfil(PerfilEnum.COMPRADOR).getId();
        Integer idPerfilProveedor = perfiles.obtenerPerfil(PerfilEnum.PROVEEDOR).getId();
        Integer idEntidadFinanciera = entidadFinanciera.getId();

        // Agrupar facturas por comprador.
        List<FacturaInfo> facturas = fa.gs.utils.collections.Lists.stream(calculos).map(c -> c.getFactura()).collect(Collectors.toList());
        Map<Integer, List<FacturaInfo>> grupos = this.facturas.agruparPorComprador(facturas);
        for (Map.Entry<Integer, List<FacturaInfo>> grupo : grupos.entrySet()) {
            // Datos de grupo de facturas.
            facturas = grupo.getValue();
            Integer idComprador = grupo.getKey();
            Persona comprador = personas.obtenerPorId(idComprador).value();
            Integer idProveedor = fa.gs.utils.collections.Lists.first(facturas).getProveedorId();
            Persona proveedor = personas.obtenerPorId(idProveedor).value();

            if (!Assertions.isNullOrEmpty(facturas)) {
                for (FacturaInfo factura : facturas) {
                    // Cambiar estado de factura.
                    Integer idFactura = factura.getId();
                    Result<?> resCambioEstado = cambiarEstadoFactura(usuario.getId(), idFactura, Estados.EstadoFacturaEnum.VENDIDA, "");
                    if (resCambioEstado.isFailure()) {
                        String msg = fa.gs.utils.misc.text.Text.select(resCambioEstado.failure().message(), resCambioEstado.failure().cause().getMessage());
                        return venderKo("Ocurrió un error modificando estado para factura '%s' de comprador '%s'. %s.", factura.getNumero(), Text.razonSocial(comprador), msg);
                    }

                    // Consolidar valores de venta.
                    CalculoDesembolsoFacturaInfo calculo = buscarCalculo(calculos, factura);
                    Result<?> resModificacionFacturaConformada = modificarFacturaConformadaAsociada(calculo);
                    if (resModificacionFacturaConformada.isFailure()) {
                        String msg = fa.gs.utils.misc.text.Text.select(resModificacionFacturaConformada.failure().message(), resModificacionFacturaConformada.failure().cause().getMessage());
                        return venderKo("Ocurrió un error modificando valores de desembolso para factura '%s' de comprador '%s'. %s.", factura.getNumero(), Text.razonSocial(comprador), msg);
                    }
                }

                // Emitir notificaciones.
                notificaciones.notificarVentaDeFacturas(facturas);
            }
        }

        // Ok.
        SolicitudDesembolsoResultadoInfo instance = new SolicitudDesembolsoResultadoInfo();
        instance.setOk(true);
        instance.setErr(null);
        return instance;
    }

    private Result<Void> modificarFacturaConformadaAsociada(CalculoDesembolsoFacturaInfo info) {
        Result<Void> result;

        try {
            Calculador_Sepsa.Extras extras = (Calculador_Sepsa.Extras) info.getExtras();
            FacturaConformada facturaConformada = facturaConformadaFacade.findByIdFactura(info.getFactura().getId());
            facturaConformada.setIdEntidadFinanciera(info.getConvenio().getIdEntidadFinanciera());
            facturaConformada.setIdConvenio(info.getConvenio());
            facturaConformada.setFechaVenta(py.com.sepsa.genisys.ejb.utils.Fechas.ahora());
            facturaConformada.setPorcentajeComision(info.getPorcentajeDescuento());
            facturaConformada.setDiasAnticipo(info.getDiasAnticipo());
            facturaConformada.setMontoNeto(info.getMontoDesembolso());
            facturaConformada.setMontoImponibleComision(extras.getMontoImponibleComision());
            facturaConformada.setMontoImpuestoComision(extras.getMontoImpuestoComision());
            facturaConformada.setMontoTotalComision(extras.getMontoTotalComision());
            facturaConformada.setMontoDistribucion(info.getMontoDistribucion());
            facturaConformada.setMontoImponibleGastosOperativos(extras.getMontoImponibleGastosOperativos());
            facturaConformada.setMontoImpuestoGastosOperativos(extras.getMontoImpuestoGastosOperativos());
            facturaConformada.setMontoTotalGastosOperativos(extras.getMontoTotalGastosOperativos());
            facturaConformadaFacade.edit(facturaConformada);
            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error modificando registro de factura conformada para factura id='%s'", info.getFactura().getId()).build();
        }

        return result;
    }

}
