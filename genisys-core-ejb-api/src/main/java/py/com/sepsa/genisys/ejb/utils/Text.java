/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import fa.gs.result.utils.Failure;
import fa.gs.utils.misc.Assertions;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Text {

    /**
     * Obtiene una cadena generica que representa un dato vacio o no existente.
     *
     * @return Texto.
     */
    public static String nd() {
        return "N/D";
    }

    public static String error(Failure failure) {
        if (!Assertions.stringNullOrEmpty(failure.message())) {
            return failure.message();
        }

        return error(failure.cause());
    }

    public static String error(Throwable thr) {
        if (thr == null) {
            return nd();
        }

        if (!Assertions.stringNullOrEmpty(thr.getMessage())) {
            return thr.getMessage();
        }

        if (!Assertions.stringNullOrEmpty(thr.getLocalizedMessage())) {
            return thr.getLocalizedMessage();
        }

        return error(thr.getCause());
    }

    /**
     * Permite obtener la razon social de una persona como cadena.
     *
     * @param persona Persona.
     * @return Razon Social como cadena.
     */
    public static String razonSocial(Persona persona) {
        if (persona == null) {
            return nd();
        }
        return persona.getRazonSocial();
    }

    /**
     * Permite obtener el ruc de una persona como cadena.
     *
     * @param persona Persona.
     * @return Ruc como cadena.
     */
    public static String ruc(Persona persona) {
        // Control.
        if (persona == null) {
            return nd();
        }

        // Ruc sin digito verificador.
        String ruc = persona.getRuc();
        if (Assertions.stringNullOrEmpty(ruc)) {
            return nd();
        }

        // Digito verificador.
        if (persona.getDvRuc() != null) {
            return String.format("%s-%s", ruc, persona.getDvRuc().toString());
        } else {
            return ruc;
        }
    }

    public static String ci(Persona persona) {
        // Control.
        if (persona == null) {
            return nd();
        }

        // Ci.
        String ci = persona.getPfCi();
        if (Assertions.stringNullOrEmpty(ci)) {
            return nd();
        }

        return ci;
    }

    public static String tipoPersona(Persona persona) {
        Personas.TipoPersonaEnum tipoPersona = Personas.TipoPersonaEnum.fromIdTipoPersona(persona.getIdTipoPersona());
        return tipoPersona.descripcion();
    }

}
