/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.enums;

import fa.gs.utils.misc.Codificable;
import java.util.Objects;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum FacturaEstadoSeguimientoLiquidacionEnum implements Codificable {
    PENDIENTE("0001", "Pendiente"),
    LIQUIDADA("0002", "Liquidada"),
    RECHAZADA("0003", "Rechazada");
    private final String codigo;
    private final String descripcion;

    private FacturaEstadoSeguimientoLiquidacionEnum(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static FacturaEstadoSeguimientoLiquidacionEnum fromCodigo(String codigo) {
        for (FacturaEstadoSeguimientoLiquidacionEnum value : FacturaEstadoSeguimientoLiquidacionEnum.values()) {
            if (Objects.equals(value.codigo, codigo)) {
                return value;
            }
        }
        return null;
    }

    @Override
    public String codigo() {
        return codigo;
    }

    @Override
    public String descripcion() {
        return descripcion;
    }

}
