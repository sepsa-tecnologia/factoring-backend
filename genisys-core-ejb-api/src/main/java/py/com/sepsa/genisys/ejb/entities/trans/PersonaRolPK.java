/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Fabio A. González Sosa
 */
@Embeddable
public class PersonaRolPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_persona")
    private int idPersona;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_rol")
    private int idRol;

    public PersonaRolPK() {
    }

    public PersonaRolPK(int idPersona, int idRol) {
        this.idPersona = idPersona;
        this.idRol = idRol;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public int getIdRol() {
        return idRol;
    }

    public void setIdRol(int idRol) {
        this.idRol = idRol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPersona;
        hash += (int) idRol;
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof PersonaRolPK)) {
            return false;
        }
        PersonaRolPK other = (PersonaRolPK) object;
        if (this.idPersona != other.idPersona) {
            return false;
        }
        if (this.idRol != other.idRol) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.trans.PersonaRolPK[ idPersona=" + idPersona + ", idRol=" + idRol + " ]";
    }

}
