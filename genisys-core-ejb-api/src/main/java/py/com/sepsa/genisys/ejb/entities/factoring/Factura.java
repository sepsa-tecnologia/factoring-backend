/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;
import py.com.sepsa.genisys.ejb.entities.info.Persona;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "factura", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Factura.findAll", query = "SELECT f FROM Factura f"),
    @NamedQuery(name = "Factura.findById", query = "SELECT f FROM Factura f WHERE f.id = :id"),
    @NamedQuery(name = "Factura.findByIdMoneda", query = "SELECT f FROM Factura f WHERE f.idMoneda = :idMoneda"),
    @NamedQuery(name = "Factura.findByIdDocumento", query = "SELECT f FROM Factura f WHERE f.idDocumento = :idDocumento"),
    @NamedQuery(name = "Factura.findByIdTipoDocumento", query = "SELECT f FROM Factura f WHERE f.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "Factura.findByNroFactura", query = "SELECT f FROM Factura f WHERE f.nroFactura = :nroFactura"),
    @NamedQuery(name = "Factura.findByNroTimbrado", query = "SELECT f FROM Factura f WHERE f.nroTimbrado = :nroTimbrado"),
    @NamedQuery(name = "Factura.findByVtoTimbrado", query = "SELECT f FROM Factura f WHERE f.vtoTimbrado = :vtoTimbrado"),
    @NamedQuery(name = "Factura.findByRazonSocialComprador", query = "SELECT f FROM Factura f WHERE f.razonSocialComprador = :razonSocialComprador"),
    @NamedQuery(name = "Factura.findByDireccionComprador", query = "SELECT f FROM Factura f WHERE f.direccionComprador = :direccionComprador"),
    @NamedQuery(name = "Factura.findByTelefonoComprador", query = "SELECT f FROM Factura f WHERE f.telefonoComprador = :telefonoComprador"),
    @NamedQuery(name = "Factura.findByRazonSocialProveedor", query = "SELECT f FROM Factura f WHERE f.razonSocialProveedor = :razonSocialProveedor"),
    @NamedQuery(name = "Factura.findByDireccionProveedor", query = "SELECT f FROM Factura f WHERE f.direccionProveedor = :direccionProveedor"),
    @NamedQuery(name = "Factura.findByTelefonoProveedor", query = "SELECT f FROM Factura f WHERE f.telefonoProveedor = :telefonoProveedor"),
    @NamedQuery(name = "Factura.findByFecha", query = "SELECT f FROM Factura f WHERE f.fecha = :fecha"),
    @NamedQuery(name = "Factura.findByFechaPago", query = "SELECT f FROM Factura f WHERE f.fechaPago = :fechaPago"),
    @NamedQuery(name = "Factura.findByDiasCredito", query = "SELECT f FROM Factura f WHERE f.diasCredito = :diasCredito"),
    @NamedQuery(name = "Factura.findByMontoIva5", query = "SELECT f FROM Factura f WHERE f.montoIva5 = :montoIva5"),
    @NamedQuery(name = "Factura.findByMontoImponible5", query = "SELECT f FROM Factura f WHERE f.montoImponible5 = :montoImponible5"),
    @NamedQuery(name = "Factura.findByMontoTotal5", query = "SELECT f FROM Factura f WHERE f.montoTotal5 = :montoTotal5"),
    @NamedQuery(name = "Factura.findByMontoIva10", query = "SELECT f FROM Factura f WHERE f.montoIva10 = :montoIva10"),
    @NamedQuery(name = "Factura.findByMontoImponible10", query = "SELECT f FROM Factura f WHERE f.montoImponible10 = :montoImponible10"),
    @NamedQuery(name = "Factura.findByMontoTotal10", query = "SELECT f FROM Factura f WHERE f.montoTotal10 = :montoTotal10"),
    @NamedQuery(name = "Factura.findByMontoTotalExento", query = "SELECT f FROM Factura f WHERE f.montoTotalExento = :montoTotalExento"),
    @NamedQuery(name = "Factura.findByMontoIvaTotal", query = "SELECT f FROM Factura f WHERE f.montoIvaTotal = :montoIvaTotal"),
    @NamedQuery(name = "Factura.findByMontoImponibleTotal", query = "SELECT f FROM Factura f WHERE f.montoImponibleTotal = :montoImponibleTotal"),
    @NamedQuery(name = "Factura.findByMontoTotalFactura", query = "SELECT f FROM Factura f WHERE f.montoTotalFactura = :montoTotalFactura"),
    @NamedQuery(name = "Factura.findByHash", query = "SELECT f FROM Factura f WHERE f.hash = :hash")})
@NamedNativeQueries({
    @NamedNativeQuery(name = "Factura.countAllADesembolsar", resultSetMapping = "FacturaCountMapping", query = "select count(*) as cantidad from factoring.factura as f where f.id_estado = 4 and f.id in (select distinct(id) from factoring.factura_conformada where id_convenio in (select id from factoring.convenio where id_entidad_financiera = :idEntidadFinanciera))"),
    @NamedNativeQuery(name = "Factura.countAllPagadasAVencer", resultSetMapping = "FacturaCountMapping", query = "select count(*) as cantidad from factoring.factura as f where f.id_estado = 5 and f.id in (select distinct(id) from factoring.factura_conformada where id_convenio in (select id from factoring.convenio where id_entidad_financiera = :idEntidadFinanciera)) and DATE_PART('day', cast(CURRENT_TIMESTAMP as TIMESTAMP) - cast(f.fecha_pago as TIMESTAMP)) < 0"),
    @NamedNativeQuery(name = "Factura.countAllPagadasVencidas", resultSetMapping = "FacturaCountMapping", query = "select count(*) as cantidad from factoring.factura as f where f.id_estado = 5 and f.id in (select distinct(id) from factoring.factura_conformada where id_convenio in (select id from factoring.convenio where id_entidad_financiera = :idEntidadFinanciera)) and DATE_PART('day', cast(CURRENT_TIMESTAMP as TIMESTAMP) - cast(f.fecha_pago as TIMESTAMP)) > 0"),
    @NamedNativeQuery(name = "Factura.countAllPagadasAVencerHoy", resultSetMapping = "FacturaCountMapping", query = "select count(*) as cantidad from factoring.factura as f where f.id_estado = 5 and f.id in (select distinct(id) from factoring.factura_conformada where id_convenio in (select id from factoring.convenio where id_entidad_financiera = :idEntidadFinanciera)) and DATE_PART('day', cast(CURRENT_TIMESTAMP as TIMESTAMP) - cast(f.fecha_pago as TIMESTAMP)) = 0"),
    @NamedNativeQuery(name = "Factura.findAllPagadasAVencer", resultSetMapping = "FacturaWithVencimientoMapping", query = "select  f.id, f.nro_factura, f.razon_social_proveedor, f.ruc_proveedor, f.razon_social_comprador, f.ruc_comprador, f.fecha_pago, f.monto_total_factura, DATE_PART('day', cast(CURRENT_TIMESTAMP as TIMESTAMP) - cast(f.fecha_pago as TIMESTAMP)) * -1 + 1 AS vencimiento from factoring.factura as f where f.id_estado = 5 and f.id in (select distinct(id) from factoring.factura_conformada where id_convenio in (select id from factoring.convenio where id_entidad_financiera = :idEntidadFinanciera)) and DATE_PART('day', cast(CURRENT_TIMESTAMP as TIMESTAMP) - cast(f.fecha_pago as TIMESTAMP)) < 0 ORDER BY vencimiento ASC LIMIT :limit"),
    @NamedNativeQuery(name = "Factura.findAllPagadasVencidas", resultSetMapping = "FacturaWithVencimientoMapping", query = "select  f.id, f.nro_factura, f.razon_social_proveedor, f.ruc_proveedor, f.razon_social_comprador, f.ruc_comprador, f.fecha_pago, f.monto_total_factura, DATE_PART('day', cast(CURRENT_TIMESTAMP as TIMESTAMP) - cast(f.fecha_pago as TIMESTAMP)) AS vencimiento from factoring.factura as f where f.id_estado = 5 and f.id in (select distinct(id) from factoring.factura_conformada where id_convenio in (select id from factoring.convenio where id_entidad_financiera = :idEntidadFinanciera)) and DATE_PART('day', cast(CURRENT_TIMESTAMP as TIMESTAMP) - cast(f.fecha_pago as TIMESTAMP)) > 0 ORDER BY vencimiento DESC LIMIT :limit"),
    @NamedNativeQuery(name = "Factura.findAllPagadasAVencerHoy", resultSetMapping = "FacturaWithVencimientoMapping", query = "select  f.id, f.nro_factura, f.razon_social_proveedor, f.ruc_proveedor, f.razon_social_comprador, f.ruc_comprador, f.fecha_pago, f.monto_total_factura, 0 AS vencimiento from factoring.factura as f where f.id_estado = 5 and f.id in (select distinct(id) from factoring.factura_conformada where id_convenio in (select id from factoring.convenio where id_entidad_financiera = :idEntidadFinanciera)) and DATE_PART('day', cast(CURRENT_TIMESTAMP as TIMESTAMP) - cast(f.fecha_pago as TIMESTAMP)) = 0 LIMIT :limit")
})
@SqlResultSetMappings({
    @SqlResultSetMapping(name = "FacturaCountMapping",
            classes = @ConstructorResult(
                    targetClass = Factura.class,
                    columns = {
                        @ColumnResult(name = "cantidad", type = Integer.class)
                    }
            )),
    @SqlResultSetMapping(name = "FacturaWithVencimientoMapping",
            classes = @ConstructorResult(
                    targetClass = Factura.class,
                    columns = {
                        @ColumnResult(name = "id", type = Integer.class),
                        @ColumnResult(name = "nro_factura", type = String.class),
                        @ColumnResult(name = "razon_social_proveedor", type = String.class),
                        @ColumnResult(name = "ruc_proveedor", type = String.class),
                        @ColumnResult(name = "razon_social_comprador", type = String.class),
                        @ColumnResult(name = "ruc_comprador", type = String.class),
                        @ColumnResult(name = "monto_total_factura", type = BigDecimal.class),
                        @ColumnResult(name = "fecha_pago", type = Date.class),
                        @ColumnResult(name = "vencimiento", type = Integer.class)
                    }
            ))
})
public class Factura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_moneda")
    private int idMoneda;

    @Basic(optional = true)
    @Column(name = "id_documento")
    private Integer idDocumento;

    @Basic(optional = true)
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;

    @Column(name = "id_persona_origen")
    private Integer idPersonaOrigen;

    @Column(name = "id_persona_destino")
    private Integer idPersonaDestino;

    @Basic(optional = false)
    @NotNull
    @Column(name = "nro_factura")
    private String nroFactura;

    @Column(name = "nro_timbrado")
    private String nroTimbrado;

    @Column(name = "vto_timbrado")
    private String vtoTimbrado;

    @Basic(optional = false)
    @NotNull
    @Column(name = "razon_social_comprador")
    private String razonSocialComprador;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ruc_comprador")
    private String rucComprador;

    @Column(name = "direccion_comprador")
    private String direccionComprador;

    @Column(name = "telefono_comprador")
    private String telefonoComprador;

    @Basic(optional = false)
    @NotNull
    @Column(name = "razon_social_proveedor")
    private String razonSocialProveedor;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ruc_proveedor")
    private String rucProveedor;

    @Column(name = "direccion_proveedor")
    private String direccionProveedor;

    @Column(name = "telefono_proveedor")
    private String telefonoProveedor;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_pago")
    @Temporal(TemporalType.DATE)
    private Date fechaPago;

    @Basic(optional = false)
    @NotNull
    @Column(name = "dias_credito")
    private int diasCredito;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_5")
    private BigDecimal montoIva5;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_5")
    private BigDecimal montoImponible5;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_5")
    private BigDecimal montoTotal5;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_10")
    private BigDecimal montoIva10;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_10")
    private BigDecimal montoImponible10;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_10")
    private BigDecimal montoTotal10;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_exento")
    private BigDecimal montoTotalExento;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_total")
    private BigDecimal montoIvaTotal;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_total")
    private BigDecimal montoImponibleTotal;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_factura")
    private BigDecimal montoTotalFactura;

    @Size(max = 2147483647)
    @Column(name = "hash")
    private String hash;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFactura", fetch = FetchType.LAZY)
    private Collection<HistoricoFactura> historicoFacturaCollection;

    @JoinColumn(name = "id_estado", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Estado idEstado;

    @JoinColumn(name = "id_lote", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Lote idLote;

    // Campos adicionales.
    @Transient
    private Integer vencimiento;

    @Transient
    private Integer cantidad;

    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = true, fetch = FetchType.LAZY)
    private FacturaConformada facturaConformada;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_persona_origen", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Persona personaOrigen;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_persona_destino", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Persona personaDestino;

    @JoinColumn(name = "id_moneda", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Moneda moneda;

    public Factura() {
    }

    public Factura(Integer id, int idMoneda, String nroFactura, String razonSocialComprador, String rucComprador, String razonSocialProveedor, String rucProveedor, Date fecha, Date fechaPago, int diasCredito, BigDecimal montoIva5, BigDecimal montoImponible5, BigDecimal montoTotal5, BigDecimal montoIva10, BigDecimal montoImponible10, BigDecimal montoTotal10, BigDecimal montoTotalExento, BigDecimal montoIvaTotal, BigDecimal montoImponibleTotal, BigDecimal montoTotalFactura) {
        this.id = id;
        this.idMoneda = idMoneda;
        this.nroFactura = nroFactura;
        this.razonSocialComprador = razonSocialComprador;
        this.rucComprador = rucComprador;
        this.razonSocialProveedor = razonSocialProveedor;
        this.rucProveedor = rucProveedor;
        this.fecha = fecha;
        this.fechaPago = fechaPago;
        this.diasCredito = diasCredito;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalFactura = montoTotalFactura;
    }

    // Constructor para FacturaCountMapping.
    public Factura(Integer cantidad) {
        this.cantidad = cantidad;
    }

    // Constructor para FacturaWithVencimientoMapping.
    public Factura(Integer id, String nroFactura, String razonSocialProveedor, String rucProveedor, String razonSocialComprador, String rucComprador, BigDecimal montoTotalFactura, Date fechaPago, Integer vencimiento) {
        this.id = id;
        this.nroFactura = nroFactura;
        this.razonSocialProveedor = razonSocialProveedor;
        this.rucProveedor = rucProveedor;
        this.razonSocialComprador = razonSocialComprador;
        this.rucComprador = rucComprador;
        this.montoTotalFactura = montoTotalFactura;
        this.fechaPago = fechaPago;
        this.vencimiento = vencimiento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Integer getIdPersonaOrigen() {
        return idPersonaOrigen;
    }

    public void setIdPersonaOrigen(Integer idPersonaOrigen) {
        this.idPersonaOrigen = idPersonaOrigen;
    }

    public Integer getIdPersonaDestino() {
        return idPersonaDestino;
    }

    public void setIdPersonaDestino(Integer idPersonaDestino) {
        this.idPersonaDestino = idPersonaDestino;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public String getVtoTimbrado() {
        return vtoTimbrado;
    }

    public void setVtoTimbrado(String vtoTimbrado) {
        this.vtoTimbrado = vtoTimbrado;
    }

    public String getRazonSocialComprador() {
        return razonSocialComprador;
    }

    public void setRazonSocialComprador(String razonSocialComprador) {
        this.razonSocialComprador = razonSocialComprador;
    }

    public String getRucComprador() {
        return rucComprador;
    }

    public void setRucComprador(String rucComprador) {
        this.rucComprador = rucComprador;
    }

    public String getDireccionComprador() {
        return direccionComprador;
    }

    public void setDireccionComprador(String direccionComprador) {
        this.direccionComprador = direccionComprador;
    }

    public String getTelefonoComprador() {
        return telefonoComprador;
    }

    public void setTelefonoComprador(String telefonoComprador) {
        this.telefonoComprador = telefonoComprador;
    }

    public String getRazonSocialProveedor() {
        return razonSocialProveedor;
    }

    public void setRazonSocialProveedor(String razonSocialProveedor) {
        this.razonSocialProveedor = razonSocialProveedor;
    }

    public String getRucProveedor() {
        return rucProveedor;
    }

    public void setRucProveedor(String rucProveedor) {
        this.rucProveedor = rucProveedor;
    }

    public String getDireccionProveedor() {
        return direccionProveedor;
    }

    public void setDireccionProveedor(String direccionProveedor) {
        this.direccionProveedor = direccionProveedor;
    }

    public String getTelefonoProveedor() {
        return telefonoProveedor;
    }

    public void setTelefonoProveedor(String telefonoProveedor) {
        this.telefonoProveedor = telefonoProveedor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public int getDiasCredito() {
        return diasCredito;
    }

    public void setDiasCredito(int diasCredito) {
        this.diasCredito = diasCredito;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalFactura() {
        return montoTotalFactura;
    }

    public void setMontoTotalFactura(BigDecimal montoTotalFactura) {
        this.montoTotalFactura = montoTotalFactura;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @XmlTransient
    public Collection<HistoricoFactura> getHistoricoFacturaCollection() {
        return historicoFacturaCollection;
    }

    public void setHistoricoFacturaCollection(Collection<HistoricoFactura> historicoFacturaCollection) {
        this.historicoFacturaCollection = historicoFacturaCollection;
    }

    public Estado getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estado idEstado) {
        this.idEstado = idEstado;
    }

    public Lote getIdLote() {
        return idLote;
    }

    public void setIdLote(Lote idLote) {
        this.idLote = idLote;
    }

    public FacturaConformada getFacturaConformada() {
        return facturaConformada;
    }

    public void setFacturaConformada(FacturaConformada facturaConformada) {
        this.facturaConformada = facturaConformada;
    }

    public Integer getVencimiento() {
        return vencimiento;
    }

    public void setVencimiento(Integer vencimiento) {
        this.vencimiento = vencimiento;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Persona getPersonaOrigen() {
        return personaOrigen;
    }

    public void setPersonaOrigen(Persona personaOrigen) {
        this.personaOrigen = personaOrigen;
    }

    public Persona getPersonaDestino() {
        return personaDestino;
    }

    public void setPersonaDestino(Persona personaDestino) {
        this.personaDestino = personaDestino;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Factura)) {
            return false;
        }
        Factura other = (Factura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.Factura[ id=" + id + " ]";
    }

}
