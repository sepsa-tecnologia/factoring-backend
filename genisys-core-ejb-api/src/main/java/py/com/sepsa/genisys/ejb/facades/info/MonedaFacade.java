/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.info;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author descauriza
 */
@Stateless(name = "MonedaFacade", mappedName = "MonedaFacade")
@LocalBean
public class MonedaFacade extends AbstractFacade<Moneda> {

    public MonedaFacade() {
        super(Moneda.class);
    }

    /**
     * Retorna el objeto moneda correspondiente al Guaraní.
     *
     * @param monedas Lista de monedas previamente obtenidas.
     * @return Objeto moneda guaraní.
     */
    public Moneda getGuarani(List<Moneda> monedas) {
        for (Moneda moneda : monedas) {
            if (moneda.getAbreviatura() != null && moneda.getAbreviatura().toLowerCase().equals("gs")) {
                return moneda;
            }
            // El prefijo más comun es 'guaran' siendo la palabra concreta 'guaran(í|íes|i|ies)' sujeto a reglas ortográficas.
            if (moneda.getDescripcion() != null && moneda.getDescripcion().toLowerCase().contains("guaran")) {
                return moneda;
            }
        }
        return null;
    }

    /**
     * Retorna el objeto moneda correspondiente al Guaraní.
     *
     * @return Objeto moneda guaraní.
     */
    public Moneda getGuarani() {
        List<Moneda> monedas = this.findAll();
        return getGuarani(monedas);
    }

}
