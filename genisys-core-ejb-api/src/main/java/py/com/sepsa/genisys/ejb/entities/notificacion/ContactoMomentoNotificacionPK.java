/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.notificacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Fabio A. González Sosa
 */
@Embeddable
public class ContactoMomentoNotificacionPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_contacto")
    private int idContacto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_momento_notificacion")
    private int idMomentoNotificacion;

    public ContactoMomentoNotificacionPK() {
    }

    public ContactoMomentoNotificacionPK(int idContacto, int idMomentoNotificacion) {
        this.idContacto = idContacto;
        this.idMomentoNotificacion = idMomentoNotificacion;
    }

    public int getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

    public int getIdMomentoNotificacion() {
        return idMomentoNotificacion;
    }

    public void setIdMomentoNotificacion(int idMomentoNotificacion) {
        this.idMomentoNotificacion = idMomentoNotificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idContacto;
        hash += (int) idMomentoNotificacion;
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ContactoMomentoNotificacionPK)) {
            return false;
        }
        ContactoMomentoNotificacionPK other = (ContactoMomentoNotificacionPK) object;
        if (this.idContacto != other.idContacto) {
            return false;
        }
        if (this.idMomentoNotificacion != other.idMomentoNotificacion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.notificacion.ContactoMomentoNotificacionPK[ idContacto=" + idContacto + ", idMomentoNotificacion=" + idMomentoNotificacion + " ]";
    }

}
