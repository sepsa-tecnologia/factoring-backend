/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.info;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "entidad_financiera", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EntidadFinanciera.findAll", query = "SELECT e FROM EntidadFinanciera e"),
    @NamedQuery(name = "EntidadFinanciera.findByIdEntidadFinanciera", query = "SELECT e FROM EntidadFinanciera e WHERE e.idEntidadFinanciera = :idEntidadFinanciera")})
public class EntidadFinanciera implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_entidad_financiera")
    private Integer idEntidadFinanciera;

    public EntidadFinanciera() {
    }

    public EntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.idEntidadFinanciera);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntidadFinanciera other = (EntidadFinanciera) obj;
        if (!Objects.equals(this.idEntidadFinanciera, other.idEntidadFinanciera)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EntidadFinanciera{" + "idEntidadFinanciera=" + idEntidadFinanciera + '}';
    }

}
