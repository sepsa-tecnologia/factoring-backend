/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.sepsa;

import fa.gs.utils.misc.Ids;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ClienteEdiServerImplDummy implements ClienteEdiServer {

    private ClienteEdiServerImplDummy() {
        ;
    }

    public static ClienteEdiServer instance() {
        ClienteEdiServerImplDummy instance = new ClienteEdiServerImplDummy();
        return instance;
    }

    @Override
    public Result<String> obtenerTokenAutenticacion() {
        return Results.ok()
                .value(Ids.randomUuid().toUpperCase())
                .build();
    }

    @Override
    public Result<RegistrarLiquidacionOutput> registrarLiquidacion(String token, RegistrarLiquidacionInput input) {
        RegistrarLiquidacionOutput output = new RegistrarLiquidacionOutput();
        return Results.ok()
                .value(output)
                .build();
    }

    @Override
    public Result<Integer> obtenerOCrearIdClienteSepsa(String token, String ruc, String razonSocial, Personas.TipoPersonaEnum tipoPersona) {
        return Results.ok().
                value(1)
                .build();
    }

}
