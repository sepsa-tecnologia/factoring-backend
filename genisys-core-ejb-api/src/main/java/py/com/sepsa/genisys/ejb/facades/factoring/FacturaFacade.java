/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.Estado;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "FacturaFacade", mappedName = "FacturaFacade")
@LocalBean
public class FacturaFacade extends AbstractFacade<Factura> {

    // <editor-fold desc="Facades.">
    @EJB
    private EstadoFacade estadoFacade;

    @EJB
    private Personas personas;
    // </editor-fold>

    /**
     * Constructor.
     */
    public FacturaFacade() {
        super(Factura.class);
    }

    /**
     * Obtiene los datos de una factura en base a un hash que representa al
     * mismo.
     *
     * @param hash Hash de los datos de la factura que se desea recuperar.
     * @return Objeto factura, si el hash coincide. Caso contrario null.
     */
    public Factura findByHash(String hash) {
        Query q = getEntityManager().createNamedQuery("Factura.findByHash");
        q.setParameter("hash", hash);
        return JpaUtils.getFirst(q);
    }

    /**
     * Obtiene la lista de facturas donde la persona indicada aparece como
     * origen de la factura.
     *
     * @param idPersona Identificador de persona.
     * @return Lista de facturas encontradas.
     */
    public List<Factura> findByPersonaOrigen(Integer idPersona) {
        Query q = getEntityManager().createNativeQuery("select f.* from factoring.factura f join trans.documento d on f.id_documento = d.id where d.id_persona_origen = ?idPersonaOrigen", Factura.class);
        q.setParameter("idPersonaOrigen", idPersona);
        return JpaUtils.getAll(q);
    }

    /**
     * Obtiene la lista de facturas donde la persona indicada aparece como
     * destino de la factura.
     *
     * @param idPersona Identificador de persona.
     * @return Lista de facturas encontradas.
     */
    public List<Factura> findByPersonaDestino(Integer idPersona) {
        Query q = getEntityManager().createNativeQuery("select f.* from factoring.factura f join trans.documento d on f.id_documento = d.id where d.id_persona_destino = ?idPersonaDestino", Factura.class);
        q.setParameter("idPersonaDestino", idPersona);
        return JpaUtils.getAll(q);
    }

    /**
     * Realiza el cambio de estado para una factura. Al mismo tiempo, registra
     * este cambio de estado dentro del historico de facturas.
     *
     * @param idFactura Indentificador de la factura a ser cambiada de estado.
     * @param idNuevoEstado Identificador del nuevo estado para la factura.
     * @param comentario Comentario opcional que describe el cambio de estado.
     * @return True si el cambio de estado fue exitoso. False caso contrario.
     */
    public boolean cambiarEstado(Integer idFactura, Integer idNuevoEstado, String comentario) {
        Factura factura = find(idFactura);
        Estado nuevoEstado = estadoFacade.find(idNuevoEstado);
        if (nuevoEstado != null && factura != null) {
            factura.setIdEstado(nuevoEstado);
            edit(factura);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Obtiene la persona asociada al proveedor de la factura.
     *
     * @param factura Factura.
     * @return Proveedor de la factura.
     */
    public Persona findProveedor(Factura factura) {
        Integer idProveedor = factura.getIdPersonaOrigen();
        return personas.obtenerPorId(idProveedor).value();
    }

    /**
     * Obtiene la persona asociada al comprador de la factura.
     *
     * @param factura Factura.
     * @return Comprador de la factura.
     */
    public Persona findComprador(Factura factura) {
        Integer idComprador = factura.getIdPersonaDestino();
        return personas.obtenerPorId(idComprador).value();
    }

    /**
     * Calcula el monto total de una lista de facturas.
     *
     * @param facturas Lista de facturas.
     * @return Monto total.
     */
    public BigDecimal calcMontoTotal(List<Factura> facturas) {
        BigDecimal total = BigDecimal.ZERO;
        for (Factura f : facturas) {
            total = total.add(f.getMontoTotalFactura());
        }
        return total;
    }

    /**
     * Permite obtener la cantidad de facturas pagadas por una entidad
     * financiera, cuya fecha de pago esten proximas.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @return Lista de facturas.
     */
    public Integer countPagadasAVencer(Integer idEntidadFinanciera) {
        try {
            Query q = getEntityManager().createNamedQuery("Factura.countAllPagadasAVencer");
            q.setParameter("idEntidadFinanciera", idEntidadFinanciera);
            Factura factura = JpaUtils.getFirst(q);
            return (factura != null) ? factura.getCantidad() : 0;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Permite obtener la lista de facturas pagadas por una entidad financiera,
     * cuya fecha de pago esten proximas.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @param limit Cantidad maxima de facturas a obtener.
     * @return Lista de facturas.
     */
    public List<Factura> findPagadasAVencer(Integer idEntidadFinanciera, Integer limit) {
        Query q = getEntityManager().createNamedQuery("Factura.findAllPagadasAVencer");
        q.setParameter("limit", limit);
        q.setParameter("idEntidadFinanciera", idEntidadFinanciera);
        return JpaUtils.getAll(q);
    }

    /**
     * Permite obtener la cantidad de facturas pagadas por una entidad
     * financiera, cuya fecha de pago sea el dia de hoy.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @return Lista de facturas.
     */
    public Integer countPagadasAVencerHoy(Integer idEntidadFinanciera) {
        try {
            Query q = getEntityManager().createNamedQuery("Factura.countAllPagadasAVencerHoy");
            q.setParameter("idEntidadFinanciera", idEntidadFinanciera);
            Factura factura = JpaUtils.getFirst(q);
            return (factura != null) ? factura.getCantidad() : 0;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Permite obtener la lista de facturas pagadas por una entidad financiera,
     * cuya fecha de pago sea el dia de hoy.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @param limit Cantidad maxima de facturas a obtener.
     * @return Lista de facturas.
     */
    public List<Factura> findPagadasAVencerHoy(Integer idEntidadFinanciera, Integer limit) {
        Query q = getEntityManager().createNamedQuery("Factura.findAllPagadasAVencerHoy");
        q.setParameter("limit", limit);
        q.setParameter("idEntidadFinanciera", idEntidadFinanciera);
        return JpaUtils.getAll(q);
    }

    /**
     * Permite obtener la cantidad de facturas pagadas por una entidad
     * financiera, cuya fecha de pago ya hayan sido superadas.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @return Lista de facturas.
     */
    public Integer countPagadasVencidas(Integer idEntidadFinanciera) {
        try {
            Query q = getEntityManager().createNamedQuery("Factura.countAllPagadasVencidas");
            q.setParameter("idEntidadFinanciera", idEntidadFinanciera);
            Factura factura = JpaUtils.getFirst(q);
            return (factura != null) ? factura.getCantidad() : 0;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Permite obtener la lista de facturas pagadas por una entidad financiera,
     * cuya fecha de pago ya hayan sido superadas.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @param limit Cantidad maxima de facturas a obtener.
     * @return Lista de facturas.
     */
    public List<Factura> findPagadasVencidas(Integer idEntidadFinanciera, Integer limit) {
        Query q = getEntityManager().createNamedQuery("Factura.findAllPagadasVencidas");
        q.setParameter("idEntidadFinanciera", idEntidadFinanciera);
        q.setParameter("limit", limit);
        return JpaUtils.getAll(q);
    }

    /**
     * Permite obtener la cantidad de facturas vendidas a una entidad
     * financiera, listas para ser desembolsadas.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @return Lista de facturas.
     */
    public Integer countADesembolsar(Integer idEntidadFinanciera) {
        try {
            Query q = getEntityManager().createNamedQuery("Factura.countAllADesembolsar");
            q.setParameter("idEntidadFinanciera", idEntidadFinanciera);
            Factura factura = JpaUtils.getFirst(q);
            return (factura != null) ? factura.getCantidad() : 0;
        } catch (Exception e) {
            return 0;
        }
    }

}
