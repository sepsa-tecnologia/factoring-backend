/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "convenio", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Convenio.findAll", query = "SELECT c FROM Convenio c"),
    @NamedQuery(name = "Convenio.c", query = "SELECT c FROM Convenio c WHERE c.id = :id"),
    @NamedQuery(name = "Convenio.findByIds", query = "SELECT c FROM Convenio c WHERE c.estado = :estado and c.idComprador = :idComprador and c.idProveedor = :idProveedor and c.idEntidadFinanciera = :idEntidadFinanciera"),
    @NamedQuery(name = "Convenio.findByIdComprador", query = "SELECT c FROM Convenio c WHERE c.estado = :estado and c.idComprador = :idComprador"),
    @NamedQuery(name = "Convenio.findByIdProveedor", query = "SELECT c FROM Convenio c WHERE c.estado = :estado and c.idProveedor = :idProveedor"),
    @NamedQuery(name = "Convenio.findByIdEntidadFinanciera", query = "SELECT c FROM Convenio c WHERE c.estado = :estado and c.idEntidadFinanciera = :idEntidadFinanciera"),
    @NamedQuery(name = "Convenio.findByEstado", query = "SELECT c FROM Convenio c WHERE c.estado = :estado")
})
public class Convenio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_comprador")
    private int idComprador;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_proveedor")
    private int idProveedor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_entidad_financiera")
    private int idEntidadFinanciera;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Column(name = "id_tipo_convenio")
    private Integer idTipoConvenio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private Character estado;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idConvenio", fetch = FetchType.LAZY)
    private Collection<ConfigCuenta> configCuentaCollection;

    @OneToMany(mappedBy = "idConvenio", fetch = FetchType.LAZY)
    private Collection<FacturaConformada> facturaConformadaCollection;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_convenio", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private TipoConvenio tipoConvenio;

    public Convenio() {
    }

    public Convenio(Integer id) {
        this.id = id;
    }

    public Convenio(Integer id, int idComprador, int idProveedor, int idEntidadFinanciera, Date fecha, Character estado) {
        this.id = id;
        this.idComprador = idComprador;
        this.idProveedor = idProveedor;
        this.idEntidadFinanciera = idEntidadFinanciera;
        this.fecha = fecha;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(int idComprador) {
        this.idComprador = idComprador;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public int getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(int idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    @XmlTransient
    public Collection<ConfigCuenta> getConfigCuentaCollection() {
        return configCuentaCollection;
    }

    public void setConfigCuentaCollection(Collection<ConfigCuenta> configCuentaCollection) {
        this.configCuentaCollection = configCuentaCollection;
    }

    public Integer getIdTipoConvenio() {
        return idTipoConvenio;
    }

    public void setIdTipoConvenio(Integer idTipoConvenio) {
        this.idTipoConvenio = idTipoConvenio;
    }

    @XmlTransient
    public Collection<FacturaConformada> getFacturaConformadaCollection() {
        return facturaConformadaCollection;
    }

    public void setFacturaConformadaCollection(Collection<FacturaConformada> facturaConformadaCollection) {
        this.facturaConformadaCollection = facturaConformadaCollection;
    }

    public TipoConvenio getTipoConvenio() {
        return tipoConvenio;
    }

    public void setTipoConvenio(TipoConvenio tipoConvenio) {
        this.tipoConvenio = tipoConvenio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Convenio)) {
            return false;
        }
        Convenio other = (Convenio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.Convenio[ id=" + id + " ]";
    }

}
