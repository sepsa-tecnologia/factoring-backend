/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.misc.Assertions;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.misc.text.Text;
import fa.gs.utils.misc.xml.Xml;
import fa.gs.utils.misc.xml.XmlPrefixInfoList;
import java.math.BigDecimal;
import java.util.Objects;
import javax.xml.xpath.XPath;
import lombok.Data;
import org.w3c.dom.Document;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class RespuestaConsultaEstadoCuenta implements Respuesta {

    String codigo;
    String mensaje;
    BigDecimal montoLineaCredito;
    BigDecimal montoUtilizado;
    BigDecimal montoPendienteAutorizacion;
    BigDecimal montoDisponible;

    public static RespuestaConsultaEstadoCuenta parse(String xmlSoap) throws Throwable {
        // Eliminar el namespace que no utiliza ningun prefijo.
        xmlSoap = Text.replaceAll(xmlSoap, "xmlns=\"http://tempuri.org/\"", "");

        // Cargar XML de respuesta SOAP.
        Document xml = Xml.parse(xmlSoap);

        // Obtener prefijos y uris.
        XmlPrefixInfoList prefixes = Xml.readPrefixes(xml);

        // Preparar consultas XPath.
        XPath xpath = Xml.xpath(prefixes);
        String xCodigo = ".//CodigoError";
        String xMensaje = ".//Mensaje";
        String xMontoLineaCredito = ".//MontoLineaCredito";
        String xMontoUtilizado = ".//MontoUtilizado";
        String xMontoPendienteAutorizacion = ".//MontoPendienteAutorizacion";
        String xMontoDisponible = ".//MontoDisponible";

        // Obtener valor.
        String codigo = Xml.readNodeTexContent(xpath, xCodigo, xml);
        String mensaje = Xml.readNodeTexContent(xpath, xMensaje, xml);
        String montoLineaCredito = Xml.readNodeTexContent(xpath, xMontoLineaCredito, xml);
        String montoUtilizado = Xml.readNodeTexContent(xpath, xMontoUtilizado, xml);
        String montoPendienteAutorizacion = Xml.readNodeTexContent(xpath, xMontoPendienteAutorizacion, xml);
        String montoDisponible = Xml.readNodeTexContent(xpath, xMontoDisponible, xml);

        // Procesar valores.
        RespuestaConsultaEstadoCuenta respuesta = new RespuestaConsultaEstadoCuenta();
        respuesta.codigo = codigo;
        respuesta.mensaje = mensaje;
        respuesta.montoLineaCredito = Numeric.adaptAsBigDecimal(montoLineaCredito);
        respuesta.montoUtilizado = Numeric.adaptAsBigDecimal(montoUtilizado);
        respuesta.montoPendienteAutorizacion = Numeric.adaptAsBigDecimal(montoPendienteAutorizacion);
        respuesta.montoDisponible = Numeric.adaptAsBigDecimal(montoDisponible);
        return respuesta;
    }

    public static RespuestaConsultaEstadoCuenta instance(String codigo, String mensaje) {
        RespuestaConsultaEstadoCuenta instance = new RespuestaConsultaEstadoCuenta();
        instance.codigo = codigo;
        instance.mensaje = mensaje;
        instance.montoLineaCredito = Numeric.UNO_NEGATIVO;
        instance.montoUtilizado = Numeric.UNO_NEGATIVO;
        instance.montoPendienteAutorizacion = Numeric.UNO_NEGATIVO;
        instance.montoDisponible = Numeric.UNO_NEGATIVO;
        return instance;
    }

    @Override
    public boolean isOk() {
        return Assertions.stringNullOrEmpty(codigo) == false && Objects.equals(codigo, "0");
    }

}
