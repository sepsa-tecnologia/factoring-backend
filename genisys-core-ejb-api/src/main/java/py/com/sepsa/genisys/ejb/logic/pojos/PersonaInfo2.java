/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.pojos;

import java.io.Serializable;
import lombok.Data;
import py.com.sepsa.genisys.ejb.logic.impl.Personas.TipoPersonaEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class PersonaInfo2 implements Serializable {

    private Integer id;
    private String razonSocial;
    private String nombreFantasia;
    private String nombres;
    private String apellidos;
    private String nroDocumento;
    private String ruc;
    private String dvRuc;
    private TipoPersonaEnum tipoPersona;
    private Long totalCompradores;
    private Long totalProveedores;
    private Boolean esComprador;
    private Boolean esProveedor;
}
