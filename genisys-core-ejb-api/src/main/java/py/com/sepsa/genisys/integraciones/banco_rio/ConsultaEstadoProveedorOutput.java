/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import java.io.Serializable;
import lombok.Data;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.WsTrackInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class ConsultaEstadoProveedorOutput implements Serializable {

    private WsTrackInfo track;
    private RespuestaConsultaEstadoProveedor respuesta;
}
