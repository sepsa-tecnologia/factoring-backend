/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.misc.Assertions;
import fa.gs.utils.misc.text.Text;
import fa.gs.utils.misc.xml.Xml;
import fa.gs.utils.misc.xml.XmlPrefixInfoList;
import java.util.Objects;
import javax.xml.xpath.XPath;
import lombok.Data;
import org.w3c.dom.Document;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class RespuestaConsultaEstadoProveedor implements Respuesta {

    String codigo;
    String mensaje;
    String fechaInsercion;
    String estado;
    String decision;
    String codCliente;

    public static RespuestaConsultaEstadoProveedor parse(String xmlSoap) throws Throwable {
        // Eliminar el namespace que no utiliza ningun prefijo.
        xmlSoap = Text.replaceAll(xmlSoap, "xmlns=\"http://tempuri.org/\"", "");

        // Cargar XML de respuesta SOAP.
        Document xml = Xml.parse(xmlSoap);

        // Obtener prefijos y uris.
        XmlPrefixInfoList prefixes = Xml.readPrefixes(xml);

        // Preparar consultas XPath.
        XPath xpath = Xml.xpath(prefixes);
        String xCodigo = ".//CodigoError";
        String xMensaje = ".//Mensaje";
        String xFechaInsercion = ".//InfoProveedor/FechaInsercion";
        String xEstado = ".//InfoProveedor/Estado";
        String xDecision = ".//InfoProveedor/ComentarioDecision";
        String xCodCliente = ".//InfoProveedor/CodigoDeProveedor";

        // Obtener valor.
        String codigo = Xml.readNodeTexContent(xpath, xCodigo, xml);
        String mensaje = Xml.readNodeTexContent(xpath, xMensaje, xml);
        String fechaInsercion = Xml.readNodeTexContent(xpath, xFechaInsercion, xml);
        String estado = Xml.readNodeTexContent(xpath, xEstado, xml);
        String decision = Xml.readNodeTexContent(xpath, xDecision, xml);
        String codCliente = Xml.readNodeTexContent(xpath, xCodCliente, xml);

        // Procesar valores.
        RespuestaConsultaEstadoProveedor respuesta = new RespuestaConsultaEstadoProveedor();
        respuesta.codigo = codigo;
        respuesta.mensaje = mensaje;
        respuesta.fechaInsercion = fechaInsercion;
        respuesta.estado = estado;
        respuesta.decision = decision;
        respuesta.codCliente = codCliente;
        return respuesta;
    }

    public static RespuestaConsultaEstadoProveedor instance(String codigo, String mensaje) {
        RespuestaConsultaEstadoProveedor instance = new RespuestaConsultaEstadoProveedor();
        instance.codigo = codigo;
        instance.mensaje = mensaje;
        instance.fechaInsercion = "";
        instance.estado = "";
        instance.decision = "";
        instance.codCliente = "";
        return instance;
    }

    @Override
    public boolean isOk() {
        return Assertions.stringNullOrEmpty(codigo) == false && Objects.equals(codigo, "0");
    }

}
