/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "persona_entidad", schema = "factoring")
@NamedQueries({
    @NamedQuery(name = "PersonaEntidad.findByIds", query = "SELECT p FROM PersonaEntidad p WHERE p.idPersona = :idPersona and p.idPerfil = :idPerfil and p.idEntidadFinanciera = :idEntidadFinanciera"),
    @NamedQuery(name = "PersonaEntidad.findByEstado", query = "SELECT p FROM PersonaEntidad p WHERE p.estado = :estado"),})
@Data
public class PersonaEntidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_persona")
    private int idPersona;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_perfil")
    private int idPerfil;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_entidad_financiera")
    private int idEntidadFinanciera;

    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private String estado;

    public PersonaEntidad() {
    }

    public PersonaEntidad(Integer id) {
        this.id = id;
    }

}
