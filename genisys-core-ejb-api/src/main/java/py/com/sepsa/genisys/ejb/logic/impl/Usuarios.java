/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.criteria.query.Order;
import fa.gs.misc.Assertions;
import fa.gs.misc.text.Joiner;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.database.query.expressions.literals.CollectionLiteral;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.Data;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.logic.impl.Personas.TipoPersonaEnum;
import py.com.sepsa.genisys.ejb.logic.pojos.UsuarioInfo;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;
import py.com.sepsa.genisys.ejb.utils.Persistence;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
@Stateless(name = "Usuarios", mappedName = "Usuarios")
@LocalBean
public class Usuarios extends LogicBean {

    @PersistenceContext(unitName = Persistence.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @EJB
    private JpaUtils jpaUtils;

    @EJB
    private Sesiones sesiones;

    @EJB
    private Perfiles perfiles;

    //<editor-fold defaultstate="collapsed" desc="Obtener Usuarios">
    public Result<Usuario> obtenerPorId(Integer id) {
        Result<Usuario> result;

        try {
            Integer[] ids = new Integer[]{id};
            Result<Collection<Usuario>> resUsuarios = obtenerPorIds(ids);
            resUsuarios.raise();

            Collection<Usuario> personas = resUsuarios.value();
            Usuario persona = Lists.first(personas);

            result = Results.ok()
                    .value(persona)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo usuario")
                    .tag("usuario.id", id)
                    .build();
        }

        return result;
    }

    public Result<Collection<Usuario>> obtenerPorIds(Integer[] ids) {
        Result<Collection<Usuario>> result;

        try {
            Collection<Usuario> instances = Lists.empty();

            int chunk = 10;
            for (int i = 0; i < ids.length; i += chunk) {
                // Obtener subrango de identificadores.
                Integer[] idsChunk = java.util.Arrays.copyOfRange(ids, i, Math.min(ids.length, i + chunk));

                // Construir query.
                StringBuilder2 builder = new StringBuilder2();
                builder.append(" select ");
                builder.append("     u.id as \"id\", ");
                builder.append("     u.id_persona as \"id_persona\", ");
                builder.append("     u.usuario as \"usuario\", ");
                builder.append("     u.estado as \"estado\", ");
                builder.append("     u.id_persona_fisica as \"id_persona_fisica\" ");
                builder.append(" from trans.usuario as u ");
                builder.append(" where ");
                builder.append("     1 = 1 ");
                builder.append("     and u.id in %s ", CollectionLiteral.instance(idsChunk).stringify(null));
                //builder.append("     and p.estado = 'A' ");
                builder.append("     ; ");

                // Ejecutar consulta.
                String query = builder.toString();
                Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

                // Mapear y agregar registros.
                rows.stream()
                        .map(row -> mapUsuario(row))
                        .forEach(p -> instances.add(p));
            }

            result = Results.ok()
                    .value(instances)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo personas")
                    .build();
        }

        return result;
    }

    public List<UsuarioInfo> selectUsuarios(Integer limit, Integer offset, Map<String, Object> filterBy, Map<String, Order> orderBy) {
        List<UsuarioInfo> personas = Lists.empty();

        try {
            // Ejecutar query.
            String query = queryUsuarios(false, limit, offset, filterBy, orderBy);
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Agregar registros.
            if (!Assertions.isNullOrEmpty(rows)) {
                rows.stream()
                        .map(this::mapUsuarioInfo)
                        .forEach(personas::add);
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando usuarios");
        }

        return personas;
    }

    public Integer countUsuarios(Map<String, Object> filterBy) {
        try {
            // Ejecutar query.
            String query = queryUsuarios(true, null, null, filterBy, null);
            Map<String, Object> row = jpaUtils.getFirst(query);

            BigInteger count = Maps.biginteger(row, "total");
            return count.intValue();
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando compradores de proveedor");
            return -1;
        }
    }

    private String queryUsuarios(boolean forCount, Integer limit, Integer offset, Map<String, Object> filterBy, Map<String, Order> orderBy) {
        // Construir query.
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");

        // Proyecciones.
        if (forCount) {
            builder.append("  cast(count(*) as int8) as \"total\" ");
        } else {
            builder.append("  u.*, ");
            builder.append("  t0.*, ");
            builder.append("  t1.* ");
        }

        // Origen de datos.
        builder.append(" from trans.usuario as u ");
        builder.append(" left join lateral ( select coalesce(pj.razon_social, pf.nombre || ' ' || pf.apellido) as \"razon_social\", coalesce(pj.nombre_fantasia, pf.nombre || ' ' || pf.apellido) as \"nombre_fantasia\", p.ruc, p.dv_ruc from info.persona as p left join info.persona_fisica as pf on pf.id = p.id left join info.persona_juridica as pj on pj.id = p.id where p.id = u.id_persona ) as T0 on true ");
        builder.append(" left join lateral ( select string_agg(pp.descripcion, ';') as \"perfiles_agg\" from factoring.usuario_perfil as up left join factoring.perfil as pp on pp.id = up.id_perfil where up.id_usuario = u.id ) as t1 on true ");

        // Filtros.
        builder.append(" where ");
        builder.append("   1 = 1 ");
        builder.append("   and u.estado = 'A' ");
        if (!Assertions.isNullOrEmpty(filterBy)) {
            filterBy.entrySet().stream().forEach(e -> {
                builder.append(" and %s ", String.valueOf(e.getValue()));
            });
        }

        // Ordenacion.
        if (forCount == false) {
            if (!Assertions.isNullOrEmpty(orderBy)) {
                String[] clauses = orderBy.entrySet().stream()
                        .map(e -> Strings.format("%s %s", e.getKey(), e.getValue()))
                        .distinct()
                        .toArray(String[]::new);
                String clause = Joiner.of(clauses).separator(",").join();

                builder.append(" order by ");
                builder.append(" %s ", clause);
            }
        }

        // Paginacion.
        if (forCount == false) {
            if (limit != null) {
                builder.append(" limit %s ", limit);
            }
            if (offset != null) {
                builder.append(" offset %s ", offset);
            }
        }

        return builder.toString();
    }
    //</editor-fold>

    public Usuario registrarNuevo(Integer idPersona, String usuario, String contrasena) throws Throwable {
        Usuario instance = new Usuario();
        instance.setIdPersona(idPersona);
        instance.setUsuario(usuario);
        instance.setContrasena(new BCryptPasswordEncoder().encode(contrasena));
        instance.setEstado('A');
        instance = jpaUtils.create(instance);
        return instance;
    }

    public void inhabilitar(Integer idUsuario) throws Throwable {
        jpaUtils.executeUpdate(Strings.format(" update trans.usuario set estado = 'I' where id = %s ", idUsuario));
    }

    public Collection<Perfil> obtenerPerfilesPorTransUsuario(Usuario usuario) {
        return sesiones.obtenerPorTransUsuario(usuario).value(Lists.empty());
    }

    public void cambiarPerfiles(Integer idUsuario, Collection<PerfilEnum> perfiles) throws Throwable {
        // Eliminar perfiles actuales.
        jpaUtils.executeUpdate(Strings.format(" delete from factoring.usuario_perfil where id_usuario = %s ", idUsuario));

        // Agregar nuevos perfiles.
        if (!Assertions.isNullOrEmpty(perfiles)) {
            for (PerfilEnum perfilEnum : perfiles) {
                Perfil perfil = this.perfiles.obtenerPerfil(perfilEnum);
                String query = Strings.format(" insert into factoring.usuario_perfil(id_usuario, id_perfil) values(%s, %s) ", idUsuario, perfil.getId());
                jpaUtils.executeUpdate(query);
            }
        }
    }

    private Usuario mapUsuario(Map<String, Object> row) {
        Integer id = Maps.integer(row, "id");
        Integer idPersona = Maps.integer(row, "id_persona");
        String usuario = Maps.string(row, "usuario");
        Character estado = Maps.character(row, "estado");
        Integer idPersonaFisica = Maps.integer(row, "id_persona_fisica");

        Usuario instance = new Usuario();
        instance.setId(id);
        instance.setIdPersona(idPersona);
        instance.setUsuario(usuario);
        instance.setEstado(estado);
        instance.setIdPersonaFisica(idPersonaFisica);
        return instance;
    }

    private UsuarioInfo mapUsuarioInfo(Map<String, Object> row) {
        Integer id = Maps.integer(row, "id");
        Integer idPersona = Maps.integer(row, "idPersona");
        String usuario = Maps.string(row, "usuario");
        String razonSocial = Maps.string(row, "razon_social", "");
        String nombreFantasia = Maps.string(row, "nombre_fantasia", "");
        String ruc = Maps.string(row, "ruc", "");
        String dvRuc = Maps.string(row, "dv_ruc", "");
        String idPerfilesAgg = Maps.string(row, "perfiles_agg", "");

        UsuarioInfo instance = new UsuarioInfo();
        instance.setId(id);
        instance.setIdPersona(idPersona);
        instance.setUsuario(usuario);
        instance.setRazonSocial(razonSocial);
        instance.setNombreFantasia(nombreFantasia);
        instance.setRuc(ruc);
        instance.setDvRuc(dvRuc);
        instance.setIdPerfilAgg(idPerfilesAgg);
        return instance;
    }

    @Data
    private static class RegistrarUsuarioInput implements Serializable {

        private TipoPersonaEnum tipo;
        private String razonSocial;
        private String nombreFantasia;
        private String nombres;
        private String apellidos;
        private String ci;
        private String ruc;

        public RegistrarUsuarioInput() {
            this.tipo = null;
            this.razonSocial = "";
            this.nombreFantasia = "";
            this.nombres = "";
            this.apellidos = "";
            this.ruc = "";
        }
    }
}
