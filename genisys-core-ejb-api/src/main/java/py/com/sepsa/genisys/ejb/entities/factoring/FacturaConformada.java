/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "factura_conformada", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FacturaConformada.findAll", query = "SELECT f FROM FacturaConformada f"),
    @NamedQuery(name = "FacturaConformada.findById", query = "SELECT f FROM FacturaConformada f WHERE f.id = :id"),
    @NamedQuery(name = "FacturaConformada.findByMontoFactura", query = "SELECT f FROM FacturaConformada f WHERE f.montoFactura = :montoFactura"),
    @NamedQuery(name = "FacturaConformada.findByMontoNotaCredito", query = "SELECT f FROM FacturaConformada f WHERE f.montoNotaCredito = :montoNotaCredito"),
    @NamedQuery(name = "FacturaConformada.findByMontoRetencion", query = "SELECT f FROM FacturaConformada f WHERE f.montoRetencion = :montoRetencion"),
    @NamedQuery(name = "FacturaConformada.findByMontoOtrosCargos", query = "SELECT f FROM FacturaConformada f WHERE f.montoOtrosCargos = :montoOtrosCargos"),
    @NamedQuery(name = "FacturaConformada.findByMontoConformado", query = "SELECT f FROM FacturaConformada f WHERE f.montoConformado = :montoConformado"),
    @NamedQuery(name = "FacturaConformada.findByMontoImponibleComision", query = "SELECT f FROM FacturaConformada f WHERE f.montoImponibleComision = :montoImponibleComision"),
    @NamedQuery(name = "FacturaConformada.findByMontoImpuestoComision", query = "SELECT f FROM FacturaConformada f WHERE f.montoImpuestoComision = :montoImpuestoComision"),
    @NamedQuery(name = "FacturaConformada.findByMontoTotalComision", query = "SELECT f FROM FacturaConformada f WHERE f.montoTotalComision = :montoTotalComision"),
    @NamedQuery(name = "FacturaConformada.findByMontoNeto", query = "SELECT f FROM FacturaConformada f WHERE f.montoNeto = :montoNeto"),
    @NamedQuery(name = "FacturaConformada.findByPorcentajeComision", query = "SELECT f FROM FacturaConformada f WHERE f.porcentajeComision = :porcentajeComision"),
    @NamedQuery(name = "FacturaConformada.findByDiasAnticipo", query = "SELECT f FROM FacturaConformada f WHERE f.diasAnticipo = :diasAnticipo"),
    @NamedQuery(name = "FacturaConformada.findByFechaVenta", query = "SELECT f FROM FacturaConformada f WHERE f.fechaVenta = :fechaVenta")})
public class FacturaConformada implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;

    @Basic(optional = true)
    @Column(name = "id_entidad_financiera")
    private Integer idEntidadFinanciera;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_factura")
    private BigDecimal montoFactura;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_nota_credito")
    private BigDecimal montoNotaCredito;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_retencion")
    private BigDecimal montoRetencion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_otros_cargos")
    private BigDecimal montoOtrosCargos;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_conformado")
    private BigDecimal montoConformado;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_comision")
    private BigDecimal montoImponibleComision;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_impuesto_comision")
    private BigDecimal montoImpuestoComision;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_comision")
    private BigDecimal montoTotalComision;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_gastos_operativos")
    private BigDecimal montoImponibleGastosOperativos;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_impuesto_gastos_operativos")
    private BigDecimal montoImpuestoGastosOperativos;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_gastos_operativos")
    private BigDecimal montoTotalGastosOperativos;

    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_neto")
    private BigDecimal montoNeto;

    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_comision")
    private BigDecimal porcentajeComision;

    @Basic(optional = false)
    @NotNull
    @Column(name = "dias_anticipo")
    private int diasAnticipo;

    @Basic(optional = true)
    @Column(name = "monto_distribucion")
    private BigDecimal montoDistribucion;

    @Column(name = "fecha_venta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVenta;

    @JoinColumn(name = "id_convenio", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Convenio idConvenio;

    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Factura factura;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFacturaConformada", fetch = FetchType.LAZY)
    private Collection<Transaccion> transaccionCollection;

    public FacturaConformada() {
    }

    public FacturaConformada(Integer id) {
        this.id = id;
    }

    public FacturaConformada(Integer id, BigDecimal montoFactura, BigDecimal montoNotaCredito, BigDecimal montoRetencion, BigDecimal montoOtrosCargos, BigDecimal montoConformado, BigDecimal montoImponibleComision, BigDecimal montoImpuestoComision, BigDecimal montoTotalComision, BigDecimal montoNeto, BigDecimal porcentajeComision, int diasAnticipo) {
        this.id = id;
        this.montoFactura = montoFactura;
        this.montoNotaCredito = montoNotaCredito;
        this.montoRetencion = montoRetencion;
        this.montoOtrosCargos = montoOtrosCargos;
        this.montoConformado = montoConformado;
        this.montoImponibleComision = montoImponibleComision;
        this.montoImpuestoComision = montoImpuestoComision;
        this.montoTotalComision = montoTotalComision;
        this.montoNeto = montoNeto;
        this.porcentajeComision = porcentajeComision;
        this.diasAnticipo = diasAnticipo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public BigDecimal getMontoFactura() {
        return montoFactura;
    }

    public void setMontoFactura(BigDecimal montoFactura) {
        this.montoFactura = montoFactura;
    }

    public BigDecimal getMontoNotaCredito() {
        return montoNotaCredito;
    }

    public void setMontoNotaCredito(BigDecimal montoNotaCredito) {
        this.montoNotaCredito = montoNotaCredito;
    }

    public BigDecimal getMontoRetencion() {
        return montoRetencion;
    }

    public void setMontoRetencion(BigDecimal montoRetencion) {
        this.montoRetencion = montoRetencion;
    }

    public BigDecimal getMontoOtrosCargos() {
        return montoOtrosCargos;
    }

    public void setMontoOtrosCargos(BigDecimal montoOtrosCargos) {
        this.montoOtrosCargos = montoOtrosCargos;
    }

    public BigDecimal getMontoConformado() {
        return montoConformado;
    }

    public void setMontoConformado(BigDecimal montoConformado) {
        this.montoConformado = montoConformado;
    }

    public BigDecimal getMontoImponibleComision() {
        return montoImponibleComision;
    }

    public void setMontoImponibleComision(BigDecimal montoImponibleComision) {
        this.montoImponibleComision = montoImponibleComision;
    }

    public BigDecimal getMontoImpuestoComision() {
        return montoImpuestoComision;
    }

    public void setMontoImpuestoComision(BigDecimal montoImpuestoComision) {
        this.montoImpuestoComision = montoImpuestoComision;
    }

    public BigDecimal getMontoTotalComision() {
        return montoTotalComision;
    }

    public void setMontoTotalComision(BigDecimal montoTotalComision) {
        this.montoTotalComision = montoTotalComision;
    }

    public BigDecimal getMontoImponibleGastosOperativos() {
        return montoImponibleGastosOperativos;
    }

    public void setMontoImponibleGastosOperativos(BigDecimal montoImponibleGastosOperativos) {
        this.montoImponibleGastosOperativos = montoImponibleGastosOperativos;
    }

    public BigDecimal getMontoImpuestoGastosOperativos() {
        return montoImpuestoGastosOperativos;
    }

    public void setMontoImpuestoGastosOperativos(BigDecimal montoImpuestoGastosOperativos) {
        this.montoImpuestoGastosOperativos = montoImpuestoGastosOperativos;
    }

    public BigDecimal getMontoTotalGastosOperativos() {
        return montoTotalGastosOperativos;
    }

    public void setMontoTotalGastosOperativos(BigDecimal montoTotalGastosOperativos) {
        this.montoTotalGastosOperativos = montoTotalGastosOperativos;
    }

    public BigDecimal getMontoNeto() {
        return montoNeto;
    }

    public void setMontoNeto(BigDecimal montoNeto) {
        this.montoNeto = montoNeto;
    }

    public BigDecimal getPorcentajeComision() {
        return porcentajeComision;
    }

    public void setPorcentajeComision(BigDecimal porcentajeComision) {
        this.porcentajeComision = porcentajeComision;
    }

    public BigDecimal getMontoDistribucion() {
        return montoDistribucion;
    }

    public void setMontoDistribucion(BigDecimal montoDistribucion) {
        this.montoDistribucion = montoDistribucion;
    }

    public int getDiasAnticipo() {
        return diasAnticipo;
    }

    public void setDiasAnticipo(int diasAnticipo) {
        this.diasAnticipo = diasAnticipo;
    }

    public Date getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(Date fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public Convenio getIdConvenio() {
        return idConvenio;
    }

    public void setIdConvenio(Convenio idConvenio) {
        this.idConvenio = idConvenio;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    @XmlTransient
    public Collection<Transaccion> getTransaccionCollection() {
        return transaccionCollection;
    }

    public void setTransaccionCollection(Collection<Transaccion> transaccionCollection) {
        this.transaccionCollection = transaccionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof FacturaConformada)) {
            return false;
        }
        FacturaConformada other = (FacturaConformada) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.FacturaConformada[ id=" + id + " ]";
    }

}
