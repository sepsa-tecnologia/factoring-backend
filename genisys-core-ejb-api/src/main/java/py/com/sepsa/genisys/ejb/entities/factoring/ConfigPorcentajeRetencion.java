/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "config_porcentaje_retencion", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConfigPorcentajeRetencion.findAll", query = "SELECT c FROM ConfigPorcentajeRetencion c"),
    @NamedQuery(name = "ConfigPorcentajeRetencion.findByIdComprador", query = "SELECT c FROM ConfigPorcentajeRetencion c WHERE c.idComprador = :idComprador"),
    @NamedQuery(name = "ConfigPorcentajeRetencion.findByPorcentajeRetencion", query = "SELECT c FROM ConfigPorcentajeRetencion c WHERE c.porcentajeRetencion = :porcentajeRetencion")})
public class ConfigPorcentajeRetencion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_comprador")
    private Integer idComprador;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_retencion")
    private BigDecimal porcentajeRetencion;

    public ConfigPorcentajeRetencion() {
    }

    public ConfigPorcentajeRetencion(Integer idComprador) {
        this.idComprador = idComprador;
    }

    public ConfigPorcentajeRetencion(Integer idComprador, BigDecimal porcentajeRetencion) {
        this.idComprador = idComprador;
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }

    public BigDecimal getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(BigDecimal porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComprador != null ? idComprador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ConfigPorcentajeRetencion)) {
            return false;
        }
        ConfigPorcentajeRetencion other = (ConfigPorcentajeRetencion) object;
        if ((this.idComprador == null && other.idComprador != null) || (this.idComprador != null && !this.idComprador.equals(other.idComprador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.ConfigPorcentajeRetencion[ idComprador=" + idComprador + " ]";
    }

}
