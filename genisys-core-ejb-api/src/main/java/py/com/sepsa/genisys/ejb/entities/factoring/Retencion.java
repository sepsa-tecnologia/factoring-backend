/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "retencion", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Retencion.findAll", query = "SELECT r FROM Retencion r"),
    @NamedQuery(name = "Retencion.findById", query = "SELECT r FROM Retencion r WHERE r.id = :id"),
    @NamedQuery(name = "Retencion.findByIdFactura", query = "SELECT r FROM Retencion r WHERE r.idFactura = :idFactura"),
    @NamedQuery(name = "Retencion.findByIdMoneda", query = "SELECT r FROM Retencion r WHERE r.idMoneda = :idMoneda"),
    @NamedQuery(name = "Retencion.findByNroRetencion", query = "SELECT r FROM Retencion r WHERE r.nroRetencion = :nroRetencion"),
    @NamedQuery(name = "Retencion.findByNroTimbrado", query = "SELECT r FROM Retencion r WHERE r.nroTimbrado = :nroTimbrado"),
    @NamedQuery(name = "Retencion.findByVtoTimbrado", query = "SELECT r FROM Retencion r WHERE r.vtoTimbrado = :vtoTimbrado"),
    @NamedQuery(name = "Retencion.findByRazonSocialComprador", query = "SELECT r FROM Retencion r WHERE r.razonSocialComprador = :razonSocialComprador"),
    @NamedQuery(name = "Retencion.findByDireccionComprador", query = "SELECT r FROM Retencion r WHERE r.direccionComprador = :direccionComprador"),
    @NamedQuery(name = "Retencion.findByTelefonoComprador", query = "SELECT r FROM Retencion r WHERE r.telefonoComprador = :telefonoComprador"),
    @NamedQuery(name = "Retencion.findByRazonSocialProveedor", query = "SELECT r FROM Retencion r WHERE r.razonSocialProveedor = :razonSocialProveedor"),
    @NamedQuery(name = "Retencion.findByDireccionProveedor", query = "SELECT r FROM Retencion r WHERE r.direccionProveedor = :direccionProveedor"),
    @NamedQuery(name = "Retencion.findByTelefonoProveedor", query = "SELECT r FROM Retencion r WHERE r.telefonoProveedor = :telefonoProveedor"),
    @NamedQuery(name = "Retencion.findByFecha", query = "SELECT r FROM Retencion r WHERE r.fecha = :fecha"),
    @NamedQuery(name = "Retencion.findByMontoImponibleTotal", query = "SELECT r FROM Retencion r WHERE r.montoImponibleTotal = :montoImponibleTotal"),
    @NamedQuery(name = "Retencion.findByMontoIvaTotal", query = "SELECT r FROM Retencion r WHERE r.montoIvaTotal = :montoIvaTotal"),
    @NamedQuery(name = "Retencion.findByMontoTotal", query = "SELECT r FROM Retencion r WHERE r.montoTotal = :montoTotal"),
    @NamedQuery(name = "Retencion.findByPorcentajeRetencion", query = "SELECT r FROM Retencion r WHERE r.porcentajeRetencion = :porcentajeRetencion"),
    @NamedQuery(name = "Retencion.findByMontoRetenidoTotal", query = "SELECT r FROM Retencion r WHERE r.montoRetenidoTotal = :montoRetenidoTotal")})
public class Retencion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_moneda")
    private int idMoneda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_retencion")
    private String nroRetencion;
    @Size(max = 2147483647)
    @Column(name = "nro_timbrado")
    private String nroTimbrado;
    @Size(max = 2147483647)
    @Column(name = "vto_timbrado")
    private String vtoTimbrado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "razon_social_comprador")
    private String razonSocialComprador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "ruc_comprador")
    private String rucComprador;
    @Size(max = 2147483647)
    @Column(name = "direccion_comprador")
    private String direccionComprador;
    @Size(max = 2147483647)
    @Column(name = "telefono_comprador")
    private String telefonoComprador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "razon_social_proveedor")
    private String razonSocialProveedor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "ruc_proveedor")
    private String rucProveedor;
    @Size(max = 2147483647)
    @Column(name = "direccion_proveedor")
    private String direccionProveedor;
    @Size(max = 2147483647)
    @Column(name = "telefono_proveedor")
    private String telefonoProveedor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_total")
    private BigDecimal montoImponibleTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_total")
    private BigDecimal montoIvaTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total")
    private BigDecimal montoTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_retencion")
    private BigDecimal porcentajeRetencion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_retenido_total")
    private BigDecimal montoRetenidoTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_factura")
    private Integer idFactura;

    public Retencion() {
    }

    public Retencion(Integer id) {
        this.id = id;
    }

    public Retencion(Integer id, int idMoneda, String nroRetencion, String razonSocialComprador, String rucComprador, String razonSocialProveedor, String rucProveedor, Date fecha, BigDecimal montoImponibleTotal, BigDecimal montoIvaTotal, BigDecimal montoTotal, BigDecimal porcentajeRetencion, BigDecimal montoRetenidoTotal) {
        this.id = id;
        this.idMoneda = idMoneda;
        this.nroRetencion = nroRetencion;
        this.razonSocialComprador = razonSocialComprador;
        this.rucComprador = rucComprador;
        this.razonSocialProveedor = razonSocialProveedor;
        this.rucProveedor = rucProveedor;
        this.fecha = fecha;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoIvaTotal = montoIvaTotal;
        this.montoTotal = montoTotal;
        this.porcentajeRetencion = porcentajeRetencion;
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getNroRetencion() {
        return nroRetencion;
    }

    public void setNroRetencion(String nroRetencion) {
        this.nroRetencion = nroRetencion;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public String getVtoTimbrado() {
        return vtoTimbrado;
    }

    public void setVtoTimbrado(String vtoTimbrado) {
        this.vtoTimbrado = vtoTimbrado;
    }

    public String getRazonSocialComprador() {
        return razonSocialComprador;
    }

    public void setRazonSocialComprador(String razonSocialComprador) {
        this.razonSocialComprador = razonSocialComprador;
    }

    public String getRucComprador() {
        return rucComprador;
    }

    public void setRucComprador(String rucComprador) {
        this.rucComprador = rucComprador;
    }

    public String getDireccionComprador() {
        return direccionComprador;
    }

    public void setDireccionComprador(String direccionComprador) {
        this.direccionComprador = direccionComprador;
    }

    public String getTelefonoComprador() {
        return telefonoComprador;
    }

    public void setTelefonoComprador(String telefonoComprador) {
        this.telefonoComprador = telefonoComprador;
    }

    public String getRazonSocialProveedor() {
        return razonSocialProveedor;
    }

    public void setRazonSocialProveedor(String razonSocialProveedor) {
        this.razonSocialProveedor = razonSocialProveedor;
    }

    public String getRucProveedor() {
        return rucProveedor;
    }

    public void setRucProveedor(String rucProveedor) {
        this.rucProveedor = rucProveedor;
    }

    public String getDireccionProveedor() {
        return direccionProveedor;
    }

    public void setDireccionProveedor(String direccionProveedor) {
        this.direccionProveedor = direccionProveedor;
    }

    public String getTelefonoProveedor() {
        return telefonoProveedor;
    }

    public void setTelefonoProveedor(String telefonoProveedor) {
        this.telefonoProveedor = telefonoProveedor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public BigDecimal getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(BigDecimal porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public BigDecimal getMontoRetenidoTotal() {
        return montoRetenidoTotal;
    }

    public void setMontoRetenidoTotal(BigDecimal montoRetenidoTotal) {
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Retencion)) {
            return false;
        }
        Retencion other = (Retencion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.Retencion[ id=" + id + " ]";
    }

}
