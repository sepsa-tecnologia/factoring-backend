/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "nota_credito", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotaCredito.findAll", query = "SELECT n FROM NotaCredito n"),
    @NamedQuery(name = "NotaCredito.findById", query = "SELECT n FROM NotaCredito n WHERE n.id = :id"),
    @NamedQuery(name = "NotaCredito.findByIdFactura", query = "SELECT n FROM NotaCredito n WHERE n.idFactura = :idFactura"),
    @NamedQuery(name = "NotaCredito.findByIdMoneda", query = "SELECT n FROM NotaCredito n WHERE n.idMoneda = :idMoneda"),
    @NamedQuery(name = "NotaCredito.findByNroNotaCredito", query = "SELECT n FROM NotaCredito n WHERE n.nroNotaCredito = :nroNotaCredito"),
    @NamedQuery(name = "NotaCredito.findByNroTimbrado", query = "SELECT n FROM NotaCredito n WHERE n.nroTimbrado = :nroTimbrado"),
    @NamedQuery(name = "NotaCredito.findByVtoTimbrado", query = "SELECT n FROM NotaCredito n WHERE n.vtoTimbrado = :vtoTimbrado"),
    @NamedQuery(name = "NotaCredito.findByRazonSocialComprador", query = "SELECT n FROM NotaCredito n WHERE n.razonSocialComprador = :razonSocialComprador"),
    @NamedQuery(name = "NotaCredito.findByDireccionComprador", query = "SELECT n FROM NotaCredito n WHERE n.direccionComprador = :direccionComprador"),
    @NamedQuery(name = "NotaCredito.findByTelefonoComprador", query = "SELECT n FROM NotaCredito n WHERE n.telefonoComprador = :telefonoComprador"),
    @NamedQuery(name = "NotaCredito.findByRazonSocialProveedor", query = "SELECT n FROM NotaCredito n WHERE n.razonSocialProveedor = :razonSocialProveedor"),
    @NamedQuery(name = "NotaCredito.findByDireccionProveedor", query = "SELECT n FROM NotaCredito n WHERE n.direccionProveedor = :direccionProveedor"),
    @NamedQuery(name = "NotaCredito.findByTelefonoProveedor", query = "SELECT n FROM NotaCredito n WHERE n.telefonoProveedor = :telefonoProveedor"),
    @NamedQuery(name = "NotaCredito.findByFecha", query = "SELECT n FROM NotaCredito n WHERE n.fecha = :fecha"),
    @NamedQuery(name = "NotaCredito.findByMontoIva5", query = "SELECT n FROM NotaCredito n WHERE n.montoIva5 = :montoIva5"),
    @NamedQuery(name = "NotaCredito.findByMontoImponible5", query = "SELECT n FROM NotaCredito n WHERE n.montoImponible5 = :montoImponible5"),
    @NamedQuery(name = "NotaCredito.findByMontoTotal5", query = "SELECT n FROM NotaCredito n WHERE n.montoTotal5 = :montoTotal5"),
    @NamedQuery(name = "NotaCredito.findByMontoIva10", query = "SELECT n FROM NotaCredito n WHERE n.montoIva10 = :montoIva10"),
    @NamedQuery(name = "NotaCredito.findByMontoImponible10", query = "SELECT n FROM NotaCredito n WHERE n.montoImponible10 = :montoImponible10"),
    @NamedQuery(name = "NotaCredito.findByMontoTotal10", query = "SELECT n FROM NotaCredito n WHERE n.montoTotal10 = :montoTotal10"),
    @NamedQuery(name = "NotaCredito.findByMontoTotalExento", query = "SELECT n FROM NotaCredito n WHERE n.montoTotalExento = :montoTotalExento"),
    @NamedQuery(name = "NotaCredito.findByMontoIvaTotal", query = "SELECT n FROM NotaCredito n WHERE n.montoIvaTotal = :montoIvaTotal"),
    @NamedQuery(name = "NotaCredito.findByMontoImponibleTotal", query = "SELECT n FROM NotaCredito n WHERE n.montoImponibleTotal = :montoImponibleTotal"),
    @NamedQuery(name = "NotaCredito.findByMontoTotalNotaCredito", query = "SELECT n FROM NotaCredito n WHERE n.montoTotalNotaCredito = :montoTotalNotaCredito")})
public class NotaCredito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_moneda")
    private int idMoneda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_nota_credito")
    private String nroNotaCredito;
    @Size(max = 2147483647)
    @Column(name = "nro_timbrado")
    private String nroTimbrado;
    @Size(max = 2147483647)
    @Column(name = "vto_timbrado")
    private String vtoTimbrado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "razon_social_comprador")
    private String razonSocialComprador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "ruc_comprador")
    private String rucComprador;
    @Size(max = 2147483647)
    @Column(name = "direccion_comprador")
    private String direccionComprador;
    @Size(max = 2147483647)
    @Column(name = "telefono_comprador")
    private String telefonoComprador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "razon_social_proveedor")
    private String razonSocialProveedor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "ruc_proveedor")
    private String rucProveedor;
    @Size(max = 2147483647)
    @Column(name = "direccion_proveedor")
    private String direccionProveedor;
    @Size(max = 2147483647)
    @Column(name = "telefono_proveedor")
    private String telefonoProveedor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_5")
    private BigDecimal montoIva5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_5")
    private BigDecimal montoImponible5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_5")
    private BigDecimal montoTotal5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_10")
    private BigDecimal montoIva10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_10")
    private BigDecimal montoImponible10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_10")
    private BigDecimal montoTotal10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_exento")
    private BigDecimal montoTotalExento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_total")
    private BigDecimal montoIvaTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_total")
    private BigDecimal montoImponibleTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_nota_credito")
    private BigDecimal montoTotalNotaCredito;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_factura")
    private Integer idFactura;

    public NotaCredito() {
    }

    public NotaCredito(Integer id) {
        this.id = id;
    }

    public NotaCredito(Integer id, int idMoneda, String nroNotaCredito, String razonSocialComprador, String rucComprador, String razonSocialProveedor, String rucProveedor, Date fecha, BigDecimal montoIva5, BigDecimal montoImponible5, BigDecimal montoTotal5, BigDecimal montoIva10, BigDecimal montoImponible10, BigDecimal montoTotal10, BigDecimal montoTotalExento, BigDecimal montoIvaTotal, BigDecimal montoImponibleTotal, BigDecimal montoTotalNotaCredito) {
        this.id = id;
        this.idMoneda = idMoneda;
        this.nroNotaCredito = nroNotaCredito;
        this.razonSocialComprador = razonSocialComprador;
        this.rucComprador = rucComprador;
        this.razonSocialProveedor = razonSocialProveedor;
        this.rucProveedor = rucProveedor;
        this.fecha = fecha;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalNotaCredito = montoTotalNotaCredito;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getNroNotaCredito() {
        return nroNotaCredito;
    }

    public void setNroNotaCredito(String nroNotaCredito) {
        this.nroNotaCredito = nroNotaCredito;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public String getVtoTimbrado() {
        return vtoTimbrado;
    }

    public void setVtoTimbrado(String vtoTimbrado) {
        this.vtoTimbrado = vtoTimbrado;
    }

    public String getRazonSocialComprador() {
        return razonSocialComprador;
    }

    public void setRazonSocialComprador(String razonSocialComprador) {
        this.razonSocialComprador = razonSocialComprador;
    }

    public String getRucComprador() {
        return rucComprador;
    }

    public void setRucComprador(String rucComprador) {
        this.rucComprador = rucComprador;
    }

    public String getDireccionComprador() {
        return direccionComprador;
    }

    public void setDireccionComprador(String direccionComprador) {
        this.direccionComprador = direccionComprador;
    }

    public String getTelefonoComprador() {
        return telefonoComprador;
    }

    public void setTelefonoComprador(String telefonoComprador) {
        this.telefonoComprador = telefonoComprador;
    }

    public String getRazonSocialProveedor() {
        return razonSocialProveedor;
    }

    public void setRazonSocialProveedor(String razonSocialProveedor) {
        this.razonSocialProveedor = razonSocialProveedor;
    }

    public String getRucProveedor() {
        return rucProveedor;
    }

    public void setRucProveedor(String rucProveedor) {
        this.rucProveedor = rucProveedor;
    }

    public String getDireccionProveedor() {
        return direccionProveedor;
    }

    public void setDireccionProveedor(String direccionProveedor) {
        this.direccionProveedor = direccionProveedor;
    }

    public String getTelefonoProveedor() {
        return telefonoProveedor;
    }

    public void setTelefonoProveedor(String telefonoProveedor) {
        this.telefonoProveedor = telefonoProveedor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalNotaCredito() {
        return montoTotalNotaCredito;
    }

    public void setMontoTotalNotaCredito(BigDecimal montoTotalNotaCredito) {
        this.montoTotalNotaCredito = montoTotalNotaCredito;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof NotaCredito)) {
            return false;
        }
        NotaCredito other = (NotaCredito) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.NotaCredito[ id=" + id + " ]";
    }

}
