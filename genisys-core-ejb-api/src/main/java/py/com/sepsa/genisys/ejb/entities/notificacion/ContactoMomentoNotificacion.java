/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.notificacion;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "contacto_momento_notificacion", schema = "notificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContactoMomentoNotificacion.findAll", query = "SELECT c FROM ContactoMomentoNotificacion c"),
    @NamedQuery(name = "ContactoMomentoNotificacion.findByIdContacto", query = "SELECT c FROM ContactoMomentoNotificacion c WHERE c.contactoMomentoNotificacionPK.idContacto = :idContacto"),
    @NamedQuery(name = "ContactoMomentoNotificacion.findByIdMomentoNotificacion", query = "SELECT c FROM ContactoMomentoNotificacion c WHERE c.contactoMomentoNotificacionPK.idMomentoNotificacion = :idMomentoNotificacion")})
public class ContactoMomentoNotificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContactoMomentoNotificacionPK contactoMomentoNotificacionPK;
    @JoinColumn(name = "id_momento_notificacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private MomentoNotificacion momentoNotificacion;

    public ContactoMomentoNotificacion() {
    }

    public ContactoMomentoNotificacion(ContactoMomentoNotificacionPK contactoMomentoNotificacionPK) {
        this.contactoMomentoNotificacionPK = contactoMomentoNotificacionPK;
    }

    public ContactoMomentoNotificacion(int idContacto, int idMomentoNotificacion) {
        this.contactoMomentoNotificacionPK = new ContactoMomentoNotificacionPK(idContacto, idMomentoNotificacion);
    }

    public ContactoMomentoNotificacionPK getContactoMomentoNotificacionPK() {
        return contactoMomentoNotificacionPK;
    }

    public void setContactoMomentoNotificacionPK(ContactoMomentoNotificacionPK contactoMomentoNotificacionPK) {
        this.contactoMomentoNotificacionPK = contactoMomentoNotificacionPK;
    }

    public MomentoNotificacion getMomentoNotificacion() {
        return momentoNotificacion;
    }

    public void setMomentoNotificacion(MomentoNotificacion momentoNotificacion) {
        this.momentoNotificacion = momentoNotificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contactoMomentoNotificacionPK != null ? contactoMomentoNotificacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ContactoMomentoNotificacion)) {
            return false;
        }
        ContactoMomentoNotificacion other = (ContactoMomentoNotificacion) object;
        if ((this.contactoMomentoNotificacionPK == null && other.contactoMomentoNotificacionPK != null) || (this.contactoMomentoNotificacionPK != null && !this.contactoMomentoNotificacionPK.equals(other.contactoMomentoNotificacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.notificacion.ContactoMomentoNotificacion[ contactoMomentoNotificacionPK=" + contactoMomentoNotificacionPK + " ]";
    }

}
