package py.com.sepsa.genisys.ejb.facades.trans;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.trans.ProveedorComprador;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 * Fachada de la entidad proveedor comprador
 *
 * @author Daniel F. Escauriza Arza
 */
@Stateless(name = "ProveedorCompradorFacade", mappedName = "ProveedorCompradorFacade")
@LocalBean
public class ProveedorCompradorFacade extends AbstractFacade<ProveedorComprador> {

    /**
     * Constructor de ProveedorCompradorFacade
     */
    public ProveedorCompradorFacade() {
        super(ProveedorComprador.class);
    }
}
