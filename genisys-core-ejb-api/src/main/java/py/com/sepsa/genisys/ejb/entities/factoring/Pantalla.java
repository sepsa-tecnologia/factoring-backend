/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "pantalla", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pantalla.findAll", query = "SELECT p FROM Pantalla p"),
    @NamedQuery(name = "Pantalla.findById", query = "SELECT p FROM Pantalla p WHERE p.id = :id"),
    @NamedQuery(name = "Pantalla.findByNombre", query = "SELECT p FROM Pantalla p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Pantalla.findByDescripcion", query = "SELECT p FROM Pantalla p WHERE p.descripcion = :descripcion"),
    @NamedQuery(name = "Pantalla.findByEstado", query = "SELECT p FROM Pantalla p WHERE p.estado = :estado"),
    @NamedQuery(name = "Pantalla.findByUrl", query = "SELECT p FROM Pantalla p WHERE p.url = :url")})
public class Pantalla implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private Character estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "url")
    private String url;
    @JoinTable(name = "perfil_pantalla", joinColumns = {
        @JoinColumn(name = "id_pantalla", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "id_perfil", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<Perfil> perfilCollection;
    @OneToMany(mappedBy = "idPadre", fetch = FetchType.LAZY)
    private Collection<Pantalla> pantallaCollection;
    @JoinColumn(name = "id_padre", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pantalla idPadre;

    public Pantalla() {
    }

    public Pantalla(Integer id) {
        this.id = id;
    }

    public Pantalla(Integer id, String nombre, String descripcion, Character estado, String url) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estado = estado;
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @XmlTransient
    public Collection<Perfil> getPerfilCollection() {
        return perfilCollection;
    }

    public void setPerfilCollection(Collection<Perfil> perfilCollection) {
        this.perfilCollection = perfilCollection;
    }

    @XmlTransient
    public Collection<Pantalla> getPantallaCollection() {
        return pantallaCollection;
    }

    public void setPantallaCollection(Collection<Pantalla> pantallaCollection) {
        this.pantallaCollection = pantallaCollection;
    }

    public Pantalla getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Pantalla idPadre) {
        this.idPadre = idPadre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Pantalla)) {
            return false;
        }
        Pantalla other = (Pantalla) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.Pantalla[ id=" + id + " ]";
    }

}
