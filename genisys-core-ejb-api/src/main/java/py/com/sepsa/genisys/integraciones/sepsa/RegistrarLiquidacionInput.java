/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.sepsa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
@Data
public class RegistrarLiquidacionInput implements Serializable {

    private String codigoIsoMoneda;
    private String codigoProducto;
    private String codigo;
    private Date fechaEvento;
    private Integer idCliente;
    private String codigoEstado;
    private String codigoTipoRegistroLiquidacion;
    private BigDecimal valor;

}
