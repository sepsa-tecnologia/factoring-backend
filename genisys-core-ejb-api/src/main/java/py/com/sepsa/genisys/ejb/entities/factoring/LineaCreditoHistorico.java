/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "linea_credito_historico", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LineaCreditoHistorico.findAll", query = "SELECT l FROM LineaCreditoHistorico l"),
    @NamedQuery(name = "LineaCreditoHistorico.findById", query = "SELECT l FROM LineaCreditoHistorico l WHERE l.id = :id"),
    @NamedQuery(name = "LineaCreditoHistorico.findByIdUsuario", query = "SELECT l FROM LineaCreditoHistorico l WHERE l.idUsuario = :idUsuario"),
    @NamedQuery(name = "LineaCreditoHistorico.findByFecha", query = "SELECT l FROM LineaCreditoHistorico l WHERE l.fecha = :fecha"),
    @NamedQuery(name = "LineaCreditoHistorico.findByOperacion", query = "SELECT l FROM LineaCreditoHistorico l WHERE l.operacion = :operacion"),
    @NamedQuery(name = "LineaCreditoHistorico.findByObservaciones", query = "SELECT l FROM LineaCreditoHistorico l WHERE l.observaciones = :observaciones")})
public class LineaCreditoHistorico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "operacion")
    private Character operacion;
    @Column(name = "id_moneda")
    private Integer idMoneda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto")
    private BigDecimal monto;
    @Size(max = 2147483647)
    @Column(name = "observaciones")
    private String observaciones;
    @JoinColumn(name = "id_linea_credito", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private LineaCredito idLineaCredito;

    // Campos adicionales.
    @JoinColumn(name = "id_moneda", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Moneda moneda;

    public LineaCreditoHistorico() {
    }

    public LineaCreditoHistorico(Integer id) {
        this.id = id;
    }

    public LineaCreditoHistorico(Integer id, Date fecha, Character operacion) {
        this.id = id;
        this.fecha = fecha;
        this.operacion = operacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Character getOperacion() {
        return operacion;
    }

    public void setOperacion(Character operacion) {
        this.operacion = operacion;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public LineaCredito getIdLineaCredito() {
        return idLineaCredito;
    }

    public void setIdLineaCredito(LineaCredito idLineaCredito) {
        this.idLineaCredito = idLineaCredito;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof LineaCreditoHistorico)) {
            return false;
        }
        LineaCreditoHistorico other = (LineaCreditoHistorico) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.LineaCreditoHistorico[ id=" + id + " ]";
    }

}
