/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils.email;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum EmailContentType {
    TEXT("text/plain"),
    HTML("text/html");
    private final String mimeType;

    private EmailContentType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String mimeType() {
        return mimeType;
    }

    @Override
    public String toString() {
        return mimeType;
    }
}
