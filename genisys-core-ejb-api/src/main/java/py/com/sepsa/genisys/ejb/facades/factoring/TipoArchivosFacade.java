/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoArchivo;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Alexander Triana Ríos
 */
@Stateless(name = "TipoArchivosFacade", mappedName = "TipoArchivosFacade")
@LocalBean
public class TipoArchivosFacade extends AbstractFacade<TipoArchivo> {

    /**
     * Constructor.
     */
    public TipoArchivosFacade() {
        super(TipoArchivo.class);
    }

    /**
     * Permite obtener un tipo de valor de cargo en base a su descripcion.
     *
     * @param descripcion Descripcion del tipo de cargo.
     * @return Tipo de cargo que concuerda con la descripcion, null en caso
     * contrario.
     */
    public TipoArchivo findByDescripcion(String descripcion) {
        Query q = getEntityManager().createNamedQuery("TipoArchivo.findByDescripcion");
        q.setParameter("descripcion", descripcion);
        return JpaUtils.getFirst(q);
    }
}
