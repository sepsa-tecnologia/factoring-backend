/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.logging.app.AppLogger;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.jboss.ejb3.annotation.TransactionTimeout;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.factoring.PersonaEntidad;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Entidades;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Constantes;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.TxWrapperCore;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_ActualizarAdhesionesProveedorPendientes", mappedName = "PF_BancoRio_ActualizarAdhesionesProveedorPendientes")
@LocalBean
public class PF_BancoRio_ActualizarAdhesionesPendientes implements Serializable {

    private AppLogger log;

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Entidades entidades;

    @EJB
    private Perfiles perfiles;

    @EJB
    private Notificaciones notificaciones;

    @EJB
    private PF_BancoRio_ActualizarAdhesionPendiente actualizarAdhesionPendiente;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    @TransactionTimeout(value = Constantes.TX_TIMEOUT_VALUE)
    public void work() throws Throwable {
        // Obtener perfiles.
        Perfil PROVEEDOR = perfiles.obtenerPerfil(PerfilEnum.PROVEEDOR);

        // Obtener operaciones de adhesion pendientes.
        log.debug("Obteniendo adhesiones pendientes ...");
        Collection<PersonaEntidad> adhesiones = obtenerSolicitudesAdhesionPendientes();
        log.debug("Consultas adhesiones: %s", Lists.size(adhesiones));
        for (PersonaEntidad adhesion : adhesiones) {
            // Obtener identificador de entidad financiera para adhesion
            Integer idEntidadFinanciera = adhesion.getIdEntidadFinanciera();
            if (!bancoRio.esBancoRio(idEntidadFinanciera)) {
                continue;
            }

            // Obtener identificador de perfil.
            Integer idPerfil = adhesion.getIdPerfil();
            if (!Objects.equals(idPerfil, PROVEEDOR.getId())) {
                continue;
            }

            // Actualizar solicitud de adhesion.
            TxWrapperCore tx = Injection.lookup(TxWrapperCore.class);
            Result<?> result = tx.execute(() -> actualizarAdhesionPendiente.work(adhesion));
            if (result.isFailure()) {
                notificaciones.notificarSepsa(result.failure().cause(), "Ocurrió un error procesando solicitud de adhesion id=%s", adhesion.getId());
            }
        }
    }

    private Collection<PersonaEntidad> obtenerSolicitudesAdhesionPendientes() throws Throwable {
        Result<List<PersonaEntidad>> result = entidades.obtenerSolicitudesAdhesionPendientes();
        result.raise();
        return result.value(Lists.empty());
    }

}
