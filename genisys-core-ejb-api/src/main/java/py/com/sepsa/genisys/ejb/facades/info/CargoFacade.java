/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.info;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.info.Cargo;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author descauriza
 */
@Stateless(name = "CargoFacade", mappedName = "CargoFacade")
@LocalBean
public class CargoFacade extends AbstractFacade<Cargo> {

    public CargoFacade() {
        super(Cargo.class);
    }

    public Cargo findById(Integer idCargo) {
        Query q = getEntityManager().createNamedQuery("Cargo.findById");
        q.setParameter("idCargo", idCargo);
        return JpaUtils.getFirst(q);
    }

}
