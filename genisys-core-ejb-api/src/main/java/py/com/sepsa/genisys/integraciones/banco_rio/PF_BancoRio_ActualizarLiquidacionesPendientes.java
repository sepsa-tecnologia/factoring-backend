/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.misc.fechas.Fechas;
import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.jboss.ejb3.annotation.TransactionTimeout;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioSeguimientoLiquidacion;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Constantes;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.TxWrapperCore;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_ActualizarLiquidacionesPendientes", mappedName = "PF_BancoRio_ActualizarLiquidacionesPendientes")
@LocalBean
public class PF_BancoRio_ActualizarLiquidacionesPendientes implements Serializable {

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Notificaciones notificaciones;

    @EJB
    private PF_BancoRio_ActualizarLiquidacionPendiente actualizarLiquidacionPendiente;

    private AppLogger log;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    @TransactionTimeout(value = Constantes.TX_TIMEOUT_VALUE * 3)
    public void work() throws Throwable {
        // Obtener identificador de usuario para ingestion de datos.
        log.debug("Obteniendo usuario de ingestión ...");
        Integer idUsuarioIngestion = bancoRio.obtenerIdUsuarioIngestion();
        if (idUsuarioIngestion <= 0) {
            throw Errors.illegalState("El usuario para la ingestión no está establecido");
        }

        // Obtener cantidad de dias de tolerancia.
        log.debug("Obteniendo dias de tolerancia ...");
        Integer diasTolerancia = bancoRio.obtenerDiasTolerancia();

        // Obtener operaciones de desembolsos pendientes.
        log.debug("Obteniendo liquidaciones pendientes ...");
        Collection<BancoRioSeguimientoLiquidacion> consultas = obtenerLiquidacionesPendientes(diasTolerancia);

        log.debug("Consultas pendientes: %s", Lists.size(consultas));
        for (BancoRioSeguimientoLiquidacion consulta : consultas) {
            TxWrapperCore tx = Injection.lookup(TxWrapperCore.class);
            Result<?> result = tx.execute(() -> work0(idUsuarioIngestion, consulta));
            if (result.isFailure()) {
                notificaciones.notificarSepsa(result.failure().cause(), "Ocurrió un error procesando solicitud de liquidación id=%s", consulta.getId());
            }
        }

        if (true) {
            System.out.println("a");
        }
    }

    /**
     * Obtiene la lista de consultas pendientes para facturas pagadas
     * (desembolsadas).
     *
     * @param diasTolerancia Cantidad de dias atras a considerar. Es decir, la
     * tolerancia en dias para considerar la consulta de estado para facturas.
     * @return Lista de consultas pendientes.
     * @throws Throwable Si ocurre un error.
     */
    private Collection<BancoRioSeguimientoLiquidacion> obtenerLiquidacionesPendientes(int diasTolerancia) throws Throwable {
        Date fecha = Fechas.addValue(Fechas.now(), Calendar.DAY_OF_MONTH, -1 * Math.abs(diasTolerancia));
        Result<Collection<BancoRioSeguimientoLiquidacion>> result = bancoRio.obtenerSolicitudesLiquidacionPendientes(Fechas.inicioDia(fecha));
        result.raise();
        return result.value(Lists.empty());
    }

    private Void work0(Integer idUsuarioIngestion, BancoRioSeguimientoLiquidacion solicitudLiquidacion) throws Throwable {
        actualizarLiquidacionPendiente.work(idUsuarioIngestion, solicitudLiquidacion);
        return null;
    }

}
