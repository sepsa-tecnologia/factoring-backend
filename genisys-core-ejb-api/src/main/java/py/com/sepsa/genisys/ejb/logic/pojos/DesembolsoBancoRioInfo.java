/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.genisys.ejb.logic.pojos;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class DesembolsoBancoRioInfo implements Serializable {

    private Integer facturaId;
    private Integer facturaEstadoId;
    private String facturaEstado;
    private String estadoDesembolso;
    private String estadoLiquidacion;
    private String facturaNro;
    private String facturaFechaEmision;
    private String facturaFechaPago;
    private String facturaRazonSocialComprador;
    private String facturaRucComprador;
    private String facturaRazonSocialProveedor;
    private String facturaRucProveedor;
    private String facturaMontoTotal;
    private String facturaMontoConformado;
    private String facturaMontoAdelantado;
    private String facturaMontoDistribucion;
    private String facturaMontoDistribucionSepsa;
    private String facturaMontoDistribucionComprador;
    private Integer facturaDiasAnticipo;
    private String bancoIdSolicitud;
    private String bancoNroOperacion;
    private Date bancoFechaSolicitudDesembolso;
    private Date bancoFechaUltimaConsultaEstadoDesembolso;
    private Date bancoFechaSolicitudLiquidacion;
    private Date bancoFechaUltimaConsultaEstadoLiquidacion;
    private String codigoLiquidacionSepsa;
    private String codigoLiquidacionComprador;
    private String codigoLiquidacionProveedor;
    private String codigoLiquidacionEntidad;

}
