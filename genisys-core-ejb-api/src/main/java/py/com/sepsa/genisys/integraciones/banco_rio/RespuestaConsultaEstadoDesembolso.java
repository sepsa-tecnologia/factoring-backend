/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.misc.Assertions;
import fa.gs.utils.misc.text.Text;
import fa.gs.utils.misc.xml.Xml;
import fa.gs.utils.misc.xml.XmlPrefixInfoList;
import java.util.Objects;
import javax.xml.xpath.XPath;
import lombok.Data;
import org.w3c.dom.Document;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class RespuestaConsultaEstadoDesembolso implements Respuesta {

    String codigo;
    String mensaje;
    String estado;
    String decision;

    public static RespuestaConsultaEstadoDesembolso parse(String xmlSoap) throws Throwable {
        // Eliminar el namespace que no utiliza ningun prefijo.
        xmlSoap = Text.replaceAll(xmlSoap, "xmlns=\"http://tempuri.org/\"", "");

        // Cargar XML de respuesta SOAP.
        Document xml = Xml.parse(xmlSoap);

        // Obtener prefijos y uris.
        XmlPrefixInfoList prefixes = Xml.readPrefixes(xml);

        // Preparar consultas XPath.
        XPath xpath = Xml.xpath(prefixes);
        String xCodigo = ".//CodigoError";
        String xMensaje = ".//Mensaje";
        String xEstado = ".//Estado";
        String xDecision = ".//ComentarioDecision";

        // Obtener valor.
        String codigo = Xml.readNodeTexContent(xpath, xCodigo, xml);
        String mensaje = Xml.readNodeTexContent(xpath, xMensaje, xml);
        String estado = Xml.readNodeTexContent(xpath, xEstado, xml);
        String decision = Xml.readNodeTexContent(xpath, xDecision, xml);

        // Procesar valores.
        RespuestaConsultaEstadoDesembolso respuesta = new RespuestaConsultaEstadoDesembolso();
        respuesta.codigo = codigo;
        respuesta.mensaje = mensaje;
        respuesta.estado = estado;
        respuesta.decision = decision;
        return respuesta;
    }

    public static RespuestaConsultaEstadoDesembolso instance(String codigo, String mensaje) {
        RespuestaConsultaEstadoDesembolso instance = new RespuestaConsultaEstadoDesembolso();
        instance.codigo = codigo;
        instance.mensaje = mensaje;
        instance.estado = "";
        instance.decision = "";
        return instance;
    }

    @Override
    public boolean isOk() {
        return Assertions.stringNullOrEmpty(codigo) == false && Objects.equals(codigo, "0");
    }

}
