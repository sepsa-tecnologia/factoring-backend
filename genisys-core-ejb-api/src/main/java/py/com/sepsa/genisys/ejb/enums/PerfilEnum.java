/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.enums;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum PerfilEnum {
    COMPRADOR("ROLE_COMPRADOR"),
    PROVEEDOR("ROLE_PROVEEDOR"),
    ENTIDAD_FINANCIERA("ROLE_ENTIDAD_FINANCIERA"),
    ADMINISTRADOR("ROLE_ADMINISTRADOR");
    private final String descripcion;

    /**
     * Constructor.
     *
     * @param descripcion Descripcion del tipo de cargo.
     * @param descripcionCorta Texto de ayuda.
     */
    PerfilEnum(String descripcion) {
        this.descripcion = descripcion;
    }

    public static String descripcionAsSimpleText(String descripcion) {
        descripcion = descripcion.replace("ROLE_", "");
        descripcion = descripcion.replace("_", " ");
        return descripcion;
    }

    public String descripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return descripcion.replace("ROLE_", "");
    }
}
