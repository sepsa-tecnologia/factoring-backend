/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
@Entity
@Table(name = "barrio", schema = "info")
@NamedQueries({
    @NamedQuery(name = "Barrio.obtenerPorId", query = "SELECT b from Barrio b WHERE b.id = :idBarrio")
})
public class Barrio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_localidad")
    private Integer idLocalidad;

    @Basic(optional = false)
    @NotNull
    @Column(name = "descripcion")
    private String descripcion;

}
