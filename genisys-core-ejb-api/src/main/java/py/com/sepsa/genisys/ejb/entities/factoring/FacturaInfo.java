/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import fa.gs.utils.collections.Maps;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import lombok.Data;
import py.com.sepsa.genisys.ejb.utils.Fechas;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class FacturaInfo implements Serializable {

    private Integer id;
    private Integer idLote;
    // Datos de comprador.
    private Integer compradorId;
    private String compradorRazonSocial;
    private String compradorRuc;
    // Datos de proveedor.
    private Integer proveedorId;
    private String proveedorRazonSocial;
    private String proveedorRuc;
    // Datos de entidad financiera a la que fue vendida la factura, si hubiere.
    private Integer entidadFinancieraId;
    private String entidadFinancieraRazonSocial;
    private String entidadFinancieraRuc;
    // Numero y timbrado.
    private String numero;
    private String timbradoNumero;
    private String timbradoVencimiento;
    // Valores de fecha.
    private Date fechaEmision;
    private Date fechaPago;
    // Valores de monto.
    private Integer monedaId;
    private BigDecimal montoTotalIva5;
    private BigDecimal montoTotalIva10;
    private BigDecimal montoTotalIva;
    private BigDecimal montoTotalExento;
    private BigDecimal montoTotalImponible;
    private BigDecimal montoTotal;
    private BigDecimal montoTotalConformado;
    private BigDecimal montoTotalDescuento;
    private BigDecimal porcentajeDescuento;
    private BigDecimal montoTotalDesembolso;
    private BigDecimal montoTotalDistribucion;
    // Metricas de dias para operacion.
    private Integer diasPlazo;
    private Integer diasAnticipo;
    private Integer diasVencimiento;
    // Tipo de factura.
    private Boolean esFacturaEdi;
    // Estado de factura.
    private Integer estadoId;
    private String estadoDescripcion;
    // Convenio utilizado para la solicitud del desembolso, si hubiere.
    private Integer convenioId;
    private String convenioDescripcion;

    public static FacturaInfo instance(Map<String, Object> row) {
        FacturaInfo instance = new FacturaInfo();
        instance.id = Maps.integer(row, "id");
        instance.idLote = Maps.integer(row, "loteId");
        instance.compradorId = Maps.integer(row, "compradorId");
        instance.compradorRazonSocial = Maps.string(row, "compradorRazonSocial");
        instance.compradorRuc = Maps.string(row, "compradorRuc");
        instance.proveedorId = Maps.integer(row, "proveedorId");
        instance.proveedorRazonSocial = Maps.string(row, "proveedorRazonSocial");
        instance.proveedorRuc = Maps.string(row, "proveedorRuc");
        instance.entidadFinancieraId = Maps.integer(row, "entidadFinancieraId");
        instance.entidadFinancieraRazonSocial = Maps.string(row, "entidadFinancieraRazonSocial");
        instance.entidadFinancieraRuc = Maps.string(row, "entidadFinancieraRuc");
        instance.numero = Maps.string(row, "numero");
        instance.timbradoNumero = Maps.string(row, "timbradoNumero");
        instance.timbradoVencimiento = Maps.string(row, "timbradoVencimiento");
        instance.fechaEmision = Maps.date(row, "fechaEmision");
        instance.fechaPago = Maps.date(row, "fechaPago");
        instance.monedaId = Maps.integer(row, "monedaId");
        instance.montoTotalIva5 = Maps.bigdecimal(row, "montoTotalIva5");
        instance.montoTotalIva10 = Maps.bigdecimal(row, "montoTotalIva10");
        instance.montoTotalIva = Maps.bigdecimal(row, "montoTotalIva");
        instance.montoTotalExento = Maps.bigdecimal(row, "montoTotalExento");
        instance.montoTotalImponible = Maps.bigdecimal(row, "montoTotalImponible");
        instance.montoTotal = Maps.bigdecimal(row, "montoTotal");
        instance.montoTotalConformado = Maps.bigdecimal(row, "montoTotalConformado");
        instance.montoTotalDescuento = Maps.bigdecimal(row, "montoTotalDescuento");
        instance.porcentajeDescuento = Maps.bigdecimal(row, "porcentajeDescuento");
        instance.montoTotalDesembolso = Maps.bigdecimal(row, "montoTotalDesembolso");
        instance.montoTotalDistribucion = Maps.bigdecimal(row, "montoTotalDistribucion");
        instance.diasPlazo = Maps.integer(row, "diasPlazo");
        instance.diasAnticipo = Maps.integer(row, "diasAnticipo");
        instance.diasVencimiento = Fechas.diasDiferencia(new Date(), instance.fechaPago);
        instance.esFacturaEdi = Maps.bool(row, "esFacturaEdi");
        instance.estadoId = Maps.integer(row, "estadoId");
        instance.estadoDescripcion = Maps.string(row, "estadoDescripcion");
        instance.convenioId = Maps.integer(row, "convenioId");
        instance.convenioDescripcion = Maps.string(row, "convenioDescripcion");
        return instance;
    }

}
