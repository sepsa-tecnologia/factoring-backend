/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import javax.ejb.Remote;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.JobStamp;

/**
 *
 * @author Fabio A. González Sosa
 */
@Remote
public interface Batchlets {

    public static Batchlets inject() {
        String jndi = "java:global/genisys-core-0.0.1/Batchlets!py.com.sepsa.genisys.ejb.logic.impl.Batchlets";
        return (Batchlets) Injection.lookup(jndi);
    }

    void stamp(String name);

    void ellapsed(String name, long millis);

    void sleep(String name, long period);

    JobStamp[] list();

}
