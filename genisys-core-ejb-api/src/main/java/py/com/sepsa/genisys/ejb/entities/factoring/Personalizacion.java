/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "personalizacion", schema = "factoring")
@Data
@NamedQueries({
    @NamedQuery(name = "Personalizacion.findAll", query = "SELECT c FROM Personalizacion c"),
    @NamedQuery(name = "Personalizacion.findById", query = "SELECT c FROM Personalizacion c WHERE c.id = :id"),
    @NamedQuery(name = "Personalizacion.findByPersonaTipo", query = "SELECT c FROM Personalizacion c WHERE c.idPersona = :idPersona and c.tipo = :tipo"),})
public class Personalizacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_persona")
    private int idPersona;

    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo")
    private String tipo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "valor")
    private String valor;

    public Personalizacion() {
    }

    public Personalizacion(Integer id) {
        this.id = id;
    }

}
