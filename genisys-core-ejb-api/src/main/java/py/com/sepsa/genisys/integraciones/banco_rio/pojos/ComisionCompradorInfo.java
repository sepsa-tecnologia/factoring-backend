/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio.pojos;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
@Data
public class ComisionCompradorInfo implements Serializable {

    private BigDecimal porcentajeComision;
    private BigDecimal montoComision;
    private String nroCuentaCredito;
}
