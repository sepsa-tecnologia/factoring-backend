/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import lombok.Data;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.FacturaInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class CalculoNetoFacturasInput implements Serializable {

    private DocumentoInfo documentoProveedor;
    private DocumentoInfo documentoComprador;
    private BigDecimal porcentajeUsoPlataforma;
    private Collection<FacturaInfo> facturas;

}
