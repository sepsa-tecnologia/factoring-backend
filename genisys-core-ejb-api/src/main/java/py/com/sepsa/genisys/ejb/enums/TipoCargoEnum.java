/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.enums;

/**
 *
 * @author Fabio A. González Sosa
 */
/**
 * Enumeracion para los tipos de cargo posibles.
 */
public enum TipoCargoEnum {
    //<editor-fold defaultstate="collapsed" desc="Constantes">
    /**
     * Indica el numero de version para la coleccion de cargos asociados a cada
     * convenio.
     *
     * @since 2.
     */
    VERSION_CARGOS("VERSION_CARGOS", "", ""),
    /**
     * Indica el porcentaje total configurado, segun tasa diaria, mensual o
     * anual.
     *
     * @since 2.
     */
    PORCENTAJE_TOTAL_IMPONIBLE_COMISION("PORCENTAJE_TOTAL_COMISION", "Total Comisión", "Porcentaje total de Comisión."),
    /**
     * Indica el porcentaje total (diario) imponible de comision que se aplica
     * sobre el monto conformado de facturas. Se deduce de
     * <code>{@link py.com.sepsa.genisys.ejb.logic.impl.TipoCargos.TipoCargoEnum#PORCENTAJE_TOTAL_IMPONIBLE_COMISION PORCENTAJE_TOTAL_IMPONIBLE_COMISION}
     * / factor_conversion</code>.
     *
     * @since 2.
     */
    PORCENTAJE_IMPONIBLE_COMISION("PORCENTAJE_COMISION", "Total Comisión", "Porcentaje total diario de Comisión."),
    /**
     * Impuesto a aplicar sobre el monto total de comisión a descontar.
     *
     * @since 1.
     */
    PORCENTAJE_IMPUESTO_COMISION("IMPUESTO_VENTA", "Impuesto sobre Comisión", "Impuesto a aplicar sobre el monto total de comisión a descontar."),
    /**
     * Indica el porcentaje que le corresponde a SEPSA del total diario de
     * comisión aplicable.
     *
     * @since 2.
     */
    PORCENTAJE_TOTAL_PARTE_COMISION_SEPSA("PORCENTAJE_TOTAL_COMISION_SEPSA", "Porcentaje Parte SEPSA", "Porcentaje de distribución de comisión configurado."),
    /**
     * Indica el porcentaje total (diario) imponible de comision que corresponde
     * a SEPSA, en base a la distribucion configurada. Se deduce de
     * <code>{@link py.com.sepsa.genisys.ejb.logic.impl.TipoCargos.TipoCargoEnum#PORCENTAJE_IMPONIBLE_COMISION PORCENTAJE_IMPONIBLE_COMISION} * {@link py.com.sepsa.genisys.ejb.logic.impl.TipoCargos.TipoCargoEnum#PORCENTAJE_TOTAL_PARTE_COMISION_SEPSA PORCENTAJE_TOTAL_PARTE_COMISION_SEPSA}
     * / 100</code>.
     *
     * @since 1.
     */
    PORCENTAJE_IMPONIBLE_COMISION_SEPSA("COMISION_SEPSA", "Comisión SEPSA", "Interés Diario de Comisión correspondiente a SEPSA."),
    /**
     * Indica el porcentaje que le corresponde al comprador del total diario de
     * comisión aplicable.
     *
     * @since 2.
     */
    PORCENTAJE_TOTAL_PARTE_COMISION_COMPRADOR("PORCENTAJE_TOTAL_COMISION_COMPRADOR", "Porcentaje Parte Comprador", "Porcentaje de distribución de comisión configurado."),
    /**
     * Indica el porcentaje total (diario) imponible de comision que corresponde
     * al comprador, en base a la distribucion configurada. Se deduce de
     * <code>{@link py.com.sepsa.genisys.ejb.logic.impl.TipoCargos.TipoCargoEnum#PORCENTAJE_IMPONIBLE_COMISION PORCENTAJE_IMPONIBLE_COMISION} * {@link py.com.sepsa.genisys.ejb.logic.impl.TipoCargos.TipoCargoEnum#PORCENTAJE_TOTAL_PARTE_COMISION_COMPRADOR PORCENTAJE_TOTAL_PARTE_COMISION_COMPRADOR}
     * / 100</code>.
     *
     * @since 1.
     */
    PORCENTAJE_IMPONIBLE_COMISION_COMPRADOR("COMISION_COMPRADOR", "Comisión Comprador", "Interés Diario de Comisión correspondiente al Comprador."),
    /**
     * Indica el porcentaje que le corresponde a la entidad financiera del total
     * diario de comisión aplicable.
     *
     * @since 2.
     */
    PORCENTAJE_TOTAL_PARTE_COMISION_ENTIDAD_FINANCIERA("PORCENTAJE_TOTAL_COMISION_ENTIDAD_FINANCIERA", "Porcentaje Parte Entidad Financiera", "Porcentaje de distribución de comisión configurado."),
    /**
     * Indica el porcentaje total (diario) imponible de comision que corresponde
     * ala entidad financiera, en base a la distribucion configurada.
     *
     * @since 1.
     */
    PORCENTAJE_IMPONIBLE_COMISION_ENTIDAD_FINANCIERA("COMISION_ENTIDAD_FINANCIERA", "Comisión Entidad Financiera", "Interés Diario de Comisión correspondiente al Comprador."),
    /**
     * Porcentaje a aplicar sobre el monto neto de facturas en concepto de
     * gastos operativos. Se deduce de
     * <code>{@link py.com.sepsa.genisys.ejb.logic.impl.TipoCargos.TipoCargoEnum#PORCENTAJE_IMPONIBLE_COMISION PORCENTAJE_IMPONIBLE_COMISION} * {@link py.com.sepsa.genisys.ejb.logic.impl.TipoCargos.TipoCargoEnum#PORCENTAJE_IMPONIBLE_COMISION_ENTIDAD_FINANCIERA PORCENTAJE_IMPONIBLE_COMISION_ENTIDAD_FINANCIERA}
     * / 100</code>.
     *
     * @since 1.
     */
    PORCENTAJE_IMPONIBLE_GASTOS_OPERATIVOS("GASTOS_OPERATIVOS", "Gastos Operativos", "Porcentaje a aplicar sobre el monto neto de facturas en concepto de gastos operativos."),
    /**
     * Impuesto a aplicar sobre el monto total de gastos operativos.
     *
     * @since 1.
     */
    PORCENTAJE_IMPUESTO_GASTOS_OPERATIVOS("IMPUESTO_GASTOS_OPERATIVOS", "Impuesto sobre Gastos Operativos", "Impuesto a aplicar sobre el monto total de gastos operativos.");
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Descripcion del tipo de cargo en BD. Debe ser unico.
     */
    private final String descripcion;

    /**
     * Texto descriptivo de ayuda. Version corta.
     */
    private final String descripcionCorta;

    /**
     * Texto descriptivo de ayuda. Version larga.
     */
    private final String descripcionLarga;
    //</editor-fold>

    /**
     * Constructor.
     *
     * @param descripcion Descripcion del tipo de cargo.
     * @param descripcionCorta Texto de ayuda.
     */
    TipoCargoEnum(String descripcion, String descripcionCorta, String descripcionLarga) {
        this.descripcion = descripcion;
        this.descripcionCorta = descripcionCorta;
        this.descripcionLarga = descripcionLarga;
    }

    public String descripcion() {
        return descripcion;
    }

    public String descripcionCorta() {
        return descripcionCorta;
    }

    public String descripcionLarga() {
        return descripcionLarga;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
