/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.notificacion;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoring;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author descauriza
 */
@Stateless(name = "NotificacionEmailFactoringFacade", mappedName = "NotificacionEmailFactoringFacade")
@LocalBean
public class NotificacionEmailFactoringFacade extends AbstractFacade<NotificacionEmailFactoring> {

    public NotificacionEmailFactoringFacade() {
        super(NotificacionEmailFactoring.class);
    }

    /**
     * Obtiene la lista de notificaciones relacionadas al sistema de factoring
     * que se encuentran en cierto estado dado.
     *
     * @param estado Estado a consultar.
     * @return Lista de notificaciones.
     */
    public List<NotificacionEmailFactoring> findByEstado(Character estado) {
        Query q = getEntityManager().createNamedQuery("NotificacionEmailFactoring.findByEstado");
        q.setParameter("estado", estado);
        return JpaUtils.getAll(q);
    }

    /**
     * Obtiene la lista de identificadores de facturas relacionados a una
     * notificacion de operacion.
     *
     * @param idNotificacionEmailFactoring Identificador de notificacion.
     * @return Identificadores de facturas, si hubieren. Caso contrario
     * {@code null}.
     */
    public Integer[] findIdFacturas(Integer idNotificacionEmailFactoring) {
        String[] SQL = {
            "SELECT",
            "id_factura",
            "FROM notificacion.notificacion_email_factoring_factura",
            "WHERE",
            "id_notificacion = ?"
        };
        try {
            Query query = getEntityManager().createNativeQuery(String.join(" ", SQL));
            query.setParameter(1, idNotificacionEmailFactoring);
            Object[] ids = JpaUtils.getAll(query).stream().map((o) -> (Integer) o).toArray(Integer[]::new);
            return (Integer[]) ids;
        } catch (Throwable thr) {
            return null;
        }
    }

    /**
     * Elimina una notificacion de operacion sobre facturas.
     *
     * @param idNotificacionEmailFactoring Identificador de notificacion.
     * @return {@code true} si la eliminacion tuvo exito, caso contrarion
     * {@code false}.
     */
    public boolean eliminar(Integer idNotificacionEmailFactoring) {
        String[] SQL = {
            "DELETE",
            "FROM notificacion.notificacion_email_factoring",
            "WHERE",
            "id = ?"
        };
        try {
            // Eliminar facturas asociadas.
            eliminarNotificacionFacturas(idNotificacionEmailFactoring);

            // Eliminar notificacion.
            Query query = getEntityManager().createNativeQuery(String.join(" ", SQL));
            query.setParameter(1, idNotificacionEmailFactoring);
            query.executeUpdate();
            return true;
        } catch (Throwable thr) {
            return false;
        }
    }

    /**
     * Elimina las relaciones con facturas para una notificacion de operacion
     * dada.
     *
     * @param idNotificacionEmailFactoring Identificador de notificacion.
     * @return {@code true} si la eliminacion tuvo exito, caso contrarion
     * {@code false}.
     */
    private boolean eliminarNotificacionFacturas(Integer idNotificacionEmailFactoring) {
        String[] SQL = {
            "DELETE",
            "FROM notificacion.notificacion_email_factoring_factura",
            "WHERE",
            "id_notificacion = ?"
        };
        try {
            Query query = getEntityManager().createNativeQuery(String.join(" ", SQL));
            query.setParameter(1, idNotificacionEmailFactoring);
            query.executeUpdate();
            return true;
        } catch (Throwable thr) {
            return false;
        }
    }

}
