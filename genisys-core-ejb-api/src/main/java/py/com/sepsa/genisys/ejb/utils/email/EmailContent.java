/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils.email;

import java.util.Objects;

/**
 *
 * @author Fabio A. González Sosa
 */
public class EmailContent {

    private String[] destinatarios;
    private String cc;
    private String asunto;
    private String cuerpo;
    private EmailContentType type;

    public EmailContent() {
        this(null, null, null, null, EmailContentType.TEXT);
    }

    public EmailContent(String destinatario, String cc, String asunto, String cuerpo, EmailContentType type) {
        this.destinatarios = new String[]{destinatario};
        this.cc = cc;
        this.asunto = asunto;
        this.cuerpo = cuerpo;
        this.type = type;
    }

    public String[] getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(String[] destinatarios) {
        this.destinatarios = destinatarios;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public EmailContentType getContentType() {
        return type;
    }

    public void setContentType(EmailContentType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.destinatarios);
        hash = 29 * hash + Objects.hashCode(this.cc);
        hash = 29 * hash + Objects.hashCode(this.asunto);
        hash = 29 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmailContent other = (EmailContent) obj;
        if (!Objects.equals(this.destinatarios, other.destinatarios)) {
            return false;
        }
        if (!Objects.equals(this.cc, other.cc)) {
            return false;
        }
        if (!Objects.equals(this.asunto, other.asunto)) {
            return false;
        }
        if (!Objects.equals(this.cuerpo, other.cuerpo)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

}
