/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.misc.Assertions;
import fa.gs.utils.misc.text.Text;
import fa.gs.utils.misc.xml.Xml;
import fa.gs.utils.misc.xml.XmlPrefixInfoList;
import java.util.Objects;
import javax.xml.xpath.XPath;
import lombok.Data;
import org.w3c.dom.Document;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
@Data
public class RespuestaSolicitudLiquidacion implements Respuesta {

    String codigo;
    String mensaje;
    String idLiquidacion;

    public static RespuestaSolicitudLiquidacion parse(String xmlSoap) throws Throwable {
        // Eliminar el namespace que no utiliza ningun prefijo.
        xmlSoap = Text.replaceAll(xmlSoap, "xmlns=\"http://tempuri.org/\"", "");

        // Cargar XML de respuesta SOAP.
        Document xml = Xml.parse(xmlSoap);

        // Obtener prefijos y uris.
        XmlPrefixInfoList prefixes = Xml.readPrefixes(xml);

        // Preparar consultas XPath.
        XPath xpath = Xml.xpath(prefixes);
        String xCodigo = ".//CodigoError";
        String xMensaje = ".//Mensaje";
        String xIdLiquidacion = ".//IdLiquidacion";

        // Obtener valor.
        String codigo = Xml.readNodeTexContent(xpath, xCodigo, xml);
        String mensaje = Xml.readNodeTexContent(xpath, xMensaje, xml);
        String idLiquidacion = Xml.readNodeTexContent(xpath, xIdLiquidacion, xml);

        // Procesar valores.
        RespuestaSolicitudLiquidacion respuesta = new RespuestaSolicitudLiquidacion();
        respuesta.codigo = codigo;
        respuesta.mensaje = mensaje;
        respuesta.idLiquidacion = idLiquidacion;
        return respuesta;
    }

    public static RespuestaSolicitudLiquidacion instance(String codigo, String mensaje) {
        RespuestaSolicitudLiquidacion instance = new RespuestaSolicitudLiquidacion();
        instance.codigo = codigo;
        instance.mensaje = mensaje;
        return instance;
    }

    @Override
    public boolean isOk() {
        return Assertions.stringNullOrEmpty(codigo) == false && Objects.equals(codigo, "0");
    }

}
