/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.misc.errors.AppErrorException;
import fa.gs.result.simple.Result;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.fechas.Fechas;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoring;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.enums.TipoPlantilla;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
@Stateless(name = "PF_NotificarFacturasAVencer", mappedName = "PF_NotificarFacturasAVencer")
@LocalBean
public class PF_NotificarFacturasAVencer implements Serializable {

    private AppLogger log;

    @EJB
    private Facturas facturas;

    @EJB
    private Notificaciones notificaciones;

    @EJB
    private Personas personas;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.core();
    }

    public void work() throws Throwable {
        // Listados de compradores activos
        Result<Collection<Persona>> resCompradores = personas.obtenerCompradoresDisponibles();
        resCompradores.raise();
        Collection<Persona> compradores = resCompradores.value();

        // Día límite para última notificación.
        Integer intervaloDias = notificaciones.obtenerEsperaNotificaciones();
        Date fechaDesde = Fechas.addValue(Fechas.now(), Calendar.DAY_OF_MONTH, -1 * Math.abs(intervaloDias));

        // Días de anticipo de facturas a vencer
        Integer diasAnticipo = obtenerDiasAnticipo().intValue();

        // Obtiene la última notificación enviada a cada proveedor.
        for (Persona persona : compradores) {
            Result<List<FacturaInfo>> resFacturasPorVencer = facturas.obtenerFacturasPorVencer(persona.getId(), diasAnticipo);
            resFacturasPorVencer.raise();
            List<FacturaInfo> facturasConformadas = resFacturasPorVencer.value();
            if (!Assertions.isNullOrEmpty(facturasConformadas)) {
                Result<NotificacionEmailFactoring> resNotificacion = notificaciones.obtenerUltimaNotificacionPersona(persona.getId(), TipoPlantilla.RECORDATORIO_FACTURAS_POR_VENCER);
                resNotificacion.raise();
                NotificacionEmailFactoring notificacion = resNotificacion.value();
                // Si ya pasó el intervalo de días luego de la última notificación, se genera una nueva notificación
                if (Assertions.isNull(notificacion) || Fechas.menor(notificacion.getFechaInsercion(), fechaDesde)) {
                    notificaciones.crearNotificacionFacturasPorVencer(facturasConformadas);
                }
            }
        }
    }

    private Long obtenerDiasAnticipo() {
        Long defaultMillis = TimeUnit.DAYS.toDays(5);

        try {
            Configuraciones configuraciones = Injection.lookup(Configuraciones.class);
            Result<String> resConfig = configuraciones.buscarValorParametro(ConfiguracionGeneralEnum.NOTIFICACION_FACTURA_POR_VENCER_DIAS_ANTICIPO);
            resConfig.raise();
            String value = resConfig.value();
            return Long.valueOf(value);
        } catch (AppErrorException | NumberFormatException thr) {
            log.error(thr, "Ocurrió un error obteniendo intervalo de ejecucion de trabajo");
            return defaultMillis;
        }
    }
}
