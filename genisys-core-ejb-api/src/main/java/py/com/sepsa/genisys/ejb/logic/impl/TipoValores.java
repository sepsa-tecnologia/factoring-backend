/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.utils.misc.numeric.Currency;
import java8.util.Objects;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.ConfigCargo;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoValor;
import py.com.sepsa.genisys.ejb.enums.TipoValorEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.TipoValorFacade;
import py.com.sepsa.genisys.ejb.logic.LogicBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "TipoValores", mappedName = "TipoValores")
@LocalBean
public class TipoValores extends LogicBean {

    @EJB
    private TipoValorFacade facade;

    /**
     * Obtiene una enumeracion de tipo de valor en base a una entidad de
     * configuracion de cargo.
     *
     * @param cargo Entidad de configuracion de cargo.
     * @return Enumeracion de tipo de valor.
     */
    public TipoValorEnum obtenerTipoValor(ConfigCargo cargo) {
        return obtenerTipoValor(cargo.getIdTipoValor());
    }

    /**
     * Obtiene una enumeracion de tipo de valor en base a una entidad de tipo de
     * valor.
     *
     * @param cargo Entidad de tipo de valor.
     * @return Enumeracion de tipo de valor.
     */
    public TipoValorEnum obtenerTipoValor(TipoValor tipoValor) {
        for (TipoValorEnum tipoValorEnum : TipoValorEnum.values()) {
            if (Objects.equals(tipoValor.getDescripcion(), tipoValorEnum.descripcion())) {
                return tipoValorEnum;
            }
        }
        return null;
    }

    /**
     * Obtiene un tipo de valor en base a una enumeracion de tipo de valor.
     *
     * @param tipoCargo Enumeracion de tipo de valor.
     * @return Entidad tipo de valor.
     */
    public TipoValor obtenerTipoValor(TipoValorEnum tipoValor) {
        return facade.findByDescripcion(tipoValor.descripcion());
    }

    /**
     * Formatea el valor de una configuracion de cargo en base al tipo de valor
     * asociado.
     *
     * @param value Valor de cargo.
     * @param valorEnum Tipo de valor de cargo.
     * @return Cadena que representa el valor formateado a su correspondiente
     * tipo.
     */
    public String obtenerValorFormateado(ConfigCargo cargo) {
        TipoValorEnum valorEnum = obtenerTipoValor(cargo);
        switch (valorEnum) {
            case PORCENTAJE:
                return String.format("%s %%", Currency.formatCurrency(cargo.getValor()));
            case MONTO:
                return Currency.formatCurrency(cargo.getValor());
            case TEXTO:
                return cargo.getValorTexto();
            default:
                return "N/D";
        }
    }

}
