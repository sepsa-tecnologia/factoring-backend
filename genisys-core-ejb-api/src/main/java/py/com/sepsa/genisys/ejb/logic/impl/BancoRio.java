/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.criteria.query.Operators;
import fa.gs.criteria.query.QueryCriteria;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.database.jpa.Jpa;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.fechas.Fechas;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java8.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioActividadEconomica;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioBanco;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioCiudad;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioPersona;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioPeticion;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioRegion;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioSeguimientoDesembolso;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioSeguimientoLiquidacion;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoDocumento;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoSociedad;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.factoring.PersonaEntidad;
import py.com.sepsa.genisys.ejb.entities.factoring.PersonaEntidadSolicitud;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.BancoRioEstadoSeguimientoSolicitudDesembolsoEnum;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.enums.FacturaEstadoSeguimientoLiquidacionEnum;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.BancoRioSeguimientoDesembolsoFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.BancoRioSeguimientoLiquidacionFacade;
import py.com.sepsa.genisys.ejb.logic.pojos.DesembolsoBancoRioInfo;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;
import py.com.sepsa.genisys.ejb.utils.Maps2;
import py.com.sepsa.genisys.ejb.utils.Persistence;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.integraciones.banco_rio.BancoRioEstadoPeticionEnum;
import py.com.sepsa.genisys.integraciones.banco_rio.BancoRioTipoPeticionEnum;
import py.com.sepsa.genisys.integraciones.banco_rio.BancoRioWS;
import py.com.sepsa.genisys.integraciones.banco_rio.CalculoNetoFacturasInput;
import py.com.sepsa.genisys.integraciones.banco_rio.CalculoNetoFacturasOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaCompradoresInput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaCompradoresOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoCuentaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoCuentaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoDesembolsoInput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoDesembolsoOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoLiquidacionInput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoLiquidacionOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoProveedorInput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoProveedorOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaFacturasLiquidablesInput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaFacturasLiquidablesOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroCompradorPersonaJuridicaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroCompradorPersonaJuridicaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaFisicaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaFisicaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaJuridicaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaJuridicaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.Respuesta;
import py.com.sepsa.genisys.integraciones.banco_rio.RespuestaRegistroProveedor;
import py.com.sepsa.genisys.integraciones.banco_rio.SolicitudDesembolsoInput;
import py.com.sepsa.genisys.integraciones.banco_rio.SolicitudDesembolsoOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.SolicitudLiquidacionInput;
import py.com.sepsa.genisys.integraciones.banco_rio.SolicitudLiquidacionOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.FacturaALiquidarInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.WsTrackInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "BancoRio", mappedName = "BancoRio")
@LocalBean
public class BancoRio implements Serializable {

    @PersistenceContext(unitName = Persistence.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    private AppLogger log;

    @EJB
    private Configuraciones configuraciones;

    @EJB
    private Personas personas;

    @EJB
    private Perfiles perfiles;

    @EJB
    private Entidades entidades;

    @EJB
    private BancoRioSeguimientoDesembolsoFacade bancoRioSeguimientoDesembolsoFacade;

    @EJB
    private BancoRioSeguimientoLiquidacionFacade bancoRioSeguimientoLiquidacionFacade;

    @EJB
    private JpaUtils jpaUtils;

    private DecimalFormat DF_PORCENTAJE;

    @PostConstruct
    public void init() {
        this.log = AppLoggerFactory.core();
        this.DF_PORCENTAJE = new DecimalFormat("###0.00");
    }

    //<editor-fold defaultstate="collapsed" desc="Listar Operaciones de Desembolso">
    public Collection<DesembolsoBancoRioInfo> listarOperacionesMes() throws Throwable {
        return listarOperaciones(ListarOperacionesAlcance.MES_ACTUAL);
    }

    public Collection<DesembolsoBancoRioInfo> listarOperacionesALaFecha() throws Throwable {
        return listarOperaciones(ListarOperacionesAlcance.A_LA_FECHA);
    }

    private Collection<DesembolsoBancoRioInfo> listarOperaciones(ListarOperacionesAlcance tipo) throws Throwable {
        final Collection<DesembolsoBancoRioInfo> operaciones = Lists.empty();
        Collection<Map<String, Object>> rows = Jpa.select(buildQueryListarOperaciones(tipo), em);
        if (!Assertions.isNullOrEmpty(rows)) {
            for (Map<String, Object> row : rows) {
                DesembolsoBancoRioInfo operacion = new DesembolsoBancoRioInfo();
                operacion.setFacturaId(Numeric.adaptAsInteger(Maps2.get(row, "factura_id")));
                operacion.setFacturaEstadoId(Numeric.adaptAsInteger(Maps2.get(row, "factura_estado_id")));
                operacion.setFacturaEstado(Maps2.string(row, "factura_estado"));
                operacion.setEstadoDesembolso(Maps2.string(row, "estado_desembolso"));
                operacion.setEstadoLiquidacion(Maps2.string(row, "estado_liquidacion"));
                operacion.setFacturaNro(Maps2.string(row, "factura_nro"));
                operacion.setFacturaFechaEmision(Maps2.string(row, "factura_fecha_emision"));
                operacion.setFacturaFechaPago(Maps2.string(row, "factura_fecha_pago"));
                operacion.setFacturaRazonSocialComprador(Maps2.string(row, "factura_razon_social_comprador"));
                operacion.setFacturaRucComprador(Maps2.string(row, "factura_ruc_comprador"));
                operacion.setFacturaRazonSocialProveedor(Maps2.string(row, "factura_razon_social_proveedor"));
                operacion.setFacturaRucProveedor(Maps2.string(row, "factura_ruc_proveedor"));
                operacion.setFacturaMontoTotal(Maps2.string(row, "factura_monto_total"));
                operacion.setFacturaMontoConformado(Maps2.string(row, "factura_monto_conformado"));
                operacion.setFacturaMontoAdelantado(Maps2.string(row, "factura_monto_adelantado"));
                operacion.setFacturaMontoDistribucion(Maps2.string(row, "factura_monto_distribucion"));
                operacion.setFacturaMontoDistribucionSepsa(Maps2.string(row, "factura_monto_distribucion_sepsa"));
                operacion.setFacturaMontoDistribucionComprador(Maps2.string(row, "factura_monto_distribucion_comprador"));
                operacion.setFacturaDiasAnticipo(Numeric.adaptAsInteger(Maps2.get(row, "factura_dias_anticipo")));
                operacion.setBancoIdSolicitud(Maps2.string(row, "banco_id_solicitud"));
                operacion.setBancoNroOperacion(Maps2.string(row, "banco_nro_operacion"));
                operacion.setBancoFechaSolicitudDesembolso(Maps2.date(row, "banco_fecha_solicitud_desembolso"));
                operacion.setBancoFechaUltimaConsultaEstadoDesembolso(Maps2.date(row, "banco_fecha_ultima_consulta_estado_desembolso"));
                operacion.setBancoFechaSolicitudLiquidacion(Maps2.date(row, "banco_fecha_solicitud_liquidacion"));
                operacion.setBancoFechaUltimaConsultaEstadoLiquidacion(Maps2.date(row, "banco_fecha_ultima_consulta_estado_liquidacion"));
                operacion.setCodigoLiquidacionSepsa(Maps2.string(row, "codigo_liquidacion_sepsa"));
                operacion.setCodigoLiquidacionComprador(Maps2.string(row, "codigo_liquidacion_comprador"));
                operacion.setCodigoLiquidacionProveedor(Maps2.string(row, "codigo_liquidacion_proveedor"));
                operacion.setCodigoLiquidacionEntidad(Maps2.string(row, "codigo_liquidacion_entidad"));
                operaciones.add(operacion);
            }
        }
        return operaciones;
    }

    private String buildQueryListarOperaciones(ListarOperacionesAlcance tipo) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");
        builder.append("     f.id as \"factura_id\", ");
        builder.append("     e.id as \"factura_estado_id\", ");
        builder.append("     e.descripcion as \"factura_estado\", ");
        builder.append("     case when sd.estado = '0001' then 'PENDIENTE' when sd.estado = '0002' then 'APROBADO' else '--' end as \"estado_desembolso\", ");
        builder.append("     case when sl.estado = '0001' then 'PENDIENTE' when sl.estado = '0002' then 'APROBADO' when sl.estado = '0003' then 'RECHAZADO' else '--' end as \"estado_liquidacion\", ");
        builder.append("     f.nro_factura as \"factura_nro\", ");
        builder.append("     cast(f.fecha as text) as \"factura_fecha_emision\", ");
        builder.append("     cast(f.fecha_pago as text) as \"factura_fecha_pago\", ");
        builder.append("     f.razon_social_comprador as \"factura_razon_social_comprador\", ");
        builder.append("     f.ruc_comprador as \"factura_ruc_comprador\", ");
        builder.append("     f.razon_social_proveedor as \"factura_razon_social_proveedor\", ");
        builder.append("     f.ruc_proveedor as \"factura_ruc_proveedor\", ");
        builder.append("     cast(f.monto_total_factura as text) as \"factura_monto_total\", ");
        builder.append("     cast(fc.monto_conformado as text) as \"factura_monto_conformado\", ");
        builder.append("     cast(fc.monto_neto as text) as \"factura_monto_adelantado\", ");
        builder.append("     cast(fc.monto_distribucion as text) as \"factura_monto_distribucion\", ");
        builder.append("     cast(((fc.monto_distribucion * 30) / 100) as text) as \"factura_monto_distribucion_sepsa\", ");
        builder.append("     cast(((fc.monto_distribucion * 70) / 100) as text) \"factura_monto_distribucion_comprador\", ");
        builder.append("     fc.dias_anticipo as \"factura_dias_anticipo\", ");
        builder.append("     sd.id_solicitud as \"banco_id_solicitud\", ");
        builder.append("     sd.nro_operacion as \"banco_nro_operacion\", ");
        builder.append("     sd.fecha_solicitud as \"banco_fecha_solicitud_desembolso\", ");
        builder.append("     sd.fecha_respuesta as \"banco_fecha_ultima_consulta_estado_desembolso\", ");
        builder.append("     sl.fecha_solicitud as \"banco_fecha_solicitud_liquidacion\", ");
        builder.append("     sl.fecha_respuesta as \"banco_fecha_ultima_consulta_estado_liquidacion\", ");
        builder.append("     fsl.cod_liq_sepsa as \"codigo_liquidacion_sepsa\", ");
        builder.append("     fsl.cod_liq_comprador as \"codigo_liquidacion_comprador\", ");
        builder.append("     fsl.cod_liq_proveedor as \"codigo_liquidacion_proveedor\", ");
        builder.append("     fsl.cod_liq_entidad as \"codigo_liquidacion_entidad\" ");
        builder.append(" from factoring.factura as f ");
        builder.append(" left join factoring.banco_rio_seguimiento_desembolso as sd on sd.id_lote = f.id_lote ");
        builder.append(" left join factoring.banco_rio_seguimiento_liquidacion as sl on sl.id_factura = f.id ");
        builder.append(" left join factoring.factura_seguimiento_liquidacion as fsl on fsl.id_factura = f.id ");
        builder.append(" left join factoring.factura_conformada as fc on fc.id = f.id  ");
        builder.append(" left join lateral (select * from factoring.estado as e where e.id = f.id_estado) as e on true ");
        builder.append(" where ");
        builder.append("     1 = 1 ");
        builder.append("     and e.id != 11 ");
        if (tipo == ListarOperacionesAlcance.A_LA_FECHA) {
            builder.append("     and extract(year from sd.fecha_solicitud) = %s ", Fechas.getAnho());
        }
        if (tipo == ListarOperacionesAlcance.MES_ACTUAL) {
            builder.append("     and extract(year from sd.fecha_solicitud) = %s ", Fechas.getAnho());
            builder.append("     and extract(month from sd.fecha_solicitud) = %02d ", Fechas.getFieldValue(Fechas.now(), Calendar.MONTH));
        }
        builder.append(" order by  ");
        builder.append("   sd.fecha_solicitud asc ");
        return builder.toString();
    }

    enum ListarOperacionesAlcance {
        MES_ACTUAL,
        A_LA_FECHA
    }
    //</editor-fold>

    public Integer obtenerIdEntidadBancoRio() {
        try {
            Result<String> resConfig = configuraciones.buscarValorParametro(ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_ID_ENTIDAD);
            String value = resConfig.value("-1");
            return Integer.valueOf(value);
        } catch (Throwable thr) {
            return null;
        }
    }

    public Integer obtenerIdUsuarioIngestion() {
        try {
            Result<String> resConfig = configuraciones.buscarValorParametro(ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_INGESTION_ID_USUARIO);
            String value = resConfig.value("-1");
            return Integer.valueOf(value);
        } catch (Throwable thr) {
            return null;
        }
    }

    public Integer obtenerDiasTolerancia() throws Throwable {
        try {
            Result<String> resConfig = configuraciones.buscarValorParametro(ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_INGESTION_DIAS_TOLERANCIA);
            String value = resConfig.value("5");
            return Integer.valueOf(value);
        } catch (Throwable thr) {
            return 1;
        }
    }

    /**
     * Determina si un identificador de entidad financiera corresponde al valor
     * configurado para la entidad BANCO RIO SAECA.
     *
     * @param idEntidadFinanciera Identificador de entidad financiera.
     * @return {@code true} si identificador corresponde a Banco RIO, caso
     * contrario {@code false}.
     */
    public boolean esBancoRio(Integer idEntidadFinanciera) {
        // Verificacion.
        Integer idEntidadBancoRio = obtenerIdEntidadBancoRio();
        if (idEntidadBancoRio == null) {
            return false;
        } else {
            return Objects.equals(idEntidadBancoRio, idEntidadFinanciera);
        }
    }

    public String formatearPorcentaje(BigDecimal value) {
        return DF_PORCENTAJE.format(value);
    }

    public DocumentoInfo resolveDocumento(Persona persona) {
        DocumentoInfo documento = new DocumentoInfo();
        Personas.TipoPersonaEnum tipo = Personas.TipoPersonaEnum.fromIdTipoPersona(persona.getIdTipoPersona());
        if (tipo == Personas.TipoPersonaEnum.FISICA) {
            documento.setTipoDocumento(obtenerTipoDocumentoCi().value());
            documento.setNroDocumento(Text.ci(persona));
        } else if (tipo == Personas.TipoPersonaEnum.JURIDICA) {
            documento.setTipoDocumento(obtenerTipoDocumentoRuc().value());
            documento.setNroDocumento(Text.ruc(persona));
        }
        return documento;
    }

    public Result<Persona> obtenerPersonaPorCodigoCliente(Integer idPerfil, String codigoCliente) {
        try {
            // Construir query.
            String query = " select id_persona from factoring.banco_rio_persona where codigo_cliente = '%s' and id_perfil = %s ";
            query = Strings.format(query, codigoCliente, idPerfil);

            // Obtener valores.
            Map<String, Object> row = jpaUtils.getFirst(query);
            if (Assertions.isNullOrEmpty(row)) {
                throw Errors.illegalState("Persona no existe");
            }

            // Obtener persona.
            Integer idPersona = Maps.integer(row, "id_persona");
            return personas.obtenerPorId(idPersona);
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .build();
        }
    }

    public BancoRioWS obtenerClienteWs() throws Throwable {
        // Obtener parametros de conexion a web service.
        String wsUrl = configuraciones.buscarValorParametro(ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_WEB_SERVICE_URL).value("");
        String wsUsuario = configuraciones.buscarValorParametro(ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_WEB_SERVICE_USUARIO).value("");
        String wsPassword = configuraciones.buscarValorParametro(ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_WEB_SERVICE_PASSWORD).value("");
        String wsClavePublica = configuraciones.buscarValorParametro(ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_WEB_SERVICE_CLAVE_PUBLICA).value("");
        if (Assertions.stringNullOrEmpty(wsUrl) || Assertions.stringNullOrEmpty(wsUsuario) || Assertions.stringNullOrEmpty(wsPassword) || Assertions.stringNullOrEmpty(wsClavePublica)) {
            throw Errors.illegalState("Los parámetros de conexión a servicio de integración son inválidos.");
        }

        // Inicializar cliente.
        return BancoRioWS.instance(wsUsuario, wsPassword, wsClavePublica);
    }

    //<editor-fold defaultstate="collapsed" desc="Solicitudes de Desembolso">
    public Result<BancoRioSeguimientoDesembolso> registrarSolicitudDesembolso(Integer idComprador, Integer idProveedor, Integer idLote, String idSolicitud, String nroOperacion) {
        Result<BancoRioSeguimientoDesembolso> result;

        try {
            BancoRioSeguimientoDesembolso instance = new BancoRioSeguimientoDesembolso();
            instance.setIdComprador(idComprador);
            instance.setIdProveedor(idProveedor);
            instance.setIdLote(idLote);
            instance.setIdSolicitud(idSolicitud);
            instance.setNroOperacion(nroOperacion);
            instance.setEstado(BancoRioEstadoSeguimientoSolicitudDesembolsoEnum.PENDIENTE.codigo());
            instance.setFechaSolicitud(Fechas.now());
            instance.setFechaRespuesta(null);
            instance = jpaUtils.create(instance);

            result = Results.ok()
                    .value(instance)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message(thr.getMessage())
                    .build();
        }

        return result;
    }

    public Result<Collection<BancoRioSeguimientoDesembolso>> obtenerSolicitudesDesembolsoPendientes(Date fecha) {
        Result<Collection<BancoRioSeguimientoDesembolso>> result;

        try {
            QueryCriteria criteria = QueryCriteria.instance();
            criteria.where("estado", Operators.EQUALS, BancoRioEstadoSeguimientoSolicitudDesembolsoEnum.PENDIENTE.codigo());
            criteria.where("fechaSolicitud", Operators.GREATER_EQUAL, fecha);
            Collection<BancoRioSeguimientoDesembolso> value = bancoRioSeguimientoDesembolsoFacade.find(criteria);
            result = Results.ok()
                    .value(value)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .build();
        }

        return result;
    }

    public Result<BancoRioSeguimientoDesembolso> obtenerSolicitudDesembolso(Integer id) {
        Result<BancoRioSeguimientoDesembolso> result;

        try {
            QueryCriteria criteria = QueryCriteria.instance();
            criteria.where("id", Operators.EQUALS, id);
            Collection<BancoRioSeguimientoDesembolso> values = bancoRioSeguimientoDesembolsoFacade.find(criteria);
            BancoRioSeguimientoDesembolso value = Lists.first(values);

            result = Results.ok()
                    .value(value)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .build();
        }

        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Solicitudes de Liquidacion">
    public BancoRioSeguimientoLiquidacion obtenerSolicitudLiquidacionPorFactura(Integer idFactura) throws Throwable {
        // Construir query.
        String query = Strings.format(" select * from factoring.banco_rio_seguimiento_liquidacion where id_factura = %s ", idFactura);
        Map<String, Object> row = jpaUtils.getFirst(query);
        if (Assertions.isNullOrEmpty(row)) {
            return null;
        } else {
            return map(row);
        }
    }

    private BancoRioSeguimientoLiquidacion map(Map<String, Object> row) {
        BancoRioSeguimientoLiquidacion instance = new BancoRioSeguimientoLiquidacion();
        instance.setId(Maps.integer(row, "id"));
        instance.setIdFactura(Maps.integer(row, "id_factura"));
        instance.setIdLiquidacion(Maps.string(row, "id_liquidacion"));
        instance.setEstado(Maps.string(row, "estado"));
        instance.setFechaSolicitud(Maps.date(row, "fecha_solicitud"));
        instance.setFechaRespuesta(Maps.date(row, "fecha_respuesta"));
        return instance;
    }

    public BancoRioSeguimientoLiquidacion registrarSolicitudLiquidacion(Integer idFactura) throws Throwable {
        BancoRioSeguimientoLiquidacion instance = new BancoRioSeguimientoLiquidacion();
        instance.setIdFactura(idFactura);
        instance.setIdLiquidacion(null);
        instance.setEstado(FacturaEstadoSeguimientoLiquidacionEnum.PENDIENTE.codigo());
        instance.setFechaSolicitud(null);
        instance.setFechaRespuesta(null);
        return jpaUtils.create(instance);
    }

    public Result<Collection<BancoRioSeguimientoLiquidacion>> obtenerSolicitudesLiquidacionPendientes(Date fecha) {
        Result<Collection<BancoRioSeguimientoLiquidacion>> result;

        try {
            QueryCriteria criteria = QueryCriteria.instance();
            criteria.where("estado", Operators.EQUALS, FacturaEstadoSeguimientoLiquidacionEnum.PENDIENTE.codigo());
            criteria.where("fechaSolicitud", Operators.GREATER_EQUAL, fecha);
            Collection<BancoRioSeguimientoLiquidacion> value = bancoRioSeguimientoLiquidacionFacade.find(criteria);
            result = Results.ok()
                    .value(value)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .build();
        }

        return result;
    }

    public Result<BancoRioSeguimientoLiquidacion> obtenerSolicitudLiquidacion(Integer id) {
        Result<BancoRioSeguimientoLiquidacion> result;

        try {
            QueryCriteria criteria = QueryCriteria.instance();
            criteria.where("id", Operators.EQUALS, id);
            Collection<BancoRioSeguimientoLiquidacion> values = bancoRioSeguimientoLiquidacionFacade.find(criteria);
            BancoRioSeguimientoLiquidacion value = Lists.first(values);

            result = Results.ok()
                    .value(value)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .build();
        }

        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Registro de Peticion Realiazada a WS">
    public void registrarPeticionAsync(BancoRioTipoPeticionEnum tipo, WsTrackInfo track) {
        Queues queues = Queues.inject();
        if (queues != null) {
            queues.post(tipo, track);
        }
    }

    public BancoRioPeticion registrarPeticion(BancoRioTipoPeticionEnum tipo, WsTrackInfo track) throws Throwable {
        if (track.getCause() != null) {
            log.error()
                    .tag("peticion.referencia", track.getReferencia())
                    .tag("peticion.tipo", tipo.codigo())
                    .cause(track.getCause())
                    .log();
        }

        BancoRioPeticion peticion = new BancoRioPeticion();
        peticion.setReferencia(track.getReferencia());
        peticion.setTipo(tipo.codigo());
        peticion.setEstado(resolveEstadoPeticion(track));
        peticion.setRequestInicio(track.getRequestInicio());
        peticion.setRequestFin(track.getRequestFin());
        peticion.setRequest(track.getRequest());
        peticion.setResponse(track.getResponse());
        peticion = jpaUtils.create(peticion);
        return peticion;
    }

    private String resolveEstadoPeticion(WsTrackInfo track) {
        if (track.isSuccess()) {
            return BancoRioEstadoPeticionEnum.OK.codigo();
        } else {
            if (track.isTimeout()) {
                return BancoRioEstadoPeticionEnum.KO_TIMEOUT.codigo();
            } else {
                return BancoRioEstadoPeticionEnum.KO.codigo();
            }
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Registro de Comprador - Persona Juridica">
    /**
     * @deprecated El banco hara el registro de compradores de forma interna.
     */
    @Deprecated
    public Result<RegistroCompradorPersonaJuridicaOutput> registrarComprador(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, RegistroCompradorPersonaJuridicaInput input) {

        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            Result<RegistroCompradorPersonaJuridicaOutput> resOutput = ws.registrarComprador(input);
            RegistroCompradorPersonaJuridicaOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.REGISTRO_COMPRADOR_PERSONA_JURIDICA, output.getTrack());

            // Control.
            if (!output.getRespuesta().isOk()) {
                String err = resolveErrorMessage(output.getTrack(), output.getRespuesta());
                return Results.ko()
                        .cause(Errors.illegalState(err))
                        .message(err)
                        .tag("peticion.referencia", output.getTrack().getReferencia())
                        .build();
            }

            // Registrar datos de persona.
            eliminarPersona(idPersona, idPerfil);
            registrarPersona(idPersona, idPerfil, input, output);

            // Registrar adhesion a entidad.
            registrarAdhesion(idPersona, idPerfil, idEntidadFinanciera, input, output);

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error registrando comprador")
                    .build();
        }
    }

    private void eliminarPersona(Integer idPersona, Integer idPerfil) throws Throwable {
        String query = "delete from factoring.banco_rio_persona where id_persona = %s and id_perfil = %s";
        query = Strings.format(query, idPersona, idPerfil);
        jpaUtils.executeUpdate(query);
    }

    private BancoRioPersona registrarPersona(Integer idPersona, Integer idPerfil, RegistroCompradorPersonaJuridicaInput input, RegistroCompradorPersonaJuridicaOutput output) throws Throwable {
        BancoRioPersona instance = new BancoRioPersona();
        instance.setIdPersona(idPersona);
        instance.setIdPerfil(idPerfil);
        instance.setCodigoCliente(output.getRespuesta().getCodigoCliente());
        instance.setIdTipoDocumento(input.getDocumento().getTipoDocumento().getId());
        instance.setImgDocumento(input.getDocumento().getImagen());
        instance.setIdCiudad(input.getDireccion().getCiudad().getId());
        instance.setIdRegion(input.getDireccion().getRegion().getId());
        instance.setIdActividadEconomica(input.getActividadEconomica().getId());
        instance.setIdTipoSociedad(input.getSociedad().getTipoSociedad().getId());
        instance.setFechaConstitucion(input.getSociedad().getFechaConstitucion());
        instance.setFacturacionPromedio(input.getFacturacionPromedio());
        instance.setFacturacionPeriodo(6);
        instance.setIdPersonaReferencia(null);
        return jpaUtils.create(instance);
    }

    private PersonaEntidad registrarAdhesion(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, RegistroCompradorPersonaJuridicaInput input, RegistroCompradorPersonaJuridicaOutput output) throws Throwable {
        // Obtener o crear persona entidad.
        Result<PersonaEntidad> resPersonaEntidad = entidades.obtenerOCrearAdhesionConEntidad(idPersona, idPerfil, idEntidadFinanciera);
        resPersonaEntidad.raise();
        PersonaEntidad personaEntidad = resPersonaEntidad.value();

        // Actualizar estado.
        Entidades.EstadoAdhesionEntidad estado = output.getRespuesta().isOk() ? Entidades.EstadoAdhesionEntidad.ACTIVO : Entidades.EstadoAdhesionEntidad.RECHAZADO;
        entidades.actualizarEstadoAdhesionEntidad(personaEntidad.getId(), estado);

        // Registrar historico.
        Result<PersonaEntidadSolicitud> resPersonaEntidadSolicitud = entidades.crearSolicitudAdhesionEntidad(personaEntidad.getId(), output.getTrack().getRequestInicio(), output.getTrack().getRequestFin(), output.getRespuesta().getMensaje());
        resPersonaEntidadSolicitud.raise();

        return personaEntidad;
    }

    public Result<BancoRioPersona> registrarComprador(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, String codCliente, Integer idTipoDocumento) {
        try {
            eliminarPersona(idPersona, idPerfil);
            BancoRioPersona persona = registrarPersona(idPersona, idPerfil, codCliente, idTipoDocumento);
            registrarAdhesion(idPersona, idPerfil, idEntidadFinanciera);

            return Results.ok()
                    .value(persona)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error registrando comprador")
                    .build();
        }
    }

    private BancoRioPersona registrarPersona(Integer idPersona, Integer idPerfil, String codCliente, Integer idTipoDocumento) throws Throwable {
        BancoRioPersona instance = new BancoRioPersona();
        instance.setIdPersona(idPersona);
        instance.setIdPerfil(idPerfil);
        instance.setCodigoCliente(codCliente);
        instance.setIdTipoDocumento(idTipoDocumento);
        instance.setImgDocumento(null);
        instance.setIdCiudad(null);
        instance.setIdRegion(null);
        instance.setIdActividadEconomica(null);
        instance.setIdTipoSociedad(null);
        instance.setFechaConstitucion(null);
        instance.setFacturacionPromedio(null);
        instance.setFacturacionPeriodo(null);
        instance.setIdPersonaReferencia(null);
        return jpaUtils.create(instance);
    }

    private PersonaEntidad registrarAdhesion(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera) throws Throwable {
        // Obtener o crear persona entidad.
        Result<PersonaEntidad> resPersonaEntidad = entidades.obtenerOCrearAdhesionConEntidad(idPersona, idPerfil, idEntidadFinanciera);
        resPersonaEntidad.raise();
        PersonaEntidad personaEntidad = resPersonaEntidad.value();

        // Actualizar estado.
        Entidades.EstadoAdhesionEntidad estado = Entidades.EstadoAdhesionEntidad.ACTIVO;
        entidades.actualizarEstadoAdhesionEntidad(personaEntidad.getId(), estado);

        // Registrar historico.
        Date hoy = Fechas.now();
        Result<PersonaEntidadSolicitud> resPersonaEntidadSolicitud = entidades.crearSolicitudAdhesionEntidad(personaEntidad.getId(), hoy, hoy, "Adhesión automática exitosa");
        resPersonaEntidadSolicitud.raise();

        return personaEntidad;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Registro de Proveedor - Persona Juridica">
    public Result<RegistroProveedorPersonaJuridicaOutput> registrarProveedor(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, RegistroProveedorPersonaJuridicaInput input) {

        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            Result<RegistroProveedorPersonaJuridicaOutput> resOutput = ws.registrarProveedor(input);
            RegistroProveedorPersonaJuridicaOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.REGISTRO_PROVEEDOR_PERSONA_JURIDICA, output.getTrack());

            // Control.
            if (!output.getRespuesta().isOk()) {
                String err = resolveErrorMessage(output.getTrack(), output.getRespuesta());
                return Results.ko()
                        .cause(Errors.illegalState(err))
                        .message(err)
                        .tag("peticion.referencia", output.getTrack().getReferencia())
                        .build();
            }

            // Registrar datos de persona.
            eliminarPersona(idPersona, idPerfil);
            registrarPersona(idPersona, idPerfil, input, output);

            // Registrar adhesion a entidad.
            registrarAdhesion(idPersona, idPerfil, idEntidadFinanciera, input, output);

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error registrando proveedor")
                    .build();
        }
    }

    private void registrarPersona(Integer idPersona, Integer idPerfil, RegistroProveedorPersonaJuridicaInput input, RegistroProveedorPersonaJuridicaOutput output) throws Throwable {
        BancoRioPersona instance = new BancoRioPersona();
        instance.setIdPersona(idPersona);
        instance.setIdPerfil(idPerfil);
        instance.setCodigoCliente(output.getRespuesta().getCodigoCliente());
        instance.setIdTipoDocumento(input.getDocumento().getTipoDocumento().getId());
        instance.setImgDocumento(input.getDocumento().getImagen());
        instance.setIdCiudad(input.getDireccion().getCiudad().getId());
        instance.setIdRegion(input.getDireccion().getRegion().getId());
        instance.setIdActividadEconomica(input.getActividadEconomica().getId());
        instance.setIdTipoSociedad(input.getSociedad().getTipoSociedad().getId());
        instance.setFechaConstitucion(input.getSociedad().getFechaConstitucion());
        instance.setFacturacionPromedio(input.getFacturacionPromedio());
        instance.setFacturacionPeriodo(6);
        instance.setIdBanco(input.getCuentaAcreditacion().getBanco().getId());
        instance.setNroCuenta(input.getCuentaAcreditacion().getNroCuenta());
        instance.setIdPersonaReferencia(input.getComprador().getPersona().getId());
        jpaUtils.create(instance);
    }

    private PersonaEntidad registrarAdhesion(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, RegistroProveedorPersonaJuridicaInput input, RegistroProveedorPersonaJuridicaOutput output) throws Throwable {
        // Obtener o crear persona entidad.
        Result<PersonaEntidad> resPersonaEntidad = entidades.obtenerOCrearAdhesionConEntidad(idPersona, idPerfil, idEntidadFinanciera);
        resPersonaEntidad.raise();
        PersonaEntidad personaEntidad = resPersonaEntidad.value();

        // Actualizar estado.
        Entidades.EstadoAdhesionEntidad estado = resolveEstadoAdhesion(output.getRespuesta());
        entidades.actualizarEstadoAdhesionEntidad(personaEntidad.getId(), estado);

        // Registrar historico.
        Result<PersonaEntidadSolicitud> resPersonaEntidadSolicitud = entidades.crearSolicitudAdhesionEntidad(personaEntidad.getId(), output.getTrack().getRequestInicio(), output.getTrack().getRequestFin(), output.getRespuesta().getMensaje());
        resPersonaEntidadSolicitud.raise();

        return personaEntidad;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Registro de Proveedor - Persona Fisica">
    public Result<RegistroProveedorPersonaFisicaOutput> registrarProveedor(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, RegistroProveedorPersonaFisicaInput input) {

        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            Result<RegistroProveedorPersonaFisicaOutput> resOutput = ws.registrarProveedor(input);
            RegistroProveedorPersonaFisicaOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.REGISTRO_PROVEEDOR_PERSONA_FISICA, output.getTrack());

            // Control.
            if (!output.getRespuesta().isOk()) {
                String err = resolveErrorMessage(output.getTrack(), output.getRespuesta());
                return Results.ko()
                        .cause(Errors.illegalState(err))
                        .message(err)
                        .tag("peticion.referencia", output.getTrack().getReferencia())
                        .build();
            }

            // Registrar datos de persona.
            eliminarPersona(idPersona, idPerfil);
            registrarPersona(idPersona, idPerfil, input, output);

            // Registrar adhesion a entidad.
            registrarAdhesion(idPersona, idPerfil, idEntidadFinanciera, input, output);

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error registrando proveedor")
                    .build();
        }
    }

    private void registrarPersona(Integer idPersona, Integer idPerfil, RegistroProveedorPersonaFisicaInput input, RegistroProveedorPersonaFisicaOutput output) throws Throwable {
        BancoRioPersona instance = new BancoRioPersona();
        instance.setIdPersona(idPersona);
        instance.setIdPerfil(idPerfil);
        instance.setCodigoCliente(output.getRespuesta().getCodigoCliente());
        instance.setIdTipoDocumento(input.getDocumento().getTipoDocumento().getId());
        instance.setImgDocumento(input.getDocumento().getImagen());
        instance.setIdCiudad(input.getDireccion().getCiudad().getId());
        instance.setIdRegion(input.getDireccion().getRegion().getId());
        instance.setIdActividadEconomica(input.getActividadEconomica().getId());
        instance.setIdTipoSociedad(null);
        instance.setFechaConstitucion(null);
        instance.setFacturacionPromedio(input.getFacturacionPromedio());
        instance.setFacturacionPeriodo(6);
        instance.setIdBanco(input.getCuentaAcreditacion().getBanco().getId());
        instance.setNroCuenta(input.getCuentaAcreditacion().getNroCuenta());
        instance.setIdPersonaReferencia(input.getComprador().getPersona().getId());
        jpaUtils.create(instance);
    }

    private PersonaEntidad registrarAdhesion(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, RegistroProveedorPersonaFisicaInput input, RegistroProveedorPersonaFisicaOutput output) throws Throwable {
        // Obtener o crear persona entidad.
        Result<PersonaEntidad> resPersonaEntidad = entidades.obtenerOCrearAdhesionConEntidad(idPersona, idPerfil, idEntidadFinanciera);
        resPersonaEntidad.raise();
        PersonaEntidad personaEntidad = resPersonaEntidad.value();

        // Actualizar estado.
        Entidades.EstadoAdhesionEntidad estado = resolveEstadoAdhesion(output.getRespuesta());
        entidades.actualizarEstadoAdhesionEntidad(personaEntidad.getId(), estado);

        // Registrar historico.
        Result<PersonaEntidadSolicitud> resPersonaEntidadSolicitud = entidades.crearSolicitudAdhesionEntidad(personaEntidad.getId(), output.getTrack().getRequestInicio(), output.getTrack().getRequestFin(), output.getRespuesta().getMensaje());
        resPersonaEntidadSolicitud.raise();

        return personaEntidad;
    }

    private Entidades.EstadoAdhesionEntidad resolveEstadoAdhesion(RespuestaRegistroProveedor respuesta) {
        if (respuesta.isOk()) {
            if (respuesta.getCodigo().equals("-3")) {
                return Entidades.EstadoAdhesionEntidad.PENDIENTE;
            } else if (respuesta.getCodigo().equals("0")) {
                return Entidades.EstadoAdhesionEntidad.ACTIVO;
            }
        }
        return Entidades.EstadoAdhesionEntidad.RECHAZADO;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de Estado de Proveedor">
    public Result<ConsultaEstadoProveedorOutput> consultarEstadoProveedor(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, ConsultaEstadoProveedorInput input) {

        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            Result<ConsultaEstadoProveedorOutput> resOutput = ws.consultarEstadoProveedor(input);
            ConsultaEstadoProveedorOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.CONSULTA_ESTADO_DE_PROVEEDOR, output.getTrack());

            // Control.
            if (!output.getRespuesta().isOk()) {
                String err = resolveErrorMessage(output.getTrack(), output.getRespuesta());
                return Results.ko()
                        .cause(Errors.illegalState(err))
                        .message(err)
                        .tag("peticion.referencia", output.getTrack().getReferencia())
                        .build();
            }

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error consultando estado de proveedor")
                    .build();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de Compradores">
    public Result<ConsultaCompradoresOutput> consultarCompradores() {
        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            ConsultaCompradoresInput input = new ConsultaCompradoresInput();
            Result<ConsultaCompradoresOutput> resOutput = ws.consultarCompradores(input);
            ConsultaCompradoresOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.CONSULTA_COMPRADORES, output.getTrack());

            // Control.
            if (!output.getRespuesta().isOk()) {
                String err = resolveErrorMessage(output.getTrack(), output.getRespuesta());
                return Results.ko()
                        .cause(Errors.illegalState(err))
                        .message(err)
                        .tag("peticion.referencia", output.getTrack().getReferencia())
                        .build();
            }

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error consultando compradores")
                    .build();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de Estado de Cuenta">
    public Result<ConsultaEstadoCuentaOutput> consultarEstadoCuenta(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, ConsultaEstadoCuentaInput input) {

        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            Result<ConsultaEstadoCuentaOutput> resOutput = ws.consultarEstadoCuenta(input);
            ConsultaEstadoCuentaOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.CONSULTA_ESTADO_DE_CUENTA, output.getTrack());

            // Control.
            if (!output.getRespuesta().isOk()) {
                String err = resolveErrorMessage(output.getTrack(), output.getRespuesta());
                return Results.ko()
                        .cause(Errors.illegalState(err))
                        .message(err)
                        .tag("peticion.referencia", output.getTrack().getReferencia())
                        .build();
            }

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error consultando estado de cuenta")
                    .build();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de Neto de Facturas">
    public Result<CalculoNetoFacturasOutput> calculoNetoFacturas(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, CalculoNetoFacturasInput input) {

        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            Result<CalculoNetoFacturasOutput> resOutput = ws.calculoNetoFacturas(input);
            CalculoNetoFacturasOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.CALCULO_NETO_FACTURAS, output.getTrack());

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error consultando monto neto de facturas")
                    .build();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Solicitud de Desmbolso de Facturas">
    public Result<SolicitudDesembolsoOutput> solicitudDesembolso(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, SolicitudDesembolsoInput input) {

        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            Result<SolicitudDesembolsoOutput> resOutput = ws.solicitudDesembolso(input);
            SolicitudDesembolsoOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.SOLICITUD_DESEMBOLSO_FACTURAS, output.getTrack());

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error solicitando desembolso de facturas")
                    .build();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de Estado de Desembolso">
    public Result<ConsultaEstadoDesembolsoOutput> consultarEstadoDesembolso(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, ConsultaEstadoDesembolsoInput input) {

        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            Result<ConsultaEstadoDesembolsoOutput> resOutput = ws.consultarEstadoDesembolso(input);
            ConsultaEstadoDesembolsoOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.CONSULTA_ESTADO_DESEMBOLSO, output.getTrack());

            // Control.
            if (!output.getRespuesta().isOk()) {
                String err = resolveErrorMessage(output.getTrack(), output.getRespuesta());
                return Results.ko()
                        .cause(Errors.illegalState(err))
                        .message(err)
                        .tag("peticion.referencia", output.getTrack().getReferencia())
                        .build();
            }

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error consultando estado de proveedor")
                    .build();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Gestion de Personas">
    public Result<Void> actualizarCodCliente(Integer idPersona, Integer idPerfil, String codCliente) {
        try {
            // Construir query.
            String sql = " update factoring.banco_rio_persona set codigo_cliente = '%s' where id_persona = %s and id_perfil = %s ";
            sql = Strings.format(sql, codCliente, idPersona, idPerfil);

            // Ejecutar query.
            int c = jpaUtils.executeUpdate(sql);
            if (c != 1) {
                throw Errors.illegalState("No se pudo actualizar el código de cliente.");
            }

            return Results.ok()
                    .nullable(true)
                    .value(null)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message(thr.getMessage())
                    .build();
        }
    }

    public Result<Void> actualizarCodigoCliente(Integer idPersona, Integer idPerfil, String codigoCliente) {
        String sql = " update factoring.banco_rio_persona set codigo_cliente = %s where id_persona = %s and id_perfil = %s ";
        sql = Strings.format(sql, codigoCliente, idPersona, idPerfil);
        return runUpdateQuery(sql);
    }

    public Result<Void> actualizarTipoDocumento(Integer idPersona, Integer idPerfil, Integer idTipoDocumento) {
        String sql = " update factoring.banco_rio_persona set id_tipo_documento = %s where id_persona = %s and id_perfil = %s ";
        sql = Strings.format(sql, idTipoDocumento, idPersona, idPerfil);
        return runUpdateQuery(sql);
    }

    public Result<Void> actualizarNroCuenta(Integer idPersona, Integer idPerfil, Integer idBanco, String nroCuenta) {
        String sql = " update factoring.banco_rio_persona set id_banco = %s, nro_cuenta = '%s' where id_persona = %s and id_perfil = %s ";
        sql = Strings.format(sql, idBanco, nroCuenta, idPersona, idPerfil);
        return runUpdateQuery(sql);
    }

    private Result<Void> runUpdateQuery(String query) {
        try {
            // Ejecutar query.
            int c = jpaUtils.executeUpdate(query);
            if (c != 1) {
                throw Errors.illegalState("No se pudo actualizar el número de cuenta.");
            }

            return Results.ok()
                    .nullable(true)
                    .value(null)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message(thr.getMessage())
                    .build();
        }
    }

    public Result<BancoRioPersona> obtenerPersona(Integer idPersona, Integer idPerfil) {
        try {
            Query q = em.createNamedQuery("BancoRioPersona.obtenerPorIds");
            q.setParameter("idPersona", idPersona);
            q.setParameter("idPerfil", idPerfil);
            BancoRioPersona instance = (BancoRioPersona) q.getSingleResult();

            return Results.ok()
                    .value(instance)
                    .build();
        } catch (NoResultException nre) {
            return Results.ok()
                    .value(null)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo datos de persona en entidad")
                    .tag("persona.id", idPersona)
                    .tag("persona.perfil.id", idPerfil)
                    .build();
        }
    }

    /**
     * Obtiene la primera persona con perfil comprador que este adherido al
     * banco Rio y que tenga relacion comercial con el proveedor indicado.
     *
     * @param idProveedor Identificador de proveedor.
     * @return Resultado de la operacion.
     */
    public Result<Persona> obtenerCompradorAdherido(Integer idProveedor) {
        try {
            // Obtener perfil comprador.
            Perfil perfilComprador = perfiles.obtenerPerfil(PerfilEnum.COMPRADOR);

            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("   bp.id_persona as \"id_persona\" ");
            builder.append(" from factoring.banco_rio_persona as bp ");
            builder.append(" left join (%s) as c on c.id_persona = bp.id_persona ", personas.queryIdCompradoresDisponibles(idProveedor));
            builder.append(" where ");
            builder.append("   1 = 1 ");
            builder.append("   and bp.id_perfil = %s ", perfilComprador.getId());
            builder.append(" limit 1 ");

            // Ejecutar query.
            String sql = builder.toString();
            Map<String, Object> row = jpaUtils.getFirst(sql);

            // Extraer datos.
            Integer idPersona = Maps.integer(row, "id_persona");
            return personas.obtenerPorId(idPersona);
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo comprador adherido para proveedor")
                    .tag("proveedor.id", idProveedor)
                    .build();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de Facturas Liquidables">
    public Result<ConsultaFacturasLiquidablesOutput> consultarFacturasLiquidables(ConsultaFacturasLiquidablesInput input) {

        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            Result<ConsultaFacturasLiquidablesOutput> resOutput = ws.consultarFacturasLiquidables(input);
            ConsultaFacturasLiquidablesOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.CONSULTA_FACTURAS_LIQUIDABLES, output.getTrack());

            // Control.
            if (!output.getRespuesta().isOk()) {
                String err = resolveErrorMessage(output.getTrack(), output.getRespuesta());
                return Results.ko()
                        .cause(Errors.illegalState(err))
                        .message(err)
                        .tag("peticion.referencia", output.getTrack().getReferencia())
                        .build();
            }

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error solicitando liquidación de facturas")
                    .build();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Solicitud de Liquidación de Facturas">
    public Result<SolicitudLiquidacionOutput> solicitarLiquidacion(SolicitudLiquidacionInput input) {

        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            Result<SolicitudLiquidacionOutput> resOutput = ws.solicitudLiquidacion(input);
            SolicitudLiquidacionOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.SOLICITUD_LIQUIDACION_FACTURAS, output.getTrack());

            // Control.
            if (!output.getRespuesta().isOk()) {
                String err = resolveErrorMessage(output.getTrack(), output.getRespuesta());
                return Results.ko()
                        .cause(Errors.illegalState(err))
                        .message(err)
                        .tag("peticion.referencia", output.getTrack().getReferencia())
                        .build();
            }

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error solicitando liquidación de facturas")
                    .build();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de Estado de Liquidacion">
    public Result<ConsultaEstadoLiquidacionOutput> consultarEstadoLiquidacion(Integer idPersona, Integer idPerfil, Integer idEntidadFinanciera, ConsultaEstadoLiquidacionInput input) {

        try {
            // Consumir servicio.
            BancoRioWS ws = obtenerClienteWs();
            Result<ConsultaEstadoLiquidacionOutput> resOutput = ws.consultarEstadoLiquidacion(input);
            ConsultaEstadoLiquidacionOutput output = resOutput.value();

            // Registrar datos de seguimiento.
            registrarPeticionAsync(BancoRioTipoPeticionEnum.CONSULTA_ESTADO_LIQUIDACION, output.getTrack());

            // Control.
            if (!output.getRespuesta().isOk()) {
                String err = resolveErrorMessage(output.getTrack(), output.getRespuesta());
                return Results.ko()
                        .cause(Errors.illegalState(err))
                        .message(err)
                        .tag("peticion.referencia", output.getTrack().getReferencia())
                        .build();
            }

            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error consultando estado de proveedor")
                    .build();
        }
    }
    //</editor-fold>

    public String resolveErrorMessage(WsTrackInfo track, Respuesta respuesta) {
        if (track.isSuccess()) {
            if (!respuesta.isOk()) {
                return Strings.format("Respuesta de Entidad: (%s) %s.", respuesta.getCodigo(), respuesta.getMensaje());
            } else {
                // No es un error.
                throw Errors.illegalState();
            }
        } else {
            if (track.isTimeout()) {
                return "Respuesta de Entidad: Tiempo de Espera superado.";
            } else {
                if (respuesta != null && !respuesta.isOk()) {
                    return Strings.format("Respuesta de Entidad: (%s) %s.", respuesta.getCodigo(), respuesta.getMensaje());
                } else if (track.getCause() != null) {
                    return Strings.format("Respuesta de Entidad: %s", track.getCause().getMessage());
                } else {
                    return "Respuesta de Entidad: ERROR";
                }
            }
        }
    }

    public FacturaALiquidarInfo obtenerDatosLiquidacion(Integer idFactura) {
        try {
            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("   f.nro_factura, ");
            builder.append("   f.fecha_pago, ");
            builder.append("   fc.monto_neto, ");
            builder.append("   brsd.id_solicitud, ");
            builder.append("   brsd.nro_operacion ");
            builder.append(" from factoring.factura as f ");
            builder.append(" left join factoring.factura_conformada as fc on fc.id = f.id ");
            builder.append(" left join factoring.banco_rio_seguimiento_desembolso as brsd on brsd.id_lote = f.id_lote ");
            builder.append(" where ");
            builder.append("   1 = 1 ");
            builder.append("   and f.id = %s ", idFactura);
            builder.append("   and brsd.estado = '%s' ", BancoRioEstadoSeguimientoSolicitudDesembolsoEnum.RESUELTA.codigo());

            // Ejecutar query.
            String query = builder.toString();
            Map<String, Object> row = jpaUtils.getFirst(query);

            // Control.
            if (Assertions.isNullOrEmpty(row)) {
                throw Errors.illegalState("No existen datos de liquidación");
            }

            FacturaALiquidarInfo instance = new FacturaALiquidarInfo();
            instance.setNroFactura(Maps.string(row, "nro_factura"));
            instance.setFechaVencimiento(Maps.date(row, "fecha_pago"));
            instance.setMonto(Maps.bigdecimal(row, "monto_neto"));
            instance.setIdSolicitud(Maps.string(row, "id_solicitud"));
            instance.setNroOperacion(Maps.string(row, "nro_operacion"));
            return instance;
        } catch (Throwable thr) {
            return null;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Bancos">
    public Result<Collection<BancoRioBanco>> obtenerBancos() {
        Result<Collection<BancoRioBanco>> result;

        try {
            // Ejecutar query.
            String sql = " select * from factoring.banco_rio_banco ";
            Collection<Map<String, Object>> rows = jpaUtils.getAll(sql);
            Collection<BancoRioBanco> items = Lists.stream(rows)
                    .map(this::map2Banco)
                    .sorted((a, b) -> a.getDescripcion().compareTo(b.getDescripcion()))
                    .collect(Collectors.toList());

            result = Results.ok()
                    .value(items)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo bancos")
                    .build();
        }

        return result;
    }

    private BancoRioBanco map2Banco(Map<String, Object> row) {
        // Control.
        if (row == null || row.isEmpty()) {
            return null;
        }

        BancoRioBanco instance = new BancoRioBanco();
        instance.setId(Maps.integer(row, "id"));
        instance.setCodigo(Maps.string(row, "codigo", ""));
        instance.setDescripcion(Maps.string(row, "descripcion", ""));
        return instance;
    }

    public Result<BancoRioBanco> obtenerBanco(Integer idBanco) {
        Result<BancoRioBanco> result;

        try {
            // Obtener tipo documento.
            String sql = Strings.format(" select * from factoring.banco_rio_banco where id = %s ", idBanco);
            Map<String, Object> row = jpaUtils.getFirst(sql);
            BancoRioBanco item = map2Banco(row);
            if (item == null) {
                throw Errors.illegalArgument("No existe el banco");
            }

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo banco")
                    .build();
        }

        return result;
    }

    public Result<BancoRioBanco> obtenerBancoPorCodigo(String codBanco) {
        Result<BancoRioBanco> result;

        try {
            // Obtener tipo documento.
            String sql = Strings.format(" select * from factoring.banco_rio_banco where codigo = '%s' ", codBanco);
            Map<String, Object> row = jpaUtils.getFirst(sql);
            BancoRioBanco item = map2Banco(row);
            if (item == null) {
                throw Errors.illegalArgument("No existe el banco");
            }

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo banco")
                    .build();
        }

        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Tipos de Documento">
    public Result<Collection<BancoRioTipoDocumento>> obtenerTiposDocumento() {
        Result<Collection<BancoRioTipoDocumento>> result;

        try {
            // Ejecutar query.
            String sql = " select * from factoring.banco_rio_tipo_documento ";
            Collection<Map<String, Object>> rows = jpaUtils.getAll(sql);
            Collection<BancoRioTipoDocumento> items = Lists.stream(rows)
                    .map(this::map2TipoDocumento)
                    .sorted((a, b) -> a.getDescripcion().compareTo(b.getDescripcion()))
                    .collect(Collectors.toList());

            result = Results.ok()
                    .value(items)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo tipos de documentos")
                    .build();
        }

        return result;
    }

    private BancoRioTipoDocumento map2TipoDocumento(Map<String, Object> row) {
        // Control.
        if (row == null || row.isEmpty()) {
            return null;
        }

        BancoRioTipoDocumento instance = new BancoRioTipoDocumento();
        instance.setId(Maps.integer(row, "id"));
        instance.setCodigo(Maps.string(row, "codigo", ""));
        instance.setDescripcion(Maps.string(row, "descripcion", ""));
        instance.setSigla(Maps.string(row, "sigla", ""));
        return instance;
    }

    public Result<BancoRioTipoDocumento> obtenerTipoDocumento(Integer idTipoDocumento) {
        Result<BancoRioTipoDocumento> result;

        try {
            // Obtener tipo documento.
            String sql = Strings.format(" select * from factoring.banco_rio_tipo_documento where id = %s ", idTipoDocumento);
            Map<String, Object> row = jpaUtils.getFirst(sql);
            BancoRioTipoDocumento item = map2TipoDocumento(row);
            if (item == null) {
                throw Errors.illegalArgument("No existe el tipo de documento RUC");
            }

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo tipo de documento RUC")
                    .build();
        }

        return result;
    }

    public Result<BancoRioTipoDocumento> obtenerTipoDocumentoPorSigla(String sigla) {
        Result<BancoRioTipoDocumento> result;

        try {
            // Obtener tipo documento.
            BancoRioTipoDocumento item = obtenerTipoDocumento(sigla);
            if (item == null) {
                throw Errors.illegalArgument("No existe el tipo de documento");
            }

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo tipo de documento")
                    .build();
        }

        return result;
    }

    private Result<BancoRioTipoDocumento> obtenerTipoDocumentoRuc() {
        return obtenerTipoDocumentoPorSigla("RUC");
    }

    private Result<BancoRioTipoDocumento> obtenerTipoDocumentoCi() {
        return obtenerTipoDocumentoPorSigla("CI");
    }

    private BancoRioTipoDocumento obtenerTipoDocumento(String sigla) throws Throwable {
        String sql = Strings.format(" select * from factoring.banco_rio_tipo_documento where sigla = '%s' ", sigla);
        Map<String, Object> row = jpaUtils.getFirst(sql);
        return map2TipoDocumento(row);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Actividades Economicas">
    public Result<Collection<BancoRioActividadEconomica>> obtenerActividadesEconomicas() {
        Result<Collection<BancoRioActividadEconomica>> result;

        try {
            // Ejecutar query.
            String sql = " select * from factoring.banco_rio_actividad_economica ";
            Collection<Map<String, Object>> rows = jpaUtils.getAll(sql);
            Collection<BancoRioActividadEconomica> items = Lists.stream(rows)
                    .map(this::map2ActividadEconomica)
                    .sorted((a, b) -> a.getDescripcion().compareTo(b.getDescripcion()))
                    .collect(Collectors.toList());

            result = Results.ok()
                    .value(items)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo actividades económicas")
                    .build();
        }

        return result;
    }

    public Result<BancoRioActividadEconomica> obtenerActividadEconomicaPorCodigo(String codigo) {
        Result<BancoRioActividadEconomica> result;

        try {
            // Ejecutar query.
            String sql = Strings.format(" select * from factoring.banco_rio_actividad_economica where codigo = '%s' ", codigo);
            Collection<Map<String, Object>> rows = jpaUtils.getAll(sql);
            Map<String, Object> row = Lists.first(rows);

            BancoRioActividadEconomica item = map2ActividadEconomica(row);
            if (item == null) {
                throw Errors.illegalArgument("No existe la actividad económica");
            }

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo actividad económica")
                    .build();
        }

        return result;
    }

    private BancoRioActividadEconomica map2ActividadEconomica(Map<String, Object> row) {
        // Control.
        if (row == null || row.isEmpty()) {
            return null;
        }

        BancoRioActividadEconomica instance = new BancoRioActividadEconomica();
        instance.setId(Maps.integer(row, "id"));
        instance.setCodigo(Maps.string(row, "codigo", ""));
        instance.setDescripcion(Maps.string(row, "descripcion", ""));
        instance.setTipoPersona(Maps.string(row, "tipo_persona", ""));
        return instance;
    }

    public Result<BancoRioActividadEconomica> obtenerActividadEconomica(Integer idActividadEconomica) {
        Result<BancoRioActividadEconomica> result;

        try {
            // Obtener tipo documento.
            String sql = Strings.format(" select * from factoring.banco_rio_actividad_economica where id = %s ", idActividadEconomica);
            Map<String, Object> row = jpaUtils.getFirst(sql);
            BancoRioActividadEconomica item = map2ActividadEconomica(row);
            if (item == null) {
                throw Errors.illegalArgument("No existe el tipo de actividad económica");
            }

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo tipo de actividad económica")
                    .build();
        }

        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Tipos de Sociedad">
    public Result<Collection<BancoRioTipoSociedad>> obtenerTiposSociedades() {
        Result<Collection<BancoRioTipoSociedad>> result;

        try {
            // Ejecutar query.
            String sql = " select * from factoring.banco_rio_tipo_sociedad ";
            Collection<Map<String, Object>> rows = jpaUtils.getAll(sql);
            Collection<BancoRioTipoSociedad> items = Lists.stream(rows)
                    .map(this::map2TipoSociedad)
                    .sorted((a, b) -> a.getDescripcion().compareTo(b.getDescripcion()))
                    .collect(Collectors.toList());

            result = Results.ok()
                    .value(items)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo tipos de sociedad")
                    .build();
        }

        return result;
    }

    private BancoRioTipoSociedad map2TipoSociedad(Map<String, Object> row) {
        // Control.
        if (row == null || row.isEmpty()) {
            return null;
        }

        BancoRioTipoSociedad instance = new BancoRioTipoSociedad();
        instance.setId(Maps.integer(row, "id"));
        instance.setDescripcion(Maps.string(row, "descripcion", ""));
        instance.setSigla(Maps.string(row, "sigla", ""));
        return instance;
    }

    public Result<BancoRioTipoSociedad> obtenerTipoSociedad(Integer idTipoSociedad) {
        Result<BancoRioTipoSociedad> result;

        try {
            // Obtener tipo documento.
            String sql = Strings.format(" select * from factoring.banco_rio_tipo_sociedad where id = %s ", idTipoSociedad);
            Map<String, Object> row = jpaUtils.getFirst(sql);
            BancoRioTipoSociedad item = map2TipoSociedad(row);
            if (item == null) {
                throw Errors.illegalArgument("No existe el tipo de sociedad");
            }

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo tipo de sociedad")
                    .build();
        }

        return result;
    }

    public Result<BancoRioTipoSociedad> obtenerTipoSociedadPorSigla(String sigla) {
        Result<BancoRioTipoSociedad> result;

        try {
            // Obtener tipo documento.
            String sql = Strings.format(" select * from factoring.banco_rio_tipo_sociedad where sigla = '%s' ", sigla);
            Map<String, Object> row = jpaUtils.getFirst(sql);
            BancoRioTipoSociedad item = map2TipoSociedad(row);
            if (item == null) {
                throw Errors.illegalArgument("No existe el tipo de sociedad");
            }

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo tipo de sociedad")
                    .build();
        }

        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Ciudades">
    public Result<Collection<BancoRioCiudad>> obtenerCiudades() {
        Result<Collection<BancoRioCiudad>> result;

        try {
            // Ejecutar query.
            String sql = " select * from factoring.banco_rio_ciudad ";
            Collection<Map<String, Object>> rows = jpaUtils.getAll(sql);
            Collection<BancoRioCiudad> items = Lists.stream(rows)
                    .map(this::map2Ciudad)
                    .sorted((a, b) -> a.getDescripcion().compareTo(b.getDescripcion()))
                    .collect(Collectors.toList());

            result = Results.ok()
                    .value(items)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo ciudades")
                    .build();
        }

        return result;
    }

    private BancoRioCiudad map2Ciudad(Map<String, Object> row) {
        // Control.
        if (row == null || row.isEmpty()) {
            return null;
        }

        BancoRioCiudad instance = new BancoRioCiudad();
        instance.setId(Maps.integer(row, "id"));
        instance.setCodigo(Maps.string(row, "codigo", ""));
        instance.setDescripcion(Maps.string(row, "descripcion", ""));
        instance.setCodigoRegion(Maps.string(row, "codigo_region", ""));
        return instance;
    }

    public Result<BancoRioCiudad> obtenerCiudad(Integer idCiudad) {
        Result<BancoRioCiudad> result;

        try {
            // Obtener tipo documento.
            String sql = Strings.format(" select * from factoring.banco_rio_ciudad where id = %s ", idCiudad);
            Map<String, Object> row = jpaUtils.getFirst(sql);
            BancoRioCiudad item = map2Ciudad(row);
            if (item == null) {
                throw Errors.illegalArgument("No existe la ciudad");
            }

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo ciudad")
                    .build();
        }

        return result;
    }

    public Result<BancoRioCiudad> obtenerCiudadPorCodigo(String codigo) {
        Result<BancoRioCiudad> result;

        try {
            // Obtener tipo documento.
            String sql = Strings.format(" select * from factoring.banco_rio_ciudad where codigo = '%s' ", codigo);
            Map<String, Object> row = jpaUtils.getFirst(sql);
            BancoRioCiudad item = map2Ciudad(row);
            if (item == null) {
                throw Errors.illegalArgument("No existe la ciudad");
            }

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo ciudad")
                    .build();
        }

        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Regiones">
    public Result<Collection<BancoRioRegion>> obtenerRegiones() {
        Result<Collection<BancoRioRegion>> result;

        try {
            // Ejecutar query.
            String sql = " select * from factoring.banco_rio_region ";
            Collection<Map<String, Object>> rows = jpaUtils.getAll(sql);
            Collection<BancoRioRegion> items = Lists.stream(rows)
                    .map(this::map2Region)
                    .sorted((a, b) -> a.getDescripcion().compareTo(b.getDescripcion()))
                    .collect(Collectors.toList());

            result = Results.ok()
                    .value(items)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo regiones")
                    .build();
        }

        return result;
    }

    public Result<BancoRioRegion> obtenerRegionPorCodigo(String codigoRegion) {
        Result<BancoRioRegion> result;

        try {
            // Ajustar codigo de region.
            codigoRegion = StringUtils.leftPad(codigoRegion, 2, '0');

            // Ejecutar query.
            String sql = Strings.format(" select * from factoring.banco_rio_region where codigo = '%s' ", codigoRegion);
            Map<String, Object> row = jpaUtils.getFirst(sql);
            BancoRioRegion item = map2Region(row);

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo region")
                    .build();
        }

        return result;
    }

    public Result<BancoRioRegion> obtenerRegion(Integer idRegion) {
        Result<BancoRioRegion> result;

        try {
            // Ejecutar query.
            String sql = Strings.format(" select * from factoring.banco_rio_region where id = '%s' ", idRegion);
            Map<String, Object> row = jpaUtils.getFirst(sql);
            BancoRioRegion item = map2Region(row);

            result = Results.ok()
                    .value(item)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo region")
                    .build();
        }

        return result;
    }

    private BancoRioRegion map2Region(Map<String, Object> row) {
        // Control.
        if (row == null || row.isEmpty()) {
            return null;
        }

        BancoRioRegion instance = new BancoRioRegion();
        instance.setId(Maps.integer(row, "id"));
        instance.setCodigo(Maps.string(row, "codigo", ""));
        instance.setDescripcion(Maps.string(row, "descripcion", ""));
        return instance;
    }
    //</editor-fold>

}
