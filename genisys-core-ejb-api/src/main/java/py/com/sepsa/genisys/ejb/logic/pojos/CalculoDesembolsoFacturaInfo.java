/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.genisys.ejb.logic.pojos;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.Convenio;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class CalculoDesembolsoFacturaInfo implements Serializable {

    // Estado de calculo.
    private Boolean ok;
    private String message;
    // Datos de factura.
    private FacturaInfo factura;
    private Convenio convenio;
    private Integer diasAnticipo;
    // Monto y porcentaje respectivo de descuento aplicado.
    private BigDecimal porcentajeDescuento;
    private BigDecimal montoDescuento;
    // Monto a distribuir entre las partes, segun convenio. Normalmente es igual al monto de descuento.
    private BigDecimal montoDistribucion;
    // Monto final de factura luego de descuento.
    private BigDecimal montoDesembolso;
    // Datos adicionales de calculo.
    private Object extras;
}
