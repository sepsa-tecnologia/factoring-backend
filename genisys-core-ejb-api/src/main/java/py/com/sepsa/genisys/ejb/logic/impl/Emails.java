/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.info.ContactoEmail;
import py.com.sepsa.genisys.ejb.entities.info.Email;
import py.com.sepsa.genisys.ejb.utils.Constantes;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;
import py.com.sepsa.genisys.ejb.utils.Persistence;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Emails", mappedName = "Emails")
@LocalBean
public class Emails implements Serializable {

    @PersistenceContext(unitName = Persistence.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @EJB
    private JpaUtils jpaUtils;

    public Email obtenerPorId(Integer idEmail) throws Throwable {
        Query q = em.createNamedQuery("Email.findById");
        q.setParameter("id", idEmail);
        return (Email) q.getSingleResult();
    }

    public Result<Integer> registrarEmail(Integer idPersona, String email) {
        Result<Integer> result;

        try {
            // Obtener o crear contacto.
            Integer idContacto = obtenerIdContacto(idPersona);

            // Obtener o crear email.
            Integer idEmail = obtenerIdEmail(email);

            // Crear registro contacto_email si es necesario.
            registarContactoEmail(idContacto, idEmail);

            result = Results.ok()
                    .value(idEmail)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrio un error registrando email")
                    .tag("persona.id", idPersona)
                    .build();
        }

        return result;
    }

    /**
     * Obtiene el registro de contacto asociado a una persona. Si el registro no
     * existe, se crea uno nuevo.
     *
     * @param idPersona Identificador de persona.
     * @return Registro de contacto existente, caso contrario registro nuevo
     * creado.
     * @throws Throwable Si ocurre algun error.
     */
    private Integer obtenerIdContacto(Integer idPersona) throws Throwable {
        Integer idContacto = obtenerIdContacto0(idPersona);
        if (idContacto == null) {
            idContacto = registrarContacto0(idPersona);
        }
        return idContacto;
    }

    private Integer registrarContacto0(Integer idPersona) throws Throwable {
        // Crear registro de contacto.
        // NOTE: El identificador de contacto es igual al identificador de persona.
        // NOTE: El identificador de tipo de contacto se toma del valor existente en mercurio.
        // NOTE: El identificador de cargo se toma del valor existente en mercurio.
        String query = "insert into info.contacto(id_contacto, id_tipo_contacto, id_cargo, estado) values(%s, %s, %s, 'A');";
        query = Strings.format(query, idPersona, Constantes.ID_TIPO_CONTACTO_GENERICO, Constantes.ID_CARGO_GENERICO);
        jpaUtils.executeUpdate(query);
        return idPersona;
    }

    private Integer obtenerIdContacto0(Integer idPersona) throws Throwable {
        // Construir query.
        String query = "select id_contacto as \"id\" from info.contacto where id_contacto = %s and estado = 'A' limit 1;";
        query = Strings.format(query, idPersona);

        // Ejecutar consulta.
        Map<String, Object> row = jpaUtils.getFirst(query);

        // Extraer resultado.
        Object obj = Maps.get(row, "id");
        if (obj == null) {
            return null;
        } else {
            Integer idContacto = Numeric.adaptAsInteger(obj);
            return idContacto;
        }
    }

    /**
     * Obtiene el registro de email asociado a una direccion de correo
     * electronico. Si el registro no existe, se crea uno nuevo.
     *
     * @param email Direccion de correo electronico.
     * @return Registro de email existente, caso contrario registro nuevo
     * creado.
     * @throws Throwable Si ocurre algun error.
     */
    private Integer obtenerIdEmail(String email) throws Throwable {
        Integer idEmail = obtenerIdEmail0(email);
        if (idEmail == null) {
            idEmail = registrarEmail0(email);
        }
        return idEmail;
    }

    private Integer registrarEmail0(String email) throws Throwable {
        // Verificar si ya existe.
        Query q = em.createNamedQuery("Email.findByEmail");
        q.setParameter("email", email);
        Email email0 = jpaUtils.getFirst(q);

        // Retorna id si ya existe.
        if (email0 != null) {
            return email0.getId();
        }

        // Registrar email si no existe.
        // NOTE: El identificador para tipo de email es igual al valor existente en mercurio.
        email0 = new Email();
        email0.setEmail(email);
        email0.setIdTipoEmail(1);
        email0.setPrincipal("S");
        email0 = jpaUtils.create(email0);
        return email0.getId();
    }

    private Integer obtenerIdEmail0(String email) throws Throwable {
        // Construir query.
        String query = "select id as \"id\" from info.email where email = '%s' limit 1;";
        query = Strings.format(query, email);

        // Ejecutar consulta.
        Map<String, Object> row = jpaUtils.getFirst(query);

        // Extraer resultado.
        Object obj = Maps.get(row, "id");
        if (obj == null) {
            return null;
        } else {
            Integer idEmail = Numeric.adaptAsInteger(obj);
            return idEmail;
        }
    }

    private void registarContactoEmail(Integer idContacto, Integer idEmail) throws Throwable {
        if (!contactoEmailExiste(idContacto, idEmail)) {
            registarContactoEmail0(idContacto, idEmail);
        } else {
            habilitarNotificacionesFactoring(idContacto, idEmail);
        }
    }

    private boolean contactoEmailExiste(Integer idContacto, Integer idEmail) throws Throwable {
        // Construir consulta.
        String query = "select count(*) as \"total\" from info.contacto_email where id_contacto = %s and id_email = %s ;";
        query = Strings.format(query, idContacto, idEmail);

        // Ejecutar consulta.
        Map<String, Object> row = jpaUtils.getFirst(query);

        // Extraer resultado.
        Object obj = Maps.get(row, "total");
        if (obj == null) {
            return false;
        } else {
            Integer total = Numeric.adaptAsInteger(obj);
            return total > 0;
        }
    }

    private void registarContactoEmail0(Integer idContacto, Integer idEmail) throws Throwable {
        // Construir query.
        String query = "insert into info.contacto_email(id_email, id_contacto, notificacion_documento, notificacion_factura, notificacion_nota_credito, notificacion_factoring, notificacion_recibo) values(%s, %s, 'N', 'N', 'N', 'S', 'N');";
        query = Strings.format(query, idEmail, idContacto);
        jpaUtils.executeUpdate(query);
    }

    private void habilitarNotificacionesFactoring(Integer idContacto, Integer idEmail) throws Throwable {
        // Construir query.
        String query = "update info.contacto_email set notificacion_factoring = 'S' where id_email = %s and id_contacto = %s ;";
        query = Strings.format(query, idEmail, idContacto);
        jpaUtils.executeUpdate(query);
    }

    /**
     * Obtiene la informacion minima de contactos habilitados para
     * notificaciones via email.
     *
     * @param idPersona Identificador de persona.
     * @return Contactos.
     */
    public List<ContactoEmail> obtenerContactosEmailDisponibles(Integer idPersona) {
        List<ContactoEmail> emails = Lists.empty();

        try {
            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     c.id_contacto as \"id_contacto\", ");
            builder.append("     e.id as \"id_email\", ");
            builder.append("     e.email as \"email\" ");
            builder.append(" from info.contacto_email as ce ");
            builder.append(" left join info.contacto as c on ce.id_contacto = c.id_contacto ");
            builder.append(" left join info.email as e on ce.id_email = e.id ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and c.estado = 'A' ");
            builder.append("     and c.id_contacto = %s", idPersona);
            builder.append("     and ce.notificacion_factoring = 'S' ");
            builder.append(" ; ");
            String sql = builder.toString();

            // Ejecutar consulta.
            Collection<Map<String, Object>> rows = jpaUtils.getAll(sql);

            for (Map<String, Object> row : rows) {
                Integer idContacto = Maps.integer(row, "id_contacto");
                Integer idEmail = Maps.integer(row, "id_email");
                String email = Maps.string(row, "email");

                ContactoEmail instance = new ContactoEmail();
                instance.setIdContacto(idContacto);
                instance.setIdEmail(idEmail);
                instance.setEmail(email);
                emails.add(instance);
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error obteniendo contactos de persona");
        }

        return emails;
    }

}
