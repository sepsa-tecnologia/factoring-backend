/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum BancoRioTipoPeticionEnum {
    REGISTRO_PROVEEDOR_PERSONA_JURIDICA("0001"),
    REGISTRO_PROVEEDOR_PERSONA_FISICA("0002"),
    REGISTRO_COMPRADOR_PERSONA_JURIDICA("0003"),
    CONSULTA_ESTADO_DE_CUENTA("0004"),
    CALCULO_NETO_FACTURAS("0005"),
    CONSULTA_ESTADO_DE_PROVEEDOR("0006"),
    SOLICITUD_DESEMBOLSO_FACTURAS("0007"),
    CONSULTA_ESTADO_DESEMBOLSO("0008"),
    SOLICITUD_LIQUIDACION_FACTURAS("0009"),
    CONSULTA_COMPRADORES("0010"),
    CONSULTA_ESTADO_LIQUIDACION("0011"),
    CONSULTA_FACTURAS_LIQUIDABLES("0012");
    private final String codigo;

    private BancoRioTipoPeticionEnum(String codigo) {
        this.codigo = codigo;
    }

    public String codigo() {
        return codigo;
    }

}
