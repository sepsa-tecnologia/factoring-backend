/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.misc.Assertions;
import fa.gs.misc.Numeric;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.fechas.Fechas;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import java.io.ByteArrayInputStream;
import java.util.Base64;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Configuraciones;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.impl.Personas.TipoPersonaEnum;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.FacturaALiquidarInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.FacturaInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.WsTrackInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
public class BancoRioWS {

    private final String usuario;

    private final String password;

    private final String clavePublica;

    private final AppLogger log;

    private BancoRioWS(String usuario, String password, String clavePublica) {
        this.usuario = usuario;
        this.password = password;
        this.clavePublica = clavePublica;
        this.log = AppLoggerFactory.core();
    }

    public static BancoRioWS instance(String usuario, String password, String clavePublica) {
        return new BancoRioWS(usuario, password, clavePublica);
    }

    private void initRequest(WsTrackInfo track, String xml) {
        log.debug("SOAP REQUEST %s >> [%s]", track.getReferencia(), xml);
        track.setRequest(xml);
    }

    private void initResponse(WsTrackInfo track, String xml) {
        log.debug("SOAP RESPONSE %s << [%s]", track.getReferencia(), xml);
        track.setResponse(xml);
    }

    //<editor-fold defaultstate="collapsed" desc="Consulta de Estado de Cuenta">
    public Result<ConsultaEstadoCuentaOutput> consultarEstadoCuenta(ConsultaEstadoCuentaInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaConsultaEstadoCuenta respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest);

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Código de respuesta HTTP inválido (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaConsultaEstadoCuenta.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaConsultaEstadoCuenta.instance("-1", thr.getMessage());
            track.setKo(thr);
        }

        // Datos finales.
        ConsultaEstadoCuentaOutput output = new ConsultaEstadoCuentaOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(ConsultaEstadoCuentaInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\"> ");
        builder.append("    <soap:Header/> ");
        builder.append("    <soap:Body> ");
        builder.append("       <tem:ObtenerSaldoLineaCredito> ");
        builder.append("          <tem:saldoLineaRequest> ");
        builder.append("             <tem:TipoDocumento>");
        builder.append("                <tem:Codigo>%s</tem:Codigo>", input.getDocumento().getTipoDocumento().getCodigo());
        builder.append("                <tem:Descripcion>%s</tem:Descripcion>", input.getDocumento().getTipoDocumento().getDescripcion());
        builder.append("                <tem:Sigla>%s</tem:Sigla>", input.getDocumento().getTipoDocumento().getSigla());
        builder.append("                <tem:TipoPersona/>");
        builder.append("             </tem:TipoDocumento>");
        builder.append("             <tem:NroDocumento>%s</tem:NroDocumento>", input.getDocumento().getNroDocumento());
        builder.append("             <tem:MontoEstaOperacion>0</tem:MontoEstaOperacion>");
        builder.append("          </tem:saldoLineaRequest> ");
        builder.append("          <tem:Acceso> ");
        builder.append("             <tem:usuario>%s</tem:usuario> ", usuario);
        builder.append("             <tem:password>%s</tem:password> ", password);
        builder.append("             <tem:claveP>%s</tem:claveP> ", clavePublica);
        builder.append("          </tem:Acceso> ");
        builder.append("       </tem:ObtenerSaldoLineaCredito> ");
        builder.append("    </soap:Body> ");
        builder.append(" </soap:Envelope> ");
        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Registro de Proveedor - Persona Juridica">
    public Result<RegistroProveedorPersonaJuridicaOutput> registrarProveedor(RegistroProveedorPersonaJuridicaInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaRegistroProveedor respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest);

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Código de respuesta HTTP inválido (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaRegistroProveedor.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaRegistroProveedor.instance("-1", thr.getMessage(), "-1");
            track.setKo(thr);
        }

        // Datos finales.
        RegistroProveedorPersonaJuridicaOutput output = new RegistroProveedorPersonaJuridicaOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(RegistroProveedorPersonaJuridicaInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\"> ");
        builder.append("    <soap:Header/> ");
        builder.append("    <soap:Body> ");
        builder.append("        <tem:AltaProveedorJuridico> ");
        builder.append("        <tem:personaJuridica> ");
        builder.append("            <tem:Comprador> ");
        builder.append("                <tem:TipoPersona>%s</tem:TipoPersona> ", resolveTipoPersona(input.getComprador().getPersona()));
        builder.append("                <tem:TipoDocumento> ");
        builder.append("                    <tem:Codigo>%s</tem:Codigo> ", input.getComprador().getDocumento().getTipoDocumento().getCodigo());
        builder.append("                    <tem:Descripcion>%s</tem:Descripcion> ", input.getComprador().getDocumento().getTipoDocumento().getDescripcion());
        builder.append("                    <tem:Sigla>%s</tem:Sigla> ", input.getComprador().getDocumento().getTipoDocumento().getSigla());
        builder.append("                </tem:TipoDocumento> ");
        builder.append("                <tem:NroDocumento>%s</tem:NroDocumento> ", cleanupRuc(input.getComprador().getDocumento().getNroDocumento()));
        builder.append("                <tem:Tasa>0</tem:Tasa> ");
        builder.append("                <tem:CobrarSeguro>false</tem:CobrarSeguro> ");
        builder.append("                <tem:PorcentajeGastosAdm>0</tem:PorcentajeGastosAdm> ");
        builder.append("            </tem:Comprador> ");
        builder.append("            <tem:CodCliente /> ");
        builder.append("            <tem:TipoPersona>JURIDICA</tem:TipoPersona> ");
        builder.append("            <tem:TipoDocumento> ");
        builder.append("               <tem:Codigo>%s</tem:Codigo> ", input.getDocumento().getTipoDocumento().getCodigo());
        builder.append("               <tem:Descripcion>%s</tem:Descripcion> ", input.getDocumento().getTipoDocumento().getDescripcion());
        builder.append("               <tem:Sigla>%s</tem:Sigla> ", input.getDocumento().getTipoDocumento().getSigla());
        builder.append("               <tem:TipoPersona/> ");
        builder.append("            </tem:TipoDocumento> ");
        builder.append("            <tem:NroDocumento>%s</tem:NroDocumento> ", cleanupRuc(input.getDocumento().getNroDocumento()));
        builder.append("            <tem:Direccion> ");
        builder.append("               <tem:Calle1>%s</tem:Calle1> ", input.getDireccion().getCalle1());
        builder.append("               <tem:Calle2>%s</tem:Calle2> ", input.getDireccion().getCalle2());
        builder.append("               <tem:Referencia>Y</tem:Referencia> ");
        builder.append("               <tem:NroCalle>%s</tem:NroCalle> ", input.getDireccion().getNroCalle());
        builder.append("               <tem:Calle3>%s</tem:Calle3>", input.getDireccion().getCalle3());
        builder.append("               <tem:Barrio>%s</tem:Barrio> ", input.getDireccion().getBarrio());
        builder.append("               <tem:Ciudad> ");
        builder.append("                  <tem:Codigo>%s</tem:Codigo> ", input.getDireccion().getCiudad().getCodigo());
        builder.append("                  <tem:Descripcion>%s</tem:Descripcion> ", input.getDireccion().getCiudad().getDescripcion());
        builder.append("                  <tem:Sigla/> ");
        builder.append("                  <tem:TipoPersona/> ");
        builder.append("                  <tem:CodRegion>%s</tem:CodRegion> ", input.getDireccion().getCiudad().getCodigoRegion());
        builder.append("               </tem:Ciudad> ");
        builder.append("               <tem:Region> ");
        builder.append("                  <tem:Codigo>%s</tem:Codigo> ", input.getDireccion().getRegion().getCodigo());
        builder.append("                  <tem:Descripcion>%s</tem:Descripcion> ", input.getDireccion().getRegion().getDescripcion());
        builder.append("                  <tem:Sigla/> ");
        builder.append("                  <tem:TipoPersona/> ");
        builder.append("               </tem:Region> ");
        builder.append("               <tem:NroLineaBaja>%s</tem:NroLineaBaja> ", input.getContacto().getNroLineaBaja());
        builder.append("               <tem:NroCelular>%s</tem:NroCelular> ", input.getContacto().getNroCelular());
        builder.append("               <tem:Email>%s</tem:Email> ", input.getContacto().getEmail());
        builder.append("            </tem:Direccion> ");
        builder.append("            <tem:ActividadEconomica> ");
        builder.append("               <tem:Codigo>%s</tem:Codigo> ", input.getActividadEconomica().getCodigo());
        builder.append("               <tem:Descripcion>%s</tem:Descripcion> ", input.getActividadEconomica().getDescripcion());
        builder.append("               <tem:Sigla/> ");
        builder.append("               <tem:TipoPersona/> ");
        builder.append("            </tem:ActividadEconomica> ");
        builder.append("            <tem:PromedioMensual>%s</tem:PromedioMensual> ", input.getFacturacionPromedio().toString());
        builder.append("            <tem:DescripcionRubro>%s</tem:DescripcionRubro> ", input.getRubro().getDescripcion());
        builder.append("            <tem:DocumentoDeIdentidad>%s</tem:DocumentoDeIdentidad>", base64(input.getDocumento().getImagen()));
        builder.append("            <tem:InfoCuentaAcreditacion>");
        builder.append("              <tem:Banco>");
        builder.append("                <tem:Codigo>%s</tem:Codigo>", input.getCuentaAcreditacion().getBanco().getCodigo());
        builder.append("                <tem:Descripcion>%s</tem:Descripcion>", input.getCuentaAcreditacion().getBanco().getDescripcion());
        builder.append("                <tem:Sigla/>");
        builder.append("                <tem:TipoPersona/>");
        builder.append("                </tem:Banco>");
        builder.append("              <tem:NroCuenta>%s</tem:NroCuenta>", input.getCuentaAcreditacion().getNroCuenta());
        builder.append("            </tem:InfoCuentaAcreditacion>");
        builder.append("            <tem:FechaConstitucion>%sT00:00:00</tem:FechaConstitucion> ", Fechas.toString(input.getSociedad().getFechaConstitucion(), "yyyy-MM-dd"));
        builder.append("            <tem:RazonSocial>%s</tem:RazonSocial> ", input.getRazonSocial());
        builder.append("            <tem:TipoSociedad> ");
        builder.append("               <tem:Codigo/> ");
        builder.append("               <tem:Descripcion>%s</tem:Descripcion> ", input.getSociedad().getTipoSociedad().getDescripcion());
        builder.append("               <tem:Sigla>%s</tem:Sigla> ", input.getSociedad().getTipoSociedad().getSigla());
        builder.append("               <tem:TipoPersona/> ");
        builder.append("            </tem:TipoSociedad> ");
        builder.append("        </tem:personaJuridica> ");
        builder.append("          <tem:Acceso> ");
        builder.append("             <tem:usuario>%s</tem:usuario> ", usuario);
        builder.append("             <tem:password>%s</tem:password> ", password);
        builder.append("             <tem:claveP>%s</tem:claveP> ", clavePublica);
        builder.append("          </tem:Acceso> ");
        builder.append("        </tem:AltaProveedorJuridico> ");
        builder.append("    </soap:Body> ");
        builder.append(" </soap:Envelope> ");
        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Registro de Proveedor - Persona Fisica">
    public Result<RegistroProveedorPersonaFisicaOutput> registrarProveedor(RegistroProveedorPersonaFisicaInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaRegistroProveedor respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest);

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Código de respuesta HTTP inválido (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaRegistroProveedor.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaRegistroProveedor.instance("-1", thr.getMessage(), "-1");
            track.setKo(thr);
        }

        // Datos finales.
        RegistroProveedorPersonaFisicaOutput output = new RegistroProveedorPersonaFisicaOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(RegistroProveedorPersonaFisicaInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\"> ");
        builder.append("    <soap:Header/> ");
        builder.append("    <soap:Body> ");
        builder.append("        <tem:AltaProveedorFisico> ");
        builder.append("        <tem:personaFisica> ");
        builder.append("            <tem:Comprador> ");
        builder.append("                <tem:TipoPersona>%s</tem:TipoPersona> ", resolveTipoPersona(input.getComprador().getPersona()));
        builder.append("                <tem:TipoDocumento> ");
        builder.append("                    <tem:Codigo>%s</tem:Codigo> ", input.getComprador().getDocumento().getTipoDocumento().getCodigo());
        builder.append("                    <tem:Descripcion>%s</tem:Descripcion> ", input.getComprador().getDocumento().getTipoDocumento().getDescripcion());
        builder.append("                    <tem:Sigla>%s</tem:Sigla> ", input.getComprador().getDocumento().getTipoDocumento().getSigla());
        builder.append("                </tem:TipoDocumento> ");
        builder.append("                <tem:NroDocumento>%s</tem:NroDocumento> ", cleanupRuc(input.getComprador().getDocumento().getNroDocumento()));
        builder.append("                <tem:Tasa>0</tem:Tasa> ");
        builder.append("                <tem:CobrarSeguro>false</tem:CobrarSeguro> ");
        builder.append("                <tem:PorcentajeGastosAdm>0</tem:PorcentajeGastosAdm> ");
        builder.append("            </tem:Comprador> ");
        builder.append("            <tem:CodCliente/> ");
        builder.append("            <tem:TipoPersona>FISICA</tem:TipoPersona> ");
        builder.append("            <tem:TipoDocumento> ");
        builder.append("               <tem:Codigo>%s</tem:Codigo> ", input.getDocumento().getTipoDocumento().getCodigo());
        builder.append("               <tem:Descripcion>%s</tem:Descripcion> ", input.getDocumento().getTipoDocumento().getDescripcion());
        builder.append("               <tem:Sigla>%s</tem:Sigla> ", input.getDocumento().getTipoDocumento().getSigla());
        builder.append("               <tem:TipoPersona/> ");
        builder.append("            </tem:TipoDocumento> ");
        builder.append("            <tem:NroDocumento>%s</tem:NroDocumento> ", cleanupRuc(input.getDocumento().getNroDocumento()));
        builder.append("            <tem:Direccion> ");
        builder.append("               <tem:Calle1>%s</tem:Calle1> ", input.getDireccion().getCalle1());
        builder.append("               <tem:Calle2>%s</tem:Calle2> ", input.getDireccion().getCalle2());
        builder.append("               <tem:Referencia>Y</tem:Referencia> ");
        builder.append("               <tem:NroCalle>%s</tem:NroCalle> ", input.getDireccion().getNroCalle());
        builder.append("               <tem:Calle3>%s</tem:Calle3>", input.getDireccion().getCalle3());
        builder.append("               <tem:Barrio>%s</tem:Barrio> ", input.getDireccion().getBarrio());
        builder.append("               <tem:Ciudad> ");
        builder.append("                  <tem:Codigo>%s</tem:Codigo> ", input.getDireccion().getCiudad().getCodigo());
        builder.append("                  <tem:Descripcion>%s</tem:Descripcion> ", input.getDireccion().getCiudad().getDescripcion());
        builder.append("                  <tem:Sigla/> ");
        builder.append("                  <tem:TipoPersona/> ");
        builder.append("                  <tem:CodRegion>%s</tem:CodRegion> ", input.getDireccion().getCiudad().getCodigoRegion());
        builder.append("               </tem:Ciudad> ");
        builder.append("               <tem:Region> ");
        builder.append("                  <tem:Codigo>%s</tem:Codigo> ", input.getDireccion().getRegion().getCodigo());
        builder.append("                  <tem:Descripcion>%s</tem:Descripcion> ", input.getDireccion().getRegion().getDescripcion());
        builder.append("                  <tem:Sigla/> ");
        builder.append("                  <tem:TipoPersona/> ");
        builder.append("               </tem:Region> ");
        builder.append("               <tem:NroLineaBaja>%s</tem:NroLineaBaja> ", input.getContacto().getNroLineaBaja());
        builder.append("               <tem:NroCelular>%s</tem:NroCelular> ", input.getContacto().getNroCelular());
        builder.append("               <tem:Email>%s</tem:Email> ", input.getContacto().getEmail());
        builder.append("            </tem:Direccion> ");
        builder.append("            <tem:ActividadEconomica> ");
        builder.append("               <tem:Codigo>%s</tem:Codigo> ", input.getActividadEconomica().getCodigo());
        builder.append("               <tem:Descripcion>%s</tem:Descripcion> ", input.getActividadEconomica().getDescripcion());
        builder.append("               <tem:Sigla/> ");
        builder.append("               <tem:TipoPersona/> ");
        builder.append("            </tem:ActividadEconomica> ");
        builder.append("            <tem:PromedioMensual>%s</tem:PromedioMensual> ", input.getFacturacionPromedio().toString());
        builder.append("            <tem:DescripcionRubro>%s</tem:DescripcionRubro> ", input.getRubro().getDescripcion());
        builder.append("            <tem:DocumentoDeIdentidad>%s</tem:DocumentoDeIdentidad>", base64(input.getDocumento().getImagen()));
        builder.append("            <tem:InfoCuentaAcreditacion>");
        builder.append("              <tem:Banco>");
        builder.append("                <tem:Codigo>%s</tem:Codigo>", input.getCuentaAcreditacion().getBanco().getCodigo());
        builder.append("                <tem:Descripcion>%s</tem:Descripcion>", input.getCuentaAcreditacion().getBanco().getDescripcion());
        builder.append("                <tem:Sigla/>");
        builder.append("                <tem:TipoPersona/>");
        builder.append("                </tem:Banco>");
        builder.append("              <tem:NroCuenta>%s</tem:NroCuenta>", input.getCuentaAcreditacion().getNroCuenta());
        builder.append("            </tem:InfoCuentaAcreditacion>");
        builder.append("            <tem:FechaNacimiento>%s</tem:FechaNacimiento> ", Fechas.toString(input.getFechaNacimiento(), "dd/MM/yyyy"));
        builder.append("            <tem:PrimerNombre>%s</tem:PrimerNombre> ", primeraParte(input.getNombres()));
        builder.append("            <tem:SegundoNombre>%s</tem:SegundoNombre> ", segundaParte(input.getNombres()));
        builder.append("            <tem:PrimerApellido>%s</tem:PrimerApellido> ", primeraParte(input.getApellidos()));
        builder.append("            <tem:SegundoApellido>%s</tem:SegundoApellido> ", segundaParte(input.getApellidos()));
        builder.append("        </tem:personaFisica> ");
        builder.append("          <tem:Acceso> ");
        builder.append("             <tem:usuario>%s</tem:usuario> ", usuario);
        builder.append("             <tem:password>%s</tem:password> ", password);
        builder.append("             <tem:claveP>%s</tem:claveP> ", clavePublica);
        builder.append("          </tem:Acceso> ");
        builder.append("        </tem:AltaProveedorFisico> ");
        builder.append("    </soap:Body> ");
        builder.append(" </soap:Envelope> ");
        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Registro de Comprador - Persona Juridica">
    /**
     * @deprecated El banco hara el registro de compradores de forma interna.
     */
    @Deprecated
    public Result<RegistroCompradorPersonaJuridicaOutput> registrarComprador(RegistroCompradorPersonaJuridicaInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaRegistroComprador respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest);

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Código de respuesta HTTP inválido (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaRegistroComprador.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaRegistroComprador.instance("-1", thr.getMessage(), "-1");
            track.setKo(thr);
        }

        // Datos finales.
        RegistroCompradorPersonaJuridicaOutput output = new RegistroCompradorPersonaJuridicaOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(RegistroCompradorPersonaJuridicaInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\"> ");
        builder.append("    <soap:Header/> ");
        builder.append("    <soap:Body> ");
        builder.append("        <tem:AltaComprador> ");
        builder.append("        <tem:personaJuridica> ");
        builder.append("            <tem:CodCliente></tem:CodCliente> ");
        builder.append("            <tem:TipoPersona>JURIDICA</tem:TipoPersona> ");
        builder.append("            <tem:TipoDocumento> ");
        builder.append("               <tem:Codigo>%s</tem:Codigo> ", input.getDocumento().getTipoDocumento().getCodigo());
        builder.append("               <tem:Descripcion>%s</tem:Descripcion> ", input.getDocumento().getTipoDocumento().getDescripcion());
        builder.append("               <tem:Sigla>%s</tem:Sigla> ", input.getDocumento().getTipoDocumento().getSigla());
        builder.append("               <tem:TipoPersona/> ");
        builder.append("            </tem:TipoDocumento> ");
        builder.append("            <tem:NroDocumento>%s</tem:NroDocumento> ", cleanupRuc(input.getDocumento().getNroDocumento()));
        builder.append("            <tem:Direccion> ");
        builder.append("               <tem:Calle1>%s</tem:Calle1> ", input.getDireccion().getCalle1());
        builder.append("               <tem:Calle2>%s</tem:Calle2> ", input.getDireccion().getCalle2());
        builder.append("               <tem:Referencia>Y</tem:Referencia> ");
        builder.append("               <tem:NroCalle>%s</tem:NroCalle> ", input.getDireccion().getNroCalle());
        builder.append("               <tem:Calle3>%s</tem:Calle3>", input.getDireccion().getCalle3());
        builder.append("               <tem:Barrio>%s</tem:Barrio> ", input.getDireccion().getBarrio());
        builder.append("               <tem:Ciudad> ");
        builder.append("                  <tem:Codigo>%s</tem:Codigo> ", input.getDireccion().getCiudad().getCodigo());
        builder.append("                  <tem:Descripcion>%s</tem:Descripcion> ", input.getDireccion().getCiudad().getDescripcion());
        builder.append("                  <tem:Sigla/> ");
        builder.append("                  <tem:TipoPersona/> ");
        builder.append("                  <tem:CodRegion>%s</tem:CodRegion> ", input.getDireccion().getCiudad().getCodigoRegion());
        builder.append("               </tem:Ciudad> ");
        builder.append("               <tem:Region> ");
        builder.append("                  <tem:Codigo>%s</tem:Codigo> ", input.getDireccion().getRegion().getCodigo());
        builder.append("                  <tem:Descripcion>%s</tem:Descripcion> ", input.getDireccion().getRegion().getDescripcion());
        builder.append("                  <tem:Sigla/> ");
        builder.append("                  <tem:TipoPersona/> ");
        builder.append("               </tem:Region> ");
        builder.append("               <tem:NroLineaBaja>%s</tem:NroLineaBaja> ", input.getContacto().getNroLineaBaja());
        builder.append("               <tem:NroCelular>%s</tem:NroCelular> ", input.getContacto().getNroCelular());
        builder.append("               <tem:Email>%s</tem:Email> ", input.getContacto().getEmail());
        builder.append("            </tem:Direccion> ");
        builder.append("            <tem:ActividadEconomica> ");
        builder.append("               <tem:Codigo>%s</tem:Codigo> ", input.getActividadEconomica().getCodigo());
        builder.append("               <tem:Descripcion>%s</tem:Descripcion> ", input.getActividadEconomica().getDescripcion());
        builder.append("               <tem:Sigla/> ");
        builder.append("               <tem:TipoPersona/> ");
        builder.append("            </tem:ActividadEconomica> ");
        builder.append("            <tem:PromedioMensual>%s</tem:PromedioMensual> ", input.getFacturacionPromedio().toString());
        builder.append("            <tem:DescripcionRubro>%s</tem:DescripcionRubro> ", input.getRubro().getDescripcion());
        builder.append("            <tem:FechaConstitucion>%sT00:00:00</tem:FechaConstitucion> ", Fechas.toString(input.getSociedad().getFechaConstitucion(), "yyyy-MM-dd"));
        builder.append("            <tem:RazonSocial>%s</tem:RazonSocial> ", input.getRazonSocial());
        builder.append("            <tem:TipoSociedad> ");
        builder.append("               <tem:Codigo/> ");
        builder.append("               <tem:Descripcion>%s</tem:Descripcion> ", input.getSociedad().getTipoSociedad().getDescripcion());
        builder.append("               <tem:Sigla>%s</tem:Sigla> ", input.getSociedad().getTipoSociedad().getSigla());
        builder.append("               <tem:TipoPersona/> ");
        builder.append("            </tem:TipoSociedad> ");
        builder.append("        </tem:personaJuridica> ");
        builder.append("          <tem:Acceso> ");
        builder.append("             <tem:usuario>%s</tem:usuario> ", usuario);
        builder.append("             <tem:password>%s</tem:password> ", password);
        builder.append("             <tem:claveP>%s</tem:claveP> ", clavePublica);
        builder.append("          </tem:Acceso> ");
        builder.append("        </tem:AltaComprador> ");
        builder.append("    </soap:Body> ");
        builder.append(" </soap:Envelope> ");
        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de Compradores Activos">
    public Result<ConsultaCompradoresOutput> consultarCompradores(ConsultaCompradoresInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaConsultaCompradores respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest, "text/xml;charset=UTF-8");

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Código de respuesta HTTP inválido (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaConsultaCompradores.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaConsultaCompradores.instance("-1", thr.getMessage());
            track.setKo(thr);
        }

        // Datos finales.
        ConsultaCompradoresOutput output = new ConsultaCompradoresOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(ConsultaCompradoresInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"> ");
        builder.append("   <soap:Header/> ");
        builder.append("   <soap:Body> ");
        builder.append("     <tem:ConsultarCompradores> ");
        builder.append("       <tem:Acceso> ");
        builder.append("         <tem:usuario>%s</tem:usuario> ", usuario);
        builder.append("         <tem:password>%s</tem:password> ", password);
        builder.append("         <tem:claveP>%s</tem:claveP> ", clavePublica);
        builder.append("       </tem:Acceso> ");
        builder.append("     </tem:ConsultarCompradores> ");
        builder.append("   </soap:Body> ");
        builder.append(" </soap:Envelope> ");
        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de Estado de Proveedor">
    public Result<ConsultaEstadoProveedorOutput> consultarEstadoProveedor(ConsultaEstadoProveedorInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaConsultaEstadoProveedor respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest, "text/xml;charset=UTF-8");

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Código de respuesta HTTP inválido (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaConsultaEstadoProveedor.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaConsultaEstadoProveedor.instance("-1", thr.getMessage());
            track.setKo(thr);
        }

        // Datos finales.
        ConsultaEstadoProveedorOutput output = new ConsultaEstadoProveedorOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(ConsultaEstadoProveedorInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"> ");
        builder.append("   <soap:Header/> ");
        builder.append("   <soap:Body> ");
        builder.append("     <tem:ConsultarEstadoProveedor> ");
        builder.append("       <tem:Acceso> ");
        builder.append("         <tem:usuario>%s</tem:usuario> ", usuario);
        builder.append("         <tem:password>%s</tem:password> ", password);
        builder.append("         <tem:claveP>%s</tem:claveP> ", clavePublica);
        builder.append("       </tem:Acceso> ");
        builder.append("       <tem:consultaProveedorRequest> ");
        builder.append("         <tem:Comprador> ");
        builder.append("           <tem:TipoPersona>%s</tem:TipoPersona> ", resolveTipoPersona(input.getComprador()));
        builder.append("           <tem:TipoDocumento> ");
        builder.append("             <tem:Codigo>%s</tem:Codigo> ", input.getCompradorDocumento().getTipoDocumento().getCodigo());
        builder.append("             <tem:Descripcion>%s</tem:Descripcion> ", input.getCompradorDocumento().getTipoDocumento().getDescripcion());
        builder.append("             <tem:Sigla>%s</tem:Sigla> ", input.getCompradorDocumento().getTipoDocumento().getSigla());
        builder.append("           </tem:TipoDocumento> ");
        builder.append("           <tem:NroDocumento>%s</tem:NroDocumento> ", cleanupRuc(input.getCompradorDocumento().getNroDocumento()));
        builder.append("           <tem:PromedioMensual>0</tem:PromedioMensual> ");
        builder.append("           <tem:Tasa>0</tem:Tasa> ");
        builder.append("           <tem:CobrarSeguro>false</tem:CobrarSeguro> ");
        builder.append("           <tem:PorcentajeGastosAdm>0</tem:PorcentajeGastosAdm> ");
        builder.append("         </tem:Comprador> ");
        builder.append("         <tem:Proveedor> ");
        builder.append("           <tem:TipoPersona>%s</tem:TipoPersona> ", resolveTipoPersona(input.getProveedor()));
        builder.append("           <tem:TipoDocumento> ");
        builder.append("             <tem:Codigo>%s</tem:Codigo> ", input.getProveedorDocumento().getTipoDocumento().getCodigo());
        builder.append("             <tem:Descripcion>%s</tem:Descripcion> ", input.getProveedorDocumento().getTipoDocumento().getDescripcion());
        builder.append("             <tem:Sigla>%s</tem:Sigla> ", input.getProveedorDocumento().getTipoDocumento().getSigla());
        builder.append("           </tem:TipoDocumento> ");
        builder.append("           <tem:NroDocumento>%s</tem:NroDocumento> ", cleanupRuc(input.getProveedorDocumento().getNroDocumento()));
        builder.append("           <tem:PromedioMensual>0</tem:PromedioMensual> ");
        builder.append("         </tem:Proveedor> ");
        builder.append("       </tem:consultaProveedorRequest> ");
        builder.append("     </tem:ConsultarEstadoProveedor> ");
        builder.append("   </soap:Body> ");
        builder.append(" </soap:Envelope> ");
        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Calculo Monto Neto de Facturas">
    public Result<CalculoNetoFacturasOutput> calculoNetoFacturas(CalculoNetoFacturasInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaCalculoNetoFacturas respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest);

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Código de respuesta HTTP inválido (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaCalculoNetoFacturas.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaCalculoNetoFacturas.instance("-1", thr.getMessage());
            track.setKo(thr);
        }

        // Datos finales.
        CalculoNetoFacturasOutput output = new CalculoNetoFacturasOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(CalculoNetoFacturasInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\"> ");
        builder.append("    <soap:Header/> ");
        builder.append("    <soap:Body> ");
        builder.append("        <tem:CalcularNetoFacturas> ");
        builder.append("          <tem:simuladorRequest>");
        builder.append("             <tem:Simulador>");
        builder.append("                <tem:TipoDocumentoComprador>");
        builder.append("                   <tem:Codigo>%s</tem:Codigo>", input.getDocumentoComprador().getTipoDocumento().getCodigo());
        builder.append("                   <tem:Descripcion>%s</tem:Descripcion>", input.getDocumentoComprador().getTipoDocumento().getDescripcion());
        builder.append("                   <tem:Sigla>%s</tem:Sigla>", input.getDocumentoComprador().getTipoDocumento().getSigla());
        builder.append("                   <tem:TipoPersona/>");
        builder.append("                </tem:TipoDocumentoComprador>");
        builder.append("                <tem:NroDocumentoComprador>%s</tem:NroDocumentoComprador>", cleanupRuc(input.getDocumentoComprador().getNroDocumento()));
        builder.append("                <tem:TipoDocumentoProveedor>");
        builder.append("                   <tem:Codigo>%s</tem:Codigo>", input.getDocumentoProveedor().getTipoDocumento().getCodigo());
        builder.append("                   <tem:Descripcion>%s</tem:Descripcion>", input.getDocumentoProveedor().getTipoDocumento().getDescripcion());
        builder.append("                   <tem:Sigla>%s</tem:Sigla>", input.getDocumentoProveedor().getTipoDocumento().getSigla());
        builder.append("                   <tem:TipoPersona/>");
        builder.append("                </tem:TipoDocumentoProveedor>");
        builder.append("                <tem:NroDocumentoProveedor>%s</tem:NroDocumentoProveedor>", cleanupRuc(input.getDocumentoProveedor().getNroDocumento()));
        builder.append("                <tem:PorcentajeUsoPlataforma>%s</tem:PorcentajeUsoPlataforma>", input.getPorcentajeUsoPlataforma());
        builder.append("                <tem:TasaAnualNominal>0</tem:TasaAnualNominal>");
        builder.append("                <tem:TasaAnualEfectiva>0</tem:TasaAnualEfectiva>");
        builder.append("                <tem:DetalleDeCalculo>");
        for (FacturaInfo factura : input.getFacturas()) {
            builder.append("               <tem:SimuladorCalculoDetalle>");
            builder.append("                  <tem:NroFactura>%s</tem:NroFactura>", factura.getNumero());
            builder.append("                  <tem:Monto>%s</tem:Monto>", Numeric.round(factura.getMonto(), 0));
            builder.append("                  <tem:FechaVto>%sT00:00:00</tem:FechaVto> ", Fechas.toString(factura.getFechaVencimiento(), "yyyy-MM-dd"));
            builder.append("               </tem:SimuladorCalculoDetalle>");
        }
        builder.append("                </tem:DetalleDeCalculo>");
        builder.append("             </tem:Simulador>");
        builder.append("          </tem:simuladorRequest>");
        builder.append("          <tem:Acceso> ");
        builder.append("             <tem:usuario>%s</tem:usuario> ", usuario);
        builder.append("             <tem:password>%s</tem:password> ", password);
        builder.append("             <tem:claveP>%s</tem:claveP> ", clavePublica);
        builder.append("          </tem:Acceso> ");
        builder.append("        </tem:CalcularNetoFacturas> ");
        builder.append("    </soap:Body> ");
        builder.append(" </soap:Envelope> ");
        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Solicitud de Desembolso">
    public Result<SolicitudDesembolsoOutput> solicitudDesembolso(SolicitudDesembolsoInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaSolicitudDesembolso respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest, "text/xml;charset=UTF-8");

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Código de respuesta HTTP inválido (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaSolicitudDesembolso.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaSolicitudDesembolso.instance("-1", thr.getMessage());
            track.setKo(thr);
        }

        // Datos finales.
        SolicitudDesembolsoOutput output = new SolicitudDesembolsoOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(SolicitudDesembolsoInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
        builder.append("   <soapenv:Header/>");
        builder.append("   <soapenv:Body>");
        builder.append("     <tem:SolicitarAdelantoDeFacturas>");
        builder.append("       <tem:Acceso>");
        builder.append("         <tem:usuario>%s</tem:usuario>", usuario);
        builder.append("         <tem:password>%s</tem:password>", password);
        builder.append("         <tem:claveP>%s</tem:claveP>", clavePublica);
        builder.append("       </tem:Acceso>");
        builder.append("       <tem:solicitudRequest>");
        builder.append("         <tem:Comprador>");
        builder.append("           <tem:CodCliente>%s</tem:CodCliente>", input.getCompradorCodCliente());
        builder.append("           <tem:TipoPersona>%s</tem:TipoPersona>", resolveTipoPersona(input.getComprador()));
        builder.append("           <tem:TipoDocumento>");
        builder.append("             <tem:Codigo>%s</tem:Codigo>", input.getCompradorDocumento().getTipoDocumento().getCodigo());
        builder.append("             <tem:Descripcion>%s</tem:Descripcion>", input.getCompradorDocumento().getTipoDocumento().getDescripcion());
        builder.append("             <tem:Sigla>%s</tem:Sigla>", input.getCompradorDocumento().getTipoDocumento().getSigla());
        builder.append("             <tem:TipoPersona/>");
        builder.append("           </tem:TipoDocumento>");
        builder.append("           <tem:NroDocumento>%s</tem:NroDocumento>", cleanupRuc(input.getCompradorDocumento().getNroDocumento()));
        builder.append("           <tem:EmailNotificacion>%s</tem:EmailNotificacion>", input.getCompradorEmailNotificacion());
        builder.append("         </tem:Comprador>");
        builder.append("         <tem:Proveedor>");
        builder.append("           <tem:CodCliente>%s</tem:CodCliente>", input.getProveedorCodCliente());
        builder.append("           <tem:TipoPersona>%s</tem:TipoPersona>", resolveTipoPersona(input.getProveedor()));
        builder.append("           <tem:TipoDocumento>");
        builder.append("             <tem:Codigo>%s</tem:Codigo>", input.getProveedorDocumento().getTipoDocumento().getCodigo());
        builder.append("             <tem:Descripcion>%s</tem:Descripcion>", input.getProveedorDocumento().getTipoDocumento().getDescripcion());
        builder.append("             <tem:Sigla>%s</tem:Sigla>", input.getProveedorDocumento().getTipoDocumento().getSigla());
        builder.append("             <tem:TipoPersona/>");
        builder.append("           </tem:TipoDocumento>");
        builder.append("           <tem:NroDocumento>%s</tem:NroDocumento>", cleanupRuc(input.getProveedorDocumento().getNroDocumento()));
        builder.append("           <tem:EmailNotificacion>%s</tem:EmailNotificacion>", input.getProveedorEmailNotificacion());
        builder.append("         </tem:Proveedor>");
        builder.append("         <tem:PorcentajeUsoPlataforma>%s</tem:PorcentajeUsoPlataforma>", input.getPorcentajeUsoPlataforma());
        builder.append("         <tem:InfoCuentaAcreditacion>");
        builder.append("           <tem:Banco>");
        builder.append("             <tem:Codigo>%s</tem:Codigo>", input.getProveedorCuentaAcreditacion().getBanco().getCodigo());
        builder.append("             <tem:Descripcion>%s</tem:Descripcion>", input.getProveedorCuentaAcreditacion().getBanco().getDescripcion());
        builder.append("             <tem:Sigla/>");
        builder.append("             <tem:TipoPersona/>");
        builder.append("           </tem:Banco>");
        builder.append("           <tem:NroCuenta>%s</tem:NroCuenta>", input.getProveedorCuentaAcreditacion().getNroCuenta());
        builder.append("         </tem:InfoCuentaAcreditacion>");
        builder.append("         <tem:DetalleFacturas>");
        for (FacturaInfo factura : input.getFacturas()) {
            builder.append("       <tem:SolicitudAdelantoDetalle>");
            builder.append("         <tem:NroFactura>%s</tem:NroFactura>", factura.getNumero());
            builder.append("         <tem:Monto>%s</tem:Monto>", Numeric.round(factura.getMonto(), 0));
            builder.append("         <tem:FechaVto>%sT00:00:00</tem:FechaVto> ", Fechas.toString(factura.getFechaVencimiento(), "yyyy-MM-dd"));
            builder.append("       </tem:SolicitudAdelantoDetalle>");
        }
        builder.append("         </tem:DetalleFacturas>      ");
        builder.append("       </tem:solicitudRequest>");
        builder.append("     </tem:SolicitarAdelantoDeFacturas>");
        builder.append("   </soapenv:Body>");
        builder.append(" </soapenv:Envelope>");
        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta Estado de Desembolso">
    public Result<ConsultaEstadoDesembolsoOutput> consultarEstadoDesembolso(ConsultaEstadoDesembolsoInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaConsultaEstadoDesembolso respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest, "text/xml;charset=UTF-8");

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Ourrió un error consumiendo servicio de entidad financiera (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaConsultaEstadoDesembolso.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaConsultaEstadoDesembolso.instance("-1", thr.getMessage());
            track.setKo(thr);
        }

        // Datos finales.
        ConsultaEstadoDesembolsoOutput output = new ConsultaEstadoDesembolsoOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(ConsultaEstadoDesembolsoInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"> ");
        builder.append("   <soapenv:Header/> ");
        builder.append("   <soapenv:Body> ");
        builder.append("     <tem:ConsultarSolicitud> ");
        builder.append("       <tem:Acceso> ");
        builder.append("         <tem:usuario>%s</tem:usuario> ", usuario);
        builder.append("         <tem:password>%s</tem:password> ", password);
        builder.append("         <tem:claveP>%s</tem:claveP> ", clavePublica);
        builder.append("       </tem:Acceso> ");
        builder.append("       <tem:request> ");
        builder.append("         <tem:Comprador> ");
        builder.append("           <tem:TipoPersona>%s</tem:TipoPersona> ", resolveTipoPersona(input.getComprador()));
        builder.append("           <tem:TipoDocumento> ");
        builder.append("             <tem:Codigo>%s</tem:Codigo> ", input.getCompradorDocumento().getTipoDocumento().getCodigo());
        builder.append("             <tem:Descripcion>%s</tem:Descripcion> ", input.getCompradorDocumento().getTipoDocumento().getDescripcion());
        builder.append("             <tem:Sigla>%s</tem:Sigla> ", input.getCompradorDocumento().getTipoDocumento().getSigla());
        builder.append("             <tem:TipoPersona/> ");
        builder.append("           </tem:TipoDocumento> ");
        builder.append("           <tem:NroDocumento>%s</tem:NroDocumento> ", cleanupRuc(input.getCompradorDocumento().getNroDocumento()));
        builder.append("           <tem:PromedioMensual>0</tem:PromedioMensual> ");
        builder.append("           <tem:Tasa>0</tem:Tasa> ");
        builder.append("           <tem:CobrarSeguro>false</tem:CobrarSeguro> ");
        builder.append("           <tem:PorcentajeGastosAdm>0</tem:PorcentajeGastosAdm> ");
        builder.append("         </tem:Comprador> ");
        builder.append("         <tem:TipoConsulta>POR_ID_SOLICITUD</tem:TipoConsulta> ");
        builder.append("         <tem:IdSolicitud>%s</tem:IdSolicitud> ", input.getIdSolicitud());
        builder.append("       </tem:request> ");
        builder.append("         </tem:ConsultarSolicitud> ");
        builder.append("     </soapenv:Body> ");
        builder.append(" </soapenv:Envelope> ");
        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de Facturas Liquidables">
    public Result<ConsultaFacturasLiquidablesOutput> consultarFacturasLiquidables(ConsultaFacturasLiquidablesInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaConsultaFacturasLiquidables respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest, "text/xml;charset=UTF-8");

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Código de respuesta HTTP inválido (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaConsultaFacturasLiquidables.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaConsultaFacturasLiquidables.instance("-1", thr.getMessage());
            track.setKo(thr);
        }

        // Datos finales.
        ConsultaFacturasLiquidablesOutput output = new ConsultaFacturasLiquidablesOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(ConsultaFacturasLiquidablesInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"> ");
        builder.append("    <soapenv:Header/> ");
        builder.append("    <soapenv:Body> ");
        builder.append("       <tem:ConsultarFacturasALiquidar> ");
        builder.append("          <tem:Acceso> ");
        builder.append("             <tem:usuario>%s</tem:usuario> ", usuario);
        builder.append("             <tem:password>%s</tem:password> ", password);
        builder.append("             <tem:claveP>%s</tem:claveP> ", clavePublica);
        builder.append("          </tem:Acceso> ");
        builder.append("          <tem:request> ");
        builder.append("             <tem:Comprador> ");
        builder.append("                <tem:TipoPersona>%s</tem:TipoPersona> ", resolveTipoPersona(input.getComprador()));
        builder.append("                <tem:TipoDocumento> ");
        builder.append("                   <tem:Codigo>%s</tem:Codigo> ", input.getCompradorDocumento().getTipoDocumento().getCodigo());
        builder.append("                   <tem:Descripcion>%s</tem:Descripcion> ", input.getCompradorDocumento().getTipoDocumento().getDescripcion());
        builder.append("                   <tem:Sigla>%s</tem:Sigla> ", input.getCompradorDocumento().getTipoDocumento().getSigla());
        builder.append("                </tem:TipoDocumento> ");
        builder.append("                <tem:NroDocumento>%s</tem:NroDocumento> ", cleanupRuc(input.getCompradorDocumento().getNroDocumento()));
        builder.append("                <tem:PromedioMensual>0</tem:PromedioMensual> ");
        builder.append("                <tem:Tasa>0</tem:Tasa> ");
        builder.append("                <tem:CobrarSeguro>false</tem:CobrarSeguro> ");
        builder.append("                <tem:PorcentajeGastosAdm>0</tem:PorcentajeGastosAdm> ");
        builder.append("             </tem:Comprador> ");
        builder.append("             <tem:FechaInicio>%sT00:00:00</tem:FechaInicio> ", Fechas.toString(input.getFechaDesde(), "yyyy-MM-dd"));
        builder.append("             <tem:FechaFin>%sT23:59:59</tem:FechaFin> ", Fechas.toString(input.getFechaHasta(), "yyyy-MM-dd"));
        builder.append("          </tem:request> ");
        builder.append("       </tem:ConsultarFacturasALiquidar> ");
        builder.append("    </soapenv:Body> ");
        builder.append(" </soapenv:Envelope> ");
        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Solicitud de Liquidacion">
    public Result<SolicitudLiquidacionOutput> solicitudLiquidacion(SolicitudLiquidacionInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaSolicitudLiquidacion respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest, "text/xml;charset=UTF-8");

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Código de respuesta HTTP inválido (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaSolicitudLiquidacion.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaSolicitudLiquidacion.instance("-1", thr.getMessage());
            track.setKo(thr);
        }

        // Datos finales.
        SolicitudLiquidacionOutput output = new SolicitudLiquidacionOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(SolicitudLiquidacionInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">");
        builder.append("   <soapenv:Header/>");
        builder.append("   <soapenv:Body>");
        builder.append("     <tem:SolicitarLiquidacionDeFacturas>");
        builder.append("       <tem:Acceso>");
        builder.append("         <tem:usuario>%s</tem:usuario>", usuario);
        builder.append("         <tem:password>%s</tem:password>", password);
        builder.append("         <tem:claveP>%s</tem:claveP>", clavePublica);
        builder.append("       </tem:Acceso>");
        builder.append("       <tem:request>");
        builder.append("         <tem:Comprador>");
        builder.append("           <tem:TipoPersona>%s</tem:TipoPersona>", resolveTipoPersona(input.getComprador()));
        builder.append("           <tem:TipoDocumento>");
        builder.append("             <tem:Codigo>%s</tem:Codigo>", input.getCompradorDocumento().getTipoDocumento().getCodigo());
        builder.append("             <tem:Descripcion>%s</tem:Descripcion>", input.getCompradorDocumento().getTipoDocumento().getDescripcion());
        builder.append("             <tem:Sigla>%s</tem:Sigla>", input.getCompradorDocumento().getTipoDocumento().getSigla());
        builder.append("           </tem:TipoDocumento>");
        builder.append("           <tem:NroDocumento>%s</tem:NroDocumento>", cleanupRuc(input.getCompradorDocumento().getNroDocumento()));
        builder.append("           <tem:PromedioMensual>0</tem:PromedioMensual>");
        builder.append("           <tem:Tasa>0</tem:Tasa>");
        builder.append("           <tem:CobrarSeguro>false</tem:CobrarSeguro>");
        builder.append("           <tem:PorcentajeGastosAdm>0</tem:PorcentajeGastosAdm>");
        builder.append("         </tem:Comprador>");
        builder.append("         <tem:InfoComisionComprador>");
        builder.append("           <tem:PorcentajeComision>%s</tem:PorcentajeComision>", input.getComisionComprador().getPorcentajeComision());
        builder.append("           <tem:MontoComision>%s</tem:MontoComision>", input.getComisionComprador().getMontoComision());
        builder.append("           <tem:NroCuentaCredito>%s</tem:NroCuentaCredito>", input.getComisionComprador().getNroCuentaCredito());
        builder.append("         </tem:InfoComisionComprador>");
        builder.append("         <tem:InfoComisionBanco>");
        builder.append("           <tem:PorcentajeComision>%s</tem:PorcentajeComision>", input.getComisionBanco().getPorcentajeComision());
        builder.append("           <tem:MontoComision>%s</tem:MontoComision>", input.getComisionBanco().getMontoComision());
        builder.append("         </tem:InfoComisionBanco>");
        builder.append("         <tem:InfoComisionSepsa>");
        builder.append("           <tem:MontoDebito>%s</tem:MontoDebito>", input.getComisionComprador().getMontoComision());
        builder.append("           <tem:NroCuentaDebito>%s</tem:NroCuentaDebito>", input.getComisionSepsa().getNroCuentaDebito());
        builder.append("         </tem:InfoComisionSepsa>");
        builder.append("         <tem:ListaFacturasALiquidar>");
        if (!Assertions.isNullOrEmpty(input.getFacturasALiquidar())) {
            for (FacturaALiquidarInfo factura : input.getFacturasALiquidar()) {
                builder.append("     <tem:InfoFacturaALiquidar>");
                builder.append("       <tem:NroOperacion>%s</tem:NroOperacion>", factura.getNroOperacion());
                builder.append("       <tem:IdSolicitud>%s</tem:IdSolicitud>", factura.getIdSolicitud());
                builder.append("       <tem:NroFactura>%s</tem:NroFactura>", factura.getNroFactura());
                builder.append("       <tem:FechaVto>%sT00:00:00</tem:FechaVto> ", Fechas.toString(factura.getFechaVencimiento(), "yyyy-MM-dd"));
                builder.append("       <tem:Monto>%s</tem:Monto>", Numeric.round(factura.getMonto(), 0));
                builder.append("     </tem:InfoFacturaALiquidar>");
            }
        }
        builder.append("         </tem:ListaFacturasALiquidar>      ");
        builder.append("       </tem:request>");
        builder.append("     </tem:SolicitarLiquidacionDeFacturas>");
        builder.append("   </soapenv:Body>");
        builder.append(" </soapenv:Envelope>");
        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta Estado de Liquidacion">
    public Result<ConsultaEstadoLiquidacionOutput> consultarEstadoLiquidacion(ConsultaEstadoLiquidacionInput input) {
        // Objeto de seguimiento.
        WsTrackInfo track = new WsTrackInfo();
        track.initReferencia();

        // Datos de respuesta.
        RespuestaConsultaEstadoLiquidacion respuesta;

        try {
            try (CloseableHttpClient client = prepareHttpClient()) {
                // Enviar peticion.
                String xmlSoapRequest = genSOAP(input);
                initRequest(track, xmlSoapRequest);
                HttpResponse response = post(track, client, obtenerWsUrl(), xmlSoapRequest, "text/xml;charset=UTF-8");

                // Registrar respuesta.
                String xmlSoapResponse = EntityUtils.toString(response.getEntity());
                initResponse(track, xmlSoapResponse);

                // Control.
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    throw Errors.illegalState("Ourrió un error consumiendo servicio de entidad financiera (%s)", statusCode);
                }

                // Procesar respuesta.
                respuesta = RespuestaConsultaEstadoLiquidacion.parse(xmlSoapResponse);
                track.setOk();
            }
        } catch (Throwable thr) {
            // Procesar respuesta.
            respuesta = RespuestaConsultaEstadoLiquidacion.instance("-1", thr.getMessage());
            track.setKo(thr);
        }

        // Datos finales.
        ConsultaEstadoLiquidacionOutput output = new ConsultaEstadoLiquidacionOutput();
        output.setTrack(track);
        output.setRespuesta(respuesta);

        return Results.ok()
                .value(output)
                .build();
    }

    private String genSOAP(ConsultaEstadoLiquidacionInput input) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"> ");
        builder.append("   <soapenv:Header/> ");
        builder.append("   <soapenv:Body> ");
        builder.append("     <tem:ConsultarLiquidacion> ");
        builder.append("       <tem:Acceso> ");
        builder.append("         <tem:usuario>%s</tem:usuario> ", usuario);
        builder.append("         <tem:password>%s</tem:password> ", password);
        builder.append("         <tem:claveP>%s</tem:claveP> ", clavePublica);
        builder.append("       </tem:Acceso> ");
        builder.append("       <tem:request> ");
        builder.append("         <tem:Comprador> ");
        builder.append("           <tem:TipoPersona>%s</tem:TipoPersona> ", resolveTipoPersona(input.getComprador()));
        builder.append("           <tem:TipoDocumento> ");
        builder.append("             <tem:Codigo>%s</tem:Codigo> ", input.getCompradorDocumento().getTipoDocumento().getCodigo());
        builder.append("             <tem:Descripcion>%s</tem:Descripcion> ", input.getCompradorDocumento().getTipoDocumento().getDescripcion());
        builder.append("             <tem:Sigla>%s</tem:Sigla> ", input.getCompradorDocumento().getTipoDocumento().getSigla());
        builder.append("             <tem:TipoPersona/> ");
        builder.append("           </tem:TipoDocumento> ");
        builder.append("           <tem:NroDocumento>%s</tem:NroDocumento> ", cleanupRuc(input.getCompradorDocumento().getNroDocumento()));
        builder.append("           <tem:PromedioMensual>0</tem:PromedioMensual> ");
        builder.append("           <tem:Tasa>0</tem:Tasa> ");
        builder.append("           <tem:CobrarSeguro>false</tem:CobrarSeguro> ");
        builder.append("           <tem:PorcentajeGastosAdm>0</tem:PorcentajeGastosAdm> ");
        builder.append("         </tem:Comprador> ");
        builder.append("         <tem:IdSolicitud>%s</tem:IdSolicitud> ", input.getIdLiquidacion());
        builder.append("       </tem:request> ");
        builder.append("         </tem:ConsultarLiquidacion> ");
        builder.append("     </soapenv:Body> ");
        builder.append(" </soapenv:Envelope> ");
        return builder.toString();
    }
    //</editor-fold>

    private String obtenerWsUrl() throws Throwable {
        Configuraciones config = Injection.lookup(Configuraciones.class);
        String url = config.buscarValorParametro(ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_WEB_SERVICE_URL).value("");
        if (Assertions.stringNullOrEmpty(url)) {
            throw Errors.illegalState("No se configuro la url de conexion a servicio de entidad financiera.");
        }
        return url;
    }

    /**
     * Determina el valor de tipo persona a incluir en el XML de peticiones
     * SOAP.
     *
     * @param persona Persona.
     * @return Tipo de persona.
     */
    private String resolveTipoPersona(Persona persona) {
        Personas.TipoPersonaEnum tipoPersona = TipoPersonaEnum.fromIdTipoPersona(persona.getIdTipoPersona());
        switch (tipoPersona) {
            case FISICA:
                return "FISICA";
            case JURIDICA:
                return "JURIDICA";
            default:
                return "";
        }
    }

    /**
     * Codifica un array de bytes a su representacion en Base 64.
     *
     * @param bytes Array de bytes.
     * @return Cadena en base 64.
     */
    private String base64(byte[] bytes) {
        if (Assertions.isNullOrEmpty(bytes)) {
            return "";
        } else {
            String b64 = Base64.getMimeEncoder().encodeToString(bytes);
            b64 = b64.replaceAll("\r\n", "");
            b64 = b64.replaceAll("\r", "");
            b64 = b64.replaceAll("\n", "");
            return b64;
        }
    }

    /**
     * Dada una cadena numerica de RUC, retorna el mismo sin el separador de
     * digito verificador en caso de que exista.
     * <br/>
     * Ej.: <code>123456-7</code> pasa a ser <code>1234567</code>.
     *
     * @param ruc Cadena de RUC.
     * @return cadena sin el separador de digito verificador.
     */
    private String cleanupRuc(String ruc) {
        // Eliminar separador, si hubiere.
        if (ruc.contains("-")) {
            ruc = ruc.replaceAll(Pattern.quote("-"), "");
            return ruc;
        } else {
            return ruc;
        }
    }

    /**
     * Dada una cadena idealmente compuesta de varias palabras separadas por
     * espacios, retorna la primera palabra disponible.
     * <br/>
     * Ej.: <code>a b c</code> pasa a ser <code>a</code>.
     *
     * @param value Cadena de palabras separadas por espacios.
     * @return Primera palabra.
     */
    private String primeraParte(String value) {
        // Control.
        if (Assertions.stringNullOrEmpty(value)) {
            return "";
        }

        value = value.trim();
        String[] parts = value.split(Pattern.quote(" "));
        return parts[0];
    }

    /**
     * Dada una cadena idealmente compuesta de varias palabras separadas por
     * espacios, retorna la subcadena de palabras que no contienen a la primera.
     * <br/>
     * Ej.: <code>a b c</code> pasa a ser <code>b c</code>.
     *
     * @param value
     * @return
     */
    private String segundaParte(String value) {
        // Control.
        if (Assertions.stringNullOrEmpty(value)) {
            return "";
        }

        value = value.trim();
        String[] parts = value.split(Pattern.quote(" "));
        if (parts.length <= 1) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < parts.length; i++) {
            builder.append(parts[i]);
            builder.append(" ");
        }
        return builder.toString().trim();
    }

    private HttpResponse post(WsTrackInfo track, HttpClient client, String endpoint, String content) throws Throwable {
        return post(track, client, endpoint, content, "application/soap+xml;charset=UTF-8");
    }

    private HttpResponse post(WsTrackInfo track, HttpClient client, String endpoint, String content, String contentType) throws Throwable {
        HttpPost POST = new HttpPost(endpoint);
        POST.setHeader("Content-type", contentType);

        byte[] contentBytes = Strings.getBytes(content);
        InputStreamEntity entity = new InputStreamEntity(new ByteArrayInputStream(contentBytes), contentBytes.length);
        POST.setEntity(entity);

        try {
            track.initRequestInicio();
            HttpResponse response = client.execute(POST);
            return response;
        } finally {
            track.initRequestFin();
        }
    }

    private CloseableHttpClient prepareHttpClient() throws Throwable {
        int timeout = (int) TimeUnit.MINUTES.toMillis(5);

        // Configuracion general de peticiones.
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(timeout)
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .build();

        // Construccion cliente HTTP.
        return HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .build();
    }

}
