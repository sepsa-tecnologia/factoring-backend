/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.notificacion;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "notificacion_email_factoring_factura", schema = "notificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotificacionEmailFactoringFactura.findAll", query = "SELECT n FROM NotificacionEmailFactoringFactura n"),
    @NamedQuery(name = "NotificacionEmailFactoringFactura.findByIdNotificacion", query = "SELECT n FROM NotificacionEmailFactoringFactura n WHERE n.notificacionEmailFactoringFacturaPK.idNotificacion = :idNotificacion"),
    @NamedQuery(name = "NotificacionEmailFactoringFactura.findByIdFactura", query = "SELECT n FROM NotificacionEmailFactoringFactura n WHERE n.notificacionEmailFactoringFacturaPK.idFactura = :idFactura")})
public class NotificacionEmailFactoringFactura implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NotificacionEmailFactoringFacturaPK notificacionEmailFactoringFacturaPK;
    @JoinColumn(name = "id_notificacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private NotificacionEmailFactoring notificacionEmailFactoring;
    @JoinColumn(name = "id_factura", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Factura factura;

    public NotificacionEmailFactoringFactura() {
    }

    public NotificacionEmailFactoringFactura(NotificacionEmailFactoringFacturaPK notificacionEmailFactoringFacturaPK) {
        this.notificacionEmailFactoringFacturaPK = notificacionEmailFactoringFacturaPK;
    }

    public NotificacionEmailFactoringFactura(int idNotificacion, int idFactura) {
        this.notificacionEmailFactoringFacturaPK = new NotificacionEmailFactoringFacturaPK(idNotificacion, idFactura);
    }

    public NotificacionEmailFactoringFacturaPK getNotificacionEmailFactoringFacturaPK() {
        return notificacionEmailFactoringFacturaPK;
    }

    public void setNotificacionEmailFactoringFacturaPK(NotificacionEmailFactoringFacturaPK notificacionEmailFactoringFacturaPK) {
        this.notificacionEmailFactoringFacturaPK = notificacionEmailFactoringFacturaPK;
    }

    public NotificacionEmailFactoring getNotificacionEmailFactoring() {
        return notificacionEmailFactoring;
    }

    public void setNotificacionEmailFactoring(NotificacionEmailFactoring notificacionEmailFactoring) {
        this.notificacionEmailFactoring = notificacionEmailFactoring;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notificacionEmailFactoringFacturaPK != null ? notificacionEmailFactoringFacturaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof NotificacionEmailFactoringFactura)) {
            return false;
        }
        NotificacionEmailFactoringFactura other = (NotificacionEmailFactoringFactura) object;
        if ((this.notificacionEmailFactoringFacturaPK == null && other.notificacionEmailFactoringFacturaPK != null) || (this.notificacionEmailFactoringFacturaPK != null && !this.notificacionEmailFactoringFacturaPK.equals(other.notificacionEmailFactoringFacturaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.notificacion.NotificacionEmailFactoringFactura[ notificacionEmailFactoringFacturaPK=" + notificacionEmailFactoringFacturaPK + " ]";
    }

}
