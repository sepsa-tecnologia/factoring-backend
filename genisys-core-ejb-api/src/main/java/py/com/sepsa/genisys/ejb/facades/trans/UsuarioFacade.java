package py.com.sepsa.genisys.ejb.facades.trans;

import fa.gs.utils.misc.text.Strings;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 * Fachada de la entidad usuario
 *
 * @author Daniel F. Escauriza Arza
 */
@Stateless(name = "UsuarioFacade", mappedName = "UsuarioFacade")
@LocalBean
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @EJB
    private JpaUtils jpaUtils;

    /**
     * Obtiene un usuario por su nick.
     *
     * @param userId Identificador del usuario
     * @return Usuario
     */
    public Usuario getUser(String userId) {
        Usuario user = null;
        if (userId != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
            Root<Usuario> root = cq.from(Usuario.class);
            cq.select(root);

            cq.where(qb.equal(root.get("usuario"), userId));

            javax.persistence.Query q = getEntityManager().createQuery(cq);

            List rList = JpaUtils.getAll(q);
            if (!rList.isEmpty()) {
                user = (Usuario) rList.get(0);
            }
        }
        return user;
    }

    /**
     * Cambia la contraseña de un usuario
     *
     * @param user Usuario
     * @param newPass Contraseña nueva
     * @param newPassConf Confirmación de la nueva contraseña
     * @param oldPass Contraseña anterior
     * @param estado Estado del usuario
     * @return Si se ha actualizo o no la contraseña
     */
    public boolean changePass(Usuario user, String newPass, String newPassConf, String oldPass, Character estado) {
        boolean changed = false;
        if (user != null && newPass.equals(newPassConf)) {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String encodedOldPass = user.getContrasena();
            if (encoder.matches(oldPass, encodedOldPass)) {
                String encodedNewPass = encoder.encode(newPass);
                user.setContrasena(encodedNewPass);
                user.setEstado(estado);
                edit(user);
                changed = true;
            }
        }

        return changed;
    }

    public void changePass(Integer idUsuario, String newPassPlain) throws Throwable {
        String newPassSalted = new BCryptPasswordEncoder().encode(newPassPlain);
        String query = Strings.format(" update trans.usuario set contrasena = '%s' where id = %s ", newPassSalted, idUsuario);
        jpaUtils.executeUpdate(query);
    }

    /**
     * Constructor de UsuarioFacade
     */
    public UsuarioFacade() {
        super(Usuario.class);
    }
}
