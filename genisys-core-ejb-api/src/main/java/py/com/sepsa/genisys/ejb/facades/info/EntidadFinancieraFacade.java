/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.info;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.info.EntidadFinanciera;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author descauriza
 */
@Stateless(name = "EntidadFinancieraFacade", mappedName = "EntidadFinancieraFacade")
@LocalBean
public class EntidadFinancieraFacade extends AbstractFacade<EntidadFinanciera> {

    public EntidadFinancieraFacade() {
        super(EntidadFinanciera.class);
    }

}
