/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.ConfigCargo;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ConfigCargoFacade", mappedName = "ConfigCargoFacade")
@LocalBean
public class ConfigCargoFacade extends AbstractFacade<ConfigCargo> {

    /**
     * Constructor.
     */
    public ConfigCargoFacade() {
        super(ConfigCargo.class);
    }

    /**
     * Permite obtener la configuracion de cargo aplicado a un convenio
     * especifico y que sea del tipo dado.
     *
     * @param idConvenio Identificador de convenio.
     * @param idTipoCargo Identificador de tipo de cargo.
     * @return Configuracion de cargo, si hubiere. Null caso contrario.
     */
    public ConfigCargo findForConvenio(Integer idConvenio, Integer idTipoCargo) {
        Query q = getEntityManager().createNamedQuery("ConfigCargo.findByIdConvenioTipoCargo");
        q.setParameter("idConvenio", idConvenio);
        q.setParameter("idTipoCargo", idTipoCargo);
        return JpaUtils.getFirst(q);
    }

}
