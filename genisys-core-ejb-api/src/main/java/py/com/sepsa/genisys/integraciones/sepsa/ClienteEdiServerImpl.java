/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.sepsa;

import com.google.gson.JsonObject;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.Type;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.fechas.Fechas;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.misc.text.Strings;
import fa.gs.utils.rest.responses.ServiceRequests;
import fa.gs.utils.result.simple.Result;
import fa.gs.utils.result.simple.Results;
import java.util.Collection;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import lombok.Getter;
import lombok.Setter;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
public class ClienteEdiServerImpl implements ClienteEdiServer {

    private static final AppLogger log = AppLoggerFactory.core();

    private final String ediCoreUrl;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private String softwareName;

    @Getter
    @Setter
    private String softwareVersion;

    private ClienteEdiServerImpl(String ediCoreUrl) {
        this.ediCoreUrl = ediCoreUrl;
    }

    public static ClienteEdiServer dev() {
        return instance("https://core-dev.edi.com.py:443", "fgonzalez", "fgonzalez");
    }

    public static ClienteEdiServer prod() {
        return instance("https://core.edi.com.py:443", "preventa", "preventa");
    }

    private static ClienteEdiServer instance(String ediCoreUrl, String ediCoreUsername, String ediCorePassword) {
        ClienteEdiServerImpl cliente = new ClienteEdiServerImpl(ediCoreUrl);
        cliente.setSoftwareName("E-COBRO");
        cliente.setSoftwareVersion("1.0.0");
        cliente.setUsername(ediCoreUsername);
        cliente.setPassword(ediCorePassword);
        return cliente;
    }

    private WebTarget prepareWebTarget(String fmt, Object... args) {
        String endpoint = Strings.format(fmt, args);
        Client client = (ResteasyClient) ResteasyClientBuilder.newBuilder().build();
        WebTarget target = client.target(endpoint);
        return target;
    }

    @Override
    public Result<String> obtenerTokenAutenticacion() {
        try {
            // Inicializar cliente.
            WebTarget target = prepareWebTarget("%s/SepsaCore-api/api/usuario/login", ediCoreUrl);

            // Enviar peticion.
            Response response = target.request()
                    .header("Connection", "keep-alive")
                    .post(getObtenerTokenRequestBody());

            response.bufferEntity();

            // Control.
            int status = response.getStatus();
            if (status != 200) {
                return Results.ko()
                        .message("EDI Server presenta un error (HTTP %s)", status)
                        .build();
            }

            // Respuesta de petición.
            JsonObject response0 = ServiceRequests.readJsonContent(response).getAsJsonObject();

            // Validar respuesta.
            Boolean success = JsonResolver.bool(response0, "success", Boolean.FALSE);
            if (success == false) {
                log.debug("No se pudo obtener un token de autenticacion [%s]", response0.toString());
                throw Errors.illegalState("Ocurrió un error obteniendo token.");
            }

            // Obtener token.
            String token = JsonResolver.get(response0, "payload.token", Type.STRING);

            return Results.ok()
                    .value(token)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .build();
        }
    }

    private Entity getObtenerTokenRequestBody() {
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add("userName", username);
        builder.add("password", password);

        JsonObject json = builder.build();
        return ServiceRequests.writeJsonContent(json);
    }

    @Override
    public Result<RegistrarLiquidacionOutput> registrarLiquidacion(String token, RegistrarLiquidacionInput input) {
        try {
            // Enviar peticion.
            String url = Strings.format("%s/SepsaFacturacion-api/api/registro-liquidacion", ediCoreUrl);
            WebTarget target = prepareWebTarget(url);
            Response response = target.request()
                    .header("Connection", "keep-alive")
                    .header("Authorization", "Bearer " + token)
                    .post(prepareBody(input));

            response.bufferEntity();

            // Control.
            int status = response.getStatus();
            if (status != 200) {
                return Results.ko()
                        .message("Sepsa-Core presenta un error (HTTP %s)", status)
                        .build();
            }

            // Respuesta de petición.
            JsonObject response0 = ServiceRequests.readJsonContent(response).getAsJsonObject();

            // Resultado de operacion.
            Boolean success = JsonResolver.bool(response0, "success", Boolean.FALSE);
            if (success == false) {
                throw Errors.illegalState("No se pudo registrar la liquidación.");
            }

            RegistrarLiquidacionOutput output = new RegistrarLiquidacionOutput();
            return Results.ok()
                    .value(output)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .build();
        }
    }

    private Entity prepareBody(RegistrarLiquidacionInput input) {
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add("codigoIsoMoneda", input.getCodigoIsoMoneda());
        builder.add("codigoProducto", input.getCodigoProducto());
        builder.add("codigo", input.getCodigo());
        builder.add("fechaEvento", Fechas.toString(input.getFechaEvento(), "yyyy-MM-dd"));
        builder.add("idCliente", input.getIdCliente());
        builder.add("codigoEstado", input.getCodigoEstado());
        builder.add("codigoTipoRegistroLiquidacion", input.getCodigoTipoRegistroLiquidacion());
        builder.add("valor", input.getValor());

        JsonObject json = builder.build();
        return ServiceRequests.writeJsonContent(json);
    }

    @Override
    public Result<Integer> obtenerOCrearIdClienteSepsa(String token, String ruc, String razonSocial, Personas.TipoPersonaEnum tipoPersona) {
        try {
            // Obtener o crear cliente.
            Integer idCliente = consultarCliente(token, ruc);
            if (idCliente == null) {
                idCliente = registrarCliente(token, ruc, razonSocial, tipoPersona);
                if (idCliente == null) {
                    throw Errors.illegalState("No se pudo procesar al cliente con RUC '%s'", ruc);
                }
            }

            return Results.ok()
                    .value(idCliente)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .build();
        }
    }

    private Integer consultarCliente(String token, String ruc) throws Throwable {
        // Enviar peticion.
        String url = Strings.format("%s/SepsaCore-api/api/cliente?nroDocumento=%s", ediCoreUrl, ruc);
        WebTarget target = prepareWebTarget(url);
        Response response = target.request()
                .header("Connection", "keep-alive")
                .header("Authorization", "Bearer " + token)
                .get();

        response.bufferEntity();

        // Control.
        int status = response.getStatus();
        if (status != 200) {
            throw Errors.illegalState("Sepsa-Core presenta un error (HTTP %s)", status);
        }

        // Respuesta de petición.
        JsonObject response0 = ServiceRequests.readJsonContent(response).getAsJsonObject();

        // Resultado de operacion.
        Boolean success = JsonResolver.bool(response0, "success", Boolean.FALSE);
        if (success == false) {
            throw Errors.illegalState("No se pudo consultar al cliente.");
        }

        // Obtener id cliente.
        Collection<Integer> idsCliente = JsonResolver.objectCollection(response0, "payload.data", j -> j.getAsJsonObject().get("idCliente").getAsInt());
        if (Assertions.isNullOrEmpty(idsCliente)) {
            return null;
        } else {
            return Lists.first(idsCliente);
        }
    }

    private Integer registrarCliente(String token, String ruc, String razonSocial, Personas.TipoPersonaEnum tipoPersona) throws Throwable {
        // Enviar peticion.
        String url = Strings.format("%s/SepsaCore-api/api/cliente", ediCoreUrl);
        WebTarget target = prepareWebTarget(url);
        Response response = target.request()
                .header("Connection", "keep-alive")
                .header("Authorization", "Bearer " + token)
                .post(prepareBody(ruc, razonSocial, tipoPersona));

        response.bufferEntity();

        // Control.
        int status = response.getStatus();
        if (status != 200) {
            throw Errors.illegalState("Sepsa-Core presenta un error (HTTP %s)", status);
        }

        // Respuesta de petición.
        JsonObject response0 = ServiceRequests.readJsonContent(response).getAsJsonObject();

        // Resultado de operacion.
        Boolean success = JsonResolver.bool(response0, "success", Boolean.FALSE);
        if (success == false) {
            throw Errors.illegalState("No se pudo registrar la liquidación.");
        }

        Integer idCliente = JsonResolver.integer(response0, "payload.idCliente");
        return idCliente;
    }

    private Entity prepareBody(String ruc, String razonSocial, Personas.TipoPersonaEnum tipoPersona) {
        JsonObjectBuilder ob1 = JsonObjectBuilder.instance();
        ob1.add("codigoTipoPersona", String.valueOf(tipoPersona));
        ob1.add("razonSocial", razonSocial);
        ob1.add("nombreFantasia", razonSocial);
        ob1.add("estado", "A");

        JsonObjectBuilder ob0 = JsonObjectBuilder.instance();
        ob0.add("codigoTipoDocumento", "RUC");
        ob0.add("idComercial", "2343");
        ob0.add("estado", "I");
        ob0.add("porcentajeRetencion", "0");
        ob0.add("nroDocumento", ruc);
        ob0.add("razonSocial", razonSocial);
        ob0.add("nroPlanCuenta", "0");
        ob0.add("persona", ob1.build());

        JsonObject json = ob0.build();
        return ServiceRequests.writeJsonContent(json);
    }

}
