/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.criteria.query.Order;
import fa.gs.misc.Assertions;
import fa.gs.misc.Numeric;
import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.database.query.expressions.literals.CollectionLiteral;
import fa.gs.utils.database.query.expressions.literals.Literal;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.ConfigCargo;
import py.com.sepsa.genisys.ejb.entities.factoring.Convenio;
import py.com.sepsa.genisys.ejb.entities.factoring.ConvenioInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoConvenio;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.enums.TipoCargoEnum;
import py.com.sepsa.genisys.ejb.enums.TipoConvenioEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.ConvenioFacade;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.logic.impl.Cargos.ConfigCargoView;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Convenios", mappedName = "Convenios")
@LocalBean
public class Convenios extends LogicBean {

    @EJB
    private JpaUtils jpaUtils;

    @EJB
    private Personas personas;

    @EJB
    private Configuraciones configuraciones;

    @EJB
    private ConvenioFacade convenioFacade;

    /**
     * Obtiene el porcentaje de sobrecargo a aplicar por utilizacion de la
     * plataforma de factoring. Opcional para algunas entidades (Ej.: Banco
     * RIO).
     *
     * @return Porcentaje configurado de uso de plataforma.
     */
    public BigDecimal obtenerPorcentajeUsoPlataforma() {
        Result<String> result = configuraciones.buscarValorParametro(ConfiguracionGeneralEnum.SYS_PORCENTAJE_USO_PLATAFORMA);
        String value = result.value("1");
        return Numeric.wrap(value);
    }

    /**
     * Cambia el estado de una coleccion de convenios.
     *
     * @param idConvenios Identificadores de convenios.
     * @return {@code true} si los convenios fueron cambiados al estado
     * inactivo, caso contrario {@code false}.
     */
    public Boolean inactivarConvenios(Integer[] idConvenios) {
        if (!Assertions.isNullOrEmpty(idConvenios)) {
            try {
                // Construir consulta.
                Literal literal = CollectionLiteral.instance(idConvenios);
                String query = Strings.format("update factoring.convenio set estado = 'I' where id in %s ;", literal.stringify(null));

                // Actualizar registros.
                jpaUtils.executeUpdate(query);
                return true;
            } catch (Throwable thr) {
                Errors.dump(System.err, thr, "Ocurrió un error inactivando convenios");
                return false;
            }
        } else {
            return true;
        }

    }

    /**
     * Obtiene un tipo de convenio en base a una enumeracion de tipo de
     * convenio.
     *
     * @param tipoConvenio Enumeracion de tipo de convenio.
     * @return Tipo de convenio.
     */
    public TipoConvenio obtenerTipoConvenio(TipoConvenioEnum tipoConvenio) {
        return beans.getFacades().getTipoConvenioFacade().findByDescripcion(tipoConvenio.descripcion());
    }

    /**
     * Metodo de utilidad para obtener la configuracion de cargo determinado
     * para un convenio dado.
     *
     * @param idConvenio Convenio.
     * @param tipoCargo Tipo de cargo conocido de antemano.
     * @return Configuracion de cargo configurado, valga la redundancia. Caso
     * contrario {@code null}.
     */
    public Result<ConfigCargo> obtenerConfigCargo(Integer idConvenio, TipoCargoEnum tipoCargo) {
        Result<ConfigCargo> result;

        try {
            Integer idTipoCargo = beans.getTipoCargos().obtenerTipoCargo(tipoCargo).getId();
            ConfigCargo configCargo = beans.getFacades().getConfigCargoFacade().findForConvenio(idConvenio, idTipoCargo);
            result = Results.ok().value(configCargo).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error obteniendo configuración de cargo").build();
        }

        return result;
    }

    /**
     * Obtiene el valor numerico asociado a una configuracion de cargo.
     *
     * @param convenio Convenio.
     * @param tipoCargoEnum Tipo de cargo.
     * @param fallback Valor por defecto en caso de que no se pueda determinar
     * el valor concreto de la configuracion de cargo.
     * @return Valor de tipo de cargo.
     */
    public BigDecimal obtenerConfigCargo(Convenio convenio, TipoCargoEnum tipoCargoEnum, BigDecimal fallback) {
        Result<ConfigCargo> resConfigCargo = beans.getConvenios().obtenerConfigCargo(convenio.getId(), tipoCargoEnum);
        return beans.getCargos().obtenerValor(resConfigCargo.value(null), fallback);
    }

    /**
     * Obtiene el valor textual asociado a una configuracion de cargo.
     *
     * @param idConvenio Convenio.
     * @param tipoCargoEnum Tipo de cargo.
     * @param fallback Valor por defecto en caso de que no se pueda determinar
     * el valor concreto de la configuracion de cargo.
     * @return Valor de tipo de cargo.
     */
    public String obtenerConfigCargo(Integer idConvenio, TipoCargoEnum tipoCargoEnum, String fallback) {
        Result<ConfigCargo> resConfigCargo = beans.getConvenios().obtenerConfigCargo(idConvenio, tipoCargoEnum);
        return beans.getCargos().obtenerValor(resConfigCargo.value(null), fallback);
    }

    /**
     * Obtiene la coleccion de cargos asociados a un convenio, adaptados para
     * ser presentados en componentes de vista.
     *
     * @param idConvenio Convenio.
     * @return Coleccion de cargos aplicables.
     */
    public Collection<ConfigCargoView> obtenerConfigCargos(Integer idConvenio) {
        Collection<ConfigCargoView> cargos = new LinkedList<>();
        addConfigCargoView(cargos, idConvenio, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_COMPRADOR);
        addConfigCargoView(cargos, idConvenio, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_ENTIDAD_FINANCIERA);
        addConfigCargoView(cargos, idConvenio, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_SEPSA);
        addConfigCargoView(cargos, idConvenio, TipoCargoEnum.PORCENTAJE_IMPUESTO_COMISION);
        addConfigCargoView(cargos, idConvenio, TipoCargoEnum.PORCENTAJE_IMPONIBLE_GASTOS_OPERATIVOS);
        addConfigCargoView(cargos, idConvenio, TipoCargoEnum.PORCENTAJE_IMPUESTO_GASTOS_OPERATIVOS);
        return cargos;
    }

    public Result<Collection<Convenio>> obtenerConveniosEntidadFinanciera(Integer idEntidadFinanciera) {
        Result<Collection<Convenio>> result;

        try {
            Collection<Convenio> convenios = convenioFacade.findAllByIdEntidadFinanciera(idEntidadFinanciera);
            result = Results.ok()
                    .value(convenios)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo convenios para entidad_financiera=%s", idEntidadFinanciera)
                    .build();
        }

        return result;
    }

    /**
     * Obtiene una lista de todos los convenios donde exista el proveedor,
     * comprador y entidad dada. Idealmente, solo deberia existir un solo
     * convenio.
     *
     * @param idComprador Id de una persona comprador.
     * @param idProveedor Id de una persona proveedor.
     * @param idEntidadFinanciera Id de una persona entidad financiera.
     * @return Resultado de la operacion.
     */
    public Result<Collection<Convenio>> obtenerConvenios(Integer idComprador, Integer idProveedor, Integer idEntidadFinanciera) {
        Result<Collection<Convenio>> result;

        try {
            Collection<Convenio> convenios = obtenerConvenios0(idComprador, idProveedor, idEntidadFinanciera);
            result = Results.ok()
                    .value(convenios)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo convenio para comprador=%s proveedor=%s entidad_financiera=%s", idComprador, idProveedor, idEntidadFinanciera)
                    .build();
        }

        return result;
    }

    private Collection<Convenio> obtenerConvenios0(Integer idComprador, Integer idProveedor, Integer idEntidadFinanciera) {
        // Obtener convenios especificos entre comprador, proveedor, entidad.
        List<Convenio> convenios0 = convenioFacade.find(idComprador, idProveedor, idEntidadFinanciera);
        if (!Assertions.isNullOrEmpty(convenios0)) {
            return convenios0;
        }

        // Obtener convenios especificos entre cualquier comprador, proveedor y entidad.
        List<Convenio> convenios1 = convenioFacade.find(-1, idProveedor, idEntidadFinanciera);
        if (!Assertions.isNullOrEmpty(convenios1)) {
            return convenios1;
        }

        // Obtener convenios especificos entre cualquier comprador, proveedor y entidad.
        List<Convenio> convenios2 = convenioFacade.find(idComprador, -1, idEntidadFinanciera);
        if (!Assertions.isNullOrEmpty(convenios2)) {
            return convenios2;
        }

        // Obtener convenios especificos entre cualquier comprador, cualquier proveedor y entidad.
        List<Convenio> convenios3 = convenioFacade.find(-1, -1, idEntidadFinanciera);
        if (!Assertions.isNullOrEmpty(convenios3)) {
            return convenios3;
        }

        return fa.gs.utils.collections.Lists.empty();
    }

    public Result<Convenio> obtenerPorId(Integer idConvenio) {
        try {
            Convenio convenio = convenioFacade.findById(idConvenio);
            return Results.ok()
                    .value(convenio)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo convenio")
                    .tag("convenio.id", idConvenio)
                    .build();
        }
    }

    /**
     * Obtiene la informacion base de convenios. Utilizado solo para obtener y
     * desplegar datos en vistas de tipo tabla.
     *
     * @param limit Limit.
     * @param offset Offset.
     * @param filterBy Filtros.
     * @param orderBy Ordenacion.
     * @return Convenios.
     */
    public List<ConvenioInfo> selectConvenios(Integer limit, Integer offset, Map<String, Object> filterBy, Map<String, Order> orderBy) {
        List<ConvenioInfo> convenios = Lists.empty();

        try {
            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     c.id as \"convenio_id\", ");
            builder.append("     c.fecha as \"convenio_fecha_alta\", ");
            builder.append("     tc.id as \"convenio_tipo_id\", ");
            builder.append("     tc.descripcion as \"convenio_tipo_descripcion\", ");
            builder.append("     p0.id as \"comprador_id\", ");
            builder.append("     coalesce(pj0.razon_social, pf0.nombre || ' ' || pf0.apellido) as \"comprador_razon_social\", ");
            builder.append("     p1.id as \"proveedor_id\", ");
            builder.append("     coalesce(pj1.razon_social, pf1.nombre || ' ' || pf1.apellido) as \"proveedor_razon_social\", ");
            builder.append("     p2.id as \"entidad_financiera_id\", ");
            builder.append("     coalesce(pj2.razon_social, pf2.nombre || ' ' || pf2.apellido) as \"entidad_financiera_razon_social\" ");
            builder.append(" from factoring.convenio as c ");
            builder.append(" left join factoring.tipo_convenio as tc on tc.id = c.id_tipo_convenio  ");
            builder.append(" left join info.persona as p0 on c.id_comprador = p0.id  ");
            builder.append(" left join info.persona_fisica as pf0 on p0.id = pf0.id  ");
            builder.append(" left join info.persona_juridica as pj0 on p0.id = pj0.id  ");
            builder.append(" left join info.persona as p1 on c.id_proveedor = p1.id  ");
            builder.append(" left join info.persona_fisica as pf1 on p1.id = pf1.id  ");
            builder.append(" left join info.persona_juridica as pj1 on p1.id = pj1.id  ");
            builder.append(" left join info.persona as p2 on c.id_entidad_financiera = p2.id  ");
            builder.append(" left join info.persona_fisica as pf2 on p2.id = pf2.id  ");
            builder.append(" left join info.persona_juridica as pj2 on p2.id = pj2.id  ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and c.estado = 'A' ");
            if (!Assertions.isNullOrEmpty(filterBy)) {
                filterBy.entrySet().stream().forEach(e -> {
                    builder.append(" and %s ", String.valueOf(e.getValue()));
                });
            }
            builder.append(" order by c.fecha desc ");
            builder.append(" limit %s ", limit);
            builder.append(" offset %s ", offset);
            builder.append(" ; ");

            // Ejecutar query.
            String query = builder.toString();
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Agregar registros.
            if (!Assertions.isNullOrEmpty(rows)) {
                rows.stream()
                        .map(ConvenioInfo::instance)
                        .forEach(convenios::add);
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando convenios");
        }

        return convenios;
    }

    /**
     * Obtiene la cantidad de convenios disponibles, en su forma base. Este
     * metodo es analogo al metodo de seleccion de datos
     * {@link #selectConvenios(java.lang.Integer, java.lang.Integer, java.util.Map, java.util.Map) selectConvenios}.
     *
     * @param filterBy Filtros.
     * @return Convenios.
     */
    public Integer countConvenios(Map<String, Object> filterBy) {
        try {
            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     cast(count(*) as int8) as \"total\" ");
            builder.append(" from factoring.convenio as c ");
            builder.append(" left join factoring.tipo_convenio as tc on tc.id = c.id_tipo_convenio  ");
            builder.append(" left join info.persona as p0 on c.id_comprador = p0.id  ");
            builder.append(" left join info.persona_fisica as pf0 on p0.id = pf0.id  ");
            builder.append(" left join info.persona_juridica as pj0 on p0.id = pj0.id  ");
            builder.append(" left join info.persona as p1 on c.id_proveedor = p1.id  ");
            builder.append(" left join info.persona_fisica as pf1 on p1.id = pf1.id  ");
            builder.append(" left join info.persona_juridica as pj1 on p1.id = pj1.id  ");
            builder.append(" left join info.persona as p2 on c.id_entidad_financiera = p2.id  ");
            builder.append(" left join info.persona_fisica as pf2 on p2.id = pf2.id  ");
            builder.append(" left join info.persona_juridica as pj2 on p2.id = pj2.id  ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and c.estado = 'A' ");
            if (!Assertions.isNullOrEmpty(filterBy)) {
                filterBy.entrySet().stream().forEach(e -> {
                    builder.append(" and %s ", String.valueOf(e.getValue()));
                });
            }
            builder.append(" ; ");

            // Ejecutar query.
            String query = builder.toString();
            Map<String, Object> row = jpaUtils.getFirst(query);

            BigInteger count = Maps.biginteger(row, "total");
            return count.intValue();
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando convenios");
            return -1;
        }
    }

    /**
     * Retorna la lista de compradores que tienen convenio con el usuario
     * logueado.
     *
     * @return Lista de compradores conveniados.
     */
    public Collection<Persona> obtenerCompradores(Collection<Convenio> convenios) {
        // Control.
        if (Assertions.isNullOrEmpty(convenios)) {
            return Lists.empty();
        }

        // Determinar si existe algun convenio que contemple a todos los compradores.
        boolean useAll = convenios.stream().anyMatch(c -> c.getIdComprador() == -1);
        if (useAll) {
            Result<Collection<Persona>> result = personas.obtenerCompradoresDisponibles();
            return result.value(Lists.empty());
        }

        // Retornar compradores especificados.
        Integer[] idCompradores = convenios.stream().map(c -> c.getIdComprador()).toArray(Integer[]::new);
        Result<Collection<Persona>> result = personas.obtenerPorIds(idCompradores);
        return result.value(Lists.empty());
    }

    /**
     * Retorna la lista de proveedores que tienen convenio con el usuario
     * logueado.
     *
     * @return Lista de proveedores conveniados.
     */
    public Collection<Persona> obtenerProveedores(Collection<Convenio> convenios) {
        // Control.
        if (Assertions.isNullOrEmpty(convenios)) {
            return Lists.empty();
        }

        // Determinar si existe algun convenio que contemple a todos los proveedores.
        boolean useAll = convenios.stream().anyMatch(c -> c.getIdProveedor() == -1);
        if (useAll) {
            Result<Collection<Persona>> result = personas.obtenerProveedoresDisponibles();
            return result.value(Lists.empty());
        }

        // Retornar proveedores especificados.
        Integer[] idProveedores = convenios.stream().map(c -> c.getIdProveedor()).toArray(Integer[]::new);
        Result<Collection<Persona>> result = personas.obtenerPorIds(idProveedores);
        return result.value(Lists.empty());
    }

    /**
     * Retorna la lista de entidades financieras que tienen convenio con el
     * usuario logueado.
     *
     * @return Lista de entidades financieras conveniados.
     */
    public Collection<Persona> obtenerEntidadesFinancieras(Integer idPersona, Collection<Convenio> convenios) {
        // Control.
        if (Assertions.isNullOrEmpty(convenios)) {
            return Lists.empty();
        }

        // Determinar si existe algun convenio que contemple a todos las entidades financieras.
        boolean useAll = convenios.stream().anyMatch(c -> c.getIdEntidadFinanciera() == -1);
        if (useAll) {
            Result<Collection<Persona>> result = personas.obtenerEntidadesFinancierasDisponibles(idPersona);
            return result.value(Lists.empty());
        }

        // Retornar entidades especificadas.
        Integer[] idProveedores = convenios.stream().map(c -> c.getIdEntidadFinanciera()).toArray(Integer[]::new);
        Result<Collection<Persona>> result = personas.obtenerPorIds(idProveedores);
        return result.value(Lists.empty());
    }

    /**
     * Registra un nuevo convenio.
     *
     * @param usuario Usuario que crea el convenio.
     * @param idComprador Comprador asignado.
     * @param idProveedor Proveedor asignado.
     * @param idEntidadFinanciera Entidad financiera asignada.
     * @param tipoConvenio Tipo de convenio.
     * @return Resultado de la operacion.
     */
    public Result<Convenio> registrarConvenio(Usuario usuario, Integer idComprador, Integer idProveedor, Integer idEntidadFinanciera, TipoConvenioEnum tipoConvenio) {
        Result<Convenio> result;
        try {
            Convenio convenio = new Convenio();
            convenio.setIdTipoConvenio(obtenerTipoConvenio(tipoConvenio).getId());
            convenio.setEstado('A');
            convenio.setIdUsuario(usuario.getId());
            convenio.setFecha(Fechas.ahora());
            convenio.setIdComprador(idComprador);
            convenio.setIdProveedor(idProveedor);
            convenio.setIdEntidadFinanciera(idEntidadFinanciera);

            beans.getFacades().getConvenioFacade().create(convenio);
            result = Results.ok().value(convenio).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error registrando convenio").build();
        }
        return result;
    }

    /**
     * Agrega una adaptacion de cargo a una coleccion existente, siempre que el
     * cargo este asociadoa un convenio dado.
     *
     * @param views Coleccion de cargos adaptados para su utilizacion en vistas.
     * @param convenio Convenio.
     * @param cargoEnum Enumeracion de tipo de cargo a adaptar.
     */
    private void addConfigCargoView(Collection<ConfigCargoView> views, Integer idConvenio, TipoCargoEnum cargoEnum) {
        ConfigCargoView view;
        try {
            // Obtener configuracion de cargo.
            Result<ConfigCargo> resCargo = obtenerConfigCargo(idConvenio, cargoEnum);
            resCargo.raise();

            // Generar representacion para vista.
            ConfigCargo cargo = resCargo.value();
            if (cargo == null) {
                view = new ConfigCargoView(cargoEnum.descripcionCorta(), cargoEnum.descripcionLarga(), "N/D");
            } else {
                String valor = beans.getTipoValores().obtenerValorFormateado(cargo);
                view = new ConfigCargoView(cargoEnum.descripcionCorta(), cargoEnum.descripcionLarga(), valor);
            }
        } catch (Throwable thr) {
            view = new ConfigCargoView(cargoEnum.descripcionCorta(), cargoEnum.descripcionLarga(), "N/D");
        }
        views.add(view);
    }

}
