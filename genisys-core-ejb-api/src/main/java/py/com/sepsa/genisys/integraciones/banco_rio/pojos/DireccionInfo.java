/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio.pojos;

import java.io.Serializable;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioCiudad;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioRegion;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class DireccionInfo implements Serializable {

    private String calle1;
    private String calle2;
    private String referencia;
    private String nroCalle;
    private String calle3;
    private String barrio;
    private BancoRioCiudad ciudad;
    private BancoRioRegion region;
}
