/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.criteria.query.Operators;
import fa.gs.criteria.query.QueryCriteria;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.misc.errors.Errors;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.Archivo;
import py.com.sepsa.genisys.ejb.entities.factoring.ArchivoAdjunto;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoArchivo;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.enums.TipoArchivoEnum;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.Fechas;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Archivos", mappedName = "Archivos")
@LocalBean
public class Archivos extends LogicBean {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Directorio raiz en el cual seran almacenados los archivos adjuntos.
     */
    private static final String BASE_PATH = "/opt/genisys/files";

    /**
     * Formato para la asignacion de nombres para directorios.
     */
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    //</editor-fold>

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        ;
    }

    /**
     * Permite obtener la informacion de los archivos que fueron adjuntados a
     * una entidad dada. Por ejemplo una factura, una linea de credito, etc.
     *
     * @param idEntidad Identificador de entidad.
     * @param tipoEntidad Descriptor de tipo de entidad.
     * @return Resultado de la operacion.
     */
    public Result<Collection<Archivo>> buscarArchivosAdjuntos(Integer idEntidad, TipoEntidadArchivoAdjuntoEnum tipoEntidad) {
        try {
            /**
             * Se obtienen los identificadores de cada archivo adjunto a una
             * entidad y posteriormente se realiza una consulta para obtener las
             * entidades propias de archivo. No tengo una explicacion exacta del
             * por que esto funciona y no la relacion de join dentro de la
             * entidad ArchivoAjunto.
             */
            Integer[] ids = beans.getFacades().getArchivoAdjuntoFacade().findByIdEntidad(idEntidad, tipoEntidad.descripcion)
                    .stream()
                    .map((fa) -> fa.getIdArchivo())
                    .toArray(Integer[]::new);

            // Controlar si hay registros.
            if (ids == null || ids.length <= 0) {
                return Results.ok().value(Collections.emptyList()).build();
            }

            // Buscar registros.
            QueryCriteria criteria = QueryCriteria.instance();
            criteria.where("id", Operators.IN, ids);
            Collection<Archivo> archivos = beans.getFacades().getArchivoFacade().find(criteria);
            return Results.ok().value(archivos).build();
        } catch (Throwable thr) {
            return Results.ko().cause(thr).message("Ocurrió un error obteniendo archivos adjuntos de entidad id='%s' tipo='%s'", idEntidad, tipoEntidad.descripcion).build();
        }
    }

    /**
     * Permite relacionar una entidad con un archivo fisico adjunto.
     *
     * @param idEntidad Identificador de entidad a la que se desea adjuntar el
     * archivo.
     * @param tipoEntidad Descriptor de tipo de entidad.
     * @param usuario Usuario que realiza la accion de adjuntar archivos.
     * @param perfil Perfil actual del usuario que realiza la accion de adjuntar
     * archivos.
     * @param cacheArchivo Archivos en cache que se desea adjuntar.
     * @param tipoArchivo Descriptor de tipo de archivo.
     * @return Resultado de la operacion.
     * @throws Exception Si ocurre algun error al momento de persistir el
     * archivo en cache en el almacenamiento fisico.
     */
    public Result<Archivo> adjuntarArchivo(Integer idEntidad, TipoEntidadArchivoAdjuntoEnum tipoEntidad, Usuario usuario, Perfil perfil, Archivos.CacheArchivo cacheArchivo, TipoArchivoEnum tipoArchivo) throws Exception {
        // Persistir bytes desde memoria a disco.
        Result<File> resPersistir = persistir(cacheArchivo.getContenido());
        if (!resPersistir.isSuccess()) {
            return Results.ko().cause(resPersistir.failure().cause()).message("Ocurrió un error persistiendo archivo").build();
        }

        // Adjuntar archivo persistido a factura.
        return adjuntarArchivo(idEntidad, tipoEntidad, usuario, perfil, resPersistir.value(), cacheArchivo.getNombre(), tipoArchivo);
    }

    /**
     * Permite relacionar una entidad con un archivo fisico adjunto.
     *
     * @param idEntidad Identificador de entidad a la que se desea adjuntar el
     * archivo.
     * @param tipoEntidad Descriptor de tipo de entidad.
     * @param usuario Usuario que realiza la accion de adjuntar archivos.
     * @param perfil Perfil actual del usuario que realiza la accion de adjuntar
     * archivos.
     * @param file Archivo en almacenamiento fisico.
     * @param nombreArchivo Nombre de archivo.
     * @param tipoArchivo Descriptor de tipo de archivo.
     * @return Resultado de la operacion.
     */
    public Result<Archivo> adjuntarArchivo(Integer idEntidad, TipoEntidadArchivoAdjuntoEnum tipoEntidad, Usuario usuario, Perfil perfil, File file, String nombreArchivo, TipoArchivoEnum tipoArchivo) {
        Result<Archivo> result;
        try {
            // Calcular hash de archivo.
            Result<String> resHash = hash(file);
            resHash.raise();

            // Determinar tipo de archivo.
            Result<TipoArchivo> resTipoArchivo = beans.getTipoArchivos().obtenerTipoArchivo(tipoArchivo);
            resTipoArchivo.raise();

            // Crear registro de archivo.
            Archivo archivo = new Archivo();
            archivo.setIdUsuario(usuario.getId());
            archivo.setIdPerfil(perfil);
            archivo.setNombre(nombreArchivo);
            archivo.setIdTipoArchivo(resTipoArchivo.value());
            archivo.setPath(file.getAbsolutePath());
            archivo.setHash(resHash.value());
            archivo.setFecha(Fechas.ahora());
            beans.getFacades().getArchivoFacade().create(archivo);

            // Asociar archivo creado con factura.
            ArchivoAdjunto archivoAdjunto = new ArchivoAdjunto();
            archivoAdjunto.setIdArchivo(archivo.getId());
            archivoAdjunto.setIdEntidad(idEntidad);
            archivoAdjunto.setTipoEntidad(tipoEntidad.descripcion);
            beans.getFacades().getArchivoAdjuntoFacade().create(archivoAdjunto);

            result = Results.ok().value(archivo).build();
        } catch (Throwable thr) {
            thr.printStackTrace(System.err);
            result = Results.ko().cause(thr).message("Ocurrió un error adjuntando archivo '%s' a entidad id='%s' tipo='%s'", nombreArchivo, idEntidad, tipoEntidad.descripcion).build();
        }
        return result;
    }

    //<editor-fold defaultstate="collapsed" desc="Utilidades">
    /**
     * Transfiere un array de bytes a disco.
     *
     * @param contenido Bytes.
     * @return Resultado de la operacion.
     */
    public Result<File> persistir(ByteBuffer contenido) {
        Result<File> result;
        try {
            // Determinar directorio donde sera almacenado el archivo.
            Result<File> resParentDir = resolveParentDir();
            resParentDir.raise();

            // Escribir bytes a disco.
            File file = new File(resParentDir.value(), generateUuid());
            try (FileChannel fileChannel = new FileOutputStream(file).getChannel()) {
                fileChannel.write(contenido);
            }

            result = Results.ok().value(file).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error persistiendo contenido de archivo").build();
        }
        return result;
    }

    /**
     * Retorna una referencia al directorio padre donde son almacenados los
     * archivos.
     *
     * @return Resultado de la operacion.
     */
    private Result<File> resolveParentDir() {
        Result<File> result;
        try {
            String timestamp = sdf.format(Fechas.ahora());
            Path path = Paths.get(BASE_PATH, timestamp);
            File dir = path.toFile();
            if (!dir.exists()) {
                boolean ok = dir.mkdirs();
                if (!ok) {
                    throw Errors.illegalState("No se pudo crear carpeta '%s'", dir.getAbsolutePath());
                }
            }

            result = Results.ok().value(dir).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error determinando directorio padre para persistencia de archivo").build();
        }
        return result;
    }

    /**
     * Genera un identificador teoricamente unico que puede ser asignado a un
     * archvivo.
     *
     * @return Cadena con identificador unico.
     */
    private String generateUuid() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replace("-", "");
    }

    /**
     * Aplica el algoritmo de resumen MD5 al contenido de un archivo.
     *
     * @param file Referencia a archivo.
     * @return Resultado de la operacion.
     */
    public Result<String> hash(File file) {
        return hash(file, "MD5");
    }

    /**
     * Aplica un algoritmo de resumen (hash) al contenido de un archivo.
     *
     * @param file Referencia a archivo.
     * @param algoritmo Nombre de algoritmo especifico a utilizar.
     * @return Resultado de la operacion.
     */
    public Result<String> hash(File file, String algoritmo) {
        Result<String> result;
        try {
            // Aplicar algoritmo de hash.
            MessageDigest md = MessageDigest.getInstance(algoritmo);
            FileInputStream fis = new FileInputStream(file);
            byte[] chunk = new byte[1024];
            int read = 0;
            while ((read = fis.read(chunk)) != -1) {
                md.update(chunk, 0, read);
            }

            // Obtener bytes de resumen.
            byte[] hash = md.digest();

            // Convertir bytes en cadena hexadecimal.
            StringBuilder sb = new StringBuilder("");
            for (int i = 0; i < hash.length; i++) {
                sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
            }

            result = Results.ok().value(sb.toString()).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error calculando hash de archivo '%s' utilizando algoritmo '%s'", file.getAbsolutePath(), algoritmo).build();
        }

        return result;
    }
    //</editor-fold>

    public enum TipoEntidadArchivoAdjuntoEnum {
        FACTURA("F"),
        LINEA_CREADITO("LC");

        //<editor-fold defaultstate="collapsed" desc="Atributos">
        private final String descripcion;
        //</editor-fold>

        public String descripcion() {
            return descripcion;
        }

        TipoEntidadArchivoAdjuntoEnum(String descripcion) {
            this.descripcion = descripcion;
        }
    }

    public static class CacheArchivo {

        //<editor-fold defaultstate="collapsed" desc="Atributos">
        /**
         * Nombre del archivo.
         */
        private final String nombre;

        /**
         * Bytes de contenido.
         */
        private final ByteBuffer contenido;
        //</editor-fold>

        /**
         * Constructor.
         *
         * @param nombre Nombre del archivo.
         * @param contenido Contenido del archivo.
         */
        public CacheArchivo(String nombre, ByteBuffer contenido) {
            this.nombre = nombre;
            this.contenido = contenido;
        }

        //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
        public String getNombre() {
            return nombre;
        }

        public ByteBuffer getContenido() {
            return contenido;
        }
        //</editor-fold>
    }
}
