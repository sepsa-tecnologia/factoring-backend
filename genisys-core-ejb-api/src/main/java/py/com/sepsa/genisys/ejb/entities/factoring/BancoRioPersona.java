/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.Type;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
@Entity
@Table(name = "banco_rio_persona", schema = "factoring")
@NamedQueries({
    @NamedQuery(name = "BancoRioPersona.obtenerPorIds", query = "SELECT bp FROM BancoRioPersona bp WHERE bp.idPersona = :idPersona and bp.idPerfil = :idPerfil")
})
public class BancoRioPersona implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_persona")
    private Integer idPersona;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_perfil")
    private Integer idPerfil;

    @Basic(optional = true)
    @Column(name = "codigo_cliente")
    private String codigoCliente;

    @Basic(optional = true)
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    @Basic(optional = true)
    @Column(name = "img_documento", columnDefinition = "bytea")
    private byte[] imgDocumento;

    @Basic(optional = true)
    @Column(name = "id_ciudad")
    private Integer idCiudad;

    @Basic(optional = true)
    @Column(name = "id_region")
    private Integer idRegion;

    @Basic(optional = true)
    @Column(name = "id_actividad_economica")
    private Integer idActividadEconomica;

    @Basic(optional = true)
    @Column(name = "id_tipo_sociedad")
    private Integer idTipoSociedad;

    @Basic(optional = true)
    @Column(name = "fecha_constitucion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaConstitucion;

    @Basic(optional = true)
    @Column(name = "facturacion_promedio")
    private BigDecimal facturacionPromedio;

    @Basic(optional = true)
    @Column(name = "facturacion_periodo")
    private Integer facturacionPeriodo;

    @Basic(optional = true)
    @Column(name = "id_persona_referencia")
    private Integer idPersonaReferencia;

    @Basic(optional = true)
    @Column(name = "id_banco")
    private Integer idBanco;

    @Basic(optional = true)
    @Column(name = "nro_cuenta")
    private String nroCuenta;

}
