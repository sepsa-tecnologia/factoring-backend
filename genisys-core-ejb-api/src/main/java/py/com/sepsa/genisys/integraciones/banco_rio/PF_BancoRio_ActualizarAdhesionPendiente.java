/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Units;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioPersona;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoDocumento;
import py.com.sepsa.genisys.ejb.entities.factoring.PersonaEntidad;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Entidades;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_ActualizarAdhesionPendiente", mappedName = "PF_BancoRio_ActualizarAdhesionPendiente")
@LocalBean
public class PF_BancoRio_ActualizarAdhesionPendiente implements Serializable {

    private AppLogger log;

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Personas personas;

    @EJB
    private Entidades entidades;

    @EJB
    private Perfiles perfiles;

    @EJB
    private Notificaciones notificaciones;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    public Entidades.EstadoAdhesionEntidad work(PersonaEntidad adhesion) throws Throwable {
        Integer idPersona = adhesion.getIdPersona();
        Integer idPerfil = adhesion.getIdPerfil();
        Integer idEntidadFinanciera = adhesion.getIdEntidadFinanciera();
        Integer idPerfilComprador = perfiles.obtenerPerfil(PerfilEnum.COMPRADOR).getId();

        // Obtener registro de proveedor.
        Result<Persona> resPersona = personas.obtenerPorId(idPersona);
        Persona personaProveedor = resPersona.value();

        // Obtener registro de proveedor con banco.
        Result<BancoRioPersona> resPersonaBancoRio = bancoRio.obtenerPersona(idPersona, idPerfil);
        if (resPersonaBancoRio.isFailure()) {
            log.error(resPersonaBancoRio.failure().cause(), resPersonaBancoRio.failure().message());
            throw Errors.illegalState(resPersonaBancoRio.failure().cause(), "No se pudo determinar la información de proveedor registrado para con la entidad financiera");
        }

        // Persona proveedor.
        BancoRioPersona personaProveedorBanco = resPersonaBancoRio.value(null);

        // Obtener tipo de documento de proveedor
        Result<BancoRioTipoDocumento> resTipoDocumento = bancoRio.obtenerTipoDocumento(personaProveedorBanco.getIdTipoDocumento());
        if (resTipoDocumento.isFailure()) {
            log.error(resTipoDocumento.failure().cause(), resTipoDocumento.failure().message());
            throw Errors.illegalState(resTipoDocumento.failure().cause(), "No se pudo determinar el tipo de documento del proveedor registrado para con la entidad financiera");
        }

        // Tipo de documento de proveedor.
        BancoRioTipoDocumento tipoDocumentoProveedor = resTipoDocumento.value();

        // Obtener registro de proveedor.
        resPersona = personas.obtenerPorId(personaProveedorBanco.getIdPersonaReferencia());
        Persona personaComprador = resPersona.value();

        // Obtener registro de comprador con banco.
        resPersonaBancoRio = bancoRio.obtenerPersona(personaProveedorBanco.getIdPersonaReferencia(), idPerfilComprador);
        if (resPersonaBancoRio.isFailure()) {
            log.error(resPersonaBancoRio.failure().cause(), resPersonaBancoRio.failure().message());
            throw Errors.illegalState(resPersonaBancoRio.failure().cause(), "No se pudo determinar la información de comprador registrado para con la entidad financiera");
        }

        // Persona comprador.
        BancoRioPersona personaCompradorBanco = resPersonaBancoRio.value(null);

        // Obtener tipo de documento de comprador
        resTipoDocumento = bancoRio.obtenerTipoDocumento(personaCompradorBanco.getIdTipoDocumento());
        if (resTipoDocumento.isFailure()) {
            log.error(resTipoDocumento.failure().cause(), resTipoDocumento.failure().message());
            throw Errors.illegalState(resTipoDocumento.failure().cause(), "No se pudo determinar el tipo de documento del comprador registrado para con la entidad financiera");
        }

        // Tipo de documento de comprador.
        BancoRioTipoDocumento tipoDocumentoComprador = resTipoDocumento.value();

        // Preparar parametros.
        ConsultaEstadoProveedorInput input = new ConsultaEstadoProveedorInput();
        input.setProveedor(personaProveedor);
        input.setProveedorDocumento(new DocumentoInfo());
        input.getProveedorDocumento().setTipoDocumento(tipoDocumentoProveedor);
        input.getProveedorDocumento().setNroDocumento(Text.ruc(personaProveedor));
        input.setComprador(personaComprador);
        input.setCompradorDocumento(new DocumentoInfo());
        input.getCompradorDocumento().setTipoDocumento(tipoDocumentoComprador);
        input.getCompradorDocumento().setNroDocumento(Text.ruc(personaComprador));

        // Consumir servicio.
        Result<ConsultaEstadoProveedorOutput> resConsulta = bancoRio.consultarEstadoProveedor(idPersona, idPerfil, idEntidadFinanciera, input);
        if (resConsulta.isFailure()) {
            throw Errors.illegalState(resConsulta.failure().cause(), resConsulta.failure().message());
        }

        // Actualizar estado de adhesion.
        ConsultaEstadoProveedorOutput output = resConsulta.value();
        if (output.getRespuesta().isOk()) {
            Integer idPersonaEntidad = adhesion.getId();
            Result<Void> resActualizacion;

            // Determinar estado.
            Entidades.EstadoAdhesionEntidad estadoAdhesion = resolveEstadoAdhesion(output.getRespuesta());

            // Actualizar estado de solicitud.
            resActualizacion = entidades.actualizarEstadoAdhesionEntidad(idPersonaEntidad, estadoAdhesion);
            if (resActualizacion.isFailure()) {
                log.error(resActualizacion.failure().cause(), resActualizacion.failure().message());
                throw Errors.illegalState(resActualizacion.failure().cause(), "No se pudo actualizar el estado de la solicitud de adhesión.");
            }

            // Actualizar codigo de cliente de persona si adhesion fue exitosa.
            if (estadoAdhesion == Entidades.EstadoAdhesionEntidad.ACTIVO) {
                resActualizacion = bancoRio.actualizarCodCliente(idPersona, idPerfil, output.getRespuesta().getCodCliente());
                if (resActualizacion.isFailure()) {
                    log.error(resActualizacion.failure().cause(), resActualizacion.failure().message());
                    throw Errors.illegalState(resActualizacion.failure().cause(), "No se pudo actualizar el código del cliente.");
                }
            }
        }

        // Estado final.
        Result<Entidades.EstadoAdhesionEntidad> resEstado = entidades.obtenerEstadoAdhesion(idPersona, idPerfil, idEntidadFinanciera);
        resEstado.raise();
        return resEstado.value();
    }

    private Entidades.EstadoAdhesionEntidad resolveEstadoAdhesion(RespuestaConsultaEstadoProveedor respuesta) {
        String msg = Units.execute("", () -> respuesta.getEstado().toUpperCase().trim());

        if (msg.startsWith("APROBAD")) {
            return py.com.sepsa.genisys.ejb.logic.impl.Entidades.EstadoAdhesionEntidad.ACTIVO;
        }

        if (msg.startsWith("RECHAZAD")) {
            return py.com.sepsa.genisys.ejb.logic.impl.Entidades.EstadoAdhesionEntidad.RECHAZADO;
        }

        return py.com.sepsa.genisys.ejb.logic.impl.Entidades.EstadoAdhesionEntidad.PENDIENTE;
    }

}
