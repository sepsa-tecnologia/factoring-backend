/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.Personalizacion;
import py.com.sepsa.genisys.ejb.enums.TipoPersonalizacionEnum;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;
import py.com.sepsa.genisys.ejb.utils.Persistence;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Personalizaciones", mappedName = "Personalizaciones")
@LocalBean
public class Personalizaciones extends LogicBean {

    @PersistenceContext(unitName = Persistence.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    public Result<Personalizacion> obtenerPorTipo(Integer idPersona, TipoPersonalizacionEnum tipo) {
        Result<Personalizacion> result;

        try {
            Query q = em.createNamedQuery("Personalizacion.findByPersonaTipo");
            q.setParameter("idPersona", idPersona);
            q.setParameter("tipo", tipo.codigo());

            Personalizacion personalizacion = JpaUtils.getFirst(q);
            result = Results.ok()
                    .value(personalizacion)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo valor de personalizacion")
                    .tag("personalizacion.codigo", tipo.codigo())
                    .build();
        }

        return result;
    }

}
