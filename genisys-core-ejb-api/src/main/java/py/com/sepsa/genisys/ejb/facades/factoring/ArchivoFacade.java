/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.Archivo;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author Alexander Triana Ríos
 */
@Stateless(name = "ArchivoFacade", mappedName = "ArchivoFacade")
@LocalBean
public class ArchivoFacade extends AbstractFacade<Archivo> {

    /**
     * Constructor.
     */
    public ArchivoFacade() {
        super(Archivo.class);
    }
}
