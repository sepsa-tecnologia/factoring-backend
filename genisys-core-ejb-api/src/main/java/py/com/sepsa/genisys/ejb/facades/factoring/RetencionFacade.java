/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.Retencion;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "RetencionFacade", mappedName = "RetencionFacade")
@LocalBean
public class RetencionFacade extends AbstractFacade<Retencion> {

    /**
     * Constructor.
     */
    public RetencionFacade() {
        super(Retencion.class);
    }

    /**
     * Permite recuperar la retención asociada a una factura dada.
     *
     * @param idFactura Identificador de factura.
     * @return Retención asociada a la factura, si hubiere. Null caso contrario.
     */
    public Retencion findByIdFactura(Integer idFactura) {
        Query q = getEntityManager().createNamedQuery("Retencion.findByIdFactura");
        q.setParameter("idFactura", idFactura);
        return JpaUtils.getFirst(q);
    }

}
