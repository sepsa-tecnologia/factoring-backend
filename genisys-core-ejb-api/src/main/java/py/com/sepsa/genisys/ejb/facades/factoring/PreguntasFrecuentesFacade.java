/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.PreguntaFrecuente;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PreguntasFrecuentesFacade", mappedName = "PreguntasFrecuentesFacade")
@LocalBean
public class PreguntasFrecuentesFacade extends AbstractFacade<PreguntaFrecuente> {

    /**
     * Constructor.
     */
    public PreguntasFrecuentesFacade() {
        super(PreguntaFrecuente.class);
    }

    public List<PreguntaFrecuente> findAll() {
        Query q = getEntityManager().createNamedQuery("PreguntaFrecuente.findAllSorted");
        return JpaUtils.getAll(q);
    }
}
