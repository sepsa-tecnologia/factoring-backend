/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.trans;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.trans.Rol;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author descauriza
 */
@Stateless(name = "RolFacade", mappedName = "RolFacade")
@LocalBean
public class RolFacade extends AbstractFacade<Rol> {

    public RolFacade() {
        super(Rol.class);
    }

}
