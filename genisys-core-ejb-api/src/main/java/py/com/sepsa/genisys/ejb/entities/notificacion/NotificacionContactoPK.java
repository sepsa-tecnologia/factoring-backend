/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.notificacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Fabio A. González Sosa
 */
@Embeddable
public class NotificacionContactoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_notificacion")
    private int idNotificacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_contacto")
    private int idContacto;

    public NotificacionContactoPK() {
    }

    public NotificacionContactoPK(int idNotificacion, int idContacto) {
        this.idNotificacion = idNotificacion;
        this.idContacto = idContacto;
    }

    public int getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(int idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public int getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idNotificacion;
        hash += (int) idContacto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof NotificacionContactoPK)) {
            return false;
        }
        NotificacionContactoPK other = (NotificacionContactoPK) object;
        if (this.idNotificacion != other.idNotificacion) {
            return false;
        }
        if (this.idContacto != other.idContacto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.notificacion.NotificacionContactoPK[ idNotificacion=" + idNotificacion + ", idContacto=" + idContacto + " ]";
    }

}
