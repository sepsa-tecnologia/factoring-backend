/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum BancoRioEstadoPeticionEnum {
    OK("0001"),
    KO("0002"),
    KO_TIMEOUT("0003");
    private final String codigo;

    private BancoRioEstadoPeticionEnum(String codigo) {
        this.codigo = codigo;
    }

    public String codigo() {
        return codigo;
    }

}
