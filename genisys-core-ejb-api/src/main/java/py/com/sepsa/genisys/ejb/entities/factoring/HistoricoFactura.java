/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "historico_factura", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistoricoFactura.findAll", query = "SELECT h FROM HistoricoFactura h"),
    @NamedQuery(name = "HistoricoFactura.findById", query = "SELECT h FROM HistoricoFactura h WHERE h.id = :id"),
    @NamedQuery(name = "HistoricoFactura.findByIdFactura", query = "SELECT h FROM HistoricoFactura h WHERE h.idFactura.id = :idFactura ORDER BY h.fecha ASC"),
    @NamedQuery(name = "HistoricoFactura.findByIdUsuario", query = "SELECT h FROM HistoricoFactura h WHERE h.idUsuario = :idUsuario ORDER BY h.fecha ASC"),
    @NamedQuery(name = "HistoricoFactura.findByComentario", query = "SELECT h FROM HistoricoFactura h WHERE h.comentario = :comentario"),
    @NamedQuery(name = "HistoricoFactura.findByFecha", query = "SELECT h FROM HistoricoFactura h WHERE h.fecha = :fecha ORDER BY h.fecha ASC")})
public class HistoricoFactura implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_usuario")
    private int idUsuario;
    @Size(max = 2147483647)
    @Column(name = "comentario")
    private String comentario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "id_estado", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Estado idEstado;
    @JoinColumn(name = "id_factura", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Factura idFactura;

    public HistoricoFactura() {
    }

    public HistoricoFactura(Integer id) {
        this.id = id;
    }

    public HistoricoFactura(Integer id, int idUsuario, Date fecha) {
        this.id = id;
        this.idUsuario = idUsuario;
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Estado getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estado idEstado) {
        this.idEstado = idEstado;
    }

    public Factura getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Factura idFactura) {
        this.idFactura = idFactura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof HistoricoFactura)) {
            return false;
        }
        HistoricoFactura other = (HistoricoFactura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura[ id=" + id + " ]";
    }

}
