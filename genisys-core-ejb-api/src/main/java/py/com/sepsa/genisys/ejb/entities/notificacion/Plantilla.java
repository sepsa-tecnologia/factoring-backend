/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.notificacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "plantilla", schema = "notificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Plantilla.findAll", query = "SELECT p FROM Plantilla p"),
    @NamedQuery(name = "Plantilla.findById", query = "SELECT p FROM Plantilla p WHERE p.id = :id"),
    @NamedQuery(name = "Plantilla.findByIdTipoDocumento", query = "SELECT p FROM Plantilla p WHERE p.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "Plantilla.findByAsunto", query = "SELECT p FROM Plantilla p WHERE p.asunto = :asunto"),
    @NamedQuery(name = "Plantilla.findByCuerpo", query = "SELECT p FROM Plantilla p WHERE p.cuerpo = :cuerpo"),
    @NamedQuery(name = "Plantilla.findByAsuntoCuerpo", query = "SELECT p FROM Plantilla p WHERE p.asunto = :asunto and p.cuerpo = :cuerpo")})
public class Plantilla implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;
    @Size(max = 2147483647)
    @Column(name = "asunto")
    private String asunto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "cuerpo")
    private String cuerpo;
    @Column(name = "id_momento_notificacion")
    private Integer idMomentoNotificacion;
    @Column(name = "id_tipo_notificacion")
    private Integer idTipoNotificacion;

    public Plantilla() {
    }

    public Plantilla(Integer id) {
        this.id = id;
    }

    public Plantilla(Integer id, String cuerpo) {
        this.id = id;
        this.cuerpo = cuerpo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public Integer getIdMomentoNotificacion() {
        return idMomentoNotificacion;
    }

    public void setIdMomentoNotificacion(Integer idMomentoNotificacion) {
        this.idMomentoNotificacion = idMomentoNotificacion;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Plantilla)) {
            return false;
        }
        Plantilla other = (Plantilla) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.notificacion.Plantilla[ id=" + id + " ]";
    }

}
