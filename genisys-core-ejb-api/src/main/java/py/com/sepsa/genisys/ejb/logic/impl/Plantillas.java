/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import java.util.Objects;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.notificacion.Plantilla;
import py.com.sepsa.genisys.ejb.enums.TipoPlantilla;
import py.com.sepsa.genisys.ejb.facades.notificacion.PlantillaFacade;
import py.com.sepsa.genisys.ejb.logic.LogicBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Plantillas", mappedName = "Plantillas")
@LocalBean
public class Plantillas extends LogicBean {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Texto general para agrupar todas las plantillas de notificacion para
     * factoring. Ver tabla {@code notificacion.plantilla}, columna
     * {@code asunto}.
     */
    public static final String ASUNTO = "NOTIFICACION_FACTORING";

    @EJB
    private PlantillaFacade plantillaFacade;
    // </editor-fold>

    /**
     * Obtiene un objeto plantilla en base a su tipo.
     *
     * @param tipo Tipo de plantilla.
     * @return Plantilla de notificacion.
     */
    public Plantilla find(TipoPlantilla tipo) {
        for (Plantilla plantilla : plantillaFacade.findAll()) {
            boolean a = Objects.equals(plantilla.getAsunto(), ASUNTO);
            boolean b = Objects.equals(plantilla.getCuerpo(), tipo.getCuerpo());
            if (a && b) {
                return plantilla;
            }
        }
        return null;
    }

}
