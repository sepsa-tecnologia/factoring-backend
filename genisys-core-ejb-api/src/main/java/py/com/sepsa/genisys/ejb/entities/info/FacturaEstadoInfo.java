/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.info;

import fa.gs.utils.collections.Maps;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class FacturaEstadoInfo implements Serializable {

    private Integer facturaId;
    private String facturaNumero;
    private Date facturaFechaEmision;
    private String proveedorRuc;
    private String estadoDescripcion;
    private Date estadoFechaCambio;
    private String observaciones;

    public static FacturaEstadoInfo instance(Map<String, Object> row) {
        FacturaEstadoInfo instance = new FacturaEstadoInfo();
        instance.facturaId = Maps.integer(row, "facturaId");
        instance.facturaNumero = Maps.string(row, "facturaNumero");
        instance.facturaFechaEmision = Maps.date(row, "facturaFechaEmision");
        instance.proveedorRuc = Maps.string(row, "proveedorRuc");
        instance.estadoDescripcion = Maps.string(row, "estadoDescripcion");
        instance.estadoFechaCambio = Maps.date(row, "estadoFechaCambio");
        instance.observaciones = Maps.string(row, "observaciones");
        return instance;
    }
}
