/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "archivo_adjunto", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArchivoAdjunto.findAll", query = "SELECT a FROM ArchivoAdjunto a"),
    @NamedQuery(name = "ArchivoAdjunto.findById", query = "SELECT a FROM ArchivoAdjunto a WHERE a.id = :id"),
    @NamedQuery(name = "ArchivoAdjunto.findByIdEntidad", query = "SELECT a FROM ArchivoAdjunto a WHERE a.idEntidad = :idEntidad"),
    @NamedQuery(name = "ArchivoAdjunto.findByTipo", query = "SELECT a FROM ArchivoAdjunto a WHERE a.tipoEntidad = :tipoEntidad"),
    @NamedQuery(name = "ArchivoAdjunto.findByIdEntidadTipoEntidad", query = "SELECT a FROM ArchivoAdjunto a WHERE a.idEntidad = :idEntidad and a.tipoEntidad = :tipoEntidad")})
public class ArchivoAdjunto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_archivo")
    private Integer idArchivo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_entidad")
    private Integer idEntidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "tipo_entidad")
    private String tipoEntidad;

    // Campos adicionales.
    @JoinColumn(name = "id_archivo", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Archivo archivo;

    public ArchivoAdjunto() {
    }

    public ArchivoAdjunto(Integer id) {
        this.id = id;
    }

    public ArchivoAdjunto(Integer id, Integer idEntidad, String tipo) {
        this.id = id;
        this.idEntidad = idEntidad;
        this.tipoEntidad = tipo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public Integer getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(Integer idEntidad) {
        this.idEntidad = idEntidad;
    }

    public String getTipoEntidad() {
        return tipoEntidad;
    }

    public void setTipoEntidad(String tipoEntidad) {
        this.tipoEntidad = tipoEntidad;
    }

    public Archivo getArchivo() {
        return archivo;
    }

    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ArchivoAdjunto)) {
            return false;
        }
        ArchivoAdjunto other = (ArchivoAdjunto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.ArchivoAdjunto[ id=" + id + " ]";
    }

}
