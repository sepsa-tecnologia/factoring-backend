/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.pojos;

import java.io.Serializable;
import lombok.Data;
import py.com.sepsa.genisys.ejb.logic.impl.Personas.TipoPersonaEnum;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
@Data
public class RegistrarPersonaInput implements Serializable {

    private TipoPersonaEnum tipo;
    private String razonSocial;
    private String nombreFantasia;
    private String nombres;
    private String apellidos;
    private String ci;
    private String ruc;

    public RegistrarPersonaInput() {
        this.tipo = null;
        this.razonSocial = "";
        this.nombreFantasia = "";
        this.nombres = "";
        this.apellidos = "";
        this.ruc = "";
    }
}
