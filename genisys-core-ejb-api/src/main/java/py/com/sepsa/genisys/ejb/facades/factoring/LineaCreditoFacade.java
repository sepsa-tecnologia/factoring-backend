/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.LineaCredito;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "LineaCreditoFacade", mappedName = "LineaCreditoFacade")
@LocalBean
public class LineaCreditoFacade extends AbstractFacade<LineaCredito> {

    /**
     * Constructor.
     */
    public LineaCreditoFacade() {
        super(LineaCredito.class);
    }

}
