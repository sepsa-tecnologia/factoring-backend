/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_AgregarLiquidacion", mappedName = "PF_BancoRio_AgregarLiquidacion")
@LocalBean
public class PF_BancoRio_AgregarLiquidacion implements Serializable {

    private AppLogger log;

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Facturas facturas;

    @EJB
    private PF_BancoRio_SolicitarLiquidacionFactura solicitarLiquidacionFactura;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    public void work(Integer idFactura) throws Throwable {
        // Obtener identificador de entidad financiera para venta.
        Result<Integer> resIdEntidadFinanciera = facturas.obtenerIdEntidadFinancieraVenta(idFactura);
        if (resIdEntidadFinanciera.isFailure()) {
            throw Errors.illegalState(resIdEntidadFinanciera.failure().cause(), "No se pudo determinar la entidad financiera de venta para factura id=%s", idFactura);
        }

        // Control.
        Integer idEntidadFinanciera = resIdEntidadFinanciera.value();
        if (!bancoRio.esBancoRio(idEntidadFinanciera)) {
            return;
        }

        // Control.
        Boolean existeFactura = facturas.existeNroFacturaEnFacturaSeguimientoLiquidacion(idFactura).value(Boolean.FALSE);
        if (!existeFactura) {
            // Registrar liquidacion pendiente de factura.
            facturas.registrarFacturaSeguimientoLiquidacion(idFactura);

            // Solicitar liquidacion en entidad.
            registrarSeguimientoLiquidacionFactura(idFactura);
        }
    }

    private void registrarSeguimientoLiquidacionFactura(Integer idFactura) throws Throwable {
        // Obtener factura.
        Result<FacturaInfo> resFactura = facturas.buscarFactura(idFactura);
        resFactura.raise();
        FacturaInfo factura = resFactura.value();

        // Obtener id entidad financiera.
        Integer idEntidadFinanciera = bancoRio.obtenerIdEntidadBancoRio();

        // Obtener id comprador.
        Integer idComprador = factura.getCompradorId();

        // Solicitar liquidacion.
        solicitarLiquidacionFactura.solicitarLiquidacionFactura(idEntidadFinanciera, idComprador, factura);
    }

}
