/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "banco_rio_seguimiento_desembolso", schema = "factoring")
@Data
public class BancoRioSeguimientoDesembolso implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_comprador")
    private Integer idComprador;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_proveedor")
    private Integer idProveedor;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_lote")
    private Integer idLote;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_solicitud")
    private String idSolicitud;

    @Basic(optional = false)
    @NotNull
    @Column(name = "nro_operacion")
    private String nroOperacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private String estado;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_solicitud")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSolicitud;

    @Basic(optional = true)
    @Column(name = "fecha_respuesta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRespuesta;

}
