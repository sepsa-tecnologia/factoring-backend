/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.NotaCredito;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "NotaCreditoFacade", mappedName = "NotaCreditoFacade")
@LocalBean
public class NotaCreditoFacade extends AbstractFacade<NotaCredito> {

    /**
     * Constructor.
     */
    public NotaCreditoFacade() {
        super(NotaCredito.class);
    }

    /**
     * Permite recuperar la nota de credito asociada a una factura dada.
     *
     * @param idFactura Identificador de factura.
     * @return Nota de credito asociada a la factura, si hubiere. Null caso
     * contrario.
     */
    public NotaCredito findByIdFactura(Integer idFactura) {
        Query q = getEntityManager().createNamedQuery("NotaCredito.findByIdFactura");
        q.setParameter("idFactura", idFactura);
        return JpaUtils.getFirst(q);
    }

}
