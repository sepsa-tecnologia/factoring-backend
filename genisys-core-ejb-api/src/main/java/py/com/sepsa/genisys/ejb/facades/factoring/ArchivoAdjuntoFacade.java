/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.ArchivoAdjunto;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ArchivoAdjuntoFacade", mappedName = "ArchivoAdjuntoFacade")
@LocalBean
public class ArchivoAdjuntoFacade extends AbstractFacade<ArchivoAdjunto> {

    /**
     * Constructor.
     */
    public ArchivoAdjuntoFacade() {
        super(ArchivoAdjunto.class);
    }

    /**
     * Obtiene los registros de archivos asociados a una entidad dada, descrito
     * por su tipo e identificador correspondiente.
     *
     * @param idEntidad Identificador de entidad.
     * @param tipoEntidad Descriptor de tipo de entidad.
     * @return Lista de archivos asociados, si hubiere.
     */
    public List<ArchivoAdjunto> findByIdEntidad(Integer idEntidad, String tipoEntidad) {
        Query q = getEntityManager().createNamedQuery("ArchivoAdjunto.findByIdEntidadTipoEntidad");
        q.setParameter("idEntidad", idEntidad);
        q.setParameter("tipoEntidad", tipoEntidad);
        return JpaUtils.getAll(q);
    }

}
