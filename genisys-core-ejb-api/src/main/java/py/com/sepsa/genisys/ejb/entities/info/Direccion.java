/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
@Entity
@Table(name = "direccion", schema = "info")
@NamedQueries({
    @NamedQuery(name = "Direccion.obtenerPorId", query = "SELECT d from Direccion d WHERE d.id = :idDireccion")
})
public class Direccion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_calle1")
    private Integer idCalle1;

    @Basic(optional = true)
    @Column(name = "id_calle2")
    private Integer idCalle2;

    @Basic(optional = true)
    @Column(name = "id_calle3")
    private Integer idCalle3;

    @Basic(optional = true)
    @Column(name = "id_calle4")
    private Integer idCalle4;

    @Basic(optional = true)
    @Column(name = "numero")
    private String numero;

    @Basic(optional = true)
    @Column(name = "observacion")
    private String observacion;

    @Basic(optional = true)
    @Column(name = "latitud")
    private String latitud;

    @Basic(optional = true)
    @Column(name = "longitud")
    private String longitud;

    @Basic(optional = true)
    @Column(name = "direccion")
    private String direccion;

}
