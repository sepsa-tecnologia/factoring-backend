/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraInput;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraOutput;

/**
 * Interface que define el comportamiento de un bean que puede ser utilizado en
 * la vista de calculadora.
 *
 * @author Fabio A. González Sosa
 */
public interface Calculador {

    /**
     * Metodo que valida los datos de entrada.
     *
     * @return Mensaje de error, si hubiere. Caso contrario {@code null}.
     */
    String validar();

    /**
     * Metodo principal de calculo.
     *
     * @throws Throwable Error producido durante el calculo.
     */
    void calcular() throws Throwable;

    void setCalculadoraInput(CalculadoraInput input);

    CalculadoraInput getCalculadoraInput();

    void setCalculadoraOutput(CalculadoraOutput output);

    CalculadoraOutput getCalculadoraOutput();

}
