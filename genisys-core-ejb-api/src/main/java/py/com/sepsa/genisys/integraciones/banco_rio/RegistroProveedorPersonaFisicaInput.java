/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioActividadEconomica;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ContactoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.CuentaInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DireccionInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ReferenciaInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.RubroInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class RegistroProveedorPersonaFisicaInput implements Serializable {

    private String nombres;
    private String apellidos;
    private DocumentoInfo documento;
    private Date fechaNacimiento;
    private DireccionInfo direccion;
    private ContactoInfo contacto;
    private BancoRioActividadEconomica actividadEconomica;
    private RubroInfo rubro;
    private CuentaInfo cuentaAcreditacion;
    private BigDecimal facturacionPromedio;
    private ReferenciaInfo comprador;

}
