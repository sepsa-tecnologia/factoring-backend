/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "config_cuenta", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConfigCuenta.findAll", query = "SELECT c FROM ConfigCuenta c"),
    @NamedQuery(name = "ConfigCuenta.findById", query = "SELECT c FROM ConfigCuenta c WHERE c.id = :id"),
    @NamedQuery(name = "ConfigCuenta.findByIdCuenta", query = "SELECT c FROM ConfigCuenta c WHERE c.idCuenta = :idCuenta"),
    @NamedQuery(name = "ConfigCuenta.findByTipoCuenta", query = "SELECT c FROM ConfigCuenta c WHERE c.tipoCuenta = :tipoCuenta")})
public class ConfigCuenta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cuenta")
    private int idCuenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo_cuenta")
    private int tipoCuenta;
    @JoinColumn(name = "id_convenio", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Convenio idConvenio;

    public ConfigCuenta() {
    }

    public ConfigCuenta(Integer id) {
        this.id = id;
    }

    public ConfigCuenta(Integer id, int idCuenta, int tipoCuenta) {
        this.id = id;
        this.idCuenta = idCuenta;
        this.tipoCuenta = tipoCuenta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public int getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(int tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public Convenio getIdConvenio() {
        return idConvenio;
    }

    public void setIdConvenio(Convenio idConvenio) {
        this.idConvenio = idConvenio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ConfigCuenta)) {
            return false;
        }
        ConfigCuenta other = (ConfigCuenta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.genisys.ejb.entities.factoring.ConfigCuenta[ id=" + id + " ]";
    }

}
