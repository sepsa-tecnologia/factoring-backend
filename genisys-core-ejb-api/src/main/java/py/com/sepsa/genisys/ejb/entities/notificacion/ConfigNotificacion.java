/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.notificacion;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "config_notificacion", schema = "notificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConfigNotificacion.findAll", query = "SELECT c FROM ConfigNotificacion c"),
    @NamedQuery(name = "ConfigNotificacion.findById", query = "SELECT c FROM ConfigNotificacion c WHERE c.id = :id"),
    @NamedQuery(name = "ConfigNotificacion.findByIdPersonaOrigen", query = "SELECT c FROM ConfigNotificacion c WHERE c.idPersonaOrigen = :idPersonaOrigen"),
    @NamedQuery(name = "ConfigNotificacion.findByIdLocalOrigen", query = "SELECT c FROM ConfigNotificacion c WHERE c.idLocalOrigen = :idLocalOrigen"),
    @NamedQuery(name = "ConfigNotificacion.findByIdPersonaDestino", query = "SELECT c FROM ConfigNotificacion c WHERE c.idPersonaDestino = :idPersonaDestino"),
    @NamedQuery(name = "ConfigNotificacion.findByIdLocalDestino", query = "SELECT c FROM ConfigNotificacion c WHERE c.idLocalDestino = :idLocalDestino"),
    @NamedQuery(name = "ConfigNotificacion.findByEstado", query = "SELECT c FROM ConfigNotificacion c WHERE c.estado = :estado")})
public class ConfigNotificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_persona_origen")
    private Integer idPersonaOrigen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_local_origen")
    private int idLocalOrigen;
    @Column(name = "id_persona_destino")
    private Integer idPersonaDestino;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_local_destino")
    private int idLocalDestino;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private Character estado;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "configNotificacion")
    private Collection<NotificacionContacto> notificacionContactoCollection;

    public ConfigNotificacion() {
    }

    public ConfigNotificacion(Integer id) {
        this.id = id;
    }

    public ConfigNotificacion(Integer id, int idLocalOrigen, int idLocalDestino, Character estado) {
        this.id = id;
        this.idLocalOrigen = idLocalOrigen;
        this.idLocalDestino = idLocalDestino;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPersonaOrigen() {
        return idPersonaOrigen;
    }

    public void setIdPersonaOrigen(Integer idPersonaOrigen) {
        this.idPersonaOrigen = idPersonaOrigen;
    }

    public int getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(int idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdPersonaDestino() {
        return idPersonaDestino;
    }

    public void setIdPersonaDestino(Integer idPersonaDestino) {
        this.idPersonaDestino = idPersonaDestino;
    }

    public int getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(int idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    @XmlTransient
    public Collection<NotificacionContacto> getNotificacionContactoCollection() {
        return notificacionContactoCollection;
    }

    public void setNotificacionContactoCollection(Collection<NotificacionContacto> notificacionContactoCollection) {
        this.notificacionContactoCollection = notificacionContactoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ConfigNotificacion)) {
            return false;
        }
        ConfigNotificacion other = (ConfigNotificacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.notificacion.ConfigNotificacion[ id=" + id + " ]";
    }

}
