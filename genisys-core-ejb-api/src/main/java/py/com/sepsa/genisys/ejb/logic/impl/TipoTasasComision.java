/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.utils.misc.numeric.Numeric;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.logic.LogicBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "TipoTasasComision", mappedName = "TipoTasasComision")
@LocalBean
public class TipoTasasComision extends LogicBean {

    /**
     * Obtiene una lista con las enumeraciones disponibles para tipos de tasa de
     * comision.
     *
     * @return Lista de tasas de comision.
     */
    public List<TipoTasaComisionEnum> obtenerTipoTasasComision() {
        return Arrays.asList(
                TipoTasasComision.TipoTasaComisionEnum.SIMPLE_DIARIO_1,
                TipoTasasComision.TipoTasaComisionEnum.SIMPLE_MENSUAL_30,
                TipoTasasComision.TipoTasaComisionEnum.SIMPLE_ANUAL_360,
                TipoTasasComision.TipoTasaComisionEnum.SIMPLE_ANUAL_365
        );
    }

    /**
     * Enumeracion para los tipos de valores posibles en la configuracion de un
     * cargo.
     */
    public enum TipoTasaComisionEnum {
        SIMPLE_DIARIO_1("Simple Diario", 1L),
        SIMPLE_MENSUAL_30("Simple Mensual (30)", 30L),
        SIMPLE_ANUAL_360("Simple Anual (360)", 360L),
        SIMPLE_ANUAL_365("Simple Anual (365)", 365L);

        // Representa la descripcion del tipo de valor de cargo.
        private final String descripcion;
        private final BigDecimal factor;

        TipoTasaComisionEnum(String value, long factor) {
            this.descripcion = value;
            this.factor = Numeric.wrap(factor);
        }

        public String descripcion() {
            return descripcion;
        }

        public BigDecimal factor() {
            return factor;
        }

        @Override
        public String toString() {
            return descripcion;
        }

    }

}
