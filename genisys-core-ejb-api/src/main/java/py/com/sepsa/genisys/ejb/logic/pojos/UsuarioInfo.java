/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.pojos;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class UsuarioInfo implements Serializable {

    private Integer id;
    private String usuario;
    private Integer idPersona;
    private String razonSocial;
    private String nombreFantasia;
    private String ruc;
    private String dvRuc;
    private String idPerfilAgg;
    private String txtPerfilAgg;
}
