/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.enums;

/**
 * Enumeracion para los tipos de valores posibles en la configuracion de un
 * cargo.
 */
public enum TipoValorEnum {
    PORCENTAJE("PORCENTAJE"),
    MONTO("MONTO"),
    TEXTO("TEXTO");

    // Representa la descripcion del tipo de valor de cargo.
    private final String descripcion;

    TipoValorEnum(String value) {
        this.descripcion = value;
    }

    public String descripcion() {
        return descripcion;
    }

}
