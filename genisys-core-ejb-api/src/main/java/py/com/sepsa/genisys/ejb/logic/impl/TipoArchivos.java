/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoArchivo;
import py.com.sepsa.genisys.ejb.enums.TipoArchivoEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.TipoArchivosFacade;
import py.com.sepsa.genisys.ejb.logic.LogicBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "TipoArchivos", mappedName = "TipoArchivos")
@LocalBean
public class TipoArchivos extends LogicBean {

    @EJB
    private TipoArchivosFacade tipoArchivosFacade;

    /**
     * Obtiene un objeto tipo de archivo en base a su enumeracion de tipo.
     *
     * @param tipoArchivoEnum Enumeracion de Tipo de archivo.
     * @return Resultado de la operacion.
     */
    public Result<TipoArchivo> obtenerTipoArchivo(TipoArchivoEnum tipoArchivoEnum) {
        Result<TipoArchivo> result = Results.ok().nullable(true).build();

        for (TipoArchivo tipoArchivo : tipoArchivosFacade.findAll()) {
            if (tipoArchivo.getDescripcion().equals(tipoArchivoEnum.descripcion())) {
                result = Results.ok().value(tipoArchivo).build();
                break;
            }
        }

        return result;
    }

}
