/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import java.io.Serializable;
import java.util.Collection;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ComisionBancoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ComisionCompradorInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ComisionSepsaInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.FacturaALiquidarInfo;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
@Data
public class SolicitudLiquidacionInput implements Serializable {

    private Persona comprador;
    private DocumentoInfo compradorDocumento;
    private ComisionCompradorInfo comisionComprador;
    private ComisionSepsaInfo comisionSepsa;
    private ComisionBancoInfo comisionBanco;
    private Collection<FacturaALiquidarInfo> facturasALiquidar;
}
