/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Fabio A. González Sosa
 */
public class AbstractFacade<T> extends fa.gs.criteria.facades.AbstractFacade<T> {

    @PersistenceContext(unitName = Persistence.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    public AbstractFacade(Class<T> entityClass) {
        super(entityClass);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

}
