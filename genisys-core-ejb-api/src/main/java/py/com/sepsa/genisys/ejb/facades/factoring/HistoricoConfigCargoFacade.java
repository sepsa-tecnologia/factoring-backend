/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoConfigCargo;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "HistoricoConfigCargoFacade", mappedName = "HistoricoConfigCargoFacade")
@LocalBean
public class HistoricoConfigCargoFacade extends AbstractFacade<HistoricoConfigCargo> {

    /**
     * Constructor.
     */
    public HistoricoConfigCargoFacade() {
        super(HistoricoConfigCargo.class);
    }

}
