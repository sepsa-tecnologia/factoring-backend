/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.factoring.UsuarioPerfil;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "UsuarioPerfilFacade", mappedName = "UsuarioPerfilFacade")
@LocalBean
public class UsuarioPerfilFacade extends AbstractFacade<UsuarioPerfil> {

    /**
     * Constructor.
     */
    public UsuarioPerfilFacade() {
        super(UsuarioPerfil.class);
    }

    /**
     * Permite obtener la lista de perfiles disponibles que posee un usuario
     * dado.
     *
     * @param idUsuario Identificador de usuario.
     * @return Lista de perfiles asignados al usuario.
     */
    public List<Perfil> getUsuarioPerfiles(Integer idUsuario) {
        Query q = getEntityManager().createNamedQuery("UsuarioPerfil.findByIdUsuario");
        q.setParameter("idUsuario", idUsuario);
        List<UsuarioPerfil> e = JpaUtils.getAll(q);
        return e.stream().map((up) -> up.getPerfil()).collect(Collectors.toList());
    }

}
