/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.notificacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Fabio A. González Sosa
 */
@Embeddable
public class NotificacionEmailFactoringFacturaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_notificacion")
    private int idNotificacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_factura")
    private int idFactura;

    public NotificacionEmailFactoringFacturaPK() {
    }

    public NotificacionEmailFactoringFacturaPK(int idNotificacion, int idFactura) {
        this.idNotificacion = idNotificacion;
        this.idFactura = idFactura;
    }

    public int getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(int idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idNotificacion;
        hash += (int) idFactura;
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof NotificacionEmailFactoringFacturaPK)) {
            return false;
        }
        NotificacionEmailFactoringFacturaPK other = (NotificacionEmailFactoringFacturaPK) object;
        if (this.idNotificacion != other.idNotificacion) {
            return false;
        }
        if (this.idFactura != other.idFactura) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.notificacion.NotificacionEmailFactoringFacturaPK[ idNotificacion=" + idNotificacion + ", idFactura=" + idFactura + " ]";
    }

}
