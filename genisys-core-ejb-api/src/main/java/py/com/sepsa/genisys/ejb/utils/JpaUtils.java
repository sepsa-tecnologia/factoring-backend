/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.database.jpa.Jpa;
import fa.gs.utils.misc.numeric.Numeric;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "JpaUtils", mappedName = "JpaUtils")
@LocalBean
public class JpaUtils implements Serializable {

    @PersistenceContext(unitName = Persistence.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    private <T> boolean checkConstraints(T entity) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(entity);
        if (constraintViolations.size() > 0) {
            for (ConstraintViolation<T> constraint : constraintViolations) {
                String violation = constraint.getRootBeanClass().getSimpleName() + "." + constraint.getPropertyPath() + " " + constraint.getMessage();
                throw new ValidationException(violation);
            }
            return false;
        }
        return true;
    }

    public <T> T create(T entity) throws Throwable {
        boolean valid = checkConstraints(entity);
        if (valid) {
            em.persist(entity);
            em.flush();
        }
        return entity;
    }

    public <T> T edit(T entity) {
        boolean valid = checkConstraints(entity);
        if (valid) {
            em.merge(entity);
            em.flush();
        }
        return entity;
    }

    public <T> void remove(T entity) {
        em.remove(entity);
        em.flush();
    }

    public int executeUpdate(String query) throws Throwable {
        Query q = em.createNativeQuery(query);
        return q.executeUpdate();
    }

    public Map<String, Object> getFirst(String query) throws Throwable {
        Collection<Map<String, Object>> rows = getAll(query);
        return Lists.first(rows);
    }

    public Collection<Map<String, Object>> getAll(String query) throws Throwable {
        try {
            return Jpa.select(query, em);
        } catch (NoResultException nre) {
            return Lists.empty();
        }
    }

    public static <T> T getFirst(Query query) {
        try {
            return (T) query.getResultList()
                    .stream()
                    .findFirst()
                    .orElse(null);
        } catch (NoResultException nre) {
            return null;
        }
    }

    public static <T> List<T> getAll(Query query) {
        try {
            return query.getResultList();
        } catch (Throwable thr) {
            return Lists.empty();
        }
    }

    public Integer getTotal(String query) throws Throwable {
        return getTotal(query, "total");
    }

    public Integer getTotal(String query, String countColumnAlias) throws Throwable {
        Map<String, Object> row = getFirst(query);
        Object obj = Maps.get(row, countColumnAlias);
        if (obj == null) {
            return 0;
        } else {
            return Numeric.adaptAsInteger(obj);
        }
    }

}
