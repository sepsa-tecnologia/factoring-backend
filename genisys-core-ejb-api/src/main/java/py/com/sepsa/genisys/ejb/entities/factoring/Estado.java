/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "estado", schema = "factoring")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estado.findAll", query = "SELECT e FROM Estado e"),
    @NamedQuery(name = "Estado.findById", query = "SELECT e FROM Estado e WHERE e.id = :id"),
    @NamedQuery(name = "Estado.findByDescripcion", query = "SELECT e FROM Estado e WHERE e.descripcion = :descripcion"),
    @NamedQuery(name = "Estado.findByTipo", query = "SELECT e FROM Estado e WHERE e.tipo = :tipo"),
    @NamedQuery(name = "Estado.findByDescripcionTipo", query = "SELECT e FROM Estado e WHERE e.descripcion = :descripcion and e.tipo = :tipo")})
public class Estado implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Se desactiva la generacion automatica de valores del campo ID ya que no
     * es posible utilizar setId sobre una entidad y luego utilizar el metodo
     * create del facade correspondiente. La capa de persistencia omite el valor
     * de ID establecido explicitamente ya que la anotacion @GeneratedValue
     * fuerza a generar un valor arbitrario que pudiera estar duplicado.
     *
     * Por un error inicial de diseño (estados creados manualmente) cada valor
     * de ID debe ser conocido de antemano, por lo tanto una generacion
     * automatica de valores para el campo ID no es necesaria.
     */
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "tipo")
    private Character tipo;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEstado", fetch = FetchType.LAZY)
    private Collection<HistoricoFactura> historicoFacturaCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEstado", fetch = FetchType.LAZY)
    private Collection<Factura> facturaCollection;

    public Estado() {
    }

    public Estado(Integer id) {
        this.id = id;
    }

    public Estado(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    public Collection<HistoricoFactura> getHistoricoFacturaCollection() {
        return historicoFacturaCollection;
    }

    public void setHistoricoFacturaCollection(Collection<HistoricoFactura> historicoFacturaCollection) {
        this.historicoFacturaCollection = historicoFacturaCollection;
    }

    @XmlTransient
    public Collection<Factura> getFacturaCollection() {
        return facturaCollection;
    }

    public void setFacturaCollection(Collection<Factura> facturaCollection) {
        this.facturaCollection = facturaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Estado)) {
            return false;
        }
        Estado other = (Estado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getDescripcion();
    }

}
