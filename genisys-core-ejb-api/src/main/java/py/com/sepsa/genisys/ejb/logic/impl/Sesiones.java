/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.misc.Assertions;
import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.security.crypto.bcrypt.BCrypt;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.factoring.UsuarioPropio;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.facades.factoring.UsuarioPerfilFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;
import py.com.sepsa.genisys.ejb.utils.Persistence;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Sesiones", mappedName = "Sesiones")
@LocalBean
public class Sesiones implements Serializable {

    @PersistenceContext(unitName = Persistence.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @EJB
    private JpaUtils jpaUtils;

    @EJB
    private Personas personas;

    @EJB
    private UsuarioPerfilFacade usuarioPerfilFacade;

    public Result<Usuario> obtenerPorTransId(Integer idUsuario) {
        Result<Usuario> result;

        try {
            // Construir query.
            Query q = em.createNamedQuery("Usuario.findById");
            q.setParameter("id", idUsuario);

            // Obtener usuarios.
            Usuario usuario = JpaUtils.getFirst(q);
            result = Results.ok()
                    .value(usuario)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error verificando usuario")
                    .tag("usuario.id", idUsuario)
                    .build();
        }

        return result;
    }

    public Result<Collection<Usuario>> obtenerPorTransUsuario(String username) {
        Result<Collection<Usuario>> result;

        try {
            // Construir query.
            Query q = em.createNamedQuery("Usuario.findByUsuario");
            q.setParameter("usuario", username);

            // Obtener usuarios.
            Collection<Usuario> usuarios = JpaUtils.getAll(q);
            result = Results.ok()
                    .value(usuarios)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error verificando usuario")
                    .tag("usuario.usuario", username)
                    .build();
        }

        return result;
    }

    public Result<Usuario> obtenerPorTransUsuario(String username, String plainPassword) {
        Result<Usuario> result;

        try {
            // Obtener usuarios.
            Result<Collection<Usuario>> resUsuarios = obtenerPorTransUsuario(username);
            resUsuarios.raise(true);
            Collection<Usuario> usuarios = resUsuarios.value(Lists.empty());

            // Control.
            if (Assertions.isNullOrEmpty(usuarios)) {
                throw Errors.illegalArgument("Credenciales invalidas");
            }

            // Obtener usuario con contrasenha especificada.
            Usuario usuario = null;
            for (Usuario usuario0 : usuarios) {
                if (BCrypt.checkpw(plainPassword, usuario0.getContrasena())) {
                    usuario = usuario0;
                    break;
                }
            }

            // Control.
            if (usuario == null) {
                throw Errors.illegalArgument("Credenciales invalidas");
            }

            // Verificar persona asociada a usuario.
            Result<Persona> resPersona = personas.obtenerPorId(usuario.getIdPersona());
            resPersona.raise(true);
            Persona persona = resPersona.value();

            // Control.
            if (persona == null) {
                throw Errors.illegalArgument("Credenciales invalidas");
            }

            result = Results.ok()
                    .value(usuario)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error verificando usuario")
                    .tag("usuario.usuario", username)
                    .build();
        }

        return result;
    }

    public Result<Collection<Perfil>> obtenerPorTransUsuario(Usuario usuario) {
        return obtenerPorTransUsuario(usuario.getId());
    }

    public Result<Collection<Perfil>> obtenerPorTransUsuario(Integer idUsuario) {
        Result<Collection<Perfil>> result;

        try {
            Collection<Perfil> perfiles = usuarioPerfilFacade.getUsuarioPerfiles(idUsuario);
            if (Assertions.isNullOrEmpty(perfiles)) {
                perfiles = Lists.empty();
            }

            result = Results.ok()
                    .value(perfiles)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo perfiles de usuario")
                    .tag("usuario.id", idUsuario)
                    .build();
        }

        return result;
    }

    public Result<UsuarioPropio> obtenerPorFactoringPersonaAlias(Integer idPerona, String alias) {
        Result<UsuarioPropio> result;

        try {
            // Obtener usuarios.
            Query q = em.createNamedQuery("UsuarioPropio.findByPersonaAlias");
            q.setParameter("idPersona", idPerona);
            q.setParameter("alias", alias);
            List<UsuarioPropio> usuarios = jpaUtils.getAll(q);

            // Control.
            if (Assertions.isNullOrEmpty(usuarios)) {
                throw Errors.illegalArgument("Credenciales invalidas");
            }

            UsuarioPropio usuario = usuarios.get(0);
            result = Results.ok()
                    .value(usuario)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error verificando usuario")
                    .build();
        }

        return result;
    }

    public Result<Usuario> obtenerPorFactoringUsuario(String authId, String authSecret) {
        Result<Usuario> result;

        try {
            // Obtener usuarios.
            Query q = em.createNamedQuery("UsuarioPropio.findByAuthId");
            q.setParameter("authId", authId);
            List<UsuarioPropio> usuarios = jpaUtils.getAll(q);

            // Control.
            if (Assertions.isNullOrEmpty(usuarios)) {
                throw Errors.illegalArgument("Credenciales invalidas");
            }

            // Obtener usuario con contrasenha especificada.
            UsuarioPropio usuario = null;
            for (UsuarioPropio usuario0 : usuarios) {
                if (BCrypt.checkpw(authSecret, usuario0.getAuthSecret())) {
                    usuario = usuario0;
                    break;
                }
            }

            // Control.
            if (usuario == null) {
                return Results.ko()
                        .message("Credenciales inválidas")
                        .build();
            }

            // Obtener usuario trans.
            result = obtenerPorTransId(usuario.getIdUsuario());
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error verificando usuario")
                    .build();
        }

        return result;
    }

}
