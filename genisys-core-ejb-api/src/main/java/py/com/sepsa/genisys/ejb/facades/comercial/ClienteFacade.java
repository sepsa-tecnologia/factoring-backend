/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.comercial;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.comercial.Cliente;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ClienteFacade", mappedName = "ClienteFacade")
@LocalBean
public class ClienteFacade extends AbstractFacade<Cliente> {

    /**
     * Constructor.
     */
    public ClienteFacade() {
        super(Cliente.class);
    }

    /**
     * Obtiene un objeto cliente en base a un id de cliente.
     *
     * @param idCliente Identificador de cliente.
     * @return Objeto cliente, si hubiere. Null caso contrario.
     */
    public Cliente findByIdCliente(Integer idCliente) {
        Query q = getEntityManager().createNamedQuery("Cliente.findByIdCliente");
        q.setParameter("idCliente", idCliente);
        return JpaUtils.getFirst(q);
    }

}
