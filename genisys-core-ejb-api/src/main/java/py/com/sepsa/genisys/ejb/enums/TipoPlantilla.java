/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.enums;

import java.util.Objects;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum TipoPlantilla {
    TEXTO("TEXTO", "Alerta"),
    SOLICITUD_CONFORMACION("SOLICITUD_CONFORMACION", "Solicitud de Conformación de Facturas"),
    CONFORMACION("CONFORMACION", "Conformación de Facturas"),
    VENTA("VENTA", "Venta de Facturas"),
    PAGO("PAGO", "Desembolso de Facturas"),
    COBRO("COBRO", "Pago de Facturas Adelantadas"),
    RECHAZO_POR_COMPRADOR("RECHAZO_COMPRADOR", "Rechazo de Facturas por Comprador"),
    RECHAZO_POR_PROVEEDOR("RECHAZO_PROVEEDOR", "Rechazo de Facturas por Proveedor"),
    RECHAZO_POR_ENTIDAD("RECHAZO_ENTIDAD", "Rechazo de Facturas por Entidad Financiera"),
    EN_ESTUDIO("EN_ESTUDIO", "Aceptación de Facturas para Análisis"),
    RECORDATORIO_FACTURAS_CONFORMADAS("RECORDATORIO_FACTURAS_CONFORMADAS", "Recordatorio de facturas conformadas"),
    RECORDATORIO_FACTURAS_POR_VENCER("RECORDATORIO_FACTURAS_POR_VENCER", "Recordatorio de facturas por vencer");

    /**
     * Representa el cuerpo de la plantilla. El cual es utilizado para
     * distinguir los tipos de plantillas en lugar de determinar el cuerpo de la
     * plantilla en sí.
     */
    private final String cuerpo;

    /**
     * Representa el asunto, como texto mas "humano", a ser incluido dentro del
     * cuerpo de una notificacion.
     */
    private final String asunto;

    /**
     * Constructor.
     *
     * @param cuerpo cuerpo de la plantilla.
     * @param asunto texto que describe de manera mas amigable el significador
     * del cuerpo.
     */
    TipoPlantilla(String cuerpo, String asunto) {
        this.cuerpo = cuerpo;
        this.asunto = asunto;
    }

    public static TipoPlantilla fromAsunto(String asunto) {
        for (TipoPlantilla tipo : TipoPlantilla.values()) {
            if (Objects.equals(asunto, tipo.getAsunto())) {
                return tipo;
            }
        }
        return null;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public String getAsunto() {
        return asunto;
    }

    @Override
    public String toString() {
        return cuerpo;
    }

}
