/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.fechas.Fechas;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.jboss.ejb3.annotation.TransactionTimeout;
import py.com.sepsa.genisys.ejb.entities.factoring.Estado;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Constantes;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.TxWrapperCore;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_AgregarLiquidaciones", mappedName = "PF_BancoRio_AgregarLiquidaciones")
@LocalBean
public class PF_BancoRio_AgregarLiquidaciones implements Serializable {

    private AppLogger log;

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Facturas facturas;

    @EJB
    private Estados estados;

    @EJB
    private Notificaciones notificaciones;

    @EJB
    private PF_BancoRio_AgregarLiquidacion agregarLiquidacion;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    @TransactionTimeout(value = Constantes.TX_TIMEOUT_VALUE)
    public void work() throws Throwable {
        // Estado de factura requerido para iniciar la liquidacion (cobrado por entidad).
        Estado COBRADA = estados.buscarEstado(Estados.EstadoFacturaEnum.COBRADA).value();
        Integer idEstado = COBRADA.getId();

        // Dias de tolerancia.
        Integer diasTolerancia = bancoRio.obtenerDiasTolerancia();
        Date fechaHasta = Fechas.now();
        Date fechaDesde = Fechas.addValue(fechaHasta, Calendar.DAY_OF_MONTH, -1 * Math.abs(diasTolerancia));

        // Obtiene los identificadores de facturas que hayan tenido el cambio de estado entre las fechas inidicadas.
        Result<List<Integer>> resIdFacturas = facturas.obtenerIdFacturasCambioEstado(idEstado, fechaDesde, fechaHasta);
        resIdFacturas.raise();
        List<Integer> idFacturas = resIdFacturas.value();

        // Marcar las facturas para seguimiento de liquidacion.
        if (!Assertions.isNullOrEmpty(idFacturas)) {
            for (Integer idFactura : idFacturas) {
                TxWrapperCore tx = Injection.lookup(TxWrapperCore.class);
                Result<?> result = tx.execute(() -> work0(idFactura));
                if (result.isFailure()) {
                    notificaciones.notificarSepsa(result.failure().cause(), "Ocurrió un error agregando liquidación para factura id=%s", idFactura);
                }
            }
        }
    }

    private Void work0(Integer idFactura) throws Throwable {
        agregarLiquidacion.work(idFactura);
        return null;
    }

}
