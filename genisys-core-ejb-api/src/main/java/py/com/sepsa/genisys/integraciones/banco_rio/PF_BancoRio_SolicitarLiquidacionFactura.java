/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.fechas.Fechas;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioPersona;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioSeguimientoLiquidacion;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Configuraciones;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.impl.Sepsa;
import py.com.sepsa.genisys.ejb.logic.pojos.DistribucionComisionInfo;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ComisionBancoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ComisionCompradorInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ComisionSepsaInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.FacturaALiquidarInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PF_BancoRio_SolicitarLiquidacionFactura", mappedName = "PF_BancoRio_SolicitarLiquidacionFactura")
@LocalBean
public class PF_BancoRio_SolicitarLiquidacionFactura implements Serializable {

    @EJB
    private BancoRio bancoRio;

    @EJB
    private Sepsa sepsa;

    @EJB
    private Personas personas;

    @EJB
    private Configuraciones configuraciones;

    @EJB
    private Perfiles perfiles;

    @EJB
    private JpaUtils jpaUtils;

    public void solicitarLiquidacionFactura(Integer idEntidadFinanciera, Integer idComprador, FacturaInfo factura) throws Throwable {
        // Control.
        BancoRioSeguimientoLiquidacion instance = bancoRio.obtenerSolicitudLiquidacionPorFactura(factura.getId());
        if (instance != null) {
            return;
        }

        // Obtener datos de factura desembolsada.
        FacturaALiquidarInfo facturaALiquidar = bancoRio.obtenerDatosLiquidacion(factura.getId());
        if (facturaALiquidar == null) {
            throw Errors.illegalState("No se encontraron datos de liquidación para factura (Entidad id=%s, Comprador id=%s, Factura id=%s)", idEntidadFinanciera, idComprador, factura.getId());
        }

        // Obtener distribucion de comisiones.
        DistribucionComisionInfo distribucionComisiones = sepsa.calcularDistribucionComisiones(factura, idEntidadFinanciera);

        // Datos finales de detalle para liquidacion.
        BancoRioInfoLiquidacion facturaALiquidarBancoRio = new BancoRioInfoLiquidacion();
        facturaALiquidarBancoRio.factura = factura;
        facturaALiquidarBancoRio.facturaALiquidar = facturaALiquidar;
        facturaALiquidarBancoRio.distribucionComision = distribucionComisiones;

        // Enviar liquidacion a entidad.
        solicitarLiquidacionFactura0(idComprador, facturaALiquidarBancoRio);
    }

    private void solicitarLiquidacionFactura0(Integer idComprador, BancoRioInfoLiquidacion facturaALiquidarBancoRio) throws Throwable {
        // Obtener o crear registro de liquidacion.
        BancoRioSeguimientoLiquidacion infoSeguimiento = bancoRio.registrarSolicitudLiquidacion(facturaALiquidarBancoRio.factura.getId());

        // Obtener datos.
        Persona comprador = personas.obtenerPorId(idComprador).value();
        DocumentoInfo compradorDocumento = bancoRio.resolveDocumento(comprador);
        ComisionCompradorInfo comisionComprador = obtenerComisionComprador(idComprador, facturaALiquidarBancoRio);
        ComisionSepsaInfo comisionSepsa = obtenerComisionSepsa(facturaALiquidarBancoRio);
        ComisionBancoInfo comisionBanco = obtenerComisionBanco(facturaALiquidarBancoRio);
        Collection<FacturaALiquidarInfo> facturasALiquidar = Lists.wrap(facturaALiquidarBancoRio.facturaALiquidar);

        // Actualizar informacion de peticiones para seguimiento.
        infoSeguimiento.setFechaSolicitud(Fechas.now());
        jpaUtils.edit(infoSeguimiento);

        // Solicitar liquidacion.
        SolicitudLiquidacionInput input = new SolicitudLiquidacionInput();
        input.setComprador(comprador);
        input.setCompradorDocumento(compradorDocumento);
        input.setComisionComprador(comisionComprador);
        input.setComisionSepsa(comisionSepsa);
        input.setComisionBanco(comisionBanco);
        input.setFacturasALiquidar(facturasALiquidar);
        Result<SolicitudLiquidacionOutput> resLiquidacion = bancoRio.solicitarLiquidacion(input);

        // Control.
        if (resLiquidacion.isFailure()) {
            resLiquidacion.raise();
        }

        // Control.
        SolicitudLiquidacionOutput output = resLiquidacion.value();
        if (output.getRespuesta().isOk()) {
            // Actualizar informacion de peticiones para seguimiento.
            infoSeguimiento.setIdLiquidacion(resLiquidacion.value().getRespuesta().idLiquidacion);
            infoSeguimiento.setFechaRespuesta(resLiquidacion.value().getTrack().getRequestFin());
            jpaUtils.edit(infoSeguimiento);
        }
    }

    private ComisionCompradorInfo obtenerComisionComprador(Integer idComprador, BancoRioInfoLiquidacion facturaALiquidar) {
        // Obtener perfil de comprador.
        Perfil perfil = perfiles.obtenerPerfil(PerfilEnum.COMPRADOR);
        Integer idPerfil = perfil.getId();

        // Obtener monto total de comision.
        BigDecimal montoTotalComision = facturaALiquidar.distribucionComision.getMontoComisionComprador();

        // Obtener porcentaje total de comision.
        BigDecimal porcentajeTotalComision = facturaALiquidar.distribucionComision.getPorcentajeComisionComprador();

        // Obtener numero de cuenta.
        BancoRioPersona persona = bancoRio.obtenerPersona(idComprador, idPerfil).value();
        String nroCuenta = persona.getNroCuenta();

        // Datos de comision.
        ComisionCompradorInfo info = new ComisionCompradorInfo();
        info.setMontoComision(montoTotalComision);
        info.setPorcentajeComision(porcentajeTotalComision);
        info.setNroCuentaCredito(nroCuenta);
        return info;
    }

    private ComisionSepsaInfo obtenerComisionSepsa(BancoRioInfoLiquidacion facturaALiquidar) {
        // Obtener monto total de comision.
        BigDecimal montoTotalComision = facturaALiquidar.distribucionComision.getMontoComisionSepsa();

        // Obtener porcentaje total de comision.
        BigDecimal porcentajeTotalComision = facturaALiquidar.distribucionComision.getPorcentajeComisionSepsa();

        // Obtener numero de cuenta.
        Result<String> resCuenta = configuraciones.buscarValorParametro(ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_LIQUIDACION_CUENTA_SEPSA);
        String nroCuenta = resCuenta.value("");
        if (Assertions.stringNullOrEmpty(nroCuenta)) {
            throw Errors.illegalState("No se encontró una configuración del parámetro '%s'", ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_LIQUIDACION_CUENTA_SEPSA.nombre());
        }

        // Datos de comision.
        ComisionSepsaInfo info = new ComisionSepsaInfo();
        info.setMontoComision(montoTotalComision);
        info.setPorcentajeComision(porcentajeTotalComision);
        info.setNroCuentaDebito(nroCuenta);
        return info;
    }

    private ComisionBancoInfo obtenerComisionBanco(BancoRioInfoLiquidacion facturaALiquidar) {
        // Obtener monto total de comision.
        BigDecimal montoTotalComision = facturaALiquidar.distribucionComision.getMontoComisionEntidad();

        // Obtener porcentaje total de comision.
        BigDecimal porcentajeTotalComision = facturaALiquidar.distribucionComision.getPorcentajeComisionEntidad();

        // Datos de comision.
        ComisionBancoInfo info = new ComisionBancoInfo();
        info.setMontoComision(montoTotalComision);
        info.setPorcentajeComision(porcentajeTotalComision);
        return info;
    }

    private static class BancoRioInfoLiquidacion {

        private FacturaInfo factura;
        private FacturaALiquidarInfo facturaALiquidar;
        private DistribucionComisionInfo distribucionComision;
    }

}
