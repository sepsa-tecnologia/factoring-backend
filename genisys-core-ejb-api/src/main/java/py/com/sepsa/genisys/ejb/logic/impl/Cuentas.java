/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.com.sepsa.genisys.ejb.entities.info.Cuenta;
import py.com.sepsa.genisys.ejb.entities.info.CuentaEntidadFinanciera;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;
import py.com.sepsa.genisys.ejb.utils.Persistence;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Cuentas", mappedName = "Cuentas")
@LocalBean
public class Cuentas implements Serializable {

    @PersistenceContext(unitName = Persistence.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @EJB
    private JpaUtils jpaUtils;

    public Result<Void> registrarCuentaEntidadFinanciera(Integer idPersona, Integer idEntidadFinanciera, String nroCuenta) {
        Result<Void> result;

        try {
            // Verificar si cuenta ya existe.
            boolean existe = cuentaExiste(idPersona, idEntidadFinanciera, nroCuenta);
            if (!existe) {
                registrarCuentaEntidadFinanciera0(idPersona, idEntidadFinanciera, nroCuenta);
            }

            result = Results.ok()
                    .nullable(true)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrio un error registrando cuenta de entidad financiera")
                    .tag("persona.id", idPersona)
                    .tag("entidad.id", idEntidadFinanciera)
                    .build();
        }

        return result;
    }

    private boolean cuentaExiste(Integer idPersona, Integer idEntidadFinanciera, String nroCuenta) throws Throwable {
        // Construir consulta.
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");
        builder.append("     count(*) as \"total\" ");
        builder.append(" from info.cuenta_entidad_financiera as ce ");
        builder.append(" left join info.cuenta as c on ce.id_cuenta = c.id ");
        builder.append(" left join info.entidad_financiera as e on ce.id_entidad_financiera = e.id_entidad_financiera ");
        builder.append(" where  ");
        builder.append("     1 = 1 ");
        builder.append("     and ce.nro_cuenta = '%s' ", nroCuenta);
        builder.append("     and ce.id_persona = %s ", idPersona);
        builder.append("     and ce.id_entidad_financiera = %s ", idEntidadFinanciera);
        builder.append(" ; ");

        // Ejecutar consulta.
        String query = builder.toString();
        Integer total = jpaUtils.getTotal(query);

        return total > 0;
    }

    private void registrarCuentaEntidadFinanciera0(Integer idPersona, Integer idEntidadFinanciera, String nroCuenta) throws Throwable {
        // Craar cuenta.
        // NOTE: El identificador de tipo de cuenta es igual al valor existente en mercurio.
        Cuenta cuenta = new Cuenta();
        cuenta.setIdPersona(idPersona);
        cuenta.setIdTipoCuenta(1);
        cuenta.setOrden(1);
        cuenta.setEstado("A");
        cuenta.setIdLocal(1);
        cuenta = jpaUtils.create(cuenta);

        // Asociar nro de cuenta con cuenta en entidad financiera.
        String query = "insert into info.cuenta_entidad_financiera (id_entidad_financiera, id_persona, id_cuenta, nro_cuenta, fecha_alta) values(%s, %s, %s, '%s', now());";
        query = Strings.format(query, idEntidadFinanciera, idPersona, cuenta.getId(), nroCuenta);
        jpaUtils.executeUpdate(query);
    }

    public List<CuentaEntidadFinanciera> obtenerCuentasFinancierasDisponibles(Integer idPersona) {
        return obtenerCuentasFinancierasDisponibles(idPersona, null);
    }

    public List<CuentaEntidadFinanciera> obtenerCuentasFinancierasDisponibles(Integer idPersona, Integer idEntidadFinanciera) {
        final List<CuentaEntidadFinanciera> cuentas = Lists.empty();

        try {
            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     ce.id_cuenta as \"idCuenta\", ");
            builder.append("     ce.id_entidad_financiera as \"idEntidadFinanciera\", ");
            builder.append("     coalesce(pj.razon_social, pf.nombre || ' ' || pf.apellido) as \"razonSocialEntidadFinanciera\", ");
            builder.append("     ce.id_persona as \"idPersona\", ");
            builder.append("     ce.nro_cuenta as \"nroCuenta\", ");
            builder.append("     ce.fecha_alta as \"fechaAlta\" ");
            builder.append(" from info.cuenta as c ");
            builder.append(" join info.cuenta_entidad_financiera as ce on c.id = ce.id_cuenta ");
            builder.append(" left join info.persona_fisica as pf on pf.id = ce.id_entidad_financiera ");
            builder.append(" left join info.persona_juridica as pj on pj.id = ce.id_entidad_financiera  ");
            builder.append(" where  ");
            builder.append("     1 = 1 ");
            builder.append("     and c.estado = 'A' ");
            builder.append("     and ce.id_persona = %s ", idPersona);
            if (idEntidadFinanciera != null) {
                builder.append(" and ce.id_entidad_financiera = %s ", idEntidadFinanciera);
            }
            builder.append(" ; ");

            // Ejecutar query.
            String query = builder.toString();
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Mapear resultados.
            if (!Assertions.isNullOrEmpty(rows)) {
                rows.stream().forEach(row -> {
                    Integer idCuenta0 = Maps.integer(row, "idCuenta");
                    Integer idEntidadFinanciera0 = Maps.integer(row, "idEntidadFinanciera");
                    String razonSocialEntidadFinanciera0 = Maps.string(row, "razonSocialEntidadFinanciera");
                    Integer idPersona0 = Maps.integer(row, "idPersona");
                    String nroCuenta0 = Maps.string(row, "nroCuenta");
                    Date fechaAlta0 = Maps.date(row, "fechaAlta");

                    CuentaEntidadFinanciera cuenta = new CuentaEntidadFinanciera();
                    cuenta.setIdCuenta(idCuenta0);
                    cuenta.setIdEntidadFinanciera(idEntidadFinanciera0);
                    cuenta.setRazonSocialEntidadFinanciera(razonSocialEntidadFinanciera0);
                    cuenta.setIdPersona(idPersona0);
                    cuenta.setNroCuenta(nroCuenta0);
                    cuenta.setFechaAlta(fechaAlta0);
                    cuentas.add(cuenta);
                });
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error obteniendo cuentas financieras para persona");
        }

        return cuentas;
    }

}
