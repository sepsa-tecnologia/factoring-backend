/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import java.util.UUID;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Ids {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Array de 32 enteros generados aleatoriamente que sirven como clave para
     * el algoritmo TEA.
     */
    private static final int[] TEA_CIPHER_KEY = {
        0x37ffbab9, 0x3c2cec4f, 0x9f203042, 0x73c0157b,
        0x8519678e, 0x073b8a36, 0xa95f1e96, 0x1c72e7b3,
        0xf76991da, 0x6e452c30, 0x6bb0ba07, 0x94f8f69e,
        0xc3c113d6, 0x1d56b1bc, 0x5550d56f, 0x3291e0d4,
        0x96d7f531, 0x674fdb85, 0x6460e17e, 0x69dd1877,
        0xffcdf8a0, 0x5e44e713, 0xbec49565, 0x137af54b,
        0xc189c66c, 0xae6beb71, 0xecec2bba, 0xdb24b32e,
        0xf39f6622, 0x8e789f64, 0xdd5de8b7, 0xe799ed0e
    };

    /**
     * Cifrador de valores. Se utiliza un algoritmo especifico para la
     * manipulacion de enteros.
     */
    private static final TeaCipher cipher;
    //</editor-fold>

    /**
     * Inicializador estatico.
     */
    static {
        cipher = new TeaCipher();
    }

    /**
     * Enmascara el valor de un entero, que representa una identificador de base
     * de datos, de manera a que no pueda ser manipulado directamente de manera
     * externa.
     *
     * @param id Identificador de base de datos.
     * @return Cadena hexadecimal con identificador enmascarado.
     */
    public static String mask(Integer id) {
        long input = id * 1L;
        long output = cipher.encrypt(input, TEA_CIPHER_KEY);
        return TeaCipher.long2string(output);
    }

    /**
     * Desenmascara el valor de una cadena hexadecimal, que representa un
     * identificador de base de datos enmascarado, de manera a obtener el valor
     * original dentro de la base de datos.
     *
     * @param idMasked Cadena hexadecimal de identificador enmascarado.
     * @return Identificador de base de datos.
     */
    public static Integer unmask(String idMasked) {
        long input = TeaCipher.string2long(idMasked);
        long output = cipher.decrypt(input, TEA_CIPHER_KEY);
        return (int) output;
    }

    /**
     * Genera un UUID aleatorio.
     *
     * @return UUID como cadena sin los separadores (guiones medios).
     */
    public static String randomUuid() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replace("-", "");
    }

}
