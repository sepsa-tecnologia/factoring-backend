/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioSeguimientoLiquidacion;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "BancoRioSeguimientoLiquidacionFacade", mappedName = "BancoRioSeguimientoLiquidacionFacade")
@LocalBean
public class BancoRioSeguimientoLiquidacionFacade extends AbstractFacade<BancoRioSeguimientoLiquidacion> {

    /**
     * Constructor.
     */
    public BancoRioSeguimientoLiquidacionFacade() {
        super(BancoRioSeguimientoLiquidacion.class);
    }

}
