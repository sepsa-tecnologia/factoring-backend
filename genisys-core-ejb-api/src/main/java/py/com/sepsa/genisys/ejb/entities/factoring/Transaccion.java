/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "transaccion", schema = "factoring")
public class Transaccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "id_documento")
    private Integer idDocumento;

    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;

    @Column(name = "id_factura_conformada")
    private Integer idFacturaConformada;

    @Column(name = "id_cuenta_ori")
    private Integer idCuentaOri;

    @Column(name = "id_cuenta_dest")
    private Integer idCuentaDest;

    @Column(name = "porcentaje")
    private BigDecimal porcentaje;

    @Column(name = "monto")
    private BigDecimal monto;

    @Column(name = "estado")
    private Character estado;

    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Integer getIdFacturaConformada() {
        return idFacturaConformada;
    }

    public void setIdFacturaConformada(Integer idFacturaConformada) {
        this.idFacturaConformada = idFacturaConformada;
    }

    public Integer getIdCuentaOri() {
        return idCuentaOri;
    }

    public void setIdCuentaOri(Integer idCuentaOri) {
        this.idCuentaOri = idCuentaOri;
    }

    public Integer getIdCuentaDest() {
        return idCuentaDest;
    }

    public void setIdCuentaDest(Integer idCuentaDest) {
        this.idCuentaDest = idCuentaDest;
    }

    public BigDecimal getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = porcentaje;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        hash = 67 * hash + Objects.hashCode(this.idDocumento);
        hash = 67 * hash + Objects.hashCode(this.idTipoDocumento);
        hash = 67 * hash + Objects.hashCode(this.idFacturaConformada);
        hash = 67 * hash + Objects.hashCode(this.idCuentaOri);
        hash = 67 * hash + Objects.hashCode(this.idCuentaDest);
        hash = 67 * hash + Objects.hashCode(this.porcentaje);
        hash = 67 * hash + Objects.hashCode(this.monto);
        hash = 67 * hash + Objects.hashCode(this.estado);
        hash = 67 * hash + Objects.hashCode(this.fecha);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transaccion other = (Transaccion) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.idDocumento, other.idDocumento)) {
            return false;
        }
        if (!Objects.equals(this.idTipoDocumento, other.idTipoDocumento)) {
            return false;
        }
        if (!Objects.equals(this.idFacturaConformada, other.idFacturaConformada)) {
            return false;
        }
        if (!Objects.equals(this.idCuentaOri, other.idCuentaOri)) {
            return false;
        }
        if (!Objects.equals(this.idCuentaDest, other.idCuentaDest)) {
            return false;
        }
        if (!Objects.equals(this.porcentaje, other.porcentaje)) {
            return false;
        }
        if (!Objects.equals(this.monto, other.monto)) {
            return false;
        }
        if (!Objects.equals(this.estado, other.estado)) {
            return false;
        }
        if (!Objects.equals(this.fecha, other.fecha)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Transaccion{" + "id=" + id + ", idDocumento=" + idDocumento + ", idTipoDocumento=" + idTipoDocumento + ", idFacturaConformada=" + idFacturaConformada + ", idCuentaOri=" + idCuentaOri + ", idCuentaDest=" + idCuentaDest + ", porcentaje=" + porcentaje + ", monto=" + monto + ", estado=" + estado + ", fecha=" + fecha + '}';
    }

}
