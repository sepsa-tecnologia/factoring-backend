/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import java.util.Collection;
import java.util.Objects;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.facades.factoring.PerfilFacade;
import py.com.sepsa.genisys.ejb.logic.LogicBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Perfiles", mappedName = "Perfiles")
@LocalBean
public class Perfiles extends LogicBean {

    @EJB
    private PerfilFacade facade;

    /**
     * Obtiene una enumeracion de perfil en base a una entidad de perfil.
     *
     * @param perfil Entidad perfil.
     * @return Enumeracion de perfil.
     */
    public PerfilEnum obtenerPerfil(Perfil perfil) {
        for (PerfilEnum perfilEnum : PerfilEnum.values()) {
            if (Objects.equals(perfilEnum.descripcion(), perfil.getDescripcion())) {
                return perfilEnum;
            }
        }
        return null;
    }

    /**
     * Obtiene un perfil en base a una enumeracion de perfil.
     *
     * @param perfilEnum Enumeracion de perfil.
     * @return Entidad perfil.
     */
    public Perfil obtenerPerfil(PerfilEnum perfilEnum) {
        return facade.findByDescripcion(perfilEnum.descripcion());
    }

    /**
     * Obtiene todos los perfiles disponibles.
     *
     * @return Coleccion de perfiles disponibles.
     */
    public Collection<Perfil> obtenerPerfiles() {
        return facade.findAll();
    }

}
