/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class JobStamp implements Serializable {

    private String name;
    private Long lastRun;
    private Long lastPeriod;
    private long runs;
    private long ellapsed;
}
