/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import fa.gs.criteria.query.Operators;
import fa.gs.criteria.query.Order;
import fa.gs.criteria.query.QueryCriteria;
import fa.gs.utils.misc.errors.Errors;
import java.util.Map;
import java.util.regex.Pattern;
import java8.util.Objects;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Criterias {

    public static QueryCriteria instance(int limit, int offset, Map<String, Order> sortBy, Map<String, Object> filterBy) {
        QueryCriteria instance = QueryCriteria.instance();
        instance.setLimit(limit);
        instance.setOffset(offset);
        if (sortBy != null) {
            sortBy.entrySet().forEach(e -> {
                instance.order(e.getKey(), e.getValue());
            });
        }
        if (filterBy != null) {
            filterBy.entrySet().forEach(e -> {
                String[] parts = e.getKey().split(Pattern.quote("$"));
                if (parts != null && parts.length == 2) {
                    String R = parts[0].trim();
                    Operators O = adapt(parts[1].trim());
                    instance.where(R, O, e.getValue());
                } else {
                    instance.where(e.getKey(), Operators.EQUALS, e.getValue());
                }
            });
        }
        return instance;
    }

    private static Operators adapt(String op) {
        for (Operators operator : Operators.values()) {
            if (Objects.equals(operator.toString(), op)) {
                return operator;
            }
        }
        throw Errors.unsupported("Operador no soportado '%s'", op);
    }

}
