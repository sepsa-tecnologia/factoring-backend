/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.factoring;

import fa.gs.utils.collections.Maps;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class ConvenioInfo implements Serializable {

    private Integer id;
    private Date fechaAlta;
    private Integer idTipoConvenio;
    private String descripcionTipoConvenio;
    private Integer idComprador;
    private String razonSocialComprador;
    private Integer idProveedor;
    private String razonSocialProveedor;
    private Integer idEntidadFinanciera;
    private String razonSocialEntidadFinanciera;

    public static ConvenioInfo instance(Map<String, Object> row) {
        ConvenioInfo instance = new ConvenioInfo();
        instance.id = Maps.integer(row, "convenio_id");
        instance.fechaAlta = Maps.date(row, "convenio_fecha_alta");
        instance.idTipoConvenio = Maps.integer(row, "convenio_tipo_id");
        instance.descripcionTipoConvenio = Maps.string(row, "convenio_tipo_descripcion");
        instance.idComprador = Maps.integer(row, "comprador_id");
        instance.razonSocialComprador = Maps.string(row, "comprador_razon_social");
        instance.idProveedor = Maps.integer(row, "proveedor_id");
        instance.razonSocialProveedor = Maps.string(row, "proveedor_razon_social");
        instance.idEntidadFinanciera = Maps.integer(row, "entidad_financiera_id");
        instance.razonSocialEntidadFinanciera = Maps.string(row, "entidad_financiera_razon_social");
        return instance;
    }

}
