/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.info;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.info.PersonaFisica;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author descauriza
 */
@Stateless(name = "PersonaFisicaFacade", mappedName = "PersonaFisicaFacade")
@LocalBean
public class PersonaFisicaFacade extends AbstractFacade<PersonaFisica> {

    public PersonaFisicaFacade() {
        super(PersonaFisica.class);
    }

}
