/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.fechas.Fechas;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoring;
import py.com.sepsa.genisys.ejb.enums.TipoPlantilla;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
@Stateless(name = "PF_NotificarFacturasConformadas", mappedName = "PF_NotificarFacturasConformadas")
@LocalBean
public class PF_NotificarFacturasConformadas implements Serializable {

    @EJB
    private Facturas facturas;

    @EJB
    private Notificaciones notificaciones;

    @EJB
    private Personas personas;

    public void work() throws Throwable {
        // Listados de proveedores activos
        Result<Collection<Persona>> resProveedores = personas.obtenerProveedoresDisponibles();
        resProveedores.raise();
        Collection<Persona> proveedores = resProveedores.value();

        // Día límite para última notificación.
        Integer intervaloDias = notificaciones.obtenerEsperaNotificaciones();
        Date fechaDesde = Fechas.addValue(Fechas.now(), Calendar.DAY_OF_MONTH, -1 * Math.abs(intervaloDias));

        // Obtiene la última notificación enviada a cada proveedor.
        for (Persona persona : proveedores) {
            Result<List<FacturaInfo>> resFacturasConformadas = facturas.obtenerFacturasConformadasVigentes(persona.getId());
            resFacturasConformadas.raise();
            List<FacturaInfo> facturasConformadas = resFacturasConformadas.value();
            if (!Assertions.isNullOrEmpty(facturasConformadas)) {
                Result<NotificacionEmailFactoring> resNotificacion = notificaciones.obtenerUltimaNotificacionPersona(persona.getId(), TipoPlantilla.RECORDATORIO_FACTURAS_CONFORMADAS);
                resNotificacion.raise();
                NotificacionEmailFactoring notificacion = resNotificacion.value();
                // Si ya pasó el intervalo de días luego de la última notificación, se genera una nueva notificación
                if (Assertions.isNull(notificacion) || Fechas.menor(notificacion.getFechaInsercion(), fechaDesde)) {
                    notificaciones.crearNotificacionFacturasConformadas(facturasConformadas);
                }
            }
        }
    }

}
