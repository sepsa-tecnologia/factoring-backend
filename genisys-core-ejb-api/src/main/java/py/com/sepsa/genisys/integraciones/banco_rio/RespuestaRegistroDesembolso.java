/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.utils.misc.text.Text;
import fa.gs.utils.misc.xml.Xml;
import fa.gs.utils.misc.xml.XmlPrefixInfoList;
import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Pattern;
import javax.xml.xpath.XPath;
import lombok.Data;
import org.w3c.dom.Document;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class RespuestaRegistroDesembolso implements Serializable {

    String respuestaEstado;
    String respuestaMensaje;
    String respuestaId;

    public static RespuestaRegistroDesembolso parse(String xmlSoap) throws Throwable {
        // Eliminar el namespace que no utiliza ningun prefijo.
        xmlSoap = Text.replaceAll(xmlSoap, "xmlns=\"http://tempuri.org/\"", "");

        // Cargar XML de respuesta SOAP.
        Document xml = Xml.parse(xmlSoap);

        // Obtener prefijos y uris.
        XmlPrefixInfoList prefixes = Xml.readPrefixes(xml);

        // Preparar consultas XPath.
        XPath xpath = Xml.xpath(prefixes);
        String xEstado = ".//estado";
        String xMensaje = ".//mensaje";

        // Obtener valor.
        String estado = Xml.readNodeTexContent(xpath, xEstado, xml);
        String mensaje = Xml.readNodeTexContent(xpath, xMensaje, xml);

        // Procesar valores.
        RespuestaRegistroDesembolso respuesta = new RespuestaRegistroDesembolso();
        if (Objects.equals(estado, "0")) {
            respuesta.respuestaEstado = "0";
            respuesta.respuestaMensaje = mensaje;
            respuesta.respuestaId = mensaje.split(Pattern.quote(":"))[1].trim();
        } else {
            respuesta.respuestaEstado = estado;
            respuesta.respuestaMensaje = mensaje;
            respuesta.respuestaId = "";
        }

        return respuesta;
    }

}
