/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.util.Collection;
import javax.ejb.EJB;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculoDesembolsoFacturaInfo;
import py.com.sepsa.genisys.ejb.logic.pojos.SolicitudDesembolsoResultadoInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
public class PF_FacturaVentaLote_Base extends LogicBean implements Serializable {

    @EJB
    protected Notificaciones notificaciones;

    @EJB
    protected Facturas facturas;

    @EJB
    protected Personas personas;

    @EJB
    protected Perfiles perfiles;

    protected CalculoDesembolsoFacturaInfo buscarCalculo(Collection<CalculoDesembolsoFacturaInfo> calculos, FacturaInfo factura) {
        for (CalculoDesembolsoFacturaInfo calculo : calculos) {
            if (calculo.getFactura().getId().equals(factura.getId())) {
                return calculo;
            }
        }
        return null;
    }

    protected SolicitudDesembolsoResultadoInfo venderKo(String fmt, Object... args) {
        SolicitudDesembolsoResultadoInfo instance = new SolicitudDesembolsoResultadoInfo();
        instance.setOk(false);
        instance.setErr(Strings.format(fmt, args));
        return instance;
    }

    protected Result<Factura> cambiarEstadoFactura(Integer idUsuario, Integer idFactura, Estados.EstadoFacturaEnum nuevoEstado, String comentario) {
        // Cambio de estado.
        Result<Factura> resCambioEstado = facturas.cambiarEstado(idUsuario, idFactura, nuevoEstado);
        if (resCambioEstado.isFailure()) {
            return resCambioEstado;
        }

        // Registro de historico.
        Factura factura0 = resCambioEstado.value();
        Result<HistoricoFactura> resHistorico = facturas.agregarHistorico(idUsuario, factura0, nuevoEstado, comentario);
        if (resHistorico.isFailure()) {
            return Results.ko()
                    .cause(resHistorico.failure().cause())
                    .build();
        } else {
            return Results.ok()
                    .value(factura0)
                    .build();
        }
    }

}
