/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabio A. González Sosa
 */
@Entity
@Table(name = "persona_rol", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PersonaRol.findAll", query = "SELECT p FROM PersonaRol p"),
    @NamedQuery(name = "PersonaRol.findByIdPersona", query = "SELECT p FROM PersonaRol p WHERE p.personaRolPK.idPersona = :idPersona"),
    @NamedQuery(name = "PersonaRol.findByIdRol", query = "SELECT p FROM PersonaRol p WHERE p.personaRolPK.idRol = :idRol"),
    @NamedQuery(name = "PersonaRol.findByEstado", query = "SELECT p FROM PersonaRol p WHERE p.estado = :estado"),
    @NamedQuery(name = "PersonaRol.findByPersonaRol", query = "SELECT p FROM PersonaRol p WHERE p.personaRolPK.idPersona = :idPersona and p.personaRolPK.idRol = :idRol and p.estado = 'A'")
})
public class PersonaRol implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PersonaRolPK personaRolPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private Character estado;
    @JoinColumn(name = "id_rol", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Rol rol;

    public PersonaRol() {
    }

    public PersonaRol(PersonaRolPK personaRolPK) {
        this.personaRolPK = personaRolPK;
    }

    public PersonaRol(PersonaRolPK personaRolPK, Character estado) {
        this.personaRolPK = personaRolPK;
        this.estado = estado;
    }

    public PersonaRol(int idPersona, int idRol) {
        this.personaRolPK = new PersonaRolPK(idPersona, idRol);
    }

    public PersonaRolPK getPersonaRolPK() {
        return personaRolPK;
    }

    public void setPersonaRolPK(PersonaRolPK personaRolPK) {
        this.personaRolPK = personaRolPK;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personaRolPK != null ? personaRolPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof PersonaRol)) {
            return false;
        }
        PersonaRol other = (PersonaRol) object;
        if ((this.personaRolPK == null && other.personaRolPK != null) || (this.personaRolPK != null && !this.personaRolPK.equals(other.personaRolPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.siediweb.ejb.entities.trans.PersonaRol[ personaRolPK=" + personaRolPK + " ]";
    }

}
