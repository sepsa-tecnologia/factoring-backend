/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.enums;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum TipoConvenioEnum {
    NORMAL("NORMAL");
    private final String descripcion;

    TipoConvenioEnum(String descripcion) {
        this.descripcion = descripcion;
    }

    public String descripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return descripcion();
    }

}
