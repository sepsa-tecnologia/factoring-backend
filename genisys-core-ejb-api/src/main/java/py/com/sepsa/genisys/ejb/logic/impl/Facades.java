/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.facades.comercial.ClienteFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.ArchivoAdjuntoFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.ArchivoFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.ConfigCargoFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.ConfiguracionFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.ConvenioFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.EstadoFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.FacturaConformadaFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.FacturaFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.HistoricoConfigCargoFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.HistoricoFacturaFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.LineaCreditoFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.LineaCreditoHistoricoFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.LoteFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.NotaCreditoFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.PreguntasFrecuentesFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.RetencionFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.TipoArchivosFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.TipoCargoFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.TipoConvenioFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.TipoValorFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.TransaccionFacade;
import py.com.sepsa.genisys.ejb.facades.factoring.UsuarioPerfilFacade;
import py.com.sepsa.genisys.ejb.facades.info.ContactoEmailFacade;
import py.com.sepsa.genisys.ejb.facades.info.EmailFacade;
import py.com.sepsa.genisys.ejb.facades.info.EntidadFinancieraFacade;
import py.com.sepsa.genisys.ejb.facades.info.MonedaFacade;
import py.com.sepsa.genisys.ejb.facades.notificacion.NotificacionEmailFactoringFacade;
import py.com.sepsa.genisys.ejb.facades.notificacion.NotificacionEmailFactoringFacturaFacade;
import py.com.sepsa.genisys.ejb.facades.notificacion.PlantillaFacade;
import py.com.sepsa.genisys.ejb.facades.trans.ProveedorCompradorFacade;
import py.com.sepsa.genisys.ejb.facades.trans.UsuarioFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Facades", mappedName = "Facades")
@LocalBean
public class Facades implements Serializable {

    // <editor-fold desc="Facades para manipulacion de entidades">
    @EJB
    private UsuarioFacade usuarioFacade;

    @EJB
    private TipoArchivosFacade tipoArchivosFacade;

    @EJB
    private ArchivoFacade archivoFacade;

    @EJB
    private ArchivoAdjuntoFacade archivoAdjuntoFacade;

    @EJB
    private UsuarioPerfilFacade usuarioPerfilFacade;

    @EJB
    private MonedaFacade monedaFacade;

    @EJB
    private FacturaFacade facturaFacade;

    @EJB
    private LoteFacade loteFacade;

    @EJB
    private FacturaConformadaFacade facturaConformadaFacade;

    @EJB
    private NotaCreditoFacade notaCreditoFacade;

    @EJB
    private ProveedorCompradorFacade proveedorCompradorFacade;

    @EJB
    private EntidadFinancieraFacade entidadFinancieraFacade;

    @EJB
    private ConvenioFacade convenioFacade;

    @EJB
    private TipoConvenioFacade tipoConvenioFacade;

    @EJB
    private RetencionFacade retencionFacade;

    @EJB
    private EstadoFacade estadoFacade;

    @EJB
    private HistoricoFacturaFacade historicoFacturaFacade;

    @EJB
    private HistoricoConfigCargoFacade HistoricoConfigCargoFacade;

    @EJB
    private PreguntasFrecuentesFacade preguntasFrecuentesFacade;

    @EJB
    private ConfigCargoFacade configCargoFacade;

    @EJB
    private TipoCargoFacade tipoCargoFacade;

    @EJB
    private TipoValorFacade tipoValorFacade;

    @EJB
    private TransaccionFacade transaccionFacade;

    @EJB
    private NotificacionEmailFactoringFacade notificacionEmailFactoringFacade;

    @EJB
    private NotificacionEmailFactoringFacturaFacade notificacionEmailFactoringFacturaFacade;

    @EJB
    private PlantillaFacade plantillaFacade;

    @EJB
    private ClienteFacade clienteFacade;

    @EJB
    private ContactoEmailFacade contactoEmailFacade;

    @EJB
    private EmailFacade emailFacade;

    @EJB
    private LineaCreditoFacade lineaCreditoFacade;

    @EJB
    private LineaCreditoHistoricoFacade lineaCreditoHistoricoFacade;

    @EJB
    private ConfiguracionFacade configuracionFacade;
    // </editor-fold>

    // <editor-fold desc="Getters y Setters">
    public UsuarioFacade getUsuarioFacade() {
        return usuarioFacade;
    }

    public void setUsuarioFacade(UsuarioFacade usuarioFacade) {
        this.usuarioFacade = usuarioFacade;
    }

    public TipoArchivosFacade getTipoArchivosFacade() {
        return tipoArchivosFacade;
    }

    public void setTipoArchivosFacade(TipoArchivosFacade tipoArchivosFacade) {
        this.tipoArchivosFacade = tipoArchivosFacade;
    }

    public ArchivoFacade getArchivoFacade() {
        return archivoFacade;
    }

    public void setArchivoFacade(ArchivoFacade archivoFacade) {
        this.archivoFacade = archivoFacade;
    }

    public ArchivoAdjuntoFacade getArchivoAdjuntoFacade() {
        return archivoAdjuntoFacade;
    }

    public void setArchivoAdjuntoFacade(ArchivoAdjuntoFacade archivoAdjuntoFacade) {
        this.archivoAdjuntoFacade = archivoAdjuntoFacade;
    }

    public UsuarioPerfilFacade getUsuarioPerfilFacade() {
        return usuarioPerfilFacade;
    }

    public void setUsuarioPerfilFacade(UsuarioPerfilFacade usuarioPerfilFacade) {
        this.usuarioPerfilFacade = usuarioPerfilFacade;
    }

    public MonedaFacade getMonedaFacade() {
        return monedaFacade;
    }

    public void setMonedaFacade(MonedaFacade monedaFacade) {
        this.monedaFacade = monedaFacade;
    }

    public FacturaFacade getFacturaFacade() {
        return facturaFacade;
    }

    public void setFacturaFacade(FacturaFacade facturaFacade) {
        this.facturaFacade = facturaFacade;
    }

    public LoteFacade getLoteFacade() {
        return loteFacade;
    }

    public void setLoteFacade(LoteFacade loteFacade) {
        this.loteFacade = loteFacade;
    }

    public FacturaConformadaFacade getFacturaConformadaFacade() {
        return facturaConformadaFacade;
    }

    public void setFacturaConformadaFacade(FacturaConformadaFacade facturaConformadaFacade) {
        this.facturaConformadaFacade = facturaConformadaFacade;
    }

    public NotaCreditoFacade getNotaCreditoFacade() {
        return notaCreditoFacade;
    }

    public void setNotaCreditoFacade(NotaCreditoFacade notaCreditoFacade) {
        this.notaCreditoFacade = notaCreditoFacade;
    }

    public ProveedorCompradorFacade getProveedorCompradorFacade() {
        return proveedorCompradorFacade;
    }

    public void setProveedorCompradorFacade(ProveedorCompradorFacade proveedorCompradorFacade) {
        this.proveedorCompradorFacade = proveedorCompradorFacade;
    }

    public EntidadFinancieraFacade getEntidadFinancieraFacade() {
        return entidadFinancieraFacade;
    }

    public void setEntidadFinancieraFacade(EntidadFinancieraFacade entidadFinancieraFacade) {
        this.entidadFinancieraFacade = entidadFinancieraFacade;
    }

    public ConvenioFacade getConvenioFacade() {
        return convenioFacade;
    }

    public void setConvenioFacade(ConvenioFacade convenioFacade) {
        this.convenioFacade = convenioFacade;
    }

    public TipoConvenioFacade getTipoConvenioFacade() {
        return tipoConvenioFacade;
    }

    public void setTipoConvenioFacade(TipoConvenioFacade tipoConvenioFacade) {
        this.tipoConvenioFacade = tipoConvenioFacade;
    }

    public RetencionFacade getRetencionFacade() {
        return retencionFacade;
    }

    public void setRetencionFacade(RetencionFacade retencionFacade) {
        this.retencionFacade = retencionFacade;
    }

    public EstadoFacade getEstadoFacade() {
        return estadoFacade;
    }

    public void setEstadoFacade(EstadoFacade estadoFacade) {
        this.estadoFacade = estadoFacade;
    }

    public HistoricoFacturaFacade getHistoricoFacturaFacade() {
        return historicoFacturaFacade;
    }

    public void setHistoricoFacturaFacade(HistoricoFacturaFacade historicoFacturaFacade) {
        this.historicoFacturaFacade = historicoFacturaFacade;
    }

    public HistoricoConfigCargoFacade getHistoricoConfigCargoFacade() {
        return HistoricoConfigCargoFacade;
    }

    public void setHistoricoConfigCargoFacade(HistoricoConfigCargoFacade HistoricoConfigCargoFacade) {
        this.HistoricoConfigCargoFacade = HistoricoConfigCargoFacade;
    }

    public PreguntasFrecuentesFacade getPreguntasFrecuentesFacade() {
        return preguntasFrecuentesFacade;
    }

    public void setPreguntasFrecuentesFacade(PreguntasFrecuentesFacade preguntasFrecuentesFacade) {
        this.preguntasFrecuentesFacade = preguntasFrecuentesFacade;
    }

    public ConfigCargoFacade getConfigCargoFacade() {
        return configCargoFacade;
    }

    public void setConfigCargoFacade(ConfigCargoFacade configCargoFacade) {
        this.configCargoFacade = configCargoFacade;
    }

    public TipoCargoFacade getTipoCargoFacade() {
        return tipoCargoFacade;
    }

    public void setTipoCargoFacade(TipoCargoFacade tipoCargoFacade) {
        this.tipoCargoFacade = tipoCargoFacade;
    }

    public TipoValorFacade getTipoValorFacade() {
        return tipoValorFacade;
    }

    public void setTipoValorFacade(TipoValorFacade tipoValorFacade) {
        this.tipoValorFacade = tipoValorFacade;
    }

    public TransaccionFacade getTransaccionFacade() {
        return transaccionFacade;
    }

    public void setTransaccionFacade(TransaccionFacade transaccionFacade) {
        this.transaccionFacade = transaccionFacade;
    }

    public NotificacionEmailFactoringFacade getNotificacionEmailFactoringFacade() {
        return notificacionEmailFactoringFacade;
    }

    public void setNotificacionEmailFactoringFacade(NotificacionEmailFactoringFacade notificacionEmailFactoringFacade) {
        this.notificacionEmailFactoringFacade = notificacionEmailFactoringFacade;
    }

    public NotificacionEmailFactoringFacturaFacade getNotificacionEmailFactoringFacturaFacade() {
        return notificacionEmailFactoringFacturaFacade;
    }

    public void setNotificacionEmailFactoringFacturaFacade(NotificacionEmailFactoringFacturaFacade notificacionEmailFactoringFacturaFacade) {
        this.notificacionEmailFactoringFacturaFacade = notificacionEmailFactoringFacturaFacade;
    }

    public PlantillaFacade getPlantillaFacade() {
        return plantillaFacade;
    }

    public void setPlantillaFacade(PlantillaFacade plantillaFacade) {
        this.plantillaFacade = plantillaFacade;
    }

    public ClienteFacade getClienteFacade() {
        return clienteFacade;
    }

    public void setClienteFacade(ClienteFacade clienteFacade) {
        this.clienteFacade = clienteFacade;
    }

    public ContactoEmailFacade getContactoEmailFacade() {
        return contactoEmailFacade;
    }

    public void setContactoEmailFacade(ContactoEmailFacade contactoEmailFacade) {
        this.contactoEmailFacade = contactoEmailFacade;
    }

    public EmailFacade getEmailFacade() {
        return emailFacade;
    }

    public void setEmailFacade(EmailFacade emailFacade) {
        this.emailFacade = emailFacade;
    }

    public LineaCreditoFacade getLineaCreditoFacade() {
        return lineaCreditoFacade;
    }

    public void setLineaCreditoFacade(LineaCreditoFacade lineaCreditoFacade) {
        this.lineaCreditoFacade = lineaCreditoFacade;
    }

    public LineaCreditoHistoricoFacade getLineaCreditoHistoricoFacade() {
        return lineaCreditoHistoricoFacade;
    }

    public void setLineaCreditoHistoricoFacade(LineaCreditoHistoricoFacade lineaCreditoHistoricoFacade) {
        this.lineaCreditoHistoricoFacade = lineaCreditoHistoricoFacade;
    }

    public ConfiguracionFacade getConfiguracionFacade() {
        return configuracionFacade;
    }

    public void setConfiguracionFacade(ConfiguracionFacade configuracionFacade) {
        this.configuracionFacade = configuracionFacade;
    }
    // </editor-fold>

}
