/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.errors.Errors;
import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraInput;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraOutput;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.CalculoNetoFacturasInput;
import py.com.sepsa.genisys.integraciones.banco_rio.CalculoNetoFacturasOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.RespuestaCalculoNetoFacturas;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.FacturaInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Calculador_BancoRio extends Calculador_Base {

    private CalculadoraInput calculadoraInput;
    private CalculadoraOutput calculadoraOutput;

    public Calculador_BancoRio() {
        this.calculadoraOutput = null;
    }

    @Override
    public String validar() {
        Perfiles perfiles = Injection.lookup(Perfiles.class);
        Entidades entidades = Injection.lookup(Entidades.class);

        Perfil perfilComprador = perfiles.obtenerPerfil(PerfilEnum.COMPRADOR);
        Perfil perfilProveedor = perfiles.obtenerPerfil(PerfilEnum.PROVEEDOR);

        // Validacion por defecto.
        String tmp = validarDatosBase();
        if (tmp != null) {
            return tmp;
        }

        final CalculadoraInput input = getCalculadoraInput();

        // Comprador debe estar adherida a entidad.
        Result<Entidades.EstadoAdhesionEntidad> resAdhesionComprador = entidades.obtenerEstadoAdhesion(input.getComprador().getId(), perfilComprador.getId(), input.getEntidadFinanciera().getId());
        Entidades.EstadoAdhesionEntidad adhesionComprador = resAdhesionComprador.value(null);
        if (adhesionComprador == null || adhesionComprador != Entidades.EstadoAdhesionEntidad.ACTIVO) {
            return "El comprador no está adherido a la entidad financiera.";
        }

        // Proveedor debe estar adherida a entidad.
        Result<Entidades.EstadoAdhesionEntidad> resAdhesionProveedor = entidades.obtenerEstadoAdhesion(input.getProveedor().getId(), perfilProveedor.getId(), input.getEntidadFinanciera().getId());
        Entidades.EstadoAdhesionEntidad adhesionProveedor = resAdhesionProveedor.value(null);
        if (adhesionProveedor == null || adhesionProveedor != Entidades.EstadoAdhesionEntidad.ACTIVO) {
            return "El proveedor no está adherido a la entidad financiera.";
        }

        return null;
    }

    @Override
    public void calcular() throws Throwable {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        Convenios convenios = Injection.lookup(Convenios.class);
        PF_FacturaVentaLote_BancoRio ventaLote = Injection.lookup(PF_FacturaVentaLote_BancoRio.class);

        // Datos de factura.
        FacturaInfo factura = new FacturaInfo();
        factura.setNumero("000-000-0000000");
        factura.setMonto(calculadoraInput.getMontoFactura());
        factura.setFechaVencimiento(calculadoraInput.getFechaPagoFactura());

        // Adaptar datos de entrada.
        CalculoNetoFacturasInput input = new CalculoNetoFacturasInput();
        input.setDocumentoComprador(bancoRio.resolveDocumento(this.calculadoraInput.getComprador()));
        input.setDocumentoProveedor(bancoRio.resolveDocumento(this.calculadoraInput.getProveedor()));
        input.setPorcentajeUsoPlataforma(convenios.obtenerPorcentajeUsoPlataforma());
        input.setFacturas(Lists.empty());
        input.getFacturas().add(factura);

        // Consumir servicio.
        Integer idPersona = null;
        Integer idPerfil = null;
        Result<CalculoNetoFacturasOutput> resOutput = bancoRio.calculoNetoFacturas(idPersona, idPerfil, this.calculadoraInput.getEntidadFinanciera().getId(), input);
        resOutput.raise();

        // Control.
        CalculoNetoFacturasOutput output = resOutput.value();
        if (!output.getRespuesta().isOk()) {
            String msg = bancoRio.resolveErrorMessage(output.getTrack(), output.getRespuesta());
            throw Errors.illegalState(msg);
        }

        // Adaptar datos de salida.
        RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura detalleCalculo = Lists.first(output.getRespuesta().getDetalles());
        this.calculadoraOutput = new CalculadoraOutput.Impl();
        this.calculadoraOutput.setDiasAdelantados(detalleCalculo.getDiasInteres());
        this.calculadoraOutput.setPorcentajeTotalComision(ventaLote.obtenerPorcentajeDescuento(detalleCalculo));
        this.calculadoraOutput.setMontoTotalComision(ventaLote.obtenerMontoDescuento(detalleCalculo));
        this.calculadoraOutput.setMontoTotalDesembolso(detalleCalculo.getNeto());
        this.calculadoraOutput.setExtras(detalleCalculo);
    }

}
