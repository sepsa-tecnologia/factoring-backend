/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.security;

import com.google.gson.JsonObject;
import fa.gs.result.simple.Result;
import fa.gs.utils.authentication.AbstractUserTokenAuthenticator;
import fa.gs.utils.authentication.tokens.TokenDecoder;
import fa.gs.utils.authentication.tokens.simple.SimpleTokenManager;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.JsonResolver;
import java.util.Collection;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.impl.Sesiones;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class UserTokenAuthenticator extends AbstractUserTokenAuthenticator<JsonObject> {

    private static final String ID_USUARIO_JSON_PATH = "usuario$id";

    private SimpleTokenManager tokenManager;

    private Sesiones sesiones;

    private Personas personas;

    public UserTokenAuthenticator() {
        this.sesiones = Injection.lookup(Sesiones.class);
        this.personas = Injection.lookup(Personas.class);
        this.tokenManager = new SimpleTokenManager();
    }

    @Override
    protected TokenDecoder<JsonObject> getTokenDecoder() {
        return tokenManager;
    }

    public final AuthenticationInfo authenticateToken(String token) throws Throwable {
        JsonObject json = getTokenDecoder().decodeToken(token);
        return parseTokenPayload(json);
    }

    @Override
    protected AuthenticationInfo parseTokenPayload(JsonObject payload) throws Throwable {
        Integer idUsuario = JsonResolver.integer(payload, ID_USUARIO_JSON_PATH);

        // Obtener usuario.
        Result<Usuario> resUsuario = sesiones.obtenerPorTransId(idUsuario);
        resUsuario.raise(true);
        Usuario usuario = resUsuario.value();

        // Obtener perfiles.
        Result<Collection<Perfil>> resPerfiles = sesiones.obtenerPorTransUsuario(usuario);
        resPerfiles.raise(true);
        Collection<Perfil> perfiles = resPerfiles.value();

        // Encapsular informacion de autenticacion.
        AuthenticationInfo info = wrap(usuario, perfiles);
        return info;
    }

    private AuthenticationInfo wrap(Usuario usuario, Collection<Perfil> perfiles) throws Throwable {
        UserAuthenticationInfo info = new UserAuthenticationInfo();
        info.setMUsuario(usuario);
        info.setMPerfiles(perfiles);
        info.refresh();
        return info;
    }

    public static boolean tokenIsValid(String token) {
        try {
            SimpleTokenManager tokenManager = new SimpleTokenManager();
            JsonObject json = tokenManager.decodeToken(token);
            return json != null;
        } catch (Throwable thr) {
            Errors.dump(System.err, thr);
            return false;
        }
    }

    public static String issueToken(Integer idUsuario) {
        // Generar payload.
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add(ID_USUARIO_JSON_PATH, idUsuario);

        // Generar token.
        JsonObject json = builder.build();
        SimpleTokenManager tokenManager = new SimpleTokenManager();
        return tokenManager.encodeToken(json);
    }

}
