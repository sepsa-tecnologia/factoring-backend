/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Persistence {

    /**
     * Nombre parametrizado para la unidad de persistencia configurada.
     */
    public static final String PERSISTENCE_UNIT_NAME = "Genisys-PU";

}
