/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import java.util.LinkedList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.Convenio;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "ConvenioFacade", mappedName = "ConvenioFacade")
@LocalBean
public class ConvenioFacade extends AbstractFacade<Convenio> {

    /**
     * Constructor.
     */
    public ConvenioFacade() {
        super(Convenio.class);
    }

    public Convenio findById(Integer idConvenio) {
        try {
            Query q = getEntityManager().createNamedQuery("Convenio.findById");
            q.setParameter("id", idConvenio);
            return (Convenio) q.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    /**
     * Obtiene una lista de todos los convenios donde exista el proveedor,
     * comprador y entidad dada. Idealmente, solo deberia existir un solo
     * convenio.
     *
     * @param idProveedor Id de una persona proveedor.
     * @param idComprador Id de una persona comprador.
     * @param idEntidadFinanciera Id de una persona entidad financiera.
     * @return Lista de convenios.
     */
    public List<Convenio> find(Integer idComprador, Integer idProveedor, Integer idEntidadFinanciera) {
        List<Convenio> convenios = new LinkedList<>();
        try {
            Query q = getEntityManager().createNamedQuery("Convenio.findByIds");
            q.setParameter("idComprador", idComprador);
            q.setParameter("idProveedor", idProveedor);
            q.setParameter("idEntidadFinanciera", idEntidadFinanciera);
            q.setParameter("estado", 'A');
            convenios.addAll(JpaUtils.getAll(q));
        } catch (Throwable thr) {
            thr.printStackTrace(System.err);
        }
        return convenios;
    }

    /**
     * Obtiene una lista de todos los convenios donde exista la entidad
     * financiera dada.
     *
     * @param idPersona Id de la entidad financiera en el convenio.
     * @return Lista de convenios.
     */
    public List<Convenio> findAllByIdEntidadFinanciera(Integer idPersona) {
        Query q = getEntityManager().createNamedQuery("Convenio.findByIdEntidadFinanciera");
        q.setParameter("idEntidadFinanciera", idPersona);
        q.setParameter("estado", 'A');
        return JpaUtils.getAll(q);
    }

}
