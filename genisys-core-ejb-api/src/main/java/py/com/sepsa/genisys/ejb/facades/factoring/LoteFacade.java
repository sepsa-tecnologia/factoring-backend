/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.Lote;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "LoteFacade", mappedName = "LoteFacade")
@LocalBean
public class LoteFacade extends AbstractFacade<Lote> {

    /**
     * Constructor.
     */
    public LoteFacade() {
        super(Lote.class);
    }

    public Lote findById(Integer idLote) {
        Query q = getEntityManager().createNamedQuery("Lote.findById");
        q.setParameter("id", idLote);
        return JpaUtils.getFirst(q);
    }

}
