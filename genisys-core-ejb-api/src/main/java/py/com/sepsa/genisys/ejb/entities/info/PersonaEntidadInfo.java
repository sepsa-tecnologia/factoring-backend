/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.entities.info;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class PersonaEntidadInfo implements Serializable {

    private Integer id;
    private Integer idPersona;
    private Integer idPerfil;
    private Integer idEntidadFinanciera;
    private String razonSocialEntidadFinanciera;
    private String rucEntidadFinanciera;
    private String estado;
}
