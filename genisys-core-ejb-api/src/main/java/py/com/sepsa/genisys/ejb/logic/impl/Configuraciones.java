/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.criteria.query.Operators;
import fa.gs.criteria.query.QueryCriteria;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.Configuracion;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.logic.LogicBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Configuraciones", mappedName = "Configuraciones")
@LocalBean
public class Configuraciones extends LogicBean {

    @PostConstruct
    public void init() {
        ;
    }

    public Result<String> buscarValorParametro(ConfiguracionGeneralEnum param) {
        return buscarValorParametro(param.nombre());
    }

    public Result<String> buscarValorParametro(String nombre) {
        Result<String> result;
        try {
            QueryCriteria criteria = QueryCriteria.instance();
            criteria.where("parametro", Operators.EQUALS, nombre);
            Configuracion config = beans.getFacades().getConfiguracionFacade().findFirst(criteria);
            if (config != null) {
                result = Results.ok().value(config.getValor()).build();
            } else {
                result = Results.ko().message("Valor de parámetro de configuración '%s' no definido", nombre).build();
            }
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error obteniendo valor de parámetro de configuración '%s'", nombre).build();
        }
        return result;
    }

}
