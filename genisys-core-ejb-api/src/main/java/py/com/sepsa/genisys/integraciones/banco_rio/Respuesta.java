/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

/**
 *
 * @author Fabio A. González Sosa
 */
public interface Respuesta {

    public String getCodigo();

    public String getMensaje();

    public boolean isOk();

}
