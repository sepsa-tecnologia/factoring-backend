/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.text.Strings;
import fa.gs.utils.misc.text.Text;
import fa.gs.utils.misc.xml.Xml;
import fa.gs.utils.misc.xml.XmlPrefixInfoList;
import java.util.Collection;
import java.util.Objects;
import javax.xml.xpath.XPath;
import lombok.Data;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.CompradorInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class RespuestaConsultaCompradores implements Respuesta {

    String codigo;
    String mensaje;
    Collection<CompradorInfo> compradores;

    public static RespuestaConsultaCompradores parse(String xmlSoap) throws Throwable {
        // Eliminar el namespace que no utiliza ningun prefijo.
        xmlSoap = Text.replaceAll(xmlSoap, "xmlns=\"http://tempuri.org/\"", "");

        // Cargar XML de respuesta SOAP.
        Document xml = Xml.parse(xmlSoap);

        // Obtener prefijos y uris.
        XmlPrefixInfoList prefixes = Xml.readPrefixes(xml);

        // Preparar consultas XPath.
        XPath xpath = Xml.xpath(prefixes);
        String xCodigo = ".//CodigoError";
        String xMensaje = ".//Mensaje";
        String xComprador = ".//Comprador";
        String xCodCliente = ".//CodCliente";
        String xTipoPersona = ".//TipoPersona";
        String xTipoDocumento = ".//Sigla";
        String xNroDocumento = ".//NroDocumento";
        String xRazonSocial = ".//RazonSocial";
        String xCuentaCredito = ".//CuentaCredito";
        String xCuentaDebito = ".//CuentaDebito";

        // Obtener valor.
        String codigo = Xml.readNodeTexContent(xpath, xCodigo, xml);
        String mensaje = Xml.readNodeTexContent(xpath, xMensaje, xml);
        NodeList compradores0 = Xml.readNodes(xpath, xComprador, xml);
        Collection<CompradorInfo> compradores = Lists.empty();
        for (int i = 0; i < compradores0.getLength(); i++) {
            Node detalle0 = compradores0.item(i);
            CompradorInfo comprador = new CompradorInfo();
            comprador.setCodCliente(Xml.readNodeTexContent(xpath, xCodCliente, detalle0));
            comprador.setTipoPersona(Xml.readNodeTexContent(xpath, xTipoPersona, detalle0));
            comprador.setTipoDocumento(Xml.readNodeTexContent(xpath, xTipoDocumento, detalle0));
            comprador.setNroDocumento(asRuc(Xml.readNodeTexContent(xpath, xNroDocumento, detalle0)));
            comprador.setRazonSocial(Xml.readNodeTexContent(xpath, xRazonSocial, detalle0));
            comprador.setNroCuentaCredito(Xml.readNodeTexContent(xpath, xCuentaCredito, detalle0));
            comprador.setNroCuentaDebito(Xml.readNodeTexContent(xpath, xCuentaDebito, detalle0));
            compradores.add(comprador);
        }

        // Procesar valores.
        RespuestaConsultaCompradores respuesta = new RespuestaConsultaCompradores();
        respuesta.codigo = codigo;
        respuesta.mensaje = mensaje;
        respuesta.compradores = compradores;
        return respuesta;
    }

    private static String asRuc(String value) {
        if (Assertions.stringNullOrEmpty(value)) {
            return "";
        }

        if (!Assertions.isRuc(value)) {
            if (value.length() > 1) {
                String a = value.substring(0, value.length() - 1);
                String b = value.substring(value.length() - 1);
                return Strings.format("%s-%s", a, b);
            } else {
                return value;
            }
        } else {
            return value;
        }
    }

    public static RespuestaConsultaCompradores instance(String codigo, String mensaje) {
        RespuestaConsultaCompradores instance = new RespuestaConsultaCompradores();
        instance.codigo = codigo;
        instance.mensaje = mensaje;
        instance.compradores = Lists.empty();
        return instance;
    }

    @Override
    public boolean isOk() {
        return Assertions.stringNullOrEmpty(codigo) == false && Objects.equals(codigo, "0");
    }

}
