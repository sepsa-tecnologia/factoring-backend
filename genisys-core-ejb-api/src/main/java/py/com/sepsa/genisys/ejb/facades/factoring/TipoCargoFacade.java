/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.TipoCargo;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "TipoCargoFacade", mappedName = "TipoCargoFacade")
@LocalBean
public class TipoCargoFacade extends AbstractFacade<TipoCargo> {

    /**
     * Constructor.
     */
    public TipoCargoFacade() {
        super(TipoCargo.class);
    }

    /**
     * Permite obtener un tipo de cargo en base a su descripcion.
     *
     * @param descripcion Descripcion del tipo de cargo.
     * @return Tipo de cargo que concuerda con la descripcion, null en caso
     * contrario.
     */
    public TipoCargo findByDescripcion(String descripcion) {
        Query q = getEntityManager().createNamedQuery("TipoCargo.findByDescripcion");
        q.setParameter("descripcion", descripcion);
        return JpaUtils.getFirst(q);
    }
}
