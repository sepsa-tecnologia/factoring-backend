/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.Estado;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "EstadoFacade", mappedName = "EstadoFacade")
@LocalBean
public class EstadoFacade extends AbstractFacade<Estado> {

    /**
     * Constructor.
     */
    public EstadoFacade() {
        super(Estado.class);
    }

    public Estado findByDescripcionTipo(String descripcion, Character tipo) {
        Query q = getEntityManager().createNamedQuery("Estado.findByDescripcionTipo");
        q.setParameter("descripcion", descripcion);
        q.setParameter("tipo", tipo);
        return JpaUtils.getFirst(q);
    }

}
