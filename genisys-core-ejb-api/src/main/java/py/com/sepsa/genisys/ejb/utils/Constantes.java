/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Constantes {

    /**
     * Divisor para calcular el monto total sin iva 5%.
     */
    public static final String IVA_05_DIVISOR = "1.05";

    /**
     * Divisor para calcular el monto total sin iva 10%.
     */
    public static final String IVA_10_DIVISOR = "1.10";

    /**
     * Valor base para tabla info.tipo_contacto.
     */
    public static final Integer ID_TIPO_CONTACTO_GENERICO = 11;

    /**
     * Valor base para tabla info.cargo.
     */
    public static final Integer ID_CARGO_GENERICO = 11;

    public static final int TX_TIMEOUT_VALUE = 600;

    public static final TimeUnit TX_TIMEOUT_UNIT = TimeUnit.SECONDS;

}
