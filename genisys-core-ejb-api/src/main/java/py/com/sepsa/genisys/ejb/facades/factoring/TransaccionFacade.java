/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import py.com.sepsa.genisys.ejb.entities.factoring.Transaccion;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "TransaccionFacade", mappedName = "TransaccionFacade")
@LocalBean
public class TransaccionFacade extends AbstractFacade<Transaccion> {

    /**
     * Constructor.
     */
    public TransaccionFacade() {
        super(Transaccion.class);
    }

}
