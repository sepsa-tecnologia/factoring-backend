/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.integraciones.banco_rio.pojos;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import py.com.sepsa.genisys.ejb.utils.Ids;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class WsTrackInfo implements Serializable {

    private boolean success;
    private boolean isTimeout;
    private Throwable cause;
    private String referencia;
    private Date requestInicio;
    private Date requestFin;
    private String request;
    private String response;

    public WsTrackInfo() {
        this.referencia = "";
        this.requestInicio = null;
        this.requestFin = null;
    }

    public void initReferencia() {
        this.referencia = Ids.randomUuid();
    }

    public void initRequestInicio() {
        this.requestInicio = new Date();
    }

    public void initRequestFin() {
        this.requestFin = new Date();
    }

    public void initRequest(String content) {
        this.request = content;
    }

    public void initResponse(String content) {
        this.response = content;
    }

    public final void setOk() {
        this.success = true;
        this.isTimeout = false;
        this.cause = null;
    }

    public final void setKo() {
        this.success = false;
        this.isTimeout = false;
        this.cause = null;
    }

    public final void setKo(Throwable thr) {
        this.success = false;
        this.isTimeout = isTimeoutException(thr);
        this.cause = thr;
    }

    private boolean isTimeoutException(Throwable thr) {
        if (thr instanceof org.apache.http.conn.ConnectTimeoutException) {
            return true;
        }

        if (thr instanceof org.apache.http.conn.ConnectionPoolTimeoutException) {
            return true;
        }

        if (thr instanceof org.apache.http.conn.HttpHostConnectException) {
            return true;
        }

        if (thr instanceof java.net.SocketTimeoutException) {
            return true;
        }

        return false;
    }

}
