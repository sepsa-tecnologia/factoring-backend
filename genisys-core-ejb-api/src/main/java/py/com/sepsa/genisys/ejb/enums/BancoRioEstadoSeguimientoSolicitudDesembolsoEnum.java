/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.enums;

import fa.gs.utils.misc.Codificable;
import java8.util.Objects;

/**
 *
 * @author Fabio A. González Sosa
 */
public enum BancoRioEstadoSeguimientoSolicitudDesembolsoEnum implements Codificable {
    PENDIENTE("0001", "Pendiente"),
    RESUELTA("0002", "Resuelta");
    private final String codigo;
    private final String descripcion;

    private BancoRioEstadoSeguimientoSolicitudDesembolsoEnum(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static BancoRioEstadoSeguimientoSolicitudDesembolsoEnum fromCodigo(String codigo) {
        for (BancoRioEstadoSeguimientoSolicitudDesembolsoEnum value : BancoRioEstadoSeguimientoSolicitudDesembolsoEnum.values()) {
            if (Objects.equals(value.codigo, codigo)) {
                return value;
            }
        }
        return null;
    }

    @Override
    public String codigo() {
        return codigo;
    }

    @Override
    public String descripcion() {
        return descripcion;
    }

}
