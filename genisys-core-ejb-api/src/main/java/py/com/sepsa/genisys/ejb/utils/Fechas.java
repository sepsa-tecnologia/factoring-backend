/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Seconds;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Fechas {

    /**
     * Formato de fecha.
     */
    public static final String FORMAT_DD_MM_YYYY = "dd-MM-yyyy";

    /**
     * Convierte una representacion en texto de fecha en base a un patron
     * indicado.
     *
     * @param txt Texto.
     * @param format Patron de formato.
     * @return Objeto fecha si el texto concuerda con el patron indicado, caso
     * contrarion {@code null}.
     */
    public static Date parse(String txt, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.parse(txt);
        } catch (Throwable thr) {
            return null;
        }
    }

    /**
     * Convierte una fecha en una representacion de texto en base a un patron
     * indicado.
     *
     * @param fecha Fecha.
     * @param format Patron de formato.
     * @return Texto.
     */
    public static String format(Date fecha, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.format(fecha);
        } catch (Throwable thr) {
            return "";
        }
    }

    /**
     * Obtiene la fecha actual.
     *
     * @return fecha actual.
     */
    public static Date ahora() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    /**
     * Construye un objeto date que representa una fecha.
     *
     * @param anho Año.
     * @param mes Mes.
     * @param dia Dia.
     * @return Objeto date.
     */
    public static Date buildFecha(Integer anho, Integer mes, Integer dia) {
        return buildFechaHora(anho, mes, dia, 0, 0, 0);
    }

    /**
     * Construye un objeto date que representa un momento de tiempo.
     *
     * @param hora Hora.
     * @param minuto Minuto.
     * @param segundos Segundos.
     * @return Objeto date.
     */
    public static Date buildHora(Integer hora, Integer minuto, Integer segundos) {
        return buildFechaHora(1990, 1, 1, hora, minuto, segundos);
    }

    /**
     * Construye un objeto date que representa una fecha.
     *
     * @param anho Año.
     * @param mes Mes.
     * @param dia Dia.
     * @param hora Hora.
     * @param minuto Minuto.
     * @param segundos Segundos.
     * @return Objeto date.
     */
    public static Date buildFechaHora(Integer anho, Integer mes, Integer dia, Integer hora, Integer minuto, Integer segundos) {
        Calendar fecha = Calendar.getInstance();
        fecha.set(Calendar.ERA, GregorianCalendar.AD);
        fecha.set(Calendar.YEAR, anho);
        fecha.set(Calendar.MONTH, mes);
        fecha.set(Calendar.DAY_OF_MONTH, dia);
        fecha.set(Calendar.HOUR_OF_DAY, hora);
        fecha.set(Calendar.MINUTE, minuto);
        fecha.set(Calendar.SECOND, segundos);
        return fecha.getTime();
    }

    /**
     * Combina dos objetos de tipo Date para formar uno solo. Este metodo es
     * complementario a separarFechaHora.
     *
     * @param fecha Objeto Date donde nos interesa solo la parte de fecha
     * (dd/mm/yy).
     * @param hora Object Date donde nos interesa solo la parte de tiempo
     * (hh:mm).
     * @return Objeto Date que combina las partes de fecha y tiempo (dd/mm/yy
     * hh:mm).
     */
    public static Date combinarFechaHora(Date fecha, Date hora) {
        Calendar cFecha = Calendar.getInstance();
        cFecha.setTime(fecha);

        Calendar cHora = Calendar.getInstance();
        cHora.setTime(hora);

        Calendar cFechaHora = Calendar.getInstance();
        cFechaHora.set(Calendar.ERA, GregorianCalendar.AD);
        cFechaHora.set(Calendar.YEAR, cFecha.get(Calendar.YEAR));
        cFechaHora.set(Calendar.MONTH, cFecha.get(Calendar.MONTH));
        cFechaHora.set(Calendar.DAY_OF_MONTH, cFecha.get(Calendar.DAY_OF_MONTH));
        cFechaHora.set(Calendar.HOUR_OF_DAY, cHora.get(Calendar.HOUR_OF_DAY));
        cFechaHora.set(Calendar.MINUTE, cHora.get(Calendar.MINUTE));
        cFechaHora.set(Calendar.SECOND, cHora.get(Calendar.SECOND));
        return cFechaHora.getTime();
    }

    /**
     * Separa un objeto Date en dos diferentes separados por las parte de fecha
     * y tiempo. Este metodo es complementario a combinarFechaHora.
     *
     * @param fecha Objeto Date combinado que contiene las partes de fecha y
     * tiempo (dd/mm/yy hh:mm).
     * @return Array de dos objetos Date donde el primer elemento corresponde a
     * la parte de fecha (dd/mm/yy) y el segundo elemento corresponde a la parte
     * de tiempo (hh:mm).
     */
    public static Date[] separarFechaHora(Date fecha) {
        Calendar cFechaHora = Calendar.getInstance();
        cFechaHora.setTime(fecha);

        Calendar cFecha = Calendar.getInstance();
        cFecha.set(Calendar.ERA, GregorianCalendar.AD);
        cFecha.set(Calendar.YEAR, cFechaHora.get(Calendar.YEAR));
        cFecha.set(Calendar.MONTH, cFechaHora.get(Calendar.MONTH));
        cFecha.set(Calendar.DAY_OF_MONTH, cFechaHora.get(Calendar.DAY_OF_MONTH));

        Calendar cHora = Calendar.getInstance();
        cHora.set(Calendar.HOUR_OF_DAY, cFechaHora.get(Calendar.HOUR_OF_DAY));
        cHora.set(Calendar.MINUTE, cFechaHora.get(Calendar.MINUTE));
        cHora.set(Calendar.SECOND, cFechaHora.get(Calendar.SECOND));

        Date[] fechaHora = new Date[]{cFecha.getTime(), cHora.getTime()};
        return fechaHora;
    }

    /**
     * Calcula los dias de diferencia (sin considerar la parte de tiempo
     * hh:mm:ss de la fecha) entre una fecha arbitraria y la fecha de hoy (now).
     *
     * @param fecha Fecha arbitraria.
     * @return Cantidad de dias entre ambas fechas.
     */
    public static int diasDiferencia(Date fecha) {
        DateTime a = new DateTime(fecha);
        return Days.daysBetween(LocalDate.now(), a.toLocalDate()).getDays();
    }

    /**
     * Calcula los dias de diferencia (sin considerar la parte de tiempo
     * hh:mm:ss de la fecha) entre dos fechas diferentes.
     *
     * @param inicio Fecha de inicio.
     * @param fin Fecha de fin.
     * @return Cantidad de dias entre ambas fechas.
     */
    public static int diasDiferencia(Date inicio, Date fin) {
        DateTime a = new DateTime(inicio);
        DateTime b = new DateTime(fin);
        return Days.daysBetween(a.toLocalDate(), b.toLocalDate()).getDays();
    }

    /**
     * Calcula los dias de diferencia (considerando la parte de tiempo hh:mm:ss)
     * entre dos fechas diferentes.
     *
     * @param inicio Fecha de inicio.
     * @param fin Fecha de fin.
     * @return Cantidad de segundos entre ambas fechas.
     */
    public static int segundosDiferencia(Date inicio, Date fin) {
        DateTime a = new DateTime(inicio);
        DateTime b = new DateTime(fin);
        return Seconds.secondsBetween(a, b).getSeconds();
        //return Days.daysBetween(a.toLocalDate(), b.toLocalDate()).getDays();
    }

    /**
     * Obtiene el primer dia del año actual.
     *
     * @return Fecha completa del primer dia del año.
     */
    public static Date primerDiaAnho() {
        Integer anho = Calendar.getInstance().get(Calendar.YEAR);
        Date fecha = buildFecha(anho, 1, 1);
        Date hora = buildHora(0, 0, 0);
        return combinarFechaHora(fecha, hora);
    }

    /**
     * Obtiene el ultimo dia del año actual.
     *
     * @return Fecha completa del ultimo dia del año.
     */
    public static Date ultimoDiaAnho() {
        Integer anho = Calendar.getInstance().get(Calendar.YEAR);
        Date fecha = buildFecha(anho, 12, 31);
        Date hora = buildHora(23, 59, 59);
        return combinarFechaHora(fecha, hora);
    }

}
