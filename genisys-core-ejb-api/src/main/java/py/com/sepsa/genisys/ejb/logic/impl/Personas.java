/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.logic.impl;

import fa.gs.criteria.query.Order;
import fa.gs.misc.text.Joiner;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Arrays;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.database.query.expressions.literals.CollectionLiteral;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.misc.pagination.Pagination;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java8.util.Objects;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.info.Barrio;
import py.com.sepsa.genisys.ejb.entities.info.Calle;
import py.com.sepsa.genisys.ejb.entities.info.ContactoEmail;
import py.com.sepsa.genisys.ejb.entities.info.Direccion;
import py.com.sepsa.genisys.ejb.entities.info.Email;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.info.PersonaFisica;
import py.com.sepsa.genisys.ejb.entities.info.PersonaInfo;
import py.com.sepsa.genisys.ejb.entities.info.PersonaJuridica;
import py.com.sepsa.genisys.ejb.entities.info.Telefono;
import py.com.sepsa.genisys.ejb.entities.trans.PersonaRol;
import py.com.sepsa.genisys.ejb.entities.trans.PersonaRolPK;
import py.com.sepsa.genisys.ejb.entities.trans.ProveedorComprador;
import py.com.sepsa.genisys.ejb.entities.trans.ProveedorCompradorPK;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.enums.RolEnum;
import py.com.sepsa.genisys.ejb.enums.TipoTelefonoEnum;
import py.com.sepsa.genisys.ejb.logic.LogicBean;
import py.com.sepsa.genisys.ejb.logic.pojos.DireccionInfo;
import py.com.sepsa.genisys.ejb.logic.pojos.PersonaInfo2;
import py.com.sepsa.genisys.ejb.logic.pojos.PersonasBusquedaAvanzadaInput;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;
import py.com.sepsa.genisys.ejb.utils.Persistence;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ContactoInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "Personas", mappedName = "Personas")
@LocalBean
public class Personas extends LogicBean {

    @PersistenceContext(unitName = Persistence.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @EJB
    private Emails emails;

    @EJB
    private JpaUtils jpaUtils;

    //<editor-fold defaultstate="collapsed" desc="Obtener Contacto">
    public Result<ContactoInfo> obtenerDatosContacto(Integer idPersona) {
        try {
            Telefono telefonoLineaBaja = obtenerOCrearTelefono(idPersona, TipoTelefonoEnum.LINEA_BAJA);
            Telefono telefonoMovil = obtenerOCrearTelefono(idPersona, TipoTelefonoEnum.MOVIL);
            Email email = obtenerOCrearEmail(idPersona);

            ContactoInfo info = new ContactoInfo();
            info.setNroLineaBaja(telefonoLineaBaja.getNumero());
            info.setNroCelular(telefonoMovil.getNumero());
            info.setEmail(email.getEmail());

            return Results.ok()
                    .value(info)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo datos de dirección")
                    .tag("persona.id", idPersona)
                    .build();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Obtener Direccion">
    public Result<DireccionInfo> obtenerDatosDireccion(Integer idPersona) {
        try {
            Direccion direccion = obtenerOCrearDireccion(idPersona);
            Barrio barrio = obtenerOCrearBarrio(direccion.getId());
            Calle calle1 = obtenerOCrearCalle(direccion.getId(), 1);
            Calle calle2 = obtenerOCrearCalle(direccion.getId(), 2);
            Calle calle3 = obtenerOCrearCalle(direccion.getId(), 3);

            DireccionInfo info = new DireccionInfo();
            info.setCalle1(calle1.getDescripcion());
            info.setCalle2(calle2.getDescripcion());
            info.setCalle3(calle3.getDescripcion());
            info.setBarrio(barrio.getDescripcion());
            info.setNroCalle(direccion.getNumero());
            info.setReferencia(direccion.getObservacion());

            return Results.ok()
                    .value(info)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo datos de dirección")
                    .tag("persona.id", idPersona)
                    .build();
        }
    }

    private String obtenerCalle(Integer idCalle) throws Throwable {
        String query = Strings.format(" select c.descripcion from info.calle as c where c.id = %s ", idCalle);
        Map<String, Object> row = jpaUtils.getFirst(query);
        return Maps.string(row, "descripcion", "");
    }

    private String obtenerBarrio(Integer idCalle) throws Throwable {
        String query = Strings.format(" select b.descripcion from info.calle as c left join info.barrio as b on b.id = c.id_barrio where c.id = %s ", idCalle);
        Map<String, Object> row = jpaUtils.getFirst(query);
        return Maps.string(row, "descripcion", "");
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Obtener Personas">
    public Result<Persona> obtenerPorId(Integer id) {
        Result<Persona> result;

        try {
            Integer[] ids = new Integer[]{id};
            Result<Collection<Persona>> resPersonas = obtenerPorIds(ids);
            resPersonas.raise();

            Collection<Persona> personas = resPersonas.value();
            Persona persona = Lists.first(personas);

            result = Results.ok()
                    .value(persona)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo persona")
                    .tag("persona.id", id)
                    .build();
        }

        return result;
    }

    public Result<Collection<Persona>> obtenerPorIds(Integer[] ids) {
        Result<Collection<Persona>> result;

        try {
            Collection<Persona> instances = Lists.empty();

            int chunk = 10;
            for (int i = 0; i < ids.length; i += chunk) {
                // Obtener subrango de identificadores.
                Integer[] idsChunk = java.util.Arrays.copyOfRange(ids, i, Math.min(ids.length, i + chunk));

                // Construir query.
                StringBuilder2 builder = new StringBuilder2();
                builder.append(" select ");
                builder.append("     p.id as \"id_persona\", ");
                builder.append("     p.id_tipo_persona as \"id_tipo_persona\", ");
                builder.append("     p.id_direccion as \"id_direccion\", ");
                builder.append("     p.estado as \"estado\", ");
                builder.append("     p.ruc as \"ruc\", ");
                builder.append("     p.dv_ruc as \"dv_ruc\", ");
                builder.append("     coalesce(pj.razon_social, pf.nombre || ' ' || pf.apellido) as \"razon_social\", ");
                builder.append("     pj.razon_social as \"pj_razon_social\", ");
                builder.append("     pj.nombre_fantasia as \"pj_nombre_fantasia\", ");
                builder.append("     pf.nombre as \"pf_nombres\", ");
                builder.append("     pf.apellido as \"pf_apellidos\", ");
                builder.append("     pf.nro_documento as \"pf_ci\" ");
                builder.append(" from info.persona as p ");
                builder.append(" left join info.persona_fisica as pf on pf.id = p.id ");
                builder.append(" left join info.persona_juridica as pj on pj.id = p.id ");
                builder.append(" where ");
                builder.append("     1 = 1 ");
                builder.append("     and p.id in %s ", CollectionLiteral.instance(idsChunk).stringify(null));
                builder.append("     and p.estado = 'A' ");
                builder.append("     ; ");

                // Ejecutar consulta.
                String query = builder.toString();
                Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

                // Mapear y agregar registros.
                rows.stream()
                        .map(row -> mapPersona(row))
                        .forEach(p -> instances.add(p));
            }

            result = Results.ok()
                    .value(instances)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo personas")
                    .build();
        }

        return result;
    }

    public Result<Persona> obtenerPorRuc(String ruc) {
        Result<Persona> result;

        try {
            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     p.id as \"id_persona\" ");
            builder.append(" from info.persona as p ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            if (Assertions.isRuc(ruc)) {
                RucInfo ruc0 = separarRuc(ruc);
                builder.append(" and p.ruc = '%s' ", ruc0.ruc);
                builder.append(" and p.dv_ruc = %s ", ruc0.dvRuc);
            } else {
                builder.append(" and p.ruc = '%s' ", ruc);
            }
            builder.append("     and p.estado = 'A' ");
            builder.append(" ; ");
            String query = builder.toString();

            // Ejecutar query.
            Map<String, Object> row = jpaUtils.getFirst(query);
            Integer idPersona = Maps.integer(row, "id_persona");

            // Obtener persona.
            result = obtenerPorId(idPersona);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo persona")
                    .tag("persona.ruc", ruc)
                    .build();
        }

        return result;
    }

    public Result<Collection<Persona>> obtenerPorDatos(String razonSocial, String nombreFantasia, String ruc) {
        Result<Collection<Persona>> result;

        try {
            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("   p.id as \"id\" ");
            builder.append(" from info.persona as p ");
            builder.append(" left join info.persona_fisica as pf on p.id = pf.id  ");
            builder.append(" left join info.persona_juridica as pj on p.id = pj.id ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and p.estado = 'A' ");
            if (!Assertions.stringNullOrEmpty(razonSocial)) {
                builder.append(" and pj.razon_social ilike '%%%s%%' ", razonSocial);
            }
            if (!Assertions.stringNullOrEmpty(nombreFantasia)) {
                builder.append(" and pj.nombre_fantasia ilike '%%%s%%' ", nombreFantasia);
            }
            if (!Assertions.stringNullOrEmpty(ruc)) {
                if (Assertions.isRuc(ruc)) {
                    RucInfo ruc0 = separarRuc(ruc);
                    builder.append(" and p.ruc = '%s' ", ruc0.ruc);
                    builder.append(" and p.dv_ruc = %s ", ruc0.dvRuc);
                } else {
                    builder.append(" and p.ruc ilike '%%%s%%' ", ruc);
                }
            }

            // Ejecutar consulta.
            String query = builder.toString();
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Obtener identificadores de personas.
            Integer[] ids = Lists.stream(rows)
                    .map(r -> Maps.integer(r, "id"))
                    .distinct()
                    .toArray(Integer[]::new);

            // Consulta de personas.
            return obtenerPorIds(ids);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo personas")
                    .build();
        }

        return result;
    }

    private RucInfo separarRuc(String ruc) {
        RucInfo info = new RucInfo();
        if (Assertions.isRuc(ruc)) {
            String[] parts = ruc.split(Pattern.quote("-"));
            info.ruc = parts[0];
            info.dvRuc = parts[1];
        } else {
            info.ruc = ruc;
            info.dvRuc = "";
        }
        return info;
    }

    public Result<Long> obtenerTotalPersonasPorBusquedaAvanzada(PersonasBusquedaAvanzadaInput input, Pagination pagination) {
        try {
            // Ejecutar query.
            String query = obtenerQueryPersonasPorBusquedaAvanzada(true, input, pagination);
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);
            Map<String, Object> row = Lists.first(rows);

            // Control.
            if (Assertions.isNullOrEmpty(row)) {
                return Results.ok()
                        .value(0L)
                        .build();
            }

            // Obtener total.
            Long total = Numeric.adaptAsLong(Maps.get(row, "total"));
            return Results.ok()
                    .value(total)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo datos de personas")
                    .build();
        }
    }

    public Result<Collection<PersonaInfo2>> obtenerPersonasPorBusquedaAvanzada(PersonasBusquedaAvanzadaInput input, Pagination pagination) {
        try {
            // Ejecutar query.
            String query = obtenerQueryPersonasPorBusquedaAvanzada(false, input, pagination);
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Completar datos adicionales.
            completarDatosAdicionalesBusquedaAvanzada(rows);

            // Preparar datos finales.
            Collection<PersonaInfo2> personas = mapPersonasBusquedaAvanzada(rows);
            return Results.ok()
                    .value(personas)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo datos de personas")
                    .build();
        }
    }

    private String obtenerQueryPersonasPorBusquedaAvanzada(boolean forCount, PersonasBusquedaAvanzadaInput input, Pagination pagination) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");
        if (forCount) {
            builder.append("  cast(count(*) as int8) as \"total\" ");
        } else {
            builder.append("   p.id as \"id\", ");
            builder.append("   p.ruc as \"ruc\", ");
            builder.append("   cast(p.dv_ruc as text) as \"dv_ruc\", ");
            builder.append("   pj.razon_social as \"razon_social\", ");
            builder.append("   pj.nombre_fantasia as \"nombre_fantasia\", ");
            builder.append("   pf.nombre as \"nombres\", ");
            builder.append("   pf.apellido as \"apellidos\", ");
            builder.append("   pf.nro_documento as \"nro_documento\", ");
            builder.append("   p.id_tipo_persona as \"id_tipo_persona\" ");
        }
        builder.append(" from info.persona as p ");
        builder.append(" left join info.persona_fisica as pf on p.id = pf.id  ");
        builder.append(" left join info.persona_juridica as pj on p.id = pj.id ");
        builder.append(" where ");
        builder.append("     1 = 1 ");
        builder.append("     and p.estado = 'A' ");
        if (!Assertions.stringNullOrEmpty(input.getRazonSocial())) {
            builder.append(" and pj.razon_social ilike '%%%s%%' ", input.getRazonSocial());
        }
        if (!Assertions.stringNullOrEmpty(input.getNombreFantasia())) {
            builder.append(" and pj.nombre_fantasia ilike '%%%s%%' ", input.getNombreFantasia());
        }
        if (!Assertions.stringNullOrEmpty(input.getNombres())) {
            builder.append(" and pf.nombre ilike '%%%s%%' ", input.getNombres());
        }
        if (!Assertions.stringNullOrEmpty(input.getApellidos())) {
            builder.append(" and pf.apellido ilike '%%%s%%' ", input.getApellidos());
        }
        if (!Assertions.stringNullOrEmpty(input.getNroDocumento())) {
            builder.append(" and pf.nro_documento ilike '%%%s%%' ", input.getNroDocumento());
        }
        if (!Assertions.stringNullOrEmpty(input.getRuc())) {
            if (Assertions.isRuc(input.getRuc())) {
                RucInfo ruc0 = separarRuc(input.getRuc());
                builder.append(" and p.ruc = '%s' ", ruc0.ruc);
                builder.append(" and p.dv_ruc = %s ", ruc0.dvRuc);
            } else {
                builder.append(" and p.ruc ilike '%%%s%%' ", input.getRuc());
            }
        }
        if (!Assertions.isNull(input.getPersoneria())) {
            builder.append(" and p.id_tipo_persona = %s ", input.getPersoneria().idTipoPersona);
        }
        if (!forCount) {
            if (pagination != null) {
                builder.append(" limit %s ", pagination.getLimit());
                builder.append(" offset %s ", pagination.getOffset());
            }
        }

        return builder.toString();
    }

    private void completarDatosAdicionalesBusquedaAvanzada(Collection<Map<String, Object>> rows) throws Throwable {
        if (!Assertions.isNullOrEmpty(rows)) {
            for (Map<String, Object> row : rows) {
                // Datos iniciales.
                Integer idPersona = Maps.integer(row, "id");

                // Preparar query.
                StringBuilder2 builder = new StringBuilder2();
                builder.append(" select ");
                builder.append("   cast(tmp0.total as int8) as \"total_compradores\", ");
                builder.append("   cast(tmp1.total as int8) as \"total_proveedores\", ");
                builder.append("   (tmp2.total > 0) as \"es_comprador\", ");
                builder.append("   (tmp3.total > 0) as \"es_proveedor\" ");
                builder.append(" from info.persona as p ");
                builder.append(" left join lateral ( select count(*) as \"total\" from trans.proveedor_comprador where id_comprador = p.id and estado = 'A') as tmp0 on true ");
                builder.append(" left join lateral ( select count(*) as \"total\" from trans.proveedor_comprador where id_proveedor = p.id and estado = 'A') as tmp1 on true ");
                builder.append(" left join lateral ( select count(*) as \"total\" from trans.persona_rol where id_persona = p.id and id_rol = %s ) as tmp2 on true", RolEnum.COMPRADOR.idRol());
                builder.append(" left join lateral ( select count(*) as \"total\" from trans.persona_rol where id_persona = p.id and id_rol = %s ) as tmp3 on true", RolEnum.PROVEEDOR.idRol());
                builder.append(" where p.id = %s ", idPersona);

                // Ejecutar consulta.
                String query = builder.toString();
                Collection<Map<String, Object>> rows0 = jpaUtils.getAll(query);

                // Agregar datos adicionales.                
                Map<String, Object> row0 = Lists.first(rows0);
                if (row0 != null) {
                    row.putAll(row0);
                }
            }
        }
    }

    private Collection<PersonaInfo2> mapPersonasBusquedaAvanzada(Collection<Map<String, Object>> rows) throws Throwable {
        Collection<PersonaInfo2> personas = Lists.empty();
        if (!Assertions.isNullOrEmpty(rows)) {
            for (Map<String, Object> row : rows) {
                PersonaInfo2 persona = new PersonaInfo2();
                persona.setId(Maps.integer(row, "id"));
                persona.setRazonSocial(Maps.string(row, "razon_social"));
                persona.setNombreFantasia(Maps.string(row, "nombre_fantasia"));
                persona.setNombres(Maps.string(row, "nombres"));
                persona.setApellidos(Maps.string(row, "apellidos"));
                persona.setNroDocumento(Maps.string(row, "nro_documento"));
                persona.setRuc(Maps.string(row, "ruc"));
                persona.setDvRuc(Maps.string(row, "dv_ruc"));
                persona.setTipoPersona(TipoPersonaEnum.fromIdTipoPersona(Maps.integer(row, "id_tipo_persona")));
                persona.setTotalCompradores(Numeric.adaptAsLong(Maps.get(row, "total_compradores")));
                persona.setTotalProveedores(Numeric.adaptAsLong(Maps.get(row, "total_proveedores")));
                persona.setEsComprador(Maps.bool(row, "es_comprador"));
                persona.setEsProveedor(Maps.bool(row, "es_proveedor"));
                personas.add(persona);
            }
        }
        return personas;
    }
    //</editor-fold>

    public void cambiarPersoneria(Integer idPersona, TipoPersonaEnum tipoPersona, String razonSocial, String ruc) throws Throwable {
        // Eliminar datos existentes.
        jpaUtils.executeUpdate(String.format(" delete from info.persona_fisica where id = %s ", idPersona));
        jpaUtils.executeUpdate(String.format(" delete from info.persona_juridica where id = %s ", idPersona));
        jpaUtils.executeUpdate(String.format(" update info.persona set id_tipo_persona = %s where id = %s ", tipoPersona.idTipoPersona, idPersona));

        // Crear nuevos datos.
        if (tipoPersona == TipoPersonaEnum.FISICA) {
            String query = " insert into info.persona_fisica (id, nombre, apellido, nro_documento) values(%s, '%s', '', '%s') ";
            query = Strings.format(query, idPersona, razonSocial, separarRuc(ruc).ruc);
            jpaUtils.executeUpdate(query);
            return;
        }

        // Crear nuevos datos.
        if (tipoPersona == TipoPersonaEnum.JURIDICA) {
            String query = " insert into info.persona_juridica (id, razon_social, nombre_fantasia) values(%s, '%s', '%s') ";
            query = Strings.format(query, idPersona, razonSocial, razonSocial);
            jpaUtils.executeUpdate(query);
            return;
        }

        throw Errors.illegalState();
    }

    private Integer[] obtenerIdPersonasEnRol(Integer idPersona, PerfilEnum perfil) throws Throwable {
        String query;

        if (perfil == PerfilEnum.COMPRADOR || perfil == PerfilEnum.PROVEEDOR) {
            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" select ");
            builder.append("     p.id as \"id_persona\" ");
            builder.append(" from trans.persona_rol as pr ");
            builder.append(" left join info.persona as p on pr.id_persona = p.id ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and pr.id_rol = %s ", getIdRol(perfil));
            builder.append("     and pr.estado = 'A' ");
            builder.append("     and p.estado = 'A' ");
            builder.append("     ; ");
            query = builder.toString();

            // Ejecutar consulta y extraer identificadores de persona.
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);
            return unwrapIds(rows, "id_persona");
        } else if (perfil == PerfilEnum.ENTIDAD_FINANCIERA) {
            return obtenerIdEntidadesFinancierasPersona(idPersona);
        } else {
            throw Errors.unsupported("Perfil '%s' no soportado.", perfil.descripcion());
        }
    }

    /**
     * Obtiene los identificadores de entidades financieras con las que puede
     * operar una persona. Primeramente se verifica la tabla
     * factoring.persona_entidad_fija en busca de entidades fijadas a la
     * persona. Segundo, si la lista anterior es vacia se toman todos los
     * identificadores de info.entidad_financiera.
     *
     * @return Identificadores de personas con rol entidad financiera.
     */
    private Integer[] obtenerIdEntidadesFinancierasPersona(Integer idPersona) {
        StringBuilder2 builder = new StringBuilder2();
        String query;

        try {
            // Construir query.
            builder.clear();
            builder.append(" select ");
            builder.append("     pef.id_entidad_financiera as \"id_persona\" ");
            builder.append(" from factoring.persona_entidad_fija as pef ");
            builder.append(" left join info.persona as p on p.id = pef.id_entidad_financiera");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and p.estado = 'A' ");
            if (idPersona != null) {
                builder.append(" and pef.id_persona = %s ", idPersona);
            }
            builder.append("     ; ");
            query = builder.toString();

            // Ejecutar query.
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);
            if (rows != null && !rows.isEmpty()) {
                return unwrapIds(rows, "id_persona");
            }
        } catch (Throwable thr) {
            ;
        }

        try {
            // Construir query.
            builder.clear();
            builder.append(" select ");
            builder.append("     p.id as \"id_persona\" ");
            builder.append(" from info.entidad_financiera as ef ");
            builder.append(" left join info.persona as p on ef.id_entidad_financiera = p.id ");
            builder.append(" where ");
            builder.append("     1 = 1 ");
            builder.append("     and p.estado = 'A' ");
            builder.append("     ; ");
            query = builder.toString();

            // Ejecutar query.
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);
            if (rows != null && !rows.isEmpty()) {
                return unwrapIds(rows, "id_persona");
            }
        } catch (Throwable thr) {
            ;
        }

        return new Integer[0];
    }

    private Integer[] unwrapIds(Collection<Map<String, Object>> rows, String colname) {
        Collection<Integer> ids = new HashSet<>();
        if (!Assertions.isNullOrEmpty(rows)) {
            rows.stream().forEach(row -> {
                Integer idPersona = Maps.integer(row, colname);
                ids.add(idPersona);
            });
        }
        return Arrays.unwrap(ids, Integer.class);
    }

    //<editor-fold defaultstate="collapsed" desc="Busqueda de Proveedores">
    /**
     * Obtiene la lista de todos los proveedores disposnibles.
     *
     * @return
     */
    public Result<Collection<Persona>> obtenerProveedoresDisponibles() {
        Result<Collection<Persona>> result;

        try {
            Integer[] ids = obtenerIdPersonasEnRol(null, PerfilEnum.PROVEEDOR);
            return obtenerPorIds(ids);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error consultando proveedores disponibles")
                    .build();
        }

        return result;
    }

    /**
     * Obtiene la lista de todos los proveedores disponibles asociados a un
     * comprador especifico.
     *
     * @param idComprador
     * @return
     */
    public Result<Collection<Persona>> obtenerProveedoresDisponibles(Integer idComprador) {
        Result<Collection<Persona>> result;

        try {
            Integer[] ids = obtenerIdProveedoresDisponibles(idComprador);
            return obtenerPorIds(ids);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error consultando proveedores de comprador")
                    .tag("comprador.id", idComprador)
                    .build();
        }

        return result;
    }

    private String queryIdProveedoresDisponibles(Integer idComprador) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");
        builder.append("     id_proveedor as \"id_persona\" ");
        builder.append(" from trans.proveedor_comprador as pc ");
        builder.append(" left join info.persona as p0 on p0.id = pc.id_proveedor ");
        builder.append(" left join info.persona as p1 on p1.id = pc.id_comprador ");
        builder.append(" where ");
        builder.append("     1 = 1 ");
        builder.append("     and pc.estado = 'A' ");
        builder.append("     and p0.estado = 'A' ");
        builder.append("     and p1.estado = 'A' ");
        if (idComprador != null) {
            builder.append("     and pc.id_comprador = %s ", idComprador);
        }
        return builder.toString();
    }

    private Integer[] obtenerIdProveedoresDisponibles(Integer idComprador) throws Throwable {
        // Construir query.
        String query = queryIdProveedoresDisponibles(idComprador);

        // Ejecutar consulta y extraer identificadores de persona.
        Collection<Map<String, Object>> rows = jpaUtils.getAll(query);
        Collection<Integer> ids = new HashSet<>();
        if (!Assertions.isNullOrEmpty(rows)) {
            rows.stream().forEach(row -> {
                Integer idPersona = Maps.integer(row, "id_persona");
                ids.add(idPersona);
            });
        }

        return Arrays.unwrap(ids, Integer.class);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Busqueda Avanzada de Proveedores">
    public List<PersonaInfo> selectProveedores(Integer idComprador, Integer limit, Integer offset, Map<String, Object> filterBy, Map<String, Order> orderBy) {
        List<PersonaInfo> personas = Lists.empty();

        try {
            // Ejecutar query.
            String query = queryProveedores(idComprador, false, limit, offset, filterBy, orderBy);
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Agregar registros.
            if (!Assertions.isNullOrEmpty(rows)) {
                rows.stream()
                        .map(this::mapPersonaInfo)
                        .forEach(personas::add);
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando facturas");
        }

        return personas;
    }

    public Integer countProveedores(Integer idComprador, Map<String, Object> filterBy) {
        try {
            // Ejecutar query.
            String query = queryProveedores(idComprador, true, null, null, filterBy, null);
            Map<String, Object> row = jpaUtils.getFirst(query);

            BigInteger count = Maps.biginteger(row, "total");
            return count.intValue();
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando proveedores de proveedor");
            return -1;
        }
    }

    private String queryProveedores(Integer idComprador, boolean forCount, Integer limit, Integer offset, Map<String, Object> filterBy, Map<String, Order> orderBy) {
        // Construir query.
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");

        // Proyecciones.
        if (forCount) {
            builder.append("  cast(count(*) as int8) as \"total\" ");
        } else {
            builder.append("  * ");
        }

        // Origen de datos.
        builder.append(" from ( %s ) as P ", queryPersonas(QueryPersonasKind.PROVEEDORES_DE_COMPRADOR, idComprador));

        // Filtros.
        builder.append(" where ");
        builder.append("   1 = 1 ");
        if (!Assertions.isNullOrEmpty(filterBy)) {
            filterBy.entrySet().stream().forEach(e -> {
                builder.append(" and %s ", String.valueOf(e.getValue()));
            });
        }

        // Ordenacion.
        if (forCount == false) {
            if (!Assertions.isNullOrEmpty(orderBy)) {
                String[] clauses = orderBy.entrySet().stream()
                        .map(e -> Strings.format("%s %s", e.getKey(), e.getValue()))
                        .distinct()
                        .toArray(String[]::new);
                String clause = Joiner.of(clauses).separator(",").join();

                builder.append(" order by ");
                builder.append(" %s ", clause);
            }
        }

        // Paginacion.
        if (forCount == false) {
            if (limit != null) {
                builder.append(" limit %s ", limit);
            }
            if (offset != null) {
                builder.append(" offset %s ", offset);
            }
        }

        return builder.toString();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Busqueda de Compradores">
    /**
     * Obtiene todos los compradores disponibles.
     *
     * @return
     */
    public Result<Collection<Persona>> obtenerCompradoresDisponibles() {
        Result<Collection<Persona>> result;

        try {
            Integer[] ids = obtenerIdPersonasEnRol(null, PerfilEnum.COMPRADOR);
            return obtenerPorIds(ids);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error consultando compradores disponibles")
                    .build();
        }

        return result;
    }

    /**
     * Obtiene todos los compradores asociados a un proveedor especifico.
     *
     * @param idProveedor
     * @return
     */
    public Result<Collection<Persona>> obtenerCompradoresDisponibles(Integer idProveedor) {
        Result<Collection<Persona>> result;

        try {
            Integer[] ids = obtenerIdCompradoresDisponibles(idProveedor);
            return obtenerPorIds(ids);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error consultando compradores de proveedor")
                    .tag("proveedor.id", idProveedor)
                    .build();
        }

        return result;
    }

    public String queryIdCompradoresDisponibles(Integer idProveedor) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");
        builder.append("     id_comprador as \"id_persona\" ");
        builder.append(" from trans.proveedor_comprador as pc ");
        builder.append(" left join info.persona as p0 on p0.id = pc.id_proveedor ");
        builder.append(" left join info.persona as p1 on p1.id = pc.id_comprador ");
        builder.append(" where ");
        builder.append("     1 = 1 ");
        builder.append("     and pc.estado = 'A' ");
        builder.append("     and p0.estado = 'A' ");
        builder.append("     and p1.estado = 'A' ");
        if (idProveedor != null) {
            builder.append("     and pc.id_proveedor = %s ", idProveedor);
        }
        return builder.toString();
    }

    private Integer[] obtenerIdCompradoresDisponibles(Integer idProveedor) throws Throwable {
        // Construir query.
        String query = queryIdCompradoresDisponibles(idProveedor);

        // Ejecutar consulta y extraer identificadores de persona.
        Collection<Map<String, Object>> rows = jpaUtils.getAll(query);
        Collection<Integer> ids = new HashSet<>();
        if (!Assertions.isNullOrEmpty(rows)) {
            rows.stream().forEach(row -> {
                Integer idPersona = Maps.integer(row, "id_persona");
                ids.add(idPersona);
            });
        }

        return Arrays.unwrap(ids, Integer.class);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Busqueda Avanzada de Compradores">
    public List<PersonaInfo> selectCompradores(Integer idProveedor, Integer limit, Integer offset, Map<String, Object> filterBy, Map<String, Order> orderBy) {
        List<PersonaInfo> personas = Lists.empty();

        try {
            // Ejecutar query.
            String query = queryCompradores(idProveedor, false, limit, offset, filterBy, orderBy);
            Collection<Map<String, Object>> rows = jpaUtils.getAll(query);

            // Agregar registros.
            if (!Assertions.isNullOrEmpty(rows)) {
                rows.stream()
                        .map(this::mapPersonaInfo)
                        .forEach(personas::add);
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando facturas");
        }

        return personas;
    }

    public Integer countCompradores(Integer idProveedor, Map<String, Object> filterBy) {
        try {
            // Ejecutar query.
            String query = queryCompradores(idProveedor, true, null, null, filterBy, null);
            Map<String, Object> row = jpaUtils.getFirst(query);

            BigInteger count = Maps.biginteger(row, "total");
            return count.intValue();
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error consultando compradores de proveedor");
            return -1;
        }
    }

    private String queryCompradores(Integer idProveedor, boolean forCount, Integer limit, Integer offset, Map<String, Object> filterBy, Map<String, Order> orderBy) {
        // Construir query.
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");

        // Proyecciones.
        if (forCount) {
            builder.append("  cast(count(*) as int8) as \"total\" ");
        } else {
            builder.append("  * ");
        }

        // Origen de datos.
        builder.append(" from ( %s ) as P ", queryPersonas(QueryPersonasKind.COMPRADORES_DE_PROVEEDOR, idProveedor));

        // Filtros.
        builder.append(" where ");
        builder.append("   1 = 1 ");
        if (!Assertions.isNullOrEmpty(filterBy)) {
            filterBy.entrySet().stream().forEach(e -> {
                builder.append(" and %s ", String.valueOf(e.getValue()));
            });
        }

        // Ordenacion.
        if (forCount == false) {
            if (!Assertions.isNullOrEmpty(orderBy)) {
                String[] clauses = orderBy.entrySet().stream()
                        .map(e -> Strings.format("%s %s", e.getKey(), e.getValue()))
                        .distinct()
                        .toArray(String[]::new);
                String clause = Joiner.of(clauses).separator(",").join();

                builder.append(" order by ");
                builder.append(" %s ", clause);
            }
        }

        // Paginacion.
        if (forCount == false) {
            if (limit != null) {
                builder.append(" limit %s ", limit);
            }
            if (offset != null) {
                builder.append(" offset %s ", offset);
            }
        }

        return builder.toString();
    }
    //</editor-fold>

    private enum QueryPersonasKind {
        COMPRADORES_DE_PROVEEDOR,
        PROVEEDORES_DE_COMPRADOR
    }

    private String queryPersonas(QueryPersonasKind kind, Integer idPersona) {
        String subqueryIdPersonas;
        if (kind == QueryPersonasKind.COMPRADORES_DE_PROVEEDOR) {
            subqueryIdPersonas = queryIdCompradoresDisponibles(idPersona);
        } else if (kind == QueryPersonasKind.PROVEEDORES_DE_COMPRADOR) {
            subqueryIdPersonas = queryIdProveedoresDisponibles(idPersona);
        } else {
            throw Errors.illegalState();
        }

        // Construir query.
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");

        // Proyecciones.
        builder.append("  p.id as \"id\", ");
        builder.append("  coalesce(pj.razon_social, pf.nombre || ' ' || pf.apellido) as \"razon_social\", ");
        builder.append("  coalesce(p.ruc || '-' || p.dv_ruc) as \"ruc\", ");
        builder.append("  p.id_tipo_persona \"tipo_persona\" ");

        // Origen de datos.
        builder.append(" from info.persona as p ");
        builder.append(" left join info.persona_fisica as pf on pf.id = p.id ");
        builder.append(" left join info.persona_juridica as pj on pj.id = p.id ");

        // Filtros.
        builder.append(" where ");
        builder.append("   1 = 1 ");
        builder.append("   and p.id in ( %s ) ", subqueryIdPersonas);

        return builder.toString();
    }

    private PersonaInfo mapPersonaInfo(Map<String, Object> row) {
        PersonaInfo instance = new PersonaInfo();
        instance.setId(Maps.integer(row, "id"));
        instance.setRazonSocial(Maps.string(row, "razon_social"));
        instance.setRuc(Maps.string(row, "ruc"));
        instance.setTipoPersona(TipoPersonaEnum.fromIdTipoPersona(Maps.integer(row, "tipo_persona")));
        return instance;
    }

    /**
     * Obtiene la lista de entidades financieras con las que puede operar una
     * persona.
     *
     * @param idPersona
     * @return
     */
    public Result<Collection<Persona>> obtenerEntidadesFinancierasDisponibles(Integer idPersona) {
        Result<Collection<Persona>> result;

        try {
            Integer[] ids = obtenerIdPersonasEnRol(idPersona, PerfilEnum.ENTIDAD_FINANCIERA);
            return obtenerPorIds(ids);
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error consultando entidades financieras disponibles")
                    .build();
        }

        return result;
    }

    /**
     * Obtiene la lista de entidades financieras con las que puede operar una
     * persona.
     *
     * @param idPersona
     * @return
     */
    public Result<Collection<Persona>> obtenerEntidadesFinancierasDisponibles() {
        return obtenerEntidadesFinancierasDisponibles(null);
    }

    public Result<Persona> registrarPersonaJuridica(String razonSocial, String nombreFantasia, String ruc) {
        Result<Persona> result;

        try {
            // Registrar persona.
            RegistrarPersonaInput registrarPersona = new RegistrarPersonaInput();
            registrarPersona.tipo = TipoPersonaEnum.JURIDICA;
            registrarPersona.razonSocial = razonSocial;
            registrarPersona.nombreFantasia = nombreFantasia;
            registrarPersona.ruc = ruc;
            Persona persona = registrarPersona(registrarPersona);

            // Asignar local por defecto.
            crearLocal(persona);

            result = Results.ok()
                    .value(persona)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error registrando persona juridica")
                    .tag("persona.razon_social", razonSocial)
                    .tag("persona.nombre_fantasia", nombreFantasia)
                    .tag("persona.ruc", ruc)
                    .build();
        }

        return result;
    }

    public Result<Persona> registrarPersonaFisica(String nombres, String apellidos, String ci, String ruc) {
        Result<Persona> result;

        try {
            // Registrar persona.
            RegistrarPersonaInput registrarPersona = new RegistrarPersonaInput();
            registrarPersona.tipo = TipoPersonaEnum.FISICA;
            registrarPersona.nombres = nombres;
            registrarPersona.apellidos = apellidos;
            registrarPersona.ci = ci;
            registrarPersona.ruc = ruc;
            Persona persona = registrarPersona(registrarPersona);

            // Asignar local por defecto.
            crearLocal(persona);

            result = Results.ok()
                    .value(persona)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error registrando persona fisica")
                    .tag("persona.nombres", nombres)
                    .tag("persona.apellidos", apellidos)
                    .tag("persona.ruc", ruc)
                    .build();
        }

        return result;
    }

    private Persona registrarPersona(RegistrarPersonaInput input) throws Throwable {
        // Registrar persona.
        // NOTE: El identificador para tipo de persona es identico al valor en mercurio.
        Persona persona = new Persona();
        persona.setIdTipoPersona(input.tipo.idTipoPersona);
        if (Assertions.isRuc(input.ruc)) {
            RucInfo ruc0 = separarRuc(input.ruc);
            persona.setRuc(ruc0.ruc);
            persona.setDvRuc(Numeric.wrap(ruc0.dvRuc));
        } else {
            persona.setRuc(input.ruc);
        }
        persona.setEstado('A');
        persona = jpaUtils.create(persona);

        if (input.tipo == TipoPersonaEnum.JURIDICA) {
            // Registrar datos especificos de persona.
            PersonaJuridica personaJuridica = new PersonaJuridica();
            personaJuridica.setId(persona.getId());
            personaJuridica.setRazonSocial(input.razonSocial);
            personaJuridica.setNombreFantasia(input.nombreFantasia);
            jpaUtils.create(personaJuridica);

            // Datos adicionales no contemplados en BD.
            persona.setRazonSocial(input.razonSocial.trim());
        }

        if (input.tipo == TipoPersonaEnum.FISICA) {
            // Registrar datos especificos de persona.
            PersonaFisica personaFisica = new PersonaFisica();
            personaFisica.setId(persona.getId());
            personaFisica.setNombre(input.nombres);
            personaFisica.setApellido(input.apellidos);
            personaFisica.setNroDocumento(input.ci);
            jpaUtils.create(personaFisica);

            // Datos adicionales no contemplados en BD.
            persona.setRazonSocial((input.nombres + input.apellidos).trim());
        }

        return persona;
    }

    public Result<PersonaRol> agregarRol(Integer idPersona, PerfilEnum perfil) {
        Result<PersonaRol> result;

        try {
            // Obtener rol.
            Integer idRol = getIdRol(perfil);

            // Verificar rol actual.
            if (tieneRol(idPersona, idRol)) {
                // Actualizar rol por si exista como inactivo.
                actualizarRol(idPersona, idRol);

                // Ok.
                return Results.ok()
                        .nullable(true)
                        .value(null)
                        .build();
            }

            // Asignar rol.
            PersonaRol personaRol = new PersonaRol();
            personaRol.setPersonaRolPK(new PersonaRolPK());
            personaRol.getPersonaRolPK().setIdPersona(idPersona);
            personaRol.getPersonaRolPK().setIdRol(getIdRol(perfil));
            personaRol.setEstado('A');
            personaRol = jpaUtils.create(personaRol);

            result = Results.ok()
                    .value(personaRol)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .build();
        }

        return result;
    }

    private boolean tieneRol(Integer idPersona, Integer idRol) throws Throwable {
        String query = Strings.format("select count(*) as \"total\" from trans.persona_rol as pr where pr.id_persona = %s and pr.id_rol = %s ", idPersona, idRol);
        Integer total = jpaUtils.getTotal(query);
        return (total > 0);
    }

    private void actualizarRol(Integer idPersona, Integer idRol) throws Throwable {
        String query = Strings.format("update trans.persona_rol set estado = 'A' where id_persona = %s and id_rol = %s ", idPersona, idRol);
        Integer total = jpaUtils.executeUpdate(query);
        if (total <= 0) {
            throw Errors.illegalState("Ocurrió un error actualizando rol.");
        }
    }

    public Result<Void> agregarRelacion(Integer idProveedor, Integer idComprador) {
        Result<Void> result;

        try {
            // Verificar relacion.
            if (tieneRelacion(idProveedor, idComprador)) {
                // Actualizar relacion por si exista como inactivo.
                actualizarRelacion(idProveedor, idComprador);

                // Ok.
                return Results.ok()
                        .nullable(true)
                        .value(null)
                        .build();
            }

            // Crear relacion comercial.
            ProveedorComprador proveedorComprador = new ProveedorComprador();
            proveedorComprador.setProveedorCompradorPK(new ProveedorCompradorPK());
            proveedorComprador.getProveedorCompradorPK().setIdProveedor(idProveedor);
            proveedorComprador.getProveedorCompradorPK().setIdComprador(idComprador);
            proveedorComprador.setFechaInsercion(Fechas.ahora());
            proveedorComprador.setEstado('A');
            proveedorComprador = jpaUtils.create(proveedorComprador);

            result = Results.ok()
                    .nullable(true)
                    .value(null)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .build();
        }

        return result;
    }

    private boolean tieneRelacion(Integer idProveedor, Integer idComprador) throws Throwable {
        String query = Strings.format("select count(*) as \"total\" from trans.proveedor_comprador as pc where pc.id_proveedor = %s and pc.id_comprador = %s ", idProveedor, idComprador);
        Integer total = jpaUtils.getTotal(query);
        return (total > 0);
    }

    private void actualizarRelacion(Integer idProveedor, Integer idComprador) throws Throwable {
        String query = Strings.format("update trans.proveedor_comprador set estado = 'A', fecha_modificacion = now() where id_proveedor = %s and id_comprador = %s ", idProveedor, idComprador);
        Integer total = jpaUtils.executeUpdate(query);
        if (total <= 0) {
            throw Errors.illegalState("Ocurrió un error actualizando relacion comercial.");
        }
    }

    private Persona mapPersona(Map<String, Object> row) {
        Integer id = Maps.integer(row, "id_persona");
        Integer idTipoPersona = Maps.integer(row, "id_tipo_persona");
        Integer idDireccion = Maps.integer(row, "id_direccion");
        Character estado = Maps.character(row, "estado");
        String ruc = Maps.string(row, "ruc");
        BigDecimal dvRuc = Maps.bigdecimal(row, "dv_ruc");
        String razonSocial = Maps.string(row, "razon_social");
        String pjRazonSocial = Maps.string(row, "pj_razon_social", "");
        String pjNombreFantasia = Maps.string(row, "pj_nombre_fantasia", "");
        String pfNombres = Maps.string(row, "pf_nombres", "");
        String pfApellidos = Maps.string(row, "pf_apellidos", "");
        String pfCi = Maps.string(row, "pf_ci");

        Persona instance = new Persona();
        instance.setId(id);
        instance.setIdTipoPersona(idTipoPersona);
        instance.setIdDireccion(idDireccion);
        instance.setEstado(estado);
        instance.setRuc(ruc);
        instance.setDvRuc(dvRuc);
        instance.setRazonSocial(razonSocial);
        instance.setPjRazonSocial(pjRazonSocial);
        instance.setPjNombreFantasia(pjNombreFantasia);
        instance.setPfNombres(pfNombres);
        instance.setPfApellidos(pfApellidos);
        instance.setPfCi(pfCi);
        return instance;
    }

    private Integer getIdRol(PerfilEnum perfil) {
        if (perfil == PerfilEnum.COMPRADOR) {
            return 1;
        }

        if (perfil == PerfilEnum.PROVEEDOR) {
            return 2;
        }

        throw Errors.illegalArgument("Rol '%s' desconocido.", perfil);
    }

    //<editor-fold defaultstate="collapsed" desc="Gestión de Datos - Direccion">
    private void crearLocal(Persona persona) throws Throwable {
        // Utilizar partes de RUC como valor de GLN.
        String gln = persona.getRuc();
        if (persona.getDvRuc() != null) {
            gln = gln + String.valueOf(persona.getDvRuc().intValue());
        }

        // Construir query.
        String query = "insert into info.local(id, id_persona, descripcion, gln, estado, observacion, facturable, entidad) values(1, %s, '---', %s, 'A', '', 'N', 'N');";
        query = Strings.format(query, persona.getId(), gln);
        jpaUtils.executeUpdate(query);
    }

    private void asociarDireccion(Integer idPersona, Integer idDireccion) throws Throwable {
        String sql = Strings.format(" update info.local set id_direccion = %s where id = 1 and id_persona = %s ", idDireccion, idPersona);
        jpaUtils.executeUpdate(sql);
    }

    private Integer obtenerIdBarrio(Integer idDireccion) throws Throwable {
        // Construir query.
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");
        builder.append(" c.id_barrio as \"id\" ");
        builder.append(" from info.direccion as d ");
        builder.append(" left join info.calle as c on c.id = d.id_calle1 ");
        builder.append(" where d.id = %s ", idDireccion);

        String sql = builder.toString();
        Map<String, Object> row = jpaUtils.getFirst(sql);
        Object id0 = Maps.get(row, "id");
        if (id0 != null) {
            return Numeric.adaptAsInteger(id0);
        } else {
            return null;
        }
    }

    private Barrio crearBarrio(String descripcion) throws Throwable {
        Barrio instance = new Barrio();
        instance.setIdLocalidad(0);
        instance.setDescripcion(descripcion);
        instance = jpaUtils.create(instance);
        return instance;
    }

    private Integer obtenerIdDireccion(Integer idPersona) throws Throwable {
        String sql = Strings.format(" select id_direccion from info.local where id = 1 and id_persona = %s ", idPersona);
        Map<String, Object> row = jpaUtils.getFirst(sql);
        Object id0 = Maps.get(row, "id_direccion");
        if (id0 != null) {
            return Numeric.adaptAsInteger(id0);
        } else {
            return null;
        }
    }

    private Integer obtenerIdCalle(Integer idDireccion, Integer calle) throws Throwable {
        String sql = Strings.format(" select id_calle%s as \"id\" from info.direccion where id = %s ", calle, idDireccion);
        Map<String, Object> row = jpaUtils.getFirst(sql);
        Object id0 = Maps.get(row, "id");
        if (id0 != null) {
            return Numeric.adaptAsInteger(id0);
        } else {
            return null;
        }
    }

    private Calle crearCalle(Integer idBarrio, String descripcion) throws Throwable {
        Calle instance = new Calle();
        instance.setIdBarrio(idBarrio);
        instance.setDescripcion(descripcion);
        instance = jpaUtils.create(instance);
        return instance;
    }

    private void asociarCalle(Integer idDireccion, Integer idCalle, Integer calle) throws Throwable {
        String sql = Strings.format("update info.direccion set id_calle%s = %s where id = %s", calle, idCalle, idDireccion);
        jpaUtils.executeUpdate(sql);
    }

    private Direccion crearDireccion(Integer idPersona) throws Throwable {
        // Crear registros auxiliares.
        Barrio barrio = crearBarrio("");
        Calle calle1 = crearCalle(barrio.getId(), "");
        Calle calle2 = crearCalle(barrio.getId(), "");
        Calle calle3 = crearCalle(barrio.getId(), "");
        Calle calle4 = crearCalle(barrio.getId(), "");

        // Crear direccion.
        Direccion direccion = new Direccion();
        direccion.setIdCalle1(calle1.getId());
        direccion.setIdCalle2(calle2.getId());
        direccion.setIdCalle3(calle3.getId());
        direccion.setIdCalle4(calle4.getId());
        direccion.setNumero("");
        direccion.setObservacion("");
        direccion.setLatitud("");
        direccion.setLongitud("");
        direccion.setDireccion("");
        direccion = jpaUtils.create(direccion);

        // Asociar direccion a local por defecto.
        asociarDireccion(idPersona, direccion.getId());

        return direccion;
    }

    private Direccion obtenerOCrearDireccion(Integer idPersona) throws Throwable {
        Integer idDireccion = obtenerIdDireccion(idPersona);
        if (idDireccion == null) {
            return crearDireccion(idPersona);
        } else {
            Query q = em.createNamedQuery("Direccion.obtenerPorId");
            q.setParameter("idDireccion", idDireccion);
            return (Direccion) q.getSingleResult();
        }
    }

    private Barrio obtenerOCrearBarrio(Integer idDireccion) throws Throwable {
        Integer idBarrio = obtenerIdBarrio(idDireccion);
        if (idBarrio == null) {
            return crearBarrio("");
        } else {
            Query q = em.createNamedQuery("Barrio.obtenerPorId");
            q.setParameter("idBarrio", idBarrio);
            return (Barrio) q.getSingleResult();
        }
    }

    private Calle obtenerOCrearCalle(Integer idDireccion, Integer calle) throws Throwable {
        Integer idCalle = obtenerIdCalle(idDireccion, calle);
        if (idCalle == null) {
            Integer idBarrio = obtenerOCrearBarrio(idDireccion).getId();
            Calle calle0 = crearCalle(idBarrio, "");
            asociarCalle(idDireccion, calle0.getId(), calle);
            return calle0;
        } else {
            Query q = em.createNamedQuery("Calle.obtenerPorId");
            q.setParameter("idCalle", idCalle);
            return (Calle) q.getSingleResult();
        }
    }

    public void actualizarDatosPersonaJuridica(Integer idPersona, String razonSocial, String nombreFantasia, String ruc) throws Throwable {
        // Actualizar ruc.
        actualizarRuc(idPersona, ruc);

        // Actualizar datos informativos.
        String query = Strings.format(" update info.persona_juridica set razon_social = '%s', nombre_fantasia = '%s' where id = %s ", razonSocial, nombreFantasia, idPersona);
        jpaUtils.executeUpdate(query);
    }

    public void actualizarDatosPersonaFisica(Integer idPersona, String nombres, String apellidos, String ci, String ruc) throws Throwable {
        // Actualizar ruc.
        actualizarRuc(idPersona, ruc);

        // Actualizar datos informativos.
        String query = Strings.format(" update info.persona_fisica set nombre = '%s', apellido = '%s', nro_documento = '%s' where id = %s ", nombres, apellidos, ci, idPersona);
        jpaUtils.executeUpdate(query);
    }

    public void actualizarRuc(Integer idPersona, String ruc) throws Throwable {
        RucInfo ruc0 = separarRuc(ruc);
        String query = Strings.format(" update info.persona set ruc = '%s', dv_ruc = %s where id = %s ", ruc0.ruc, ruc0.dvRuc, idPersona);
        jpaUtils.executeUpdate(query);
    }

    public void actualizarBarrio(Integer idPersona, String descripcion) throws Throwable {
        Direccion direccion = obtenerOCrearDireccion(idPersona);
        Barrio barrio = obtenerOCrearBarrio(direccion.getId());
        barrio.setDescripcion(descripcion);
        jpaUtils.edit(barrio);
    }

    public void actualizarCalle1(Integer idPersona, String descripcion) throws Throwable {
        actualizarCalle(idPersona, 1, descripcion);
    }

    public void actualizarCalle2(Integer idPersona, String descripcion) throws Throwable {
        actualizarCalle(idPersona, 2, descripcion);
    }

    public void actualizarCalle3(Integer idPersona, String descripcion) throws Throwable {
        actualizarCalle(idPersona, 3, descripcion);
    }

    public void actualizarCalle4(Integer idPersona, String descripcion) throws Throwable {
        actualizarCalle(idPersona, 4, descripcion);
    }

    private void actualizarCalle(Integer idPersona, Integer calle, String descripcion) throws Throwable {
        Direccion direccion = obtenerOCrearDireccion(idPersona);
        Calle calle0 = obtenerOCrearCalle(direccion.getId(), calle);
        calle0.setDescripcion(descripcion);
        jpaUtils.edit(calle0);
    }

    public void actualizarNroCasa(Integer idPersona, String nroCasa) throws Throwable {
        Direccion direccion = obtenerOCrearDireccion(idPersona);
        direccion.setNumero(nroCasa);
        jpaUtils.edit(direccion);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Gestión de Datos - Teléfono">
    private Integer obtenerIdTipoTelefono(TipoTelefonoEnum tipo) throws Throwable {
        String sql = Strings.format(" select id from info.tipo_telefono where tipo = '%s' ", tipo.descripcion());
        Map<String, Object> row = jpaUtils.getFirst(sql);
        Object id = Maps.get(row, "id");
        if (id != null) {
            return Numeric.adaptAsInteger(id);
        } else {
            return null;
        }
    }

    private Integer obtenerIdTelefono(Integer idPersona, Integer idTipoTelefono) throws Throwable {
        StringBuilder2 builder = new StringBuilder2();
        builder.append(" select ");
        builder.append("   id_telefono ");
        builder.append(" from info.local_telefono as lt ");
        builder.append(" left join info.telefono as t on t.id = lt.id_telefono ");
        builder.append(" where ");
        builder.append("   1 = 1 ");
        builder.append("   and lt.id_persona = %s ", idPersona);
        builder.append("   and lt.id_local = 1 ");
        builder.append("   and t.id_tipo = %s ", idTipoTelefono);

        String sql = builder.toString();
        Map<String, Object> row = jpaUtils.getFirst(sql);
        Object id0 = Maps.get(row, "id_telefono");
        if (id0 != null) {
            return Numeric.adaptAsInteger(id0);
        } else {
            return null;
        }
    }

    private Telefono crearTelefono(TipoTelefonoEnum tipo) throws Throwable {
        Telefono instance = new Telefono();
        instance.setIdTipo(obtenerIdTipoTelefono(tipo));
        instance.setIdPrefijo(0);
        instance.setIdCuenta(null);
        instance.setPrincipal("S");
        instance.setNumero("");
        instance = jpaUtils.create(instance);
        return instance;
    }

    private void asociarTelefono(Integer idPersona, Integer idTelefono) throws Throwable {
        String sql = Strings.format("insert into info.local_telefono(id_persona, id_local, id_telefono) values(%s, %s, %s)", idPersona, 1, idTelefono);
        jpaUtils.executeUpdate(sql);
    }

    private Telefono obtenerOCrearTelefono(Integer idPersona, TipoTelefonoEnum tipo) throws Throwable {
        Integer idTipoTelefono = obtenerIdTipoTelefono(tipo);
        Integer idTelefono = obtenerIdTelefono(idPersona, idTipoTelefono);
        if (idTelefono == null) {
            Telefono telefono = crearTelefono(tipo);
            asociarTelefono(idPersona, telefono.getId());
            return telefono;
        } else {
            Query q = em.createNamedQuery("Telefono.obtenerPorId");
            q.setParameter("idTelefono", idTelefono);
            return (Telefono) q.getSingleResult();
        }
    }

    public void actualizarTelefonoLineaBaja(Integer idPersona, String numero) throws Throwable {
        Telefono telefono = obtenerOCrearTelefono(idPersona, TipoTelefonoEnum.LINEA_BAJA);
        telefono.setNumero(numero);
        jpaUtils.edit(telefono);
    }

    public void actualizarTelefonoMovil(Integer idPersona, String numero) throws Throwable {
        Telefono telefono = obtenerOCrearTelefono(idPersona, TipoTelefonoEnum.MOVIL);
        telefono.setNumero(numero);
        jpaUtils.edit(telefono);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Gestión de Datos - Email">
    public void actualizarEmail(Integer idPersona, String email) throws Throwable {
        Email email0 = obtenerOCrearEmail(idPersona);
        email0.setEmail(email);
        jpaUtils.edit(email0);
    }

    private Email obtenerOCrearEmail(Integer idPersona) throws Throwable {
        Integer idEmail = obtenerIdEmail(idPersona);
        if (idEmail == null) {
            Email email = crearEmail(idPersona, "");
            return email;
        } else {
            return emails.obtenerPorId(idEmail);
        }
    }

    private Integer obtenerIdEmail(Integer idPersona) throws Throwable {
        List<ContactoEmail> contactos = emails.obtenerContactosEmailDisponibles(idPersona);
        if (Assertions.isNullOrEmpty(contactos)) {
            return null;
        } else {
            ContactoEmail contacto = Lists.first(contactos);
            return contacto.getIdEmail();
        }
    }

    private Email crearEmail(Integer idPersona, String email) throws Throwable {
        Result<Integer> result = emails.registrarEmail(idPersona, email);
        result.raise();

        Integer idEmail = result.value();
        return emails.obtenerPorId(idEmail);
    }
    //</editor-fold>

    @Data
    private static class RegistrarPersonaInput implements Serializable {

        private TipoPersonaEnum tipo;
        private String razonSocial;
        private String nombreFantasia;
        private String nombres;
        private String apellidos;
        private String ci;
        private String ruc;

        public RegistrarPersonaInput() {
            this.tipo = null;
            this.razonSocial = "";
            this.nombreFantasia = "";
            this.nombres = "";
            this.apellidos = "";
            this.ruc = "";
        }
    }

    private static class RucInfo implements Serializable {

        private String ruc;
        private String dvRuc;
    }

    public enum TipoPersonaEnum {
        FISICA(1, "Persona Física"),
        JURIDICA(2, "Persona Jurídica");
        private final Integer idTipoPersona;
        private final String descripcion;

        private TipoPersonaEnum(Integer idTipoPersona, String descripcion) {
            this.idTipoPersona = idTipoPersona;
            this.descripcion = descripcion;
        }

        public static TipoPersonaEnum fromIdTipoPersona(Integer idTipoPersona) {
            for (TipoPersonaEnum enum0 : TipoPersonaEnum.values()) {
                if (Objects.equals(enum0.idTipoPersona, idTipoPersona)) {
                    return enum0;
                }
            }
            throw Errors.illegalState();
        }

        public String descripcion() {
            return descripcion;
        }

    }
}
