/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ejb.facades.factoring;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.utils.AbstractFacade;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "PerfilFacade", mappedName = "PerfilFacade")
@LocalBean
public class PerfilFacade extends AbstractFacade<Perfil> {

    /**
     * Constructor.
     */
    public PerfilFacade() {
        super(Perfil.class);
    }

    /**
     * Obtiene un perfil en base a su descripcion.
     *
     * @param descripcion Descripcion en base de datos.
     * @return Perfil.
     */
    public Perfil findByDescripcion(String descripcion) {
        Query q = getEntityManager().createNamedQuery("Perfil.findByDescripcion");
        q.setParameter("descripcion", descripcion);
        q.setParameter("estado", 'A');
        return JpaUtils.getFirst(q);
    }
}
