<#ftl encoding='UTF-8'>

<#if generarFullHtml?? && generarFullHtml == true>
<!DOCTYPE html>
<html lang="es" style="margin: 0; background-color: white; color: black !important;font-family: -apple-system, system-ui, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, sans-serif;">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta charset="utf-8">
        </head>
    <body bgcolor="white" style="margin: 0; background-color: white; color: black !important; font-family: -apple-system, system-ui, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, sans-serif;">
<#else>
        <div bgcolor="white" style="margin: 0; background-color: white; color: black !important; font-family: -apple-system, system-ui, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, sans-serif;">
</#if>

            <img src="${urlTracking}" alt="" style="display: none !important;"/>

            <div style="background-color: #e1a200;">
                <div style="padding-left: 8%; padding-right: 8%; max-width:640px; margin: 0 auto;">
                    <div  style="color: white; font-size: 18pt; padding-top: 30px; padding-bottom: 30px; font-weight: 700; margin: 0;">
                        ${titulo}
                    </div>
                </div>
            </div>

            <div style="background-color: white; padding-top: 40px; padding-bottom: 60px;">
                <div style="padding-left: 8%; padding-right: 8%; max-width:640px; margin: 0 auto;">
                    <#list mensajes as mensaje>
                    <p align="justify" style="text-align: justify;">${mensaje}</p>
                    </#list>
                </div>

                <div style="max-width:640px; padding-top: 40px; margin: 0 auto;">
                    <table style="border-collapse: collapse;width: 100%;">
                        <tr>
                            <th style="padding: 8px;text-align: left;border-bottom: 1px solid #ddd;">Número de Factura</th>
                            <th style="padding: 8px;text-align: left;border-bottom: 1px solid #ddd;">Fecha de Emisión</th>
                            <th style="padding: 8px;text-align: left;border-bottom: 1px solid #ddd;">Fecha de Vencimiento</th>
                            <th style="padding: 8px;text-align: right;border-bottom: 1px solid #ddd;">Monto Total</th>
                            <#if usarMontoConformado?? && usarMontoConformado == true>
                            <th style="padding: 8px;text-align: right;border-bottom: 1px solid #ddd;">Monto Conformado</th>
                            </#if>
                            <#if usarMontosDesembolso?? && usarMontosDesembolso == true>
                            <th style="padding: 8px;text-align: right;border-bottom: 1px solid #ddd;">Comisión Operativa</th>
                            <th style="padding: 8px;text-align: right;border-bottom: 1px solid #ddd;">Monto Desembolsado</th>
                            </#if>
                        </tr>
                        <#list facturas as factura>
                        <tr>
                            <td style="padding: 8px;text-align: left;border-bottom: 1px solid #ddd;">${factura.numero}</td>
                            <td style="padding: 8px;text-align: left;border-bottom: 1px solid #ddd;">${factura.fechaEmision}</td>
                            <td style="padding: 8px;text-align: left;border-bottom: 1px solid #ddd;">${factura.fechaVencimiento}</td>
                            <td style="padding: 8px;text-align: right;border-bottom: 1px solid #ddd;">${factura.montoTotal}</td>
                            <#if usarMontoConformado?? && usarMontoConformado == true>
                                <#if factura.montoTotalConformado??>
                                <td style="padding: 8px;text-align: right;border-bottom: 1px solid #ddd;">${factura.montoTotalConformado}</td>
                                <#else>
                                <td style="padding: 8px;text-align: right;border-bottom: 1px solid #ddd;">N/D</td>
                                </#if>
                            </#if>
                            <#if usarMontosDesembolso?? && usarMontosDesembolso == true>
                                <td style="padding: 8px;text-align: right;border-bottom: 1px solid #ddd;">${factura.montoTotalDescuento}</td>
                                <td style="padding: 8px;text-align: right;border-bottom: 1px solid #ddd;">${factura.montoTotalDesembolso}</td>
                            </#if>
                        </tr>
                        </#list>
                    </table>
                </div>
            </div>

            <div style="background-color: #434a54; color: #dfdfdf;">
                <div style="padding-left: 8%; padding-right: 8%; padding-top: 15px; padding-bottom: 15px; max-width:640px; margin: 0 auto;">
                    <div><strong>Sistemas Electrónicos del Paraguay S.A.</strong></div>
                    <div style="text-decoration: none !important;">Teléfono: (021) 238 7500</div>
                    <div style="text-decoration: none !important;">Email: info@sepsa.com.py</div>
                </div>
            </div>
<#if generarFullHtml?? && generarFullHtml == true>
    </body>
</html>
<#else>
</div>
</#if>
