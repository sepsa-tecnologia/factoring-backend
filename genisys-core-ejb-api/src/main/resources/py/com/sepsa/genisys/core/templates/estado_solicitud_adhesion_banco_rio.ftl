<table class="table table-striped">
    <thead>
        <tr>
            <th style="width: 30%; max-width: 30%;"></th>
            <th style="width: 70%; max-width: 70%;"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Código de Cliente
            </td>
            <td>
                <span>${codCliente}</span>
            </td>
        </tr>
        <tr>
            <td>
                Fecha de Solicitud
            </td>
            <td>
                <span>${fechaSolicitud}</span>
            </td>
        </tr>
        <tr>
            <td>
                Estado
            </td>
            <td>
                <span>${estado}</span>
            </td>
        </tr>
        <tr>
            <td>
                Observaciones
            </td>
            <td>
                <span>${observaciones}</span>
            </td>
        </tr>
    </tbody>
</table>