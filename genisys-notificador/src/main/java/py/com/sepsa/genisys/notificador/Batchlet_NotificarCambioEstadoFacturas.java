/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.notificador;

import fa.gs.utils.logging.app.AppLogger;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.logic.impl.PF_NotificarCambiosEstadoFacturas;
import py.com.sepsa.genisys.ejb.utils.AbstractBatchlet;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("Batchlet_NotificarCambioEstadoFacturas")
public class Batchlet_NotificarCambioEstadoFacturas extends AbstractBatchlet {

    public static final String JOB_NAME = "NOTIFICAR_CAMBIO_ESTADO_FACTURAS";

    @EJB
    private PF_NotificarCambiosEstadoFacturas notificarCambiosEstadoFacturas;

    private AppLogger log;

    @PostConstruct
    public void init() {
        this.log = AppLoggerFactory.notificador();
    }

    @Override
    protected String getName() {
        return JOB_NAME;
    }
    
    @Override
    protected long getPeriod() {
        return TimeUnit.SECONDS.toMillis(30);
    }

    @Override
    protected boolean continueOnFailure() {
        return true;
    }

    @Override
    protected void work() throws Throwable {
        try {
            notificarCambiosEstadoFacturas.work();
        } catch (Throwable thr) {
            log.error(thr, "Ocurrió un error generando notificaciones");
            throw thr;
        }
    }

}
