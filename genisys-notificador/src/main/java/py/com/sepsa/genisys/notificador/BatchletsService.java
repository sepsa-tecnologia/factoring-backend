/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.notificador;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author Fabio A. González Sosa
 */
@Singleton
@Startup
@LocalBean
public class BatchletsService implements Serializable {

    private JobOperator operator;

    @PostConstruct
    public void init() {
        // Iniciar batchlets.
        operator = BatchRuntime.getJobOperator();
        operator.start(Batchlet_NotificarCambioEstadoFacturas.JOB_NAME, null);
        operator.start(Batchlet_NotificacionFacturasAVencer.JOB_NAME, null);
        operator.start(Batchlet_NotificacionFacturasConformadas.JOB_NAME, null);
    }

}
