/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.notificador;

import fa.gs.utils.logging.app.AppLogger;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.logic.impl.PF_NotificarFacturasConformadas;
import py.com.sepsa.genisys.ejb.utils.AbstractBatchlet;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("Batchlet_NotificacionFacturasConformadas")
public class Batchlet_NotificacionFacturasConformadas extends AbstractBatchlet {

    public static final String JOB_NAME = "NOTIFICAR_FACTURAS_CONFORMADAS";

    @EJB
    private PF_NotificarFacturasConformadas actualizarFacturasConformadas;

    private AppLogger log;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.notificador();
    }

    @Override
    protected String getName() {
        return JOB_NAME;
    }

    @Override
    protected long getPeriod() {
        return readPeriodConfig(ConfiguracionGeneralEnum.NOTIFICACION_FACTURA_CONFORMADA_PROVEEDOR_INTERVALO, TimeUnit.MINUTES.toMillis(5));
    }

    @Override
    protected boolean continueOnFailure() {
        return true;
    }

    @Override
    protected void work() throws Throwable {
        try {
            actualizarFacturasConformadas.work();
        } catch (Throwable thr) {
            log.error(thr, "Ocurrió un error notificando alerta de facturas pendientes de conformación");
            throw thr;
        }
    }

}
