/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.bancorio;

import fa.gs.utils.logging.app.AppLogger;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.utils.AbstractBatchlet;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_ActualizarLiquidacionesPendientes;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("Batchlet_ConsultaEstadoLiquidaciones")
public class Batchlet_ConsultaEstadoLiquidaciones extends AbstractBatchlet {

    public static final String JOB_NAME = "CONSULTAR_ESTADO_LIQUIDACIONES";

    @EJB
    private PF_BancoRio_ActualizarLiquidacionesPendientes actualizarLiquidacionesPendientes;

    private AppLogger log;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    @Override
    protected String getName() {
        return JOB_NAME;
    }

    @Override
    protected long getPeriod() {
        return readPeriodConfig(ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_INGESTION_INTERVALO, TimeUnit.MINUTES.toMillis(5));
    }

    @Override
    protected boolean continueOnFailure() {
        return true;
    }

    @Override
    public void work() throws Throwable {
        try {
            actualizarLiquidacionesPendientes.work();
        } catch (Throwable thr) {
            log.error(thr, "Ocurrió un error importando estado de liquidaciones");
            throw thr;
        }
    }

}
