/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.bancorio;

import fa.gs.utils.logging.app.AppLogger;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.utils.AbstractBatchlet;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_AdherirCompradores;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("Batchlet_AgregarCompradores")
public class Batchlet_AgregarCompradores extends AbstractBatchlet {

    public static final String JOB_NAME = "AGREGAR_COMPRADORES";

    @EJB
    private PF_BancoRio_AdherirCompradores adherirCompradores;

    private AppLogger log;

    @PostConstruct
    public void init() {
        log = AppLoggerFactory.ingest("banco-rio");
    }

    @Override
    protected String getName() {
        return JOB_NAME;
    }

    @Override
    protected long getPeriod() {
        return TimeUnit.MINUTES.toMillis(30);
    }

    @Override
    protected boolean continueOnFailure() {
        return true;
    }

    @Override
    protected void work() throws Throwable {
        try {
            adherirCompradores.adherirCompradores();
        } catch (Throwable thr) {
            log.error(thr, "Ocurrió un error verificando nuevos compradores");
            throw thr;
        }
    }

}
