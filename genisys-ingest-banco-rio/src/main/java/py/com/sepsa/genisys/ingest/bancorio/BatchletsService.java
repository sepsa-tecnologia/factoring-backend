/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.bancorio;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author Fabio A. González Sosa
 */
@Singleton
@Startup
@LocalBean
public class BatchletsService implements Serializable {

    private JobOperator operator;

    @PostConstruct
    public void init() {
        operator = BatchRuntime.getJobOperator();

        /**
         * Consulta la lista de compradores adheridos. Los nuevos compradores
         * son agregados automaticamente y para aquellos ya existentes se
         * actualizan sus datos.
         */
        operator.start(Batchlet_AgregarCompradores.JOB_NAME, null);

        /**
         * Consulta el estado de solicitudes de adhesion pendientes.
         * Inicialmente afecta a las solicitudes de adhesion realizadas por
         * proveedores.
         */
        operator.start(Batchlet_ConsultaEstadoAdhesiones.JOB_NAME, null);

        /**
         * Consulta el estado de solicitudes de desembolsos pendientes. Para
         * desembolsos realizados, se cambia el estado de las facturas a pagadas
         * (desembolsdas).
         */
        operator.start(Batchlet_ConsultaEstadoDesembolsos.JOB_NAME, null);

        /**
         * Consulta el estado de facturas desembolsadas, cuando el banco recibe
         * el pago por parte del comprador. El banco indica que las facturas
         * estan cobradas y listas para liqudiar. Se cambia el estado de las
         * facturas a cobradas.
         */
        operator.start(Batchlet_ConsultaEstadoCobros.JOB_NAME, null);

        /**
         * Consulta la lista de facturas cobradas y realiza la solicitud de
         * liquidacion de las mismas al banco.
         */
        operator.start(Batchlet_AgregarLiquidaciones.JOB_NAME, null);

        /**
         * Consulta el estado de facturas cobradas cuya solicitud de liquidacion
         * fue enviada al banco. Se cambia el estado de las facturas a
         * liquidadas.
         */
        operator.start(Batchlet_ConsultaEstadoLiquidaciones.JOB_NAME, null);
    }

}
