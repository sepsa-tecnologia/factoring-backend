/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio.pojos;

import fa.gs.utils.misc.json.serialization.JsonPostConstruct;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class JBancoRioDireccion implements Serializable {

    @JsonProperty
    private String calle1;

    @JsonProperty
    private String calle2;

    @JsonProperty(name = "nro_calle")
    private String nroCalle;

    @JsonProperty
    private String barrio;

    @JsonProperty(name = "codigo_ciudad")
    private String codigoCiudad;

    @JsonPostConstruct
    public void init() {
        if (calle1 == null) {
            calle1 = "";
        }
        if (calle2 == null) {
            calle2 = "";
        }
        if (nroCalle == null) {
            nroCalle = "";
        }
        if (barrio == null) {
            barrio = "";
        }
        if (codigoCiudad == null) {
            codigoCiudad = "";
        }
    }

}
