/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio.pojos;

import fa.gs.utils.misc.json.serialization.JsonPostConstruct;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class JBancoRioContacto implements Serializable {

    @JsonProperty(name = "nro_linea_baja")
    private String nroLineaBaja;

    @JsonProperty(name = "nro_celular")
    private String nroCelular;

    @JsonProperty
    private String email;

    @JsonPostConstruct
    public void init() {
        if (nroLineaBaja == null) {
            nroLineaBaja = "";
        }
        if (nroCelular == null) {
            nroCelular = "";
        }
        if (email == null) {
            email = "";
        }
    }
}
