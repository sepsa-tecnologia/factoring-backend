/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas;

import com.google.gson.JsonElement;
import fa.gs.misc.fechas.Fechas;
import fa.gs.utils.database.query.expressions.literals.SQLStringLiterals;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.adapter.impl.Json2Epoch;
import fa.gs.utils.misc.json.serialization.JsonPostConstruct;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import fa.gs.utils.misc.json.serialization.JsonResolution;
import fa.gs.utils.misc.pagination.Pagination;
import fa.gs.utils.misc.text.Strings;
import fa.gs.utils.rest.controllers.RestControllerActionWithCustomParam;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.pojos.JPaginacion;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
public class FacturasBusquedaAvanzadaAction extends RestControllerActionWithCustomParam<FacturasBusquedaAvanzadaAction.Input> {

    @Override
    public Response doAction(Input params) throws Throwable {
        // Adaptar parametros.
        Map<String, Object> filtros = adaptarFiltros(params);
        Pagination paginacion = adaptarPaginacion(params);

        // Obtener datos.
        Facturas facturas = Injection.lookup(Facturas.class);
        Integer total = facturas.countFacturas(filtros);
        if (total == null) {
            return ControllersUtils.responseKo("Error al contar total de facturas");
        }

        List<FacturaInfo> datos = facturas.selectFacturas(paginacion.getLimit().intValue(), paginacion.getOffset().intValue(), filtros, null);
        if (datos == null) {
            return ControllersUtils.responseKo("Ocurrió un error al obtener facturas");
        }

        // Preparar datos de salida.
        JsonElement json = ControllersUtils.slice(params.paginacion.getPage(), params.paginacion.getPageSize(), total.longValue(), datos, this::adapt2Json);
        return ControllersUtils.responseOk(json);
    }

    private Map<String, Object> adaptarFiltros(Input params) {
        Map<String, Object> filters = new HashMap<>();

        // Filtro de id factura.
        if (params.getFiltros().idFactura != null) {
            filters.put("id", Strings.format(" f.id = %s ", params.getFiltros().idFactura));
        }

        // Filtro de estado.
        if (params.getFiltros().estado != null) {
            filters.put("idEstado.descripcion", Strings.format(" e.descripcion = '%s' ", params.getFiltros().estado));
        }

        // Filtro de monto total.
        if (params.getFiltros().montoTotal != null) {
            filters.put("montoTotalFactura", Strings.format(" f.monto_total_factura = %s ", params.getFiltros().montoTotal));
        }

        // Filtro de días de crédito.
        if (params.getFiltros().diasCredito != null) {
            filters.put("diasCredito", Strings.format(" f.dias_credito = %s ", params.getFiltros().diasCredito));
        }

        // Filtro fecha desde.
        if (params.getFiltros().fechaDesde != null) {
            Date fecha = Fechas.inicioDia(params.getFiltros().fechaDesde);
            filters.put("fecha$geq", Strings.format(" f.fecha >= %s ", SQLStringLiterals.fechaHora(fecha)));
        }

        // Filtro fecha hasta.
        if (params.getFiltros().fechaHasta != null) {
            Date fecha = Fechas.finDia(params.getFiltros().fechaHasta);
            filters.put("fecha$leq", Strings.format(" f.fecha <= %s ", SQLStringLiterals.fechaHora(fecha)));
        }

        // Filtro fecha pago desde.
        if (params.getFiltros().fechaPagoDesde != null) {
            Date fecha = Fechas.inicioDia(params.getFiltros().fechaPagoDesde);
            filters.put("fechaPago$geq", Strings.format(" f.fecha_pago >= %s ", SQLStringLiterals.fechaHora(fecha)));
        }

        // Filtro fecha pago hasta.
        if (params.getFiltros().fechaPagoHasta != null) {
            Date fecha = Fechas.finDia(params.getFiltros().fechaPagoHasta);
            filters.put("fechaPago$leq", Strings.format(" f.fecha_pago <= %s ", SQLStringLiterals.fechaHora(fecha)));
        }

        // Filtro de mostrar expiradas.
        if (params.getFiltros().mostrarExpiradas != null && params.getFiltros().mostrarExpiradas == true) {
            filters.remove("fechaPago$geq");
            filters.remove("fechaPago$leq");
        }

        // Filtro de razon social de Comprador.
        if (!Assertions.stringNullOrEmpty(params.getFiltros().razonSocialComprador)) {
            String literal = SQLStringLiterals.phrasify(params.getFiltros().razonSocialComprador);
            filters.put("razonSocialComprador", Strings.format(" f.razon_social_comprador ILIKE '%s' ", literal));
        }

        // Filtro de RUC de Comprador.
        if (!Assertions.stringNullOrEmpty(params.getFiltros().rucComprador)) {
            String literal = SQLStringLiterals.phrasify(params.getFiltros().rucComprador);
            filters.put("rucComprador", Strings.format(" f.ruc_comprador ILIKE '%s' ", literal));
        }

        // Filtro de razon social de Proveedor.
        if (!Assertions.stringNullOrEmpty(params.getFiltros().razonSocialProveedor)) {
            String literal = SQLStringLiterals.phrasify(params.getFiltros().razonSocialProveedor);
            filters.put("razonSocialProveedor", Strings.format(" f.razon_social_proveedor ILIKE '%s' ", literal));
        }

        // Filtro de RUC de Proveedor.
        if (!Assertions.stringNullOrEmpty(params.getFiltros().rucProveedor)) {
            String literal = SQLStringLiterals.phrasify(params.getFiltros().rucProveedor);
            filters.put("rucProveedor", Strings.format(" f.ruc_proveedor ILIKE '%s' ", literal));
        }

        // Filtro de Número de Factura.
        if (!Assertions.stringNullOrEmpty(params.getFiltros().nroFactura)) {
            String literal = SQLStringLiterals.phrasify(params.getFiltros().nroFactura);
            filters.put("nroFactura", Strings.format(" f.nro_factura ILIKE '%s' ", literal));
        }
        return filters;
    }

    private Pagination adaptarPaginacion(Input params) {
        if (params.paginacion != null) {
            return Pagination.fromPages(params.paginacion.getPage(), params.paginacion.getPageSize());
        } else {
            return Pagination.fromPages(1L, 20L);
        }
    }

    private JsonElement adapt2Json(FacturaInfo instance) {
        JsonObjectBuilder ob = JsonObjectBuilder.instance();
        ob.add("id", instance.getId());
        ob.add("nro_factura", instance.getNumero());
        ob.add("estado", instance.getEstadoDescripcion());
        ob.add("monto_total", instance.getMontoTotal());
        ob.add("plazo", instance.getDiasPlazo());
        ob.add("razon_social_comprador", instance.getCompradorRazonSocial());
        ob.add("ruc_comprador", instance.getCompradorRuc());
        ob.add("razon_social_proveedor", instance.getProveedorRazonSocial());
        ob.add("ruc_proveedor", instance.getProveedorRuc());
        ob.add("fecha", instance.getFechaEmision());
        ob.add("fecha_pago", instance.getFechaPago());
        return ob.build();
    }

    @Data
    public static class Input {

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private Filtros filtros;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private JPaginacion paginacion;

        @JsonPostConstruct
        public void init() {
            if (paginacion == null) {
                this.paginacion = new JPaginacion();
            }
        }

    }

    @Data
    public static class Filtros {

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private Integer idFactura;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String nroFactura;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String estado;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private BigDecimal montoTotal;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private Integer diasCredito;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String razonSocialComprador;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String rucComprador;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String razonSocialProveedor;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String rucProveedor;

        @JsonProperty(resolution = JsonResolution.OPTIONAL, fromJsonAdapter = Json2Epoch.class)
        private Date fechaDesde;

        @JsonProperty(resolution = JsonResolution.OPTIONAL, fromJsonAdapter = Json2Epoch.class)
        private Date fechaHasta;

        @JsonProperty(resolution = JsonResolution.OPTIONAL, fromJsonAdapter = Json2Epoch.class)
        private Date fechaPagoDesde;

        @JsonProperty(resolution = JsonResolution.OPTIONAL, fromJsonAdapter = Json2Epoch.class)
        private Date fechaPagoHasta;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private Boolean mostrarExpiradas;

    }

}
