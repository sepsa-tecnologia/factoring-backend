/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.servicio;

import com.google.gson.JsonElement;
import fa.gs.misc.fechas.Fechas;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.json.JsonArrayBuilder;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.rest.controllers.RestControllerActionWithNoParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.util.concurrent.TimeUnit;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.logic.impl.Batchlets;
import py.com.sepsa.genisys.ejb.utils.JobStamp;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ListarJobsAction extends RestControllerActionWithNoParam {

    @Override
    public Response doAction() throws Throwable {
        JsonArrayBuilder ab = JsonArrayBuilder.instance();

        Batchlets batchlets = Batchlets.inject();
        JobStamp[] stamps = batchlets.list();
        if (!Assertions.isNullOrEmpty(stamps)) {
            for (JobStamp stamp : stamps) {
                JsonObjectBuilder ob = JsonObjectBuilder.instance();
                ob.add("name", stamp.getName());
                ob.add("last_run", Fechas.toString(Fechas.fromEpoch(stamp.getLastRun()), "yyyy-MM-dd HH:mm:ss"));
                ob.add("last_period", formatMillis(stamp.getLastPeriod()));
                ob.add("runs", stamp.getRuns());
                ob.add("avg_ellapsed", stamp.getRuns() > 0 ? formatMillis(stamp.getEllapsed() / stamp.getRuns()) : "0");
                ab.add(ob.build());
            }
        }

        JsonElement json = ab.build();
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    private String formatMillis(Long millis) {
        if (millis == null) {
            return "n/d";
        }

        long D = TimeUnit.DAYS.toMillis(1);
        long H = TimeUnit.HOURS.toMillis(1);
        long M = TimeUnit.MINUTES.toMillis(1);
        long S = TimeUnit.SECONDS.toMillis(1);
        if (millis > D) {
            double v = millis / D;
            return String.format("%.02fD", v);
        } else if (millis > H) {
            double v = millis / H;
            return String.format("%.02fH", v);
        } else if (millis > M) {
            double v = millis / M;
            return String.format("%.02fM", v);
        } else if (millis > S) {
            double v = millis / S;
            return String.format("%.02fS", v);
        } else {
            double v = millis / 1;
            return String.format("%.02f", v);
        }
    }

}
