/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.proveedores;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonArrayBuilder;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.io.Serializable;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ConsultarAction extends RestControllerActionWithJsonObjectParam {

    private Personas personas;

    public ConsultarAction() {
        this.personas = Injection.lookup(Personas.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        Input input = processInput(json0);

        // Obtener persona.
        Result<Persona> resPersona = personas.obtenerPorRuc(input.ruc);
        if (resPersona.isFailure()) {
            return ControllersUtils.responseKo(resPersona);
        }

        // Persona.
        Persona persona = resPersona.value();

        // Preparar datos de salida.
        JsonArrayBuilder aBuilder = JsonArrayBuilder.instance();
        JsonObjectBuilder jBuilder = JsonObjectBuilder.instance();
        jBuilder.add("id", Ids.mask(persona.getId()));
        jBuilder.add("razon_social", Text.razonSocial(persona));
        jBuilder.add("ruc", Text.ruc(persona));
        jBuilder.add("personeria", Text.tipoPersona(persona));
        aBuilder.add(jBuilder.build());

        JsonElement json = aBuilder.build();
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    private Input processInput(JsonObject json) throws Throwable {
        // RUC.
        String ruc = JsonResolver.string(json, "proveedor.ruc", null);
        if (Assertions.stringNullOrEmpty(ruc)) {
            throw Errors.illegalArgument("El campo RUC no puede ser vacío o nulo.");
        }
        if (!Assertions.isRuc(ruc)) {
            throw Errors.illegalArgument("El campo RUC es inválido.");
        }

        Input input = new Input();
        input.ruc = ruc;
        return input;
    }

    @Data
    private static class Input implements Serializable {

        String ruc;
    }
}
