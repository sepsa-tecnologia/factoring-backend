/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas;

import fa.gs.result.simple.Result;
import fa.gs.utils.misc.errors.Errors;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.rest.controllers.facturas.pojos.JIdentificacionFactura;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Utils {

    /**
     * Obtiene los datos de una factura dado los parametros de entrada.
     *
     * @param idComprador Identificador de persona comprador.
     * @param identificacion Datos de identificacion, que contempla dos formatos
     * diferentes.
     * @return Datos de factura, si hubiere.
     */
    public static FacturaInfo identificarFactura(Integer idComprador, JIdentificacionFactura identificacion) throws Throwable {
        Personas personas = Injection.lookup(Personas.class);
        Facturas facturas = Injection.lookup(Facturas.class);

        FacturaInfo factura = null;

        // Determinar factura por datos de forma 1.
        if (identificacion.getForma() == JIdentificacionFactura.Forma.POR_ID) {
            // Obtener factura.
            Result<FacturaInfo> resFactura = facturas.buscarFactura(identificacion.getIdFactura());
            if (resFactura.isFailure()) {
                throw Errors.illegalState("No se pudo determinar la factura.");
            }

            // Factura.
            factura = resFactura.value();
        }

        // Determinar factura por datos de forma 2.
        if (identificacion.getForma() == JIdentificacionFactura.Forma.POR_DATOS_FACTURA) {
            // Obtener comprador.
            Result<Persona> resComprador = personas.obtenerPorId(idComprador);
            if (resComprador.isFailure()) {
                throw Errors.illegalState("No se pudo determinar el comprador.");
            }

            // Obtener proveedor.
            Result<Persona> resProveedor = personas.obtenerPorRuc(identificacion.getRucProveedor());
            if (resProveedor.isFailure()) {
                throw Errors.illegalState("No se pudo determinar el proveedor.");
            }

            // Datos de origen de factura.
            Persona comprador = resComprador.value();
            Persona proveedor = resProveedor.value();

            // Obtener factura.
            Result<FacturaInfo> resFactura = facturas.buscarFactura(identificacion.getNumero(), Text.ruc(proveedor), Text.ruc(comprador), identificacion.getFechaEmision());
            if (resFactura.isFailure()) {
                throw Errors.illegalState("No se pudo determinar la factura.");
            }

            // Factura.
            factura = resFactura.value();
        }

        return factura;
    }

}
