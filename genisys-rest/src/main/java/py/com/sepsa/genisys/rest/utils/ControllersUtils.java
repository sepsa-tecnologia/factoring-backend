/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.utils;

import com.google.gson.JsonElement;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errno;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonArrayBuilder;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.text.Strings;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.util.Collection;
import java.util.function.Function;
import javax.ws.rs.core.Response;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ControllersUtils {

    public static Errno errno(int code) {
        int code0 = Math.abs(code);
        return Errors.errno("API", code0);
    }

    public static Response responseKo(fa.gs.result.simple.Result<?> result) {
        return ServiceResponse.ko()
                .cause(result.failure().message())
                .build();
    }

    public static Response responseKo(fa.gs.utils.result.simple.Result result) {
        return ServiceResponse.ko()
                .cause(result)
                .build();
    }

    public static Response responseKo(String fmt, Object... args) {
        String msg = Strings.format(fmt, args);
        return ServiceResponse.ko()
                .cause(msg)
                .build();
    }

    public static Response responseOk() {
        return ServiceResponse.ok()
                .build();
    }

    public static Response responseOk(JsonElement json) {
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    public static <T> JsonElement slice(Long page, Long pageSize, Long total, Collection<T> elements, Function<T, JsonElement> mapper) {
        // Paginacion.
        JsonObjectBuilder op = JsonObjectBuilder.instance();
        op.add("page", page);
        op.add("pageSize", pageSize);
        op.add("total", total);

        // Datos.
        JsonArrayBuilder ab = JsonArrayBuilder.instance();
        if (!Assertions.isNullOrEmpty(elements)) {
            for (T element : elements) {
                if (element != null) {
                    JsonElement jelement = mapper.apply(element);
                    if (jelement != null && jelement.isJsonNull() == false) {
                        ab.add(jelement);
                    }
                }
            }
        }

        // Datos finales.
        JsonObjectBuilder ob = JsonObjectBuilder.instance();
        ob.add("total", op.build());
        ob.add("data", ab.build());
        return ob.build();
    }

}
