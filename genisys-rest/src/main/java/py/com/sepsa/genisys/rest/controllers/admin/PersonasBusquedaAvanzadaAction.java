/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.admin;

import com.google.gson.JsonElement;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.serialization.JsonPostConstruct;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import fa.gs.utils.misc.json.serialization.JsonResolution;
import fa.gs.utils.misc.pagination.Pagination;
import fa.gs.utils.rest.controllers.RestControllerActionWithCustomParam;
import java.util.Collection;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.pojos.PersonaInfo2;
import py.com.sepsa.genisys.ejb.logic.pojos.PersonasBusquedaAvanzadaInput;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.pojos.JPaginacion;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class PersonasBusquedaAvanzadaAction extends RestControllerActionWithCustomParam<PersonasBusquedaAvanzadaAction.Input> {

    @Override
    public Response doAction(Input params) throws Throwable {
        // Adaptar parametros.
        PersonasBusquedaAvanzadaInput input = adaptarFiltros(params);
        Pagination paginacion = adaptarPaginacion(params);

        // Obtener datos.
        Personas personas = Injection.lookup(Personas.class);
        Result<Long> resTotal = personas.obtenerTotalPersonasPorBusquedaAvanzada(input, paginacion);
        if (resTotal.isFailure()) {
            return ControllersUtils.responseKo(resTotal);
        }

        Result<Collection<PersonaInfo2>> resDatos = personas.obtenerPersonasPorBusquedaAvanzada(input, paginacion);
        if (resDatos.isFailure()) {
            return ControllersUtils.responseKo(resDatos);
        }

        // Preparar datos de salida.
        Long total = resTotal.value();
        Collection<PersonaInfo2> datos = resDatos.value();
        JsonElement json = ControllersUtils.slice(params.paginacion.getPage(), params.paginacion.getPageSize(), total, datos, this::adapt2Json);
        return ControllersUtils.responseOk(json);
    }

    private PersonasBusquedaAvanzadaInput adaptarFiltros(Input params) {
        PersonasBusquedaAvanzadaInput input = new PersonasBusquedaAvanzadaInput();
        input.setRuc(params.filtros.ruc);
        input.setRazonSocial(params.filtros.razonSocial);
        input.setNombreFantasia(params.filtros.nombreFantasia);
        input.setNombres(params.filtros.nombres);
        input.setApellidos(params.filtros.apellidos);
        input.setNroDocumento(params.filtros.nroDocumento);
        input.setPersoneria(params.filtros.personeria);
        return input;
    }

    private Pagination adaptarPaginacion(Input params) {
        if (params.paginacion != null) {
            return Pagination.fromPages(params.paginacion.getPage(), params.paginacion.getPageSize());
        } else {
            return Pagination.fromPages(1L, 20L);
        }
    }

    private JsonElement adapt2Json(PersonaInfo2 instance) {
        JsonObjectBuilder ob = JsonObjectBuilder.instance();
        ob.add("id", instance.getId());
        ob.add("razon_social", instance.getRazonSocial());
        ob.add("nombre_fantasia", instance.getNombreFantasia());
        ob.add("nombres", instance.getNombres());
        ob.add("apellidos", instance.getApellidos());
        ob.add("nro_documento", instance.getNroDocumento());
        ob.add("ruc", instance.getRuc());
        ob.add("dv_ruc", instance.getDvRuc());
        ob.add("tipo_persona", instance.getTipoPersona().descripcion());
        ob.add("total_compradores", instance.getTotalCompradores());
        ob.add("total_proveedores", instance.getTotalProveedores());
        ob.add("es_comprador", instance.getEsComprador());
        ob.add("es_proveedor", instance.getEsProveedor());
        return ob.build();
    }

    @Data
    public static class Input {

        @JsonProperty
        private Filtros filtros;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private JPaginacion paginacion;

        @JsonPostConstruct
        public void init() {
            if (paginacion == null) {
                this.paginacion = new JPaginacion();
            }
        }

    }

    @Data
    public static class Filtros {

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String razonSocial;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String nombreFantasia;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String nombres;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String apellidos;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String nroDocumento;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private String ruc;

        @JsonProperty(resolution = JsonResolution.OPTIONAL)
        private Personas.TipoPersonaEnum personeria;

    }

}
