/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.admin;

import com.google.gson.JsonElement;
import fa.gs.misc.Assertions;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import fa.gs.utils.rest.controllers.RestControllerActionWithCustomParam;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.pojos.RegistrarPersonaInput;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
public class PersonaJuridicaEditarAction extends RestControllerActionWithCustomParam<PersonaJuridicaEditarAction.Input> {

    @Override
    public Response doAction(Input params) throws Throwable {
        // Adaptar parametros.
        RegistrarPersonaInput input = adaptarInput(params);

        // Validar RUC único
        String ruc = params.ruc;
        Personas personas = Injection.lookup(Personas.class);
        Persona persona = personas.obtenerPorRuc(ruc).value(null);
        if (Assertions.isNull(persona)) {
            return ControllersUtils.responseKo("No se encuentra una persona con el RUC especificado.");
        }

        // Validar datos
        String err = validarInput(params);
        if (!Assertions.isNull(err)) {
            return ControllersUtils.responseKo(err);
        }

        // Editar datos.
        personas.actualizarDatosPersonaJuridica(persona.getId(), input.getRazonSocial(), input.getNombreFantasia(), input.getRuc());

        // Preparar Respuesta
        persona = personas.obtenerPorRuc(ruc).value(null);
        JsonElement json = adapt2Json(persona);
        return ControllersUtils.responseOk(json);
    }

    private RegistrarPersonaInput adaptarInput(Input params) {
        RegistrarPersonaInput input = new RegistrarPersonaInput();
        input.setRuc(params.ruc);
        input.setRazonSocial(params.razonSocial);
        input.setNombreFantasia(params.nombreFantasia);

        input.setTipo(Personas.TipoPersonaEnum.JURIDICA);
        return input;
    }

    private String validarInput(Input params) {

        // Validar RUC
        if (Assertions.stringNullOrEmpty(params.ruc)) {
            return "El campo 'ruc' es mandatorio";
        }

        // Validar razón social
        if (Assertions.stringNullOrEmpty(params.razonSocial)) {
            return "El campo 'razonSocial' es mandatorio";
        }

        // Validar apellidos
        if (Assertions.stringNullOrEmpty(params.nombreFantasia)) {
            return "El campo 'nombreFantasia' es mandatorio";
        }

        return null;
    }

    private JsonElement adapt2Json(Persona instance) {
        JsonObjectBuilder ob = JsonObjectBuilder.instance();
        ob.add("id", instance.getId());
        ob.add("razon_social", instance.getPjRazonSocial());
        ob.add("nombre_fantasia", instance.getPjNombreFantasia());
        ob.add("ruc", instance.getRuc());
        ob.add("dv_ruc", instance.getDvRuc());
        ob.add("tipo_persona", Personas.TipoPersonaEnum.fromIdTipoPersona(instance.getIdTipoPersona()).descripcion());
        return ob.build();
    }

    @Data
    public static class Input {

        @JsonProperty
        private String razonSocial;

        @JsonProperty
        private String nombreFantasia;

        @JsonProperty
        private String ruc;

    }

}
