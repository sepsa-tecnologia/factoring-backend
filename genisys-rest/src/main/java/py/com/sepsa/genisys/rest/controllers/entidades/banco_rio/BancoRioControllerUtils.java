/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.codec.binary.Base64;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioActividadEconomica;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioBanco;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioCiudad;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioPersona;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioRegion;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoDocumento;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoSociedad;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Entidades;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroCompradorPersonaJuridicaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroCompradorPersonaJuridicaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaFisicaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaFisicaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaJuridicaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaJuridicaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;

/**
 *
 * @author Fabio A. González Sosa
 */
public class BancoRioControllerUtils {

    public static final String DIRECCION_REFERENCIA = "Y";

    static Perfil obtenerPerfil(PerfilEnum perfilEnum) {
        Perfiles perfiles = Injection.lookup(Perfiles.class);
        Perfil perfil = perfiles.obtenerPerfil(perfilEnum);
        return perfil;
    }

    static Result<RegistroCompradorPersonaJuridicaOutput> registrarComprador(Persona persona, RegistroCompradorPersonaJuridicaInput input) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        return bancoRio.registrarComprador(
                persona.getId(),
                obtenerPerfil(PerfilEnum.COMPRADOR).getId(),
                bancoRio.obtenerIdEntidadBancoRio(),
                input
        );
    }

    static Result<RegistroProveedorPersonaJuridicaOutput> registrarProveedor(Persona persona, RegistroProveedorPersonaJuridicaInput input) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        return bancoRio.registrarProveedor(
                persona.getId(),
                obtenerPerfil(PerfilEnum.PROVEEDOR).getId(),
                bancoRio.obtenerIdEntidadBancoRio(),
                input
        );
    }

    static Result<RegistroProveedorPersonaFisicaOutput> registrarProveedor(Persona persona, RegistroProveedorPersonaFisicaInput input) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        return bancoRio.registrarProveedor(
                persona.getId(),
                obtenerPerfil(PerfilEnum.PROVEEDOR).getId(),
                bancoRio.obtenerIdEntidadBancoRio(),
                input
        );
    }

    static String validarRuc(String ruc) {
        if (Assertions.stringNullOrEmpty(ruc)) {
            throw Errors.illegalArgument("El campo RUC no puede ser nulo o vacío.");
        }

        boolean a = Assertions.isPositiveInteger(ruc);
        boolean b = Assertions.isRuc(ruc);
        if (!(a || b)) {
            throw Errors.illegalArgument("El campo RUC es inválido.");
        }

        return ruc;
    }

    static String validarRazonSocial(String razonSocial) {
        if (Assertions.stringNullOrEmpty(razonSocial)) {
            throw Errors.illegalArgument("El campo razón social no puede ser nulo o vacío.");
        }
        return razonSocial;
    }

    static Persona validarPersona(String ruc) {
        Personas personas = Injection.lookup(Personas.class);
        Result<Persona> resPersona = personas.obtenerPorRuc(ruc);
        Persona persona = resPersona.value(null);
        if (persona == null) {
            throw Errors.illegalArgument("El campo RUC no corresponde a una persona registrada.");
        }
        return persona;
    }

    static String validarNroDocumento(String nroDocumento) {
        if (Assertions.stringNullOrEmpty(nroDocumento)) {
            throw Errors.illegalArgument("El campo número de documento no puede ser nulo o vacío.");
        }
        return nroDocumento;
    }

    static BancoRioTipoDocumento validarTipoDocumento(String codigoTipoDocumento) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        Result<BancoRioTipoDocumento> resTipoDocumento = bancoRio.obtenerTipoDocumentoPorSigla(codigoTipoDocumento);
        BancoRioTipoDocumento tipoDocumento = resTipoDocumento.value(null);
        if (tipoDocumento == null) {
            throw Errors.illegalArgument("El campo código de tipo para documento es inválido.");
        }
        return tipoDocumento;
    }

    static byte[] validarImagenDocumento(String imagen) {
        if (Assertions.stringNullOrEmpty(imagen)) {
            throw Errors.illegalArgument("El campo imágen para documento no puede ser nulo o vacío.");
        }
        if (!org.apache.commons.codec.binary.Base64.isBase64(imagen)) {
            throw Errors.illegalArgument("El campo imágen para documento no es un valor válido codificado en base64.");
        }
        byte[] b64 = Base64.decodeBase64(imagen);
        long maxSize = 5 * 1024 * 1024;
        if (b64.length > maxSize) {
            throw Errors.illegalArgument("El campo imágen para documento supera el límite impuesto (5MB).");
        }
        return b64;
    }

    static BancoRioTipoSociedad validarTipoSociendad(String codigoTipoSociedad) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        Result<BancoRioTipoSociedad> resTipoSociedad = bancoRio.obtenerTipoSociedadPorSigla(codigoTipoSociedad);
        BancoRioTipoSociedad tipoSociedad = resTipoSociedad.value(null);
        if (tipoSociedad == null) {
            throw Errors.illegalArgument("El campo código de tipo para sociedad es inválido.");
        }
        return tipoSociedad;
    }

    static String validarCalle1(String calle1) {
        if (Assertions.stringNullOrEmpty(calle1)) {
            throw Errors.illegalArgument("El campo calle 1 no puede ser nulo o vacío.");
        }
        return calle1;
    }

    static String validarCalle2(String calle2) {
        if (Assertions.stringNullOrEmpty(calle2)) {
            throw Errors.illegalArgument("El campo calle 2 no puede ser nulo o vacío.");
        }
        return calle2;
    }

    static String validarNroCalle(String nroCalle) {
        if (Assertions.stringNullOrEmpty(nroCalle)) {
            throw Errors.illegalArgument("El campo número de calle no puede ser nulo o vacío.");
        }
        return nroCalle;
    }

    static BancoRioCiudad validarCiudad(String codigoCiudad) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        Result<BancoRioCiudad> resCiudad = bancoRio.obtenerCiudadPorCodigo(codigoCiudad);
        BancoRioCiudad ciudad = resCiudad.value(null);
        if (ciudad == null) {
            throw Errors.illegalArgument("El campo código de tipo para ciudad es inválido.");
        }
        return ciudad;
    }

    static BancoRioRegion validarRegion(String codigoRegion) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        Result<BancoRioRegion> resRegion = bancoRio.obtenerRegionPorCodigo(codigoRegion);
        BancoRioRegion region = resRegion.value(null);
        if (region == null) {
            throw Errors.illegalArgument("El campo código de tipo para región es inválido.");
        }
        return region;
    }

    static Date validarFechaDeConstitucion(Date fechaConstitucion) {
        if (fechaConstitucion == null) {
            throw Errors.illegalArgument("El campo fecha de constitución para sociedad no puede ser nulo o vacío.");
        }
        return fechaConstitucion;
    }

    static String validarNroCelular(String nroCelular) {
        if (Assertions.stringNullOrEmpty(nroCelular)) {
            throw Errors.illegalArgument("El número de celular para contacto no puede ser nulo o vacío.");
        }
        return nroCelular;
    }

    static String validarNroLineaBaja(String nroLineaBaja) {
        if (Assertions.stringNullOrEmpty(nroLineaBaja)) {
            throw Errors.illegalArgument("El número de línea baja para contacto no puede ser nulo o vacío.");
        }
        return nroLineaBaja;
    }

    static String validarEmail(String email) {
        if (Assertions.stringNullOrEmpty(email)) {
            throw Errors.illegalArgument("El email para contacto no puede ser nulo o vacío.");
        }
        if (!Assertions.isEmail(email)) {
            throw Errors.illegalArgument("El email para contacto es inválido.");
        }
        return email;
    }

    static BancoRioActividadEconomica validarActividadEconomica(String codigoActividadEconomica) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        Result<BancoRioActividadEconomica> resActividadEconomica = bancoRio.obtenerActividadEconomicaPorCodigo(codigoActividadEconomica);
        BancoRioActividadEconomica actividadEconomica = resActividadEconomica.value(null);
        if (actividadEconomica == null) {
            throw Errors.illegalArgument("El campo código de tipo para actividad económica es inválido.");
        }
        return actividadEconomica;
    }

    static BigDecimal validarFacturacionPromedio(BigDecimal facturacionPromedio) {
        if (facturacionPromedio == null) {
            throw Errors.illegalArgument("El campo facturación promedio es inválido.");
        }
        if (!Assertions.isPositiveDecimal(facturacionPromedio)) {
            throw Errors.illegalArgument("El campo facturación promedio debe ser mayor a cero.");
        }
        return facturacionPromedio;
    }

    static String validarBarrio(String barrio) {
        // No necesita validacion.
        return barrio;
    }

    static String validarRubro(String rubro) {
        if (Assertions.stringNullOrEmpty(rubro)) {
            rubro = "RUBRO";
        }
        return rubro;
    }

    static Persona validarCodigoClienteComprador(String codCliente) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        Result<Persona> resPersona = bancoRio.obtenerPersonaPorCodigoCliente(obtenerPerfil(PerfilEnum.COMPRADOR).getId(), codCliente);
        Persona persona = resPersona.value(null);
        if (persona == null) {
            throw Errors.illegalArgument("El campo código de cliente no corresponde a un comprador de referencia válido.");
        }
        return persona;
    }

    public static BancoRioPersona obtenerPersona(Persona persona, PerfilEnum perfil) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);

        // Id perfil.
        Perfil perfil0 = obtenerPerfil(perfil);
        Integer idPerfil = perfil0.getId();

        // Id persona.
        Integer idPersona = persona.getId();

        Result<BancoRioPersona> resEstado = bancoRio.obtenerPersona(idPersona, idPerfil);
        return resEstado.value(null);
    }

    public static Entidades.EstadoAdhesionEntidad obtenerEstadoAdhesionEntidad(Persona persona, PerfilEnum perfil) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        Entidades entidades = Injection.lookup(Entidades.class);

        // Id entidad.
        Integer idEntidadFinanciera = bancoRio.obtenerIdEntidadBancoRio();

        // Id perfil.
        Perfil perfil0 = obtenerPerfil(perfil);
        Integer idPerfil = perfil0.getId();

        // Id persona.
        Integer idPersona = persona.getId();

        Result<Entidades.EstadoAdhesionEntidad> resEstado = entidades.obtenerEstadoAdhesion(idPersona, idPerfil, idEntidadFinanciera);
        return resEstado.value(null);
    }

    static Entidades.EstadoAdhesionEntidad validarEstadoAdhesionOkEntidadProveedor(Persona persona) {
        if (persona == null) {
            return null;
        }

        Entidades.EstadoAdhesionEntidad estadoAdhesion = obtenerEstadoAdhesionEntidad(persona, PerfilEnum.PROVEEDOR);
        if (estadoAdhesion != null) {
            throw Errors.illegalArgument("El proveedor de referencia ya cuenta con una adhesión a la entidad con estado '%s'.", estadoAdhesion);
        }
        return estadoAdhesion;
    }

    static Entidades.EstadoAdhesionEntidad validarEstadoAdhesionKoEntidadProveedor(Persona persona) {
        if (persona == null) {
            return null;
        }

        Entidades.EstadoAdhesionEntidad estadoAdhesion = obtenerEstadoAdhesionEntidad(persona, PerfilEnum.PROVEEDOR);
        if (estadoAdhesion != Entidades.EstadoAdhesionEntidad.ACTIVO) {
            throw Errors.illegalArgument("El proveedor de referencia no cuenta con una adhesión a la entidad con estado '%s'.", Entidades.EstadoAdhesionEntidad.ACTIVO);
        }
        return estadoAdhesion;
    }

    static Entidades.EstadoAdhesionEntidad validarEstadoAdhesionOkEntidadComprador(Persona persona) {
        if (persona == null) {
            return null;
        }

        Entidades.EstadoAdhesionEntidad estadoAdhesion = obtenerEstadoAdhesionEntidad(persona, PerfilEnum.COMPRADOR);
        if (estadoAdhesion != null) {
            throw Errors.illegalArgument("El comprador de referencia ya cuenta con una adhesión a la entidad con estado '%s'.", estadoAdhesion);
        }
        return estadoAdhesion;
    }

    static Entidades.EstadoAdhesionEntidad validarEstadoAdhesionKoEntidadComprador(Persona persona) {
        if (persona == null) {
            return null;
        }

        Entidades.EstadoAdhesionEntidad estadoAdhesion = obtenerEstadoAdhesionEntidad(persona, PerfilEnum.COMPRADOR);
        if (estadoAdhesion != Entidades.EstadoAdhesionEntidad.ACTIVO) {
            throw Errors.illegalArgument("El comprador de referencia no cuenta con una adhesión a la entidad con estado '%s'.", Entidades.EstadoAdhesionEntidad.ACTIVO);
        }
        return estadoAdhesion;
    }

    static DocumentoInfo validarDocumentoComprador(Persona persona) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        DocumentoInfo documento = bancoRio.resolveDocumento(persona);
        if (documento == null) {
            throw Errors.illegalArgument("El campo código de cliente no corresponde a un comprador con documento de identificación válido.");
        }
        return documento;
    }

    static String validarNroCuenta(String nroCuenta) {
        if (Assertions.stringNullOrEmpty(nroCuenta)) {
            throw Errors.illegalArgument("El campo número de cuenta no puede ser nulo o vacío.");
        }
        return nroCuenta;
    }

    static BancoRioBanco validarBanco(String codBanco) {
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        Result<BancoRioBanco> resBanco = bancoRio.obtenerBancoPorCodigo(codBanco);
        BancoRioBanco banco = resBanco.value(null);
        if (banco == null) {
            throw Errors.illegalArgument("El campo código de tipo para banco es inválido.");
        }
        return banco;
    }

    static Date validarFechaNacimiento(Date fechaNacimiento) {
        if (fechaNacimiento == null) {
            throw Errors.illegalArgument("El fecha de nacimiento no puede ser nulo o vacío.");
        }
        return fechaNacimiento;
    }

}
