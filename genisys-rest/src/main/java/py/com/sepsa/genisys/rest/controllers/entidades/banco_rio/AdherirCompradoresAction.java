/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.rest.controllers.RestControllerActionWithNoParam;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_AdherirCompradores;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;
import py.com.sepsa.genisys.rest.utils.TxWrapperRest;

/**
 *
 * @author Fabio A. González Sosa
 */
public class AdherirCompradoresAction extends RestControllerActionWithNoParam {

    private final Notificaciones notificaciones;

    private final PF_BancoRio_AdherirCompradores adherirCompradores;

    public AdherirCompradoresAction() {
        this.notificaciones = Injection.lookup(Notificaciones.class);
        this.adherirCompradores = Injection.lookup(PF_BancoRio_AdherirCompradores.class);
    }

    @Override
    public Response doAction() throws Throwable {
        TxWrapperRest tx = Injection.lookup(TxWrapperRest.class);
        Result<?> result = tx.execute(this::adherirCompradores);
        if (result.isFailure()) {
            notificaciones.notificarSepsa(result.failure().cause(), "Ocurrió un error consumiendo lista de compradores activos");
            return ControllersUtils.responseKo("No se pudo procesar la lista de compradores activos");
        }

        return ControllersUtils.responseOk();
    }

    private Void adherirCompradores() throws Throwable {
        adherirCompradores.adherirCompradores();
        return null;
    }

}
