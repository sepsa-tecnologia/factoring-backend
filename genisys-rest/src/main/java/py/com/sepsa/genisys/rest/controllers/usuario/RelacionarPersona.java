/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.genisys.rest.controllers.usuario;

import fa.gs.result.simple.Result;
import fa.gs.utils.misc.errors.Errors;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RelacionarPersona {

    public static void relacionarProveedor(Integer idPersonaComprador, String rucProveedor) throws Throwable {
        final Personas personas = Injection.lookup(Personas.class);

        // Obtener comprador.
        Result<Persona> resPersona = personas.obtenerPorRuc(rucProveedor);
        Persona persona = resPersona.value(null);
        if (persona == null) {
            throw Errors.illegalArgument("Proveedor no existe.");
        }

        // Registrar relacion comercial.
        Result<Void> resRelacion = personas.agregarRelacion(persona.getId(), idPersonaComprador);
        resRelacion.raise();
    }

    public static void relacionarComprador(Integer idPersonaProveedor, String rucComprador) throws Throwable {
        final Personas personas = Injection.lookup(Personas.class);

        // Obtener comprador.
        Result<Persona> resPersona = personas.obtenerPorRuc(rucComprador);
        Persona persona = resPersona.value(null);
        if (persona == null) {
            throw Errors.illegalArgument("Comprador no existe.");
        }

        // Registrar relacion comercial.
        Result<Void> resRelacion = personas.agregarRelacion(idPersonaProveedor, persona.getId());
        resRelacion.raise();
    }

}
