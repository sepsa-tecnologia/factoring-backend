/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas;

import com.google.gson.JsonObject;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.facades.info.MonedaFacade;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.utils.Context;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RegistrarComoCompradorAction extends RestControllerActionWithJsonObjectParam {

    private Personas personas;

    private MonedaFacade monedas;

    public RegistrarComoCompradorAction() {
        this.personas = Injection.lookup(Personas.class);
        this.monedas = Injection.lookup(MonedaFacade.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        UserAuthenticationInfo auth = Context.getUserInfo();
        Input input = processInput(json0);

        // Obtener comprador.
        Result<Persona> resComprador = personas.obtenerPorId(auth.getMUsuario().getIdPersona());
        if (resComprador.isFailure()) {
            throw Errors.illegalState(resComprador.failure().cause(), "No se pudo determinar el comprador.");
        }

        // Validar comprador.
        Persona comprador = resComprador.value();
        if (comprador == null || comprador.getId() == null) {
            throw Errors.illegalState("No se pudo determinar el comprador.");
        }

        // Obtener proveedor.
        Result<Persona> resProveedor = personas.obtenerPorRuc(input.rucProveedor);
        if (resProveedor.isFailure()) {
            throw Errors.illegalState(resProveedor.failure().cause(), "No se pudo determinar el proveedor.");
        }

        // Validar proveedor.
        // TODO: VERIFICAR SI PROVEEDOR TIENE ROL PROVEEDOR.
        Persona proveedor = resProveedor.value();
        if (proveedor == null || proveedor.getId() == null) {
            throw Errors.illegalState("No se pudo determinar el proveedor.");
        }

        // Registrar factura.
        FacturaInfo factura = RegistrarComoProveedorAction.registrarFactura(auth.getMUsuario().getId(), proveedor, comprador, input.numero, input.fechaEmision, input.fechaPago, input.monto, input.moneda);
        return processOutput(factura);
    }

    private Input processInput(JsonObject json0) {
        // Numero de factura.
        String numero = JsonResolver.string(json0, "numero");
        if (Assertions.stringNullOrEmpty(numero)) {
            throw Errors.illegalArgument("Número de factura no puede ser vacío o nulo.");
        }
        if (!Assertions.isNumeroFactura(numero)) {
            throw Errors.illegalArgument("Número de factura no válido (xxx-xxx-xxxxxxx).");
        }

        // RUC Proveedor.
        String rucProveedor = JsonResolver.string(json0, "proveedor");
        if (Assertions.stringNullOrEmpty(rucProveedor)) {
            throw Errors.illegalArgument("Número de RUC de proveedor no puede ser vacío o nulo.");
        }
        if (!Assertions.isRuc(rucProveedor)) {
            throw Errors.illegalArgument("Número de RUC de proveedor no válido (xxxxxxxxx-x).");
        }

        // Fecha de emision.
        Date fechaEmision = JsonResolver.date(json0, "fecha_emision");
        if (Assertions.isNull(fechaEmision)) {
            throw Errors.illegalArgument("Fecha de emisión de factura no puede ser vacío o nulo.");
        }

        // Fecha de pago.
        Date fechaPago = JsonResolver.date(json0, "fecha_pago");
        if (Assertions.isNull(fechaPago)) {
            throw Errors.illegalArgument("Fecha de pago de factura no puede ser vacío o nulo.");
        }

        // Monto de factura.
        BigDecimal monto = JsonResolver.bigdecimal(json0, "monto");
        if (Assertions.isNull(monto)) {
            throw Errors.illegalArgument("Monto de factura no puede ser vacío o nulo.");
        }
        if (Numeric.menorIgual(monto, Numeric.CERO)) {
            throw Errors.illegalArgument("Monto de factura no puede ser menor o igual a cero (0).");
        }

        // Moneda.
        Moneda moneda = monedas.getGuarani();
        if (Assertions.isNull(moneda)) {
            throw Errors.illegalArgument("No se pudo determinar la moneda (Guaraní).");
        }

        Input input = new Input();
        input.setNumero(numero);
        input.setRucProveedor(rucProveedor);
        input.setFechaEmision(fechaEmision);
        input.setFechaPago(fechaPago);
        input.setMonto(monto);
        input.setMoneda(moneda);
        return input;
    }

    private Response processOutput(FacturaInfo factura) {
        // Preparar datos de salida.
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add("id_factura", Ids.mask(factura.getId()));
        builder.add("fecha_emision", factura.getFechaEmision());
        builder.add("fecha_pago", factura.getFechaPago());
        builder.add("numero", factura.getNumero());
        builder.add("monto", factura.getMontoTotal());
        builder.add("proveedor", factura.getProveedorRuc());

        JsonObject json = builder.build();
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    @Data
    private static class Input implements Serializable {

        String numero;
        String rucProveedor;
        Date fechaEmision;
        Date fechaPago;
        BigDecimal monto;
        Moneda moneda;
    }
}
