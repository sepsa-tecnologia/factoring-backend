/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import com.google.gson.JsonElement;
import fa.gs.misc.Assertions;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Units;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.fechas.Fechas;
import fa.gs.utils.misc.json.JsonArrayBuilder;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import fa.gs.utils.rest.controllers.RestControllerActionWithCustomParam;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaFacturasLiquidablesInput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaFacturasLiquidablesOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.RespuestaConsultaFacturasLiquidables;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ConsultarFacturasLiquidablesAction extends RestControllerActionWithCustomParam<ConsultarFacturasLiquidablesAction.Input> {

    @Override
    public Response doAction(ConsultarFacturasLiquidablesAction.Input params) throws Throwable {
        // Consultar estado de registro de proveedor.
        Input0 input0 = adaptarParametros(params);

        // Datos de consulta.
        ConsultaFacturasLiquidablesInput wsInput = new ConsultaFacturasLiquidablesInput();
        wsInput.setComprador(input0.comprador);
        wsInput.setCompradorDocumento(input0.compradorDocumento);
        wsInput.setFechaDesde(input0.fechaDesde);
        wsInput.setFechaHasta(input0.fechaHasta);

        // Consulta de datos.
        BancoRio rio = Injection.lookup(BancoRio.class);
        Result<ConsultaFacturasLiquidablesOutput> result = rio.consultarFacturasLiquidables(wsInput);
        if (result.isFailure()) {
            return ControllersUtils.responseKo(result);
        }

        // Control.
        ConsultaFacturasLiquidablesOutput output = result.value();
        if (!output.getRespuesta().isOk()) {
            String err = rio.resolveErrorMessage(output.getTrack(), output.getRespuesta());
            return ControllersUtils.responseKo(err);
        }

        // Ok.
        JsonElement json = adaptarDatosSalida(output);
        return ControllersUtils.responseOk(json);
    }

    private JsonElement adaptarDatosSalida(ConsultaFacturasLiquidablesOutput output) {
        JsonObjectBuilder ob = JsonObjectBuilder.instance();
        ob.add("codigo", output.getRespuesta().getCodigo());
        ob.add("mensaje", output.getRespuesta().getMensaje());
        ob.add("facturas", adaptar(output.getRespuesta().getFacturas()));
        return ob.build();
    }

    private JsonElement adaptar(Collection<RespuestaConsultaFacturasLiquidables.FacturaALiquidar> facturas) {
        JsonArrayBuilder ab = JsonArrayBuilder.instance();

        if (!Assertions.isNullOrEmpty(facturas)) {
            for (RespuestaConsultaFacturasLiquidables.FacturaALiquidar factura : facturas) {
                JsonObjectBuilder ob = JsonObjectBuilder.instance();
                ob.add("nro_operacion", factura.getNroOperacion());
                ob.add("id_solicitud", factura.getIdSolicitud());
                ob.add("nro_factura", factura.getNroFactura());
                ob.add("monto", factura.getMonto());
                ob.add("fecha_vto", Fechas.toString(factura.getFechaVto(), "yyyy-MM-dd"));
                ab.add(ob.build());
            }
        }

        return ab.build();
    }

    private Input0 adaptarParametros(ConsultarFacturasLiquidablesAction.Input params) {
        // Datos de input para operacion de registro de proveedor.
        String ruc = BancoRioControllerUtils.validarRuc(params.ruc);
        Persona comprador = BancoRioControllerUtils.validarPersona(params.ruc);

        // Documento asociado.
        BancoRio rio = Injection.lookup(BancoRio.class);
        DocumentoInfo documento = rio.resolveDocumento(comprador);
        if (documento == null) {
            throw Errors.illegalArgument("El comprador no posee un documeno válido registrado.");
        }

        // Fecha desde.
        Date fechaDesde = Units.execute(() -> Fechas.parse(params.fechaDesde, "yyyy-MM-dd"));

        // Fecha hasta.
        Date fechaHasta = Units.execute(() -> Fechas.parse(params.fechaHasta, "yyyy-MM-dd"));

        // Datos finales.
        Input0 input = new Input0();
        input.comprador = comprador;
        input.compradorDocumento = documento;
        input.fechaDesde = fechaDesde;
        input.fechaHasta = fechaHasta;
        return input;
    }

    private static class Input0 implements Serializable {

        private Persona comprador;
        private DocumentoInfo compradorDocumento;
        private Date fechaDesde;
        private Date fechaHasta;
    }

    @Data
    public static class Input implements Serializable {

        @JsonProperty
        private String ruc;

        @JsonProperty(name = "fecha_desde")
        private String fechaDesde;

        @JsonProperty(name = "fecha_hasta")
        private String fechaHasta;

    }

}
