/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import com.google.gson.JsonElement;
import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.json.JsonArrayBuilder;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.rest.controllers.RestControllerActionWithNoParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.util.Collection;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioRegion;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ListarRegionesAction extends RestControllerActionWithNoParam {

    private final BancoRio bancoRio;

    public ListarRegionesAction() {
        this.bancoRio = Injection.lookup(BancoRio.class);
    }

    @Override
    public Response doAction() throws Throwable {
        // Obtener bancos.
        Result<Collection<BancoRioRegion>> result = bancoRio.obtenerRegiones();
        if (result.isFailure()) {
            return ServiceResponse.ko()
                    .cause(result.failure().cause())
                    .build();
        }

        Collection<BancoRioRegion> regiones = result.value(Lists.empty());
        JsonElement json = adaptarDatosSalida(regiones);
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    private JsonElement adaptarDatosSalida(Collection<BancoRioRegion> regiones) {
        JsonArrayBuilder ab = JsonArrayBuilder.instance();
        if (!Assertions.isNullOrEmpty(regiones)) {
            for (BancoRioRegion region : regiones) {
                JsonObjectBuilder ob = JsonObjectBuilder.instance();
                ob.add("id", region.getId());
                ob.add("descripcion", region.getDescripcion());
                ob.add("codigo", region.getCodigo());
                ab.add(ob.build());
            }
        }
        return ab.build();
    }

}
