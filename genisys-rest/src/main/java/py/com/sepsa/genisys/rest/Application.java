/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest;

import fa.gs.utils.collections.Sets;
import fa.gs.utils.rest.responses.GsonMessageBodyWriter;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import javax.ws.rs.ApplicationPath;
import org.reflections.Reflections;
import py.com.sepsa.valkiria.api.rest.filters.AccessControlRequestFilter;
import py.com.sepsa.valkiria.api.rest.filters.AccessControlResponseFilter;

/**
 *
 * @author Fabio A. González Sosa
 */
@ApplicationPath("/api")
public class Application extends javax.ws.rs.core.Application {

    /**
     * Constructor.
     */
    public Application() {
        ;
    }

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> set = Sets.empty();
        set.addAll(super.getClasses());
        set.addAll(scanRestControllers());
        set.addAll(scanRequestFilterProviders());
        set.addAll(scanResponseFilterProviders());
        set.addAll(scanBodyParsers());
        return set;
    }

    public Collection<Class<?>> scanRestControllers() {
        Reflections reflections = new Reflections("py.com.sepsa.genisys.rest.controllers");
        return reflections.getSubTypesOf(ApplicationRestController.class).stream()
                .map(c -> (Class<?>) c)
                .collect(Collectors.toList());
    }

    public Collection<Class<?>> scanRequestFilterProviders() {
        Collection<Class<?>> klasses = new HashSet<>();
        klasses.add(AccessControlRequestFilter.class);
        return klasses;
    }

    public Collection<Class<?>> scanResponseFilterProviders() {
        Collection<Class<?>> klasses = new HashSet<>();
        klasses.add(AccessControlResponseFilter.class);
        return klasses;
    }

    public Collection<Class<?>> scanBodyParsers() {
        Collection<Class<?>> klasses = new HashSet<>();
        klasses.add(GsonMessageBodyWriter.class);
        return klasses;
    }

}
