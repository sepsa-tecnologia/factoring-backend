/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.pojos;

import fa.gs.utils.misc.json.serialization.JsonProperty;
import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class JPaginacion implements Serializable {

    @JsonProperty
    private Long page;

    @JsonProperty
    private Long pageSize;

    public JPaginacion() {
        this.page = 1L;
        this.pageSize = 20L;
    }

}
