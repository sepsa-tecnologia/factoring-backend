/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.proveedores;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.rest.ApplicationRestController;

/**
 * UTILIZADO POR BANCO RIO Y NOBLEZA S.A.
 *
 * @author Fabio A. González Sosa
 */
@Path("/")
public class ProveedoresController extends ApplicationRestController {

    @POST
    @Path("/v1/proveedor.consultar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response proveedorConsultar(final String body) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.COMPRADOR, PerfilEnum.ENTIDAD_FINANCIERA);
            return executeControllerAction(ConsultarAction.class, body);
        });
    }

    @POST
    @Path("/v1/proveedor.registrar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response proveedorRegistrar(final String body) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.COMPRADOR, PerfilEnum.ENTIDAD_FINANCIERA);
            return executeControllerAction(RegistrarAction.class, body);
        });
    }

}
