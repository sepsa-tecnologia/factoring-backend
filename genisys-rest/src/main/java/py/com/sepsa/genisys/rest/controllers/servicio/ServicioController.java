/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.servicio;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.rest.ApplicationRestController;

/**
 *
 * @author Fabio A. González Sosa
 */
@Path("/")
public class ServicioController extends ApplicationRestController {

    @GET
    @Path("/v1/servicio.ping")
    @Produces(MediaType.APPLICATION_JSON)
    public Response ping() {
        return wrapInTx(() -> {
            return executeControllerAction(PingAction.class);
        });
    }

    @POST
    @Path("/v1/servicio.tokenInfo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response infoToken(final String body) {
        return wrapInTx(() -> {
            return executeControllerAction(TokenInfoAction.class, body);
        });
    }

    @GET
    @Path("/v1/servicio.listarJobs")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarJobs() {
        return wrapInTx(() -> {
            return executeControllerAction(ListarJobsAction.class);
        });
    }

    @GET
    @Path("/v1/servicio.notificarCambiosEstadoFacturas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response notificarCambiosEstadoFacturas() {
        return wrapInTx(() -> {
            return executeControllerAction(NotificarCambiosEstadoFacturasAction.class);
        });
    }

}
