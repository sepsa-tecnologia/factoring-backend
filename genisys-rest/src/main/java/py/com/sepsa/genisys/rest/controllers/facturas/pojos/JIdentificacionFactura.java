/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas.pojos;

import com.google.gson.JsonObject;
import fa.gs.misc.json.Json;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonResolver;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import py.com.sepsa.genisys.ejb.utils.Ids;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class JIdentificacionFactura implements Serializable {

    JIdentificacionFactura.Forma forma;
    Integer idFactura;
    String numero;
    String rucProveedor;
    Date fechaEmision;

    public static enum Forma {
        POR_ID,
        POR_DATOS_FACTURA
    }

    public static JIdentificacionFactura parse(JsonObject json0) {
        JIdentificacionFactura input = new JIdentificacionFactura();

        if (Json.hasPath(json0, "id_factura")) {
            // Id de factura.
            String idFactura0 = JsonResolver.string(json0, "id_factura");
            if (Assertions.stringNullOrEmpty(idFactura0)) {
                throw Errors.illegalArgument("Identificador de factura no puede ser vacío o nulo.");
            }
            Integer idFactura = Ids.unmask(idFactura0);
            if (Assertions.isNull(idFactura) || idFactura <= 0) {
                throw Errors.illegalArgument("Identificador de factura inválido.");
            }

            // Datos de forma 1.
            input.setForma(Forma.POR_ID);
            input.setIdFactura(idFactura);
        } else {
            // Numero de factura.
            String numero = JsonResolver.string(json0, "numero");
            if (Assertions.stringNullOrEmpty(numero)) {
                throw Errors.illegalArgument("Número de factura no puede ser vacío o nulo.");
            }
            if (!Assertions.isNumeroFactura(numero)) {
                throw Errors.illegalArgument("Número de factura no válido (xxx-xxx-xxxxxxx).");
            }

            // RUC Proveedor.
            String rucProveedor = JsonResolver.string(json0, "proveedor");
            if (Assertions.stringNullOrEmpty(rucProveedor)) {
                throw Errors.illegalArgument("Número de RUC de proveedor no puede ser vacío o nulo.");
            }
            if (!Assertions.isRuc(rucProveedor)) {
                throw Errors.illegalArgument("Número de RUC de proveedor no válido (xxxxxxxxx-x).");
            }

            // Fecha de emision.
            Date fechaEmision = JsonResolver.date(json0, "fecha_emision");
            if (Assertions.isNull(fechaEmision)) {
                throw Errors.illegalArgument("Fecha de emisión de factura no puede ser vacío o nulo.");
            }

            // Datos de forma 1.
            input.setForma(Forma.POR_DATOS_FACTURA);
            input.setNumero(numero);
            input.setRucProveedor(rucProveedor);
            input.setFechaEmision(fechaEmision);
        }

        return input;
    }
}
