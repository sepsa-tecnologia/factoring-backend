/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import fa.gs.utils.rest.controllers.RestControllerActionWithCustomParam;
import java.io.Serializable;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioSeguimientoLiquidacion;
import py.com.sepsa.genisys.ejb.enums.ConfiguracionGeneralEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_ActualizarLiquidacionPendiente;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ActualizarLiquidacionAction extends RestControllerActionWithCustomParam<ActualizarLiquidacionAction.Input> {

    private AppLogger log;

    public ActualizarLiquidacionAction() {
        this.log = AppLoggerFactory.ingest("banco-rio");
    }

    @Override
    public Response doAction(ActualizarLiquidacionAction.Input params) throws Throwable {
        // Inyectar beans.
        BancoRio bancoRio = Injection.lookup(BancoRio.class);
        Notificaciones notificaciones = Injection.lookup(Notificaciones.class);
        PF_BancoRio_ActualizarLiquidacionPendiente actualizarLiquidacionPendiente = Injection.lookup(PF_BancoRio_ActualizarLiquidacionPendiente.class);

        // Obtener usuario ingestion.
        Integer idUsuarioIngestion = bancoRio.obtenerIdUsuarioIngestion();
        if (idUsuarioIngestion == null) {
            notificaciones.notificarSepsa("No existe un valor configurado para el parámetro '%s'", ConfiguracionGeneralEnum.INTEGRACION_BANCO_RIO_INGESTION_ID_USUARIO);
            return ControllersUtils.responseKo("No se pudo procesar la solicitud de liquidacion");
        }

        // Obtener liquidacion pendiente.
        Result<BancoRioSeguimientoLiquidacion> resSolicitud = bancoRio.obtenerSolicitudLiquidacion(params.idSeguimientoLiquidacion);
        if (resSolicitud.isFailure()) {
            log.error()
                    .cause(resSolicitud.failure().cause())
                    .message(resSolicitud.failure().message())
                    .log();
            return ControllersUtils.responseKo("No se pudo procesar la solicitud de liquidación");
        }

        try {
            BancoRioSeguimientoLiquidacion solicitud = resSolicitud.value();
            actualizarLiquidacionPendiente.work(idUsuarioIngestion, solicitud);
            return ControllersUtils.responseOk();
        } catch (Throwable thr) {
            notificaciones.notificarSepsa(thr, "Ocurrió un error verificando solicitud de liquidacion id=%s", params.idSeguimientoLiquidacion);
            return ControllersUtils.responseKo("No se pudo procesar la solicitud de liquidacion");
        }
    }

    @Data
    public static class Input implements Serializable {

        @JsonProperty
        private Integer idSeguimientoLiquidacion;

    }

}
