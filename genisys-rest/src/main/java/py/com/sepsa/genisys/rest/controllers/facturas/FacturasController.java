/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas;

import fa.gs.utils.misc.json.serialization.JsonDeserializer;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.rest.ApplicationRestController;
import static py.com.sepsa.genisys.rest.ApplicationRestController.checkAuthentication;

/**
 *
 * @author Fabio A. González Sosa
 */
@Path("/")
public class FacturasController extends ApplicationRestController {

    @POST
    @Path("/v1/factura.registrar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Deprecated
    public Response facturaRegistrar(final String body) {
        return wrapInTx(() -> {
            checkAuthentication();
            return executeControllerAction(RegistrarComoCompradorAction.class, body);
        });
    }

    @POST
    @Path("/v1/factura.registrarComoComprador")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response facturaRegistrarComoComprador(final String body) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.COMPRADOR);
            return executeControllerAction(RegistrarComoCompradorAction.class, body);
        });
    }

    @POST
    @Path("/v1/factura.registrarComoProveedor")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response facturaRegistrarComoProveedor(final String body) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.PROVEEDOR);
            return executeControllerAction(RegistrarComoProveedorAction.class, body);
        });
    }

    @POST
    @Path("/v1/factura.eliminar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response facturaEliminar(final String body) {
        return wrapInTx(() -> {
            checkAuthentication();
            return executeControllerAction(EliminarAction.class, body);
        });
    }

    @POST
    @Path("/v1/factura.consultarEstado")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response facturaConsultarEstado(final String body) {
        return wrapInTx(() -> {
            checkAuthentication();
            return executeControllerAction(ConsultarEstadoAction.class, body);
        });
    }

    @POST
    @Path("/v1/factura.aprobarConformacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response facturaAprobarConformacion(final String body) {
        return wrapInTx(() -> {
            checkAuthentication();
            return executeControllerAction(ConformarAction.class, body);
        });
    }

    @POST
    @Path("/v1/factura.busquedaAvanzada")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response facturaBusquedaAvanzada(final String body) {
        return wrapInTx(() -> {
            checkAuthentication();
            FacturasBusquedaAvanzadaAction.Input input = JsonDeserializer.deserialize(body, FacturasBusquedaAvanzadaAction.Input.class);
            return executeControllerAction(FacturasBusquedaAvanzadaAction.class, input);
        });
    }

    @POST
    @Path("/v1/factura.calcularDesembolso")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response facturaCalcularDesembolso(final String body) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.PROVEEDOR);
            return executeControllerAction(CalcularDesembolsoAction.class, body);
        });
    }

    @POST
    @Path("/v1/factura.solicitarDesembolso")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response facturaSolicitarDesembolso(final String body) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.PROVEEDOR);
            return executeControllerAction(SolicitarDesembolsoAction.class, body);
        });
    }
    
    @POST
    @Path("/v1/factura.rechazarComoComprador")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response facturaRechazarComoComprador(final String body) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.COMPRADOR);
            return executeControllerAction(RechazarComoCompradorAction.class, body);
        });
    }

}
