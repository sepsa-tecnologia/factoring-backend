/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.genisys.rest.controllers.usuario;

import com.google.gson.JsonObject;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;

/**
 *
 * @author Fabio A. González Sosa
 */
public class AltaCompradorAction extends RestControllerActionWithJsonObjectParam {

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        AltaPersona.Input input = AltaPersona.processInput(json0, PerfilEnum.COMPRADOR);
        AltaPersona.Output output = AltaPersona.alta(input);
        return ServiceResponse.ok()
                .payload(AltaPersona.processOutput(output))
                .build();
    }

}
