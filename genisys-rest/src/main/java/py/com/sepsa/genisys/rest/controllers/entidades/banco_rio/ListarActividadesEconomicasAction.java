/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import com.google.gson.JsonElement;
import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.json.JsonArrayBuilder;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.rest.controllers.RestControllerActionWithNoParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.util.Collection;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioActividadEconomica;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ListarActividadesEconomicasAction extends RestControllerActionWithNoParam {

    private final BancoRio bancoRio;

    public ListarActividadesEconomicasAction() {
        this.bancoRio = Injection.lookup(BancoRio.class);
    }

    @Override
    public Response doAction() throws Throwable {
        // Obtener bancos.
        Result<Collection<BancoRioActividadEconomica>> result = bancoRio.obtenerActividadesEconomicas();
        if (result.isFailure()) {
            return ServiceResponse.ko()
                    .cause(result.failure().cause())
                    .build();
        }

        Collection<BancoRioActividadEconomica> actividadesEconomicas = result.value(Lists.empty());
        JsonElement json = adaptarDatosSalida(actividadesEconomicas);
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    private JsonElement adaptarDatosSalida(Collection<BancoRioActividadEconomica> actividadesEconomicas) {
        JsonArrayBuilder ab = JsonArrayBuilder.instance();
        if (!Assertions.isNullOrEmpty(actividadesEconomicas)) {
            for (BancoRioActividadEconomica actividadEconomica : actividadesEconomicas) {
                JsonObjectBuilder ob = JsonObjectBuilder.instance();
                ob.add("id", actividadEconomica.getId());
                ob.add("descripcion", actividadEconomica.getDescripcion());
                ob.add("codigo", actividadEconomica.getCodigo());
                ob.add("tipo_persona", actividadEconomica.getTipoPersona());
                ab.add(ob.build());
            }
        }
        return ab.build();
    }

}
