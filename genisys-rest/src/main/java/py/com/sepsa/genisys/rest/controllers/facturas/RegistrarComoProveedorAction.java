/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas;

import com.google.gson.JsonObject;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.Todo;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.facades.info.MonedaFacade;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.rest.utils.Context;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RegistrarComoProveedorAction extends RestControllerActionWithJsonObjectParam {

    private Personas personas;

    private MonedaFacade monedas;

    public RegistrarComoProveedorAction() {
        this.personas = Injection.lookup(Personas.class);
        this.monedas = Injection.lookup(MonedaFacade.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        UserAuthenticationInfo auth = Context.getUserInfo();
        Input input = processInput(json0);

        // Obtener proveedor.
        Result<Persona> resProveedor = personas.obtenerPorId(auth.getMUsuario().getIdPersona());
        if (resProveedor.isFailure()) {
            throw Errors.illegalState(resProveedor.failure().cause(), "No se pudo determinar el proveedor.");
        }

        // Validar proveedor.
        Persona proveedor = resProveedor.value();
        if (proveedor == null || proveedor.getId() == null) {
            throw Errors.illegalState("No se pudo determinar el proveedor.");
        }

        // Obtener comprador.
        Result<Persona> resComprador = personas.obtenerPorRuc(input.rucComprador);
        if (resComprador.isFailure()) {
            throw Errors.illegalState(resComprador.failure().cause(), "No se pudo determinar el comprador.");
        }

        // Validar comprador.
        // TODO: VERIFICAR SI COMPRADOR TIENE ROL COMPRADOR.
        Persona comprador = resComprador.value();
        if (comprador == null || comprador.getId() == null) {
            throw Errors.illegalState("No se pudo determinar el comprador.");
        }

        // Registrar factura.
        FacturaInfo factura = registrarFactura(auth.getMUsuario().getId(), proveedor, comprador, input.numero, input.fechaEmision, input.fechaPago, input.monto, input.moneda);
        return processOutput(factura);
    }

    static FacturaInfo registrarFactura(Integer idUsuario, Persona proveedor, Persona comprador, String numero, Date fechaEmision, Date fechaPago, BigDecimal monto, Moneda moneda) throws Throwable {
        final Facturas facturas = Injection.lookup(Facturas.class);

        // Verificar si factura ya existe.
        boolean ok = facturas.facturaExiste(numero, Text.ruc(proveedor), Text.ruc(comprador), fechaEmision, fechaPago, monto, moneda.getId());
        if (ok) {
            throw Errors.illegalState("Factura ya está registrada.");
        }

        // Registrar factura.
        Result<Factura> resFactura0 = facturas.registrarFactura(idUsuario, comprador, proveedor, numero, fechaEmision, fechaPago, monto, moneda);
        if (resFactura0.isFailure()) {
            throw Errors.illegalState(resFactura0.failure().cause(), "Ocurrió un error registrando factura.");
        }

        // Factura registrada.
        Factura factura0 = resFactura0.value();

        // Agregar historico.
        Result<HistoricoFactura> resHistoricoFactura = facturas.agregarHistorico(idUsuario, factura0, Estados.EstadoFacturaEnum.NO_CONFORMADA, "Factura registrada desde API REST.");
        if (resHistoricoFactura.isFailure()) {
            throw Errors.illegalState(resHistoricoFactura.failure().cause(), "Ocurrió un error registrando histórico de factura.");
        }

        // Si no existe relacion comercial entre comprador y proveedor, registrarPersonaJuridica.
        // TODO: IMPLEMENTAR.
        Todo.TODO();

        // Obtener datos finales.
        Result<FacturaInfo> resFactura = facturas.buscarFactura(factura0.getId());
        resFactura.raise();
        return resFactura.value();
    }

    private Input processInput(JsonObject json0) {
        // Numero de factura.
        String numero = JsonResolver.string(json0, "numero");
        if (Assertions.stringNullOrEmpty(numero)) {
            throw Errors.illegalArgument("Número de factura no puede ser vacío o nulo.");
        }
        if (!Assertions.isNumeroFactura(numero)) {
            throw Errors.illegalArgument("Número de factura no válido (xxx-xxx-xxxxxxx).");
        }

        // RUC Comprador.
        String rucComprador = JsonResolver.string(json0, "comprador");
        if (Assertions.stringNullOrEmpty(rucComprador)) {
            throw Errors.illegalArgument("Número de RUC de comprador no puede ser vacío o nulo.");
        }
        if (!Assertions.isRuc(rucComprador)) {
            throw Errors.illegalArgument("Número de RUC de comprador no válido (xxxxxxxxx-x).");
        }

        // Fecha de emision.
        Date fechaEmision = JsonResolver.date(json0, "fecha_emision");
        if (Assertions.isNull(fechaEmision)) {
            throw Errors.illegalArgument("Fecha de emisión de factura no puede ser vacío o nulo.");
        }

        // Fecha de pago.
        Date fechaPago = JsonResolver.date(json0, "fecha_pago");
        if (Assertions.isNull(fechaPago)) {
            throw Errors.illegalArgument("Fecha de pago de factura no puede ser vacío o nulo.");
        }

        // Monto de factura.
        BigDecimal monto = JsonResolver.bigdecimal(json0, "monto");
        if (Assertions.isNull(monto)) {
            throw Errors.illegalArgument("Monto de factura no puede ser vacío o nulo.");
        }
        if (Numeric.menorIgual(monto, Numeric.CERO)) {
            throw Errors.illegalArgument("Monto de factura no puede ser menor o igual a cero (0).");
        }

        // Moneda.
        Moneda moneda = monedas.getGuarani();
        if (Assertions.isNull(moneda)) {
            throw Errors.illegalArgument("No se pudo determinar la moneda (Guaraní).");
        }

        Input input = new Input();
        input.setNumero(numero);
        input.setRucComprador(rucComprador);
        input.setFechaEmision(fechaEmision);
        input.setFechaPago(fechaPago);
        input.setMonto(monto);
        input.setMoneda(moneda);
        return input;
    }

    private Response processOutput(FacturaInfo factura) {
        // Preparar datos de salida.
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add("id_factura", Ids.mask(factura.getId()));
        builder.add("fecha_emision", factura.getFechaEmision());
        builder.add("fecha_pago", factura.getFechaPago());
        builder.add("numero", factura.getNumero());
        builder.add("monto", factura.getMontoTotal());
        builder.add("proveedor", factura.getProveedorRuc());

        JsonObject json = builder.build();
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    @Data
    private static class Input implements Serializable {

        String numero;
        String rucComprador;
        Date fechaEmision;
        Date fechaPago;
        BigDecimal monto;
        Moneda moneda;
    }
}
