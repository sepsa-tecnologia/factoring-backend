/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.genisys.rest.controllers.usuario;

import com.google.gson.JsonObject;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.logic.impl.Usuarios;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.ApplicationRestController;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RelacionarCompradorAction extends RestControllerActionWithJsonObjectParam {

    private final Usuarios usuarios;

    public RelacionarCompradorAction() {
        this.usuarios = Injection.lookup(Usuarios.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Proveedor.
        AuthenticationInfo auth = ApplicationRestController.getAuthentication();
        Integer idUsuario = (Integer) auth.getUsuario().id();
        Usuario usuario = usuarios.obtenerPorId(idUsuario).value();
        Integer idPersonaProveedor = usuario.getIdPersona();

        // Ruc de comprador.
        String rucProveedor = JsonResolver.string(json0, "comprador");
        if (Assertions.stringNullOrEmpty(rucProveedor)) {
            throw Errors.illegalState("RUC de comprador no puede ser nulo.");
        }
        if (!Assertions.isRuc(rucProveedor)) {
            throw Errors.illegalState("RUC de comprador no es válido.");
        }

        // Crear relacion.
        RelacionarPersona.relacionarComprador(idPersonaProveedor, rucProveedor);
        return ServiceResponse.ok().build();
    }

}
