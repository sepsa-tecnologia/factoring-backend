/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.utils;

import fa.gs.utils.authentication.tokens.TokenExtractor;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.result.simple.Result;
import javax.servlet.http.HttpServletRequest;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.ejb.security.UserTokenAuthenticator;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Context {

    public static HttpServletRequest getRequest() {
        HttpServletRequest request = ResteasyProviderFactory.getContextData(HttpServletRequest.class);
        return request;
    }

    public static UserAuthenticationInfo getUserInfo() throws Throwable {
        TokenExtractor.COOKIE_NAME = "APP_SESSION_TOKEN_GENISYS";
        String token = TokenExtractor.fromHttpRequest(getRequest());
        UserTokenAuthenticator auth = new UserTokenAuthenticator();
        Result<AuthenticationInfo> result = auth.authenticateUserToken(token);
        if (result.isFailure()) {
            throw result.failure().cause();
        } else {
            return (UserAuthenticationInfo) result.value();
        }
    }

}
