/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.admin;

import fa.gs.utils.misc.json.serialization.JsonDeserializer;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.rest.ApplicationRestController;

/**
 *
 * @author Fabio A. González Sosa
 */
@Path("/")
public class AdminController extends ApplicationRestController {

    @POST
    @Path("/v1/admin/personas.busquedaAvanzada")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response personasBusquedaAvanzada(String json) {
        return wrapInTx(() -> {
            PersonasBusquedaAvanzadaAction.Input input = JsonDeserializer.deserialize(json, PersonasBusquedaAvanzadaAction.Input.class);
            return executeControllerAction(PersonasBusquedaAvanzadaAction.class, input);
        });
    }

    @POST
    @Path("/v1/admin/personas.registrarPersonaFisica")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response personasRegistrarPersonaFisica(String json) {
        return wrapInTx(() -> {
            PersonaFisicaRegistrarAction.Input input = JsonDeserializer.deserialize(json, PersonaFisicaRegistrarAction.Input.class);
            return executeControllerAction(PersonaFisicaRegistrarAction.class, input);
        });
    }

    @POST
    @Path("/v1/admin/personas.registrarPersonaJuridica")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response personasRegistrarPersonaJuridica(String json) {
        return wrapInTx(() -> {
            PersonaJuridicaRegistrarAction.Input input = JsonDeserializer.deserialize(json, PersonaJuridicaRegistrarAction.Input.class);
            return executeControllerAction(PersonaJuridicaRegistrarAction.class, input);
        });
    }

    @POST
    @Path("/v1/admin/personas.editarPersonaFisica")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response personasEditarPersonaFisica(String json) {
        return wrapInTx(() -> {
            PersonaFisicaEditarAction.Input input = JsonDeserializer.deserialize(json, PersonaFisicaEditarAction.Input.class);
            return executeControllerAction(PersonaFisicaEditarAction.class, input);
        });
    }

    @POST
    @Path("/v1/admin/personas.editarPersonaJuridica")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response personasEditarPersonaJuridica(String json) {
        return wrapInTx(() -> {
            PersonaJuridicaEditarAction.Input input = JsonDeserializer.deserialize(json, PersonaJuridicaEditarAction.Input.class);
            return executeControllerAction(PersonaJuridicaEditarAction.class, input);
        });
    }

}
