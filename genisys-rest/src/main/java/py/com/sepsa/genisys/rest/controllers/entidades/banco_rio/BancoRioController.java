/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import fa.gs.utils.misc.json.serialization.JsonDeserializer;
import fa.gs.utils.rest.responses.ServiceResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_ActualizarDesembolsosPendientes;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_ActualizarLiquidacionesPendientes;
import py.com.sepsa.genisys.rest.ApplicationRestController;
import static py.com.sepsa.genisys.rest.ApplicationRestController.checkAuthentication;

/**
 *
 * @author Fabio A. González Sosa
 */
@Path("/")
public class BancoRioController extends ApplicationRestController {

    @GET
    @Path("/v1/entidades/banco_rio/actividadesEconomicas.listar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarActividadesEconomicas() {
        return wrapInTx(() -> {
            return executeControllerAction(ListarActividadesEconomicasAction.class);
        });
    }

    @GET
    @Path("/v1/entidades/banco_rio/bancos.listar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarBancos() {
        return wrapInTx(() -> {
            return executeControllerAction(ListarBancosAction.class);
        });
    }

    @GET
    @Path("/v1/entidades/banco_rio/ciudades.listar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarCiudades() {
        return wrapInTx(() -> {
            return executeControllerAction(ListarCiudadesAction.class);
        });
    }

    @GET
    @Path("/v1/entidades/banco_rio/regiones.listar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarRegiones() {
        return wrapInTx(() -> {
            return executeControllerAction(ListarRegionesAction.class);
        });
    }

    @GET
    @Path("/v1/entidades/banco_rio/tiposSociedad.listar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTiposSociedad() {
        return wrapInTx(() -> {
            return executeControllerAction(ListarTiposSociedadAction.class);
        });
    }

    @GET
    @Path("/v1/entidades/banco_rio/tiposDocumento.listar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTiposDocumentod() {
        return wrapInTx(() -> {
            return executeControllerAction(ListarTiposDocumentodadAction.class);
        });
    }

    @POST
    @Path("/v1/entidades/banco_rio/comprador.adherirNuevos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response compradorAdherirNuevos() {
        return executeControllerAction(AdherirCompradoresAction.class);
    }

    @POST
    @Path("/v1/entidades/banco_rio/comprador.registrarPersonaJuridica")
    @Produces(MediaType.APPLICATION_JSON)
    public Response compradorRegistrarPersonaJuridica(String json) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.COMPRADOR, PerfilEnum.ENTIDAD_FINANCIERA);
            RegistrarCompradorPJAction.Input input = JsonDeserializer.deserialize(json, RegistrarCompradorPJAction.Input.class);
            return executeControllerAction(RegistrarCompradorPJAction.class, input);
        });
    }

    @POST
    @Path("/v1/entidades/banco_rio/comprador.consultarRegistro")
    @Produces(MediaType.APPLICATION_JSON)
    public Response compradorConsultarRegistro(String json) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.COMPRADOR, PerfilEnum.ENTIDAD_FINANCIERA);
            ConsultarRegistroCompradorAction.Input input = JsonDeserializer.deserialize(json, ConsultarRegistroCompradorAction.Input.class);
            return executeControllerAction(ConsultarRegistroCompradorAction.class, input);
        });
    }

    @POST
    @Path("/v1/entidades/banco_rio/proveedor.registrarPersonaJuridica")
    @Produces(MediaType.APPLICATION_JSON)
    public Response proveedorRegistrarPersonaJuridica(String json) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.COMPRADOR, PerfilEnum.ENTIDAD_FINANCIERA);
            RegistrarProveedorPJAction.Input input = JsonDeserializer.deserialize(json, RegistrarProveedorPJAction.Input.class);
            return executeControllerAction(RegistrarProveedorPJAction.class, input);
        });
    }

    @POST
    @Path("/v1/entidades/banco_rio/proveedor.registrarPersonaFisica")
    @Produces(MediaType.APPLICATION_JSON)
    public Response proveedorRegistrarPersonaFisica(String json) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.COMPRADOR, PerfilEnum.PROVEEDOR, PerfilEnum.ENTIDAD_FINANCIERA);
            RegistrarProveedorPFAction.Input input = JsonDeserializer.deserialize(json, RegistrarProveedorPFAction.Input.class);
            return executeControllerAction(RegistrarProveedorPFAction.class, input);
        });
    }

    @POST
    @Path("/v1/entidades/banco_rio/proveedor.consultarRegistro")
    @Produces(MediaType.APPLICATION_JSON)
    public Response proveedorConsultarRegistro(String json) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.COMPRADOR, PerfilEnum.PROVEEDOR, PerfilEnum.ENTIDAD_FINANCIERA);
            ConsultarRegistroProveedorAction.Input input = JsonDeserializer.deserialize(json, ConsultarRegistroProveedorAction.Input.class);
            return executeControllerAction(ConsultarRegistroProveedorAction.class, input);
        });
    }

    @GET
    @Path("/v1/entidades/banco_rio/desembolsos.actualizarPendientes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarDesembolsos() {
        return wrapInTx(() -> {
            PF_BancoRio_ActualizarDesembolsosPendientes worker = Injection.lookup(PF_BancoRio_ActualizarDesembolsosPendientes.class);
            worker.work();
            return ServiceResponse.ok().build();
        });
    }

    @POST
    @Path("/v1/entidades/banco_rio/desembolsos.actualizarPorId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarDesembolsoPorId(String json) {
        return wrapInTx(() -> {
            ActualizarDesembolsoAction.Input input = JsonDeserializer.deserialize(json, ActualizarDesembolsoAction.Input.class);
            return executeControllerAction(ActualizarDesembolsoAction.class, input);
        });
    }

    @GET
    @Path("/v1/entidades/banco_rio/cobros.agregarNuevos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response agregarCobros() {
        return wrap(() -> {
            return executeControllerAction(AgregarCobrosAction.class);
        });
    }

    @POST
    @Path("/v1/entidades/banco_rio/cobros.agregarNuevoPorId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response agregarCobroPorId(String json) {
        return wrapInTx(() -> {
            AgregarCobroPorIdAction.Input input = JsonDeserializer.deserialize(json, AgregarCobroPorIdAction.Input.class);
            return executeControllerAction(AgregarCobroPorIdAction.class, input);
        });
    }

    @GET
    @Path("/v1/entidades/banco_rio/liquidaciones.agregarNuevos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response agregarLiquidaciones() {
        return wrapInTx(() -> {
            return executeControllerAction(LiquidacionAgregarNuevosAction.class);
        });
    }

    @POST
    @Path("/v1/entidades/banco_rio/liquidaciones.agregarNuevoPorId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response agregarLiquidacionPorId(String json) {
        return wrapInTx(() -> {
            LiquidacionAgregarNuevoPorIdAction.Input input = JsonDeserializer.deserialize(json, LiquidacionAgregarNuevoPorIdAction.Input.class);
            return executeControllerAction(LiquidacionAgregarNuevoPorIdAction.class, input);
        });
    }

    @GET
    @Path("/v1/entidades/banco_rio/liquidaciones.actualizarPendientes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarLiquidaciones() {
        return wrapInTx(() -> {
            PF_BancoRio_ActualizarLiquidacionesPendientes worker = Injection.lookup(PF_BancoRio_ActualizarLiquidacionesPendientes.class);
            worker.work();
            return ServiceResponse.ok().build();
        });
    }

    @POST
    @Path("/v1/entidades/banco_rio/liquidaciones.actualizarPorId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarLiquidacionPorId(String json) {
        return wrapInTx(() -> {
            ActualizarLiquidacionAction.Input input = JsonDeserializer.deserialize(json, ActualizarLiquidacionAction.Input.class);
            return executeControllerAction(ActualizarLiquidacionAction.class, input);
        });
    }

    @POST
    @Path("/v1/entidades/banco_rio/liquidaciones.consularFacturas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consularFacurasLiquidables(String json) {
        return wrapInTx(() -> {
            ConsultarFacturasLiquidablesAction.Input input = JsonDeserializer.deserialize(json, ConsultarFacturasLiquidablesAction.Input.class);
            return executeControllerAction(ConsultarFacturasLiquidablesAction.class, input);
        });
    }

}
