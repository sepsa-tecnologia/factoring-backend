/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.usuario;

import com.google.gson.JsonObject;
import fa.gs.misc.json.JsonObjectBuilder;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.Units;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.io.Serializable;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.logic.impl.Sesiones;
import py.com.sepsa.genisys.ejb.security.UserTokenAuthenticator;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class LoginAction extends RestControllerActionWithJsonObjectParam {

    private Sesiones sesiones;

    public LoginAction() {
        this.sesiones = Injection.lookup(Sesiones.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        Input input = processInput(json0);

        // Obtener usuario.
        Usuario usuario = obtenerUsuario(input);
        if (usuario == null) {
            throw Errors.illegalArgument("Credenciales inválidas.");
        }

        // Generar token de sesion.
        Integer idUsuario = Units.execute(() -> usuario.getId());
        String token = UserTokenAuthenticator.issueToken(idUsuario);

        // Preparar datos de salida.
        JsonObject json = processOutput(token);

        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    private Usuario obtenerUsuario(Input input) throws Throwable {
        Result<Usuario> result = sesiones.obtenerPorFactoringUsuario(input.authId, input.authSecret);
        return result.value(null);
    }

    private Input processInput(JsonObject json) throws Throwable {
        // Id de usuario.
        String authId = JsonResolver.string(json, "usuario.id");
        if (Assertions.stringNullOrEmpty(authId)) {
            throw Errors.illegalArgument("Se esperaba un id.");
        }

        // Secret de usuario.
        String authSecret = JsonResolver.string(json, "usuario.secret");
        if (Assertions.stringNullOrEmpty(authSecret)) {
            throw Errors.illegalArgument("Se esperaba un secret.");
        }

        Input input = new Input();
        input.authId = authId;
        input.authSecret = authSecret;
        return input;
    }

    private JsonObject processOutput(String token) {
        JsonObjectBuilder builder0 = JsonObjectBuilder.instance();
        builder0.add("id", "");
        builder0.add("token", token);

        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add("sesion", builder0.build());

        return builder.build();
    }

    @Data
    private static class Input implements Serializable {

        Integer idEntidad;
        String authId;
        String authSecret;

    }

}
