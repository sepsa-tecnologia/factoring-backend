/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.proveedores;

import com.google.gson.JsonObject;
import fa.gs.result.simple.Result;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.io.Serializable;
import javax.validation.ValidationException;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.PersonaRol;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.impl.Sesiones;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.rest.ApplicationRestController;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RegistrarAction extends RestControllerActionWithJsonObjectParam {

    private final Sesiones sesiones;

    private final Personas personas;

    public RegistrarAction() {
        this.personas = Injection.lookup(Personas.class);
        this.sesiones = Injection.lookup(Sesiones.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        Input input = processInput(json0);

        // Obtener persona.
        Persona persona = obtenerPersona(input);

        // Agregar relacion comercial con comprador.
        agregarRelacionComercial(persona);

        // Preparar datos de salida.
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add("id", Ids.mask(persona.getId()));
        builder.add("razon_social", Text.razonSocial(persona));
        builder.add("ruc", Text.ruc(persona));
        builder.add("tipo", Text.tipoPersona(persona));

        JsonObject json = builder.build();
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    private Persona obtenerPersona(Input input) throws Throwable {
        // Obtener persona.
        Result<Persona> resPersona = personas.obtenerPorRuc(input.ruc);
        Persona persona = resPersona.value(null);
        if (persona == null) {
            // Registrar nueva persona.
            if (input.tipoPersona == Personas.TipoPersonaEnum.FISICA) {
                resPersona = personas.registrarPersonaFisica(input.nombres, input.apellidos, input.ci, input.ruc);
                persona = resPersona.value(null);
            } else if (input.tipoPersona == Personas.TipoPersonaEnum.JURIDICA) {
                resPersona = personas.registrarPersonaJuridica(input.razonSocial, input.nombreFantasia, input.ruc);
                persona = resPersona.value(null);
            } else {
                throw Errors.illegalState();
            }
        }

        // Agregar rol de proveedor.
        Result<PersonaRol> resRol = personas.agregarRol(persona.getId(), PerfilEnum.PROVEEDOR);
        resRol.raise();

        // Control.
        if (persona == null) {
            throw new ValidationException("No se pudo registrar al proveedor.");
        }

        return persona;
    }

    private void agregarRelacionComercial(Persona persona) throws Throwable {
        // Obtener info de comprador.
        AuthenticationInfo auth = ApplicationRestController.getAuthentication();
        Integer idUsuario = (Integer) auth.getUsuario().id();
        Result<Usuario> resUsuario = sesiones.obtenerPorTransId(idUsuario);
        resUsuario.raise();
        Usuario usuario = resUsuario.value();

        // Agregar relacion comercial.
        Integer idProveedor = persona.getId();
        Integer idComprador = usuario.getIdPersona();
        Result<?> result = personas.agregarRelacion(idProveedor, idComprador);
        result.raise();
    }

    private Input processInput(JsonObject json) throws Throwable {
        // Tipo de Persona.
        String tipoPersona0 = JsonResolver.string(json, "proveedor.tipo", "j");
        if (Assertions.stringNullOrEmpty(tipoPersona0)) {
            tipoPersona0 = "j";
        }

        Personas.TipoPersonaEnum tipoPersona;
        if (tipoPersona0.toLowerCase().equals("j")) {
            tipoPersona = Personas.TipoPersonaEnum.JURIDICA;
        } else {
            tipoPersona = Personas.TipoPersonaEnum.FISICA;
        }

        // RUC.
        String ruc = JsonResolver.string(json, "proveedor.ruc");
        if (Assertions.stringNullOrEmpty(ruc)) {
            throw Errors.illegalArgument("RUC no puede ser vacío o nulo.");
        }
        if (!Assertions.isRuc(ruc)) {
            throw Errors.illegalArgument("RUC con formato inválido.");
        }

        // Datos de Persona: JURIDICA.
        if (tipoPersona == Personas.TipoPersonaEnum.JURIDICA) {
            // Razon Social.
            String razonSocial = JsonResolver.string(json, "proveedor.razon_social");
            if (Assertions.stringNullOrEmpty(razonSocial)) {
                throw Errors.illegalArgument("Razón social no puede ser vacía o nula.");
            }

            // Nombre de fantasia.
            String nombreFantasia = JsonResolver.string(json, "proveedor.nombre_fantasia", razonSocial);

            Input input = new Input();
            input.tipoPersona = tipoPersona;
            input.razonSocial = razonSocial;
            input.nombreFantasia = nombreFantasia;
            input.ruc = ruc;
            return input;
        }

        // Datos de Persona: FISICA.
        if (tipoPersona == Personas.TipoPersonaEnum.FISICA) {
            // Nombres.
            String nombres = JsonResolver.string(json, "proveedor.nombres");
            if (Assertions.stringNullOrEmpty(nombres)) {
                throw Errors.illegalArgument("Nombres no puede ser vacía o nula.");
            }

            // Apellidos.
            String apellidos = JsonResolver.string(json, "proveedor.apellidos");

            // Ci.
            String ci = JsonResolver.string(json, "proveedor.ci");
            if (Assertions.stringNullOrEmpty(ci)) {
                throw Errors.illegalArgument("Documento de identidad no puede ser nula o vacía.");
            }

            Input input = new Input();
            input.tipoPersona = tipoPersona;
            input.nombres = nombres;
            input.apellidos = apellidos;
            input.ci = ci;
            input.ruc = ruc;
            return input;
        }

        throw Errors.illegalArgument("No se pudo determinar el tipo de persona.");
    }

    @Data
    private static class Input implements Serializable {

        Personas.TipoPersonaEnum tipoPersona;
        String razonSocial;
        String nombreFantasia;
        String nombres;
        String apellidos;
        String ci;
        String ruc;

    }

}
