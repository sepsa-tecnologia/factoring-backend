/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.admin;

import fa.gs.misc.Assertions;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import fa.gs.utils.rest.controllers.RestControllerActionWithCustomParam;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
public class PersonaEliminarAction extends RestControllerActionWithCustomParam<PersonaEliminarAction.Input> {

    @Override
    public Response doAction(Input params) throws Throwable {

        // Validar datos
        String err = validarInput(params);
        if (!Assertions.isNull(err)) {
            return ControllersUtils.responseKo(err);
        }

        // Registrar datos.
        Personas personas = Injection.lookup(Personas.class);
        // NOTE: ELIMINAR ES BASICAMENTE MODIFICAR EL ESTADO DE INFO.PERSONA A 'I'.

        /*Result<Persona> resDatos = personas.registrarPersonaFisica(input.getNombres(), input.getApellidos(), input.getCi(), input.getRuc());
        if (resDatos.isFailure()) {
            return ControllersUtils.responseKo(resDatos);
        }*/
        // Preparar datos de salida
        /*Result<Persona> resNuevaPersona = personas.obtenerPorId(resDatos.value().getId());
        if (resNuevaPersona.isFailure()) {
            return ControllersUtils.responseKo(resNuevaPersona);
        }*/
        return ControllersUtils.responseOk();
    }

    private String validarInput(Input params) {
        // Validar RUC
        if (Assertions.stringNullOrEmpty(params.ruc)) {
            return "El campo 'ruc' es mandatorio";
        }

        // Validar RUC
        String ruc = params.ruc;
        Personas personas = Injection.lookup(Personas.class);
        Persona persona = personas.obtenerPorRuc(ruc).value(null);
        if (Assertions.isNull(persona)) {
            return "No se encuentra una persona con el RUC especificado";
        }

        return null;
    }

    @Data
    public static class Input {

        @JsonProperty
        private String ruc;

    }

}
