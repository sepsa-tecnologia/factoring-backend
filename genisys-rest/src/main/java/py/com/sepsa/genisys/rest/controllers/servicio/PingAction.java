/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.servicio;

import com.google.gson.JsonObject;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.rest.controllers.RestControllerActionWithNoParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import javax.ws.rs.core.Response;

/**
 *
 * @author Fabio A. González Sosa
 */
public class PingAction extends RestControllerActionWithNoParam {

    @Override
    public Response doAction() throws Throwable {
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add("ping", "pong");
        JsonObject json = builder.build();

        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

}
