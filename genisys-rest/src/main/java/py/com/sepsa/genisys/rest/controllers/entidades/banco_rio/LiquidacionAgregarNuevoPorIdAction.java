/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import fa.gs.utils.misc.json.serialization.JsonProperty;
import fa.gs.utils.rest.controllers.RestControllerActionWithCustomParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.io.Serializable;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_AgregarLiquidacion;

/**
 *
 * @author Fabio A. González Sosa
 */
public class LiquidacionAgregarNuevoPorIdAction extends RestControllerActionWithCustomParam<LiquidacionAgregarNuevoPorIdAction.Input> {

    @Override
    public Response doAction(Input param) throws Throwable {
        PF_BancoRio_AgregarLiquidacion bean = Injection.lookup(PF_BancoRio_AgregarLiquidacion.class);
        bean.work(param.idFactura);
        return ServiceResponse.ok().build();
    }

    @Data
    public static class Input implements Serializable {

        @JsonProperty
        private Integer idFactura;
    }

}
