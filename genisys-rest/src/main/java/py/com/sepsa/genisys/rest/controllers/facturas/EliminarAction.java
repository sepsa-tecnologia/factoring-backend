/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas;

import com.google.gson.JsonObject;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.io.Serializable;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.controllers.facturas.pojos.JIdentificacionFactura;
import py.com.sepsa.genisys.rest.utils.Context;

/**
 *
 * @author Fabio A. González Sosa
 */
public class EliminarAction extends RestControllerActionWithJsonObjectParam {

    private Facturas facturas;

    public EliminarAction() {
        this.facturas = Injection.lookup(Facturas.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        UserAuthenticationInfo auth = Context.getUserInfo();
        Input input = processInput(json0);

        // Obtener factura.
        FacturaInfo factura = Utils.identificarFactura(auth.getMUsuario().getIdPersona(), input.identificacionFactura);
        if (factura == null) {
            throw Errors.illegalState("No se pudo determinar la factura.");
        }

        // Control de estado.
        boolean ok = facturas.checkFacturaEnEstado(factura, Estados.EstadoFacturaEnum.NO_CONFORMADA, Estados.EstadoFacturaEnum.CONFORMADA, Estados.EstadoFacturaEnum.ELIMINADA);
        if (!ok) {
            throw Errors.illegalState("Estado de factura no permite su eliminación.");
        }

        // Cambio de estado de factura.
        Result<Factura> resCambioEstado = facturas.cambiarEstado(auth.getMUsuario().getId(), factura.getId(), Estados.EstadoFacturaEnum.ELIMINADA);
        if (resCambioEstado.isFailure()) {
            throw resCambioEstado.failure().cause();
        }

        // Agregar historico de cambio.
        Result<?> resHistorico = facturas.agregarHistorico(auth.getMUsuario().getId(), resCambioEstado.value(), Estados.EstadoFacturaEnum.ELIMINADA, "Factura Eliminada");
        if (resHistorico.isFailure()) {
            throw resCambioEstado.failure().cause();
        }

        return ServiceResponse.ok().build();
    }

    private Input processInput(JsonObject json0) {
        Input input = new Input();
        input.identificacionFactura = JIdentificacionFactura.parse(json0);
        return input;
    }

    @Data
    private static class Input implements Serializable {

        JIdentificacionFactura identificacionFactura;
    }

}
