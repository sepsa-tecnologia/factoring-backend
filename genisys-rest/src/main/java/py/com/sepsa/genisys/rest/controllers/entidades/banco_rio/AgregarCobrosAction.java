/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.rest.controllers.RestControllerActionWithNoParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_ActualizarCobrosPendientes;

/**
 *
 * @author Fabio A. González Sosa
 */
public class AgregarCobrosAction extends RestControllerActionWithNoParam {

    private AppLogger log;

    public AgregarCobrosAction() {
        this.log = AppLoggerFactory.ingest("banco-rio");
    }

    @Override
    public Response doAction() throws Throwable {
        PF_BancoRio_ActualizarCobrosPendientes actualizarCobrosPendientes = Injection.lookup(PF_BancoRio_ActualizarCobrosPendientes.class);
        actualizarCobrosPendientes.work();
        return ServiceResponse.ok().build();
    }

}
