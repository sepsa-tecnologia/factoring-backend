/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fa.gs.misc.collections.Lists;
import fa.gs.misc.fechas.Fechas;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.Lote;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.logic.impl.PF_FacturaVentaLote_BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.PF_FacturaVentaLote_Sepsa;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculoDesembolsoFacturaInfo;
import py.com.sepsa.genisys.ejb.logic.pojos.SolicitudDesembolsoResultadoInfo;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.utils.Context;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class SolicitarDesembolsoAction extends RestControllerActionWithJsonObjectParam {

    private Facturas facturas;

    private Perfiles perfiles;

    private Personas personas;

    private Notificaciones notificaciones;

    private BancoRio integracionBancoRio;

    private PF_FacturaVentaLote_Sepsa ventaLoteSepsa;

    private PF_FacturaVentaLote_BancoRio ventaLoteBancoRio;

    public SolicitarDesembolsoAction() {
        this.facturas = Injection.lookup(Facturas.class);
        this.perfiles = Injection.lookup(Perfiles.class);
        this.personas = Injection.lookup(Personas.class);
        this.notificaciones = Injection.lookup(Notificaciones.class);
        this.integracionBancoRio = Injection.lookup(BancoRio.class);
        this.ventaLoteSepsa = Injection.lookup(PF_FacturaVentaLote_Sepsa.class);
        this.ventaLoteBancoRio = Injection.lookup(PF_FacturaVentaLote_BancoRio.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        UserAuthenticationInfo auth = Context.getUserInfo();
        Input input = processInput(json0);

        // Usuario proveedor.
        Usuario usuario = auth.getMUsuario();

        // Perfil proveedor.
        Perfil perfilProveedor = perfiles.obtenerPerfil(PerfilEnum.PROVEEDOR);

        // Entidad financiera.
        Persona entidad = personas.obtenerPorId(input.idEntidadFinanciera).value(null);
        if (entidad == null) {
            throw Errors.illegalState("Entidad financiera inválida.");
        }

        // Facturas.
        List<FacturaInfo> facturas = Lists.empty();
        for (Integer idFactura : input.idFacturas) {
            FacturaInfo factura = this.facturas.buscarFactura(idFactura).value(null);
            if (factura == null) {
                throw Errors.illegalState("Identificador de factura '%s' es inválido.", Ids.mask(idFactura));
            }
            facturas.add(factura);
        }

        // Calcular desembolsos.
        Collection<CalculoDesembolsoFacturaInfo> calculos = CalcularDesembolsoAction.calcularDesembolsos(usuario, perfilProveedor, facturas, entidad, input.fechaAdelanto);
        for (CalculoDesembolsoFacturaInfo calculo : calculos) {
            if (calculo.getOk() == false) {
                throw Errors.illegalState("Error en factura '%s'. %s.", Ids.mask(calculo.getFactura().getId()), calculo.getMessage());
            }
        }

        // Generar lote.
        Lote lote = generarLote();

        // Solicitar desembolso.
        SolicitudDesembolsoResultadoInfo result = solicitarDesembolso(usuario, perfilProveedor, lote, calculos, entidad);
        if (!result.isOk()) {
            throw Errors.illegalState(result.getErr());
        }

        // Notificar.
        List<FacturaInfo> facturasOk = Lists.empty();
        facturasOk.addAll(facturas);
        notificaciones.notificarVentaDeFacturas(facturasOk);

        return ControllersUtils.responseOk();
    }

    private Lote generarLote() throws Throwable {
        Result<Lote> resLote = this.facturas.crearLoteFacturas();
        resLote.raise();
        return resLote.value();
    }

    private SolicitudDesembolsoResultadoInfo solicitarDesembolso(Usuario usuario, Perfil perfil, Lote lote, Collection<CalculoDesembolsoFacturaInfo> calculos, Persona entidadFinanciera) throws Throwable {
        if (integracionBancoRio.esBancoRio(entidadFinanciera.getId())) {
            return ventaLoteBancoRio.vender(usuario, perfil, lote.getId(), calculos, entidadFinanciera);
        }

        if (true) {
            return ventaLoteSepsa.vender(usuario, perfil, lote.getId(), calculos, entidadFinanciera);
        }

        throw Errors.illegalState("No debe ocurrir.");
    }

    private Input processInput(JsonObject json0) {
        // Identificadores de facturas.
        Collection<Integer> idFacturas = Lists.empty();
        for (JsonElement element : json0.get("id_facturas").getAsJsonArray()) {
            // Id de factura.
            String idFactura0 = element.getAsString();
            if (Assertions.stringNullOrEmpty(idFactura0)) {
                throw Errors.illegalArgument("Identificador de factura no puede ser vacío o nulo.");
            }
            Integer idFactura = Ids.unmask(idFactura0);
            if (Assertions.isNull(idFactura) || idFactura <= 0) {
                throw Errors.illegalArgument("Identifiador de factura inválido.");
            }
            idFacturas.add(idFactura);
        }

        // Identificador de entidad financiera.
        String idEntidad0 = JsonResolver.string(json0, "id_entidad_financiera");
        if (Assertions.stringNullOrEmpty(idEntidad0)) {
            throw Errors.illegalArgument("Identificador de entidad financiera no puede ser vacío o nulo.");
        }
        Integer idEntidad = Ids.unmask(idEntidad0);
        if (Assertions.isNull(idEntidad) || idEntidad <= 0) {
            throw Errors.illegalArgument("Identificador de entidad financiera inválido.");
        }

        // Fecha de desembolso.
        Date fechaAdelanto = Fechas.now();
        if (json0.get("fecha_adelanto") != null) {
            fechaAdelanto = Fechas.fromEpoch(json0.get("fecha_adelanto").getAsLong());
        }

        Input input = new Input();
        input.idFacturas = idFacturas;
        input.idEntidadFinanciera = idEntidad;
        input.fechaAdelanto = fechaAdelanto;
        return input;
    }

    @Data
    private static class Input implements Serializable {

        private Collection<Integer> idFacturas;
        private Integer idEntidadFinanciera;
        private Date fechaAdelanto;
    }

}
