/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas;

import com.google.gson.JsonObject;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.io.Serializable;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.info.FacturaEstadoInfo;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.controllers.facturas.pojos.JIdentificacionFactura;
import py.com.sepsa.genisys.rest.utils.Context;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ConsultarEstadoAction extends RestControllerActionWithJsonObjectParam {

    private Facturas facturas;

    public ConsultarEstadoAction() {
        this.facturas = Injection.lookup(Facturas.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        UserAuthenticationInfo auth = Context.getUserInfo();
        Input input = processInput(json0);

        // Obtener factura.
        FacturaInfo factura = Utils.identificarFactura(auth.getMUsuario().getIdPersona(), input.identificacionFactura);
        if (factura == null) {
            throw Errors.illegalState("No se pudo determinar la factura.");
        }

        // Obtener estado.
        Result<FacturaEstadoInfo> resEstado = facturas.obtenerEstado(factura.getId());
        if (resEstado.isFailure()) {
            throw Errors.illegalState("No se pudo determinar la factura.");
        }

        // Estado.
        FacturaEstadoInfo estado = resEstado.value();
        return processOutput(estado);
    }

    private Response processOutput(FacturaEstadoInfo info) {
        // Preparar datos de salida.
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add("id_factura", Ids.mask(info.getFacturaId()));
        builder.add("numero", info.getFacturaNumero());
        builder.add("fecha_emision", info.getFacturaFechaEmision());
        builder.add("proveedor", info.getProveedorRuc());
        builder.add("estado", info.getEstadoDescripcion());
        builder.add("fecha_cambio_estado", info.getEstadoFechaCambio());
        builder.add("observaciones", info.getObservaciones());

        JsonObject json = builder.build();
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    private Input processInput(JsonObject json0) {
        Input input = new Input();
        input.identificacionFactura = JIdentificacionFactura.parse(json0);
        return input;
    }

    @Data
    private static class Input implements Serializable {

        JIdentificacionFactura identificacionFactura;
    }
}
