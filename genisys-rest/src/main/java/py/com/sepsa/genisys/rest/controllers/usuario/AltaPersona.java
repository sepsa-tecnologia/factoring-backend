/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.genisys.rest.controllers.usuario;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonArrayBuilder;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.util.Collection;
import lombok.Data;
import org.springframework.security.crypto.bcrypt.BCrypt;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.factoring.UsuarioPerfil;
import py.com.sepsa.genisys.ejb.entities.factoring.UsuarioPerfilPK;
import py.com.sepsa.genisys.ejb.entities.factoring.UsuarioPropio;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.PersonaRol;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.impl.Sesiones;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.JpaUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class AltaPersona {

    //<editor-fold defaultstate="collapsed" desc="Registro de datos">
    public static Output alta(Input input) throws Throwable {
        // Obtener o crear persona.
        Persona persona = obtenerPersona(input);

        // Obtener o crear email de notificaciones.
        asociarEmail(persona.getId(), input);

        // Obtener o crear usuario.
        UsuarioPropio usuario = obtenerUsuario(input, persona);

        // Obtener perfiles de usuario.
        Collection<Perfil> perfiles = obtenerPerfiles(usuario.getIdUsuario());

        Output output = new Output();
        output.idPersona = persona.getId();
        output.idUsuario = usuario.getIdUsuario();
        output.perfiles = perfiles;
        return output;
    }

    private static Persona obtenerPersona(Input input) throws Throwable {
        final Personas personas = Injection.lookup(Personas.class);

        // Obtener persona.
        Result<Persona> resPersona = personas.obtenerPorRuc(input.persona.ruc);
        Persona persona = resPersona.value(null);
        if (persona == null) {
            // Registrar nueva persona.
            if (input.persona.tipoPersona == Personas.TipoPersonaEnum.FISICA) {
                resPersona = personas.registrarPersonaFisica(input.persona.nombres, input.persona.apellidos, input.persona.ci, input.persona.ruc);
                persona = resPersona.value(null);
            } else if (input.persona.tipoPersona == Personas.TipoPersonaEnum.JURIDICA) {
                resPersona = personas.registrarPersonaJuridica(input.persona.razonSocial, input.persona.nombreFantasia, input.persona.ruc);
                persona = resPersona.value(null);
            } else {
                throw Errors.illegalState();
            }
        }

        // Control.
        if (persona == null) {
            throw Errors.illegalArgument("No se pudo registrar a la persona.");
        }

        // Agregar rol de proveedor.
        Result<PersonaRol> resRol = personas.agregarRol(persona.getId(), input.perfil);
        resRol.raise();

        return persona;
    }

    private static void asociarEmail(Integer idPersona, Input input) throws Throwable {
        final Personas personas = Injection.lookup(Personas.class);
        personas.actualizarEmail(idPersona, input.persona.email);
    }

    private static UsuarioPropio obtenerUsuario(Input input, Persona persona) throws Throwable {
        final Sesiones sesiones = Injection.lookup(Sesiones.class);
        final JpaUtils jpaUtils = Injection.lookup(JpaUtils.class);
        final Perfiles perfiles = Injection.lookup(Perfiles.class);

        // Verificar si usuario propio ya existe.
        Result<UsuarioPropio> resUsuarioPropio = sesiones.obtenerPorFactoringPersonaAlias(persona.getId(), input.username);
        UsuarioPropio usuarioPropio = resUsuarioPropio.value(null);
        if (usuarioPropio != null) {
            return usuarioPropio;
        }

        // Registrar usuario (trans).
        Usuario usuario = new Usuario();
        usuario.setIdPersona(persona.getId());
        usuario.setUsuario(Strings.format("factoring@%s", Ids.randomUuid()));
        usuario.setContrasena(BCrypt.hashpw(input.password, BCrypt.gensalt()));
        usuario.setEstado('A');
        usuario = jpaUtils.create(usuario);

        // Registrar usuario (factoring).
        usuarioPropio = new UsuarioPropio();
        usuarioPropio.setIdUsuario(usuario.getId());
        usuarioPropio.setIdPersona(persona.getId());
        usuarioPropio.setAlias(input.username);
        usuarioPropio.setAuthId(input.username);
        usuarioPropio.setAuthSecret(BCrypt.hashpw(input.password, BCrypt.gensalt()));
        usuarioPropio = jpaUtils.create(usuarioPropio);

        // Registrar perfil.
        Perfil perfil0 = perfiles.obtenerPerfil(input.perfil);
        UsuarioPerfil usuarioPerfil = new UsuarioPerfil();
        usuarioPerfil.setUsuarioPerfilPK(new UsuarioPerfilPK());
        usuarioPerfil.getUsuarioPerfilPK().setIdPerfil(perfil0.getId());
        usuarioPerfil.getUsuarioPerfilPK().setIdUsuario(usuario.getId());
        usuarioPerfil = jpaUtils.create(usuarioPerfil);

        // Preparar datos de salida.
        return usuarioPropio;
    }

    private static Collection<Perfil> obtenerPerfiles(Integer idUsuario) throws Throwable {
        final Sesiones sesiones = Injection.lookup(Sesiones.class);
        Result<Collection<Perfil>> resPerfiles = sesiones.obtenerPorTransUsuario(idUsuario);
        resPerfiles.raise();
        return resPerfiles.value();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Procesar JSON de entrada">
    public static Input processInput(JsonObject json, PerfilEnum perfil) throws Throwable {
        Input instance = new Input();
        instance.perfil = perfil;
        processUsuario(instance, json);
        processPersona(instance, json);
        return instance;
    }

    private static void processUsuario(Input instance, JsonObject json) throws Throwable {
        if (!json.has("usuario")) {
            throw Errors.illegalArgument("El campo usuario no puede ser nulo o vacío.");
        }

        JsonElement usuario0 = json.get("usuario");
        if (!usuario0.isJsonObject()) {
            throw Errors.illegalArgument("El campo usuario debe ser un objeto.");
        }

        // Nombre de usuario.
        String username = JsonResolver.string(json, "usuario.username");
        if (Assertions.stringNullOrEmpty(username)) {
            throw Errors.illegalArgument("El campo usuario.username no puede ser vacía o nula.");
        }

        // Contraseña.
        String password = JsonResolver.string(json, "usuario.password");
        if (Assertions.stringNullOrEmpty(password)) {
            throw Errors.illegalArgument("El campo usuario.password no puede ser vacía o nula.");
        }

        // Ok.
        instance.username = username;
        instance.password = password;
    }

    private static void processPersona(Input instance, JsonObject json) throws Throwable {
        if (!json.has("persona")) {
            throw Errors.illegalArgument("El campo persona no puede ser nulo o vacío.");
        }

        JsonElement persona0 = json.get("persona");
        if (!persona0.isJsonObject()) {
            throw Errors.illegalArgument("El campo persona debe ser un objeto.");
        }

        // Tipo de Persona.
        String tipoPersona0 = JsonResolver.string(json, "persona.tipo", "j");
        if (Assertions.stringNullOrEmpty(tipoPersona0)) {
            tipoPersona0 = "j";
        }

        Personas.TipoPersonaEnum tipoPersona;
        if (tipoPersona0.toLowerCase().equals("j")) {
            tipoPersona = Personas.TipoPersonaEnum.JURIDICA;
        } else {
            tipoPersona = Personas.TipoPersonaEnum.FISICA;
        }

        // RUC.
        String ruc = JsonResolver.string(json, "persona.ruc");
        if (Assertions.stringNullOrEmpty(ruc)) {
            throw Errors.illegalArgument("RUC no puede ser vacío o nulo.");
        }
        if (!Assertions.isRuc(ruc)) {
            throw Errors.illegalArgument("RUC con formato inválido.");
        }

        // Email.
        String email = JsonResolver.string(json, "persona.email");
        if (Assertions.stringNullOrEmpty(email)) {
            throw Errors.illegalArgument("Email no puede ser vacío o nulo.");
        }
        if (!Assertions.isEmail(email)) {
            throw Errors.illegalArgument("Email con formato inválido.");
        }

        // Datos de Persona: JURIDICA.
        if (tipoPersona == Personas.TipoPersonaEnum.JURIDICA) {
            // Razon Social.
            String razonSocial = JsonResolver.string(json, "persona.razon_social");
            if (Assertions.stringNullOrEmpty(razonSocial)) {
                throw Errors.illegalArgument("Razón social no puede ser vacía o nula.");
            }

            // Nombre de fantasia.
            String nombreFantasia = JsonResolver.string(json, "persona.nombre_fantasia", razonSocial);

            instance.persona = new InputPersona();
            instance.persona.tipoPersona = tipoPersona;
            instance.persona.razonSocial = razonSocial;
            instance.persona.nombreFantasia = nombreFantasia;
            instance.persona.ruc = ruc;
            instance.persona.email = email;
            return;
        }

        // Datos de Persona: FISICA.
        if (tipoPersona == Personas.TipoPersonaEnum.FISICA) {
            // Nombres.
            String nombres = JsonResolver.string(json, "persona.nombres");
            if (Assertions.stringNullOrEmpty(nombres)) {
                throw Errors.illegalArgument("Nombres no puede ser vacía o nula.");
            }

            // Apellidos.
            String apellidos = JsonResolver.string(json, "persona.apellidos");

            // Ci.
            String ci = JsonResolver.string(json, "persona.ci");
            if (Assertions.stringNullOrEmpty(ci)) {
                throw Errors.illegalArgument("Documento de identidad no puede ser nula o vacía.");
            }

            instance.persona = new InputPersona();
            instance.persona.tipoPersona = tipoPersona;
            instance.persona.nombres = nombres;
            instance.persona.apellidos = apellidos;
            instance.persona.ci = ci;
            instance.persona.ruc = ruc;
            instance.persona.email = email;
            return;
        }

        throw Errors.illegalArgument("No se pudo determinar el tipo de persona.");
    }

    //<editor-fold defaultstate="collapsed" desc="Procesar JSON de salida">
    static JsonElement processOutput(Output output) throws Throwable {
        JsonObjectBuilder ob = JsonObjectBuilder.instance();
        ob.add("id_usuario", Ids.mask(output.idUsuario));
        ob.add("id_persona", Ids.mask(output.idPersona));
        ob.add("perfiles", adapt(output.perfiles));
        return ob.build();
    }

    private static JsonElement adapt(Collection<Perfil> perfiles) {
        JsonArrayBuilder ab = JsonArrayBuilder.instance();
        if (!Assertions.isNullOrEmpty(perfiles)) {
            for (Perfil perfil : perfiles) {
                ab.add(perfil.getDescripcion());
            }
        }
        return ab.build();
    }
    //</editor-fold>

    @Data
    static class Input implements Serializable {

        private String username;
        private String password;
        private InputPersona persona;
        private PerfilEnum perfil;
    }

    @Data
    static class InputPersona implements Serializable {

        private Personas.TipoPersonaEnum tipoPersona;
        private String razonSocial;
        private String nombreFantasia;
        private String nombres;
        private String apellidos;
        private String ci;
        private String ruc;
        private String email;
    }

    @Data
    static class Output implements Serializable {

        private Integer idUsuario;
        private Integer idPersona;
        private Collection<Perfil> perfiles;
    }

}
