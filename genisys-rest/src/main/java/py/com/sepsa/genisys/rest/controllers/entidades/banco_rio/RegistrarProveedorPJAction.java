/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import com.google.gson.JsonElement;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import fa.gs.utils.rest.controllers.RestControllerActionWithCustomParam;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioActividadEconomica;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioBanco;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioCiudad;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioRegion;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoDocumento;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoSociedad;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Entidades;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaJuridicaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaJuridicaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ContactoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.CuentaInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DireccionInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ReferenciaInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.RubroInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.SociedadInfo;
import py.com.sepsa.genisys.rest.controllers.entidades.banco_rio.pojos.JBancoRioContacto;
import py.com.sepsa.genisys.rest.controllers.entidades.banco_rio.pojos.JBancoRioCuentaAcreditacion;
import py.com.sepsa.genisys.rest.controllers.entidades.banco_rio.pojos.JBancoRioDireccion;
import py.com.sepsa.genisys.rest.controllers.entidades.banco_rio.pojos.JBancoRioDocumento;
import py.com.sepsa.genisys.rest.controllers.entidades.banco_rio.pojos.JBancoRioSociedad;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RegistrarProveedorPJAction extends RestControllerActionWithCustomParam<RegistrarProveedorPJAction.Input> {

    @Override
    public Response doAction(RegistrarProveedorPJAction.Input params) throws Throwable {
        // Registrar proveedor.
        Input0 input0 = adaptarParametros(params);
        Result<RegistroProveedorPersonaJuridicaOutput> result = BancoRioControllerUtils.registrarProveedor(input0.proveedor, input0.data);
        if (result.isFailure()) {
            return ControllersUtils.responseKo(result);
        }

        // Ok.
        RegistroProveedorPersonaJuridicaOutput output = result.value();
        JsonElement json = adaptarDatosSalida(output);
        return ControllersUtils.responseOk(json);
    }

    private Input0 adaptarParametros(RegistrarProveedorPJAction.Input params) {
        // Datos de input para operacion de registro de proveedor.
        String ruc = BancoRioControllerUtils.validarRuc(params.ruc);
        Persona proveedor = BancoRioControllerUtils.validarPersona(params.ruc);
        Entidades.EstadoAdhesionEntidad estadoAdhesionProveedor = BancoRioControllerUtils.validarEstadoAdhesionOkEntidadProveedor(proveedor);
        String nroDocumento = BancoRioControllerUtils.validarNroDocumento(params.documento.getNroDocumento());
        BancoRioTipoDocumento tipoDocumento = BancoRioControllerUtils.validarTipoDocumento(params.documento.getCodigoTipoDocumento());
        byte[] imagenDocumento = BancoRioControllerUtils.validarImagenDocumento(params.documento.getImagen());
        BancoRioTipoSociedad tipoSociedad = BancoRioControllerUtils.validarTipoSociendad(params.sociedad.getCodigoTipoSociedad());
        String barrio = BancoRioControllerUtils.validarBarrio(params.direccion.getBarrio());
        String calle1 = BancoRioControllerUtils.validarCalle1(params.direccion.getCalle1());
        String calle2 = BancoRioControllerUtils.validarCalle2(params.direccion.getCalle2());
        String nroCalle = BancoRioControllerUtils.validarNroCalle(params.direccion.getNroCalle());
        BancoRioCiudad ciudad = BancoRioControllerUtils.validarCiudad(params.direccion.getCodigoCiudad());
        BancoRioRegion region = BancoRioControllerUtils.validarRegion(ciudad.getCodigoRegion());
        Date fechaConstitucion = BancoRioControllerUtils.validarFechaDeConstitucion(params.sociedad.getFechaConstitucion());
        String nroCelular = BancoRioControllerUtils.validarNroCelular(params.contacto.getNroCelular());
        String nroLineaBaja = BancoRioControllerUtils.validarNroLineaBaja(params.contacto.getNroLineaBaja());
        String email = BancoRioControllerUtils.validarEmail(params.contacto.getEmail());
        BancoRioActividadEconomica actividadEconomica = BancoRioControllerUtils.validarActividadEconomica(params.codigoActividadEconomica);
        String rubro = BancoRioControllerUtils.validarRubro(params.rubro);
        String nroCuenta = BancoRioControllerUtils.validarNroCuenta(params.cuentaAcreditacion.getNroCuenta());
        BancoRioBanco banco = BancoRioControllerUtils.validarBanco(params.cuentaAcreditacion.getCodBanco());
        BigDecimal facturacionPromedio = BancoRioControllerUtils.validarFacturacionPromedio(params.facturacionPromedio);
        Persona comprador = BancoRioControllerUtils.validarCodigoClienteComprador(params.codClienteComprador);
        DocumentoInfo documentoComprador = BancoRioControllerUtils.validarDocumentoComprador(comprador);
        Entidades.EstadoAdhesionEntidad estadoAdhesionComprador = BancoRioControllerUtils.validarEstadoAdhesionKoEntidadComprador(comprador);

        RegistroProveedorPersonaJuridicaInput instance = new RegistroProveedorPersonaJuridicaInput();
        instance.setRazonSocial(Text.razonSocial(proveedor));
        instance.setDocumento(new DocumentoInfo());
        instance.getDocumento().setTipoDocumento(tipoDocumento);
        instance.getDocumento().setNroDocumento(nroDocumento);
        instance.getDocumento().setImagen(imagenDocumento);
        instance.setSociedad(new SociedadInfo());
        instance.getSociedad().setTipoSociedad(tipoSociedad);
        instance.getSociedad().setFechaConstitucion(fechaConstitucion);
        instance.setDireccion(new DireccionInfo());
        instance.getDireccion().setCalle1(calle1);
        instance.getDireccion().setCalle2(calle2);
        instance.getDireccion().setCalle3("");
        instance.getDireccion().setReferencia(BancoRioControllerUtils.DIRECCION_REFERENCIA);
        instance.getDireccion().setNroCalle(nroCalle);
        instance.getDireccion().setBarrio(barrio);
        instance.getDireccion().setCiudad(ciudad);
        instance.getDireccion().setRegion(region);
        instance.setContacto(new ContactoInfo());
        instance.getContacto().setNroLineaBaja(nroLineaBaja);
        instance.getContacto().setNroCelular(nroCelular);
        instance.getContacto().setEmail(email);
        instance.setCuentaAcreditacion(new CuentaInfo());
        instance.getCuentaAcreditacion().setNroCuenta(nroCuenta);
        instance.getCuentaAcreditacion().setBanco(banco);
        instance.setActividadEconomica(actividadEconomica);
        instance.setRubro(new RubroInfo());
        instance.getRubro().setDescripcion(rubro);
        instance.setFacturacionPromedio(facturacionPromedio);
        instance.setComprador(new ReferenciaInfo());
        instance.getComprador().setPersona(comprador);
        instance.getComprador().setDocumento(documentoComprador);

        // Datos finales.
        Input0 input = new Input0();
        input.comprador = comprador;
        input.proveedor = proveedor;
        input.data = instance;
        return input;
    }

    private JsonElement adaptarDatosSalida(RegistroProveedorPersonaJuridicaOutput output) {
        JsonObjectBuilder ob = JsonObjectBuilder.instance();
        ob.add("codigo_cliente", output.getRespuesta().getCodigoCliente());
        return ob.build();
    }

    private static class Input0 implements Serializable {

        Persona comprador;
        Persona proveedor;
        RegistroProveedorPersonaJuridicaInput data;
    }

    @Data
    public static class Input implements Serializable {

        @JsonProperty
        private String ruc;

        @JsonProperty
        private JBancoRioDocumento documento;

        @JsonProperty
        private JBancoRioSociedad sociedad;

        @JsonProperty
        private JBancoRioDireccion direccion;

        @JsonProperty
        private JBancoRioContacto contacto;

        @JsonProperty(name = "codigo_actividad_economica")
        private String codigoActividadEconomica;

        @JsonProperty
        private String rubro;

        @JsonProperty(name = "cuenta_acreditacion")
        private JBancoRioCuentaAcreditacion cuentaAcreditacion;

        @JsonProperty(name = "facturacion_promedio")
        private BigDecimal facturacionPromedio;

        @JsonProperty(name = "codigo_comprador")
        private String codClienteComprador;
    }

}
