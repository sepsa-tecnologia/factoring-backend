/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.authentication.tokens.TokenExtractor;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.Unit;
import fa.gs.utils.rest.controllers.RestController;
import fa.gs.utils.rest.exceptions.ApiUnauthorizedException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.security.UserTokenAuthenticator;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.utils.TxWrapperRest;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ApplicationRestController extends RestController {

    private final AppLogger log;

    public ApplicationRestController() {
        this.log = AppLoggerFactory.rest();
    }

    @Override
    public AppLogger getLog() {
        return log;
    }

    public Response wrapInTx(Unit<Response> unit) {
        TxWrapperRest tx = Injection.lookup(TxWrapperRest.class);
        Result<Response> result = tx.execute(() -> {
            Result<Response> result0;

            try {
                Response response = unit.execute();
                result0 = Results.ok()
                        .value(response)
                        .nullable(true)
                        .build();
            } catch (Throwable thr) {
                result0 = Results.ko()
                        .cause(thr)
                        .build();
            }

            return result0;
        });

        if (result.isSuccess()) {
            return result.value();
        } else {
            return deriveResponseFromError(result.failure().cause());
        }
    }

    public static AuthenticationInfo getAuthentication() throws ApiUnauthorizedException {
        // Extraer token.
        HttpServletRequest request = ResteasyProviderFactory.getContextData(HttpServletRequest.class);
        TokenExtractor.COOKIE_NAME = "APP_SESSION_TOKEN_GENISYS";
        String token = TokenExtractor.fromHttpRequest(request);

        // Control.
        if (Assertions.stringNullOrEmpty(token)) {
            throw new ApiUnauthorizedException();
        }

        // Control.
        if (!UserTokenAuthenticator.tokenIsValid(token)) {
            throw new ApiUnauthorizedException();
        }

        // Parsear informacion de token.
        try {
            UserTokenAuthenticator auth = new UserTokenAuthenticator();
            AuthenticationInfo info = auth.authenticateToken(token);
            return info;
        } catch (Throwable thr) {
            throw new ApiUnauthorizedException(thr);
        }
    }

    public static void checkAuthentication(PerfilEnum... allowedRoles) throws ApiUnauthorizedException {
        // Verificar perfil correcto.
        AuthenticationInfo info = getAuthentication();
        if (!Assertions.isNullOrEmpty(allowedRoles)) {
            boolean perfilOk = false;
            for (PerfilEnum perfil0 : allowedRoles) {
                if (info.isUserInRole(perfil0.descripcion())) {
                    perfilOk = true;
                    break;
                }
            }
            if (!perfilOk) {
                throw new ApiUnauthorizedException();
            }
        }
    }

}
