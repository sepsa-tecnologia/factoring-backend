/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import com.google.gson.JsonElement;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import fa.gs.utils.rest.controllers.RestControllerActionWithCustomParam;
import java.io.Serializable;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioPersona;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Entidades;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ConsultarRegistroCompradorAction extends RestControllerActionWithCustomParam<ConsultarRegistroCompradorAction.Input> {

    @Override
    public Response doAction(ConsultarRegistroCompradorAction.Input params) throws Throwable {
        // Consultar estado de registro de comprador.
        Input0 input0 = adaptarParametros(params);
        Entidades.EstadoAdhesionEntidad estado = BancoRioControllerUtils.obtenerEstadoAdhesionEntidad(input0.comprador, PerfilEnum.COMPRADOR);
        if (estado == null) {
            return ControllersUtils.responseKo("El comprador no tiene ninguna solicitud de registro pendiente");
        }

        // Consultar datos de persona.
        BancoRioPersona persona = BancoRioControllerUtils.obtenerPersona(input0.comprador, PerfilEnum.COMPRADOR);
        if (persona == null) {
            return ControllersUtils.responseKo("El comprador no tiene ninguna solicitud de registro pendiente");
        }

        // Ok.
        JsonElement json = adaptarDatosSalida(estado, persona);
        return ControllersUtils.responseOk(json);
    }

    private JsonElement adaptarDatosSalida(Entidades.EstadoAdhesionEntidad estado, BancoRioPersona persona) {
        JsonObjectBuilder ob = JsonObjectBuilder.instance();
        ob.add("codigo", estado.codigo());
        ob.add("estado", String.valueOf(estado));
        ob.add("codigo_cliente", persona.getCodigoCliente());
        return ob.build();
    }

    private Input0 adaptarParametros(ConsultarRegistroCompradorAction.Input params) {
        // Datos de input para operacion de registro de comprador.
        String ruc = BancoRioControllerUtils.validarRuc(params.ruc);
        Persona comprador = BancoRioControllerUtils.validarPersona(params.ruc);

        // Datos finales.
        Input0 input = new Input0();
        input.comprador = comprador;
        return input;
    }

    private static class Input0 implements Serializable {

        Persona comprador;
    }

    @Data
    public static class Input implements Serializable {

        @JsonProperty
        private String ruc;

    }

}
