/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.utils.Context;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RechazarComoCompradorAction extends RestControllerActionWithJsonObjectParam {

    private AppLogger log;

    private Personas personas;

    private Facturas facturas;

    private Notificaciones notificaciones;

    public RechazarComoCompradorAction() {
        this.log = AppLoggerFactory.rest();
        this.personas = Injection.lookup(Personas.class);
        this.facturas = Injection.lookup(Facturas.class);
        this.notificaciones = Injection.lookup(Notificaciones.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        UserAuthenticationInfo auth = Context.getUserInfo();
        Input input = processInput(json0);

        // Obtener comprador.
        Result<Persona> resComprador = personas.obtenerPorId(auth.getMUsuario().getIdPersona());
        if (resComprador.isFailure()) {
            throw Errors.illegalState(resComprador.failure().cause(), "No se pudo determinar el comprador.");
        }

        // Validar comprador.
        Persona comprador = resComprador.value();
        if (comprador == null || comprador.getId() == null) {
            throw Errors.illegalState("No se pudo determinar el comprador.");
        }

        // Obtener facturas.
        final List<FacturaInfo> facturas0 = Lists.empty();
        for (Integer idFactura : input.idFacturas) {
            Result<FacturaInfo> resFactura = facturas.buscarFactura(idFactura);
            FacturaInfo factura0 = resFactura.value(null);
            if (factura0 == null) {
                throw Errors.illegalState("No se pudo encontrar la factura id '%s'.", Ids.mask(idFactura));
            }

            // Validar comprador.
            if (!Objects.equals(comprador.getId(), factura0.getCompradorId())) {
                throw Errors.illegalState("La factura id '%s' no pertenece a este comprador.", Ids.mask(idFactura));
            }

            // Validar estado.
            if (!facturas.checkFacturaEnEstado(factura0, Estados.EstadoFacturaEnum.NO_CONFORMADA)) {
                throw Errors.illegalState("La factura id '%s' no presenta el estado NO CONFORMADA.", Ids.mask(idFactura));
            }

            facturas0.add(factura0);
        }

        // Rechazar facturas.
        for (FacturaInfo factura0 : facturas0) {
            try {
                rechazar(auth.getMUsuario().getId(), factura0);
            } catch (Throwable thr) {
                log.warning(thr, "No se pudo rechazar la factura id %s.", factura0.getId());
                throw Errors.illegalState("Ocurrió un error rechazando factura id '%s'.", Ids.mask(factura0.getId()));
            }
        }

        // Notificar.
        notificaciones.notificarRechazoDeFacturasPorComprador(facturas0);

        // Ok.
        return ServiceResponse.ok()
                .build();
    }

    private Input processInput(JsonObject json0) {
        // Identificadores de facturas.
        Collection<Integer> idFacturas = Lists.empty();
        for (JsonElement element : json0.get("id_facturas").getAsJsonArray()) {
            // Id de factura.
            String idFactura0 = element.getAsString();
            if (Assertions.stringNullOrEmpty(idFactura0)) {
                throw Errors.illegalArgument("Identificador de factura no puede ser vacío o nulo.");
            }
            Integer idFactura = Ids.unmask(idFactura0);
            if (Assertions.isNull(idFactura) || idFactura <= 0) {
                throw Errors.illegalArgument("Identifiador de factura inválido.");
            }
            idFacturas.add(idFactura);
        }

        Input input = new Input();
        input.setIdFacturas(idFacturas);
        return input;
    }

    private void rechazar(Integer idUsuario, FacturaInfo factura) throws Throwable {
        Result<Factura> resultFactura = facturas.cambiarEstado(idUsuario, factura.getId(), Estados.EstadoFacturaEnum.RECHAZADA_POR_COMPRADOR);
        resultFactura.raise();
        Factura factura0 = resultFactura.value();

        Result<HistoricoFactura> resHistoricoFactura = facturas.agregarHistorico(idUsuario, factura0, Estados.EstadoFacturaEnum.RECHAZADA_POR_COMPRADOR, "Factura rechazada desde API REST.");
        resHistoricoFactura.raise();
    }

    @Data
    private static class Input implements Serializable {

        private Collection<Integer> idFacturas;
    }
}
