/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.utils;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.Status;
import javax.transaction.UserTransaction;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Constantes;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "TxWrapperRest", mappedName = "TxWrapperRest")
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class TxWrapperRest implements Serializable {

    public static final AppLogger log = AppLoggerFactory.core();

    @Resource
    private UserTransaction tx;

    public <T> Result<T> execute(TxWrapperRest.Executable<T> executable) {
        // Control de seguridad.
        if (executable == null) {
            throw new IllegalArgumentException("El codigo de ejecucion no puede ser nulo");
        }

        Result<T> result;

        try {
            // Iniciar transaccion.
            log.debug("iniciando transaccion");
            tx.setTransactionTimeout((int) Constantes.TX_TIMEOUT_UNIT.toSeconds(Constantes.TX_TIMEOUT_VALUE));
            tx.begin();
            try {
                // Ejectura bloque de codigo.
                log.debug("ejecutando transaccion");
                result = executable.execute();
                if (result.isFailure()) {
                    if (result.failure().cause() != null) {
                        throw result.failure().cause();
                    } else {
                        throw Errors.illegalState();
                    }
                }

                // Confirmar (commitear) cambios realizados.
                if (tx.getStatus() == Status.STATUS_MARKED_ROLLBACK) {
                    log.debug("finalizando transaccion (rollback)");
                    tx.rollback();
                } else {
                    log.debug("finalizando transaccion (commit)");
                    tx.commit();
                }
            } catch (Throwable thr) {
                // Desechar (rollback) cambios realizados.
                log.error("desechando transaccion (rollback)");
                tx.rollback();
                result = Results.ko()
                        .cause(thr)
                        .message("Ocurrió un error durante ejecucion de bloque de código")
                        .build();
            }
        } catch (Throwable thr) {
            // No se pudo iniciar la transaccion.
            log.fatal(thr, "desechando ejecucion (error)");
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error durante ejecucion de transacción")
                    .build();
        }

        return result;
    }

    public <T> Result<T> execute(final TxWrapperRest.Executable2<T> executable2) {
        return execute(() -> {
            try {
                T value = executable2.execute();
                return Results.ok()
                        .value(value)
                        .build();
            } catch (Throwable thr) {
                return Results.ko()
                        .cause(thr)
                        .build();
            }
        });
    }

    public interface Executable<T> {

        Result<T> execute();

    }

    public interface Executable2<T> {

        T execute() throws Throwable;

    }

}
