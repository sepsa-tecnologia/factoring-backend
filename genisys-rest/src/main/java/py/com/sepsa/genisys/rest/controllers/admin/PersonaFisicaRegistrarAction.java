/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.admin;

import com.google.gson.JsonElement;
import fa.gs.misc.Assertions;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import fa.gs.utils.rest.controllers.RestControllerActionWithCustomParam;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.pojos.RegistrarPersonaInput;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
public class PersonaFisicaRegistrarAction extends RestControllerActionWithCustomParam<PersonaFisicaRegistrarAction.Input> {

    @Override
    public Response doAction(Input params) throws Throwable {
        // Adaptar parametros.
        RegistrarPersonaInput input = adaptarInput(params);

        // Validar datos
        String err = validarInput(params);
        if (!Assertions.isNull(err)) {
            return ControllersUtils.responseKo(err);
        }

        // Registrar datos.
        Personas personas = Injection.lookup(Personas.class);

        Result<Persona> resDatos = personas.registrarPersonaFisica(input.getNombres(), input.getApellidos(), input.getCi(), input.getRuc());
        if (resDatos.isFailure()) {
            return ControllersUtils.responseKo(resDatos);
        }

        // Preparar datos de salida
        Result<Persona> resNuevaPersona = personas.obtenerPorId(resDatos.value().getId());
        if (resNuevaPersona.isFailure()) {
            return ControllersUtils.responseKo(resNuevaPersona);
        }
        JsonElement json = adapt2Json(resNuevaPersona.value());
        return ControllersUtils.responseOk(json);
    }

    private RegistrarPersonaInput adaptarInput(Input params) {
        RegistrarPersonaInput input = new RegistrarPersonaInput();
        input.setRuc(params.ruc);
        input.setNombres(params.nombres);
        input.setApellidos(params.apellidos);
        input.setCi(params.ci);

        input.setTipo(Personas.TipoPersonaEnum.FISICA);
        return input;
    }

    private String validarInput(Input params) {

        // Validar RUC
        if (Assertions.stringNullOrEmpty(params.ruc)) {
            return "El campo 'ruc' es mandatorio";
        }

        // Validar RUC único
        String ruc = params.ruc;
        Personas personas = Injection.lookup(Personas.class);
        Persona persona = personas.obtenerPorRuc(ruc).value(null);
        if (!Assertions.isNull(persona)) {
            return "El RUC debe ser único";
        }

        // Validar nombres
        if (Assertions.stringNullOrEmpty(params.nombres)) {
            return "El campo 'nombres' es mandatorio";
        }

        // Validar apellidos
        if (Assertions.stringNullOrEmpty(params.apellidos)) {
            return "El campo 'apellidos' es mandatorio";
        }

        // Validar ci
        if (Assertions.stringNullOrEmpty(params.ci)) {
            return "El campo 'ci' es mandatorio";
        }

        return null;
    }

    private JsonElement adapt2Json(Persona instance) {
        JsonObjectBuilder ob = JsonObjectBuilder.instance();
        ob.add("id", instance.getId());
        ob.add("nombres", instance.getPfNombres());
        ob.add("apellidos", instance.getPfApellidos());
        ob.add("ci", instance.getPfCi());
        ob.add("ruc", instance.getRuc());
        ob.add("dv_ruc", instance.getDvRuc());
        ob.add("tipo_persona", Personas.TipoPersonaEnum.fromIdTipoPersona(instance.getIdTipoPersona()).descripcion());
        return ob.build();
    }

    @Data
    public static class Input {

        @JsonProperty
        private String nombres;

        @JsonProperty
        private String apellidos;

        @JsonProperty
        private String ci;

        @JsonProperty
        private String ruc;

    }

}
