/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio.pojos;

import fa.gs.utils.misc.json.adapter.impl.Json2Epoch;
import fa.gs.utils.misc.json.serialization.JsonPostConstruct;
import fa.gs.utils.misc.json.serialization.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class JBancoRioSociedad implements Serializable {

    @JsonProperty(name = "codigo_tipo_sociedad")
    private String codigoTipoSociedad;

    @JsonProperty(fromJsonAdapter = Json2Epoch.class, name = "fecha_constitucion")
    private Date fechaConstitucion;

    @JsonPostConstruct
    public void init() {
        if (codigoTipoSociedad == null) {
            codigoTipoSociedad = "";
        }
    }
}
