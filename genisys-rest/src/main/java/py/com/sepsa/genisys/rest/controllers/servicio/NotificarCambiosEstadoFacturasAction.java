/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.servicio;

import fa.gs.utils.rest.controllers.RestControllerActionWithNoParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.logic.impl.PF_NotificarCambiosEstadoFacturas;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class NotificarCambiosEstadoFacturasAction extends RestControllerActionWithNoParam {

    @Override
    public Response doAction() throws Throwable {
        PF_NotificarCambiosEstadoFacturas notificarCambiosEstadoFacturas = Injection.lookup(PF_NotificarCambiosEstadoFacturas.class);
        notificarCambiosEstadoFacturas.work();
        return ServiceResponse.ok()
                .build();
    }

}
