/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio;

import com.google.gson.JsonElement;
import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.json.JsonArrayBuilder;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.rest.controllers.RestControllerActionWithNoParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.util.Collection;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoDocumento;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ListarTiposDocumentodadAction extends RestControllerActionWithNoParam {

    private final BancoRio bancoRio;

    public ListarTiposDocumentodadAction() {
        this.bancoRio = Injection.lookup(BancoRio.class);
    }

    @Override
    public Response doAction() throws Throwable {
        // Obtener tipos de documento.
        Result<Collection<BancoRioTipoDocumento>> result = bancoRio.obtenerTiposDocumento();
        if (result.isFailure()) {
            return ServiceResponse.ko()
                    .cause(result.failure().cause())
                    .build();
        }

        Collection<BancoRioTipoDocumento> tiposDocumento = result.value(Lists.empty());
        JsonElement json = adaptarDatosSalida(tiposDocumento);
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    private JsonElement adaptarDatosSalida(Collection<BancoRioTipoDocumento> tiposDocumento) {
        JsonArrayBuilder ab = JsonArrayBuilder.instance();
        if (!Assertions.isNullOrEmpty(tiposDocumento)) {
            for (BancoRioTipoDocumento tipoDocumento : tiposDocumento) {
                JsonObjectBuilder ob = JsonObjectBuilder.instance();
                ob.add("id", tipoDocumento.getId());
                ob.add("descripcion", tipoDocumento.getDescripcion());
                ob.add("codigo", tipoDocumento.getCodigo());
                ob.add("sigla", tipoDocumento.getSigla());
                ab.add(ob.build());
            }
        }
        return ab.build();
    }

}
