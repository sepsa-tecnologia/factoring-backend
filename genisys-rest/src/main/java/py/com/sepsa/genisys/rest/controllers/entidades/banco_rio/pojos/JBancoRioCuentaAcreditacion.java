/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.entidades.banco_rio.pojos;

import fa.gs.utils.misc.json.serialization.JsonProperty;
import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class JBancoRioCuentaAcreditacion implements Serializable {

    @JsonProperty(name = "nro_cuenta")
    private String nroCuenta;

    @JsonProperty(name = "codigo_banco")
    private String codBanco;

}
