/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.usuario;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.rest.ApplicationRestController;

/**
 *
 * @author Fabio A. González Sosa
 */
@Path("/")
public class UsuariosController extends ApplicationRestController {

    @POST
    @Path("/v1/usuario.login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response usuarioLogin(final String body) {
        return wrapInTx(() -> {
            return executeControllerAction(LoginAction.class, body);
        });
    }

    @POST
    @Path("/v1/usuario.altaProveedor")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response usuarioAltaProveedor(final String body) {
        return wrapInTx(() -> {
            checkAuthentication();
            return executeControllerAction(AltaProveedorAction.class, body);
        });
    }

    @POST
    @Path("/v1/usuario.altaComprador")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response usuarioAltaComprador(final String body) {
        return wrapInTx(() -> {
            checkAuthentication();
            return executeControllerAction(AltaCompradorAction.class, body);
        });
    }

    @POST
    @Path("/v1/usuario.relacionarProveedor")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response usuarioRelacionarProveedor(final String body) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.COMPRADOR);
            return executeControllerAction(RelacionarProveedorAction.class, body);
        });
    }

    @POST
    @Path("/v1/usuario.relacionarComprador")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response usuarioRelacionarComprador(final String body) {
        return wrapInTx(() -> {
            checkAuthentication(PerfilEnum.PROVEEDOR);
            return executeControllerAction(RelacionarCompradorAction.class, body);
        });
    }

}
