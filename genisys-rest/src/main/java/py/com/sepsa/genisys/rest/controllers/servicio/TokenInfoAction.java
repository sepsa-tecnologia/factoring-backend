/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.servicio;

import com.google.gson.JsonObject;
import fa.gs.utils.misc.Units;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonArrayBuilder;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.io.Serializable;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.ejb.security.UserTokenAuthenticator;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.Text;

/**
 *
 * @author Fabio A. González Sosa
 */
public class TokenInfoAction extends RestControllerActionWithJsonObjectParam {

    private Personas personas;

    public TokenInfoAction() {
        this.personas = Injection.lookup(Personas.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        Input input = processInput(json0);

        if (input.getAuthInfo() == null) {
            throw new IllegalArgumentException("Token invalido");
        }

        return processOutput(input);
    }

    private Input processInput(JsonObject json0) {
        // Token de autenticacion.
        String token = JsonResolver.string(json0, "token");

        UserAuthenticationInfo info;
        try {
            UserTokenAuthenticator authenticator = new UserTokenAuthenticator();
            info = (UserAuthenticationInfo) authenticator.authenticateToken(token);
        } catch (Throwable thr) {
            Errors.dump(System.err, thr);
            info = null;
        }

        Input input = new Input();
        input.setToken(token);
        input.setAuthInfo(info);
        return input;
    }

    private Response processOutput(Input input) {
        // Obtener persona.
        Persona persona = personas.obtenerPorId(input.authInfo.getMUsuario().getIdPersona()).value(null);

        JsonObjectBuilder b0 = JsonObjectBuilder.instance();
        b0.add("id", input.authInfo.getMUsuario().getId());
        b0.add("username", input.authInfo.getMUsuario().getUsuario());

        JsonObjectBuilder b1 = JsonObjectBuilder.instance();
        b1.add("id", Units.execute(-1, () -> persona.getId()));
        b1.add("razon_social", Units.execute("", () -> Text.razonSocial(persona)));
        b1.add("ruc", Units.execute("", () -> Text.ruc(persona)));
        b1.add("personeria", Units.execute("", () -> Text.tipoPersona(persona)));

        JsonArrayBuilder b2 = JsonArrayBuilder.instance();
        for (Perfil p : input.authInfo.getMPerfiles()) {
            JsonObjectBuilder b2_0 = JsonObjectBuilder.instance();
            b2_0.add("id", p.getId());
            b2_0.add("descripcion", p.getDescripcion());
            b2.add(b2_0.build());
        }

        JsonObjectBuilder b3 = JsonObjectBuilder.instance();
        b3.add("token", input.token);
        b3.add("usuario", b0.build());
        b3.add("persona", b1.build());
        b3.add("perfiles", b2.build());

        JsonObject json = b3.build();
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    @Data
    private static class Input implements Serializable {

        String token;
        UserAuthenticationInfo authInfo;
    }

}
