/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fa.gs.misc.collections.Lists;
import fa.gs.misc.fechas.Fechas;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonArrayBuilder;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Calculador_Sepsa;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.PF_FacturaVentaLote_BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.PF_FacturaVentaLote_Sepsa;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculoDesembolsoFacturaInfo;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.utils.Context;
import py.com.sepsa.genisys.rest.utils.ControllersUtils;

/**
 *
 * @author Fabio A. González Sosa
 */
public class CalcularDesembolsoAction extends RestControllerActionWithJsonObjectParam {

    private Facturas facturas;

    private Perfiles perfiles;

    private Personas personas;

    public CalcularDesembolsoAction() {
        this.facturas = Injection.lookup(Facturas.class);
        this.perfiles = Injection.lookup(Perfiles.class);
        this.personas = Injection.lookup(Personas.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        UserAuthenticationInfo auth = Context.getUserInfo();
        Input input = processInput(json0);

        // Usuario proveedor.
        Usuario usuario = auth.getMUsuario();

        // Perfil proveedor.
        Perfil perfilProveedor = perfiles.obtenerPerfil(PerfilEnum.PROVEEDOR);

        // Entidad financiera.
        Persona entidad = personas.obtenerPorId(input.idEntidadFinanciera).value(null);
        if (entidad == null) {
            throw Errors.illegalState("Entidad financiera inválida.");
        }

        // Facturas.
        List<FacturaInfo> facturas = Lists.empty();
        for (Integer idFactura : input.idFacturas) {
            FacturaInfo factura = this.facturas.buscarFactura(idFactura).value(null);
            if (factura == null) {
                throw Errors.illegalState("Identificador de factura '%s' es inválido.", Ids.mask(idFactura));
            }
            facturas.add(factura);
        }

        Collection<CalculoDesembolsoFacturaInfo> calculos = calcularDesembolsos(usuario, perfilProveedor, facturas, entidad, input.fechaAdelanto);
        return ControllersUtils.responseOk(processOutput(calculos));
    }

    static Collection<CalculoDesembolsoFacturaInfo> calcularDesembolsos(Usuario usuario, Perfil perfil, List<FacturaInfo> facturas, Persona entidadFinanciera, Date fechaAdelanto) throws Throwable {
        BancoRio integracionBancoRio = Injection.lookup(BancoRio.class);
        PF_FacturaVentaLote_Sepsa ventaLoteSepsa = Injection.lookup(PF_FacturaVentaLote_Sepsa.class);
        PF_FacturaVentaLote_BancoRio ventaLoteBancoRio = Injection.lookup(PF_FacturaVentaLote_BancoRio.class);

        if (integracionBancoRio.esBancoRio(entidadFinanciera.getId())) {
            return ventaLoteBancoRio.calcularDesembolsos(usuario, perfil, facturas, entidadFinanciera, fechaAdelanto);
        }

        if (true) {
            return ventaLoteSepsa.calcularDesembolsos(usuario, perfil, facturas, entidadFinanciera, fechaAdelanto);
        }

        throw Errors.illegalState("No debe ocurrir.");
    }

    private JsonElement processOutput(Collection<CalculoDesembolsoFacturaInfo> calculos) {
        JsonArrayBuilder ab = JsonArrayBuilder.instance();
        for (CalculoDesembolsoFacturaInfo calculo : calculos) {
            JsonObjectBuilder ob = JsonObjectBuilder.instance();
            ob.add("id_factura", Ids.mask(calculo.getFactura().getId()));
            ob.add("ok", calculo.getOk());
            ob.add("message", calculo.getMessage());
            ob.add("dias_anticipo", calculo.getDiasAnticipo());
            ob.add("porcentaje_descuento", calculo.getPorcentajeDescuento());
            ob.add("monto_descuento", calculo.getMontoDescuento());
            ob.add("monto_desembolso", calculo.getMontoDesembolso());
            if (calculo.getExtras() != null) {
                if (calculo.getExtras() instanceof Calculador_Sepsa.Extras) {
                    Calculador_Sepsa.Extras extras = (Calculador_Sepsa.Extras) calculo.getExtras();
                    ob.add("extras", process(extras));
                }
            }
            ab.add(ob.build());
        }
        return ab.build();
    }

    private JsonElement process(Calculador_Sepsa.Extras extras) {
        JsonObjectBuilder ob = JsonObjectBuilder.instance();
        ob.add("monto_total_comision", extras.getMontoTotalComision());
        ob.add("monto_total_comision_comprador", extras.getMontoTotalComisionComprador());
        ob.add("monto_total_comision_sepsa", extras.getMontoTotalComisionSepsa());
        ob.add("monto_total_comision_entidad_financiera", extras.getMontoTotalComisionEntidadFinanciera());
        ob.add("monto_total_gastos_operativos", extras.getMontoTotalGastosOperativos());
        return ob.build();
    }

    private Input processInput(JsonObject json0) {
        // Identificadores de facturas.
        Collection<Integer> idFacturas = Lists.empty();
        for (JsonElement element : json0.get("id_facturas").getAsJsonArray()) {
            // Id de factura.
            String idFactura0 = element.getAsString();
            if (Assertions.stringNullOrEmpty(idFactura0)) {
                throw Errors.illegalArgument("Identificador de factura no puede ser vacío o nulo.");
            }
            Integer idFactura = Ids.unmask(idFactura0);
            if (Assertions.isNull(idFactura) || idFactura <= 0) {
                throw Errors.illegalArgument("Identifiador de factura inválido.");
            }
            idFacturas.add(idFactura);
        }

        // Identificador de entidad financiera.
        String idEntidad0 = JsonResolver.string(json0, "id_entidad_financiera");
        if (Assertions.stringNullOrEmpty(idEntidad0)) {
            throw Errors.illegalArgument("Identificador de entidad financiera no puede ser vacío o nulo.");
        }
        Integer idEntidad = Ids.unmask(idEntidad0);
        if (Assertions.isNull(idEntidad) || idEntidad <= 0) {
            throw Errors.illegalArgument("Identificador de entidad financiera inválido.");
        }

        // Fecha de desembolso.
        Date fechaAdelanto = Fechas.now();
        if (json0.get("fecha_adelanto") != null) {
            fechaAdelanto = Fechas.fromEpoch(json0.get("fecha_adelanto").getAsLong());
        }

        Input input = new Input();
        input.idFacturas = idFacturas;
        input.idEntidadFinanciera = idEntidad;
        input.fechaAdelanto = fechaAdelanto;
        return input;
    }

    @Data
    private static class Input implements Serializable {

        private Collection<Integer> idFacturas;
        private Integer idEntidadFinanciera;
        private Date fechaAdelanto;
    }

}
