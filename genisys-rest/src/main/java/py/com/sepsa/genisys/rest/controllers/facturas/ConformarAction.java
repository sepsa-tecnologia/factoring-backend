/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.rest.controllers.facturas;

import com.google.gson.JsonObject;
import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonObjectBuilder;
import fa.gs.utils.misc.json.JsonResolver;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.rest.controllers.RestControllerActionWithJsonObjectParam;
import fa.gs.utils.rest.responses.ServiceResponse;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.ws.rs.core.Response;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaConformada;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.rest.utils.Context;

/**
 *
 * @author Fabio A. González Sosa
 */
public class ConformarAction extends RestControllerActionWithJsonObjectParam {

    private Facturas facturas;

    private Notificaciones notificaciones;

    public ConformarAction() {
        this.facturas = Injection.lookup(Facturas.class);
        this.notificaciones = Injection.lookup(Notificaciones.class);
    }

    @Override
    public Response doAction(JsonObject json0) throws Throwable {
        // Extraer parametros de entrada.
        UserAuthenticationInfo auth = Context.getUserInfo();
        Input input = processInput(json0);

        // Obtener factura.
        Result<FacturaInfo> resFactura = facturas.buscarFactura(input.idFactura);
        if (resFactura.isFailure()) {
            throw Errors.illegalArgument("No se pudo determinar la factura.");
        }

        // Factura.
        FacturaInfo factura = resFactura.value();
        if (factura == null) {
            throw Errors.illegalArgument("No se pudo determinar la factura.");
        }

        // Verificar estado conformable de factura.
        if (!facturas.checkFacturaEnEstado(factura, Estados.EstadoFacturaEnum.NO_CONFORMADA)) {
            throw Errors.illegalArgument("El estado actual de la factura no permite su conformación.");
        }

        // Calcular monto conformado.
        BigDecimal monto0 = Numeric.sum(input.montoNotaCredito, input.montoRetencion, input.montoOtros);
        BigDecimal monto1 = factura.getMontoTotal();
        BigDecimal montoTotalConformado = Numeric.sub(monto1, monto0);
        if (Numeric.menorIgual(montoTotalConformado, Numeric.CERO)) {
            throw Errors.illegalState("El monto conformado no puede ser menor o igual a cero (0).");
        }

        // Conformar factura.
        Result<FacturaConformada> resFacturaConformada = facturas.registrarFacturaConformada(input.idFactura, factura.getMontoTotal(), input.montoNotaCredito, input.montoRetencion, input.montoOtros, montoTotalConformado);
        if (resFacturaConformada.isFailure()) {
            throw Errors.illegalState("Ocurrió un error conformando factura.");
        }

        // Cambiar estado de factura.
        Result<Factura> resFactura0 = facturas.cambiarEstado(auth.getMUsuario().getId(), input.getIdFactura(), Estados.EstadoFacturaEnum.CONFORMADA);
        if (resFacturaConformada.isFailure()) {
            throw Errors.illegalState("Ocurrió un error cambiando estado de factura.");
        }

        // Factura (2).
        Factura factura0 = resFactura0.value();

        // Agregar historico.
        Result<HistoricoFactura> resHistoricoFactura = facturas.agregarHistorico(auth.getMUsuario().getId(), factura0, Estados.EstadoFacturaEnum.CONFORMADA, input.observaciones);
        if (resHistoricoFactura.isFailure()) {
            throw Errors.illegalState("Ocurrió un error registrando histórico de factura.");
        }

        // Obtener datos finales.
        resFactura = facturas.buscarFactura(input.getIdFactura());
        factura = resFactura.value();

        // Notificar.
        List<FacturaInfo> facturasOk = Lists.empty();
        facturasOk.add(factura);
        notificaciones.notificarConformacionDeFacturas(facturasOk);

        return processOutput(factura);
    }

    private Input processInput(JsonObject json0) {
        // Id de factura.
        String idFactura0 = JsonResolver.string(json0, "id_factura");
        if (Assertions.stringNullOrEmpty(idFactura0)) {
            throw Errors.illegalArgument("Identificador de factura no puede ser vacío o nulo.");
        }
        Integer idFactura = Ids.unmask(idFactura0);
        if (Assertions.isNull(idFactura) || idFactura <= 0) {
            throw Errors.illegalArgument("Identificador de factura inválido.");
        }

        // Montos de nota de credito, si hubieren.
        BigDecimal montoNotaCredito = JsonResolver.bigdecimal(json0, "monto_nota_credito", Numeric.CERO);
        if (Numeric.menor(montoNotaCredito, Numeric.CERO)) {
            throw Errors.illegalArgument("El monto total por notas de crédito no puede ser menor a cero (0).");
        }

        // Montos de retencion, si hubieren.
        BigDecimal montoRetencion = JsonResolver.bigdecimal(json0, "monto_retencion", Numeric.CERO);
        if (Numeric.menor(montoRetencion, Numeric.CERO)) {
            throw Errors.illegalArgument("El monto total por retenciones fiscales no puede ser menor a cero (0).");
        }

        // Montros otros gastos, si hubieren.
        BigDecimal montoOtros = JsonResolver.bigdecimal(json0, "monto_otros", Numeric.CERO);
        if (Numeric.menor(montoOtros, Numeric.CERO)) {
            throw Errors.illegalArgument("El monto total por otros gastos no puede ser menor a cero (0).");
        }

        // Observaciones, si hubieren.
        String observaciones = JsonResolver.string(json0, "observaciones", "Factura conformada desde API REST.");

        Input input = new Input();
        input.setIdFactura(idFactura);
        input.setMontoNotaCredito(montoNotaCredito);
        input.setMontoRetencion(montoRetencion);
        input.setMontoOtros(montoOtros);
        input.setObservaciones(observaciones);
        return input;
    }

    private Response processOutput(FacturaInfo factura) {
        // Preparar datos de salida.
        JsonObjectBuilder builder = JsonObjectBuilder.instance();
        builder.add("id_factura", Ids.mask(factura.getId()));
        builder.add("fecha_emision", factura.getFechaEmision());
        builder.add("fecha_pago", factura.getFechaPago());
        builder.add("numero", factura.getNumero());
        builder.add("monto", factura.getMontoTotal());
        builder.add("monto_conformado", factura.getMontoTotalConformado());
        builder.add("proveedor", factura.getProveedorRuc());

        JsonObject json = builder.build();
        return ServiceResponse.ok()
                .payload(json)
                .build();
    }

    @Data
    private static class Input implements Serializable {

        Integer idFactura;
        BigDecimal montoNotaCredito;
        BigDecimal montoRetencion;
        BigDecimal montoOtros;
        String observaciones;
    }

}
