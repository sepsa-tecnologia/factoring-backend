/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.utils;

import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Ids;
import java.io.Serializable;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
public class BackingBean implements Serializable {

    protected final AppLogger log;

    private final String beanId;

    public BackingBean() {
        this.beanId = Ids.randomUuid();
        this.log = AppLoggerFactory.web();
    }

    protected AppLogger getLog() {
        return log;
    }

    public String getBeanId() {
        return beanId;
    }

}
