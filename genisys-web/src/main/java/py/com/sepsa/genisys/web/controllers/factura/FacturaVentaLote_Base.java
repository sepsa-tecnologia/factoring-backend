/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.genisys.web.controllers.factura;

import fa.gs.utils.logging.app.AppLogger;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculoDesembolsoFacturaInfo;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.web.controllers.sesion.UsuarioLogueadoMB;

/**
 *
 * @author Fabio A. González Sosa
 */
public class FacturaVentaLote_Base implements Serializable {

    protected AppLogger log;

    @Inject
    protected UsuarioLogueadoMB usuarioLogueado;

    public FacturaVentaLote_Base() {
        this.log = AppLoggerFactory.web();
    }

    protected void clearCalculos(List<CalculoDesembolsoFacturaInfo> calculos, List<FacturaInfo> facturas) {
        calculos.clear();
        for (FacturaInfo factura : facturas) {
            CalculoDesembolsoFacturaInfo calculo = new CalculoDesembolsoFacturaInfo();
            calculo.setOk(false);
            calculo.setMessage("No se pudo realizar el cálculo");
            calculo.setFactura(factura);
            calculo.setConvenio(null);
            calculo.setDiasAnticipo(-1);
            calculo.setPorcentajeDescuento(fa.gs.misc.Numeric.UNO_NEGATIVO);
            calculo.setMontoDescuento(fa.gs.misc.Numeric.UNO_NEGATIVO);
            calculo.setMontoDistribucion(fa.gs.misc.Numeric.UNO_NEGATIVO);
            calculo.setMontoDesembolso(fa.gs.misc.Numeric.UNO_NEGATIVO);
            calculo.setExtras(null);

            // Agregar a datos.
            calculos.add(calculo);
        }
    }

}
