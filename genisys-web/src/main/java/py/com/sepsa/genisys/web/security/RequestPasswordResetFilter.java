/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.security;

import fa.gs.utils.authentication.tokens.TokenExtractor;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.logic.impl.Usuarios;
import py.com.sepsa.genisys.ejb.security.UserTokenAuthenticator;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
public class RequestPasswordResetFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ;
    }

    @Override
    public void destroy() {
        ;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // Contexto de seguridad de Spring.
        SecurityContext securityContext = getSpringSecurityContext(false);

        // Extraer token de autenticacion, si hubiere.
        TokenExtractor.COOKIE_NAME = "APP_SESSION_TOKEN_GENISYS";
        String token = TokenExtractor.fromHttpRequest((HttpServletRequest) request);

        // Verificar que token sea valido (parseable), si hubiere.
        if (!Assertions.stringNullOrEmpty(token) && !UserTokenAuthenticator.tokenIsValid(token)) {
            sendInvalidTokenResponse(request, response, chain);
            return;
        }

        /**
         * Si no existe una sesion activa y se especifica un token de
         * autenticacion valido, generar las credenciales correspondientes e
         * iniciar una nueva sesion.
         */
        if (!Assertions.stringNullOrEmpty(token)) {
            // Autenticar token.
            UserTokenAuthenticator authenticator = new UserTokenAuthenticator();
            Result<AuthenticationInfo> resAuth = authenticator.authenticateUserToken(token);
            if (resAuth.isFailure() || resAuth.value() == null) {
                sendInvalidTokenResponse(request, response, chain);
                return;
            }

            // Contexto de autenticacion.
            AuthenticationInfo authenticationContext = resAuth.value();
            Integer idUsuario = authenticationContext.getUsuario().id().intValue();

            // Validar existencia de usuario
            Usuarios usuarios = Injection.lookup(Usuarios.class);
            Usuario usuario = usuarios.obtenerPorId(idUsuario).value(null);
            String requestUrl = ((HttpServletRequest) request).getRequestURI();
            if (usuario != null && usuario.getEstado().equals('R') && !requestUrl.contains("account.xhtml")) {
                HttpServletResponse httpServletResponse = (HttpServletResponse) response;
                httpServletResponse.sendRedirect("/genisys-web-0.0.1/app/account.xhtml");
                return;
            }
        }

        // Continuar camino normal.
        chain.doFilter(request, response);
    }

    private SecurityContext getSpringSecurityContext(boolean createIfEmpty) {
        SecurityContext ctx = SecurityContextHolder.getContext();
        if (ctx == null && createIfEmpty) {
            ctx = SecurityContextHolder.createEmptyContext();
        }
        return ctx;
    }

    private void sendInvalidTokenResponse(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException {
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "El token de autenticación no es válido o ha sido vulnerado. Por favor, limpie la caché y cookies de su navegador y vuelva a intentar.");
    }

}
