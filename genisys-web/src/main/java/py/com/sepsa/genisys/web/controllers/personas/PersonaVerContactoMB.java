/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.personas;

import fa.gs.utils.jsf.Jsf;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.pojos.DireccionInfo;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ContactoInfo;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "verContacto")
@ViewScoped
public class PersonaVerContactoMB extends BasicViewMB implements Serializable {

    @EJB
    private Personas personas;

    private Persona persona;

    @Getter
    private DireccionInfo direccion;

    @Getter
    private ContactoInfo contacto;

    @PostConstruct
    public void init() {
        // Inicializar persona.
        Integer idPersona = Ids.unmask(Jsf.getRequestParameter("p"));
        this.persona = personas.obtenerPorId(idPersona).value();

        // Direccion de usuario.
        direccion = personas.obtenerDatosDireccion(persona.getId()).value();

        // Contacto de usuario.
        contacto = personas.obtenerDatosContacto(persona.getId()).value();
    }

    public String razonSocial() {
        return Text.razonSocial(persona);
    }

    public String ruc() {
        return Text.ruc(persona);
    }

}
