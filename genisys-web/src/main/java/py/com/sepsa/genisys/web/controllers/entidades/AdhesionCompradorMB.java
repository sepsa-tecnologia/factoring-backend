/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.entidades;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.Todo;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioActividadEconomica;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioCiudad;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioRegion;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoSociedad;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroCompradorPersonaJuridicaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroCompradorPersonaJuridicaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ContactoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DireccionInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.RubroInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.SociedadInfo;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.varios.PersonasMB;
import py.com.sepsa.genisys.web.utils.Html;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "adhesionEntidadComprador")
@ViewScoped
public class AdhesionCompradorMB extends BasicViewMB implements Serializable {

    @Inject
    private PersonasMB personas;

    @Inject
    private TxWrapperWeb tx;

    @Getter
    @Setter
    private Persona entidadSelected;

    //<editor-fold defaultstate="collapsed" desc="Banco Rio">
    @EJB
    private BancoRio integracionBancoRio;

    @Getter
    @Setter
    private FormInputBancoRio formBancoRio;

    @Getter
    @Setter
    private Collection<BancoRioTipoSociedad> bancoRioTiposSociedad;

    @Getter
    @Setter
    private Collection<BancoRioActividadEconomica> bancoRioActividadesEconomicas;

    @Getter
    @Setter
    private Collection<BancoRioCiudad> bancoRioCiudades;
    //</editor-fold>

    @PostConstruct
    public void init() {
        // Inicializar entidad financiera.
        this.entidadSelected = null;
    }

    public boolean entidadEstaSeleccionada() {
        return entidadSelected != null;
    }

    public boolean entidadSeleccionadaEsBancoRio() {
        if (entidadEstaSeleccionada()) {
            return integracionBancoRio.esBancoRio(entidadSelected.getId());
        } else {
            return false;
        }
    }

    public Collection<Persona> getEntidadesSeleccionables() {
        return personas.getEntidadesFinancieras();
    }

    public void onEntidadSelectedChanged(AjaxBehaviorEvent event) {
        try {
            if (entidadSelected != null) {
                // Validar si existen relaciones anteriores.
                Todo.TODO();

                // Verificar tipo de banco.
                if (entidadSeleccionadaEsBancoRio()) {
                    onEntidadSelectedEsBancoRio();
                }
            } else {
                mensajes.addMessageWarning("Advertencia", "Se debe seleccionar una entidad financiera.");
            }
        } catch (Throwable thr) {
            // Error.
            entidadSelected = null;
            log.warning(thr, "Ocurrió un error seleccionando entidad financiera para adhesion.");
            mensajes.addMessageError("Error", "Ocurrió un error seleccionando entidad financiera para adhesion.");
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Adhesion - Banco Rio">
    private void onEntidadSelectedEsBancoRio() {
        // Inicializar contenedor de datos de entrada.
        this.formBancoRio = new FormInputBancoRio();

        // Inicializar datos seleccionables.
        this.bancoRioTiposSociedad = bancoRioTipoSociedadesSeleccionables();
        this.bancoRioActividadesEconomicas = bancoRioActividadesEconomicasSeleccionables();
        this.bancoRioCiudades = bancoRioCiudadesSeleccionables();
    }

    private Collection<BancoRioActividadEconomica> bancoRioActividadesEconomicasSeleccionables() {
        Result<Collection<BancoRioActividadEconomica>> result = integracionBancoRio.obtenerActividadesEconomicas();
        if (result.isFailure()) {
            log.error(result.failure().cause(), result.failure().message());
        }
        return result.value(Lists.empty());
    }

    public String bancoRioActividadEconomicaLabel(BancoRioActividadEconomica item) {
        if (item == null) {
            return Html.selectItemEmpty();
        } else {
            return Html.selectItem(item.getDescripcion(), item.getCodigo());
        }
    }

    private Collection<BancoRioTipoSociedad> bancoRioTipoSociedadesSeleccionables() {
        Result<Collection<BancoRioTipoSociedad>> result = integracionBancoRio.obtenerTiposSociedades();
        if (result.isFailure()) {
            log.error(result.failure().cause(), result.failure().message());
        }
        return result.value(Lists.empty());
    }

    public String bancoRioTipoSociedadLabel(BancoRioTipoSociedad item) {
        if (item == null) {
            return Html.selectItemEmpty();
        } else {
            return Html.selectItem(item.getDescripcion(), item.getSigla());
        }
    }

    private Collection<BancoRioCiudad> bancoRioCiudadesSeleccionables() {
        Result<Collection<BancoRioCiudad>> result = integracionBancoRio.obtenerCiudades();
        if (result.isFailure()) {
            log.error(result.failure().cause(), result.failure().message());
        }
        return result.value(Lists.empty());
    }

    public String bancoRioCiudadLabel(BancoRioCiudad item) {
        if (item == null) {
            return Html.selectItemEmpty();
        } else {
            return Html.selectItem(item.getDescripcion(), item.getCodigo());
        }
    }

    public void onBancoRioCiudadSelected(AjaxBehaviorEvent event) {
        try {
            // Validar relaciones anteriores.
            if (formBancoRio.ciudad != null) {
                // Obtener region correspondiente.
                Result<BancoRioRegion> resRegion = integracionBancoRio.obtenerRegionPorCodigo(formBancoRio.ciudad.getCodigoRegion());
                resRegion.raise();
                formBancoRio.region = resRegion.value();
            } else {
                formBancoRio.region = null;
            }
        } catch (Throwable thr) {
            // Error.
            entidadSelected = null;
            log.warning(thr, "Ocurrió un error seleccionando ciudad.");
            mensajes.addMessageError("Error", "Ocurrió un error seleccionando ciudad.");
        }
    }

    public Collection<BancoRioRegion> bancoRioRegionesSeleccionables() {
        Result<Collection<BancoRioRegion>> result = integracionBancoRio.obtenerRegiones();
        if (result.isFailure()) {
            log.error(result.failure().cause(), result.failure().message());
        }
        return result.value(Lists.empty());
    }

    public String bancoRioRegionLabel(BancoRioRegion item) {
        if (item == null) {
            return Html.selectItemEmpty();
        } else {
            return Html.selectItem(item.getDescripcion(), item.getCodigo());
        }
    }

    private Void bancoRioOnConfirmar() throws Throwable {
        Integer idPersona = usuarioLogueado.getPersona().getId();
        Integer idPerfil = usuarioLogueado.getPerfil().getId();
        Integer idEntidadFinanciera = entidadSelected.getId();

        // Registro de persona fisica.
        if (usuarioLogueado.esPersonaFisica()) {
            // TODO: IMPLEMENTAR.
        }

        // Registro de persona juridica.
        if (usuarioLogueado.esPersonaJuridica()) {
            RegistroCompradorPersonaJuridicaInput input = adaptRegistroCompradorPersonaInput();
            Result<RegistroCompradorPersonaJuridicaOutput> result = integracionBancoRio.registrarComprador(idPersona, idPerfil, idEntidadFinanciera, input);
            result.raise();
        }

        // Ok.
        return null;
    }

    private RegistroCompradorPersonaJuridicaInput adaptRegistroCompradorPersonaInput() {
        // Datos de proveedor.
        String razonSocial0 = usuarioLogueado.getPersona().getPjRazonSocial();

        // Datos de documento.
        DocumentoInfo documento0 = integracionBancoRio.resolveDocumento(usuarioLogueado.getPersona());

        // Datos de sociedad.
        SociedadInfo sociedad0 = new SociedadInfo();
        sociedad0.setTipoSociedad(formBancoRio.tipoSociedad);
        sociedad0.setFechaConstitucion(formBancoRio.fechaConstitucion);

        // Datos de direccion.
        DireccionInfo direccion0 = new DireccionInfo();
        direccion0.setCalle1(usuarioLogueado.getDireccion().getCalle1());
        direccion0.setCalle2(usuarioLogueado.getDireccion().getCalle2());
        direccion0.setReferencia("Y");
        direccion0.setNroCalle(usuarioLogueado.getDireccion().getNroCalle());
        direccion0.setCalle3(usuarioLogueado.getDireccion().getCalle3());
        direccion0.setBarrio(usuarioLogueado.getDireccion().getBarrio());
        direccion0.setCiudad(formBancoRio.ciudad);
        direccion0.setRegion(formBancoRio.region);

        // Datos de contacto.
        ContactoInfo contacto0 = new ContactoInfo();
        contacto0.setNroLineaBaja(usuarioLogueado.getContacto().getNroLineaBaja());
        contacto0.setNroCelular(usuarioLogueado.getContacto().getNroCelular());
        contacto0.setEmail(usuarioLogueado.getContacto().getEmail());

        // Datos de rubro.
        // TODO: INCLUIR RUBRO.
        RubroInfo rubro0 = new RubroInfo();
        rubro0.setDescripcion("");

        // Datos finales.
        RegistroCompradorPersonaJuridicaInput instance = new RegistroCompradorPersonaJuridicaInput();
        instance.setDocumento(documento0);
        instance.setRazonSocial(razonSocial0);
        instance.setSociedad(sociedad0);
        instance.setDireccion(direccion0);
        instance.setContacto(contacto0);
        instance.setActividadEconomica(formBancoRio.actividadEconomica);
        instance.setRubro(rubro0);
        instance.setFacturacionPromedio(formBancoRio.facturacionPromedio);
        return instance;
    }
    //</editor-fold>

    public void confirmar() {
        // Validar datos de entrada.
        String err = validarInput();
        if (err != null) {
            mensajes.addMessageError("Error", err);
            return;
        }

        // Confirmar datos.
        Result<?> result = onConfirmar();
        if (result.isFailure()) {
            log.error(result.failure().cause(), "Ocurrio un error confirmando adhesion a entidad financiera");
            mensajes.showGlobalDialogMessage(result.failure().cause().getMessage());
        } else {
            mensajes.showGlobalDialogMessage("Solicitud Registrada exitosamente");
            entidadSelected = null;
        }
    }

    private Result<Void> onConfirmar() {
        Result<Void> result;

        try {
            // Confirmar registro - Banco Rio.
            if (entidadSeleccionadaEsBancoRio()) {
                Result<Void> txResult = tx.execute(this::bancoRioOnConfirmar);
                if (txResult.isFailure()) {
                    throw txResult.failure().cause();
                }
            }

            result = Results.ok()
                    .nullable(true)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .build();
        }

        return result;
    }

    private String validarInput() {
        return null;
    }

    @Data
    public static class FormInputBancoRio implements Serializable {

        private BancoRioActividadEconomica actividadEconomica;
        private BancoRioTipoSociedad tipoSociedad;
        private Date fechaConstitucion;
        private BigDecimal facturacionPromedio;
        private BancoRioCiudad ciudad;
        private BancoRioRegion region;
    }

}
