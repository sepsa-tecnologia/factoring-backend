/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.varios;

import fa.gs.misc.Assertions;
import fa.gs.result.utils.Failure;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.omnifaces.util.Ajax;
import py.com.sepsa.genisys.web.controllers.BasicMB;
import py.com.sepsa.genisys.web.utils.CustomValidator;
import py.com.sepsa.genisys.web.utils.MessageBuilder;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "messages")
@SessionScoped
public class MessagesMB extends BasicMB {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Identificador para el cuadro de dialogo global de mensajes.
     */
    public static final String GLOBAL_DIALOG_ID = "global_dialog";

    /**
     * Mensaje a desplegar en el cuadro de dialogo global como mensaje
     * principal.
     */
    private String globalDialogContent;

    /**
     * Coleccion de mensajes de segundo nivel en importancia, cuya finalidad si
     * hubieren es la de dar una explicacion un poco mas detallada de los
     * eventos producidos.
     */
    private List<String> globalDialogSubContents;

    /**
     * Cadena que representa un identificador de seguimiento cuyo proposito es
     * identificar problemas y secuencias de procedimiento dentro de los
     * registros de diagnostico del sistema (log).
     *
     * Este identificador concuerda con los identificadores de seguimiento
     * utilizados directamente por el subsistema de logueo.
     */
    private String globalDialogContextTrackingId;

    /**
     * Interface que representa la implementacion de una accion a ejecutar
     * cuando se presiona el boton "Ok" del cuadro de dialogo global.
     */
    private GlobalDialogClickListener globalDialogOkClickListener;
    // </editor-fold>

    @PostConstruct
    public void init() {
        globalDialogContent = "";
        globalDialogSubContents = new LinkedList<>();
        globalDialogContextTrackingId = null;
        globalDialogOkClickListener = null;
    }

    //<editor-fold defaultstate="collapsed" desc="Manejo de mensajes informativos en formularios">
    /**
     * Efectua una validacion, utilizando un validador correspondiente, y en
     * caso de falla genera un mensaje dentro de la vista indicando el error.
     *
     * @param validator Validador.
     * @param value Objeto a validar.
     * @return True, si la validación produjo errores. False en caso contrario.
     */
    public <T> boolean displayMessageOnFail(CustomValidator<T> validator, T value) {
        String error = validator.validate(value);
        if (error != null) {
            addMessageError("Error", error);
            log.error("Fallo de validación, validator = %s, entity = %s, error = %s", validator.getClass().getCanonicalName(), value.getClass().getCanonicalName(), error);
            return true;
        }
        return false;
    }

    /**
     * Agrega un mensaje de éxito en la vista.
     *
     * @param summary Resumen del mensaje.
     * @param detail Detalle del mensaje.
     */
    public void addMessageSuccess(String summary, String detail) {
        // En JSF no existe el tipo de mensaje SUCCESS, por lo tanto se enmascara con un tipo raramente utilizado como el FATAL.
        addMessage(FacesMessage.SEVERITY_FATAL, summary, detail);
    }

    /**
     * Agrega un mensaje de información en la vista.
     *
     * @param summary Resumen del mensaje.
     * @param detail Detalle del mensaje.
     */
    public void addMessageInfo(String summary, String detail) {
        addMessage(FacesMessage.SEVERITY_INFO, summary, detail);
    }

    /**
     * Agrega un mensaje de advertencia en la vista.
     *
     * @param summary Resumen del mensaje.
     * @param detail Detalle del mensaje.
     */
    public void addMessageWarning(String summary, String detail) {
        addMessage(FacesMessage.SEVERITY_WARN, summary, detail);
    }

    /**
     * Agrega un mensaje de error en la vista.
     *
     * @param summary Resumen del mensaje.
     * @param detail Detalle del mensaje.
     */
    public void addMessageError(String summary, String detail) {
        addMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
    }

    public void addMessageError(String summary, Failure failure) {
        String detail = getDetail(failure);
        addMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
    }

    /**
     * Agrega un mensaje de un evento fatal en la vista.
     *
     * @param summary Resumen del mensaje.
     * @param detail Detalle del mensaje.
     */
    public void addMessageFatal(String summary, String detail) {
        addMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
    }

    private String getDetail(Failure failure) {
        if (!Assertions.stringNullOrEmpty(failure.message())) {
            return failure.message();
        }

        if (failure.cause() != null) {
            if (!Assertions.stringNullOrEmpty(failure.cause().getMessage())) {
                return failure.cause().getMessage();
            }

            if (!Assertions.stringNullOrEmpty(failure.cause().getLocalizedMessage())) {
                return failure.cause().getLocalizedMessage();
            }
        }

        return "ERROR";
    }

    /**
     * Agrega un mensaje en la vista.
     *
     * @param severity Severidad del mensaje.
     * @param summary Resumen del mensaje.
     * @param detail Detalle del mensaje.
     */
    public void addMessage(FacesMessage.Severity severity, String summary, String detail) {
        FacesMessage msg = new MessageBuilder().severity(severity).summary(summary).detail(detail).build();
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Determina la severidad que representa a la mayoria de los mensajes
     * existentes para su despliegue en la vista. Es decir, indica cual
     * FacesMessage.Severity aparece mas veces dentro de la coleccion de
     * mensajes.
     *
     * @return Severidad mayoritatia.
     */
    private FacesMessage.Severity getDominantSeverity() {
        Map<FacesMessage.Severity, Integer> counts = new HashMap<>();
        for (FacesMessage message : FacesContext.getCurrentInstance().getMessageList()) {
            FacesMessage.Severity severity = message.getSeverity();
            if (!counts.containsKey(severity)) {
                counts.put(severity, 0);
            }
            counts.put(severity, counts.get(severity) + 1);
        }
        Map.Entry<FacesMessage.Severity, Integer> max = null;
        for (Map.Entry<FacesMessage.Severity, Integer> entry : counts.entrySet()) {
            if (max == null) {
                max = entry;
            } else if (entry.getValue() > max.getValue()) {
                max = entry;
            }
        }
        FacesMessage.Severity severity = max.getKey();
        return (severity != null) ? severity : FacesMessage.SEVERITY_INFO;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Manejo de mensajes informativos en cuadro de dialogo">
    /**
     * Establece un mensaje personalizado a ser desplegado en el cuadro de
     * dialogo global.
     *
     * @param ok Listener que ejecuta codigo antes de cerrar el cuadro de
     * dialogo.
     * @param fmt Formato de mensaje.
     * @param args Parametros para formato de mensaje, si hubieren.
     */
    public void addGlobalDialogMessage(String fmt, Object... args) {
        String msg = String.format(fmt, args);
        globalDialogSubContents.add(msg);
    }

    public void showGlobalDialogMessage() {
        showGlobalDialogMessage("Advertencia");
    }

    /**
     * Establece un mensaje personalizado a ser desplegado en el cuadro de
     * dialogo global.
     *
     * @param fmt Formato de mensaje.
     * @param args Parametros para formato de mensaje, si hubieren.
     */
    public void showGlobalDialogMessage(String fmt, Object... args) {
        showGlobalDialogMessage(null, fmt, args);
    }

    /**
     * Establece un mensaje personalizado a ser desplegado en el cuadro de
     * dialogo global.
     *
     * @param ok Listener que ejecuta codigo antes de cerrar el cuadro de
     * dialogo.
     * @param fmt Formato de mensaje.
     * @param args Parametros para formato de mensaje, si hubieren.
     */
    public void showGlobalDialogMessage(GlobalDialogClickListener ok, String fmt, Object... args) {
        globalDialogContent = String.format(fmt, args);
        globalDialogOkClickListener = ok;
        globalDialogContextTrackingId = null;
        Ajax.update(String.format("%s:%s_form", GLOBAL_DIALOG_ID, GLOBAL_DIALOG_ID));
        Prime.showSepsaDialog(GLOBAL_DIALOG_ID);
    }

    /**
     * Realiza acciones una vez que el cliente presiona el boton "Ok" del cuadro
     * de dialogo global.
     */
    public void onGlobalDialogOkClick() {
        try {
            // Ejecutar listener, si hubiere.
            if (globalDialogOkClickListener != null) {
                globalDialogOkClickListener.onClick();
            }
        } finally {
            // Limpiar mensaje principal y secundarios.
            globalDialogContent = "";
            globalDialogSubContents.clear();
            globalDialogContextTrackingId = null;

            // Cerrar cuadro de dialogo.
            Prime.closeSepsaDialog(GLOBAL_DIALOG_ID);
        }
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getter y Setters">
    /**
     * Determina la clase CSS para el cuadro de mensajes dependiendo del tipo de
     * severidad para la mayoria de los mensajes existentes. Esto solo sirve
     * para le componente sepsa:messages.
     *
     * @return Clase CSS.
     */
    public String getAlertStyleClass() {
        FacesMessage.Severity severity = getDominantSeverity();
        if (severity.equals(FacesMessage.SEVERITY_INFO)) {
            return "alert-info";
        } else if (severity.equals(FacesMessage.SEVERITY_WARN)) {
            return "alert-warning";
        } else if (severity.equals(FacesMessage.SEVERITY_ERROR)) {
            return "alert-danger";
        } else if (severity.equals(FacesMessage.SEVERITY_FATAL)) {
            return "alert-success";
        } else {
            return "alert";
        }
    }

    public String getGlobalDialogId() {
        return GLOBAL_DIALOG_ID;
    }

    public String getGlobalDialogContent() {
        return globalDialogContent;
    }

    public void setGlobalDialogContent(String currentDialogMessage) {
        this.globalDialogContent = currentDialogMessage;
    }

    public List<String> getGlobalDialogSubContents() {
        return globalDialogSubContents;
    }

    public void setGlobalDialogSubContents(List<String> globalDialogSubContents) {
        this.globalDialogSubContents = globalDialogSubContents;
    }

    public String getGlobalDialogContextTrackingId() {
        return globalDialogContextTrackingId;
    }

    public void setGlobalDialogContextTrackingId(String globalDialogContextTrackingId) {
        this.globalDialogContextTrackingId = globalDialogContextTrackingId;
    }
    // </editor-fold>

    /**
     * Interface para implementacion de acciones a ejecutar cuando se presiona
     * alguno de los botones del cuadro de dialogo global.
     */
    public static interface GlobalDialogClickListener {

        public void onClick();

    }
}
