/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.notificaciones;

import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Arrays;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.util.Ajax;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoring;
import py.com.sepsa.genisys.ejb.entities.notificacion.Plantilla;
import py.com.sepsa.genisys.ejb.enums.TipoPlantilla;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.logic.impl.Plantillas;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Alexander Triana Ríos.
 */
@Named(value = "notificacionesMB")
@ViewScoped
public class NotificacionesMB extends BasicViewMB implements Serializable {

    /**
     * Bean que administra las notificaciones.
     */
    @EJB
    private Notificaciones notificaciones;

    /**
     * Bean que administra las plantillas de notificaciones disponibles.
     */
    @EJB
    private Plantillas plantillas;

    /**
     * Lista de notificaciones.
     */
    @Inject
    private py.com.sepsa.genisys.web.controllers.notificaciones.ListaMB lista;

    @Inject
    private TxWrapperWeb tx;

    /**
     * Opciones de filtro para notificaciones, por asunto.
     */
    private String[] notificacionAsuntos;

    /**
     * Opciones de filtro para notificaciones, por confirmacion de lectura.
     */
    private String[] notificacionLeida;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        notificacionAsuntos = getNotificacionAsuntoOptions();
        notificacionLeida = getNotificacionLeidaOptions();
    }

    private String[] getNotificacionAsuntoOptions() {
        Collection<String> asuntos = Lists.empty();
        for (TipoPlantilla tipoPlantilla : TipoPlantilla.values()) {
            String asunto = tipoPlantilla.getAsunto();
            asuntos.add(asunto);
        }
        return Arrays.unwrap(asuntos, String.class);
    }

    private String[] getNotificacionLeidaOptions() {
        return new String[]{"SI", "NO"};
    }

    /**
     * Método que se encarga de extraer el Asunto de los emails enviados
     * referente a las facturas.
     */
    public String getAsunto(Plantilla plantilla) {
        String cuerpoPlantilla = plantilla.getCuerpo();
        for (TipoPlantilla tipoPlantilla : TipoPlantilla.values()) {
            if (Objects.equals(cuerpoPlantilla, tipoPlantilla.toString())) {
                return tipoPlantilla.getAsunto();
            }
        }
        return "N/D";
    }

    /**
     * Método que devuelve el texto de la tabla Leido de una manera más
     * amigable.
     *
     * @param notificaionEmailFactoring
     * @return
     */
    public String getLeidoHumanText(NotificacionEmailFactoring notificaionEmailFactoring) {
        boolean leido = Objects.equals(notificaionEmailFactoring.getLeido().toString().toLowerCase(), "s");
        String texto = leido ? "SI" : "NO";
        return texto;
    }

    /**
     * Método que devuelve como cadena de texto los valores de la tabla Leído.
     *
     * @param notificaionEmailFactoring parámetro de tipo notificacion.
     * @return
     */
    public String getLeidoLabel(NotificacionEmailFactoring notificaionEmailFactoring) {
        boolean leido = Objects.equals(notificaionEmailFactoring.getLeido().toString().toLowerCase(), "s");
        String color = leido ? "label-success" : "label-danger";
        String texto = leido ? "SI" : "NO";
        String html = "\n"
                + " <span class=\"label " + color + " app_text_truncated\">\n"
                + "     " + texto + "\n"
                + " </span>";
        return html;
    }

    /**
     * Prepara la vista para la eliminacion de varias notificaciones
     * seleccionadas, si hubieren.
     *
     * @throws Throwable Error producido.
     */
    public void initEliminarNotificaciones() {
        tx.execute(this::initEliminarNotificaciones0);
    }

    private Result<Void> initEliminarNotificaciones0() {
        Result<Void> result;

        try {
            if (lista.hasSelection()) {
                List<NotificacionEmailFactoring> notificaciones = lista.getMultiSelectedItem();
                for (NotificacionEmailFactoring notificacion : notificaciones) {
                    facades.getNotificacionEmailFactoringFacade().eliminar(notificacion.getId());
                }
                mensajes.showGlobalDialogMessage("Notificaciones eliminadas exitosamente");
            } else {
                mensajes.showGlobalDialogMessage("Debe seleccionar al menos una notificación de la lista");
            }

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).build();
        } finally {
            updateViewListaDeNotificaciones();
        }

        return result;
    }

    /**
     * Elimina una notificacion individual desde la vista.
     *
     * @param idNotificacionEmailFactoring Identificador de notificacion.
     * @throws Throwable Error producido.
     */
    public void initEliminarNotificacion(final Integer idNotificacionEmailFactoring) {
        tx.execute(() -> {
            return this.initEliminarNotificacion0(idNotificacionEmailFactoring);
        });
    }

    private Result<Void> initEliminarNotificacion0(Integer idNotificacionEmailFactoring) {
        Result<Void> result;

        try {
            facades.getNotificacionEmailFactoringFacade().eliminar(idNotificacionEmailFactoring);
            mensajes.showGlobalDialogMessage("Notificación eliminada exitosamente");
            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            log.warning(thr, "Ocurrió un error eliminando notificacion id='%s'", idNotificacionEmailFactoring);
            result = Results.ko().cause(thr).build();
        } finally {
            updateViewListaDeNotificaciones();
        }

        return result;
    }

    /**
     * Meétodo que permite la visualiazación del contenido de una notificación
     * de la lista.
     *
     * @param idNotificacion Identificador de la notificación a ser leída.
     */
    public void initVerContenidoNotificacion(Integer idNotificacion) {
        String id = utils.maskInteger(idNotificacion);
        Prime.redirect(String.format("/app/abm/notificaciones/fragments/mail.xhtml?faces-redirect=true&id=%s", id));
    }

    /**
     * Actualiza la lista de facturas en la vista.
     */
    public void recargarListaNotificaciones() {
        lista.clearSelections();
        updateViewListaDeNotificaciones();
        lista.reload();
        updateViewListaDeNotificaciones();
    }

    //<editor-fold defaultstate="collapsed" desc="Filtros">
    /**
     * Aplica los filtros sobre la lista de notificaciones.
     */
    public void filtrarLista() {
        // Actualizar la vista.
        lista.filter();
        updateViewFiltrosForm();
    }

    /**
     * Limpia los valores establecidos en los campos de filtro.
     */
    public void limpiarFiltroForm() {
        CustomFilter filtro = lista.getFiltro();
        if (!filtro.isEnabled()) {
            filtro.clearFilterValues();
            updateViewFiltrosForm();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Actualizaciones de la Vista">
    private void updateViewFiltrosForm() {
        Ajax.update("filtro_notificaciones_panel:filtro_notificaciones_form");
        Prime.showBlockUi("bui", lista.getFiltro().isEnabled());
        updateViewListaDeNotificaciones();
    }

    private void updateViewListaDeNotificaciones() {
        lista.clearSelections();
        Ajax.update("tabla_notificaciones_panel:tabla_notificaciones_form:tabla_notificaciones");
        Prime.executeJs("$.fn.matchHeight._update();");
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public String getBtnFiltrarText() {
        if (lista.getFiltro().isEnabled()) {
            return "Deshacer Filtro";
        } else {
            return "Filtrar Datos";
        }
    }

    public Notificaciones getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(Notificaciones notificaciones) {
        this.notificaciones = notificaciones;
    }

    public Plantillas getPlantillas() {
        return plantillas;
    }

    public void setPlantillas(Plantillas plantillas) {
        this.plantillas = plantillas;
    }

    public ListaMB getLista() {
        return lista;
    }

    public void setLista(ListaMB lista) {
        this.lista = lista;
    }

    public String[] getNotificacionAsuntos() {
        return notificacionAsuntos;
    }

    public void setNotificacionAsuntos(String[] notificacionAsuntos) {
        this.notificacionAsuntos = notificacionAsuntos;
    }

    public String[] getNotificacionLeida() {
        return notificacionLeida;
    }

    public void setNotificacionLeida(String[] notificacionLeida) {
        this.notificacionLeida = notificacionLeida;
    }
    //</editor-fold>

}
