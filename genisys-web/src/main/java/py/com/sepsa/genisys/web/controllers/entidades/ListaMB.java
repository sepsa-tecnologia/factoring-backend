/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.entidades;

import fa.gs.criteria.query.Order;
import fa.gs.misc.Assertions;
import fa.gs.utils.database.query.expressions.literals.SQLStringLiterals;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.Dependent;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.info.PersonaEntidadInfo;
import py.com.sepsa.genisys.ejb.logic.impl.Entidades;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.CustomLazyDataModel;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class ListaMB extends py.com.sepsa.genisys.web.controllers.ListaMB<PersonaEntidadInfo> implements Serializable {

    @Override
    protected CustomLazyDataModel<PersonaEntidadInfo> buildItemsLoader() {
        Integer idPersona = usuarioLogueado.getPersona().getId();
        Integer idPerfil = usuarioLogueado.getPerfil().getId();
        return new EntidadesLazyList(idPersona, idPerfil);
    }

    @Override
    protected CustomFilter buildItemsFilter() {
        return new EntidadesListFilter();
    }

    /**
     * Clase que encapsula la carga progresiva de facturas desde la BD.
     */
    private class EntidadesLazyList extends CustomLazyDataModel<PersonaEntidadInfo> {

        private Entidades entidades;

        private final Integer idPersona;

        private final Integer idPerfil;

        /**
         * Constructor.
         */
        public EntidadesLazyList(Integer idPersona, Integer idPerfil) {
            super(null);
            this.entidades = Injection.lookup(Entidades.class);
            this.idPersona = idPersona;
            this.idPerfil = idPerfil;
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<PersonaEntidadInfo> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            // Ordenacion automatica.
            //sorts.put("P.razon_social", Order.ASCENDING);
            //sorts.put("P.ruc", Order.ASCENDING);

            // Busqueda de registros.
            List<PersonaEntidadInfo> data = entidades.selectPersonaEntidades(idPersona, idPerfil, limit, offset, filter, sorts);
            Integer total = entidades.countPersonaEntidades(idPersona, idPerfil, filter);
            setRowCount(total);
            return data;
        }

    }

    /**
     * Clase que encapsula los campos y operadores de filtro aplicables a la
     * lista principal de facturas.
     */
    @Data
    public class EntidadesListFilter extends CustomFilter {

        private String razonSocial;
        private String ruc;
        private Entidades.EstadoAdhesionEntidad estado;

        @Override
        final public void clearFilterValues() {
            this.razonSocial = "";
            this.ruc = "";
            this.estado = null;
        }

        @Override
        public Map<String, Object> getFilters() {
            Map<String, Object> filters = new HashMap<>();

            // Filtro de razon social.
            if (!Assertions.stringNullOrEmpty(razonSocial)) {
                filters.put("razonSocial", Strings.format(" pj.razon_social ilike '%s' ", SQLStringLiterals.likefy(razonSocial)));
            }

            // Filtro de ruc.
            if (!Assertions.stringNullOrEmpty(ruc)) {
                filters.put("ruc", Strings.format(" pj.ruc ilike '%s' ", SQLStringLiterals.likefy(ruc)));
            }

            // Filtro de estado.
            if (estado != null) {
                filters.put("estado", Strings.format(" pe.estado = '%s' ", estado.codigo()));
            }

            return filters;
        }

    }

}
