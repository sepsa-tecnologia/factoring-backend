/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.entidades;

import fa.gs.utils.logging.app.AppLogger;
import java.io.Serializable;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
public class AdhesionProveedor_Base implements Serializable {

    protected AppLogger log;

    public AdhesionProveedor_Base() {
        this.log = AppLoggerFactory.web();
    }
}
