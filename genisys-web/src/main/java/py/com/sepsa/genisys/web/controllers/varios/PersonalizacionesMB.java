/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.varios;

import fa.gs.result.simple.Result;
import java.awt.Color;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import py.com.sepsa.genisys.ejb.entities.factoring.Personalizacion;
import py.com.sepsa.genisys.ejb.enums.TipoPersonalizacionEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Personalizaciones;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.web.utils.BackingBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "bPersonalizaciones")
@RequestScoped
public class PersonalizacionesMB extends BackingBean {

    @Inject
    private Personalizaciones personalizaciones;

    @Getter
    private Personalizacion cssPrimaryNavbarColor;

    @Getter
    private Personalizacion cssPrimaryNavbarTextColor;

    @Getter
    private Personalizacion cssSecondaryNavbarColor;

    @Getter
    private Personalizacion cssSecondaryNavbarTextColor;

    @Getter
    private Personalizacion cssBtnColor;

    @Getter
    private Personalizacion cssBtnTextColor;

    @Getter
    private Personalizacion cssPanelColor;

    @Getter
    private Personalizacion cssPanelTextColor;

    @Getter
    private Personalizacion cssCheckboxColor;

    @Getter
    private Personalizacion imgLogo;

    @Getter
    private Personalizacion urlLogout;

    @Getter
    private Personalizacion contactoTelefono;

    @Getter
    private Personalizacion contactoEmail;

    @PostConstruct
    public void init() {
        // TODO: CORREGIR FORMA DE OBTENER ESTE PARAMETRO.
        Integer idEntidad = extractIdEntidadFinanciera();
        cssPrimaryNavbarColor = obtener(idEntidad, TipoPersonalizacionEnum.CSS_PRIMARY_NAVBAR_COLOR);
        cssPrimaryNavbarTextColor = obtener(idEntidad, TipoPersonalizacionEnum.CSS_PRIMARY_NAVBAR_TEXT_COLOR);
        cssSecondaryNavbarColor = obtener(idEntidad, TipoPersonalizacionEnum.CSS_SECONDARY_NAVBAR_COLOR);
        cssSecondaryNavbarTextColor = obtener(idEntidad, TipoPersonalizacionEnum.CSS_SECONDARY_NAVBAR_TEXT_COLOR);
        cssBtnColor = obtener(idEntidad, TipoPersonalizacionEnum.CSS_BUTTON_COLOR);
        cssBtnTextColor = obtener(idEntidad, TipoPersonalizacionEnum.CSS_BUTTON_TEXT_COLOR);
        cssPanelColor = obtener(idEntidad, TipoPersonalizacionEnum.CSS_PANEL_COLOR);
        cssPanelTextColor = obtener(idEntidad, TipoPersonalizacionEnum.CSS_PANEL_TEXT_COLOR);
        cssCheckboxColor = obtener(idEntidad, TipoPersonalizacionEnum.CSS_CHECKBOX_COLOR);
        imgLogo = obtener(idEntidad, TipoPersonalizacionEnum.IMG_LOGO);
        urlLogout = obtener(idEntidad, TipoPersonalizacionEnum.URL_LOGOUT);
        contactoTelefono = obtener(idEntidad, TipoPersonalizacionEnum.CONTACTO_TELEFONO);
        contactoEmail = obtener(idEntidad, TipoPersonalizacionEnum.CONTACTO_EMAIL);
    }

    public String getIdEntidadMasked() {
        Integer idEntidad = extractIdEntidadFinanciera();
        return Ids.mask(idEntidad);
    }

    private Integer extractIdEntidadFinanciera() {
        // TODO: IMPLEMENTAR DE FORMA CORRECTA LA OBTENCION DE ESTE DATO.
        return 0;
    }

    private Personalizacion obtener(Integer idEntidadFinanciera, TipoPersonalizacionEnum tipo) {
        Personalizacion personalizacion = null;
        if (idEntidadFinanciera != null) {
            Result<Personalizacion> result = personalizaciones.obtenerPorTipo(idEntidadFinanciera, tipo);
            if (result.isFailure()) {
                personalizacion = null;
            } else {
                personalizacion = result.value();
            }
        }

        if (personalizacion == null) {
            personalizacion = new Personalizacion();
            personalizacion.setTipo(tipo.codigo());
            personalizacion.setValor(tipo.fallback());
        }

        return personalizacion;
    }

    public String ligthen(String rgbHex, int factor) {
        Color color = Color.decode(rgbHex);
        double percentage = factor / 100.0;
        int r = (int) Math.round(Math.min(255.0, color.getRed() + 255.0 * percentage));
        int g = (int) Math.round(Math.min(255.0, color.getGreen() + 255.0 * percentage));
        int b = (int) Math.round(Math.min(255.0, color.getBlue() + 255.0 * percentage));
        return String.format("#%02x%02x%02x", r, g, b);
    }

}
