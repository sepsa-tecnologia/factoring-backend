/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.notificaciones;

import fa.gs.criteria.facades.AbstractFacade;
import fa.gs.criteria.query.Operators;
import fa.gs.criteria.query.Order;
import fa.gs.criteria.query.QueryCriteria;
import fa.gs.utils.misc.Assertions;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.enterprise.context.Dependent;
import py.com.sepsa.genisys.ejb.entities.info.ContactoEmail;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoring;
import py.com.sepsa.genisys.ejb.enums.TipoPlantilla;
import py.com.sepsa.genisys.ejb.facades.notificacion.NotificacionEmailFactoringFacade;
import py.com.sepsa.genisys.ejb.utils.Criterias;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.CustomLazyDataModel;

/**
 *
 * @author Alexander Triana Ríos
 */
@Dependent
public class ListaMB extends py.com.sepsa.genisys.web.controllers.ListaMB<NotificacionEmailFactoring> implements Serializable {

    @Override
    protected CustomLazyDataModel<NotificacionEmailFactoring> buildItemsLoader() {
        NotificacionEmailFactoringFacade facade = facades.getNotificacionEmailFactoringFacade();
        return new NotificacionEmailFactoringFacadeList(facade);
    }

    @Override
    protected CustomFilter buildItemsFilter() {
        return new NotificacionEmailFactoringListFilter();
    }

    /**
     * Clase que encapsula la carga progresiva de notificaciones de la BD.
     */
    private class NotificacionEmailFactoringFacadeList extends CustomLazyDataModel<NotificacionEmailFactoring> {

        /**
         * Constructor
         *
         * @param facade Facade de notificaciones.
         */
        public NotificacionEmailFactoringFacadeList(AbstractFacade<NotificacionEmailFactoring> facade) {
            super(facade);
        }

        @Override
        public List<NotificacionEmailFactoring> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            // Campos de ordenacion.
            if (sorts.isEmpty()) {
                sorts.put("id", Order.DESCENDING);
            }

            // Filtrar por destinatario si no es administrador.
            if (!usuarioLogueado.esAdministrador()) {
                Collection<ContactoEmail> contactos = usuarioLogueado.getContactosEmail();
                if (!Assertions.isNullOrEmpty(contactos)) {
                    // Mostrar solo notificaciones destinadas a usuario logueado.
                    Integer[] idContactos = contactos.stream()
                            .map(c -> c.getIdContacto())
                            .distinct()
                            .toArray(Integer[]::new);
                    filter.put(String.format("%s$%s", "idContacto", Operators.IN), idContactos);
                } else {
                    // No mostrar ninguna notificacion.
                    filter.put(String.format("%s$%s", "idContacto", Operators.EQUALS), -1);
                }
            }

            //Busqueda de registros.
            QueryCriteria criteria = Criterias.instance(limit, offset, sorts, filter);
            List<NotificacionEmailFactoring> data = facade.find(criteria);
            setRowCount(facade.count(criteria));
            return data;
        }
    }

    public class NotificacionEmailFactoringListFilter extends CustomFilter {

        //<editor-fold defaultstate="collapsed" desc="Atributos">
        private Integer idEmail;
        private String asunto;
        private String leido;
        private Date fechaInsercionDesde;
        private Date fechaInsercionHasta;
        //</editor-fold>

        /**
         * Constructor
         */
        public NotificacionEmailFactoringListFilter() {
            clearFilterValues();
        }

        /**
         * Limpia los valores de filtro posiblemente establecidos previamente.
         */
        @Override
        final public void clearFilterValues() {
            idEmail = null;
            asunto = null;
            leido = null;
            fechaInsercionDesde = null;
            fechaInsercionHasta = null;;
        }

        /**
         * Obtiene los valores de filtro correspondientes.
         *
         * @return filtros otganizados en pares (nombre filtro, valores).
         */
        @Override
        public Map<String, Object> getFilters() {
            Map<String, Object> filters = new HashMap<>();

            // Filtro por Id de Email.
            if (idEmail != null) {
                filters.put("id", idEmail);
            }

            // Filtro por asunto de plantilla.
            if (asunto != null && !asunto.isEmpty()) {
                TipoPlantilla tipoPlantilla = TipoPlantilla.fromAsunto(asunto);
                if (tipoPlantilla != null) {
                    filters.put("idPlantilla.cuerpo", tipoPlantilla.getCuerpo());
                }
            }

            // Filtro por notificacion leida.
            if (leido != null && !leido.isEmpty()) {
                if (Objects.equals(leido, "SI")) {
                    filters.put("leido", 's');
                } else if (Objects.equals(leido, "NO")) {
                    filters.put("leido", 'n');
                }
            }

            // Filtro por fecha de insercion desde.
            if (fechaInsercionDesde != null) {
                filters.put("fechaInsercion$geq", fechaInsercionDesde);
            }

            // Filtro por fecha de insercion hasta.
            if (!Assertions.isNull(fechaInsercionHasta)) {
                Calendar cHora = Calendar.getInstance();
                cHora.set(Calendar.HOUR_OF_DAY, 23);
                cHora.set(Calendar.MINUTE, 59);
                cHora.set(Calendar.SECOND, 59);

                filters.put("fechaInsercion$leq", Fechas.combinarFechaHora(fechaInsercionHasta, cHora.getTime()));
            }

            return filters;
        }

        //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
        public Integer getIdEmail() {
            return idEmail;
        }

        public void setIdEmail(Integer idEmail) {
            this.idEmail = idEmail;
        }

        public String getAsunto() {
            return asunto;
        }

        public void setAsunto(String asunto) {
            this.asunto = asunto;
        }

        public Date getFechaInsercionDesde() {
            return fechaInsercionDesde;
        }

        public void setFechaInsercionDesde(Date fechaInsercionDesde) {
            this.fechaInsercionDesde = fechaInsercionDesde;
        }

        public Date getFechaInsercionHasta() {
            return fechaInsercionHasta;
        }

        public void setFechaInsercionHasta(Date fechaInsercionHasta) {
            this.fechaInsercionHasta = fechaInsercionHasta;
        }

        public String getLeido() {
            return leido;
        }

        public void setLeido(String leido) {
            this.leido = leido;
        }
        //</editor-fold>

    }
}
