/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.factura;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.Units;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.misc.text.StringBuilder2;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ValidationException;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Ajax;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.factoring.Archivo;
import py.com.sepsa.genisys.ejb.entities.factoring.ConfigCargo;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaConformada;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.entities.factoring.LineaCredito;
import py.com.sepsa.genisys.ejb.entities.factoring.Lote;
import py.com.sepsa.genisys.ejb.entities.factoring.Transaccion;
import py.com.sepsa.genisys.ejb.entities.info.CuentaEntidadFinanciera;
import py.com.sepsa.genisys.ejb.entities.info.EntidadFinanciera;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.TipoArchivoEnum;
import py.com.sepsa.genisys.ejb.enums.TipoCargoEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Archivos;
import py.com.sepsa.genisys.ejb.logic.impl.Cuentas;
import py.com.sepsa.genisys.ejb.logic.impl.Estados.EstadoFacturaEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Errnos;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.archivo.CacheArchivos;
import py.com.sepsa.genisys.web.controllers.calculadora.CalculadoraInput;
import py.com.sepsa.genisys.web.controllers.factura.operaciones.ConformacionFacturaMB;
import py.com.sepsa.genisys.web.controllers.varios.CalculadoraMB;
import py.com.sepsa.genisys.web.controllers.varios.PersonasMB;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "facturas")
@ViewScoped
public class FacturaMB extends BasicViewMB implements Serializable {

    /**
     * Bean que se encarga de emitir notificaciones a la aplicación de
     * notificaciones.
     */
    @EJB
    private Notificaciones notificaciones;

    @EJB
    private Facturas facturas;

    @Inject
    private TxWrapperWeb tx;

    //<editor-fold defaultstate="collapsed" desc="Conformacion de Facturas">
    /**
     * Bean que se encarga de desacoplar algunas operaciones relacionadas a la
     * conformacion de facturas.
     */
    @Inject
    private ConformacionFacturaMB conformacionFacturas;

    /**
     * Objeto que contiene los datos a ser completados para la conformacion
     * detallada de una factura.
     */
    private ConformacionFacturaMB.Input conformacionInput;
    //</editor-fold>

    /**
     * Bean que se encarga de las operaciones de obtencion, filtrado y
     * ordenadmiento de facturas.
     */
    @Inject
    private py.com.sepsa.genisys.web.controllers.factura.ListaMB lista;

    /**
     * Persona seleccionada de la lista de personas relacionadas al usuario.
     */
    private Persona personaSelected;

    /**
     * Cadena que representa un comentario del usuario al momento de cambiar el
     * estado de una factura.
     */
    private String comentarioCambioEstadoFactura;

    /**
     * Lista de archivos adjuntos para mostrar en vista principal de facturas.
     */
    private List<Archivo> archivosAdjuntos;

    /**
     * Lista de cuentas del comprador que recibe una comision por una factura
     * vendida.
     */
    private List<CuentaEntidadFinanciera> cuentasComprador;

    /**
     * Cuenta de comprador seleccionada en la vista para el registro de pago de
     * comision.
     */
    private CuentaEntidadFinanciera cuentaCompradorSelected;

    /**
     * Bandera que indica que no se debe tener en cuenta la informacion de
     * desembolso de comision al comprador para el registro de transacciones de
     * pago de factura.
     */
    private Boolean omitirDesembolsoComprador;

    /**
     * Monto que debe ser desembolsado al comprador durante el pago de una
     * factura conformada.
     */
    private BigDecimal montoDesembolsoComprador;

    /**
     * Bandera que indica que no se debe tener en cuenta la informacion de
     * desembolso de comision a la entidad financiera para el registro de
     * transacciones de pago de factura.
     */
    private Boolean omitirDesembolsoEntidad;

    /**
     * Monto que debe ser desembolsado a la entidad financiera (a si misma)
     * durante el pago de una factura conformada.
     */
    private BigDecimal montoDesembolsoEntidad;

    /**
     * Lista de cuentas de SEPSA que recibe una comision por una factura
     * vendida.
     */
    private List<CuentaEntidadFinanciera> cuentasSepsa;

    /**
     * Cuenta de SEPSA seleccionada en la vista para el registro de pago de
     * comision.
     */
    private CuentaEntidadFinanciera cuentaSepsaSelected;

    /**
     * Bandera que indica que no se debe tener en cuenta la informacion de
     * desembolso de comision a SEPSA para el registro de transacciones de pago
     * de factura.
     */
    private Boolean omitirDesembolsoSepsa;

    /**
     * Monto que debe ser desembolsado a Sepsa durante el pago de una factura
     * conformada.
     */
    private BigDecimal montoDesembolsoSepsa;

    /**
     * Lista de cuentas del proveedor que recibe el credito por una factura
     * vendida.
     */
    private List<CuentaEntidadFinanciera> cuentasProveedor;

    /**
     * Cuenta de proveedor seleccionada en la vista para el registro de pago de
     * factura.
     */
    private CuentaEntidadFinanciera cuentaProveedorSelected;

    /**
     * Bandera que indica que no se debe tener en cuenta la informacion de
     * desembolso de comision al proveedor para el registro de transacciones de
     * pago de factura.
     */
    private Boolean omitirDesembolsoProveedor;

    /**
     * Monto que debe ser desembolsado al proveedor durante el pago de una
     * factura conformada.
     */
    private BigDecimal montoDesembolsoProveedor;

    /**
     * Lista de cuentas de la entidad financiera que acredita una factura
     * vendida.
     */
    private List<CuentaEntidadFinanciera> cuentasEntidadFinanciera;

    /**
     * Cuenta seleccionada en la vista para el registro de desembolso de factura
     * al proveedor.
     */
    private CuentaEntidadFinanciera cuentaEntidadFinancieraDesembolsoProveedorSelected;

    /**
     * Cuenta seleccionada en la vista para el registro de pago de comision a
     * comprador.
     */
    private CuentaEntidadFinanciera cuentaEntidadFinancieraDesembolsoCompradorSelected;

    /**
     * Cuenta seleccionada en la vista para el registro de pago de comision a la
     * empresa prestadora del servicio de factoring (SEPSA).
     */
    private CuentaEntidadFinanciera cuentaEntidadFinancieraDesembolsoSepsaSelected;

    /**
     * Objeto cache para archivos a subir.
     */
    private CacheArchivos cacheArchivos;

    /**
     * Factura seleccionada desde la vista, la cual puede ser utilizada
     * directamente para efectuar oepraciones sobre la misma o servir como dato
     * para despliegue de informaciones.
     */
    private FacturaInfo facturaSelected;

    /**
     * Parametro de peticion que indica el identificador de factura. Utilizado
     * en la vista de desembolso.
     */
    private String idFacturaParam;

    /**
     * Identificador de factura obtenido desde el parametro de peticion en la
     * vista de desembolso.
     */
    private Integer idFactura;

    /**
     * Bean de datos para calculadora.
     */
    @Inject
    @Getter
    @Setter
    private CalculadoraMB calculadora;

    /**
     * Bean para acceso a datos de relaciones con otros tipos de persona
     * (compradores, proveedores, entidades financieras).
     */
    @Inject
    private PersonasMB personas;

    @EJB
    private Personas personas0;

    @EJB
    private Cuentas cuentas;
    // </editor-fold>

    /**
     * Inicializa el managed bean.
     */
    @PostConstruct
    public void init() {
        personaSelected = null;

        cuentasEntidadFinanciera = new LinkedList<>();

        cuentasProveedor = new LinkedList<>();
        omitirDesembolsoProveedor = false;
        montoDesembolsoProveedor = Numeric.CERO;

        cuentasSepsa = new LinkedList<>();
        omitirDesembolsoSepsa = false;
        montoDesembolsoSepsa = Numeric.CERO;

        cuentasComprador = new LinkedList<>();
        omitirDesembolsoComprador = false;
        montoDesembolsoComprador = Numeric.CERO;

        facturaSelected = null;

        /**
         * Inicializar la cache de archivos en caso de que el bean deba ser
         * utilizado por otras vistas. Ejemplo: vista de desembolso y solicitud
         * de desembolso.
         */
        cacheArchivos = new CacheArchivos();
    }

    /**
     * Metodo de inicializacion para la vista de registro de desembolso de una
     * factura.
     */
    public void init2() {
        // Obtener valor original de identificador de factura.
        idFactura = utils.unmaskInteger(idFacturaParam);

        // Preparar estado del bean para realizar un desembolso sobre una factura valida.
        FacturaInfo factura = facturas.buscarFactura(idFactura).value(null);
        if (factura != null && facturas.checkFacturaEnEstado(factura, EstadoFacturaEnum.EN_ESTUDIO_POR_ENTIDAD)) {
            // Establecer referencia a factura desembolsable.
            facturaSelected = factura;

            // Inicializar variables de cambio de estado de facturas.
            initCambioEstadoFacturas();

            // Inicializar variables a utilizar en calculos auxiliares.
            Integer idConvenio = factura.getConvenioId();
            Integer diasAnticipados = factura.getDiasAnticipo();
            BigDecimal montoComision = factura.getMontoTotalDescuento();
            BigDecimal porcentajeTotalComision = factura.getPorcentajeDescuento();
            BigDecimal porcentajeTotalDiarioComision = Numeric.div(porcentajeTotalComision, Numeric.wrap(diasAnticipados));

            // Calculo auxiliar de montos de desembolso por cada parte involucrada.
            montoDesembolsoProveedor = factura.getMontoTotalDesembolso();
            montoDesembolsoComprador = calcularMontoParteDesembolso(idConvenio, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_COMPRADOR, montoComision, porcentajeTotalDiarioComision);
            montoDesembolsoEntidad = calcularMontoParteDesembolso(idConvenio, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_ENTIDAD_FINANCIERA, montoComision, porcentajeTotalDiarioComision);
            montoDesembolsoSepsa = calcularMontoParteDesembolso(idConvenio, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_SEPSA, montoComision, porcentajeTotalDiarioComision);

            // Inicializacion de cuentas habilitadas para las transacciones.
            cuentasSepsa = Collections.emptyList();
            cuentaSepsaSelected = null;
            cuentaCompradorSelected = null;
            cuentasComprador = cuentas.obtenerCuentasFinancierasDisponibles(factura.getCompradorId());
            cuentaProveedorSelected = null;
            cuentasProveedor = cuentas.obtenerCuentasFinancierasDisponibles(factura.getProveedorId());
            cuentaEntidadFinancieraDesembolsoCompradorSelected = null;
            cuentaEntidadFinancieraDesembolsoProveedorSelected = null;
            cuentaEntidadFinancieraDesembolsoSepsaSelected = null;
            cuentasEntidadFinanciera = cuentas.obtenerCuentasFinancierasDisponibles(factura.getEntidadFinancieraId());
        } else {
            // No hay una factura valida.
            facturaSelected = null;
        }

        // Actualizar vista.
        updateViewDesembolsoFactura();
    }

    /**
     * Prepara la vista para desplegar la calculadora de intereses utilizando la
     * factura seleccionada y los datos del proveedor actual. Se debe elegir una
     * entidad financiera para calcular los cargos a aplicar.
     *
     * @param factura Factura a utilizar dentro de la calculadora.
     */
    public void initCalculadoraInteresForProveedor(FacturaInfo factura) {
        // Datos iniciales.
        Persona comprador = personas0.obtenerPorId(factura.getCompradorId()).value(null);
        Persona proveedor = personas0.obtenerPorId(factura.getProveedorId()).value(null);
        BigDecimal monto = (factura.getMontoTotalConformado() != null) ? factura.getMontoTotalConformado() : factura.getMontoTotal();

        // Inicializar datos de calculadora.
        CalculadoraInput input = calculadora.initCalculadoraInput();
        input.setComprador(comprador);
        input.setProveedor(proveedor);
        input.setEntidadFinanciera(null);
        input.setCompradores(Arrays.asList(comprador));
        input.setProveedores(Arrays.asList(proveedor));
        input.setMontoFactura(monto);
        input.setFechaPagoFactura(factura.getFechaPago());
        input.setCanLimpiarComprador(false);
        input.setCanLimpiarProveedor(false);
        input.setCanLimpiarEntidadFinanciera(true);
        input.setCanEditFechaPagoFactura(false);
        input.setCanEditMontoFactura(false);

        // Mutar estado de calculadora.
        calculadora.setInput(input);
        calculadora.setOutput(null);

        // Desplegar y actualizar vista de calculadora.
        Ajax.update("calculadora_interes_factura_dialog:calculadora_form");
        Prime.showSepsaDialog("calculadora_interes_factura_dialog");
    }

    /**
     * Prepara la vista para la carga manual de facturas.
     */
    public void initCargarFactura() {
        // Redireccionar a vista de alta de factura.
        Prime.redirect("app/abm/factura/alta.xhtml?redirect-true");
    }

    /**
     * Prepara la vista para la carga batch de facturas.
     */
    public void initCargarFacturas() {
        // Redireccionar a vista de alta de factura.
        Prime.redirect("app/abm/factura/subir.xhtml?redirect-true");
    }

    /**
     * Prepara la vista para la edicion manual de facturas.
     */
    public void initEditarFactura() {
        // Controlar que se haya seleccionado solo una factura.
        facturaSelected = checkSoloUnaFacturaSeleccionada("Sólo se puede editar una factura a la vez.");
        if (facturaSelected != null) {
            // Controlar que la factura sea una factura manual.
            if (facturaSelected.getEsFacturaEdi()) {
                mensajes.showGlobalDialogMessage("Sólo se pueden editar facturas que hayan sido cargadas manualmente");
                return;
            }

            // Controlar que la factura pueda ser editada.
            if (!facturas.checkFacturaEnEstado(facturaSelected, EstadoFacturaEnum.NO_CONFORMADA)) {
                mensajes.showGlobalDialogMessage("Sólo se pueden editar facturas en estado \"%s\"", estados.getHumanText(EstadoFacturaEnum.NO_CONFORMADA));
                return;
            }

            // Redireccionar a vista de edicion de factura.
            Prime.redirect(String.format("app/abm/factura/editar.xhtml?id=%s&redirect-true", utils.maskInteger(facturaSelected.getId())));
        }
    }

    /**
     * Verifica si una factura paso por el proceso de conformacion, esto es,
     * verificar directamente si el registro de factura posee un registro de
     * factura conformada.
     *
     * @param factura Factura.
     * @return {@code true} si la factura ya fue conformada, caso contrario
     * {@code false}.
     */
    public boolean facturaTieneConformacion(FacturaInfo factura) {
        if (factura == null) {
            return false;
        } else {
            return factura.getMontoTotalConformado() != null;
        }
    }

    /**
     * Verifica que al menos una factura este seleccionada en la vista.
     *
     * @param msg Mensaje a desplegar en caso de que se haya seleccionado mas de
     * una factura.
     * @return Factura seleccionada.
     */
    private List<FacturaInfo> checkFacturaSeleccionada() {
        List<FacturaInfo> selection = checkSeleccion(lista, "Debe seleccionar al menos una factura de la lista");
        return selection;
    }

    /**
     * Verifica que una y solo una factura este seleccionada en la vista.
     *
     * @param msg Mensaje a deseplegar en caso de que se haya seleccionado mas
     * de una factura.
     * @return Factura seleccionada.
     */
    private FacturaInfo checkSoloUnaFacturaSeleccionada(String msg) {
        FacturaInfo selection = checkSeleccionUnica(lista, "Debe seleccionar al menos una factura de la lista", msg);
        return selection;
    }

    //<editor-fold defaultstate="collapsed" desc="Venta de Facturas">
    /**
     * Prepara la vista para la solicitud de desembolso de una factura (venta) a
     * una entidad financiera.
     */
    public void initSolicitudDesembolsoFacturas() {
        // Controlar que se haya seleccionado al menos una factura.
        List<FacturaInfo> facturasSelected = checkFacturaSeleccionada();
        if (facturasSelected != null) {
            // Controlar que las facturas puedan ser vendidas.
            if (!facturas.checkFacturasEnEstado(facturasSelected, EstadoFacturaEnum.CONFORMADA)) {
                mensajes.showGlobalDialogMessage("Sólo se puede solicitar el desembolso de facturas en estado \"%s\"", estados.getHumanText(EstadoFacturaEnum.CONFORMADA));
                return;
            }

            Result<Lote> result = tx.execute(() -> initSolicitudDesembolsoFacturas0(facturasSelected));
            if (result.isFailure()) {
                // Error.
                mensajes.showGlobalDialogMessage("Ocurrió un error generando lote de facturas para seguimiento de desembolso.");
            } else {
                // Redireccionar a vista de venta de lote.
                Lote lote = result.value();
                Prime.redirect(String.format("app/abm/factura/venta.xhtml?lote=%s&faces-redirect=true", utils.maskInteger(lote.getId())));
            }
        }
    }

    private Lote initSolicitudDesembolsoFacturas0(List<FacturaInfo> facturas0) throws Throwable {
        // Crear lote.
        Result<Lote> resLote = facturas.crearLoteFacturas();
        resLote.raise();

        // Lote creado.
        Lote lote = resLote.value();

        // Agregar facturas a lote.
        Result<Void> resAgregarALote = facturas.agregarALoteFacturas(lote, facturas0);
        resAgregarALote.raise();

        return lote;
    }

    @Deprecated
    public void initVenderFactura() {
        // Controlar que se haya seleccionado solo una factura.
        facturaSelected = checkSoloUnaFacturaSeleccionada("Sólo se puede solicitar el desembolso de una factura a la vez.");
        if (facturaSelected != null) {
            // Controlar que la factura pueda ser vendida.
            if (!facturas.checkFacturaEnEstado(facturaSelected, EstadoFacturaEnum.CONFORMADA)) {
                mensajes.showGlobalDialogMessage("Sólo se puede solicitar el desembolso de facturas en estado \"%s\"", estados.getHumanText(EstadoFacturaEnum.CONFORMADA));
                return;
            }

        }
    }

    /**
     * Adjunta un archivo a una entidad dada. No confundir entidad con
     * {@link EntidadFinanciera}.
     *
     * @param idEntidad Identificador de entidad.
     * @param tipoEntidad Descriptor de tipo de entidad.
     * @param cacheArchivo Archivo en cache.
     * @param tipoArchivo Tipo de archivo.
     * @return Resultado de la operacion.
     */
    private Result<Archivo> adjuntarArchivo(Integer idEntidad, Archivos.TipoEntidadArchivoAdjuntoEnum tipoEntidad, Archivos.CacheArchivo cacheArchivo, TipoArchivoEnum tipoArchivo) {
        Result<Archivo> result;
        try {
            Result<Archivo> resArchivo = beans.getArchivos().adjuntarArchivo(idEntidad, tipoEntidad, usuarioLogueado.getUsuario(), usuarioLogueado.getPerfil(), cacheArchivo, TipoArchivoEnum.GENERICO);
            resArchivo.raise();

            result = Results.ok().value(resArchivo.value()).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error adjuntando archivo '%s' a entidad '%s' id='%d'", cacheArchivo.getNombre(), tipoEntidad.descripcion(), idEntidad).build();
        }
        return result;
    }

    /**
     * Adjunta los archivos en cache a la factura seleccionada durante la
     * operacion actual.
     *
     * @param cache Coleccion de archivos en cache.
     */
    private void adjuntarArchivosAFactura(FacturaInfo factura, CacheArchivos cache) throws Throwable {
        for (Archivos.CacheArchivo cacheArchivo : cache.getCache()) {
            try {
                log.debug("adjuntando archivo '%s' a factura id='%s'", cacheArchivo.getNombre(), factura.getId());
                Result<Archivo> resArchivo = adjuntarArchivo(factura.getId(), Archivos.TipoEntidadArchivoAdjuntoEnum.FACTURA, cacheArchivo, TipoArchivoEnum.GENERICO);
                resArchivo.raise();
            } catch (Throwable thr) {
                mensajes.addMessageWarning("Advertencia", String.format("No se pudo adjuntar el archivo '%s'.", cacheArchivo.getNombre()));
                throw thr;
            }
        }
    }

    /**
     * Prepara la vista para el rechazo de facturas conformadas por parte del
     * proveedor.
     */
    public void initRechazoFacturasPorProveedor() {
        // Controlar si se selecciono alguna factura.
        if (lista.hasSelection()) {
            // Verificar que todas las facturas esten en el mismo estado previo.
            if (facturas.checkFacturasEnEstado(lista.getMultiSelectedItem(), EstadoFacturaEnum.CONFORMADA)) {
                // Inicializar variables de cambio de estado de factura.
                initCambioEstadoFacturas();

                // Mostrar cuadro de dialogo para rechazo de facturas.
                Prime.showSepsaDialog("rechazo_factura_por_proveedor_dialog");
                updateViewRechazoFacturasPorProveedorDialog();
            } else {
                mensajes.showGlobalDialogMessage("Sólo se pueden rechazar facturas previamente conformadas");
            }
        } else {
            mensajes.showGlobalDialogMessage("Debe seleccionar al menos una factura de la lista");
        }
    }

    /**
     * Cambia el estado de una o varias facturas seleccionadas a
     * {@link EstadoFacturaEnum#RECHAZADA_POR_PROVEEDOR} y realiza operaciones
     * complementarias.
     */
    public void rechazarFacturasPorProveedor() throws Throwable {
        tx.execute(this::rechazarFacturasPorProveedor0);
    }

    private Result<Void> rechazarFacturasPorProveedor0() {
        Result<Void> result;

        try {
            // Procesar rechazo para facturas seleccionadas.
            List<FacturaInfo> facturas = lista.getMultiSelectedItem();
            for (FacturaInfo factura : facturas) {
                // Cambiar estado de factura.
                log.info("cambiando estado de factura id='%s'", factura.getId());
                Result<Factura> resFactura = cambiarEstadoFactura(factura.getId(), EstadoFacturaEnum.RECHAZADA_POR_PROVEEDOR, comentarioCambioEstadoFactura);
                resFactura.raise();
            }

            // Notificar rechazo de facturas conformadas.
            log.info("emitiendo notificaciones");
            notificaciones.notificarRechazoDeFacturasPorProveedor(facturas);

            // Actualizar vista.
            Prime.closeSepsaDialog("rechazo_factura_por_proveedor_dialog");
            mensajes.showGlobalDialogMessage("¡Facturas rechazadas exitosamente!");
            recargarListaFacturas();

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            mensajes.showGlobalDialogMessage("Ocurrió un error rechanzando las facturas seleccionadas. Por favor vuelva a intentar.");
            result = Results.ko().cause(thr).message("Ocurrió un error rechanzando las facturas seleccionadas. Por favor vuelva a intentar.").build();
        } finally {
            // Actualizar vista.
            updateViewRechazoFacturasPorProveedorDialog();
        }

        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Pago de Facturas">
    /**
     * Prepara la vista para el cobro de una factura por parte de una entidad
     * financiera a un comprador.
     */
    public void initRegistroCobroFacturas() {
        // Controlar si se selecciono alguna factura.
        if (lista.hasSelection()) {
            // Verificar que todas las facturas esten en el mismo estado previo.
            if (facturas.checkFacturasEnEstado(lista.getMultiSelectedItem(), EstadoFacturaEnum.PAGADA)) {
                // Inicializar variables de cambio de estado de facturas.
                initCambioEstadoFacturas();

                // Mostrar cuadro de dialogo confirmacion.
                updateViewCobroFacturaDialog();
                Prime.showSepsaDialog("cobro_factura_dialog");
            } else {
                mensajes.showGlobalDialogMessage("Sólo se pueden registrar el cobro de facturas en estado \"%s\"", estados.getHumanText(EstadoFacturaEnum.PAGADA));
            }
        } else {
            mensajes.showGlobalDialogMessage("Debe seleccionar al menos una factura de la lista");
        }
    }

    /**
     * Cambia el estado de una o varias facturas seleccionadas a
     * {@link EstadoFacturaEnum#COBRADA} y realiza operaciones complementarias.
     */
    public void registrarCobroFacturas() {
        tx.execute(this::registrarCobroFacturas0);
    }

    private Result<Void> registrarCobroFacturas0() {
        Result<Void> result;

        try {
            // Procesar cobro para facturas seleccionadas.
            List<FacturaInfo> facturas = lista.getMultiSelectedItem();
            for (FacturaInfo factura : facturas) {
                // Cambiar estado de factura.
                log.info("cambiando estado de factura id='%s'", factura.getId());
                Result<Factura> resFactura = cambiarEstadoFactura(factura.getId(), EstadoFacturaEnum.COBRADA, comentarioCambioEstadoFactura);
                resFactura.raise();
            }

            // Notificar rechazo de facturas conformadas.
            log.info("emitiendo notificaciones");
            notificaciones.notificarCobroFacturas(facturas);

            // Actualizar vista.
            Prime.closeSepsaDialog("cobro_factura_dialog");
            mensajes.showGlobalDialogMessage("¡Facturas cobradas exitosamente!");
            recargarListaFacturas();

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            mensajes.showGlobalDialogMessage("Ocurrió un error registrando cobro de las facturas seleccionadas. Por favor vuelva a intentar.");
            result = Results.ko().cause(thr).message("Ocurrió un error registrando cobro de las facturas seleccionadas. Por favor vuelva a intentar.").build();
        } finally {
            // Actualizar vista.
            updateViewRechazoFacturasPorProveedorDialog();
        }

        return result;
    }
    //</editor-fold>

    /**
     * Prepara la vista para el desembolso de una factura por parte de una
     * entidad financiera.
     */
    public void initRegistroDesembolsoFactura() {
        // Controlar que se haya seleccionado solo una factura.
        facturaSelected = checkSoloUnaFacturaSeleccionada("Sólo se puede registrar el desembolso de una factura a la vez.");
        if (facturaSelected != null) {
            // Controlar que la factura pueda ser desembolsada.
            if (!facturas.checkFacturaEnEstado(facturaSelected, EstadoFacturaEnum.EN_ESTUDIO_POR_ENTIDAD)) {
                mensajes.showGlobalDialogMessage("Sólo se puede registrar el desembolso de una factura previamente en estado \"%s\"", estados.getHumanText(EstadoFacturaEnum.EN_ESTUDIO_POR_ENTIDAD));
                return;
            }

            // Redireccionar a vista de desembolso de factura.
            Prime.redirect(String.format("app/abm/factura/desembolso.xhtml?factura=%s&faces-redirect=true", utils.maskInteger(facturaSelected.getId())));
        }
    }

    /**
     * Calcula el monto que corresponde a un cargo en particular luego de
     * efectuar el desembolso de factura.
     *
     * @param idConvenio Convenio utilizado.
     * @param tipoCargo Tipo de cargo.
     * @param montoComision Monto total descontado a factura.
     * @param porcentajeTotalDiarioComision Porcentaje diario aplicado sobre
     * descuento.
     * @return Monto correspondiente a cargo.
     */
    private BigDecimal calcularMontoParteDesembolso(Integer idConvenio, TipoCargoEnum tipoCargo, BigDecimal montoComision, BigDecimal porcentajeTotalDiarioComision) {
        // Obtenemos el porcentaje relacionado al cargo indicado.
        ConfigCargo cargo = facades.getConfigCargoFacade().findForConvenio(idConvenio, beans.getTipoCargos().obtenerTipoCargo(tipoCargo).getId());
        BigDecimal porcentajeCargo = (cargo == null) ? Numeric.CERO : cargo.getValor();

        /**
         * Calculamos el peso del cargo configurado para conocer el total que le
         * corresponde a cada cargo del total.
         *
         * Este calculo permite obtener un numero entre 0 y 1 que representa el
         * peso de un cargo dentro de los cargos totales aplicados. Por ejemplo,
         * 0.5 indica que el 50% del monto de la comision le corresponde a
         * sepsa/entidad/comprador.
         */
        BigDecimal porcentajeParte = Numeric.CERO;
        if (Numeric.mayor(porcentajeTotalDiarioComision, Numeric.CERO)) {
            porcentajeParte = Numeric.div(porcentajeCargo, porcentajeTotalDiarioComision);
        }

        // Obtenemos el monto correspondiente al peso del cargo.
        BigDecimal montoParte = Numeric.mul(montoComision, porcentajeParte);
        montoParte = Numeric.round(montoParte);

        log.debug("cargo %s, valor %s, parte %s, monto %s", tipoCargo, porcentajeCargo, Numeric.round(Numeric.mul(porcentajeParte, Numeric.CIEN), 2), montoParte);
        return montoParte;
    }

    /**
     * Prepara la vista para la aceptar facturas para su desembolso por parte de
     * una entidad financiera.
     */
    public void initAnalisisFactura() {
        // Controlar si se selecciono alguna factura.
        if (lista.hasSelection()) {
            // Verificar que todas las facturas esten en el mismo estado previo.
            if (facturas.checkFacturasEnEstado(lista.getMultiSelectedItem(), EstadoFacturaEnum.VENDIDA)) {
                // Inicializar variables de cambio de estado de facturas.
                initCambioEstadoFacturas();

                // Mostrar cuadro de dialogo para aceptacion de facturas para analisis.
                Prime.showSepsaDialog("analisis_facturas_dialog");
                updateViewAnalisisFacturasDialog();
            } else {
                mensajes.showGlobalDialogMessage("Sólo se pueden aceptar facturas en estado \"%s\" para el análisis de factibilidad de desembolso", estados.getHumanText(EstadoFacturaEnum.VENDIDA));
            }
        } else {
            mensajes.showGlobalDialogMessage("Debe seleccionar al menos una factura de la lista");
        }
    }

    /**
     * Inicializa las variables comunes utilizadas durante una operacion de
     * cambio de estado para facturas.
     */
    private void initCambioEstadoFacturas() {
        comentarioCambioEstadoFactura = "";
    }

    /**
     * Cambia el estado de una factura dada.
     *
     * @return Coleccion de facturas que cambiaron de estado.
     */
    private Result<Factura> cambiarEstadoFactura(Integer idFactura, EstadoFacturaEnum nuevoEstado, String comentario) {
        // Cambio de estado.
        Result<Factura> resCambioEstado = facturas.cambiarEstado(usuarioLogueado.getUsuario().getId(), idFactura, nuevoEstado);
        if (resCambioEstado.isFailure()) {
            return resCambioEstado;
        }

        // Registro de historico.
        Factura factura0 = resCambioEstado.value();
        Result<HistoricoFactura> resHistorico = facturas.agregarHistorico(usuarioLogueado.getUsuario().getId(), factura0, nuevoEstado, comentario);
        if (resHistorico.isFailure()) {
            return Results.ko()
                    .cause(resHistorico.failure().cause())
                    .build();
        } else {
            return Results.ok()
                    .value(factura0)
                    .build();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Conformacion de Factura">
    /**
     * Prepara la vista para la conformacion de una factura a un proveedor.
     */
    public void initConformarFactura() {
        // Controlar que se haya seleccionado solo una factura.
        facturaSelected = checkSoloUnaFacturaSeleccionada("Sólo se puede conformar una factura a la vez.");
        if (facturaSelected != null) {
            // Controlar que la factura pueda ser conformada.
            if (!facturas.checkFacturaEnEstado(facturaSelected, EstadoFacturaEnum.NO_CONFORMADA, EstadoFacturaEnum.CONFORMACION_SOLICITADA)) {
                mensajes.showGlobalDialogMessage("Sólo se pueden conformar facturas en estado \"%s\" o \"%s\"", estados.getHumanText(EstadoFacturaEnum.NO_CONFORMADA), estados.getHumanText(EstadoFacturaEnum.CONFORMACION_SOLICITADA));
                return;
            }

            // Inicializar variables de cambio de estado de factura.
            initCambioEstadoFacturas();

            // Inicializar variables a utilizar durante la conformacion.
            conformacionInput = conformacionFacturas.nuevoInputConformacionFactura(facturaSelected.getId());

            // Desplegar vista de conformacion.
            updateViewConformacionFacturaDialog();
            Prime.showSepsaDialog("conformacion_factura_dialog");
        }
    }

    /**
     * Prepara la vista para la conformacion de una o mas facturas de manera
     * directa.
     */
    public void initConformarFacturas() {
        // Controlar si se selecciono alguna factura.
        if (lista.hasSelection()) {
            // Verificar que todas las facturas esten en el mismo estado previo.
            if (facturas.checkFacturasEnEstado(lista.getMultiSelectedItem(), EstadoFacturaEnum.NO_CONFORMADA, EstadoFacturaEnum.CONFORMACION_SOLICITADA)) {
                // Inicializar variables de cambio de estado de facturas.
                initCambioEstadoFacturas();

                // Mostrar cuadro de dialogo para rechazo de facturas.
                Prime.showSepsaDialog("conformacion_directa_factura_dialog");
                updateViewConformacionDirectaFacturasDialog();
            } else {
                mensajes.showGlobalDialogMessage("Sólo se pueden conformar facturas en estado \"%s\" o \"%s\"", estados.getHumanText(EstadoFacturaEnum.NO_CONFORMADA), estados.getHumanText(EstadoFacturaEnum.CONFORMACION_SOLICITADA));
            }
        } else {
            mensajes.showGlobalDialogMessage("Debe seleccionar al menos una factura de la lista");
        }
    }

    /**
     * Prepara la vista para la conformacion de una o mas facturas de manera
     * directa.
     */
    public void initSolicitudConformacionFacturas() {
        // Controlar si se selecciono alguna factura.
        if (lista.hasSelection()) {
            // Verificar que todas las facturas esten en el mismo estado previo.
            if (facturas.checkFacturasEnEstado(lista.getMultiSelectedItem(), EstadoFacturaEnum.NO_CONFORMADA)) {
                // Inicializar variables de cambio de estado de facturas.
                initCambioEstadoFacturas();

                // Mostrar cuadro de dialogo para rechazo de facturas.
                Prime.showSepsaDialog("solicitud_conformacion_facturas_dialog");
                updateViewSolicitudConformacionFacturasDialog();
            } else {
                mensajes.showGlobalDialogMessage("Sólo se puede solicitar la conformación de facturas en estado \"%s\"", estados.getHumanText(EstadoFacturaEnum.NO_CONFORMADA));
            }
        } else {
            mensajes.showGlobalDialogMessage("Debe seleccionar al menos una factura de la lista");
        }
    }

    /**
     * Realiza operaciones intermedias antes de la confirmacion final de la
     * conformacion de una factura.
     */
    public void beforeConformarFactura() {
        tx.execute(this::beforeConformarFactura0);
    }

    private Result<Void> beforeConformarFactura0() {
        Result<Void> result;

        try {
            // Validar datos de conformacion.
            String err = conformacionFacturas.validar(facturaSelected, conformacionInput);
            if (err != null) {
                mensajes.addMessageError("Error", err);
                throw new ValidationException(err);
            }

            // Inicializar datos de factura conformada.
            log.info("creando factura conformada asociada");
            FacturaConformada facturaConformada = conformacionFacturas.crearFacturaConformada(facturaSelected, conformacionInput);
            conformacionInput.setFacturaConformada(facturaConformada);

            // Desplegar vista de confirmacion.
            updateViewConfirmarConformacionFacturaDialog();
            Prime.closeSepsaDialog("conformacion_factura_dialog");
            Prime.showSepsaDialog("confirmacion_conformacion_factura_dialog");

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            String msg = thr.getMessage();
            if (!(thr instanceof ValidationException)) {
                msg = "Ocurrió un error durante las operaciones previas de conformacion de factura";
                log.warning(thr, msg);
            }
            result = Results.ko().cause(thr).message(msg).build();
        } finally {
            updateViewConformacionFacturaDialog();
        }

        return result;
    }

    /**
     * Cambia el estado de una o varias facturas seleccionadas a
     * {@link EstadoFacturaEnum#CONFORMADA} y realiza operaciones
     * complementarias.
     */
    public void conformarFactura() throws Throwable {
        tx.execute(this::conformarFactura0);
    }

    private Result<Void> conformarFactura0() {
        Result<Void> result;

        try {
            // Persistir factura conformada creada en paso previo.
            log.info("persistiendo registro de factura conformada");
            FacturaConformada facturaConformada = conformacionInput.getFacturaConformada();
            facades.getFacturaConformadaFacade().create(facturaConformada);

            // Procesar conformacion para facturas seleccionadas.
            List<FacturaInfo> facturas = Arrays.asList(facturaSelected);
            for (FacturaInfo factura : facturas) {
                // Cambiar estado de factura.
                log.info("cambiando estado de factura id='%s'", factura.getId());
                Result<Factura> resFactura = cambiarEstadoFactura(factura.getId(), EstadoFacturaEnum.CONFORMADA, comentarioCambioEstadoFactura);
                resFactura.raise();
            }

            // Notificar cambio de estado.
            log.info("emitiendo notificaciones");
            notificaciones.notificarConformacionDeFacturas(facturas);

            // Actualizar la vista.
            recargarListaFacturas();
            Prime.closeSepsaDialog("confirmacion_conformacion_factura_dialog");
            mensajes.showGlobalDialogMessage("¡Factura conformada exitosamente!");

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            mensajes.showGlobalDialogMessage("Ocurrió un error inesperado durante la conformación de la factura. Por favor vuelva a intentar.");
            result = Results.ko().cause(thr).message("Ocurrió un error inesperado durante la conformación de la factura. Por favor vuelva a intentar.").build();
        } finally {
            // Actualizar vista.
            updateViewConfirmarConformacionFacturaDialog();
        }

        return result;
    }

    /**
     * Cambia el estado de una o varias facturas seleccionadas a
     * {@link EstadoFacturaEnum#CONFORMADA} y realiza operaciones
     * complementarias.
     */
    public void conformarFacturas() {
        tx.execute(this::conformarFacturas0);
    }

    private Result<Void> conformarFacturas0() {
        Result<Void> result;

        try {
            // Procesar conformacion directa para facturas seleccionadas.
            List<FacturaInfo> facturas = lista.getMultiSelectedItem();
            for (FacturaInfo factura : facturas) {
                /**
                 * Generar un input en base a la factura actual a manera de
                 * simular el ingreso manual de datos y validaro los datos.
                 */
                log.info("procesando conformacion de factura id='%s'", factura.getId());
                ConformacionFacturaMB.Input input = conformacionFacturas.nuevoInputConformacionFactura(factura.getId());

                // Crear registro de factura conformada.
                log.info("creando factura conformada asociada");
                FacturaConformada facturaConformada = conformacionFacturas.crearFacturaConformada(factura, input);

                // Persistir registro de factura conformada.
                log.info("persistiendo registro de factura conformada");
                facades.getFacturaConformadaFacade().create(facturaConformada);

                // Cambiar estado de factura.
                log.info("cambiando estado de factura id='%s'", factura.getId());
                Result<Factura> resFactura = cambiarEstadoFactura(factura.getId(), EstadoFacturaEnum.CONFORMADA, comentarioCambioEstadoFactura);
                resFactura.raise();
            }

            // Notificar cambio de estado.
            log.info("emitiendo notificaciones");
            notificaciones.notificarConformacionDeFacturas(facturas);

            // Actualizar la vista.
            recargarListaFacturas();
            Prime.closeSepsaDialog("conformacion_directa_factura_dialog");
            mensajes.showGlobalDialogMessage("¡Facturas conformadas exitosamente!");

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            String msg = thr.getMessage();
            if (!(thr instanceof ValidationException)) {
                msg = "Ocurrió un error inesperado durante la conformación de una o más facturas. Por favor vuelva a intentar.";
                mensajes.showGlobalDialogMessage(msg);
            }
            result = Results.ko().cause(thr).message(msg).build();
        } finally {
            // Actualizar vista.
            updateViewConformacionDirectaFacturasDialog();
        }

        return result;
    }

    /**
     * Cambia el estado de una o varias facturas seleccionadas a
     * {@link EstadoFacturaEnum#CONFORMACION_SOLICITADA} y realiza operaciones
     * complementarias.
     */
    public void solicitarConformacionFacturas() throws Throwable {
        tx.execute(this::solicitarConformacionFacturas0);
    }

    private Result<Void> solicitarConformacionFacturas0() {
        Result<Void> result;

        try {
            // Procesar solicitud de conformacion para facturas seleccionadas.
            List<FacturaInfo> facturas = lista.getMultiSelectedItem();
            for (FacturaInfo factura : facturas) {
                // Cambiar estado de factura.
                log.info("cambiando estado de factura id='%s'", factura.getId());
                Result<Factura> resFactura = cambiarEstadoFactura(factura.getId(), EstadoFacturaEnum.CONFORMACION_SOLICITADA, comentarioCambioEstadoFactura);
                resFactura.raise();
            }

            // Notificar cambio de estado.
            log.info("emitiendo notificaciones");
            notificaciones.notificarSolicitudConformacionDeFacturas(facturas);

            // Actualizar la vista.
            recargarListaFacturas();
            Prime.closeSepsaDialog("solicitud_conformacion_facturas_dialog");
            mensajes.showGlobalDialogMessage("¡Solicitud de conformación de facturas realizada exitosamente!");

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            mensajes.showGlobalDialogMessage("Ocurrió un error inesperado durante la solicitud de conformación de una o más facturas. Por favor vuelva a intentar.");
            result = Results.ko().cause(thr).message("Ocurrió un error inesperado durante la solicitud de conformación de una o más facturas. Por favor vuelva a intentar.").build();
        } finally {
            // Actualizar vista.
            updateViewSolicitudConformacionFacturasDialog();
        }

        return result;
    }

    /**
     * Prepara la vista para el rechazo de facturas a conformar por parte del
     * comprador.
     */
    public void initRechazoFacturasPorComprador() {
        // Controlar si se selecciono alguna factura.
        if (lista.hasSelection()) {
            // Verificar que todas las facturas esten en el mismo estado previo.
            if (facturas.checkFacturasEnEstado(lista.getMultiSelectedItem(), EstadoFacturaEnum.NO_CONFORMADA, EstadoFacturaEnum.CONFORMACION_SOLICITADA)) {
                // Inicializar variables de cambio de estado de factura.
                initCambioEstadoFacturas();

                // Mostrar cuadro de dialogo para rechazo de facturas.
                Prime.showSepsaDialog("rechazo_factura_por_comprador_dialog");
                updateViewRechazoFacturasPorCompradorDialog();
            } else {
                mensajes.showGlobalDialogMessage("Sólo se puede rechazar la conformación de facturas en estado \"%s\" o \"%s\"", estados.getHumanText(EstadoFacturaEnum.NO_CONFORMADA), estados.getHumanText(EstadoFacturaEnum.CONFORMACION_SOLICITADA));
            }
        } else {
            mensajes.showGlobalDialogMessage("Debe seleccionar al menos una factura de la lista");
        }
    }

    /**
     * Cambia el estado de una o varias facturas seleccionadas a
     * {@link EstadoFacturaEnum#RECHAZADA_POR_COMPRADOR} y realiza operaciones
     * complementarias.
     */
    public void rechazarFacturasPorComprador() {
        tx.execute(this::rechazarFacturasPorComprador0);
    }

    public Result<Void> rechazarFacturasPorComprador0() {
        Result<Void> result;

        try {
            // Procesar rechazo para facturas seleccionadas.
            List<FacturaInfo> facturas = lista.getMultiSelectedItem();
            for (FacturaInfo factura : facturas) {
                // Cambiar estado de factura.
                log.info("cambiando estado de factura id='%s'", factura.getId());
                Result<Factura> resFactura = cambiarEstadoFactura(factura.getId(), EstadoFacturaEnum.RECHAZADA_POR_COMPRADOR, comentarioCambioEstadoFactura);
                resFactura.raise();
            }

            // Notificar rechazo de facturas conformadas.
            log.info("emitiendo notificaciones");
            notificaciones.notificarRechazoDeFacturasPorComprador(facturas);

            // Actualizar vista.
            Prime.closeSepsaDialog("rechazo_factura_por_comprador_dialog");
            mensajes.showGlobalDialogMessage("¡Facturas rechazadas exitosamente!");
            recargarListaFacturas();

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            mensajes.showGlobalDialogMessage("Ocurrió un error rechanzando las facturas seleccionadas. Por favor vuelva a intentar.");
            result = Results.ko().cause(thr).message("Ocurrió un error rechanzando las facturas seleccionadas. Por favor vuelva a intentar.").build();
        } finally {
            // Actualizar vista.
            updateViewRechazoFacturasPorCompradorDialog();
        }

        return result;
    }
    //</editor-fold>

    /**
     * Prepara la vista para el rechazo de facturas vendidas por proveedor.
     */
    public void initRechazoFacturasPorEntidad() {
        // Controlar si se selecciono alguna factura.
        if (lista.hasSelection()) {
            // Verificar que todas las facturas esten en el mismo estado previo.
            if (facturas.checkFacturasEnEstado(lista.getMultiSelectedItem(), EstadoFacturaEnum.EN_ESTUDIO_POR_ENTIDAD, EstadoFacturaEnum.VENDIDA)) {
                // Inicializar variables de cambio de estado de facturas.
                initCambioEstadoFacturas();

                // Mostrar cuadro de dialogo de seleccion de entidad financiera y confirmacion de venta.
                updateViewRechazoFacturasPorEntidadDialog();
                Prime.showSepsaDialog("rechazo_factura_por_entidad_dialog");
            } else {
                mensajes.showGlobalDialogMessage("Sólo se pueden rechazar facturas en estado \"%s\" o \"%s\"", estados.getHumanText(EstadoFacturaEnum.EN_ESTUDIO_POR_ENTIDAD), estados.getHumanText(EstadoFacturaEnum.VENDIDA));
            }
        } else {
            mensajes.showGlobalDialogMessage("Debe seleccionar al menos una factura de la lista");
        }
    }

    /**
     * Cambia el estado de una o varias facturas seleccionadas a
     * {@link EstadoFacturaEnum#RECHAZADA_POR_ENTIDAD} y realiza operaciones
     * complementarias.
     */
    public void rechazarFacturasPorEntidad() throws Throwable {
        tx.execute(this::rechazarFacturasPorEntidad0);
    }

    private Result<Void> rechazarFacturasPorEntidad0() {
        Result<Void> result;

        try {
            // Procesar rechazo para facturas seleccionadas.
            List<FacturaInfo> facturas = lista.getMultiSelectedItem();
            for (FacturaInfo factura : facturas) {
                // Cambiar estado de factura.
                log.info("cambiando estado de factura id='%s'", factura.getId());
                Result<Factura> resFactura = cambiarEstadoFactura(factura.getId(), EstadoFacturaEnum.RECHAZADA_POR_ENTIDAD, comentarioCambioEstadoFactura);
                resFactura.raise();

                // Obtener datos de solicitud de desembolso a rechazar.
                log.info("obteniendo datos de documento y factura conformada");
                Integer idProveedor = factura.getProveedorId();
                Integer idComprador = factura.getCompradorId();
                BigDecimal montoDesembolso = factura.getMontoTotalDesembolso();

                // Revertir descuento en linea de credito de proveedor.
                log.info("revirtiendo descuento en linea de credito de proveedor");
                Result<LineaCredito> resCreditoLineaProveedor = acreditarLineaCreditoPersona(idProveedor, montoDesembolso);
                if (resCreditoLineaProveedor.isFailure()) {
                    // Verificar error producido.
                    Integer errno = resCreditoLineaProveedor.failure().errno();
                    if (errno == Errnos.ERRNO_LINEA_CREDITO_CORRUPTA) {
                        mensajes.addGlobalDialogMessage("No se puede rechazar la factura en este momento ya que la línea de crédito del proveedor fue modificada de manera externa al mismo tiempo.");
                    }
                    resCreditoLineaProveedor.raise();
                }

                // Revertir descuento en linea de credito de comprador.
                log.info("revirtiendo descuento en linea de credito de comprador");
                Result<LineaCredito> resCreditoLineaComprador = acreditarLineaCreditoPersona(idComprador, montoDesembolso);
                if (resCreditoLineaComprador.isFailure()) {
                    // Verificar error producido.
                    Integer errno = resCreditoLineaComprador.failure().errno();
                    if (errno == Errnos.ERRNO_LINEA_CREDITO_CORRUPTA) {
                        mensajes.addGlobalDialogMessage("No se puede rechazar la factura en este momento ya que la línea de crédito del comprador fue modificada de manera externa al mismo tiempo.");
                    }
                    resCreditoLineaComprador.raise();
                }
            }

            // Notificar rechazo de facturas conformadas.
            log.info("emitiendo notificaciones");
            notificaciones.notificarRechazoDeFacturasPorEntidad(facturas);

            // Actualizar vista.
            Prime.closeSepsaDialog("rechazo_factura_por_entidad_dialog");
            mensajes.showGlobalDialogMessage("¡Facturas rechazadas exitosamente!");
            recargarListaFacturas();

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            // Actualizar vista.
            mensajes.showGlobalDialogMessage("Ocurrió un error rechanzando las facturas seleccionadas. Por favor vuelva a intentar.");
            result = Results.ko().cause(thr).message("Ocurrió un error rechanzando las facturas seleccionadas. Por favor vuelva a intentar.").build();
        }

        return result;
    }

    /**
     * Permite aumentar el saldo disponible de una linea de credito, esto no
     * implica aumentar el monto tope de la linea de credito.
     *
     * @param idPersona Persona a la que se realiza el aumento.
     * @param monto Monto a aumentar.
     * @return Resultado de la operacion.
     */
    private Result<LineaCredito> acreditarLineaCreditoPersona(Integer idPersona, BigDecimal monto) {
        try {
            // Identificar linea de credito.
            Result<LineaCredito> resLineaCredito = beans.getLineasCredito().buscarLineaCredito(usuarioLogueado.getPersona().getId(), idPersona);
            resLineaCredito.raise(false);
            LineaCredito lineaCredito = resLineaCredito.value(null);
            if (lineaCredito == null) {
                return Results.ok()
                        .nullable(true)
                        .build();
            }

            // Deshacer descuento realizado durante la venta.
            Result<LineaCredito> resCreditoLinea = beans.getLineasCredito().acreditarLineaCredito(lineaCredito, usuarioLogueado.getUsuario().getId(), monto);
            resCreditoLinea.raise();
            return Results.ok()
                    .value(lineaCredito)
                    .build();
        } catch (Throwable thr) {
            return Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error inesperado modificando la línea de crédito para el rechazo de desembolso de esta factura. Por favor vuelva a intentar")
                    .build();
        }
    }

    /**
     * Cambia el estado de una o varias facturas seleccionadas a
     * {@link EstadoFacturaEnum#EN_ESTUDIO_POR_ENTIDAD} y realiza operaciones
     * complementarias.
     */
    public void aceptarAnalisisFacturas() {
        tx.execute(this::aceptarAnalisisFacturas0);
    }

    private Result<Void> aceptarAnalisisFacturas0() {
        Result<Void> result;

        try {
            // Procesar aceptacion de analisis para facturas seleccionadas.
            List<FacturaInfo> facturas = lista.getMultiSelectedItem();
            for (FacturaInfo factura : facturas) {
                // Cambiar estado de factura.
                log.info("cambiando estado de factura id='%s'", factura.getId());
                Result<Factura> resFactura = cambiarEstadoFactura(factura.getId(), EstadoFacturaEnum.EN_ESTUDIO_POR_ENTIDAD, comentarioCambioEstadoFactura);
                resFactura.raise();
            }

            // Notificar cambio de estado.
            log.info("emitiendo notificaciones");
            notificaciones.notificarAnalisisDeFacturas(facturas);

            // Actualizar vista.
            Prime.closeSepsaDialog("analisis_facturas_dialog");
            mensajes.showGlobalDialogMessage("¡Facturas aceptadas para análisis exitosamente!");
            recargarListaFacturas();

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            mensajes.showGlobalDialogMessage("Ocurrió un error inesperado durante la aceptación para análisis de las facturas seleccionadas. Por favor vuelva a intentar.");
            result = Results.ko().cause(thr).message("Ocurrió un error inesperado durante la aceptación para análisis de las facturas seleccionadas. Por favor vuelva a intentar.").build();
        } finally {
            // Actualizar vista.
            updateViewAnalisisFacturasDialog();
        }

        return result;
    }

    /**
     * Valida los datos ingresados en la vista para el registro de un
     * desembolso.
     *
     * @return Mensaje de error, si hubiere.
     */
    private String validateDesembolsoInput() {
        // Deben existir las cuentas de origen y destino para el desembolso al proveedor.
        if (cuentaEntidadFinancieraDesembolsoProveedorSelected == null) {
            return "La cuenta de origen para el desembolso al proveedor no puede ser nula.";
        }
        if (cuentaProveedorSelected == null) {
            return "La cuenta de destino para el desembolso al proveedor no puede ser nula.";
        }

        // Deben existir las cuentas de origen y destino para el desembolso al comprador, si esta indicado.
        if (!omitirDesembolsoComprador) {
            if (cuentaEntidadFinancieraDesembolsoCompradorSelected == null) {
                return "La cuenta de origen para el desembolso al comprador no puede ser nula.";
            }
            if (cuentaCompradorSelected == null) {
                return "La cuenta de destino para el desembolso al comprador no puede ser nula.";
            }
        }

        // Deben existir las cuentas de origen y destino para el desembolso al proveedor de servicio (Sepsa), si esta indicado.
        if (!omitirDesembolsoSepsa) {
            if (cuentaEntidadFinancieraDesembolsoSepsaSelected == null) {
                return "La cuenta de origen para el desembolso al proveedor de servicio no puede ser nula.";
            }
            if (cuentaSepsaSelected == null) {
                return "La cuenta de destino para el desembolso al proveedor de servicio no puede ser nula.";
            }
        }

        return null;
    }

    /**
     * Cambia el estado de una o varias facturas seleccionadas a
     * {@link EstadoFacturaEnum#PAGADA} (desembolso registrado) y realiza
     * operaciones complementarias.
     */
    public void registrarDesembolsoFactura() throws Throwable {
        tx.execute(this::registrarDesembolsoFactura0);
    }

    private Result<Void> registrarDesembolsoFactura0() {
        Result<Void> result;

        try {
            // Validar datos de entrada.
            String err = validateDesembolsoInput();
            if (err != null) {
                mensajes.addMessageError("Error", err);
                throw new ValidationException(err);
            }

            // Registrar transacciones de desembolso de factura.
            FacturaInfo factura = facturaSelected;
            Date now = Fechas.ahora();

            // Cmbiar estado de factura.
            log.info("cambiando estado de factura id='%s'", factura.getId());
            Result<Factura> resFactura = cambiarEstadoFactura(factura.getId(), EstadoFacturaEnum.PAGADA, comentarioCambioEstadoFactura);
            resFactura.raise();

            // Adjuntar archivos a factura, si hubieren.
            log.info("adjuntando '%d' archivos", cacheArchivos.getCache().size());
            adjuntarArchivosAFactura(factura, cacheArchivos);

            // Registrar el pago de factura al proveedor.
            log.info("registrando transaccion de desembolso a proveedor");
            Result<Transaccion> txProveedor = facturas.registrarTransaccionDesembolso(now, cuentaEntidadFinancieraDesembolsoProveedorSelected, cuentaProveedorSelected, factura, montoDesembolsoProveedor);
            txProveedor.raise();

            // Registrar el pago de comision al comprador, si hubiere.
            if (!omitirDesembolsoComprador) {
                log.info("registrando transaccion de desembolso de comision a comprador");
                Result<Transaccion> txComprador = facturas.registrarTransaccionDesembolso(now, cuentaEntidadFinancieraDesembolsoCompradorSelected, cuentaCompradorSelected, factura, montoDesembolsoComprador);
                txComprador.raise();
            } else {
                log.info("transaccion de desembolso de comision a comprador omitida");
            }

            // Registrar el pago de comision a sepsa, si hubiere.
            if (!omitirDesembolsoSepsa) {
                log.info("registrando transaccion de desembolso de comision a proveedor de servicio");
                Result<Transaccion> txSepsa = facturas.registrarTransaccionDesembolso(now, cuentaEntidadFinancieraDesembolsoSepsaSelected, cuentaSepsaSelected, factura, montoDesembolsoSepsa);
                txSepsa.raise();
            } else {
                log.info("transaccion de desembolso de comision a proveedor de servicio");
            }

            // Notificar cambio de estado.
            log.info("emitiendo notificaciones");
            notificaciones.notificarDesembolsoDeFacturas(Arrays.asList(factura));

            // Actualizar vista.
            mensajes.showGlobalDialogMessage(FacturaMB.this::redireccionarListaFacturas, "¡Factura desembolsada exitosamente!");

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            String msg = thr.getMessage();
            if (!(thr instanceof ValidationException)) {
                msg = "Ocurrió un error al registrar alguna las transacciones de desembolso";
                mensajes.addMessageError("Error", msg);
            }
            result = Results.ko().cause(thr).message(msg).build();
        } finally {
            // Actualizar vista.
            updateViewDesembolsoFactura();
        }

        return result;
    }

    /**
     * Calcula los dias de credito restantes para una factura considerando el
     * dia actual.
     *
     * @param factura Factura que se desea saber sus dias de credito restantes.
     * @return Cantidad de dias de credito restantes para la factura.
     */
    public int diasCreditoRestantes(Factura factura) {
        return Fechas.diasDiferencia(new Date(), factura.getFechaPago());
    }

    @Getter
    @Setter
    private String historicoFacturaHtml;

    /**
     * Actualiza la vista para desplegar los registros de historicos de una
     * factura.
     *
     * @param idFactura Identificador de factura.
     */
    public void verHistoricoFactura(Integer idFactura) {
        // Obtener la lista de registros historicos de la factura.
        List<HistoricoFactura> historicoFactura = facades.getHistoricoFacturaFacade().findByIdFactura(idFactura);

        // Generar HTML.
        StringBuilder2 builder = new StringBuilder2();
        for (HistoricoFactura historico : historicoFactura) {
            try {
                String color = estados.getColor(Units.execute(() -> historico.getIdEstado().getId()));
                String estado = estados.getHumanText(Units.execute(-1, () -> historico.getIdEstado().getId()));
                String fecha = Fechas.format(historico.getFecha(), "dd/MMM/yyyy HH:mm");
                String mensaje = Assertions.stringNullOrEmpty(historico.getComentario()) ? "-- Sin Comentarios --" : historico.getComentario();
                String mensajeCls = Assertions.stringNullOrEmpty(historico.getComentario()) ? "app_historico_comentario" : "";

                builder.append(" <tr> ");
                builder.append("     <td> ");
                builder.append("         <span class=\"label label-primary app_text_truncated\" style=\"float:left; float:left; width:100%%; min-width:100%%;background-color: %s;\">%s</span>", color, estado);
                builder.append("     </td> ");
                builder.append("     <td> ");
                builder.append("         <div class=\"app_historico_fecha app_text_uppercase\">%s</div>", fecha);
                builder.append("     </td> ");
                builder.append("     <td> ");
                builder.append("         <span class=\"%s\">%s</span>", mensajeCls, mensaje);
                builder.append("     </td> ");
                builder.append(" </tr> ");
            } catch (Throwable thr) {
                Errors.dump(System.err, thr);
            }
        }

        // Actualizar vista.
        historicoFacturaHtml = builder.toString();
        updateViewHistoricoFacturaDialog();
        Prime.showSepsaDialog("historico_factura_dialog");
    }

    /**
     * Actualiza la vista para desplegar los archivos adjuntos asociados a una
     * factura.
     *
     * @param idFactura Identificador de factura.
     */
    public void verArchivosAdjuntos(Integer idFactura) {
        // Obetener la lista de archivos adjuntos.
        Result<Collection<Archivo>> resArchivosAdjuntos = beans.getArchivos().buscarArchivosAdjuntos(idFactura, Archivos.TipoEntidadArchivoAdjuntoEnum.FACTURA);
        if (!resArchivosAdjuntos.isSuccess()) {
            return;
        }

        // Inicializar coleccion de objetos a utilizar en la vista.
        archivosAdjuntos = new LinkedList<>();
        archivosAdjuntos.addAll(resArchivosAdjuntos.value());

        if (archivosAdjuntos.isEmpty()) {
            mensajes.showGlobalDialogMessage("La factura seleccionada no posee archivos adjuntos");
        } else {
            // Ordenar entradas en base a la fecha de subida.
            Collections.sort(archivosAdjuntos, (Archivo a, Archivo b) -> {
                Date A = a.getFecha();
                Date B = b.getFecha();
                return A.compareTo(B);
            });

            // Habilitar cuadro de dialogo para visualizacion de archivos.
            Prime.showSepsaDialog("archivos_adjuntos_dialog");
        }

        //Actualizar vista.
        updateViewArchivosAdjuntosDialog();
    }

    /**
     * Aplica los filtros sobre la lista de facturas.
     */
    public void filtrarLista() {
        lista.filter();
        updateViewFiltrosForm();
    }

    /**
     * Limpia los valores establecidos en los campos de filtro.
     */
    public void limpiarFiltroForm() {
        CustomFilter filtro = lista.getFiltro();
        if (!filtro.isEnabled()) {
            filtro.clearFilterValues();
            updateViewFiltrosForm();
        }
    }

    /**
     * Actualiza la lista de facturas en la vista.
     */
    public void recargarListaFacturas() {
        lista.clearSelections();
        updateViewListaDeFacturas();
        lista.reload();
        updateViewListaDeFacturas();
    }

    /**
     * Indica al cliente que debe redireccionar y cargar la vista principal de
     * lista de facturas.
     */
    public void redireccionarListaFacturas() {
        Prime.redirect("/app/abm/factura/lista.xhtml");
    }

    /**
     * Metodo llamado cuando se indica que el monto de comision, durante el
     * registro de un desembolso, debe ser omitido.
     */
    public void onOmitirDesembolsoChanged() {
        updateViewDesembolsoFactura();
    }

    // <editor-fold desc="Actualizaciones de la vista.">
    private void updateViewListaDeFacturas() {
        lista.clearSelections();
        Ajax.update("tabla_facturas_panel:tabla_facturas_form:tabla_facturas");
        Prime.executeJs("$.fn.matchHeight._update();");
    }

    private void updateViewConformacionFacturaDialog() {
        Ajax.update("conformacion_factura_dialog:conformacion_factura_form");
        Ajax.update("conformacion_factura_dialog:conformacion_factura_dialog_content_messages");
    }

    private void updateViewConformacionDirectaFacturasDialog() {
        Ajax.update("conformacion_directa_factura_dialog:conformacion_directa_factura_form");
    }

    private void updateViewSolicitudConformacionFacturasDialog() {
        Ajax.update("solicitud_conformacion_facturas_dialog:solicitud_conformacion_facturas_form");
    }

    private void updateViewAnalisisFacturasDialog() {
        Ajax.update("analisis_facturas_dialog:analisis_facturas_form");
    }

    private void updateViewConfirmarConformacionFacturaDialog() {
        Ajax.update("confirmacion_conformacion_factura_dialog:confirmacion_conformacion_factura_form");
    }

    private void updateViewRechazoFacturasPorCompradorDialog() {
        Ajax.update("rechazo_factura_por_comprador_dialog:rechazo_factura_por_comprador_form");
    }

    private void updateViewRechazoFacturasPorProveedorDialog() {
        Ajax.update("rechazo_factura_por_proveedor_dialog:rechazo_factura_por_proveedor_form");
    }

    private void updateViewRechazoFacturasPorEntidadDialog() {
        Ajax.update("rechazo_factura_por_entidad_dialog:rechazo_factura_por_entidad_form");
    }

    private void updateViewDesembolsoFactura() {
        Ajax.update("desembolso_factura_form");
        Ajax.update("desembolso_factura_form_messages");
    }

    private void updateViewCobroFacturaDialog() {
        Ajax.update("cobro_factura_dialog:cobro_factura_form");
    }

    private void updateViewFiltrosForm() {
        Ajax.update("filtro_facturas_panel:filtro_facturas_form");
        Prime.showBlockUi("bui", lista.getFiltro().isEnabled());
        updateViewListaDeFacturas();
    }

    private void updateViewHistoricoFacturaDialog() {
        Ajax.update("historico_factura_dialog:historico_factura_form");
    }

    private void updateViewArchivosAdjuntosDialog() {
        Prime.update("archivos_adjuntos_dialog:archivos_adjuntos:archivos_adjuntos_form");
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public String getBtnFiltrarText() {
        if (lista.getFiltro().isEnabled()) {
            return "Deshacer Filtro";
        } else {
            return "Filtrar Datos";
        }
    }

    public String getPersonaLabelText() {
        if (usuarioLogueado.esComprador()) {
            return "Proveedor";
        }
        if (usuarioLogueado.esProveedor()) {
            return "Comprador";
        }
        return "";
    }

    public String getOpcionesPanelClass() {
        if (usuarioLogueado.esAdministrador()) {
            return "hidden";
        } else {
            return "col-md-2";
        }
    }

    public String getListaFacturasPanelClass() {
        if (usuarioLogueado.esAdministrador()) {
            return "col-md-12";
        } else {
            return "col-md-10";
        }
    }

    public ListaMB getLista() {
        return lista;
    }

    public void setLista(ListaMB lista) {
        this.lista = lista;
    }

    public Persona getPersonaSelected() {
        return personaSelected;
    }

    public void setPersonaSelected(Persona personaSelected) {
        this.personaSelected = personaSelected;
    }

    public String getComentarioCambioEstadoFactura() {
        return comentarioCambioEstadoFactura;
    }

    public void setComentarioCambioEstadoFactura(String comentarioCambioEstadoFactura) {
        this.comentarioCambioEstadoFactura = comentarioCambioEstadoFactura;
    }

    public List<CuentaEntidadFinanciera> getCuentasComprador() {
        return cuentasComprador;
    }

    public void setCuentasComprador(List<CuentaEntidadFinanciera> cuentasComprador) {
        this.cuentasComprador = cuentasComprador;
    }

    public CuentaEntidadFinanciera getCuentaCompradorSelected() {
        return cuentaCompradorSelected;
    }

    public void setCuentaCompradorSelected(CuentaEntidadFinanciera cuentaCompradorSelected) {
        this.cuentaCompradorSelected = cuentaCompradorSelected;
    }

    public List<CuentaEntidadFinanciera> getCuentasSepsa() {
        return cuentasSepsa;
    }

    public void setCuentasSepsa(List<CuentaEntidadFinanciera> cuentasSepsa) {
        this.cuentasSepsa = cuentasSepsa;
    }

    public CuentaEntidadFinanciera getCuentaSepsaSelected() {
        return cuentaSepsaSelected;
    }

    public void setCuentaSepsaSelected(CuentaEntidadFinanciera cuentaSepsaSelected) {
        this.cuentaSepsaSelected = cuentaSepsaSelected;
    }

    public List<CuentaEntidadFinanciera> getCuentasProveedor() {
        return cuentasProveedor;
    }

    public void setCuentasProveedor(List<CuentaEntidadFinanciera> cuentasProveedor) {
        this.cuentasProveedor = cuentasProveedor;
    }

    public CuentaEntidadFinanciera getCuentaProveedorSelected() {
        return cuentaProveedorSelected;
    }

    public void setCuentaProveedorSelected(CuentaEntidadFinanciera cuentaProveedorSelected) {
        this.cuentaProveedorSelected = cuentaProveedorSelected;
    }

    public List<CuentaEntidadFinanciera> getCuentasEntidadFinanciera() {
        return cuentasEntidadFinanciera;
    }

    public void setCuentasEntidadFinanciera(List<CuentaEntidadFinanciera> cuentasEntidadFinanciera) {
        this.cuentasEntidadFinanciera = cuentasEntidadFinanciera;
    }

    public CuentaEntidadFinanciera getCuentaEntidadFinancieraDesembolsoProveedorSelected() {
        return cuentaEntidadFinancieraDesembolsoProveedorSelected;
    }

    public void setCuentaEntidadFinancieraDesembolsoProveedorSelected(CuentaEntidadFinanciera cuentaEntidadFinancieraDesembolsoProveedorSelected) {
        this.cuentaEntidadFinancieraDesembolsoProveedorSelected = cuentaEntidadFinancieraDesembolsoProveedorSelected;
    }

    public CuentaEntidadFinanciera getCuentaEntidadFinancieraDesembolsoCompradorSelected() {
        return cuentaEntidadFinancieraDesembolsoCompradorSelected;
    }

    public void setCuentaEntidadFinancieraDesembolsoCompradorSelected(CuentaEntidadFinanciera cuentaEntidadFinancieraDesembolsoCompradorSelected) {
        this.cuentaEntidadFinancieraDesembolsoCompradorSelected = cuentaEntidadFinancieraDesembolsoCompradorSelected;
    }

    public CuentaEntidadFinanciera getCuentaEntidadFinancieraDesembolsoSepsaSelected() {
        return cuentaEntidadFinancieraDesembolsoSepsaSelected;
    }

    public void setCuentaEntidadFinancieraDesembolsoSepsaSelected(CuentaEntidadFinanciera cuentaEntidadFinancieraDesembolsoSepsaSelected) {
        this.cuentaEntidadFinancieraDesembolsoSepsaSelected = cuentaEntidadFinancieraDesembolsoSepsaSelected;
    }

    public FacturaInfo getFacturaSelected() {
        return facturaSelected;
    }

    public void setFacturaSelected(FacturaInfo facturaSelected) {
        this.facturaSelected = facturaSelected;
    }

    public Boolean getOmitirDesembolsoComprador() {
        return omitirDesembolsoComprador;
    }

    public void setOmitirDesembolsoComprador(Boolean omitirDesembolsoComprador) {
        this.omitirDesembolsoComprador = omitirDesembolsoComprador;
    }

    public Boolean getOmitirDesembolsoSepsa() {
        return omitirDesembolsoSepsa;
    }

    public void setOmitirDesembolsoSepsa(Boolean omitirDesembolsoSepsa) {
        this.omitirDesembolsoSepsa = omitirDesembolsoSepsa;
    }

    public Boolean getOmitirDesembolsoProveedor() {
        return omitirDesembolsoProveedor;
    }

    public void setOmitirDesembolsoProveedor(Boolean omitirDesembolsoProveedor) {
        this.omitirDesembolsoProveedor = omitirDesembolsoProveedor;
    }

    public BigDecimal getMontoDesembolsoComprador() {
        return montoDesembolsoComprador;
    }

    public void setMontoDesembolsoComprador(BigDecimal montoDesembolsoComprador) {
        this.montoDesembolsoComprador = montoDesembolsoComprador;
    }

    public BigDecimal getMontoDesembolsoSepsa() {
        return montoDesembolsoSepsa;
    }

    public void setMontoDesembolsoSepsa(BigDecimal montoDesembolsoSepsa) {
        this.montoDesembolsoSepsa = montoDesembolsoSepsa;
    }

    public BigDecimal getMontoDesembolsoProveedor() {
        return montoDesembolsoProveedor;
    }

    public void setMontoDesembolsoProveedor(BigDecimal montoDesembolsoProveedor) {
        this.montoDesembolsoProveedor = montoDesembolsoProveedor;
    }

    public BigDecimal getMontoDesembolsoEntidad() {
        return montoDesembolsoEntidad;
    }

    public void setMontoDesembolsoEntidad(BigDecimal montoDesembolsoEntidad) {
        this.montoDesembolsoEntidad = montoDesembolsoEntidad;
    }

    public ConformacionFacturaMB.Input getConformacionInput() {
        return conformacionInput;
    }

    public void setConformacionInput(ConformacionFacturaMB.Input conformacionInput) {
        this.conformacionInput = conformacionInput;
    }

    public String getIdFacturaParam() {
        return idFacturaParam;
    }

    public void setIdFacturaParam(String idFacturaParam) {
        this.idFacturaParam = idFacturaParam;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public List<Archivo> getArchivosAdjuntos() {
        return archivosAdjuntos;
    }

    public void setArchivosAdjuntos(List<Archivo> archivosAdjuntos) {
        this.archivosAdjuntos = archivosAdjuntos;
    }

    public CacheArchivos getCacheArchivos() {
        return cacheArchivos;
    }

    public void setCacheArchivos(CacheArchivos cacheArchivos) {
        this.cacheArchivos = cacheArchivos;
    }

    public PersonasMB getPersonas() {
        return personas;
    }

    public void setPersonas(PersonasMB personas) {
        this.personas = personas;
    }
    // </editor-fold>

}
