/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.utils;

import fa.gs.utils.misc.text.StringBuilder2;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Html {

    public static String badge(String text, String bgColor) {
        return badge("span", text, bgColor);
    }

    public static String badge(String tag, String text, String bgColor) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append("<%s class=\"label label-primary app_text_truncated ui-grid-col-12\" style=\"background-color: %s\">", tag, bgColor);
        builder.append("%s", text);
        builder.append("</%s>", tag);
        return builder.toString();
    }

    public static String selectItemEmpty() {
        return selectItem("&nbsp;", "&nbsp;");
    }

    public static String selectItem(String header, String body) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append("<div style=\"max-width: 300px;\">");
        builder.append("<h4 class=\"list-group-item-heading\">%s</h4>", header);
        builder.append("<p class=\"list-group-item-text\">%s</p>", body);
        builder.append("</div>");
        return builder.toString();
    }

}
