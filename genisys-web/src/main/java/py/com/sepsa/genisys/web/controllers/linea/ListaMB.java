/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.linea;

import fa.gs.criteria.query.Operators;
import fa.gs.criteria.query.Order;
import fa.gs.criteria.query.QueryCriteria;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.Dependent;
import py.com.sepsa.genisys.ejb.entities.factoring.LineaCredito;
import py.com.sepsa.genisys.ejb.facades.factoring.LineaCreditoFacade;
import py.com.sepsa.genisys.ejb.utils.Criterias;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.CustomLazyDataModel;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class ListaMB extends py.com.sepsa.genisys.web.controllers.ListaMB<LineaCredito> implements Serializable {

    @Override
    protected CustomLazyDataModel<LineaCredito> buildItemsLoader() {
        LineaCreditoFacade facade = facades.getLineaCreditoFacade();
        return new LineaCreditosDefaultLazyList(facade);
    }

    @Override
    protected CustomFilter buildItemsFilter() {
        return new LineaCreditoListFilter();
    }

    /**
     * Clase que encapsula la carga progresiva de cargos desde la BD.
     */
    private class LineaCreditosDefaultLazyList extends CustomLazyDataModel<LineaCredito> {

        /**
         * Constructor.
         *
         * @param facade Facade de facturas.
         */
        public LineaCreditosDefaultLazyList(LineaCreditoFacade facade) {
            super(facade);
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<LineaCredito> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            // Filtras por las configuraciones realizadas bajo convenios donde exista la entidad financiera.
            if (usuarioLogueado.esComprador()) {
                filter.put(String.format("%s$%s", "idPersona", Operators.EQUALS), usuarioLogueado.getPersona().getId());
            } else if (usuarioLogueado.esProveedor()) {
                filter.put(String.format("%s$%s", "idPersona", Operators.EQUALS), usuarioLogueado.getPersona().getId());
            } else if (usuarioLogueado.esEntidadFinanciera()) {
                filter.put(String.format("%s$%s", "idEntidadFinanciera", Operators.EQUALS), usuarioLogueado.getPersona().getId());
            } else {
                ;
            }

            // Busqueda de registros.
            QueryCriteria criteria = Criterias.instance(limit, offset, sorts, filter);
            List<LineaCredito> data = facade.find(criteria);
            setRowCount(facade.count(criteria));
            return data;
        }

    }

    /**
     * Clase que encapsula los campos y operadores de filtro aplicables a la
     * lista principal de facturas.
     */
    public class LineaCreditoListFilter extends CustomFilter {

        // <editor-fold defaultstate="collapsed" desc="Atributos">
        /**
         * Campo de filtro para el dentificador de factura.
         */
        private Integer idLineaCredito;

        private String razonSocialEntidadFinanciera;

        private String razonSocialCliente;

        private BigDecimal montoDisponibleMenor;

        private BigDecimal montoDisponibleMayor;
        // </editor-fold>

        /**
         * Constructor.
         */
        public LineaCreditoListFilter() {
            clearFilterValues();
        }

        /**
         * Limpia los valores de filtro posiblemente establecidos previamente.
         */
        @Override
        final public void clearFilterValues() {
            idLineaCredito = null;
            razonSocialCliente = null;
            razonSocialEntidadFinanciera = null;
            montoDisponibleMenor = null;
            montoDisponibleMayor = null;
        }

        /**
         * Obtiene los valores de filtro correspondientes.
         *
         * @return Filtros organizados en pares {nombre filtro, valor}.
         */
        @Override
        public Map<String, Object> getFilters() {
            Map<String, Object> filters = new HashMap<>();

            // Filtro de id.
            if (idLineaCredito != null) {
                filters.put("id", idLineaCredito);
            }

            // Monto disponible menor o igual que.
            if (montoDisponibleMenor != null) {
                filters.put(String.format("%s$%s", "monto", Operators.LESS_EQUAL), montoDisponibleMenor);
            }

            // Monto disponible mayor o igual que.
            if (montoDisponibleMayor != null) {
                filters.put(String.format("%s$%s", "monto", Operators.GREATER_EQUAL), montoDisponibleMayor);
            }

            return filters;
        }

        // <editor-fold defaultstate="collapsed" desc="Getters y Setters">
        public Integer getIdLineaCredito() {
            return idLineaCredito;
        }

        public void setIdLineaCredito(Integer idLineaCredito) {
            this.idLineaCredito = idLineaCredito;
        }

        public String getRazonSocialEntidadFinanciera() {
            return razonSocialEntidadFinanciera;
        }

        public void setRazonSocialEntidadFinanciera(String razonSocialEntidadFinanciera) {
            this.razonSocialEntidadFinanciera = razonSocialEntidadFinanciera;
        }

        public String getRazonSocialCliente() {
            return razonSocialCliente;
        }

        public void setRazonSocialCliente(String razonSocialCliente) {
            this.razonSocialCliente = razonSocialCliente;
        }

        public BigDecimal getMontoDisponibleMenor() {
            return montoDisponibleMenor;
        }

        public void setMontoDisponibleMenor(BigDecimal montoDisponibleMenor) {
            this.montoDisponibleMenor = montoDisponibleMenor;
        }

        public BigDecimal getMontoDisponibleMayor() {
            return montoDisponibleMayor;
        }

        public void setMontoDisponibleMayor(BigDecimal montoDisponibleMayor) {
            this.montoDisponibleMayor = montoDisponibleMayor;
        }
        // </editor-fold>
    }

}
