/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.validation;

import java.math.BigDecimal;
import java.util.Date;
import javax.faces.validator.FacesValidator;
import py.com.sepsa.genisys.ejb.entities.factoring.ConfigCargo;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.web.utils.CustomValidator;

/**
 *
 * @author Fabio A. González Sosa
 */
@FacesValidator("cargaConfigCargoValidator")
public class CargaConfigCargoValidator extends CustomValidator<ConfigCargo> {

    /**
     * Instancia unica para la clase.
     */
    private static CargaConfigCargoValidator INSTANCE = null;

    /**
     * Constructor.
     */
    private CargaConfigCargoValidator() {

    }

    /**
     * Metodo estatico que obtiene el singleton de la clase.
     *
     * @return Instancia.
     */
    public static CargaConfigCargoValidator get() {
        if (INSTANCE == null) {
            INSTANCE = new CargaConfigCargoValidator();
        }
        return INSTANCE;
    }

    /**
     * Valida los datos ingresados en la vista de carga manual de cargo.
     *
     * @param cargo Cargo inicializado con valores establecidos en la vista.
     * @return Una cadena de texto que representa un error ocurrido. Vacio caso
     * contrario.
     */
    @Override
    public String validate(ConfigCargo cargo) {
        // Proveedor.
        if (cargo.getProveedor() == null) {
            return "Debe seleccionar un proveedor.";
        }
        // Comprador.
        if (cargo.getComprador() == null) {
            return "Debe seleccionar un comprador.";
        }
        // Tipo de valor.
        if (cargo.getIdTipoValor() == null) {
            return "Debe seleccionar un tipo de valor.";
        }
        // Valor.
        if (cargo.getValor() == null) {
            return "Debe especificar un valor para el cargo.";
        }
        if (cargo.getValor().compareTo(BigDecimal.ZERO) <= 0) {
            return "El valor no puede ser menor o igual a cero.";
        }
        // Tipo de cargo.
        if (cargo.getIdTipoCargo() == null) {
            return "Debe seleccionar un tipo de cargo.";
        }
        // Si el tipo de valor es de porcentaje, debe estar en el rango de 0 a 100.
        if (cargo.getIdTipoValor().getDescripcion().toLowerCase().contains("porcentaje") && cargo.getValor().compareTo(BigDecimal.valueOf(100)) > 0) {
            return "El valor del cargo como porcetaje no debe ser mayor al 100%.";
        }
        // Fecha de inicio de validez.
        if (cargo.getFechaInicio() == null) {
            return "La fecha de inicio de validez del cargo no puede ser vacio.";
        }
        // Fecha de fin de validez.
        if (cargo.getFechaFin() == null) {
            return "La fecha de fin de validez del cargo no puede ser vacio.";
        }
        // La fecha de inicio no puede ser igual o anterior a la fecha de fin de validez del cargo.
        Date a = Fechas.combinarFechaHora(cargo.getFechaInicio(), cargo.getHoraInicio());
        Date b = Fechas.combinarFechaHora(cargo.getFechaFin(), cargo.getHoraFin());
        if (Fechas.segundosDiferencia(a, b) <= 0) {
            return "La fecha de fin de validez no puede ser igual o anterior a la fecha de inicio de validez del cargo.";
        }
        return null;
    }

}
