/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers;

import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Units;
import java.util.List;
import javax.inject.Inject;
import py.com.sepsa.genisys.web.controllers.sesion.UsuarioLogueadoMB;

/**
 * ManagedBean básico para Vistas.
 *
 * @author Fabio A. González Sosa
 */
public class BasicViewMB extends BasicMB {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Datos de sesion del usuario actual.
     */
    @Inject
    protected UsuarioLogueadoMB usuarioLogueado;
    //</editor-fold>

    /**
     * Constructor.
     */
    public BasicViewMB() {
        ;
    }

    protected void logError(Result<?> result) {
        Throwable cause = Units.execute(() -> result.failure().cause());
        String message = Units.execute(() -> result.failure().message());
        log.error(cause, message);
    }

    /**
     * Verifica si es que uno y solo un elemento haya sido seleccionado desde
     * una lista utilizada en alguna vista.
     *
     * @param lista Lista que contiene los elementos seleccionables.
     * @return Elemento seleccionado, si hubiere. Caso contrario {@code null}.
     */
    protected <T> T checkSeleccionUnica(ListaMB<T> lista) {
        return checkSeleccionUnica(lista, "Debe seleccionar un elemento de la lista", "Debe seleccionar sólo un elemento de la lista");
    }

    /**
     * Verifica si es que uno y solo un elemento haya sido seleccionado desde
     * una lista utilizada en alguna vista.
     *
     * @param lista Lista que contiene los elementos seleccionables.
     * @param msgNoSeleccion Mensaje a deseplgar en caso de que no se haya
     * seleccionado ningun elemento.
     * @param msgSeleccionSuperada Mensaje a deseplgar en caso de que se haya
     * seleccionado mas de un elemento.
     * @return Elemento seleccionado, si hubiere. Caso contrario {@code null}.
     */
    protected <T> T checkSeleccionUnica(ListaMB<T> lista, String msgNoSeleccion, String msgSeleccionSuperada) {
        // Controlar si se selecciono alguna factura.
        if (lista.hasSelection()) {
            // Seleccion simple.
            if (lista.getSingleSelectedItem() != null) {
                Object elemento = lista.getSingleSelectedItem();
                return (elemento != null) ? (T) elemento : null;
            }

            // Seleccion multiple.
            if (lista.getMultiSelectedItem() != null) {
                List<T> elementos = lista.getMultiSelectedItem();
                if (elementos.size() > 1) {
                    // Solo se puede seleccionar un elemento.
                    mensajes.showGlobalDialogMessage(msgSeleccionSuperada);
                } else if (elementos.size() == 1) {
                    // Se retorna el unico elemento seleccionado.
                    Object elemento = elementos.get(0);
                    return (elemento != null) ? (T) elemento : null;
                }
            }
        } else {
            // No se selecciono ningun elemento.
            mensajes.showGlobalDialogMessage(msgNoSeleccion);
        }

        return null;
    }

    /**
     * Verifica si es al menos un elemento haya sido seleccionado desde una
     * lista utilizada en alguna vista.
     *
     * @param lista Lista que contiene los elementos seleccionables.
     * @param msgNoSeleccion Mensaje a deseplgar en caso de que no se haya
     * seleccionado ningun elemento.
     * @param msgSeleccionSuperada Mensaje a deseplgar en caso de que se haya
     * seleccionado mas de un elemento.
     * @return Elemento seleccionado, si hubiere. Caso contrario {@code null}.
     */
    protected <T> List<T> checkSeleccion(ListaMB<T> lista, String msgNoSeleccion) {
        // Controlar si se selecciono alguna factura.
        if (lista.hasSelection()) {
            // Seleccion simple.
            if (lista.getSingleSelectedItem() != null) {
                Object elemento = lista.getSingleSelectedItem();
                if (elemento != null) {
                    List<T> elementos = Lists.empty();
                    elementos.add((T) elemento);
                    return elementos;
                }
            }

            // Seleccion multiple.
            if (lista.getMultiSelectedItem() != null) {
                List<T> elementos = lista.getMultiSelectedItem();
                if (!elementos.isEmpty()) {
                    return elementos;
                }
            }

            // No se selecciono ningun elemento.
            mensajes.showGlobalDialogMessage(msgNoSeleccion);
        } else {
            // No se selecciono ningun elemento.
            mensajes.showGlobalDialogMessage(msgNoSeleccion);
        }

        return null;
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public UsuarioLogueadoMB getUsuarioLogueado() {
        return usuarioLogueado;
    }

    public void setUsuarioLogueado(UsuarioLogueadoMB usuarioLogueado) {
        this.usuarioLogueado = usuarioLogueado;
    }
    //</editor-fold>

}
