/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.varios;

import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Units;
import fa.gs.utils.misc.numeric.Currency;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.misc.text.Strings;
import fa.gs.utils.misc.text.Text;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Ajax;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraOutput;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.web.controllers.BasicMB;
import py.com.sepsa.genisys.web.controllers.calculadora.Calculador;
import py.com.sepsa.genisys.web.controllers.calculadora.Calculador_BancoRio;
import py.com.sepsa.genisys.web.controllers.calculadora.Calculador_Sepsa;
import py.com.sepsa.genisys.web.controllers.calculadora.CalculadoraInput;
import py.com.sepsa.genisys.web.controllers.sesion.UsuarioLogueadoMB;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("calculadora")
@Dependent
public class CalculadoraMB extends BasicMB {

    @EJB
    private Personas bPersonas;

    @EJB
    private BancoRio bancoRio;

    @Inject
    protected UsuarioLogueadoMB usuarioLogueado;

    @Getter
    @Setter
    private CalculadoraInput input;

    @Getter
    @Setter
    private CalculadoraOutput output;

    /**
     * Calculadora.
     */
    private Calculador calculadora;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        this.input = null;
        this.output = null;
    }

    //<editor-fold defaultstate="collapsed" desc="Obtener Datos de Entrada para Calculo">
    /**
     * Permite inicializar los valores por defecto para el calculo de
     * operaciones de desmbolso.
     *
     * @return Valores por defecto para calculo de desembolsos.
     */
    public CalculadoraInput initCalculadoraInput() {
        Persona persona = usuarioLogueado.getPersona();
        Perfil perfil = usuarioLogueado.getPerfil();
        PerfilEnum perfilEnum = beans.getPerfiles().obtenerPerfil(perfil);
        Date ahora = Fechas.ahora();

        CalculadoraInput instance = new CalculadoraInput();
        instance.setCanLimpiarComprador(true);
        instance.setCanLimpiarProveedor(true);
        instance.setCanLimpiarEntidadFinanciera(true);
        instance.setCanEditFechaPagoFactura(true);
        instance.setCanEditMontoFactura(true);
        instance.setFechaPagoFactura(ahora);
        instance.setFechaAdelantoFactura(ahora);
        instance.setMontoFactura(Numeric.CERO);
        completarPersona(instance, persona, perfilEnum);
        completarPersonas(instance, persona, perfilEnum);
        return instance;
    }

    /**
     * Completa los atributos de comprador, proveedor y entidad financiera por
     * defectos segun el perfil de la persona logueada.
     *
     * @param persona Persona que realiza el calculo.
     * @param perfil Perfil de persona.
     */
    @SuppressWarnings("SillyAssignment")
    private void completarPersona(CalculadoraInput input, Persona persona, PerfilEnum perfilEnum) {
        Persona comprador = (perfilEnum == PerfilEnum.COMPRADOR) ? persona : null;
        Persona proveedor = (perfilEnum == PerfilEnum.PROVEEDOR) ? persona : null;
        Persona entidadFinanciera;
        if (perfilEnum == PerfilEnum.ENTIDAD_FINANCIERA) {
            entidadFinanciera = persona;
        } else {
            entidadFinanciera = null;
        }

        input.setComprador(comprador);
        input.setProveedor(proveedor);
        input.setEntidadFinanciera(entidadFinanciera);
    }

    /**
     * Completa los atributos de compradores, proveedores y entidades
     * financieras por defectos segun el perfil de la persona logueada.
     *
     * @param persona Persona que realiza el calculo.
     * @param perfil Perfil de persona.
     */
    private void completarPersonas(CalculadoraInput input, Persona persona, PerfilEnum perfilEnum) {
        try {
            input.setCompradores(obtenerCompradores(persona, perfilEnum));
            input.setProveedores(obtenerProveedores(persona, perfilEnum));
            input.setEntidadesFinancieras(obtenerEntidadesFinancieras(persona, perfilEnum));
        } catch (Throwable thr) {
            input.setCompradores(Lists.empty());
            input.setProveedores(Lists.empty());
            input.setEntidadesFinancieras(Lists.empty());
        }
    }

    private Collection<Persona> obtenerCompradores(Persona persona, PerfilEnum perfilEnum) throws Throwable {
        Collection<Persona> compradores0 = Lists.empty();

        if (perfilEnum == PerfilEnum.COMPRADOR) {
            // Solo considerar el comprador actual.
            compradores0.add(persona);
        } else if (perfilEnum == PerfilEnum.PROVEEDOR) {
            // Solo considerar compradores que tengan relacion comercial con el proveedor.
            Result<Collection<Persona>> result = bPersonas.obtenerCompradoresDisponibles(persona.getId());
            result.raise();
            Collection<Persona> personas = result.value(null);
            if (personas != null && !personas.isEmpty()) {
                compradores0.addAll(personas);
            }
        } else {
            // Considerar todos los compradores.
            // Aplica para entidades financieras y admin.
            Result<Collection<Persona>> result = bPersonas.obtenerCompradoresDisponibles();
            result.raise();
            Collection<Persona> personas = result.value(null);
            if (personas != null && !personas.isEmpty()) {
                compradores0.addAll(personas);
            }
        }

        return compradores0;
    }

    private Collection<Persona> obtenerProveedores(Persona persona, PerfilEnum perfilEnum) throws Throwable {
        Collection<Persona> proveedores0 = Lists.empty();

        if (perfilEnum == PerfilEnum.COMPRADOR) {
            // Solo considerar proveedores que tengan relacion comercial con el comprador.
            Result<Collection<Persona>> result = bPersonas.obtenerProveedoresDisponibles(persona.getId());
            result.raise();
            Collection<Persona> personas = result.value(null);
            if (personas != null && !personas.isEmpty()) {
                proveedores0.addAll(personas);
            }
        } else if (perfilEnum == PerfilEnum.PROVEEDOR) {
            // Solo considerar el proveedor actual.
            proveedores0.add(persona);
        } else {
            // Considerar todos los proveedores.
            // Aplica para entidades financieras y admin.
            Result<Collection<Persona>> result = bPersonas.obtenerProveedoresDisponibles();
            result.raise();
            Collection<Persona> personas = result.value(null);
            if (personas != null && !personas.isEmpty()) {
                proveedores0.addAll(personas);
            }
        }

        return proveedores0;
    }

    private Collection<Persona> obtenerEntidadesFinancieras(Persona persona, PerfilEnum perfilEnum) throws Throwable {
        Collection<Persona> entidadesFinancieras0 = Lists.empty();

        if (perfilEnum == PerfilEnum.COMPRADOR) {
            // Considerar todas las entidades financieras.
            Result<Collection<Persona>> result = bPersonas.obtenerEntidadesFinancierasDisponibles(persona.getId());
            result.raise();
            Collection<Persona> personas = result.value(null);
            if (personas != null && !personas.isEmpty()) {
                entidadesFinancieras0.addAll(personas);
            }
        } else if (perfilEnum == PerfilEnum.PROVEEDOR) {
            // Considerar todas las entidades financieras.
            Result<Collection<Persona>> result = bPersonas.obtenerEntidadesFinancierasDisponibles(persona.getId());
            result.raise();
            Collection<Persona> personas = result.value(null);
            if (personas != null && !personas.isEmpty()) {
                entidadesFinancieras0.addAll(personas);
            }
        } else if (perfilEnum == PerfilEnum.ENTIDAD_FINANCIERA) {
            // Solo considerar entidad financiera actual.
            entidadesFinancieras0.add(persona);
        } else if (perfilEnum == PerfilEnum.ADMINISTRADOR) {
            // Considerar todas las entidades financieras.
            Result<Collection<Persona>> result = bPersonas.obtenerEntidadesFinancierasDisponibles(persona.getId());
            result.raise();
            Collection<Persona> personas = result.value(null);
            if (personas != null && !personas.isEmpty()) {
                entidadesFinancieras0.addAll(personas);
            }
        }

        return entidadesFinancieras0;
    }
    //</editor-fold>

    public void onEntidadSelectedChanged(AjaxBehaviorEvent event) {
        try {
            if (input.getEntidadFinanciera() != null) {
                // Instanciar nueva calculadora.
                if (bancoRio.esBancoRio(input.getEntidadFinanciera().getId())) {
                    // Banco Rio.
                    calculadora = new Calculador_BancoRio();
                } else {
                    // Sepsa.
                    calculadora = new Calculador_Sepsa();
                }
            } else {
                calculadora = null;
            }
        } catch (Throwable thr) {
            // Error.
            calculadora = null;
            log.warning(thr, "Ocurrió un error seleccionando entidad financiera para cálculo de desembolso de factura.");
        }
    }

    private String validar() {
        // Validar entidad financiera.
        if (input.getEntidadFinanciera() == null) {
            return "Se debe seleccionar una entidad financiera.";
        }

        // Si existe una entidad seleccionada, existe una instancia de calculadora.
        calculadora.setCalculadoraInput(input);
        String err = calculadora.validar();
        if (err != null) {
            return err;
        }

        return null;
    }

    /**
     * Realiza el calculo efectivo de los montos a pagar y descontar de la
     * factura, interactuando con la GUI y mostrando mensajes de error en caso
     * de que asi se requiera.
     */
    public void calcular() {
        try {
            // Validar datos de entrada.
            String err = validar();
            if (err != null) {
                mensajes.addMessageError("Error", err);
                return;
            }

            // Realizar calculos.
            calculadora.setCalculadoraInput(input);
            calculadora.calcular();
            output = calculadora.getCalculadoraOutput();
        } catch (Throwable thr) {
            log.error(thr, "Ocurrió un error realizando calculo de desembolso");
            String err = Text.select(thr.getMessage(), Units.execute("Ocurrió un error realizando calculo de desembolso", () -> thr.getCause().getMessage()));
            mensajes.addMessageError("Error", err);
        } finally {
            // Actualizar vista.
            updateViewCalculadora();
        }
    }

    /**
     * Limpia el formulario de carga de datos en la vista.
     */
    public void limpiar() {
        try {
            output = null;

            if (usuarioLogueado.esComprador()) {
                if (input.getCanLimpiarEntidadFinanciera()) {
                    input.setEntidadFinanciera(null);
                }
                if (input.getCanLimpiarProveedor()) {
                    input.setProveedor(null);
                }
            }

            if (usuarioLogueado.esProveedor()) {
                if (input.getCanLimpiarEntidadFinanciera()) {
                    input.setEntidadFinanciera(null);
                }
                if (input.getCanLimpiarComprador()) {
                    input.setComprador(null);
                }
            }

            if (usuarioLogueado.esEntidadFinanciera()) {
                if (input.getCanLimpiarComprador()) {
                    input.setComprador(null);
                }
                if (input.getCanLimpiarProveedor()) {
                    input.setProveedor(null);
                }
            }

            if (usuarioLogueado.esAdministrador()) {
                if (input.getCanLimpiarComprador()) {
                    input.setComprador(null);
                }
                if (input.getCanLimpiarProveedor()) {
                    input.setProveedor(null);
                }
                if (input.getCanLimpiarEntidadFinanciera()) {
                    input.setEntidadFinanciera(null);
                }
            }

            if (input.isCanEditFechaPagoFactura()) {
                input.setFechaPagoFactura(Fechas.ahora());
            }

            if (input.isCanEditMontoFactura()) {
                input.setMontoFactura(Numeric.CERO);
            }
        } finally {
            // Actualizar vista.
            updateViewCalculadora();
        }
    }

    public String htmlDiasAdelantados() {
        if (output == null) {
            return Text.nd();
        } else {
            return String.valueOf(output.getDiasAdelantados());
        }
    }

    public String htmlPorcentajeTotalDescuento() {
        if (output == null) {
            return Text.nd();
        } else {
            BigDecimal v = output.getPorcentajeTotalComision();
            return Strings.format("%s %%", bancoRio.formatearPorcentaje(v));
        }
    }

    public String htmlMontoTotalDescuento() {
        if (output == null) {
            return Text.nd();
        } else {
            BigDecimal v = output.getMontoTotalComision();
            return Strings.format("Gs. %s", Currency.formatCurrency(v));
        }
    }

    public String htmlMontoTotalDesembolso() {
        if (output == null) {
            return Text.nd();
        } else {
            BigDecimal v = output.getMontoTotalDesembolso();
            return Strings.format("Gs. %s", Currency.formatCurrency(v));
        }
    }

    public String htmlDetalleEntidad() {
        if (output == null) {
            return Text.nd();
        } else {
            try {
                return calculadora.htmlDetallesEntidad();
            } catch (Throwable thr) {
                return "ERROR";
            }
        }
    }

    /**
     * Actualiza la vista general de la calculadora de cargos.
     */
    private void updateViewCalculadora() {
        /**
         * Ajusta la posicion del cuadro de dialogo que contiene a la
         * calculadora cuando esta es llamada desde la vista de lista de
         * facturas.
         */
        Prime.executeJs("ajustarDialogo(PF('calculadora_interes_factura_dialog_dlg'))");

        // Actualiza el formulario de carga de datos.
        Ajax.update("@form");
    }

}
