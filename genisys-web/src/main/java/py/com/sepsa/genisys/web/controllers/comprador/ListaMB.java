/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.comprador;

import fa.gs.criteria.query.Order;
import fa.gs.misc.Assertions;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.query.expressions.literals.SQLStringLiterals;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.Dependent;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.info.PersonaInfo;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.CustomLazyDataModel;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class ListaMB extends py.com.sepsa.genisys.web.controllers.ListaMB<PersonaInfo> implements Serializable {

    @Override
    protected CustomLazyDataModel<PersonaInfo> buildItemsLoader() {
        if (usuarioLogueado.esAdministrador()) {
            return new CompradoresTodosLazyList();
        } else if (usuarioLogueado.esProveedor()) {
            return new CompradoresDeProveedorLazyList(usuarioLogueado.getPersona().getId());
        } else {
            return new CompradoresDefaultLazyList();
        }
    }

    @Override
    protected CustomFilter buildItemsFilter() {
        return new CompradorListFilter();
    }

    /**
     * Clase que encapsula la carga progresiva de facturas desde la BD.
     */
    private class CompradoresDefaultLazyList extends CustomLazyDataModel<PersonaInfo> {

        /**
         * Constructor.
         */
        public CompradoresDefaultLazyList() {
            super(null);
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<PersonaInfo> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            return Lists.empty();
        }

    }

    /**
     * Clase que encapsula la carga progresiva de facturas a mostrar a los
     * compradores.
     */
    private class CompradoresTodosLazyList extends CustomLazyDataModel<PersonaInfo> {

        private Personas personas;

        /**
         * Constructor.
         */
        public CompradoresTodosLazyList() {
            super(null);
            this.personas = Injection.lookup(Personas.class);
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<PersonaInfo> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            // Ordenacion automatica.
            sorts.put("P.razon_social", Order.ASCENDING);
            sorts.put("P.ruc", Order.ASCENDING);

            // Busqueda de registros.
            List<PersonaInfo> data = personas.selectCompradores(null, limit, offset, filter, sorts);
            Integer total = personas.countCompradores(null, filter);
            setRowCount(total);
            return data;
        }

    }

    /**
     * Clase que encapsula la carga progresiva de facturas a mostrar a los
     * compradores.
     */
    private class CompradoresDeProveedorLazyList extends CustomLazyDataModel<PersonaInfo> {

        private Integer idProveedor;

        private Personas personas;

        /**
         * Constructor.
         */
        public CompradoresDeProveedorLazyList(Integer idProveedor) {
            super(null);
            this.idProveedor = idProveedor;
            this.personas = Injection.lookup(Personas.class);
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<PersonaInfo> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            // Ordenacion automatica.
            sorts.put("P.razon_social", Order.ASCENDING);
            sorts.put("P.ruc", Order.ASCENDING);

            // Busqueda de registros.
            List<PersonaInfo> data = personas.selectCompradores(idProveedor, limit, offset, filter, sorts);
            Integer total = personas.countCompradores(idProveedor, filter);
            setRowCount(total);
            return data;
        }

    }

    /**
     * Clase que encapsula los campos y operadores de filtro aplicables a la
     * lista principal de facturas.
     */
    @Data
    public class CompradorListFilter extends CustomFilter {

        private String razonSocial;
        private String ruc;

        @Override
        final public void clearFilterValues() {
            this.razonSocial = "";
            this.ruc = "";
        }

        @Override
        public Map<String, Object> getFilters() {
            Map<String, Object> filters = new HashMap<>();

            // Filtro de razon social.
            if (!Assertions.stringNullOrEmpty(razonSocial)) {
                filters.put("razonSocial", Strings.format(" P.razon_social ilike '%s' ", SQLStringLiterals.likefy(razonSocial)));
            }

            // Filtro de ruc.
            if (!Assertions.stringNullOrEmpty(ruc)) {
                filters.put("ruc", Strings.format(" P.ruc ilike '%s' ", SQLStringLiterals.likefy(ruc)));
            }

            return filters;
        }

    }
}
