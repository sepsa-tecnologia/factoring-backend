package py.com.sepsa.genisys.web.security;

import javax.servlet.Filter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Configuraciones de seguridad
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String PARTIAL_AUTHENTICATION_ROLE_NAME = "ROLE_PARTIAL_AUTHENTICATION";
    private static final String LOGIN_PAGE_URL = "/index.xhtml";
    private static final String FORBIDDEN_PAGE_URL = "/denied.xhtml";
    private static final String ERROR_PAGE_URL = "/error.xhtml";
    private static final String NOT_FOUND_PAGE_URL = "/404.xhtml";
    private static final String HOME_PAGE_URL = "/app/home.xhtml";

    /**
     * Configuraciones de seguridad
     *
     * @param http Seguridad http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/javax.faces.resource/**").permitAll()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/test/**").permitAll()
                .antMatchers("/templates/**").denyAll()
                .antMatchers("/app/account.xhtml").permitAll()
                .antMatchers("/app/faq.xhtml").permitAll()
                .antMatchers("/app/terms.xhtml").permitAll()
                .antMatchers("/sso.xhtml").permitAll()
                .antMatchers(LOGIN_PAGE_URL).permitAll()
                .antMatchers(FORBIDDEN_PAGE_URL).permitAll()
                .antMatchers(NOT_FOUND_PAGE_URL).permitAll()
                .antMatchers(ERROR_PAGE_URL).permitAll()
                .anyRequest().authenticated()
                .and()
                .sessionManagement().maximumSessions(1)
                .and()
                .sessionFixation().changeSessionId()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                .logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .and()
                .exceptionHandling().accessDeniedPage(FORBIDDEN_PAGE_URL)
                .and()
                .headers()
                .frameOptions()
                .sameOrigin()
                .and()
                .formLogin().disable()
                .httpBasic().disable()
                .addFilterBefore(requestAuthFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(requestResetPasswordFilter(), UsernamePasswordAuthenticationFilter.class)
                // TODO: AGREGAR FILTRO DE AUTENTICACION PARCIAL.
                /**
                 * Se deshabilita la proteccion CSRF para evitar la aparicion de
                 * este error cuando se pierde una sesion creada anteriormente.
                 * ERROR:
                 * https://github.com/spring-projects/spring-security/issues/3241.
                 */
                .csrf().disable();
    }

    @Bean
    public Filter requestAuthFilter() throws Exception {
        Filter filter = new RequestAuthenticationFilter();
        return filter;
    }

    @Bean
    public Filter requestResetPasswordFilter() throws Exception {
        Filter filter = new RequestPasswordResetFilter();
        return filter;
    }

}
