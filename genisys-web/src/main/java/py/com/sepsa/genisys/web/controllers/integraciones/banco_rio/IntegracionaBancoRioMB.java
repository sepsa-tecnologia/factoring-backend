/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.integraciones.banco_rio;

import fa.gs.result.simple.Result;
import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.misc.Files;
import java.io.File;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.pojos.DesembolsoBancoRioInfo;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_ActualizarCobrosPendientes;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_ActualizarDesembolsosPendientes;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_ActualizarLiquidacionesPendientes;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_AdherirCompradores;
import py.com.sepsa.genisys.integraciones.banco_rio.PF_BancoRio_AgregarLiquidaciones;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "integracionRio")
@ViewScoped
public class IntegracionaBancoRioMB extends BasicViewMB implements Serializable {

    @Inject
    private TxWrapperWeb tx;

    @Inject
    private BancoRio bancoRio;

    public void actualizarCompradoresNuevos() {
        String traza = Ids.randomUuid();
        log.inicio(traza);
        try {
            PF_BancoRio_AdherirCompradores bean = Injection.lookup(PF_BancoRio_AdherirCompradores.class);
            Result<?> result = tx.execute(() -> {
                bean.adherirCompradores();
                return null;
            });
            result.raise();
            mensajes.showGlobalDialogMessage("Actualización realizada exitosamente");
        } catch (Throwable thr) {
            log.error(thr, "error");
            mensajes.showGlobalDialogMessage("Ocurrió un error actualizando lista de nuevos compradores adheridos por la entidad. REF: %s.", traza);
        } finally {
            log.fin(traza);
        }
    }

    public void actualizarDesembolsosPendientes() {
        String traza = Ids.randomUuid();
        log.inicio(traza);
        try {
            PF_BancoRio_ActualizarDesembolsosPendientes bean = Injection.lookup(PF_BancoRio_ActualizarDesembolsosPendientes.class);
            Result<?> result = tx.execute(() -> {
                bean.work();
                return null;
            });
            result.raise();
            mensajes.showGlobalDialogMessage("Actualización realizada exitosamente");
        } catch (Throwable thr) {
            log.error(thr, "error");
            mensajes.showGlobalDialogMessage("Ocurrió un error actualizando estado de desembolsos pendientes. REF: %s.", traza);
        } finally {
            log.fin(traza);
        }
    }

    public void actualizarFacturasLiquidables() {
        String traza = Ids.randomUuid();
        log.inicio(traza);
        try {
            PF_BancoRio_ActualizarCobrosPendientes bean = Injection.lookup(PF_BancoRio_ActualizarCobrosPendientes.class);
            Result<?> result = tx.execute(() -> {
                bean.work();
                return null;
            });
            result.raise();
            mensajes.showGlobalDialogMessage("Actualización realizada exitosamente");
        } catch (Throwable thr) {
            log.error(thr, "error");
            mensajes.showGlobalDialogMessage("Ocurrió un error actualizando estado de facturas cobradas (listas para liquidar). REF: %s.", traza);
        } finally {
            log.fin(traza);
        }
    }

    public void actualizarLiquidacionesNuevas() {
        String traza = Ids.randomUuid();
        log.inicio(traza);
        try {
            PF_BancoRio_AgregarLiquidaciones bean = Injection.lookup(PF_BancoRio_AgregarLiquidaciones.class);
            Result<?> result = tx.execute(() -> {
                bean.work();
                return null;
            });
            result.raise();
            mensajes.showGlobalDialogMessage("Actualización realizada exitosamente");
        } catch (Throwable thr) {
            log.error(thr, "error");
            mensajes.showGlobalDialogMessage("Ocurrió un error solicitando liquidación de facturas cobradas (listas para liquidar). REF: %s.", traza);
        } finally {
            log.fin(traza);
        }
    }

    public void actualizarLiquidacionesPendientes() {
        String traza = Ids.randomUuid();
        log.inicio(traza);
        try {
            PF_BancoRio_ActualizarLiquidacionesPendientes bean = Injection.lookup(PF_BancoRio_ActualizarLiquidacionesPendientes.class);
            bean.work();
            Result<?> result = tx.execute(() -> {
                bean.work();
                return null;
            });
            result.raise();
            mensajes.showGlobalDialogMessage("Actualización realizada exitosamente");
        } catch (Throwable thr) {
            log.error(thr, "error");
            mensajes.showGlobalDialogMessage("Ocurrió un error actualizando de liquidaciones pendientes de aprobación por la entidad. REF: %s.", traza);
        } finally {
            log.fin(traza);
        }
    }

    public void descargarListaOperacionesMes() {
        try {
            Collection<DesembolsoBancoRioInfo> operaciones = bancoRio.listarOperacionesMes();
            descargarOperaciones(operaciones, "operaciones_mes.csv");
        } catch (Throwable thr) {
            log.error(thr, "No se pudo obtener la lista de operaciones.");
            mensajes.showGlobalDialogMessage("No se pudo obtener la lista de operaciones.");
        }
    }

    public void descargarListaOperacionesALaFecha() {
        try {
            Collection<DesembolsoBancoRioInfo> operaciones = bancoRio.listarOperacionesALaFecha();
            descargarOperaciones(operaciones, "operaciones_a_la_fecha.csv");
        } catch (Throwable thr) {
            log.error(thr, "No se pudo obtener la lista de operaciones.");
            mensajes.showGlobalDialogMessage("No se pudo obtener la lista de operaciones.");
        }
    }

    private void descargarOperaciones(Collection<DesembolsoBancoRioInfo> operaciones, String filename) {
        try {
            File file = Files.createTemporary();
            try (PrintWriter writer = Files.getWriter(file, StandardCharsets.UTF_8)) {
                writer.append("SEP=;\n");
                writer.append("factura_id;factura_estado_id;factura_estado;estado_desembolso;estado_liquidacion;factura_nro;factura_fecha_emision;factura_fecha_pago;factura_razon_social_comprador;factura_ruc_comprador;factura_razon_social_proveedor;factura_ruc_proveedor;factura_monto_total;factura_monto_conformado;factura_monto_adelantado;factura_monto_distribucion;factura_monto_distribucion_sepsa;factura_monto_distribucion_comprador;factura_dias_anticipo;banco_id_solicitud;banco_nro_operacion;banco_fecha_solicitud_desembolso;banco_hora_solicitud_desembolso;banco_fecha_ultima_consulta_estado_desembolso;banco_hora_ultima_consulta_estado_desembolso;banco_fecha_solicitud_liquidacion;banco_hora_solicitud_liquidacion;banco_fecha_ultima_consulta_estado_liquidacion;banco_hora_ultima_consulta_estado_liquidacion;codigo_liquidacion_sepsa;codigo_liquidacion_comprador;codigo_liquidacion_proveedor;codigo_liquidacion_entidad\n");
                for (DesembolsoBancoRioInfo operacion : operaciones) {
                    writer.append(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s\n",
                            operacion.getFacturaId(),
                            operacion.getFacturaEstadoId(),
                            operacion.getFacturaEstado(),
                            operacion.getEstadoDesembolso(),
                            operacion.getEstadoLiquidacion(),
                            operacion.getFacturaNro(),
                            operacion.getFacturaFechaEmision(),
                            operacion.getFacturaFechaPago(),
                            operacion.getFacturaRazonSocialComprador(),
                            operacion.getFacturaRucComprador(),
                            operacion.getFacturaRazonSocialProveedor(),
                            operacion.getFacturaRucProveedor(),
                            operacion.getFacturaMontoTotal(),
                            operacion.getFacturaMontoConformado(),
                            operacion.getFacturaMontoAdelantado(),
                            operacion.getFacturaMontoDistribucion(),
                            operacion.getFacturaMontoDistribucionSepsa(),
                            operacion.getFacturaMontoDistribucionComprador(),
                            operacion.getFacturaDiasAnticipo(),
                            operacion.getBancoIdSolicitud(),
                            operacion.getBancoNroOperacion(),
                            Fechas.format(operacion.getBancoFechaSolicitudDesembolso(), "yyyy-MM-dd"),
                            Fechas.format(operacion.getBancoFechaSolicitudDesembolso(), "HH:mm"),
                            Fechas.format(operacion.getBancoFechaUltimaConsultaEstadoDesembolso(), "yyyy-MM-dd"),
                            Fechas.format(operacion.getBancoFechaUltimaConsultaEstadoDesembolso(), "HH:mm"),
                            Fechas.format(operacion.getBancoFechaSolicitudLiquidacion(), "yyyy-MM-dd"),
                            Fechas.format(operacion.getBancoFechaSolicitudLiquidacion(), "HH:mm"),
                            Fechas.format(operacion.getBancoFechaUltimaConsultaEstadoLiquidacion(), "yyyy-MM-dd"),
                            Fechas.format(operacion.getBancoFechaUltimaConsultaEstadoLiquidacion(), "HH:mm"),
                            operacion.getCodigoLiquidacionSepsa(),
                            operacion.getCodigoLiquidacionComprador(),
                            operacion.getCodigoLiquidacionProveedor(),
                            operacion.getCodigoLiquidacionEntidad()
                    ));
                }
                writer.flush();
            }
            Jsf.sendFile(file, filename, (int) file.length(), "text/csv");
        } catch (Throwable thr) {
            log.error(thr, "No se pudo obtener la lista de usuarios activos.");
            mensajes.showGlobalDialogMessage("No se pudo obtener la lista de usuarios activos.");
        }
    }

}
