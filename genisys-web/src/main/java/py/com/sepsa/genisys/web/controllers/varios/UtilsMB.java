/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.varios;

import java.io.Serializable;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.entities.info.CuentaEntidadFinanciera;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Text;

/**
 *
 * @author Luz Ramirez
 */
@Named(value = "utils")
@ApplicationScoped
public class UtilsMB implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Meses del año como texto.
     */
    private String[] meses;

    /**
     * Coleccion de años contando desde el actual.
     */
    private Integer[] anhos;
    //</editor-fold>

    @PostConstruct
    public void init() {
        // Meses del año.
        meses = new String[]{"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"};

        // Siguientes 10 años contanto el actual.
        anhos = new Integer[10];
        for (int y = Calendar.getInstance().get(Calendar.YEAR), i = 0; i < anhos.length; i++) {
            anhos[i] = y + i;
        }
    }

    /**
     * Enmascara un valor entero utilizando un algoritmo de cifrado.
     *
     * @param value Valor a enmascarar.
     * @return Cadena hexadecimal que representa el valor enmascarado.
     */
    public String maskInteger(Integer value) {
        return Ids.mask(value);
    }

    /**
     * Desenmascara una cadena hexadecimal obtenido mediante un llamado a
     * {@link py.com.sepsa.genisys.web.controllers.varios.UtilsMB#maskInteger(java.lang.Integer) maskInteger}.
     *
     * @param value Cadena hexadecimal que representa el valor enmascarado.
     * @return Valor entero desenmascarado.
     */
    public Integer unmaskInteger(String value) {
        return Ids.unmask(value);
    }

    /**
     * Permite generar una cadena simple de texto en base a una cadena que
     * representa la descripcion de un perfil en base de datos.
     *
     * @param perfil Descripcion de un perfil en base de datos.
     * @return Cadena simple de texto para mejor interpretacion de la
     * descripcion del perfil.
     */
    public String perfilAsSimpleText(String perfil) {
        return PerfilEnum.descripcionAsSimpleText(perfil);
    }

    /**
     * Metodo que permite obtener una representacion compleja en forma de texto
     * (HTML) de un objeto Cuenta para su presentacion en la vista.
     *
     * @param cuenta Objeto cuenta. cadena simple o compleja (como el caso de
     * html).
     * @return Cadena HTML que representa al objeto.
     */
    public String cuentaLabel(CuentaEntidadFinanciera cuenta) {
        if (cuenta == null) {
            return "";
        } else {
            String html = "<div><h4 class=\"list-group-item-heading\">%s</h4><p class=\"list-group-item-text\">%s</p></div>";
            return String.format(html, "Cuenta #" + cuenta.getNroCuenta(), "en " + cuenta.getRazonSocialEntidadFinanciera());
        }
    }

    /**
     * Metodo que permite obtener una representacion compleja en forma de texto
     * (HTML) de un objeto Persona para su presentacion en la vista.
     *
     * @param persona Objeto persona.
     * @return Cadena HTML que representa al objeto.
     */
    public String personaLabel(Persona persona) {
        String html = "<div><h4 class=\"list-group-item-heading\">%s</h4><p class=\"list-group-item-text\">%s</p></div>";
        if (persona == null) {
            return String.format(html, " ", " ");
        } else {
            return String.format(html, persona.toString(), "RUC: " + getRuc(persona));
        }
    }

    /**
     * Permite obtener la razon social de una persona como cadena.
     *
     * @param persona Persona.
     * @return Razon Social como cadena.
     */
    public String getRazonSocial(Persona persona) {
        return Text.razonSocial(persona);
    }

    /**
     * Permite obtener el ruc de una persona como cadena.
     *
     * @param persona Persona.
     * @return Ruc como cadena.
     */
    public String getRuc(Persona persona) {
        return Text.ruc(persona);
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public String[] getMeses() {
        return meses;
    }

    public void setMeses(String[] meses) {
        this.meses = meses;
    }

    public Integer[] getAnhos() {
        return anhos;
    }

    public void setAnhos(Integer[] anhos) {
        this.anhos = anhos;
    }
    //</editor-fold>

}
