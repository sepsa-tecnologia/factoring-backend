/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.utils;

import java.util.Map;

/**
 * Clase base que implementa filtros de datos para ser utilizados en conjunto
 * con el modelo de datos para tablas.
 *
 * @author Fabio A. González Sosa
 */
public abstract class CustomFilter {

    /**
     * Bandera que indica si los filtros estan activos o no.
     */
    private Boolean isEnabled;

    /**
     * Constructor.
     */
    public CustomFilter() {
        isEnabled = false;
    }

    /**
     * Obtiene los filtros a ser aplicados organizados en pares {nombre filtro,
     * valor}.
     *
     * @return Filtros.
     */
    public abstract Map<String, Object> getFilters();

    /**
     * Limpia los valores de filtro posiblemente establecidos previamente.
     */
    public abstract void clearFilterValues();

    /**
     * Indica si los filtros estan habilitados o no.
     *
     * @return True si los filtros estan habilitados. False caso contrario.
     */
    public Boolean isEnabled() {
        return isEnabled;
    }

    /**
     * Modifica el estado interno de los filtros para habilitarlos.
     */
    public void enable() {
        isEnabled = true;
    }

    /**
     * Modifica el estado interno de los filtros para deshabilitarlos.
     */
    public void disable() {
        isEnabled = false;
    }
}
