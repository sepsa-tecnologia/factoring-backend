/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.personas;

import fa.gs.result.simple.Result;
import fa.gs.utils.jsf.Jsf;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ValidationException;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "cambiarPersoneria")
@ViewScoped
public class PersonaCambiarPersoneriaMB extends BasicViewMB implements Serializable {

    @Inject
    private TxWrapperWeb tx;

    @Getter
    @Setter
    private PersonaCambiarPersoneriaMB.Input input;

    @EJB
    private Personas personas;

    private Persona persona;

    @PostConstruct
    public void init() {
        // Inicializar persona.
        Integer idPersona = Ids.unmask(Jsf.getRequestParameter("p"));
        this.persona = personas.obtenerPorId(idPersona).value();

        // Inicializar campos de entrada.
        initInput();
    }

    private void initInput() {
        Personas.TipoPersonaEnum tipoPersona = Personas.TipoPersonaEnum.fromIdTipoPersona(persona.getIdTipoPersona());
        this.input = new PersonaCambiarPersoneriaMB.Input();
        input.esPersonaFisica = tipoPersona == Personas.TipoPersonaEnum.FISICA;
        input.esPersonaJuridica = tipoPersona == Personas.TipoPersonaEnum.JURIDICA;
    }

    public void guardar() {
        Result<Void> result = tx.execute(this::guardar0);
        if (result.isFailure()) {
            // Error.
            String msg = result.failure().cause().getCause().getMessage();
            mensajes.showGlobalDialogMessage(msg);
        } else {
            // Limpiar campos de entrada.
            mensajes.showGlobalDialogMessage("Datos guardados exitosamente");
        }

        updateForm();
    }

    private Void guardar0() throws Throwable {
        // Validar datos de entrada.
        String err = validarInput(input);
        if (err != null) {
            throw new ValidationException(err);
        }

        Personas.TipoPersonaEnum tipoPersona = input.esPersonaFisica ? Personas.TipoPersonaEnum.FISICA : Personas.TipoPersonaEnum.JURIDICA;
        String razonSocial = Text.razonSocial(persona);
        String ruc = Text.ruc(persona);
        personas.cambiarPersoneria(persona.getId(), tipoPersona, razonSocial, ruc);
        return null;
    }

    private String validarInput(PersonaCambiarPersoneriaMB.Input input) {
        if (input.esPersonaFisica == false && input.esPersonaJuridica == false) {
            return "Se debe seleccionar un tipo de persona";
        }

        if (input.esPersonaFisica == true && input.esPersonaJuridica == true) {
            return "Se debe seleccionar un solo tipo de persona";
        }

        return null;
    }

    public void onPersonaFisicaChanged() {
        this.input.esPersonaJuridica = !this.input.esPersonaFisica;
    }

    public void onPersonaJuridicaChanged() {
        this.input.esPersonaFisica = !this.input.esPersonaJuridica;
    }

    private void updateForm() {
        Prime.update("cambiar_personeria_form");
    }

    public String getRuc() {
        return Text.ruc(persona);
    }

    @Data
    public static class Input implements Serializable {

        private boolean esPersonaFisica;
        private boolean esPersonaJuridica;

        public Input() {
            this.esPersonaFisica = false;
            this.esPersonaJuridica = true;
        }
    }

}
