/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.usuarios;

import fa.gs.criteria.query.Order;
import fa.gs.misc.Assertions;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.database.query.expressions.literals.SQLStringLiterals;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.Dependent;
import lombok.Data;
import py.com.sepsa.genisys.ejb.logic.impl.Usuarios;
import py.com.sepsa.genisys.ejb.logic.pojos.UsuarioInfo;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.CustomLazyDataModel;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
@Dependent
public class ListaMB extends py.com.sepsa.genisys.web.controllers.ListaMB<UsuarioInfo> implements Serializable {

    @Override
    protected CustomLazyDataModel<UsuarioInfo> buildItemsLoader() {
        if (usuarioLogueado.esAdministrador()) {
            return new UsuariosLazyList();
        } else {
            return new UsuariosDefaultLazyList();
        }
    }

    @Override
    protected CustomFilter buildItemsFilter() {
        return new UsuarioListFilter();
    }

    /**
     * Clase que encapsula la carga progresiva de facturas desde la BD.
     */
    private class UsuariosDefaultLazyList extends CustomLazyDataModel<UsuarioInfo> {

        /**
         * Constructor.
         */
        public UsuariosDefaultLazyList() {
            super(null);
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<UsuarioInfo> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            return Lists.empty();
        }

    }

    /**
     * Clase que encapsula la carga progresiva de facturas a mostrar a los
     * compradores.
     */
    private class UsuariosLazyList extends CustomLazyDataModel<UsuarioInfo> {

        private final Usuarios usuarios;

        /**
         * Constructor.
         */
        public UsuariosLazyList() {
            super(null);
            this.usuarios = Injection.lookup(Usuarios.class);
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<UsuarioInfo> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            // Ordenacion automatica.
            sorts.put("u.usuario", Order.ASCENDING);

            // Busqueda de registros.
            List<UsuarioInfo> data = usuarios.selectUsuarios(limit, offset, filter, sorts);
            Integer total = usuarios.countUsuarios(filter);
            setRowCount(total);
            return data;
        }

    }

    /**
     * Clase que encapsula los campos y operadores de filtro aplicables a la
     * lista principal de facturas.
     */
    @Data
    public class UsuarioListFilter extends CustomFilter {

        private Integer id;
        private String usuario;
        private String cliente;
        private String ruc;

        @Override
        final public void clearFilterValues() {
            this.id = null;
            this.usuario = "";
            this.cliente = "";
            this.ruc = "";
        }

        @Override
        public Map<String, Object> getFilters() {
            Map<String, Object> filters = new HashMap<>();

            // Filtro de id.
            if (!Assertions.isNull(id)) {
                filters.put("id", Strings.format(" u.id = %s ", id));
            }

            // Filtro de usuario.
            if (!Assertions.stringNullOrEmpty(usuario)) {
                filters.put("usuario", Strings.format(" u.usuario ilike '%s' ", SQLStringLiterals.likefy(usuario)));
            }

            // Filtro de cliente.
            if (!Assertions.stringNullOrEmpty(cliente)) {
                String a = SQLStringLiterals.likefy(cliente);
                filters.put("cliente", Strings.format(" (t0.razon_social ilike '%s' or t0.nombre_fantasia ilike '%s') ", a, a));
            }

            // Filtro de ruc.
            if (!Assertions.stringNullOrEmpty(ruc)) {
                String a = SQLStringLiterals.likefy(ruc);
                filters.put("cliente", Strings.format(" t0.ruc ilike '%s' ", a));
            }

            return filters;
        }

    }
}
