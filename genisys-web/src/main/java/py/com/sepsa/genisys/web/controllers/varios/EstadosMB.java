/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.varios;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.entities.factoring.Estado;
import py.com.sepsa.genisys.ejb.logic.impl.Entidades;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Estados.EstadoEnum;
import py.com.sepsa.genisys.web.controllers.sesion.UsuarioLogueadoMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "estados")
@SessionScoped
public class EstadosMB implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Codigos de colores correspondientes a cada identificador de estado,
     * ordenado segun orden de aparicion en tabla.
     */
    private static final Map<Estados.EstadoFacturaEnum, String> COLORES_ESTADO_FACTURA;

    /**
     * Codigos de colores corespondientes a cada estado posible para las
     * relacion entre una persona (comprador/proveedor) con una entidad
     * financiera.
     */
    private static final Map<String, String> COLORES_ESTADO_RELACION_PERSONA_ENTIDAD;

    static {
        COLORES_ESTADO_FACTURA = new HashMap<>();
        COLORES_ESTADO_FACTURA.put(Estados.EstadoFacturaEnum.DESCONOCIDO, "#AAB2BD");                   // gray
        COLORES_ESTADO_FACTURA.put(Estados.EstadoFacturaEnum.CONFORMACION_SOLICITADA, "#F6BB42");       // sunflower
        COLORES_ESTADO_FACTURA.put(Estados.EstadoFacturaEnum.NO_CONFORMADA, "#3BAFDA");                 // aqua
        COLORES_ESTADO_FACTURA.put(Estados.EstadoFacturaEnum.CONFORMADA, "#3F51B5");                    // purple
        COLORES_ESTADO_FACTURA.put(Estados.EstadoFacturaEnum.VENDIDA, "#967ADC");                       // lavender
        COLORES_ESTADO_FACTURA.put(Estados.EstadoFacturaEnum.PAGADA, "#4CAF50");                        // green
        COLORES_ESTADO_FACTURA.put(Estados.EstadoFacturaEnum.RECHAZADA_POR_COMPRADOR, "#F44336");       // red
        COLORES_ESTADO_FACTURA.put(Estados.EstadoFacturaEnum.RECHAZADA_POR_PROVEEDOR, "#F44336");       // red
        COLORES_ESTADO_FACTURA.put(Estados.EstadoFacturaEnum.RECHAZADA_POR_ENTIDAD, "#F44336");         // red

        COLORES_ESTADO_RELACION_PERSONA_ENTIDAD = new HashMap();
        COLORES_ESTADO_RELACION_PERSONA_ENTIDAD.put(Entidades.EstadoAdhesionEntidad.INDEFINIDO.codigo(), "#AAB2BD");
        COLORES_ESTADO_RELACION_PERSONA_ENTIDAD.put(Entidades.EstadoAdhesionEntidad.PENDIENTE.codigo(), "#AAB2BD");
        COLORES_ESTADO_RELACION_PERSONA_ENTIDAD.put(Entidades.EstadoAdhesionEntidad.ACTIVO.codigo(), "#4CAF50");
        COLORES_ESTADO_RELACION_PERSONA_ENTIDAD.put(Entidades.EstadoAdhesionEntidad.RECHAZADO.codigo(), "#F44336");
    }

    /**
     * Datos de sesion del usuario actual.
     */
    @Inject
    private UsuarioLogueadoMB usuarioLogueado;

    /**
     * Estados disponibles en el sistema.
     */
    @EJB
    private Estados estados;
    // </editor-fold>

    /**
     * Constructor.
     */
    public EstadosMB() {
        ;
    }

    /**
     * Inicializa los valores del controlador.
     */
    @PostConstruct
    public void init() {
        // Inicializacion "eager" del bean.
        estados.activate();
    }

    /**
     * Obtiene un estado dado caracterizado por su identificador.
     *
     * @param id Identificador del estado.
     * @return Objeto estado asociado al identificador, null si no existe.
     */
    public Estado getEstado(Integer id) {
        return estados.findById(id);
    }

    /**
     * Obtiene una lista de objetos estados a partir de una serie de
     * identificadores.
     *
     * @param ids Serie de identificadores de estados.
     * @return Lista de estados correspondiente a cada identificador valido.
     */
    public List<Estado> getEstados(Integer... ids) {
        return Arrays.asList(ids)
                .stream()
                .map(id -> getEstado(id))
                .filter(e -> e != null)
                .collect(Collectors.toList());
    }

    /**
     * Obtiene una lista de objetos estados a partir de una serie de
     * enumeraciones de estado.
     *
     * @param enums Serie de enumeraciones de estados.
     * @return Lista de estados correspondiente a cada enumeracion valida.
     */
    public List<Estado> getEstados(Estados.EstadoEnum... enums) {
        Integer[] ids = Arrays.asList(enums)
                .stream()
                .map(ee -> ee.getId())
                .toArray(Integer[]::new);
        return getEstados(ids);
    }

    /**
     * Obtiene un estado dado caracterizado por una enumeracion que representa
     * el estado en si.
     *
     * @param estado Enumeracion de Estado.
     * @return Entidad estado.
     */
    public Estado getEstado(EstadoEnum estado) {
        return estados.find(estado);
    }

    /**
     * Retorna los identificadores de estado de factura disponibles para el
     * usuario logueado en base al perfil que posee.
     *
     * @return Lista de estados de factura que puede observar el usuario
     * logueado.
     */
    public Integer[] getEnabledEstadosFactura() {
        Estados.EstadoFacturaEnum[] estadosValidos;
        if (usuarioLogueado.esComprador()) {
            estadosValidos = new Estados.EstadoFacturaEnum[]{
                Estados.EstadoFacturaEnum.NO_CONFORMADA,
                Estados.EstadoFacturaEnum.CONFORMACION_SOLICITADA,
                Estados.EstadoFacturaEnum.CONFORMADA,
                Estados.EstadoFacturaEnum.RECHAZADA_POR_COMPRADOR,
                Estados.EstadoFacturaEnum.RECHAZADA_POR_PROVEEDOR,
                Estados.EstadoFacturaEnum.VENDIDA,
                Estados.EstadoFacturaEnum.PAGADA,
                Estados.EstadoFacturaEnum.EN_ESTUDIO_POR_ENTIDAD,
                Estados.EstadoFacturaEnum.COBRADA,
                Estados.EstadoFacturaEnum.LIQUIDADA
            };
        } else if (usuarioLogueado.esProveedor()) {
            estadosValidos = new Estados.EstadoFacturaEnum[]{
                Estados.EstadoFacturaEnum.NO_CONFORMADA,
                Estados.EstadoFacturaEnum.CONFORMACION_SOLICITADA,
                Estados.EstadoFacturaEnum.CONFORMADA,
                Estados.EstadoFacturaEnum.RECHAZADA_POR_COMPRADOR,
                Estados.EstadoFacturaEnum.RECHAZADA_POR_PROVEEDOR,
                Estados.EstadoFacturaEnum.RECHAZADA_POR_ENTIDAD,
                Estados.EstadoFacturaEnum.VENDIDA,
                Estados.EstadoFacturaEnum.PAGADA,
                Estados.EstadoFacturaEnum.EN_ESTUDIO_POR_ENTIDAD,
                Estados.EstadoFacturaEnum.COBRADA
            };
        } else if (usuarioLogueado.esEntidadFinanciera()) {
            estadosValidos = new Estados.EstadoFacturaEnum[]{
                Estados.EstadoFacturaEnum.RECHAZADA_POR_ENTIDAD,
                Estados.EstadoFacturaEnum.VENDIDA,
                Estados.EstadoFacturaEnum.PAGADA,
                Estados.EstadoFacturaEnum.COBRADA,
                Estados.EstadoFacturaEnum.EN_ESTUDIO_POR_ENTIDAD,
                Estados.EstadoFacturaEnum.LIQUIDADA
            };
        } else {
            estadosValidos = Estados.EstadoFacturaEnum.values();
        }

        return Arrays.asList(estadosValidos)
                .stream()
                .map(ee -> ee.getId())
                .toArray(Integer[]::new);
    }

    public Entidades.EstadoAdhesionEntidad[] getEnabledEstadoAdhesionEntidad() {
        return Lists.stream(Entidades.EstadoAdhesionEntidad.values())
                .filter(e -> e != Entidades.EstadoAdhesionEntidad.INDEFINIDO)
                .toArray(Entidades.EstadoAdhesionEntidad[]::new);
    }

    /**
     * Obtiene un color relacionado para identificar a un tipo de estado
     * específico.
     *
     * @param id Identificador del estado.
     * @return Código hexadecimal RGB del color asociado al estado.
     */
    public String getColor(Integer id) {
        try {
            Estados.EstadoFacturaEnum estado = Estados.EstadoFacturaEnum.fromId(id);
            return COLORES_ESTADO_FACTURA.get(estado);
        } catch (Throwable thr) {
            return COLORES_ESTADO_FACTURA.get(Estados.EstadoFacturaEnum.DESCONOCIDO);
        }
    }

    public String getColorEstadoPersonEntidad(String estado) {
        try {
            return COLORES_ESTADO_RELACION_PERSONA_ENTIDAD.get(estado);
        } catch (Throwable thr) {
            return COLORES_ESTADO_RELACION_PERSONA_ENTIDAD.get("-");
        }
    }

    public String getTextoEstadoPersonaEntidad(String estado) {
        Entidades.EstadoAdhesionEntidad estado0 = Entidades.EstadoAdhesionEntidad.fromEstado(estado);
        return getHumanTextEstadoAdhesionEntidad(estado0);
    }

    public String getHumanText(Integer idEstado) {
        Estados.EstadoFacturaEnum estadoEnum = Estados.EstadoFacturaEnum.fromId(idEstado);
        return getHumanText(estadoEnum);
    }

    public String getHumanTextEstadoAdhesionEntidad(Entidades.EstadoAdhesionEntidad estado) {
        switch (estado) {
            case INDEFINIDO:
                return "INDEFINIDO";
            case PENDIENTE:
                return "PENDIENTE";
            case ACTIVO:
                return "ACTIVO";
            case RECHAZADO:
                return "RECHAZADO";
            default:
                throw Errors.illegalState();
        }
    }

    public String getHumanText(Estados.EstadoFacturaEnum estado) {
        Estado e = EstadosMB.this.getEstado(estado);
        if (usuarioLogueado.esComprador()) {
            switch (estado) {
                case PAGADA:
                    return "DESEMBOLSADA";
                case COBRADA:
                    return "PAGADA";
                default:
                    return getDescripcion(e);
            }
        }

        if (usuarioLogueado.esProveedor()) {
            switch (estado) {
                case PAGADA:
                    return "DESEMBOLSADA";
                case EN_ESTUDIO_POR_ENTIDAD:
                    return "EN ESTUDIO";
                default:
                    return getDescripcion(e);
            }
        }

        if (usuarioLogueado.esEntidadFinanciera()) {
            switch (estado) {
                case VENDIDA:
                    return "A DESEMBOLSAR";
                case PAGADA:
                    return "DESEMBOLSADA";
                case EN_ESTUDIO_POR_ENTIDAD:
                    return "EN ESTUDIO";
                default:
                    return getDescripcion(e);
            }
        }

        return getDescripcion(e);
    }

    private String getDescripcion(Estado estado) {
        if (estado == null) {
            return "--";
        }

        String descripcion = estado.getDescripcion();
        if (descripcion == null || descripcion.isEmpty()) {
            return "--";
        }
        return descripcion.replace('_', ' ').toUpperCase();
    }

    // <editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public List<Estado> getEstados() {
        return estados.getEstados();
    }
    // <editor-fold/>

}
