/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.sesion;

import fa.gs.utils.collections.Lists;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.entities.info.ContactoEmail;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.logic.impl.Emails;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.pojos.DireccionInfo;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ContactoInfo;
import py.com.sepsa.genisys.web.controllers.BasicMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "usuarioLogueado")
@SessionScoped
public class UsuarioLogueadoMB extends BasicMB {

    @EJB
    private Personas personas;

    @EJB
    private Emails emails;

    @Inject
    private SesionMB sesion;

    /**
     * Usuario actual.
     */
    protected Usuario usuario;

    /**
     * Perfil del usuario actual.
     */
    protected Perfil perfil;

    /**
     * Perfiles disponibles del usuario actual.
     */
    protected Collection<Perfil> perfiles;

    /**
     * Entidad Persona del usuario actual.
     */
    protected Persona persona;

    /**
     * Direcciones de correo a las cuales se puede contactar con el usuario
     * logueado.
     */
    protected Collection<ContactoEmail> contactosEmail;

    /**
     * Datos de direccion del usuario logueado.
     */
    protected DireccionInfo direccion;

    /**
     * Datos de contacto del usuario logueado.
     */
    protected ContactoInfo contacto;
    // </editor-fold>

    /**
     * Constructor.
     */
    public UsuarioLogueadoMB() {
        ;
    }

    /**
     * Inicializa los valores de la vista.
     */
    @PostConstruct
    public void init() {
        load();
    }

    /**
     * Obtiene la informacion disponible para el usuario logueado.
     */
    private void load() {
        // Informacion de autenticacion.
        UserAuthenticationInfo info = sesion.getCurrentUserInfo();

        // Usuario activo.
        usuario = info.getMUsuario();

        // Perfiles disponibles.
        perfiles = info.getMPerfiles();

        // Perfil a utilizar.
        perfil = (perfiles.isEmpty() || perfiles.size() > 1) ? null : Lists.first(perfiles);

        // Empresa (persona) correspondiente al usuario.
        persona = personas.obtenerPorId(usuario.getIdPersona()).value();

        // Contactos de usuario.
        contactosEmail = emails.obtenerContactosEmailDisponibles(persona.getId());

        // Direccion de usuario.
        direccion = personas.obtenerDatosDireccion(persona.getId()).value();

        // Contacto de usuario.
        contacto = personas.obtenerDatosContacto(persona.getId()).value();
    }

    /**
     * Vuelve a cargar e inicializar los datos del usuario logueado.
     */
    public void reload() {
        load();
    }

    /**
     * Verifica si el usuario actualmente logueado tiene perfil COMPRADOR.
     *
     * @return True si el usuario es comprador. False caso contrario.
     */
    public boolean esComprador() {
        return tienePerfil("comprador");
    }

    /**
     * Verifica si el usuario actualmente logueado tiene perfil PROVEEDOR.
     *
     * @return True si el usuario es comprador. False caso contrario.
     */
    public boolean esProveedor() {
        return tienePerfil("proveedor");
    }

    /**
     * Verifica si el usuario actualmente logueado tiene perfil ENTIDAD
     * FINANCIERA.
     *
     * @return True si el usuario es comprador. False caso contrario.
     */
    public boolean esEntidadFinanciera() {
        return tienePerfil("entidad_financiera");
    }

    /**
     * Verifica si el usuario actualmente logueado tiene perfil ADMINISTRADOR.
     *
     * @return True si el usuario es comprador. False caso contrario.
     */
    public boolean esAdministrador() {
        return tienePerfil("administrador");
    }

    /**
     * Verifica si el usuario actualmente logqueado posee el perfil indicado.
     *
     * @param perfil Perfil que se desea consultar.
     * @return True si el usuario posee el perfil. False caso contrario.
     */
    public boolean tienePerfil(String perfil) {
        if (usuario != null && this.perfil != null) {
            String a = this.perfil.getDescripcion().toLowerCase();
            String b = perfil.toLowerCase();
            return a.contains(b);
        }
        return false;
    }

    /**
     * Obtiene una cadena representando el perfil actual del usuario logueado.
     *
     * @return Descripción del perfil.
     */
    public String htmlPerfil() {
        if (esComprador()) {
            return "Comprador";
        } else if (esProveedor()) {
            return "Proveedor";
        } else if (esEntidadFinanciera()) {
            return "Entidad Financiera";
        } else if (esAdministrador()) {
            return "Administrador de Sistema";
        } else {
            return "Perfil Desconocido";
        }
    }

    public Personas.TipoPersonaEnum getTipoPersona() {
        return Personas.TipoPersonaEnum.fromIdTipoPersona(persona.getIdTipoPersona());
    }

    public DireccionInfo getDireccion() {
        return direccion;
    }

    public ContactoInfo getContacto() {
        return contacto;
    }

    public boolean esPersonaFisica() {
        return getTipoPersona() == Personas.TipoPersonaEnum.FISICA;
    }

    public boolean esPersonaJuridica() {
        return getTipoPersona() == Personas.TipoPersonaEnum.JURIDICA;
    }

    public String htmlPersonaTipo() {
        return Text.tipoPersona(persona);
    }

    public String htmlPersonaRuc() {
        return Text.ruc(persona);
    }

    public String htmlPersonaRazonSocial() {
        return Text.razonSocial(persona);
    }

    public String htmlPersonaJuridicaRazonSocial() {
        return persona.getPjRazonSocial();
    }

    public String htmlPersonaJuridicaNombreFantasia() {
        return persona.getPjNombreFantasia();
    }

    public String htmlPersonaFisicaNombres() {
        return persona.getPfNombres();
    }

    public String htmlPersonaFisicaApellidos() {
        return persona.getPfApellidos();
    }

    public String htmlPersonaFisicaCi() {
        return persona.getPfCi();
    }

    public String htmlPersonaBarrio() {
        return direccion.getBarrio();
    }

    // <editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Collection<Perfil> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List<Perfil> perfiles) {
        this.perfiles = perfiles;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Collection<ContactoEmail> getContactosEmail() {
        return contactosEmail;
    }

    public void setContactosEmail(Collection<ContactoEmail> contactosEmail) {
        this.contactosEmail = contactosEmail;
    }
    // </editor-fold>

}
