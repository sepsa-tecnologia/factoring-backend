/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.factura;

import fa.gs.misc.fechas.Fechas;
import fa.gs.utils.misc.numeric.Currency;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.PF_FacturaVentaLote_Sepsa;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculoDesembolsoFacturaInfo;
import py.com.sepsa.genisys.ejb.logic.pojos.SolicitudDesembolsoResultadoInfo;
import py.com.sepsa.genisys.ejb.utils.Text;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class FacturaVentaLote_Sepsa extends FacturaVentaLote_Base {

    @EJB
    private PF_FacturaVentaLote_Sepsa ventaLote;

    @Getter
    @Setter
    private FacturaVentaLote_Sepsa.FormInput currentInput;

    @PostConstruct
    public void init() {
        // Inicializar contenedor de datos.
        this.currentInput = new FacturaVentaLote_Sepsa.FormInput();
    }

    public void calcularDesembolsos(List<FacturaInfo> facturas, Persona entidadFinanciera) throws Throwable {
        try {
            Collection<CalculoDesembolsoFacturaInfo> calculos = ventaLote.calcularDesembolsos(usuarioLogueado.getUsuario(), usuarioLogueado.getPerfil(), facturas, entidadFinanciera, Fechas.now());
            currentInput.calculos.clear();
            currentInput.calculos.addAll(calculos);
        } catch (Throwable thr) {
            clearCalculos(currentInput.calculos, facturas);
            throw thr;
        }
    }

    public String validar(Persona entidad) {
        // TODO: IMPLEMENTAR.
        return null;
    }

    public String htmlInteresTotal(CalculoDesembolsoFacturaInfo info) {
        BigDecimal v = info.getPorcentajeDescuento();
        return Strings.format("%s %%", Currency.formatCurrency(v));
    }

    public String htmlDetalleEntidad(CalculoDesembolsoFacturaInfo info) {
        py.com.sepsa.genisys.ejb.logic.impl.Calculador_Sepsa.Extras extras = (py.com.sepsa.genisys.ejb.logic.impl.Calculador_Sepsa.Extras) info.getExtras();
        if (extras != null) {
            if (!info.getOk()) {
                StringBuilder2 builder = new StringBuilder2();
                builder.append("<div class=\"app_text_wrapped\"><strong>Estado:</strong>&nbsp;Error</div>");
                builder.append("<div class=\"app_text_wrapped\"><strong>Mensaje:</strong>&nbsp;%s</div>", info.getMessage());
                return builder.toString();
            } else {
                return htmlDetalleEntidad0(extras);
            }
        } else {
            return Text.nd();
        }
    }

    public static String htmlDetalleEntidad0(py.com.sepsa.genisys.ejb.logic.impl.Calculador_Sepsa.Extras detalle) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append("<div class=\"app_text_wrapped\"><strong>Estado:</strong>&nbsp;Ok</div>");
        builder.append("<div class=\"app_text_wrapped\"><strong>Interés (IVA Incluido):</strong>&nbsp;Gs. %s</div>", Currency.formatCurrency(detalle.getMontoTotalComision()));
        builder.append("<div class=\"app_text_wrapped\"><strong>Gastos Administrativos (IVA Incluido):</strong>&nbsp;Gs. %s</div>", Currency.formatCurrency(detalle.getMontoTotalGastosOperativos()));
        return builder.toString();
    }

    public SolicitudDesembolsoResultadoInfo vender(Integer idLote, Persona entidadFinanciera) {
        return ventaLote.vender(usuarioLogueado.getUsuario(), usuarioLogueado.getPerfil(), idLote, currentInput.calculos, entidadFinanciera);
    }

    @Data
    public static class FormInput implements Serializable {

        private List<CalculoDesembolsoFacturaInfo> calculos;
        private String comentarios;

        public FormInput() {
            this.comentarios = "";
            this.calculos = fa.gs.utils.collections.Lists.empty();
        }

    }

}
