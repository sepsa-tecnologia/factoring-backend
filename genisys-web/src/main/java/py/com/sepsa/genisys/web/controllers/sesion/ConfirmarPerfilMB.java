/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.sesion;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import py.com.sepsa.genisys.ejb.entities.factoring.Perfil;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.web.controllers.varios.MessagesMB;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("confirmarPerfilMB")
@ViewScoped
public class ConfirmarPerfilMB implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    @Inject
    private UsuarioLogueadoMB usuarioLogueado;

    @Inject
    private MessagesMB messages;

    private Perfil perfilSeleccionado;

    //</editor-fold>
    @PostConstruct
    public void init() {
        ;
    }

    public void confirmar() {
        if (perfilSeleccionado == null) {
            messages.showGlobalDialogMessage("Debe seleccionar un perfil para poder continuar");
        } else {
            // Modificar contexto de autentication del contenedor.
            SecurityContext ctx = SecurityContextHolder.getContext();
            Authentication auth = ctx.getAuthentication();
            Object username = auth.getPrincipal();
            Object password = auth.getCredentials();
            List<GrantedAuthority> authorities = new LinkedList<>();
            authorities.add(new SimpleGrantedAuthority(perfilSeleccionado.getDescripcion()));

            Authentication auth2 = new UsernamePasswordAuthenticationToken(username, password, authorities);
            ctx.setAuthentication(auth2);
            SecurityContextHolder.setContext(ctx);

            // Modificar estado interno de autenticacion.
            usuarioLogueado.setPerfil(perfilSeleccionado);

            // Redireccionar al inicio.
            try {
                ExternalContext context = Prime.getExternalContext();
                context.redirect(context.getRequestContextPath() + "/app/home.xhtml");
            } catch (Exception e) {
                ;
            }
        }

        // Actualizar vista.
        Prime.update("seleccionar_perfil_form");
        Prime.update("seleccionar_perfil_form:seleccionar_perfil_list");
    }

    public String getPerfilAsText(Perfil perfil) {
        return PerfilEnum.descripcionAsSimpleText(perfil.getDescripcion());
    }

    //<editor-fold defaultstate="collapsed" desc="Getter y Setters">
    public Perfil getPerfilSeleccionado() {
        return perfilSeleccionado;
    }

    public void setPerfilSeleccionado(Perfil perfilSeleccionado) {
        this.perfilSeleccionado = perfilSeleccionado;
    }
    //</editor-fold>

}
