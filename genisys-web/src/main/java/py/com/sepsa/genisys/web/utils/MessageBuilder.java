/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.utils;

import javax.faces.application.FacesMessage;

/**
 *
 * @author Fabio A. González Sosa
 */
public class MessageBuilder {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Tipo de severidad básica para los mensajes.
     */
    private FacesMessage.Severity severity = FacesMessage.SEVERITY_INFO;

    /**
     * Resúmen del mensaje.
     */
    private String summary = "";

    /**
     * Detalles del mensaje.
     */
    private String detail = "";
    // <editor-fold>

    /**
     * Establece la severidad del mensaje a ser construido.
     *
     * @param severity Severidad del mensaje.
     * @return Constructor del mensaje.
     */
    public MessageBuilder severity(FacesMessage.Severity severity) {
        this.severity = severity;
        return this;
    }

    /**
     * Establece el resumen del mensaje a ser construido.
     *
     * @param summary Resúmen del mensaje.
     * @return Constructor del mensaje.
     */
    public MessageBuilder summary(String summary) {
        this.summary = summary;
        return this;
    }

    /**
     * Establece el detalle del mensaje a ser construido.
     *
     * @param detail Detalle del mensaje.
     * @return Constructor del mensaje.
     */
    public MessageBuilder detail(String detail) {
        this.detail = detail;
        return this;
    }

    /**
     * Construye el mensaje final.
     *
     * @return Mensaje a ser desplegado en la vista.
     */
    public FacesMessage build() {
        FacesMessage msg = new FacesMessage(severity, summary, detail);
        return msg;
    }

}
