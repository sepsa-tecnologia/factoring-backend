/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.factura;

import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.misc.Units;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.numeric.Currency;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Ajax;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.Lote;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Estados.EstadoFacturaEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculoDesembolsoFacturaInfo;
import py.com.sepsa.genisys.ejb.logic.pojos.SolicitudDesembolsoResultadoInfo;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.varios.PersonasMB;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "facturaVentaLote")
@ViewScoped
public class FacturaVentaLoteMB extends BasicViewMB implements Serializable {

    @EJB
    private Facturas facturas;

    @Inject
    private PersonasMB personas;

    @Inject
    @Getter
    @Setter
    private FacturaVentaLote_BancoRio ventaLoteFacturasBancoRio;

    @Inject
    @Getter
    @Setter
    private FacturaVentaLote_Sepsa ventaLoteFacturasSepsa;

    @Inject
    private TxWrapperWeb tx;

    /**
     * Lote con detalles adicionales.
     */
    private Lote lote;

    /**
     * Facturas en lote.
     */
    private List<FacturaInfo> facturas0;

    /**
     * Entidad financiera seleccionada de la lista de entidades conveniadas para
     * la venta de una factura.
     */
    @Getter
    @Setter
    private Persona entidadSelected;

    @Getter
    @Setter
    private String comentarioAdicional;

    @Getter
    @Setter
    private Boolean puedeConfirmar;

    @PostConstruct
    public void init() {
        // Obtener lote.
        Integer idLote = utils.unmaskInteger(Jsf.getRequestParameter("lote"));
        this.lote = facturas.buscarLote(idLote).value();

        // Inicializar facturas en lote.
        this.facturas0 = facturas.buscarFacturasEnLote(lote.getId()).value(Lists.empty());

        // Inicializar entidad financiera.
        this.entidadSelected = null;

        // Inicializar otros elementos.
        this.puedeConfirmar = false;
    }

    public Collection<Persona> getEntidadesSeleccionables() {
        return personas.getEntidadesFinancieras();
    }

    public boolean entidadEstaSeleccionada() {
        return entidadSelected != null;
    }

    public boolean entidadSeleccionadaEsBancoRio() {
        if (entidadEstaSeleccionada()) {
            return ventaLoteFacturasBancoRio.esBancoRio(entidadSelected.getId());
        }
        return false;
    }

    public boolean entidadSeleccionadaEsSepsa() {
        boolean a = entidadEstaSeleccionada();
        boolean b = !entidadSeleccionadaEsBancoRio();
        return a && b;
    }

    public void onEntidadSelectedChanged(AjaxBehaviorEvent event) {
        try {
            // Control.
            if (!entidadEstaSeleccionada()) {
                return;
            }

            // Reiniciar datos de desembolso.
            reset();
        } catch (Throwable thr) {
            // Error.
            entidadSelected = null;
            log.warning(thr, "Ocurrió un error seleccionando entidad financiera para solicitud de desembolso de factura.");
        } finally {
            // Actualizar vista.
            updateViewVentaFactura();
        }
    }

    private void reset() {
        // No se puede confirmar la operacion.
        puedeConfirmar = false;

        // Inicializacion: Banco Rio.
        if (entidadSeleccionadaEsBancoRio()) {
            try {
                ventaLoteFacturasBancoRio.init();
                ventaLoteFacturasBancoRio.obtenerDatosProveedor(entidadSelected);
                ventaLoteFacturasBancoRio.calcularDesembolsos(facturas0, entidadSelected);
                puedeConfirmar = true;
            } catch (Throwable thr) {
                log.error(thr, "Ocurrió un error inicializando desembolso para banco rio");
                mensajes.showGlobalDialogMessage(Text.error(thr));
            }
            return;
        }

        // Inicializacion: Sepsa.
        if (true) {
            try {
                ventaLoteFacturasSepsa.init();
                ventaLoteFacturasSepsa.calcularDesembolsos(facturas0, entidadSelected);
                puedeConfirmar = true;
            } catch (Throwable thr) {
                log.error(thr, "Ocurrió un error inicializando desembolso para sepsa");
                mensajes.showGlobalDialogMessage(Text.error(thr));
            }
            return;
        }

        throw Errors.illegalState();
    }

    private String validar() {
        // Verificar si existe una entidad financiera seleccionada.
        if (!entidadEstaSeleccionada()) {
            return "Debe seleccionar una entidad financiera.";
        }

        // Validar: Banco Rio.
        if (entidadSeleccionadaEsBancoRio()) {
            return ventaLoteFacturasBancoRio.validar(entidadSelected);
        }

        // Validar: Sepsa.
        if (true) {
            return ventaLoteFacturasSepsa.validar(entidadSelected);
        }

        return null;
    }

    //<editor-fold defaultstate="collapsed" desc="Confirmacion de Venta">
    /**
     * Cambia el estado de una o factura seleccionada a
     * {@link EstadoFacturaEnum#VENDIDA} (desembolso solicitado) y realiza
     * operaciones complementarias.
     */
    public void venderLote() {
        Result<Void> result = tx.execute(this::venderLote0);
        if (result.isFailure()) {
            mensajes.showGlobalDialogMessage(result.failure().message());
        } else {
            mensajes.showGlobalDialogMessage(this::redireccionarListaFacturas, "¡Facturas procesadas exitosamente!");
            updateViewVentaFactura();
        }
    }

    private Void venderLote0() throws Throwable {
        String traza = Ids.randomUuid();
        log.inicio(traza);
        try {
            // Validar datos de entrada.
            String err = validar();
            if (err != null) {
                throw Errors.illegalState(err);
            }

            // Vender lote de facturas.
            SolicitudDesembolsoResultadoInfo resVenta = vender();
            if (resVenta.isOk() == false) {
                throw Errors.illegalState(resVenta.getErr());
            }

            // Ok.
            return null;
        } catch (Throwable thr) {
            // Error inesperado.
            String msg = fa.gs.utils.misc.text.Text.select(thr.getMessage(), Units.execute("", () -> thr.getCause().getMessage()));
            throw Errors.illegalState(thr, msg);
        } finally {
            log.fin(traza);
        }
    }

    private SolicitudDesembolsoResultadoInfo vender() {
        // Venta de facturas: Banco Rio.
        if (entidadSeleccionadaEsBancoRio()) {
            return ventaLoteFacturasBancoRio.vender(lote.getId(), entidadSelected);
        }

        // Venta de facturas: Sepsa.
        if (true) {
            return ventaLoteFacturasSepsa.vender(lote.getId(), entidadSelected);
        }

        // Ko.
        SolicitudDesembolsoResultadoInfo result = new SolicitudDesembolsoResultadoInfo();
        result.setOk(false);
        result.setErr("ERROR");
        return result;
    }
    //</editor-fold>

    public String montoConformado(CalculoDesembolsoFacturaInfo info) {
        if (info == null || info.getOk() == false) {
            return Text.nd();
        } else {
            BigDecimal v = info.getFactura().getMontoTotalConformado();
            return Strings.format("Gs. %s", Currency.formatCurrency(v));
        }
    }

    public String diasAdelanto(CalculoDesembolsoFacturaInfo info) {
        if (info == null || info.getOk() == false) {
            return Text.nd();
        } else {
            int v = info.getDiasAnticipo();
            return String.valueOf(v);
        }
    }

    public String interesTotal(CalculoDesembolsoFacturaInfo info) {
        if (info == null) {
            return Text.nd();
        }

        if (ventaLoteFacturasBancoRio.esBancoRio(entidadSelected.getId())) {
            return ventaLoteFacturasBancoRio.htmlInteresTotal(info);
        }

        if (true) {
            return ventaLoteFacturasSepsa.htmlInteresTotal(info);
        }

        throw Errors.illegalState();
    }

    public String montoDescuento(CalculoDesembolsoFacturaInfo info) {
        if (info == null || info.getOk() == false) {
            return Text.nd();
        } else {
            BigDecimal v = info.getMontoDescuento();
            return Strings.format("Gs. %s", Currency.formatCurrency(v));
        }
    }

    public String montoDesembolso(CalculoDesembolsoFacturaInfo info) {
        if (info == null || info.getOk() == false) {
            return Text.nd();
        } else {
            BigDecimal v = info.getMontoDesembolso();
            return Strings.format("Gs. %s", Currency.formatCurrency(v));
        }
    }

    public String detalleEntidad(CalculoDesembolsoFacturaInfo info) {
        if (info == null) {
            return Text.nd();
        }

        if (ventaLoteFacturasBancoRio.esBancoRio(entidadSelected.getId())) {
            return ventaLoteFacturasBancoRio.htmlDetalleEntidad(info);
        }

        if (true) {
            return ventaLoteFacturasSepsa.htmlDetalleEntidad(info);
        }

        throw Errors.illegalState();
    }

    private void updateViewVentaFactura() {
        Ajax.update("venta_factura_form");
        Ajax.update("venta_factura_form_messages");
    }

    private void redireccionarListaFacturas() {
        Prime.redirect("/app/abm/factura/lista.xhtml");
    }

}
