/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.usuarios;

import fa.gs.misc.Assertions;
import fa.gs.utils.misc.text.Joiner;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Ajax;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Usuarios;
import py.com.sepsa.genisys.ejb.logic.pojos.UsuarioInfo;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
@Named(value = "usuarios")
@ViewScoped
public class UsuariosMB extends BasicViewMB implements Serializable {

    @Inject
    private Usuarios usuarios;

    @Inject
    @Getter
    @Setter
    private py.com.sepsa.genisys.web.controllers.usuarios.ListaMB lista;

    @PostConstruct
    public void init() {
        ;
    }

    public void initAltaUsuario() {
        Prime.redirect("app/abm/usuarios/alta.xhtml?redirect-true");
    }

    public void eliminarUsuario() {
        UsuarioInfo item = checkSeleccionUnica(lista);
        if (item != null) {
            try {
                usuarios.inhabilitar(item.getId());
                mensajes.showGlobalDialogMessage("Usuario eliminado exitosamente");
                updateViewListaDeUsuarios();
            } catch (Throwable thr) {
                mensajes.showGlobalDialogMessage("Ocurrió un error eliminando usuario");
            }
        }
    }

    public void initHabilitarPerfil() {
        UsuarioInfo item = checkSeleccionUnica(lista);
        if (item != null) {
            String id = Ids.mask(item.getId());
            String path = Strings.format("app/abm/usuarios/habilitar_perfil.xhtml?u=%s", id);
            Prime.redirect(path);
        }
    }

    public void initCambiarContrasena() {
        UsuarioInfo item = checkSeleccionUnica(lista);
        if (item != null) {
            String id = Ids.mask(item.getId());
            String path = Strings.format("app/abm/usuarios/cambiar_pass.xhtml?u=%s", id);
            Prime.redirect(path);
        }
    }

    public String razonSocial(UsuarioInfo item) {
        if (Assertions.stringNullOrEmpty(item.getRazonSocial())) {
            return Text.nd();
        } else {
            return item.getRazonSocial().trim();
        }
    }

    public String nombreFantasia(UsuarioInfo item) {
        if (Assertions.stringNullOrEmpty(item.getNombreFantasia())) {
            return Text.nd();
        } else {
            return item.getNombreFantasia().trim();
        }
    }

    public String ruc(UsuarioInfo item) {
        String ruc = Joiner.of(item.getRuc(), item.getDvRuc()).separator("-").join().trim();
        if (ruc.endsWith("-")) {
            ruc = ruc.substring(0, ruc.length() - 1);
        }
        return ruc;
    }

    public String perfiles(UsuarioInfo item) {
        String[] parts = item.getIdPerfilAgg().split(";");
        if (parts.length == 0) {
            return Text.nd();
        } else if (parts.length == 1) {
            return PerfilEnum.descripcionAsSimpleText(parts[0]);
        } else {
            return Joiner.of(parts)
                    .adapter(e -> Strings.format("<div>&bull;&nbsp;%s</div>", PerfilEnum.descripcionAsSimpleText(e)))
                    .separator(" ")
                    .join();
        }
    }

    public void filtrarLista() {
        lista.filter();
        updateViewFiltrosForm();
    }

    public void limpiarFiltroForm() {
        CustomFilter filtro = lista.getFiltro();
        if (!filtro.isEnabled()) {
            filtro.clearFilterValues();
            updateViewFiltrosForm();
        }
    }

    public void recargarListaUsuarios() {
        lista.clearSelections();
        updateViewListaDeUsuarios();
        lista.reload();
        updateViewListaDeUsuarios();
    }

    private void updateViewFiltrosForm() {
        Ajax.update("filtro_usuarios_panel:filtro_usuarios_form");
        Prime.showBlockUi("bui", lista.getFiltro().isEnabled());
        updateViewListaDeUsuarios();
    }

    private void updateViewListaDeUsuarios() {
        lista.clearSelections();
        Ajax.update("tabla_usuarios_panel:tabla_usuarios_form:tabla_usuarios");
        Prime.executeJs("$.fn.matchHeight._update();");
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public String getBtnFiltrarText() {
        if (lista.getFiltro().isEnabled()) {
            return "Deshacer Filtro";
        } else {
            return "Filtrar Datos";
        }
    }

    public String getOpcionesPanelClass() {
        if (!usuarioLogueado.esAdministrador()) {
            return "hidden";
        } else {
            return "col-md-2";
        }
    }

    public String getListaUsuariosPanelClass() {
        if (!usuarioLogueado.esAdministrador()) {
            return "col-md-12";
        } else {
            return "col-md-10";
        }
    }
    //</editor-fold>

}
