/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.proveedor;

import fa.gs.result.simple.Result;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ValidationException;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.PersonaRol;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "altaProveedor")
@ViewScoped
public class ProveedorAltaMB extends BasicViewMB implements Serializable {

    @Inject
    private TxWrapperWeb tx;

    @Getter
    @Setter
    private ProveedorAltaMB.Input input;

    @EJB
    private Personas personas;

    @PostConstruct
    public void init() {
        // Inicializar campos de entrada.
        initInput();
    }

    private void initInput() {
        this.input = new ProveedorAltaMB.Input();
    }

    public void guardar() {
        Result<Void> result = tx.execute(this::guardar0);
        if (result.isFailure()) {
            // Error.
            String msg = result.failure().cause().getCause().getMessage();
            mensajes.showGlobalDialogMessage(msg);
        } else {
            // Limpiar campos de entrada.
            initInput();
            mensajes.showGlobalDialogMessage("Datos guardados exitosamente");
        }

        updateViewAltaFacturaForm();
    }

    private Void guardar0() throws Throwable {
        // Validar datos de entrada.
        String err = validarInput(input);
        if (err != null) {
            throw new ValidationException(err);
        }

        // Obtener persona.
        Result<Persona> resPersona = personas.obtenerPorRuc(input.ruc);
        Persona persona = resPersona.value(null);
        if (persona == null) {
            // Registrar nueva persona.
            if (input.esPersonaFisica) {
                resPersona = personas.registrarPersonaFisica(input.nombres, input.apellidos, input.ci, input.ruc);
                persona = resPersona.value(null);
            } else if (input.esPersonaJuridica) {
                resPersona = personas.registrarPersonaJuridica(input.razonSocial, input.razonSocial, input.ruc);
                persona = resPersona.value(null);
            } else {
                throw Errors.illegalState();
            }
        }

        // Agregar rol de proveedor.
        Result<PersonaRol> resRol = personas.agregarRol(persona.getId(), PerfilEnum.PROVEEDOR);
        resRol.raise();

        // Control.
        if (persona == null) {
            throw new ValidationException("No se pudo registrar al proveedor.");
        }

        // Registrar relacion comercial.
        Result<Void> resRelacion = personas.agregarRelacion(persona.getId(), usuarioLogueado.getPersona().getId());
        resRelacion.raise();

        return null;
    }

    private String validarInput(ProveedorAltaMB.Input input) {
        // Validar persona juridica.
        if (input.esPersonaJuridica) {
            // Validar razon social.
            if (Assertions.stringNullOrEmpty(input.razonSocial)) {
                return "El campo razón social no puede ser vacío.";
            }
        }

        // Validar persona fisica.
        if (input.esPersonaFisica) {
            // Validar nombres.
            if (Assertions.stringNullOrEmpty(input.nombres)) {
                return "El campo nombres no puede ser vacío.";
            }

            // Validar apellidos.
            if (Assertions.stringNullOrEmpty(input.apellidos)) {
                return "El campo apellidos no puede ser vacío.";
            }

            // Validar ci.
            if (Assertions.stringNullOrEmpty(input.ci)) {
                return "El campo documento de identidad no puede ser vacío.";
            }
        }

        // Validar ruc.
        if (Assertions.stringNullOrEmpty(input.ruc)) {
            return "El campo RUC no puede ser vacío.";
        }
        if (!Assertions.isRuc(input.ruc)) {
            return "El campo RUC es inválido.";
        }

        return null;
    }

    public void onPersonaFisicaChanged() {
        this.input.esPersonaJuridica = !this.input.esPersonaFisica;
    }

    public void onPersonaJuridicaChanged() {
        this.input.esPersonaFisica = !this.input.esPersonaJuridica;
    }

    private void updateViewAltaFacturaForm() {
        Prime.update("alta_proveedor_form");
    }

    @Data
    public static class Input implements Serializable {

        private boolean esPersonaFisica;
        private String nombres;
        private String apellidos;
        private String ci;
        private boolean esPersonaJuridica;
        private String razonSocial;
        private String nombreFantasia;
        private String ruc;

        public Input() {
            this.esPersonaFisica = false;
            this.nombres = "";
            this.apellidos = "";
            this.ci = "";
            this.esPersonaJuridica = true;
            this.razonSocial = "";
            this.ruc = "";
        }
    }

}
