/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers;

import fa.gs.utils.logging.app.AppLogger;
import java.io.Serializable;
import javax.inject.Inject;
import py.com.sepsa.genisys.ejb.logic.LogicBeans;
import py.com.sepsa.genisys.ejb.logic.impl.Facades;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.web.controllers.varios.EstadosMB;
import py.com.sepsa.genisys.web.controllers.varios.MessagesMB;
import py.com.sepsa.genisys.web.controllers.varios.UtilsMB;

/**
 *
 * @author Fabio A. González Sosa
 */
public class BasicMB implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Fachadas remotas.
     */
    @Inject
    protected Facades facades;

    /**
     * Estados generales del sistema.
     */
    @Inject
    protected EstadosMB estados;

    /**
     * Administracion de mensajes desplegados en pantalla.
     */
    @Inject
    protected MessagesMB mensajes;

    /**
     * Beans de logica.
     */
    @Inject
    protected LogicBeans beans;

    /**
     * Instancia de logger.
     */
    protected AppLogger log;

    /**
     * Bean global con metodos genericos para todo el sistema.
     */
    @Inject
    protected UtilsMB utils;
    // </editor-fold>

    /**
     * Constructor.
     */
    public BasicMB() {
        this.log = AppLoggerFactory.web();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Facades getFacades() {
        return facades;
    }

    public void setFacades(Facades facades) {
        this.facades = facades;
    }

    public EstadosMB getEstados() {
        return estados;
    }

    public void setEstados(EstadosMB estados) {
        this.estados = estados;
    }

    public MessagesMB getMensajes() {
        return mensajes;
    }

    public void setMensajes(MessagesMB mensajes) {
        this.mensajes = mensajes;
    }

    public LogicBeans getBeans() {
        return beans;
    }

    public void setBeans(LogicBeans beans) {
        this.beans = beans;
    }

    public AppLogger getLog() {
        return log;
    }

    public void setLog(AppLogger log) {
        this.log = log;
    }

    public UtilsMB getUtils() {
        return utils;
    }

    public void setUtils(UtilsMB utils) {
        this.utils = utils;
    }
    //</editor-fold>
}
