/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.archivo;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.entities.factoring.Archivo;
import py.com.sepsa.genisys.ejb.facades.factoring.ArchivoFacade;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("descargarArchivo")
@ViewScoped
public class DescargarArchivo implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    @EJB
    private ArchivoFacade archivoFacade;
    //</editor-fold>

    /**
     * Permite enviar el contenido de un archivo almacenado en el sistema de
     * archivos utilizando la informacion registrada en base de datos. La
     * implementacion del metodo esta basado en:
     * <ul>
     * <li><a href="https://stackoverflow.com/a/9394237">[1]</a></li>
     * </ul>
     *
     * @param archivoId Indentificador de archivo.
     * @throws Exception Error producido, si hubiere.
     */
    public void get(Integer archivoId) throws Exception {
        // Obtener informacion de archivo.
        Archivo archivo = archivoFacade.find(archivoId);
        String fileName = archivo.getNombre();
        String filePath = archivo.getPath();
        String fileType = resolveFileType(filePath);
        File file = new File(filePath);
        Integer fileSize = (int) file.length();

        // Leer contenido de archivo y enviar como respuesta.
        FacesContext ctx = FacesContext.getCurrentInstance();
        try (InputStream input = new FileInputStream(file)) {
            ExternalContext ec = ctx.getExternalContext();
            ec.responseReset();
            ec.setResponseContentType(fileType);
            ec.setResponseContentLength(fileSize);
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            try (OutputStream output = ec.getResponseOutputStream()) {
                byte[] chunk = new byte[4096];
                int read;
                while ((read = input.read(chunk)) >= 0) {
                    output.write(chunk, 0, read);
                    output.flush();
                }
            }
        }
        ctx.responseComplete();
    }

    /**
     * Intenta obtener el mime-type de un archivo.
     *
     * @param path Ruta hasta el archivo.
     * @return Mime-type.
     */
    private String resolveFileType(String path) {
        try {
            String type = Files.probeContentType(java.nio.file.Paths.get(path));
            if (type == null || type.isEmpty()) {
                throw new Exception();
            }
            return type;
        } catch (Throwable thr) {
            return "application/octet-stream";
        }
    }

}
