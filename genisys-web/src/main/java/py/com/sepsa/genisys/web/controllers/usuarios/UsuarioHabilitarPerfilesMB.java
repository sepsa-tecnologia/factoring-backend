/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.usuarios;

import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Sets;
import fa.gs.utils.jsf.Jsf;
import java.io.Serializable;
import java.util.Collection;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ValidationException;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.Usuarios;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "usuarioHabilitarPerfilesMB")
@ViewScoped
public class UsuarioHabilitarPerfilesMB extends BasicViewMB implements Serializable {

    @Inject
    private Usuarios usuarios;

    @Inject
    private Perfiles perfiles;

    @Inject
    private TxWrapperWeb tx;

    private Usuario usuario;

    private Collection<PerfilEnum> perfiles0;

    @Getter
    @Setter
    private FormInput input;

    @PostConstruct
    public void init() {
        // Obtener datos de usuario.
        Integer idUsuario = Ids.unmask(Jsf.getRequestParameter("u"));
        usuario = usuarios.obtenerPorId(idUsuario).value();
        perfiles0 = usuarios.obtenerPerfilesPorTransUsuario(usuario).stream().map(p -> perfiles.obtenerPerfil(p)).collect(Collectors.toSet());

        // Inicializar formulario.
        input = new FormInput();
        input.esComprador = perfiles0.contains(PerfilEnum.COMPRADOR);
        input.esProveedor = perfiles0.contains(PerfilEnum.PROVEEDOR);
        input.esEntidadFinanciera = perfiles0.contains(PerfilEnum.ENTIDAD_FINANCIERA);
    }

    public void guardar() {
        Result<Void> result = tx.execute(this::guardar0);
        if (result.isFailure()) {
            // Error.
            String msg = result.failure().cause().getMessage();
            mensajes.showGlobalDialogMessage(msg);
        } else {
            // Limpiar campos de entrada.
            mensajes.showGlobalDialogMessage(() -> Prime.redirect("/app/abm/usuarios/lista.xhtml"), "Datos guardados exitosamente");
        }

        updateForm();
    }

    private Void guardar0() throws Throwable {
        // Validar datos de entrada.
        String err = validarInput(input);
        if (err != null) {
            throw new ValidationException(err);
        }

        // Agrupar perfiles.
        Collection<PerfilEnum> perfiles = Sets.empty();
        if (input.esComprador) {
            perfiles.add(PerfilEnum.COMPRADOR);
        }
        if (input.esProveedor) {
            perfiles.add(PerfilEnum.PROVEEDOR);
        }
        if (input.esEntidadFinanciera) {
            perfiles.add(PerfilEnum.ENTIDAD_FINANCIERA);
        }

        // Realizar cambio
        usuarios.cambiarPerfiles(usuario.getId(), perfiles);

        return null;
    }

    private String validarInput(FormInput input) {
        return null;
    }

    private void updateForm() {
        Prime.update("habilitar_perfil_form");
    }

    @Data
    public static class FormInput implements Serializable {

        private boolean esComprador;
        private boolean esProveedor;
        private boolean esEntidadFinanciera;
    }

}
