/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.sesion;

import fa.gs.utils.authentication.tokens.TokenExtractor;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.misc.errors.Errors;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import py.com.sepsa.genisys.web.utils.BackingBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@RequestScoped
@Named("bSso")
public class SsoMB extends BackingBean {

    public void doRedirect() {
        AuthenticationInfo info = getCurrentUserInfo();
        if (info != null) {
            // Obtener token.
            String token = Jsf.getRequestParameter("session_token");

            // Generar cookie con token de autenticacion y redireccionar a home.
            TokenExtractor.COOKIE_NAME = "APP_SESSION_TOKEN_GENISYS";
            Jsf.addCookie(TokenExtractor.COOKIE_NAME, token, "/", -1);
            Jsf.redirect("/app/home.xhtml");
        }
    }

    public AuthenticationInfo getCurrentUserInfo() {
        // Contexto de sesion de spring.
        SecurityContext ctx = SecurityContextHolder.getContext();
        if (ctx == null) {
            throw Errors.illegalState("Se debe iniciar sesion");
        }

        AuthenticationInfo auth = (AuthenticationInfo) ctx.getAuthentication().getDetails();
        return auth;
    }

}
