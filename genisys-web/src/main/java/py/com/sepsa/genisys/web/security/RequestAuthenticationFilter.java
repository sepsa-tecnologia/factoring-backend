/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.security;

import fa.gs.utils.authentication.tokens.TokenExtractor;
import fa.gs.utils.authentication.user.AuthenticationInfo;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.result.simple.Result;
import java.io.IOException;
import java.security.Principal;
import java.util.Collection;
import java.util.stream.Collectors;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import py.com.sepsa.genisys.ejb.security.UserTokenAuthenticator;

/**
 *
 * @author Fabio A. González Sosa
 */
public class RequestAuthenticationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ;
    }

    @Override
    public void destroy() {
        ;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // Contexto de seguridad de Spring.
        SecurityContext securityContext = getSpringSecurityContext(false);
        Authentication securityAuthentication = (securityContext != null) ? securityContext.getAuthentication() : null;

        // Extraer token de autenticacion, si hubiere.
        TokenExtractor.COOKIE_NAME = "APP_SESSION_TOKEN_GENISYS";
        String token = TokenExtractor.fromHttpRequest((HttpServletRequest) request);

        // Verificar que token sea valido (parseable), si hubiere.
        if (!Assertions.stringNullOrEmpty(token) && !UserTokenAuthenticator.tokenIsValid(token)) {
            sendInvalidTokenResponse(request, response, chain);
            return;
        }

        /**
         * Si existe una sesion activa y no se especifica un token de
         * autenticacion valido correspondiente, no continuar.
         */
        if (securityAuthentication != null && Assertions.stringNullOrEmpty(token)) {
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        /**
         * Si no existe una sesion activa y se especifica un token de
         * autenticacion valido, generar las credenciales correspondientes e
         * iniciar una nueva sesion.
         */
        if (securityAuthentication == null && !Assertions.stringNullOrEmpty(token)) {
            // Autenticar token.
            UserTokenAuthenticator authenticator = new UserTokenAuthenticator();
            Result<AuthenticationInfo> resAuth = authenticator.authenticateUserToken(token);
            if (resAuth.isFailure() || resAuth.value() == null) {
                sendInvalidTokenResponse(request, response, chain);
                return;
            }

            // Contexto de autenticacion.
            AuthenticationInfo authenticationContext = resAuth.value();

            // Generar "roles" procesables por Spring.
            Collection<GrantedAuthority> authorities = authenticationContext.getPermisos()
                    .stream()
                    .map(pi -> new SimpleGrantedAuthority(pi.name()))
                    .collect(Collectors.toList());

            // Contexto de autenticacion de Spring.
            Principal userPrincipal = authenticationContext.getUserPrincipal();
            UsernamePasswordAuthenticationToken newSecurityAuthentication = new UsernamePasswordAuthenticationToken(userPrincipal, userPrincipal, authorities);
            newSecurityAuthentication.setDetails(authenticationContext);

            // Registrar sesion autenticada.
            securityContext = getSpringSecurityContext(true);
            securityContext.setAuthentication(newSecurityAuthentication);
        }

        // Continuar camino normal.
        chain.doFilter(request, response);
    }

    private SecurityContext getSpringSecurityContext(boolean createIfEmpty) {
        SecurityContext ctx = SecurityContextHolder.getContext();
        if (ctx == null && createIfEmpty) {
            ctx = SecurityContextHolder.createEmptyContext();
        }
        return ctx;
    }

    private void sendInvalidTokenResponse(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException {
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "El token de autenticación no es válido o ha sido vulnerado. Por favor, limpie la caché y cookies de su navegador y vuelva a intentar.");
    }

}
