/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.factura;

import fa.gs.utils.collections.Lists;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "facturasStats")
@ViewScoped
public class FacturaStatsMB extends BasicViewMB implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Cantidad total de facturas desembolsadas a cobrar en los siguientes dias.
     */
    private Integer totalPagadasAVencer;

    /**
     * Facturas desembolsadas a cobrar en los siguientes dias.
     */
    private List<Factura> pagadasAVencer;

    /**
     * Cantidad total de facturas desembolsadas, ya vencidas que no fueron
     * cobradas en día.
     */
    private Integer totalPagadasVencidas;

    /**
     * Facturas desembolsadas, ya vencidas que no fueron cobradas en día.
     */
    private List<Factura> pagadasVencidas;

    /**
     * Cantidad total de facturas desembolsadas, que vencen hoy y deben ser
     * cobradas.
     */
    private Integer totalPagadasAVencerHoy;

    /**
     * Facturas desembolsadas, que vencen hoy y deben ser cobradas.
     */
    private List<Factura> pagadasAVencerHoy;

    /**
     * Cantidad total de facturas que estan listas para desembolso.
     */
    private Integer totalADesembolsar;
    //</editor-fold>

    /**
     * Inicializa el managed bean.
     */
    @PostConstruct
    public void init() {
        if (usuarioLogueado.esEntidadFinanciera()) {
            totalPagadasAVencer = facades.getFacturaFacade().countPagadasAVencer(usuarioLogueado.getPersona().getId());
            totalPagadasAVencerHoy = facades.getFacturaFacade().countPagadasAVencerHoy(usuarioLogueado.getPersona().getId());
            totalPagadasVencidas = facades.getFacturaFacade().countPagadasVencidas(usuarioLogueado.getPersona().getId());
            totalADesembolsar = facades.getFacturaFacade().countADesembolsar(usuarioLogueado.getPersona().getId());
            pagadasAVencer = facades.getFacturaFacade().findPagadasAVencer(usuarioLogueado.getPersona().getId(), 30);
            pagadasAVencerHoy = facades.getFacturaFacade().findPagadasAVencerHoy(usuarioLogueado.getPersona().getId(), 30);
            pagadasVencidas = facades.getFacturaFacade().findPagadasVencidas(usuarioLogueado.getPersona().getId(), 30);
        } else {
            totalPagadasAVencer = 0;
            totalPagadasAVencerHoy = 0;
            totalPagadasVencidas = 0;
            totalADesembolsar = 0;
            pagadasAVencer = Lists.empty();
            pagadasAVencerHoy = Lists.empty();
            pagadasVencidas = Lists.empty();
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Integer getTotalPagadasAVencer() {
        return totalPagadasAVencer;
    }

    public void setTotalPagadasAVencer(Integer totalPagadasAVencer) {
        this.totalPagadasAVencer = totalPagadasAVencer;
    }

    public List<Factura> getPagadasAVencer() {
        return pagadasAVencer;
    }

    public void setPagadasAVencer(List<Factura> pagadasAVencer) {
        this.pagadasAVencer = pagadasAVencer;
    }

    public Integer getTotalPagadasVencidas() {
        return totalPagadasVencidas;
    }

    public void setTotalPagadasVencidas(Integer totalPagadasVencidas) {
        this.totalPagadasVencidas = totalPagadasVencidas;
    }

    public List<Factura> getPagadasVencidas() {
        return pagadasVencidas;
    }

    public void setPagadasVencidas(List<Factura> pagadasVencidas) {
        this.pagadasVencidas = pagadasVencidas;
    }

    public Integer getTotalPagadasAVencerHoy() {
        return totalPagadasAVencerHoy;
    }

    public void setTotalPagadasAVencerHoy(Integer totalPagadasAVencerHoy) {
        this.totalPagadasAVencerHoy = totalPagadasAVencerHoy;
    }

    public List<Factura> getPagadasAVencerHoy() {
        return pagadasAVencerHoy;
    }

    public void setPagadasAVencerHoy(List<Factura> pagadasAVencerHoy) {
        this.pagadasAVencerHoy = pagadasAVencerHoy;
    }

    public Integer getTotalADesembolsar() {
        return totalADesembolsar;
    }

    public void setTotalADesembolsar(Integer totalADesembolsar) {
        this.totalADesembolsar = totalADesembolsar;
    }
    //</editor-fold>

}
