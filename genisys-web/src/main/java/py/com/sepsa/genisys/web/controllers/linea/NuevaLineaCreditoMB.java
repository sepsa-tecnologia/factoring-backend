/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.linea;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.misc.numeric.Numeric;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.enterprise.context.Dependent;
import py.com.sepsa.genisys.ejb.entities.factoring.LineaCredito;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.archivo.CacheArchivos;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class NuevaLineaCreditoMB extends BasicViewMB implements Serializable {

    /**
     * Persiste los datos ingresados en la vista para el registro de una nueva
     * linea de credito.
     *
     * @param input Datos introducidos en la vista.
     * @return Resultado de la operacion.
     */
    public Result<LineaCredito> registrar(NuevaLineaCreditoMB.Input input) {
        Result<LineaCredito> result;
        try {
            // Registrar linea de credito.
            Integer idUsuario = usuarioLogueado.getUsuario().getId();
            Integer idEntidadFinanciera = usuarioLogueado.getPersona().getId();
            Integer idPersona = input.cliente.getId();
            Integer idMoneda = input.moneda.getId();
            result = beans.getLineasCredito().registrarLineaCredito(idEntidadFinanciera, idPersona, idUsuario, idMoneda, input.monto);
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error registrando nueva línea de crédito para persona id='%s' con entidad id='%s'", input.cliente.getId(), usuarioLogueado.getPersona().getId()).build();
        }
        return result;
    }

    /**
     * Valida si los datos ingresados para el registro de una linea de credito
     * son validos.
     *
     * @param input Datos ingresados.
     * @return Una cadena que indica el incumplimiento de restricciones
     * encontrado, caso contrario {@code null}.
     */
    public String validar(NuevaLineaCreditoMB.Input input) {
        // Verificar que se haya seleccionado un cliente.
        if (input.cliente == null) {
            return "El campo cliente no puede ser vacío";
        }

        // Verificar que se haya seleccionado una moneda.
        if (input.moneda == null) {
            return "El campo moneda no puede ser vacío";
        }

        // Verificar que se haya ingresado un monto.
        if (input.monto == null) {
            return "El campo monto no puede ser vacío";
        }

        // Verificar que el monto no sea cero.
        if (input.monto != null && Numeric.igual(input.monto, Numeric.CERO)) {
            return "El campo monto no puede ser igual a cero";
        }

        // Verificar que el monto sea positivo.
        if (input.monto != null && Numeric.menor(input.monto, Numeric.CERO)) {
            return "El campo monto no puede ser un valor negativo";
        }

        // Verificar si no existe una linea de credito.
        Result<LineaCredito> resLineaCredito = beans.getLineasCredito().buscarLineaCredito(usuarioLogueado.getPersona().getId(), input.cliente.getId());
        if (resLineaCredito.isSuccess()) {
            return "Ya existe una línea de crédito asignada a este cliente";
        }

        return null;
    }

    public static class Input {

        //<editor-fold defaultstate="collapsed" desc="Atributos">
        /**
         * Cliente seleccionado desde la vista.
         */
        private Persona cliente;

        /**
         * Tipo de moneda seleccionad en la vista para la asignacion de una
         * linea de credito.
         */
        private Moneda moneda;

        /**
         * Monto ingresado en la vista que indica el monto maximo a asignar a
         * una linea de credito para algun cliente.
         */
        private BigDecimal monto;

        /**
         * Coleccion de archivos subidos.
         */
        private CacheArchivos cacheArchivos;
        //</editor-fold>

        public Input() {
            cacheArchivos = new CacheArchivos();
            reset();
        }

        /**
         * Similar a
         * {@link py.com.sepsa.genisys.web.controllers.factura.operaciones.VentaFacturaMB.Input#reset reset}.
         */
        public final void reset() {
            cliente = null;
            moneda = null;
            monto = null;
        }

        //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
        public Persona getCliente() {
            return cliente;
        }

        public void setCliente(Persona cliente) {
            this.cliente = cliente;
        }

        public Moneda getMoneda() {
            return moneda;
        }

        public void setMoneda(Moneda moneda) {
            this.moneda = moneda;
        }

        public BigDecimal getMonto() {
            return monto;
        }

        public void setMonto(BigDecimal montoTope) {
            this.monto = montoTope;
        }

        public CacheArchivos getCacheArchivos() {
            return cacheArchivos;
        }

        public void setCacheArchivos(CacheArchivos cacheArchivos) {
            this.cacheArchivos = cacheArchivos;
        }
        //</editor-fold>
    }
}
