/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.convenio;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.util.Ajax;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.factoring.ConvenioInfo;
import py.com.sepsa.genisys.ejb.logic.impl.Cargos.ConfigCargoView;
import py.com.sepsa.genisys.ejb.logic.impl.Convenios;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.varios.PersonasMB;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "convenios")
@ViewScoped
public class ConveniosMB extends BasicViewMB implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Bean que se encarga de las operaciones de obtencion, filtrado y
     * ordenadmiento de convenios.
     */
    @Inject
    private py.com.sepsa.genisys.web.controllers.convenio.ListaMB lista;

    @Inject
    private PersonasMB personas;

    @EJB
    private Convenios convenios;

    @Inject
    private TxWrapperWeb tx;

    /**
     * Lista de cargos configurados que estan relacionados a un convenio y que
     * pueden ser desplegados en la vista.
     */
    private Collection<ConfigCargoView> cargosConfigurados;
    // </editor-fold>

    /**
     * Inicializa el managed bean.
     */
    @PostConstruct
    public void init() {
        ;
    }

    /**
     * Prepara la vista para la creacion de un convenio.
     */
    public void initCrearConvenio() {
        Prime.redirect("/app/abm/convenio/alta.xhtml?faces-redirect=true");
    }

    /**
     * Prepara la vista para la eliminacion de un convenio.
     */
    public void initEliminarConvenio() {
        if (!lista.hasSelection()) {
            mensajes.showGlobalDialogMessage("Debe seleccionar al menos un convenio de la lista");
        } else {
            Prime.showSepsaDialog("eliminar_convenio_confirm_dialog");
        }
    }

    /**
     * Elimina convenios seleccionados desde la vista.
     *
     * @throws Throwable Error producido.
     */
    public void eliminarConvenio() {
        tx.execute(this::eliminarConvenio0);
    }

    private Result<Void> eliminarConvenio0() {
        Result<Void> result;

        try {
            Prime.closeSepsaDialog("eliminar_convenio_confirm_dialog");
            if (!lista.hasSelection()) {
                mensajes.showGlobalDialogMessage("Debe seleccionar al menos un elemento de la lista");
            } else {
                // Obtener identificadores de convenios.
                List<ConvenioInfo> convenios0 = lista.getMultiSelectedItem();
                Integer[] idConvenios = convenios0.stream().map(c -> c.getId()).distinct().toArray(Integer[]::new);

                // Inactivar convenios.
                boolean ok = convenios.inactivarConvenios(idConvenios);
                if (!ok) {
                    throw Errors.illegalState("Ocurrió un error eliminando convenios");
                }

                // Actualizar vista.
                mensajes.showGlobalDialogMessage("Convenios eliminados exitosamente");
                recargarListaConvenios();
            }

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            log.warning(thr, "Ocurrió un error eliminando convenios");
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error eliminando convenios")
                    .build();
        } finally {
            // Actualizar vista.
            updateViewListaDeConvenios();
        }

        return result;
    }

    /**
     * Prepara la vista para la visualizacion de los cargos configurados dentro
     * de un convenio.
     *
     * @param convenio Convenio del cual se desea conocer sus cargos
     * configurados.
     */
    public void initVerCargos(ConvenioInfo convenio) {
        // Obtenes cargos configurados para el convenio.
        cargosConfigurados = beans.getConvenios().obtenerConfigCargos(convenio.getId());

        // Actualizar vista.
        updateViewCargosConvenioDialog();
        Prime.showSepsaDialog("cargos_convenio_dialog");
    }

    /**
     * Indica al cliente que debe redireccionar y cargar la vista principal de
     * lista de facturas.
     */
    public static void redireccionarListaConvenios() {
        Prime.redirect("/app/abm/convenio/lista.xhtml");
    }

    /**
     * Aplica los filtros sobre la lista de convenios.
     */
    public void filtrarLista() {
        lista.filter();
        updateViewFiltrosForm();
    }

    /**
     * Limpia los valores establecidos en los campos de filtro.
     */
    public void limpiarFiltroForm() {
        CustomFilter filtro = lista.getFiltro();
        if (!filtro.isEnabled()) {
            filtro.clearFilterValues();
            updateViewFiltrosForm();
        }
    }

    /**
     * Actualiza la lista de convenios en la vista.
     */
    public void recargarListaConvenios() {
        lista.clearSelections();
        updateViewListaDeConvenios();
        lista.reload();
        updateViewListaDeConvenios();
    }

    private void updateViewFiltrosForm() {
        Ajax.update("filtro_convenios_panel:filtro_convenios_form");
        Prime.showBlockUi("bui", lista.getFiltro().isEnabled());
        updateViewListaDeConvenios();
    }

    private void updateViewListaDeConvenios() {
        lista.clearSelections();
        Ajax.update("tabla_convenios_panel:tabla_convenios_form:tabla_convenios");
        Prime.executeJs("$.fn.matchHeight._update();");
    }

    private void updateViewCargosConvenioDialog() {
        Ajax.update("cargos_convenio_dialog:cargos_convenio_form");
        Ajax.update("cargos_convenio_dialog:cargos_convenio_dialog_content_messages");
    }

    // <editor-fold defaultstate="collapsed" desc="Getters y Setters">    
    public String getBtnFiltrarText() {
        if (lista.getFiltro().isEnabled()) {
            return "Deshacer Filtro";
        } else {
            return "Filtrar Datos";
        }
    }

    public ListaMB getLista() {
        return lista;
    }

    public void setLista(ListaMB lista) {
        this.lista = lista;
    }

    public PersonasMB getPersonas() {
        return personas;
    }

    public void setPersonas(PersonasMB personas) {
        this.personas = personas;
    }

    public Collection<ConfigCargoView> getCargosConfigurados() {
        return cargosConfigurados;
    }

    public void setCargosConfigurados(Collection<ConfigCargoView> cargosConfigurados) {
        this.cargosConfigurados = cargosConfigurados;
    }
    // </editor-fold>

}
