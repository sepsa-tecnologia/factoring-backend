/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.factura;

import fa.gs.misc.Numeric;
import fa.gs.misc.fechas.Fechas;
import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.numeric.Currency;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioBanco;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioPersona;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Entidades;
import py.com.sepsa.genisys.ejb.logic.impl.PF_FacturaVentaLote_BancoRio;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculoDesembolsoFacturaInfo;
import py.com.sepsa.genisys.ejb.logic.pojos.SolicitudDesembolsoResultadoInfo;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.integraciones.banco_rio.RespuestaCalculoNetoFacturas;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class FacturaVentaLote_BancoRio extends FacturaVentaLote_Base {

    @EJB
    private Entidades entidades;

    @EJB
    private BancoRio integracionBancoRio;

    @EJB
    private PF_FacturaVentaLote_BancoRio venderLote;

    @Getter
    @Setter
    private FacturaVentaLote_BancoRio.FormInput currentInput;

    private BancoRioPersona personaProveedor;

    private BancoRioBanco bancoProveedor;

    @PostConstruct
    public void init() {
        // Inicializar contenedor de datos.
        this.currentInput = new FacturaVentaLote_BancoRio.FormInput();
    }

    public void obtenerDatosProveedor(Persona entidadFinanciera) throws Throwable {
        // Obtener datos de adhesion con entidad.
        Integer idPersona = usuarioLogueado.getPersona().getId();
        Integer idPerfil = usuarioLogueado.getPerfil().getId();
        Integer idEntidadFinanciera = entidadFinanciera.getId();
        Result<Entidades.EstadoAdhesionEntidad> resEstado = entidades.obtenerEstadoAdhesion(idPersona, idPerfil, idEntidadFinanciera);
        Entidades.EstadoAdhesionEntidad estado = resEstado.value();

        // Control.
        if (estado == null) {
            throw Errors.illegalArgument("El proveedor debe estar adherido a la entidad financiera.", estado);
        }

        // Control.
        if (estado != Entidades.EstadoAdhesionEntidad.ACTIVO) {
            throw Errors.illegalArgument("El proveedor debe estar correctamente adherido a la entidad financiera. Actualmente cuenta con estado con %s.", estado);
        }

        // Obtener datos adicionales de adhesion con entidad.
        Result<BancoRioPersona> resPersona = integracionBancoRio.obtenerPersona(idPersona, idPerfil);
        this.personaProveedor = resPersona.value(null);

        // Control.
        if (personaProveedor == null) {
            throw Errors.illegalArgument("El proveedor debe estar correctamente adherido a la entidad financiera. No se encontraron datos del proveedor.");
        }

        // Obtener banco.
        Result<BancoRioBanco> resBanco = integracionBancoRio.obtenerBanco(personaProveedor.getIdBanco());
        this.bancoProveedor = resBanco.value(null);

        // Control.
        if (personaProveedor == null) {
            throw Errors.illegalArgument("El proveedor debe estar correctamente adherido a la entidad financiera. No se encontraron datos de la entidad financiera.");
        }
    }

    public void calcularDesembolsos(List<FacturaInfo> facturas, Persona entidadFinanciera) throws Throwable {
        try {
            Collection<CalculoDesembolsoFacturaInfo> calculos = venderLote.calcularDesembolsos(usuarioLogueado.getUsuario(), usuarioLogueado.getPerfil(), facturas, entidadFinanciera, Fechas.now());
            currentInput.calculos.clear();
            currentInput.calculos.addAll(calculos);
        } catch (Throwable thr) {
            clearCalculos(currentInput.calculos, facturas);
            throw thr;
        }
    }

    public boolean esBancoRio(Integer idEntidadFinanciera) {
        return integracionBancoRio.esBancoRio(idEntidadFinanciera);
    }

    public String validar(Persona entidad) {
        // Validar nro de cuenta.
        if (personaProveedor == null || Assertions.stringNullOrEmpty(personaProveedor.getNroCuenta())) {
            return "Se debe especificar un número de cuenta para acreditación del desembolso.";
        }

        // Validar banco.
        if (bancoProveedor == null) {
            return "Se debe especificar el banco de plaza del número de cuenta indicado.";
        }

        // Validar que facturas esten habilitadas para venta.
        for (CalculoDesembolsoFacturaInfo calculo : currentInput.calculos) {
            if (calculo.getOk() == false) {
                return "No todas las facturas están habilitadas para el desembolso.";
            }
        }

        return null;
    }

    public String htmlNroCuentaProveedor() {
        if (personaProveedor == null) {
            return Text.nd();
        }

        if (Assertions.stringNullOrEmpty(personaProveedor.getNroCuenta())) {
            return Text.nd();
        }

        return personaProveedor.getNroCuenta();
    }

    public String htmlBancoProveedor() {
        if (bancoProveedor == null) {
            return Text.nd();
        }

        if (Assertions.stringNullOrEmpty(bancoProveedor.getDescripcion())) {
            return Text.nd();
        }

        return bancoProveedor.getDescripcion();
    }

    public String htmlInteresTotal(CalculoDesembolsoFacturaInfo info) {
        BigDecimal value = info.getPorcentajeDescuento();
        return Strings.format("%s %%", integracionBancoRio.formatearPorcentaje(value));
    }

    public String htmlDetalleEntidad(CalculoDesembolsoFacturaInfo info) {
        RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura extras = (RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura) info.getExtras();
        if (extras != null) {
            if (!info.getOk()) {
                StringBuilder2 builder = new StringBuilder2();
                builder.append("<div class=\"app_text_wrapped\"><strong>Estado:</strong>&nbsp;%s</div>", extras.getEstado());
                builder.append("<div class=\"app_text_wrapped\"><strong>Mensaje:</strong>&nbsp;%s</div>", extras.getMensaje());
                return builder.toString();
            } else {
                return htmlDetalleEntidad0(extras);
            }
        } else {
            return Text.nd();
        }
    }

    public static String htmlDetalleEntidad0(RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura detalle) {
        StringBuilder2 builder = new StringBuilder2();
        builder.append("<div class=\"app_text_wrapped\"><strong>Estado:</strong>&nbsp;%s</div>", detalle.getEstado());
        builder.append("<div class=\"app_text_wrapped\"><strong>Mensaje:</strong>&nbsp;%s</div>", detalle.getMensaje());
        builder.append("<div class=\"app_text_wrapped\"><strong>Uso de Plataforma:</strong>&nbsp;Gs. %s</div>", Currency.formatCurrency(detalle.getUsoPlataforma()));
        builder.append("<div class=\"app_text_wrapped\"><strong>Interés:</strong>&nbsp;Gs. %s</div>", Currency.formatCurrency(detalle.getInteres()));
        builder.append("<div class=\"app_text_wrapped\"><strong>Seguro (IVA Incluido):</strong>&nbsp;Gs. %s</div>", Currency.formatCurrency(Numeric.sum(detalle.getSeguro(), detalle.getIvaSeguro())));
        builder.append("<div class=\"app_text_wrapped\"><strong>Gastos Administrativos (IVA Incluido):</strong>&nbsp;Gs. %s</div>", Currency.formatCurrency(Numeric.sum(detalle.getGastosAdministrativos(), detalle.getIvaGastosAdministrativos())));
        builder.append("<div class=\"app_text_wrapped\"><strong>Tasa Anual Nominal:</strong>&nbsp;%s %%</div>", Currency.formatCurrency(detalle.getTasaAnualNominal()));
        builder.append("<div class=\"app_text_wrapped\"><strong>Tasa Anual Efectiva:</strong>&nbsp;%s %%</div>", Currency.formatCurrency(detalle.getTasaAnualEfectiva()));
        return builder.toString();
    }

    public SolicitudDesembolsoResultadoInfo vender(Integer idLote, Persona entidadFinanciera) {
        return venderLote.vender(usuarioLogueado.getUsuario(), usuarioLogueado.getPerfil(), idLote, currentInput.calculos, entidadFinanciera);
    }

    @Data
    public static class FormInput implements Serializable {

        private List<CalculoDesembolsoFacturaInfo> calculos;
        private String comentarios;

        public FormInput() {
            this.comentarios = "";
            this.calculos = Lists.empty();
        }

    }

}
