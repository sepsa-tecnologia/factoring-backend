/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.varios;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;
import py.com.sepsa.genisys.ejb.logic.impl.Facades;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "monedas")
@SessionScoped
public class MonedasMB implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Fachadas remotas.
     */
    @EJB
    private Facades facades;

    /**
     * Lista de tipos de moneda cacheadas.
     */
    private List<Moneda> monedas;
    // </editor-fold>

    /**
     * Constructor.
     */
    public MonedasMB() {
        ;
    }

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        // Obtener todos los tipos de moneda disponibles.
        try {
            monedas = facades.getMonedaFacade().findAll();
        } catch (Throwable thr) {
            monedas = Collections.emptyList();
        }
    }

    /**
     * Metodo que permite obtener una representacion compleja en forma de texto
     * (HTML) de un objeto Moneda para su presentacion en la vista.
     *
     * @param moneda Objeto moneda.
     * @return Cadena HTML que representa al objeto.
     */
    public String monedaLabel(Moneda moneda) {
        String html = "<div><h4 class=\"list-group-item-heading\">%s %s</h4></div>";
        if (moneda == null) {
            return String.format(html, " ", " ");
        } else {
            return String.format(html, moneda.getDescripcion(), "(" + moneda.getAbreviatura() + ")");
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public List<Moneda> getMonedas() {
        return monedas;
    }

    public void setMonedas(List<Moneda> monedas) {
        this.monedas = monedas;
    }
    //</editor-fold>

}
