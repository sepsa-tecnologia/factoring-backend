/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers;

import fa.gs.criteria.facades.AbstractFacade;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;

/**
 * ManagedBean básico que encapsula las operaciones crear, eliminar y modificar
 * para una entidad dada.
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public abstract class AbmMB<T> extends BasicViewMB implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Objeto de tipo factura cargado manualmente.
     */
    protected T entity;

    // </editor-fold>
    /**
     * Constructor.
     */
    public AbmMB() {
        ;
    }

    @PostConstruct
    public void init() {
        initEntity();
    }

    /**
     * Inicializa la entidad a manipular.
     */
    public abstract void initEntity();

    /**
     * Permite obtener el facade para la manipulacion de la entidad.
     *
     * @return Facade relacionado a la entidad.
     */
    public abstract AbstractFacade<T> getFacade();

    /**
     * Persiste la entidad.
     *
     * @return true si la persistencia tuvo exito, false caso contrario.
     */
    public boolean guardar() {
        AbstractFacade<T> facade = getFacade();
        try {
            facade.create(entity);
            return true;
        } catch (Throwable thr) {
            log.warning(thr, "no se pudo persistir la entidad");
            return false;
        }
    }

    /**
     * Persiste las modificaciones sobre la entidad.
     *
     * @return true si la persistencia tuvo exito, false caso contrario.
     */
    public boolean editar() {
        AbstractFacade<T> facade = getFacade();
        try {
            facade.edit(entity);
            return true;
        } catch (Throwable thr) {
            log.warning(thr, "no se pudo editar la entidad");
            return false;
        }
    }

    /**
     * Elimina los datos persistidos de la entidad.
     *
     * @return true si la operacion tuvo exito, false caso contrario.
     */
    public boolean eliminar() {
        AbstractFacade<T> facade = getFacade();
        try {
            facade.remove(entity);
            return true;
        } catch (Throwable thr) {
            log.warning(thr, "no se pudo eliminar la entidad");
            return false;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public <T> T getEntity() {
        return ((T) entity);
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }
    // </editor-fold>
}
