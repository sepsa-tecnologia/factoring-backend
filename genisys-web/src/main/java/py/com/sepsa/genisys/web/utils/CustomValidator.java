/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.utils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Clase base para la validacion de objetos.
 *
 * @author Fabio A. González Sosa
 */
public abstract class CustomValidator<T> implements Validator {

    /**
     * Implementación de la lógica de validación de Primefaces.
     *
     * @param context Contexto JSF.
     * @param component Componente dentro de la vista que requiere validacion de
     * sus datos.
     * @param value Dato a ser validado.
     * @throws ValidatorException en caso de que el objeto no sea válido.
     */
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String error = validate(((T) value));
        if (error != null && !error.isEmpty()) {
            FacesMessage msg = new MessageBuilder()
                    .severity(FacesMessage.SEVERITY_ERROR)
                    .summary("Error")
                    .detail(error)
                    .build();
            throw new ValidatorException(msg);
        }
    }

    /**
     * Método base para validar un objeto.
     *
     * @param value Objeto a validar.
     * @return Una cadena indicando el error presentado durante la validación,
     * null en caso de que el objeto sea valido.
     */
    public abstract String validate(T value);

}
