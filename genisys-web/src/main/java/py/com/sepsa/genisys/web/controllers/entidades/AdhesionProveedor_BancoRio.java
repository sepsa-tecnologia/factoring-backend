/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.entidades;

import com.google.gson.JsonObject;
import fa.gs.misc.Assertions;
import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.json.JsonResolver;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioActividadEconomica;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioBanco;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioCiudad;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioRegion;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoSociedad;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaFisicaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaFisicaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaJuridicaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.RegistroProveedorPersonaJuridicaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ContactoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.CuentaInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DireccionInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.ReferenciaInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.RubroInfo;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.SociedadInfo;
import py.com.sepsa.genisys.web.controllers.sesion.UsuarioLogueadoMB;
import py.com.sepsa.genisys.web.utils.Html;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class AdhesionProveedor_BancoRio extends AdhesionProveedor_Base {

    @Inject
    private UsuarioLogueadoMB usuarioLogueado;

    @EJB
    private BancoRio integracionBancoRio;

    /**
     * Referencia comercial. El banco necesita conocer al menos un comprador del
     * proveedor para determinar cuestiones como: el oficial de cuentas.
     */
    private Persona compradorReferencia;

    @Getter
    @Setter
    private FormInput currentInput;

    @Getter
    @Setter
    private Collection<BancoRioTipoSociedad> tiposSociedad;

    @Getter
    @Setter
    private Collection<BancoRioActividadEconomica> actividadesEconomicas;

    @Getter
    @Setter
    private Collection<BancoRioCiudad> ciudades;

    @Getter
    @Setter
    private Collection<BancoRioBanco> bancos;

    public boolean esBancoRio(Integer idEntidadFinanciera) {
        return integracionBancoRio.esBancoRio(idEntidadFinanciera);
    }

    public void onEntidadSelected() {
        // Inicializar contenedor de datos de entrada.
        this.currentInput = new FormInput();

        // Inicializar datos seleccionables.
        this.tiposSociedad = obtenerTipoSociedadesSeleccionables();
        this.actividadesEconomicas = obtenerActividadesEconomicasSeleccionables();
        this.ciudades = obtenerCiudadesSeleccionables();
        this.bancos = obtenerBancosSeleccionables();

        // Inicializar relaciones comerciales.
        this.compradorReferencia = obtenerCompradorReferencia();
    }

    private Persona obtenerCompradorReferencia() {
        Integer idProveedor = usuarioLogueado.getPersona().getId();
        Result<Persona> result = integracionBancoRio.obtenerCompradorAdherido(idProveedor);
        if (result.isFailure()) {
            log.error(result.failure().cause(), result.failure().message());
        }
        return result.value(null);
    }

    public boolean tieneCompradorReferencia() {
        return compradorReferencia != null;
    }

    public String htmlCompradorTipo() {
        return Text.tipoPersona(compradorReferencia);
    }

    public String htmlCompradorRazonSocial() {
        return Text.razonSocial(compradorReferencia);
    }

    public String htmlCompradorRuc() {
        return Text.ruc(compradorReferencia);
    }

    private Collection<BancoRioActividadEconomica> obtenerActividadesEconomicasSeleccionables() {
        Result<Collection<BancoRioActividadEconomica>> result = integracionBancoRio.obtenerActividadesEconomicas();
        if (result.isFailure()) {
            log.error(result.failure().cause(), result.failure().message());
        }
        return result.value(Lists.empty());
    }

    public String actividadEconomicaLabel(BancoRioActividadEconomica item) {
        if (item == null) {
            return Html.selectItemEmpty();
        } else {
            return Html.selectItem(item.getDescripcion(), item.getCodigo());
        }
    }

    private Collection<BancoRioTipoSociedad> obtenerTipoSociedadesSeleccionables() {
        Result<Collection<BancoRioTipoSociedad>> result = integracionBancoRio.obtenerTiposSociedades();
        if (result.isFailure()) {
            log.error(result.failure().cause(), result.failure().message());
        }
        return result.value(Lists.empty());
    }

    public String tipoSociedadLabel(BancoRioTipoSociedad item) {
        if (item == null) {
            return Html.selectItemEmpty();
        } else {
            return Html.selectItem(item.getDescripcion(), item.getSigla());
        }
    }

    private Collection<BancoRioCiudad> obtenerCiudadesSeleccionables() {
        Result<Collection<BancoRioCiudad>> result = integracionBancoRio.obtenerCiudades();
        if (result.isFailure()) {
            log.error(result.failure().cause(), result.failure().message());
        }
        return result.value(Lists.empty());
    }

    public String ciudadLabel(BancoRioCiudad item) {
        if (item == null) {
            return Html.selectItemEmpty();
        } else {
            return Html.selectItem(item.getDescripcion(), item.getCodigo());
        }
    }

    private Collection<BancoRioBanco> obtenerBancosSeleccionables() {
        Result<Collection<BancoRioBanco>> result = integracionBancoRio.obtenerBancos();
        if (result.isFailure()) {
            log.error(result.failure().cause(), result.failure().message());
        }
        return result.value(Lists.empty());
    }

    public String bancoLabel(BancoRioBanco item) {
        if (item == null) {
            return Html.selectItemEmpty();
        } else {
            return Html.selectItem(item.getDescripcion(), item.getCodigo());
        }
    }

    public void onCiudadSelected(AjaxBehaviorEvent event) throws Throwable {
        try {
            // Validar relaciones anteriores.
            if (currentInput.ciudad != null) {
                // Obtener region correspondiente.
                Result<BancoRioRegion> resRegion = integracionBancoRio.obtenerRegionPorCodigo(currentInput.ciudad.getCodigoRegion());
                resRegion.raise();
                currentInput.region = resRegion.value();
            } else {
                currentInput.region = null;
            }
        } catch (Throwable thr) {
            throw Errors.illegalState(thr, "Ocurrió un error seleccionando ciudad.");
        }
    }

    public Collection<BancoRioRegion> regionesSeleccionables() {
        Result<Collection<BancoRioRegion>> result = integracionBancoRio.obtenerRegiones();
        if (result.isFailure()) {
            log.error(result.failure().cause(), result.failure().message());
        }
        return result.value(Lists.empty());
    }

    public String regionLabel(BancoRioRegion item) {
        if (item == null) {
            return Html.selectItemEmpty();
        } else {
            return Html.selectItem(item.getDescripcion(), item.getCodigo());
        }
    }

    public String validarInput(Persona entidad) {
        // Validacion de adhesion de persona fisica.
        if (usuarioLogueado.esPersonaFisica()) {
            return validarInputPersonaFisica(entidad);
        }

        // Validacion de adhesion de persona juridica.
        if (usuarioLogueado.esPersonaJuridica()) {
            return validarInputPersonaJuridica(entidad);
        }

        // Ok.
        return null;
    }

    private String validarInputPersonaFisica(Persona entidad) {
        String tmp = validarInputComunes(entidad);
        if (tmp != null) {
            return tmp;
        }

        if (currentInput.getFechaConstitucion() == null) {
            return "Se debe especificar una fecha de nacimiento válida.";
        }

        if (currentInput.getCedulaIdentidad() == null) {
            return "Se debe especificar un comprobante de cédula de identidad válida.";
        }

        return null;
    }

    private String validarInputPersonaJuridica(Persona entidad) {
        String tmp = validarInputComunes(entidad);
        if (tmp != null) {
            return tmp;
        }

        if (currentInput.getTipoSociedad() == null) {
            return "Se debe especificar un tipo de sociedad";
        }

        if (currentInput.getFechaConstitucion() == null) {
            return "Se debe especificar una fecha de constitución válida.";
        }

        if (currentInput.getCedulaTributaria() == null) {
            return "Se debe especificar un comprobante de cédula tributaria válida.";
        }

        return null;
    }

    private String validarInputComunes(Persona entidad) {
        if (Assertions.stringNullOrEmpty(currentInput.getNroCuenta())) {
            return "Se debe especificar un número de cuenta para acreditación";
        }

        if (currentInput.getBanco() == null) {
            return "Se debe especificar un banco de plaza para cuenta de acreditación.";
        }

        if (currentInput.getActividadEconomica() == null) {
            return "Se debe especificar una actividad económica.";
        }

        if (currentInput.getRegion() == null) {
            return "Se debe especificar una región geográfica de referencia.";
        }

        if (currentInput.getCiudad() == null) {
            return "Se debe especificar una ciudad de referencia.";
        }

        if (currentInput.getFacturacionPromedio() == null) {
            return "Se debe especificar una facturación promedio.";
        }
        if (!Assertions.isPositiveDecimal(currentInput.getFacturacionPromedio())) {
            return "Se debe especificar una facturación promedio mayor a cero.";
        }

        return null;
    }

    public Void onConfirmar(Persona entidad) throws Throwable {
        Integer idPersona = usuarioLogueado.getPersona().getId();
        Integer idPerfil = usuarioLogueado.getPerfil().getId();
        Integer idEntidadFinanciera = entidad.getId();

        // Registro de persona fisica.
        if (usuarioLogueado.esPersonaFisica()) {
            RegistroProveedorPersonaFisicaInput input = adaptRegistroProveedorPersonaFisicaInput();
            Result<RegistroProveedorPersonaFisicaOutput> result = integracionBancoRio.registrarProveedor(idPersona, idPerfil, idEntidadFinanciera, input);
            result.raise();
        }

        // Registro de persona juridica.
        if (usuarioLogueado.esPersonaJuridica()) {
            RegistroProveedorPersonaJuridicaInput input = adaptRegistroProveedorPersonaJuridicaInput();
            Result<RegistroProveedorPersonaJuridicaOutput> result = integracionBancoRio.registrarProveedor(idPersona, idPerfil, idEntidadFinanciera, input);
            result.raise();
        }

        // Ok.
        return null;
    }

    private RegistroProveedorPersonaFisicaInput adaptRegistroProveedorPersonaFisicaInput() {
        // Datos de proveedor.
        String nombres0 = usuarioLogueado.getPersona().getPfNombres();
        String apellidos0 = usuarioLogueado.getPersona().getPfApellidos();
        String ci0 = usuarioLogueado.getPersona().getPfCi();

        // Datos de documento (proveedor).
        DocumentoInfo documentoProveedor0 = integracionBancoRio.resolveDocumento(usuarioLogueado.getPersona());
        documentoProveedor0.setImagen(getFileContent(currentInput.cedulaIdentidad));
        documentoProveedor0.setNroDocumento(ci0);

        // Datos de direccion.
        DireccionInfo direccion0 = new DireccionInfo();
        direccion0.setCalle1(usuarioLogueado.getDireccion().getCalle1());
        direccion0.setCalle2(usuarioLogueado.getDireccion().getCalle2());
        direccion0.setReferencia("Y");
        direccion0.setNroCalle(usuarioLogueado.getDireccion().getNroCalle());
        direccion0.setCalle3(usuarioLogueado.getDireccion().getCalle3());
        direccion0.setBarrio(usuarioLogueado.getDireccion().getBarrio());
        direccion0.setCiudad(currentInput.ciudad);
        direccion0.setRegion(currentInput.region);

        // Datos de contacto.
        ContactoInfo contacto0 = new ContactoInfo();
        contacto0.setNroLineaBaja(usuarioLogueado.getContacto().getNroLineaBaja());
        contacto0.setNroCelular(usuarioLogueado.getContacto().getNroCelular());
        contacto0.setEmail(usuarioLogueado.getContacto().getEmail());

        // Datos de acreditacion.
        CuentaInfo cuenta0 = new CuentaInfo();
        cuenta0.setNroCuenta(currentInput.nroCuenta);
        cuenta0.setBanco(currentInput.banco);

        // Datos de rubro.
        // TODO: INCLUIR RUBRO.
        RubroInfo rubro0 = new RubroInfo();
        rubro0.setDescripcion("EL RUBRO");

        // Datos de referencia comercial.
        ReferenciaInfo referencia0 = new ReferenciaInfo();
        referencia0.setPersona(compradorReferencia);
        referencia0.setDocumento(integracionBancoRio.resolveDocumento(compradorReferencia));

        // Datos finales.
        RegistroProveedorPersonaFisicaInput instance = new RegistroProveedorPersonaFisicaInput();
        instance.setDocumento(documentoProveedor0);
        instance.setNombres(nombres0);
        instance.setApellidos(apellidos0);
        instance.setFechaNacimiento(currentInput.fechaConstitucion);
        instance.setDireccion(direccion0);
        instance.setContacto(contacto0);
        instance.setActividadEconomica(currentInput.actividadEconomica);
        instance.setRubro(rubro0);
        instance.setCuentaAcreditacion(cuenta0);
        instance.setFacturacionPromedio(currentInput.facturacionPromedio);
        instance.setComprador(referencia0);
        return instance;
    }

    private byte[] getFileContent(JsonObject json) {
        String content = JsonResolver.string(json, "fileContent");
        return Base64.getMimeDecoder().decode(content);
    }

    private RegistroProveedorPersonaJuridicaInput adaptRegistroProveedorPersonaJuridicaInput() {
        // Datos de proveedor.
        String razonSocial0 = usuarioLogueado.getPersona().getPjRazonSocial();

        // Datos de documento.
        DocumentoInfo documento0 = integracionBancoRio.resolveDocumento(usuarioLogueado.getPersona());
        documento0.setImagen(getFileContent(currentInput.cedulaTributaria));

        // Datos de sociedad.
        SociedadInfo sociedad0 = new SociedadInfo();
        sociedad0.setTipoSociedad(currentInput.tipoSociedad);
        sociedad0.setFechaConstitucion(currentInput.fechaConstitucion);

        // Datos de direccion.
        DireccionInfo direccion0 = new DireccionInfo();
        direccion0.setCalle1(usuarioLogueado.getDireccion().getCalle1());
        direccion0.setCalle2(usuarioLogueado.getDireccion().getCalle2());
        direccion0.setReferencia("Y");
        direccion0.setNroCalle(usuarioLogueado.getDireccion().getNroCalle());
        direccion0.setCalle3(usuarioLogueado.getDireccion().getCalle3());
        direccion0.setBarrio(usuarioLogueado.getDireccion().getBarrio());
        direccion0.setCiudad(currentInput.ciudad);
        direccion0.setRegion(currentInput.region);

        // Datos de contacto.
        ContactoInfo contacto0 = new ContactoInfo();
        contacto0.setNroLineaBaja(usuarioLogueado.getContacto().getNroLineaBaja());
        contacto0.setNroCelular(usuarioLogueado.getContacto().getNroCelular());
        contacto0.setEmail(usuarioLogueado.getContacto().getEmail());

        // Datos de acreditacion.
        CuentaInfo cuenta0 = new CuentaInfo();
        cuenta0.setNroCuenta(currentInput.nroCuenta);
        cuenta0.setBanco(currentInput.banco);

        // Datos de rubro.
        // TODO: INCLUIR RUBRO.
        RubroInfo rubro0 = new RubroInfo();
        rubro0.setDescripcion("EL RUBRO");

        // Datos de referencia comercial.
        ReferenciaInfo referencia0 = new ReferenciaInfo();
        referencia0.setPersona(compradorReferencia);
        referencia0.setDocumento(integracionBancoRio.resolveDocumento(compradorReferencia));

        // Datos finales.
        RegistroProveedorPersonaJuridicaInput instance = new RegistroProveedorPersonaJuridicaInput();
        instance.setDocumento(documento0);
        instance.setRazonSocial(razonSocial0);
        instance.setSociedad(sociedad0);
        instance.setDireccion(direccion0);
        instance.setContacto(contacto0);
        instance.setActividadEconomica(currentInput.actividadEconomica);
        instance.setRubro(rubro0);
        instance.setCuentaAcreditacion(cuenta0);
        instance.setFacturacionPromedio(currentInput.facturacionPromedio);
        instance.setComprador(referencia0);
        return instance;
    }

    @Data
    public static class FormInput implements Serializable {

        private BancoRioActividadEconomica actividadEconomica;
        private BancoRioTipoSociedad tipoSociedad;
        private Date fechaConstitucion;
        private BigDecimal facturacionPromedio;
        private BancoRioCiudad ciudad;
        private BancoRioRegion region;
        private JsonObject cedulaTributaria;
        private JsonObject cedulaIdentidad;
        private String nroCuenta;
        private BancoRioBanco banco;
    }

}
