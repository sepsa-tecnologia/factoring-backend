/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.calculadora;

/**
 * Interface que define el comportamiento de un bean que puede ser utilizado en
 * la vista de calculadora.
 *
 * @author Fabio A. González Sosa
 */
public interface Calculador extends py.com.sepsa.genisys.ejb.logic.impl.Calculador {

    /**
     * Genera una seccion de HTML con detalles adicionales relacionados al
     * cáluclo, si hubiere.
     *
     * @return Cadena HTML con detalles adicionales relacionados al cálculo.
     */
    String htmlDetallesEntidad();

}
