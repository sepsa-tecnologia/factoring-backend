/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.entidades;

import fa.gs.misc.Assertions;
import fa.gs.misc.collections.Lists;
import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.misc.Units;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.text.StringBuilder2;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Ajax;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioPersona;
import py.com.sepsa.genisys.ejb.entities.factoring.BancoRioTipoDocumento;
import py.com.sepsa.genisys.ejb.entities.factoring.PersonaEntidadSolicitud;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.info.PersonaEntidadInfo;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.BancoRio;
import py.com.sepsa.genisys.ejb.logic.impl.Entidades;
import py.com.sepsa.genisys.ejb.logic.impl.Perfiles;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoCuentaInput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoCuentaOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoProveedorInput;
import py.com.sepsa.genisys.integraciones.banco_rio.ConsultaEstadoProveedorOutput;
import py.com.sepsa.genisys.integraciones.banco_rio.RespuestaConsultaEstadoCuenta;
import py.com.sepsa.genisys.integraciones.banco_rio.RespuestaConsultaEstadoProveedor;
import py.com.sepsa.genisys.integraciones.banco_rio.pojos.DocumentoInfo;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.Html;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "entidades")
@ViewScoped
public class EntidadesMB extends BasicViewMB implements Serializable {

    @Inject
    private Personas personas;

    @Inject
    private Entidades entidades;

    @Inject
    private Perfiles perfiles;

    @Inject
    private BancoRio integracionBancoRio;

    @Inject
    private TxWrapperWeb tx;

    @Inject
    @Getter
    @Setter
    private py.com.sepsa.genisys.web.controllers.entidades.ListaMB lista;

    @Getter
    @Setter
    private Collection<PersonaEntidadSolicitud> historicoSolicitudesAdhesion;

    @Getter
    @Setter
    private String htmlLastConsultaEstadoAdhesion;

    @Getter
    @Setter
    private RespuestaConsultaEstadoCuenta respuestaConsultaEstadoCuentaBancoRio;

    @Getter
    @Setter
    private RespuestaConsultaEstadoProveedor respuestaConsultaEstadoProveedorBancoRio;

    @PostConstruct
    public void init() {
        // Lista de historico de solicitudes realizadas a una entidad puntual.
        this.historicoSolicitudesAdhesion = Lists.empty();

        // Datos de consulta de estado de solicitud de adhesion.
        this.htmlLastConsultaEstadoAdhesion = "";

        // Datos de estado de cuenta para banco rio.
        this.respuestaConsultaEstadoCuentaBancoRio = null;
        this.respuestaConsultaEstadoProveedorBancoRio = null;
    }

    public void initSolicitarAdhesion() {
        Prime.redirect("app/abm/entidades/adhesion.xhtml?redirect-true");
    }

    public String htmlEstado(PersonaEntidadInfo item) {
        String text = estados.getTextoEstadoPersonaEntidad(item.getEstado());
        String color = estados.getColorEstadoPersonEntidad(item.getEstado());
        return Html.badge(text, color);
    }

    public String htmlRespuestaEntidadSolicitud(PersonaEntidadSolicitud item) {
        String respuesta = item.getRespuestaEntidad();
        if (Assertions.stringNullOrEmpty(respuesta)) {
            respuesta = "";
        }
        return respuesta.trim();
    }

    public String htmlEstadoTexto(PersonaEntidadInfo item) {
        String text = estados.getTextoEstadoPersonaEntidad(item.getEstado());
        return text;
    }

    public void onVerSolicitudesAdhesion(PersonaEntidadInfo item) {
        historicoSolicitudesAdhesion = entidades.obtenerSolicitudesAdhesionConEntidad(item.getId()).value(Lists.empty());
        updateViewHistoricoSolicitudAdhesionDialog();
        Prime.showSepsaDialog("historico_solicitud_adhesion_dialog");
    }

    public void onConsultarEstadoProveedor(PersonaEntidadInfo item) {
        // Control.
        if (!entidades.tieneEstado(item, Entidades.EstadoAdhesionEntidad.PENDIENTE)) {
            mensajes.showGlobalDialogMessage("Para consultar el estado de la solicitud se debe contar con el estado: PENDIENTE.");
            return;
        }

        // Consulta de estado.
        try {
            // Consulta de Estado - Banco Rio.
            if (esBancoRio(item.getIdEntidadFinanciera())) {
                // Consultar.
                Result<Boolean> resConsulta = tx.execute(() -> {
                    return onConsultarEstadoProveedorBancoRio(item);
                });
                // Control.
                if (resConsulta.isFailure()) {
                    Throwable thr0 = resConsulta.failure().cause().getCause();
                    if (thr0 != null) {
                        throw thr0;
                    }

                    Throwable thr1 = resConsulta.failure().cause();
                    if (thr1 != null) {
                        throw thr1;
                    }

                    throw Errors.illegalState("No se pudo consultar el estado de adhesión del proveedor.");
                }
            }
        } catch (Throwable thr) {
            log.error(thr, "Ocurrio un error consultando estado de cuenta");
            mensajes.showGlobalDialogMessage(thr.getMessage());
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Consulta de Estado de Solicitud - Banco Rio">
    private Boolean onConsultarEstadoProveedorBancoRio(PersonaEntidadInfo item) throws Throwable {
        Integer idPersona = item.getIdPersona();
        Integer idPerfil = item.getIdPerfil();
        Integer idEntidadFinanciera = item.getIdEntidadFinanciera();
        Integer idPerfilComprador = perfiles.obtenerPerfil(PerfilEnum.COMPRADOR).getId();

        // Obtener registro de proveedor.
        Result<Persona> resPersona = personas.obtenerPorId(idPersona);
        Persona personaProveedor = resPersona.value();

        // Obtener registro de proveedor con banco.
        Result<BancoRioPersona> resPersonaBancoRio = integracionBancoRio.obtenerPersona(idPersona, idPerfil);
        if (resPersonaBancoRio.isFailure()) {
            log.error(resPersonaBancoRio.failure().cause(), resPersonaBancoRio.failure().message());
            mensajes.showGlobalDialogMessage("No se pudo determinar la información de proveedor registrado para con la entidad financiera");
            return false;
        }

        // Persona proveedor.
        BancoRioPersona personaProveedorBanco = resPersonaBancoRio.value(null);

        // Obtener tipo de documento de proveedor
        Result<BancoRioTipoDocumento> resTipoDocumento = integracionBancoRio.obtenerTipoDocumento(personaProveedorBanco.getIdTipoDocumento());
        if (resTipoDocumento.isFailure()) {
            log.error(resTipoDocumento.failure().cause(), resTipoDocumento.failure().message());
            mensajes.showGlobalDialogMessage("No se pudo determinar el tipo de documento del proveedor registrado para con la entidad financiera");
            return false;
        }
        BancoRioTipoDocumento tipoDocumentoProveedor = resTipoDocumento.value();

        // Obtener registro de proveedor.
        resPersona = personas.obtenerPorId(personaProveedorBanco.getIdPersonaReferencia());
        Persona personaComprador = resPersona.value();

        // Obtener registro de comprador con banco.
        resPersonaBancoRio = integracionBancoRio.obtenerPersona(personaProveedorBanco.getIdPersonaReferencia(), idPerfilComprador);
        if (resPersonaBancoRio.isFailure()) {
            log.error(resPersonaBancoRio.failure().cause(), resPersonaBancoRio.failure().message());
            mensajes.showGlobalDialogMessage("No se pudo determinar la información de comprador registrado para con la entidad financiera");
            return false;
        }

        // Persona comprador.
        BancoRioPersona personaCompradorBanco = resPersonaBancoRio.value(null);

        // Obtener tipo de documento de comprador
        resTipoDocumento = integracionBancoRio.obtenerTipoDocumento(personaCompradorBanco.getIdTipoDocumento());
        if (resTipoDocumento.isFailure()) {
            log.error(resTipoDocumento.failure().cause(), resTipoDocumento.failure().message());
            mensajes.showGlobalDialogMessage("No se pudo determinar el tipo de documento del comprador registrado para con la entidad financiera");
            return false;
        }

        // Tipo de documento de comprador.
        BancoRioTipoDocumento tipoDocumentoComprador = resTipoDocumento.value();

        // Preparar parametros.
        ConsultaEstadoProveedorInput input = new ConsultaEstadoProveedorInput();
        input.setProveedor(personaProveedor);
        input.setProveedorDocumento(new DocumentoInfo());
        input.getProveedorDocumento().setTipoDocumento(tipoDocumentoProveedor);
        input.getProveedorDocumento().setNroDocumento(Text.ruc(personaProveedor));
        input.setComprador(personaComprador);
        input.setCompradorDocumento(new DocumentoInfo());
        input.getCompradorDocumento().setTipoDocumento(tipoDocumentoComprador);
        input.getCompradorDocumento().setNroDocumento(Text.ruc(personaComprador));

        // Consumir servicio.
        Result<ConsultaEstadoProveedorOutput> resConsulta = integracionBancoRio.consultarEstadoProveedor(idPersona, idPerfil, idEntidadFinanciera, input);
        if (resConsulta.isFailure()) {
            throw Errors.illegalState(resConsulta.failure().message());
        }

        // Actualizar estado de adhesion.
        ConsultaEstadoProveedorOutput output = resConsulta.value();
        if (output.getRespuesta().isOk()) {
            Integer idPersonaEntidad = item.getId();
            Result<Void> resActualizacion;

            // Determinar estado.
            py.com.sepsa.genisys.ejb.logic.impl.Entidades.EstadoAdhesionEntidad estadoAdhesion = resolveEstadoAdhesion(output.getRespuesta());

            // Actualizar estado de solicitud.
            resActualizacion = entidades.actualizarEstadoAdhesionEntidad(idPersonaEntidad, estadoAdhesion);
            if (resActualizacion.isFailure()) {
                log.error(resActualizacion.failure().cause(), resActualizacion.failure().message());
                mensajes.showGlobalDialogMessage("No se pudo actualizar el estado de la solicitud de adhesión.");
                return false;
            }

            // Actualizar codigo de cliente de persona si adhesion fue exitosa.
            if (estadoAdhesion == Entidades.EstadoAdhesionEntidad.ACTIVO) {
                resActualizacion = integracionBancoRio.actualizarCodCliente(idPersona, idPerfil, output.getRespuesta().getCodCliente());
                if (resActualizacion.isFailure()) {
                    log.error(resActualizacion.failure().cause(), resActualizacion.failure().message());
                    mensajes.showGlobalDialogMessage("No se pudo actualizar el código del cliente.");
                    return false;
                }
            }
        }

        // Procesar datos.
        if (output.getRespuesta().isOk()) {
            // Datos.
            Map<String, String> templateData = Maps.empty();
            templateData.put("codCliente", fa.gs.utils.misc.text.Text.normalize(output.getRespuesta().getCodCliente()));
            templateData.put("fechaSolicitud", fa.gs.utils.misc.text.Text.normalize(output.getRespuesta().getFechaInsercion()));
            templateData.put("estado", fa.gs.utils.misc.text.Text.normalize(output.getRespuesta().getEstado()));
            templateData.put("observaciones", fa.gs.utils.misc.text.Text.normalize(output.getRespuesta().getDecision()));

            // Generar cuerpo con datos.
            Configuration cfg = new Configuration(new Version(2, 3, 23));
            cfg.setClassForTemplateLoading(EntidadesMB.class, "/py/com/sepsa/genisys/core/templates");
            cfg.setDefaultEncoding("utf-8");
            cfg.setOutputEncoding("ut-8");
            Template template = cfg.getTemplate("estado_solicitud_adhesion_banco_rio.ftl");
            StringWriter writer = new StringWriter();
            template.process(templateData, writer);
            this.htmlLastConsultaEstadoAdhesion = writer.toString();
        } else {
            StringBuilder2 builder = new StringBuilder2();
            builder.append("<p><strong>%s</strong></p>", output.getRespuesta().getMensaje());
            this.htmlLastConsultaEstadoAdhesion = builder.toString();
        }

        // Mostrar datos.
        Ajax.update("estado_solicitud_adhesion_dialog:estado_solicitud_adhesion_dialog_form");
        Prime.showSepsaDialog("estado_solicitud_adhesion_dialog");

        // Actualizar tabla.
        recargarListaEntidades();

        return true;
    }

    private py.com.sepsa.genisys.ejb.logic.impl.Entidades.EstadoAdhesionEntidad resolveEstadoAdhesion(RespuestaConsultaEstadoProveedor respuesta) {
        String msg = Units.execute("", () -> respuesta.getEstado().toUpperCase().trim());

        if (msg.startsWith("APROBAD")) {
            return py.com.sepsa.genisys.ejb.logic.impl.Entidades.EstadoAdhesionEntidad.ACTIVO;
        }

        if (msg.startsWith("RECHAZAD")) {
            return py.com.sepsa.genisys.ejb.logic.impl.Entidades.EstadoAdhesionEntidad.RECHAZADO;
        }

        return py.com.sepsa.genisys.ejb.logic.impl.Entidades.EstadoAdhesionEntidad.PENDIENTE;
    }
    //</editor-fold>

    public void onConsultarEstadoCuenta(PersonaEntidadInfo item) {
        // Control.
        if (!entidades.tieneEstado(item, Entidades.EstadoAdhesionEntidad.ACTIVO)) {
            mensajes.showGlobalDialogMessage("Para consultar el estado de cuenta se debe contar con el estado: ACTIVO.");
            return;
        }

        // Consulta de estado.
        try {
            // Consulta de Estado - Banco Rio.
            if (esBancoRio(item.getIdEntidadFinanciera())) {
                onConsultarEstadoCuentaBancoRio(item);
            }
        } catch (Throwable thr) {
            log.error(thr, "Ocurrio un error consultando estado de cuenta");
            mensajes.showGlobalDialogMessage(thr.getMessage());
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Consulta de Estado de Cuenta - Banco Rio">
    private boolean esBancoRio(Integer idEntidadFinanciera) {
        return integracionBancoRio.esBancoRio(idEntidadFinanciera);
    }

    private void onConsultarEstadoCuentaBancoRio(PersonaEntidadInfo item) throws Throwable {
        Integer idPersona = item.getIdPersona();
        Integer idPerfil = item.getIdPerfil();
        Integer idEntidadFinanciera = item.getIdEntidadFinanciera();

        // Obtener registro de persona con banco.
        Result<BancoRioPersona> resPersonaBancoRio = integracionBancoRio.obtenerPersona(idPersona, idPerfil);
        if (resPersonaBancoRio.isFailure()) {
            log.error(resPersonaBancoRio.failure().cause(), resPersonaBancoRio.failure().message());
            mensajes.showGlobalDialogMessage("No se pudo determinar la información de persona registrada para con la entidad financiera");
            return;
        }

        // Datos de persona con banco.
        BancoRioPersona persona = resPersonaBancoRio.value(null);
        if (persona == null) {
            mensajes.showGlobalDialogMessage("No se pudo determinar la información de persona registrada para con la entidad financiera");
            return;
        }

        // Preparar parametros.
        ConsultaEstadoCuentaInput input = new ConsultaEstadoCuentaInput();
        input.setDocumento(integracionBancoRio.resolveDocumento(usuarioLogueado.getPersona()));

        // Consumir servicio.
        Result<ConsultaEstadoCuentaOutput> resConsulta = integracionBancoRio.consultarEstadoCuenta(idPersona, idPerfil, idEntidadFinanciera, input);
        resConsulta.raise();

        // Mostrar datos.
        ConsultaEstadoCuentaOutput output = resConsulta.value();
        if (output.getRespuesta().isOk()) {
            this.respuestaConsultaEstadoCuentaBancoRio = output.getRespuesta();
            Ajax.update("estado_cuenta_banco_rio_dialog:estado_cuenta_banco_rio_dialog_form");
            Prime.showSepsaDialog("estado_cuenta_banco_rio_dialog");
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Gestion de Tabla de Datos">
    public void filtrarLista() {
        lista.filter();
        updateViewFiltrosForm();
    }

    public void limpiarFiltroForm() {
        CustomFilter filtro = lista.getFiltro();
        if (!filtro.isEnabled()) {
            filtro.clearFilterValues();
            updateViewFiltrosForm();
        }
    }

    public void recargarListaEntidades() {
        lista.clearSelections();
        updateViewListaDeEntidades();
        lista.reload();
        updateViewListaDeEntidades();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Gestion de Actualizacion de Vista">
    private void updateViewFiltrosForm() {
        Ajax.update("filtro_entidades_panel:filtro_entidades_form");
        Prime.showBlockUi("bui", lista.getFiltro().isEnabled());
        updateViewListaDeEntidades();
    }

    private void updateViewListaDeEntidades() {
        lista.clearSelections();
        Ajax.update("tabla_entidades_panel:tabla_entidades_form:tabla_entidades");
        Prime.executeJs("$.fn.matchHeight._update();");
    }

    private void updateViewHistoricoSolicitudAdhesionDialog() {
        Ajax.update("historico_solicitud_adhesion_dialog:historico_solicitud_adhesion_dialog_form");
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public String getBtnFiltrarText() {
        if (lista.getFiltro().isEnabled()) {
            return "Deshacer Filtro";
        } else {
            return "Filtrar Datos";
        }
    }

    public String getOpcionesPanelClass() {
        if (usuarioLogueado.esAdministrador()) {
            return "hidden";
        } else {
            return "col-md-2";
        }
    }

    public String getListaEntidadesPanelClass() {
        if (usuarioLogueado.esAdministrador()) {
            return "col-md-12";
        } else {
            return "col-md-10";
        }
    }
    //</editor-fold>

}
