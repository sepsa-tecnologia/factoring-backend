/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.notificaciones;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.entities.notificacion.NotificacionEmailFactoring;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.utils.email.EmailContent;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;

/**
 *
 * @author Alexander Triana Ríos.
 */
@Named(value = "notificacionesContenidoMB")
@ViewScoped
public class NotificacionesContenidoMB extends BasicViewMB implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    @EJB
    private Notificaciones notificaciones;

    /**
     * Parámetro de peticion que indica el identificador de notificacion,
     * utilizado en la vista de notificaciones.
     */
    private String idNotificacionParam;

    /**
     * Identificador de notificacion obtenido desde el parametro de peticion en
     * la vista de notificaciones.
     */
    private Integer idNotificacion;

    /**
     * Registro de notificacion asociado al identificador de notificacion
     * recibido como parametro.
     */
    private NotificacionEmailFactoring notificacion;
    //</editor-fold>

    /**
     * Inicializa el managed bean.
     */
    public void init() {
        // Obtener valor original de identificador de notificacion.
        idNotificacion = utils.unmaskInteger(idNotificacionParam);

        // Obtener registro de notificacion asociado a identificador, si hubiere.
        try {
            notificacion = facades.getNotificacionEmailFactoringFacade().find(idNotificacion);
        } catch (Throwable thr) {
            notificacion = null;
        }
    }

    /**
     * Replica el contenido de la notificacion por email para desplegar en la
     * vista HTML.
     *
     * @return Texto que representa el contenido de la notificación o un mensaje
     * de advertencia en caso de error.
     */
    public String getContenidoNotificacion() {
        try {
            EmailContent content = notificaciones.generarContenidoNotificacionEmail(notificacion, false);
            return content.getCuerpo();
        } catch (Throwable thr) {
            thr.printStackTrace(System.err);
            return "No se pudo cargar el contenido de la notificación. Vuelva a intentar.";
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public String getIdNotificacionParam() {
        return idNotificacionParam;
    }

    public void setIdNotificacionParam(String idNotificacionParam) {
        this.idNotificacionParam = idNotificacionParam;
    }

    public NotificacionEmailFactoring getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(NotificacionEmailFactoring notificacion) {
        this.notificacion = notificacion;
    }
    //</editor-fold>
}
