/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.usuarios;

import fa.gs.misc.Assertions;
import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.collections.Sets;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ValidationException;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.impl.Sesiones;
import py.com.sepsa.genisys.ejb.logic.impl.Usuarios;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "usuarioAlta")
@ViewScoped
public class UsuarioAltaMB extends BasicViewMB implements Serializable {

    @Inject
    private Personas personas;

    @Inject
    private Sesiones sesiones;

    @Inject
    private Usuarios usuarios;

    @Inject
    private TxWrapperWeb tx;

    @Getter
    private Collection<Persona> personasSeleccionables;

    @Getter
    @Setter
    private FormInput input;

    @PostConstruct
    public void init() {
        // Inicializar conjunto de personas seleccionables.
        personasSeleccionables = initPersonasSeleccionables();

        // Inicializar formulario.
        input = new FormInput();
        input.persona = null;
        input.usuario = null;
        input.contrasena = null;
    }

    private Collection<Persona> initPersonasSeleccionables() {
        Set<Persona> personas0 = Sets.empty();
        personas0.addAll(this.personas.obtenerCompradoresDisponibles().value(Lists.empty()));
        personas0.addAll(this.personas.obtenerProveedoresDisponibles().value(Lists.empty()));
        personas0.addAll(this.personas.obtenerEntidadesFinancierasDisponibles().value(Lists.empty()));

        List<Persona> personas1 = Lists.empty();
        personas1.addAll(personas0);
        personas1.sort((Persona o1, Persona o2) -> o1.getRazonSocial().compareTo(o2.getRazonSocial()));
        return personas1;
    }

    public void guardar() {
        Result<Void> result = tx.execute(this::guardar0);
        if (result.isFailure()) {
            // Error.
            String msg = result.failure().cause().getMessage();
            mensajes.showGlobalDialogMessage(msg);
        } else {
            mensajes.showGlobalDialogMessage(() -> Prime.redirect("/app/abm/usuarios/lista.xhtml"), "Datos guardados exitosamente");
        }

        updateForm();
    }

    private Void guardar0() throws Throwable {
        // Validar datos de entrada.
        String err = validarInput(input);
        if (err != null) {
            throw new ValidationException(err);
        }

        // Registrar nuevo.
        usuarios.registrarNuevo(input.persona.getId(), input.usuario, input.contrasena);
        return null;
    }

    private String validarInput(FormInput input) {
        if (input.persona == null) {
            return "Se debe especificar un cliente";
        }

        if (Assertions.stringNullOrEmpty(input.usuario)) {
            return "Se debe especificar un nombre de usuario";
        }

        if (Assertions.stringNullOrEmpty(input.contrasena)) {
            return "Se debe especificar una contraseña";
        }

        Collection<?> existentes = sesiones.obtenerPorTransUsuario(input.usuario).value(Lists.empty());
        if (!Assertions.isNullOrEmpty(existentes)) {
            return "Se debe especificar un nombre de usuario que no exista previamente";
        }

        return null;
    }

    private void updateForm() {
        Prime.update("alta_usuario_form");
    }

    @Data
    public static class FormInput implements Serializable {

        private Persona persona;
        private String usuario;
        private String contrasena;
    }

}
