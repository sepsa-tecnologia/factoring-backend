/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.calculadora;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.varios.CalculadoraMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("calculadoraTmp")
@ViewScoped
public class CalculadoraTmpMB extends BasicViewMB implements Serializable {

    /**
     * Bean de datos para calculadora.
     */
    @Inject
    @Getter
    @Setter
    private CalculadoraMB calculadora;

    @PostConstruct
    public void init() {
        CalculadoraInput input = calculadora.initCalculadoraInput();
        calculadora.setInput(input);
        calculadora.setOutput(null);
    }

}
