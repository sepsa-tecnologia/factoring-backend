/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.utils;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.errors.Errors;
import java.util.Collection;
import java.util.Objects;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Fabio A. González Sosa
 */
@FacesConverter("genisys.SelectionModelItemConverter")
public class SelectionModelItemConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (Objects.equals(value, "---")) {
            return null;
        }

        Collection<Object> items = collectSelectionModelItems(component);
        for (Object item : items) {
            if (Objects.equals(value, String.valueOf(item))) {
                return item;
            }
        }

        throw Errors.illegalState();
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "---";
        }

        return String.valueOf(value);
    }

    public static Collection<Object> collectSelectionModelItems(UIComponent parent) {
        Collection<Object> items = Lists.empty();

        // Procesar componentes UISelectItems, no se da soporte a UISelectItem.
        for (UIComponent child : parent.getChildren()) {
            if (child instanceof UISelectItems) {
                UISelectItems uiSelectItems = (UISelectItems) child;
                collectSelectionModelItems(items, uiSelectItems);
            }
        }

        return items;
    }

    private static void collectSelectionModelItems(Collection<Object> items, UISelectItems uiSelectItems) {
        Object uiSelectItemsValue = uiSelectItems.getValue();
        if (uiSelectItemsValue instanceof Collection) {
            Collection selectionModelItems = (Collection) uiSelectItemsValue;
            for (Object selectionModelItem : selectionModelItems) {
                items.add(selectionModelItem);
            }
        }
    }

}
