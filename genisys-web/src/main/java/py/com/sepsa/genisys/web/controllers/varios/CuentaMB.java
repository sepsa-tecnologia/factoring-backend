/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.varios;

import fa.gs.utils.misc.Assertions;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.sesion.SesionMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "cuenta")
@ViewScoped
public class CuentaMB extends BasicViewMB implements Serializable {

    @Inject
    private SesionMB sesion;

    private String passwordActual;
    private String passwordNuevoConfirmacion;
    private String passwordNuevo;

    /**
     * Inicializa el managed bean.
     */
    @PostConstruct
    public void init() {
        passwordActual = "";
        passwordNuevoConfirmacion = "";
        passwordNuevo = "";
    }

    /**
     * Permite realizar el cambio de contraseña para un usuario logueado.
     */
    public void cambiarPassword() {
        // Validar datos de entrada.
        String error = validateInput();
        if (error != null) {
            mensajes.showGlobalDialogMessage(error);
        } else {
            // Intentamos modificar la contraseña.
            boolean success = facades.getUsuarioFacade().changePass(usuarioLogueado.getUsuario(), passwordNuevo, passwordNuevoConfirmacion, passwordActual, 'A');
            if (success) {
                try {
                    // Terminar sesion.
                    sesion.onLogout();
                } catch (Throwable thr) {
                    log.error(thr, "No se pudo cerrar la sesión.");
                    mensajes.showGlobalDialogMessage("No se pudo cerrar la sesión.");
                }
            } else {
                mensajes.showGlobalDialogMessage("La contraseña no es válida.");
            }
        }
    }

    /**
     * Valida los datos de entrada para el formulario de cambio de contraseña.
     *
     * @return Un mensaje indicando el error, si hubiere. Null en caso
     * contrario.
     */
    private String validateInput() {
        if (Assertions.stringNullOrEmpty(passwordActual)) {
            return "El campo contraseña actual no puede ser vacío.";
        }
        if (Assertions.stringNullOrEmpty(passwordNuevo)) {
            return "El campo contraseña nueva no puede ser vacío.";
        }
        if (Assertions.stringNullOrEmpty(passwordNuevoConfirmacion)) {
            return "El campo confirmación de contraseña nueva no puede ser vacío.";
        }
        if (!passwordNuevo.equals(passwordNuevoConfirmacion)) {
            return "La contraseña nueva y su confirmación no coinciden.";
        }
        if (passwordActual.equals(passwordNuevo)) {
            return "La nueva contraseña no puede ser igual a la actual.";
        }
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public String getPasswordActual() {
        return passwordActual;
    }

    public void setPasswordActual(String passwordActual) {
        this.passwordActual = passwordActual;
    }

    public String getPasswordActualConfirmacion() {
        return passwordNuevoConfirmacion;
    }

    public void setPasswordActualConfirmacion(String passwordActualConfirmacion) {
        this.passwordNuevoConfirmacion = passwordActualConfirmacion;
    }

    public String getPasswordNuevo() {
        return passwordNuevo;
    }

    public void setPasswordNuevo(String passwordNuevo) {
        this.passwordNuevo = passwordNuevo;
    }

    // </editor-fold>
}
