/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.utils;

import fa.gs.utils.misc.text.Strings;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialViewContext;
import org.omnifaces.util.Ajax;
import org.omnifaces.util.Components;
import org.omnifaces.util.Faces;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Prime {

    /**
     * Despliega un cuadro de diálogo de Primefaces en la vista del cliente.
     *
     * @param widgetVar Nombre del cuadro de dialogo.
     */
    public static void showPrimefacesDialog(String widgetVar) {
        executeJs(String.format("PF('%s').show();", widgetVar));
    }

    /**
     * Cierra un cuadro de diálogo de Primefaces en la vista del cliente.
     *
     * @param widgetVar Nombre del cuadro de dialogo.
     */
    public static void closePrimefacesDialog(String widgetVar) {
        executeJs(String.format("PF('%s').hide();", widgetVar));
    }

    /**
     * Despliega un cuadro de diálogo personalizado en la vista del cliente.
     *
     * @param dialogId Identificador del cuadro de dialogo.
     */
    public static void showSepsaDialog(String dialogId) {
        if (!dialogId.endsWith("_dlg")) {
            dialogId = String.format("%s_dlg", dialogId);
        }
        showPrimefacesDialog(dialogId);
    }

    /**
     * Cierra un cuadro de diálogo personalizado en la vista del cliente.
     *
     * @param dialogId Identificador del cuadro de dialogo.
     */
    public static void closeSepsaDialog(String dialogId) {
        if (!dialogId.endsWith("_dlg")) {
            dialogId = String.format("%s_dlg", dialogId);
        }
        closePrimefacesDialog(dialogId);
    }

    /**
     * Bloquea o desbloquea una porción de la vista utilizando BlockUI.
     *
     * @param widgetVar nombre del componente block ui.
     * @param show True si se desea mostrar el block ui, False caso contrario.
     */
    public static void showBlockUi(String widgetVar, boolean show) {
        if (show) {
            executeJs(String.format("PF('%s').show();", widgetVar));
        } else {
            executeJs(String.format("PF('%s').hide();", widgetVar));
        }
    }

    /**
     * Obtiene el contexto externo del entorno JSF.
     *
     * @return Contexto externo.
     */
    public static ExternalContext getExternalContext() {
        return FacesContext.getCurrentInstance().getExternalContext();
    }

    /**
     * Actualiza uno o varios componentes programaticamente desde un managed
     * bean.
     *
     * @param clientIds ID de componentes a actualizar.
     */
    public static void update(String... clientIds) {
        PartialViewContext viewContext = FacesContext.getCurrentInstance().getPartialViewContext();
        viewContext.getRenderIds().addAll(Arrays.asList(clientIds));
    }

    /**
     * Ejecuta un fragmento de código javascript en JSF.
     *
     * @param script código javascript a ejecutar.
     */
    public static void executeJs(String code) {
        if (Faces.isAjaxRequestWithPartialRendering()) {
            Ajax.oncomplete(code);
        } else {
            String script = Strings.format("$(document).ready(function(){%s;});", code);
            Components.addScript(script);
        }
    }

    /**
     * Permite realizar una redireccion en el lado cliente, de manera
     * programatica desde el lado del servidor..
     *
     * @param path Path relativo a la vista a la que se desea redireccionar.
     */
    public static void redirect(String path) {
        // Controlar que el path empiece con el caracter de separacion esperado.
        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        // Intentar redireccionar.
        try {
            ExternalContext context = Prime.getExternalContext();
            context.redirect(context.getRequestContextPath() + path);
        } catch (Exception e) {
            ;
        }
    }

    /**
     * Permite enviar un archivo como respuesta a una peticion JSF no ajax.
     *
     * @param file Archivo a procesar.
     * @param fileName Nombre de archivo para el lado del cliente.
     * @param contentLength Tamnanho del archivo en disco.
     * @param contentType MIME Type del archivo.
     * @throws Exception Si no se puede enviar el archivo.
     */
    public static void sendFile(File file, String fileName, int contentLength, String contentType) throws Exception {
        InputStream stream = new FileInputStream(file);
        Prime.sendFile(stream, fileName, contentLength, contentType);
    }

    /**
     * Permite enviar un archivo como respuesta a una peticion JSF no ajax.
     *
     * @param stream Stream de datos a enviar.
     * @param fileName Nombre de archivo para el lado del cliente.
     * @param contentLength Tamnanho del archivo en disco.
     * @param contentType MIME Type del archivo.
     * @throws Exception Si no se puede enviar el archivo.
     */
    public static void sendFile(InputStream stream, String fileName, int contentLength, String contentType) throws Exception {
        FacesContext ctx = FacesContext.getCurrentInstance();
        try (InputStream input = stream) {
            ExternalContext ec = ctx.getExternalContext();
            ec.responseReset();
            ec.setResponseContentType(contentType);
            ec.setResponseContentLength(contentLength);
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            try (OutputStream output = ec.getResponseOutputStream()) {
                byte[] chunk = new byte[4096];
                int read;
                while ((read = input.read(chunk)) >= 0) {
                    output.write(chunk, 0, read);
                    output.flush();
                }
            }
        }
        ctx.responseComplete();
    }

}
