/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.entidades;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Entidades;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.varios.PersonasMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "adhesionEntidadProveedor")
@ViewScoped
public class AdhesionProveedorMB extends BasicViewMB implements Serializable {

    @Inject
    private PersonasMB personas;

    @Inject
    private Entidades entidades;

    @Inject
    private TxWrapperWeb tx;

    @Getter
    @Setter
    private Persona entidadSelected;

    @Inject
    @Getter
    @Setter
    private AdhesionProveedor_BancoRio adhesionProveedorBancoRio;

    @PostConstruct
    public void init() {
        // Inicializar entidad financiera.
        this.entidadSelected = null;
    }

    public boolean entidadEstaSeleccionada() {
        return entidadSelected != null;
    }

    public boolean entidadSeleccionadaEsBancoRio() {
        if (entidadEstaSeleccionada()) {
            return adhesionProveedorBancoRio.esBancoRio(entidadSelected.getId());
        } else {
            return false;
        }
    }

    public Collection<Persona> getEntidadesSeleccionables() {
        return personas.getEntidadesFinancieras();
    }

    public void onEntidadSelectedChanged(AjaxBehaviorEvent event) {
        try {
            if (entidadSelected != null) {
                // Validar si existen relaciones anteriores.
                Result<Entidades.EstadoAdhesionEntidad> resEstadoAdhesion = entidades.obtenerEstadoAdhesion(usuarioLogueado.getPersona().getId(), usuarioLogueado.getPerfil().getId(), entidadSelected.getId());
                if (resEstadoAdhesion.isFailure()) {
                    mensajes.addMessageError("Error", resEstadoAdhesion.failure().message());
                    entidadSelected = null;
                    return;
                }
                Entidades.EstadoAdhesionEntidad estado = resEstadoAdhesion.value();
                if (estado != null && estado != Entidades.EstadoAdhesionEntidad.RECHAZADO) {
                    mensajes.addMessageWarning("Advertencia", Strings.format("Ya existe una solicitud previa con estado %s para la entidad seleccionada", estado.toString()));
                    entidadSelected = null;
                    return;
                }

                // Iniciar nueva solicitud de adhesion.
                if (entidadSeleccionadaEsBancoRio()) {
                    adhesionProveedorBancoRio.onEntidadSelected();
                }
            } else {
                mensajes.addMessageWarning("Advertencia", "Se debe seleccionar una entidad financiera.");
            }
        } catch (Throwable thr) {
            // Error.
            entidadSelected = null;
            log.warning(thr, "Ocurrió un error seleccionando entidad financiera para adhesion.");
            mensajes.addMessageError("Error", "Ocurrió un error seleccionando entidad financiera para adhesion.");
        }
    }

    public void confirmar() {
        // Validar datos de entrada.
        String err = validarInput();
        if (err != null) {
            mensajes.addMessageError("Error", err);
            return;
        }

        // Confirmar datos.
        Result<?> result = onConfirmar();
        if (result.isFailure()) {
            log.error(result.failure().cause(), "Ocurrio un error confirmando adhesion a entidad financiera");
            mensajes.showGlobalDialogMessage(result.failure().cause().getMessage());
        } else {
            mensajes.showGlobalDialogMessage("Solicitud Registrada exitosamente");
            entidadSelected = null;
        }
    }

    private Result<Void> onConfirmar() {
        Result<Void> result;

        try {
            // Confirmar registro - Banco Rio.
            if (entidadSeleccionadaEsBancoRio()) {
                Result<Void> txResult = tx.execute(() -> adhesionProveedorBancoRio.onConfirmar(entidadSelected));
                if (txResult.isFailure()) {
                    throw txResult.failure().cause();
                }
            }

            result = Results.ok()
                    .nullable(true)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .build();
        }

        return result;
    }

    private String validarInput() {
        // Validacion: Banco Rio.
        if (entidadSeleccionadaEsBancoRio()) {
            return adhesionProveedorBancoRio.validarInput(entidadSelected);
        }

        return null;
    }

}
