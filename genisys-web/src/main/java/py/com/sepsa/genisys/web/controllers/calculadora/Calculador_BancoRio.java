/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.calculadora;

import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraInput;
import py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraOutput;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.integraciones.banco_rio.RespuestaCalculoNetoFacturas;
import py.com.sepsa.genisys.web.controllers.factura.FacturaVentaLote_BancoRio;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Calculador_BancoRio implements Calculador {

    private final py.com.sepsa.genisys.ejb.logic.impl.Calculador_BancoRio impl;

    public Calculador_BancoRio() {
        this.impl = new py.com.sepsa.genisys.ejb.logic.impl.Calculador_BancoRio();
    }

    @Override
    public String validar() {
        return impl.validar();
    }
    
    @Override
    public String htmlDetallesEntidad() {
        if (getCalculadoraOutput() == null || getCalculadoraOutput().getExtras() == null) {
            return Text.nd();
        }

        RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura detalleCalculo = (RespuestaCalculoNetoFacturas.DetalleCalculoNetoFactura) getCalculadoraOutput().getExtras();
        return FacturaVentaLote_BancoRio.htmlDetalleEntidad0(detalleCalculo);
    }

    @Override
    public void calcular() throws Throwable {
        impl.calcular();
    }

    @Override
    public void setCalculadoraInput(CalculadoraInput input) {
        impl.setCalculadoraInput(input);
    }

    @Override
    public CalculadoraInput getCalculadoraInput() {
        return impl.getCalculadoraInput();
    }

    @Override
    public void setCalculadoraOutput(CalculadoraOutput output) {
        impl.setCalculadoraOutput(output);
    }

    @Override
    public CalculadoraOutput getCalculadoraOutput() {
        return impl.getCalculadoraOutput();
    }

}
