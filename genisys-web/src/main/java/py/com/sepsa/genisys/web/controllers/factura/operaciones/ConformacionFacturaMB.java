/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.factura.operaciones;

import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.numeric.Numeric;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.enterprise.context.Dependent;
import py.com.sepsa.genisys.ejb.entities.comercial.Cliente;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaConformada;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.NotaCredito;
import py.com.sepsa.genisys.ejb.entities.factoring.Retencion;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class ConformacionFacturaMB extends BasicViewMB implements Serializable {

    /**
     * Crea un contenedor inicial de datos a ser utilizados y procesados durante
     * la operacion de conformacion de facturas.
     *
     * @param idFactura Factura que se desea conformar.
     * @return Input con algunos valores calculados y precargados de manera
     * automatica.
     */
    public ConformacionFacturaMB.Input nuevoInputConformacionFactura(Integer idFactura) {
        // Nuevo input.
        ConformacionFacturaMB.Input input = new ConformacionFacturaMB.Input();

        try {
            // Inicializar el monto de nota de credito asociado, si hubiere.
            NotaCredito documentoNotaCredito = facades.getNotaCreditoFacade().findByIdFactura(idFactura);
            if (documentoNotaCredito != null) {
                input.setMontoNotaCredito(documentoNotaCredito.getMontoTotalNotaCredito());
            }

            // Inicializar el monto o porcentaje de retencion asociado, si hubiere.
            Retencion documentoRetencion = facades.getRetencionFacade().findByIdFactura(idFactura);
            if (documentoRetencion != null) {
                // Existe un documento de retencion asociado, utilizar valor porcentual.
                input.setValorRetencionEsPorcentual(true);
                input.setValorRetencion(documentoRetencion.getPorcentajeRetencion());
            } else {
                // Configuracion de retencion por defecto.
                try {
                    Cliente informacionComercialCliente = facades.getClienteFacade().findByIdCliente(usuarioLogueado.getPersona().getId());
                    if (informacionComercialCliente != null) {
                        Integer porcentajeConfigurado = informacionComercialCliente.getPorcentajeRetencion();
                        if (porcentajeConfigurado > 0) {
                            // No existe un documento de retencion asociado, pero si un porcentaje de retencion por defecto.
                            input.setValorRetencionEsPorcentual(true);
                            input.setValorRetencion(BigDecimal.valueOf(porcentajeConfigurado));
                        }
                    }
                } catch (Throwable thr) {
                    log.warning(thr, "Ocurrió un error consultando información de cliente");
                }
            }
        } catch (Throwable thr) {
            log.error(thr, "Ocurrió un error durante la inicialización de un nuevo input para conformación de facturas");
        }

        return input;
    }

    /**
     * Calcula el monto de retencion aplicable sobre una factura.
     *
     * @param Factura Factura con los montos de impuestos sujetos a retencion.
     * @param valorRetencion Valor de retencion.
     * @param valorRetencionEsPorcentual Indica si {@code valorRetencion} debe
     * considerarse como un monto o un valor porcentual entre 0 y 100.
     * @return Monto de retencion correspondiente.
     */
    private BigDecimal calcularMontoRetencion(FacturaInfo factura, BigDecimal valorRetencion, boolean valorRetencionEsPorcentual) {
        // Porcentaje de retencion mayor a 0.
        if (valorRetencionEsPorcentual && (Numeric.menor(valorRetencion, Numeric.CERO) || Numeric.mayor(valorRetencion, Numeric.CIEN))) {
            throw new IllegalArgumentException("El valor de retención, como valor porcentual, debe estar entre 0 y 100");
        }

        BigDecimal montoRetencion;
        if (valorRetencionEsPorcentual) {
            // Regla de tres simple para obtener el monto retenido: (porcentaje x monto) / 100.
            montoRetencion = Numeric.div(Numeric.mul(valorRetencion, factura.getMontoTotalIva()), Numeric.CIEN);
        } else {
            // Utilizar el valor como monto real.
            montoRetencion = valorRetencion;
        }
        return montoRetencion;
    }

    /**
     * Calcula el monto que puede ser conformado sobre una factura.
     *
     * @param factura Factura a ser conformada.
     * @param input Datos de entrada con montos posiblemente descontables.
     * @return Monto conformable para la factura.
     */
    private BigDecimal calcularMontoConformacion(FacturaInfo factura, ConformacionFacturaMB.Input input) {
        // Montos que pueden ser descontados de la factura a conformar.
        BigDecimal montoNotaCredito = input.montoNotaCredito;
        BigDecimal montoRetencion = calcularMontoRetencion(factura, input.valorRetencion, input.valorRetencionEsPorcentual);
        BigDecimal montoOtrosCargos = input.montoOtrosCargos;

        // Calcular el monto que puede descartarse del monto de la factura.
        BigDecimal montoDescontable = Numeric.sum(montoNotaCredito, montoRetencion, montoOtrosCargos);
        montoDescontable = Numeric.round(montoDescontable);

        // Calcular el monto conformable.
        BigDecimal montoFactura = factura.getMontoTotal();
        BigDecimal montoConformable = Numeric.sub(montoFactura, montoDescontable);
        return montoConformable;
    }

    /**
     * Crea un registro de factura conformada en base a los datos de entrada
     * proporcionados.
     *
     * @param factura Factura a ser conformada.
     * @param input Datos adicionales relacionados a la conformacion de la
     * factura.
     * @return Factura conformada.
     */
    public FacturaConformada crearFacturaConformada(FacturaInfo factura, ConformacionFacturaMB.Input input) {
        // Establecer datos iniciales de factura conformada.
        FacturaConformada facturaConformada = new FacturaConformada();
        facturaConformada.setId(factura.getId());
        facturaConformada.setMontoFactura(factura.getMontoTotal());
        facturaConformada.setMontoNotaCredito(input.montoNotaCredito);
        facturaConformada.setMontoRetencion(calcularMontoRetencion(factura, input.valorRetencion, input.valorRetencionEsPorcentual));
        facturaConformada.setMontoOtrosCargos(input.montoOtrosCargos);
        facturaConformada.setMontoConformado(calcularMontoConformacion(factura, input));

        // Inicializar campos relacionados a la venta de una factura, pero que no son utilizados durante la conformacio.
        facturaConformada.setPorcentajeComision(Numeric.CERO);
        facturaConformada.setMontoImponibleComision(Numeric.CERO);
        facturaConformada.setMontoImpuestoComision(Numeric.CERO);
        facturaConformada.setMontoTotalComision(Numeric.CERO);
        facturaConformada.setMontoImponibleGastosOperativos(Numeric.CERO);
        facturaConformada.setMontoImpuestoGastosOperativos(Numeric.CERO);
        facturaConformada.setMontoTotalGastosOperativos(Numeric.CERO);
        facturaConformada.setMontoNeto(Numeric.CERO);

        // Retornar registro de factura conformada.
        return facturaConformada;
    }

    /**
     * Valida si los datos ingresados para la conformacion de una factura son
     * validos.
     *
     * @param factura Factura a conformar.
     * @param input Datos ingresados.
     * @return Una cadena que indica el incumplimiento de restricciones
     * encontrado, caso contrario {@code null}.
     */
    public String validar(FacturaInfo factura, ConformacionFacturaMB.Input input) {
        // Monto de nota de credito no vacio.
        if (Assertions.isNull(input.montoNotaCredito)) {
            return "El campo monto de nota de crédito no puede ser vacío.";
        }

        // Monto de nota de credito debe ser mayor o igual a cero.
        if (Numeric.menor(input.montoNotaCredito, Numeric.CERO)) {
            return "El valor de monto de nota de crédito no puede ser inferior a cero.";
        }

        // Monto de nota de credito no superior a monto total de factura.
        if (Numeric.mayor(input.montoNotaCredito, factura.getMontoTotal())) {
            return "El valor de monto de nota de crédito no puede ser superior al monto total de la factura.";
        }

        // Tipo de valor de retencion.
        String tipoValorRetencion = (input.valorRetencionEsPorcentual) ? "porcentaje" : "monto";

        // Porcentaje/Monto de retencion no vacio.
        if (Assertions.isNull(input.valorRetencion)) {
            return String.format("El campo %s de retención no puede ser vacío.", tipoValorRetencion);
        }

        // Porcentaje/Monto de retencion mayor a cero.
        if (Numeric.menor(input.valorRetencion, Numeric.CERO)) {
            return String.format("El campo %s de retención no puede ser inferior a cero.", tipoValorRetencion);
        }

        // Porcentaje de retencion mayor a 0.
        if (input.valorRetencionEsPorcentual && Numeric.menor(input.valorRetencion, Numeric.CERO)) {
            return "El valor de porcentaje de retención no puede ser inferior a 0%";
        }

        // Porcentaje de retencion no superior o igual a 100.
        if (input.valorRetencionEsPorcentual && Numeric.mayorIgual(input.valorRetencion, Numeric.CIEN)) {
            return "El valor de porcentaje de retención no puede ser superior o igual al 100%";
        }

        // Monto de otros cargos no vacio.
        if (Assertions.isNull(input.montoOtrosCargos)) {
            return "El campo monto de otros cargos no puede ser vacío.";
        }

        // Monto de otros cargos mayor a cero.
        if (input.montoOtrosCargos.compareTo(Numeric.CERO) < 0) {
            return "El valor de monto de otros cargos no puede ser inferior a cero.";
        }

        // Monto de otros cargos no superior a monto total de factura.
        if (input.montoOtrosCargos.compareTo(factura.getMontoTotal()) > 0) {
            return "El valor de monto de otros cargos no puede ser superior al monto total de la factura.";
        }

        // La sumatoria de montos descontables no deben superar o igualar el valor total de la factura, es decir, el monto conformable debe ser mayor a cero.
        BigDecimal montoConformacion = calcularMontoConformacion(factura, input);
        if (Numeric.menorIgual(montoConformacion, Numeric.CERO)) {
            return "Los montos de nota de crédito/retención/otros, en conjunto, no pueden ser mayor o igual al monto total de la factura";
        }

        return null;
    }

    /**
     * Clase que sirve como contenedor de los valores de entrada para la
     * conformacion manual de una factura.
     */
    public static class Input {

        //<editor-fold defaultstate="collapsed" desc="Atributos">
        /**
         * Bandera que indica si el valor indicado como retencion representa un
         * monto ({@code false}) o un valor porcentual ({@code true}).
         */
        private Boolean valorRetencionEsPorcentual;

        /**
         * Valor utilizado al momento de conformar una factura, que representa
         * el porcentaje de retencion a aplicar sobre la factura seleccionada.
         */
        private BigDecimal valorRetencion;

        /**
         * Valor utilizado al momento de conformar una factura, que representa
         * el monto de nota de credito asociado a la factura seleccionada.
         */
        private BigDecimal montoNotaCredito;

        /**
         * Valor utilizado al momento de conformar una factura, que representa
         * el monto de otros cargos aplicados a la factura de manera externa por
         * el comprador/proveedor.
         */
        private BigDecimal montoOtrosCargos;

        /**
         * Factura conformada creada como paso previo a la confirmacion final de
         * la conformacion de una factura.
         */
        private FacturaConformada facturaConformada;
        //</editor-fold>

        /**
         * Constructor.
         */
        public Input() {
            montoNotaCredito = Numeric.CERO;
            valorRetencionEsPorcentual = false;
            valorRetencion = Numeric.CERO;
            montoOtrosCargos = Numeric.CERO;
            facturaConformada = null;
        }

        //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
        public Boolean getValorRetencionEsPorcentual() {
            return valorRetencionEsPorcentual;
        }

        public void setValorRetencionEsPorcentual(Boolean valorRetencionEsPorcentual) {
            this.valorRetencionEsPorcentual = valorRetencionEsPorcentual;
        }

        public BigDecimal getValorRetencion() {
            return valorRetencion;
        }

        public void setValorRetencion(BigDecimal valorRetencion) {
            this.valorRetencion = valorRetencion;
        }

        public BigDecimal getMontoNotaCredito() {
            return montoNotaCredito;
        }

        public void setMontoNotaCredito(BigDecimal montoNotaCredito) {
            this.montoNotaCredito = montoNotaCredito;
        }

        public BigDecimal getMontoOtrosCargos() {
            return montoOtrosCargos;
        }

        public void setMontoOtrosCargos(BigDecimal montoOtrosCargos) {
            this.montoOtrosCargos = montoOtrosCargos;
        }

        public FacturaConformada getFacturaConformada() {
            return facturaConformada;
        }

        public void setFacturaConformada(FacturaConformada facturaConformada) {
            this.facturaConformada = facturaConformada;
        }
        //</editor-fold>

    }
}
