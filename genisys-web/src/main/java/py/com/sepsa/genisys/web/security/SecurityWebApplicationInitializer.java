package py.com.sepsa.genisys.web.security;

import org.springframework.security.web.context.*;

/**
 * Inicializador para la seguridad web
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

    /**
     * Constructor.
     */
    public SecurityWebApplicationInitializer() {
        super(SecurityConfig.class);
    }
}
