/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.genisys.web.controllers.calculadora;

import java.util.Collection;
import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.sepsa.genisys.ejb.entities.info.Persona;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CalculadoraInput extends py.com.sepsa.genisys.ejb.logic.pojos.CalculadoraInput.Impl {

    /**
     * Lista de proveedores conveniados.
     */
    private Collection<Persona> proveedores;

    /**
     * Bandera que indica si el campo de proveedor puede limpiarse.
     */
    private Boolean canLimpiarProveedor;

    /**
     * Lista de proveedores conveniados.
     */
    private Collection<Persona> compradores;

    /**
     * Bandera que indica si el campo de comprador puede limpiarse.
     */
    private Boolean canLimpiarComprador;

    /**
     * Lista de proveedores conveniados.
     */
    private Collection<Persona> entidadesFinancieras;

    /**
     * Bandera que indica si el campo de entidad financiera puede limpiarse.
     */
    private Boolean canLimpiarEntidadFinanciera;

    /**
     * Bandera que indica si es posible modificar desde la vista el valor de la
     * fecha de pago de factura.
     */
    private boolean canEditFechaPagoFactura;

    /**
     * Bandera que indica si es posible modificar desde la vista el valor del
     * monto de la factura.
     */
    private boolean canEditMontoFactura;

}
