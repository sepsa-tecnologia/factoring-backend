/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.convenio.operaciones;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.numeric.Numeric;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.factoring.ConfigCargo;
import py.com.sepsa.genisys.ejb.entities.factoring.Convenio;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.TipoCargoEnum;
import py.com.sepsa.genisys.ejb.enums.TipoConvenioEnum;
import py.com.sepsa.genisys.ejb.enums.TipoValorEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.logic.impl.TipoTasasComision;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.convenio.ConveniosMB;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "altaConvenio")
@ViewScoped
public class AltaConvenioMB extends BasicViewMB {

    @Inject
    private Personas personas;

    /**
     * Proveedores disponibles.
     */
    @Getter
    @Setter
    private Collection<Persona> proveedores;

    /**
     * Compradores disponibles.
     */
    @Getter
    @Setter
    private Collection<Persona> compradores;

    /**
     * Entidades financieras disponibles.
     */
    @Getter
    @Setter
    private Collection<Persona> entidades;

    /**
     * Lista de tasas de comision disponibles.
     */
    @Getter
    @Setter
    private List<TipoTasasComision.TipoTasaComisionEnum> tasasComision;

    @Inject
    private TxWrapperWeb tx;

    /**
     * Contiene los datos de entrada para la creacion de un convenio.
     */
    @Getter
    @Setter
    private AltaConvenioMB.Input input;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        // Inicializar valores seleccionables.
        this.proveedores = personas.obtenerProveedoresDisponibles().value(Lists.empty());
        this.compradores = personas.obtenerCompradoresDisponibles().value(Lists.empty());
        this.entidades = personas.obtenerEntidadesFinancierasDisponibles(null).value(Lists.empty());
        this.tasasComision = beans.getTipoTasasComision().obtenerTipoTasasComision();
        this.input = new Input();
    }

    /**
     * Valida un valor porcentual dentro de un rango especifico.
     *
     * @param porcentaje Valor porcentual.
     * @param nombreCampo Nombre a utilizar como nombre de campo en el mensaje
     * de error, si hubiere.
     * @param noIgualCero Indica si el valor porcentual no puede ser igual a
     * cero.
     * @param noIgualCien Inidica si el valor porcentual no puede ser igual a
     * cien.
     * @return Mensaje de texto indicando el error de validacion encontrado, si
     * hubiere.
     */
    private String validarPorcentaje(BigDecimal porcentaje, String nombreCampo, boolean noIgualCero, boolean noIgualCien) {
        // Porcentaje no puede ser nulo.
        if (porcentaje == null) {
            return String.format("El valor del porcentaje de %s no puede ser nulo", nombreCampo);
        }

        // Porcentaje no puede ser menor a 0%.
        if (Numeric.menor(porcentaje, Numeric.CERO)) {
            return String.format("El valor de porcentaje de %s no puede ser inferior a 0%%", nombreCampo);
        }

        if (noIgualCero) {
            // Porcentaje no puede ser igual a 0%.
            if (Numeric.igual(porcentaje, Numeric.CERO)) {
                return String.format("El valor de porcentaje de %s no puede ser igual a 0%%", nombreCampo);
            }
        }

        // Porcentaje no puede ser mayor a 100%.
        if (Numeric.mayor(porcentaje, Numeric.CIEN)) {
            return String.format("El valor de porcentaje de %s no puede ser superior a 100%%", nombreCampo);
        }

        if (noIgualCien) {
            // Porcentaje no puede ser igual a 100%.
            if (Numeric.igual(porcentaje, Numeric.CIEN)) {
                return String.format("El valor de porcentaje de %s no puede ser igual a 100%%", nombreCampo);
            }
        }

        return null;
    }

    /**
     * Valida los datos de entrada para una operacion de registro de nuevo
     * convenio.
     *
     * @param input Datos de entrada.
     * @return Mensaje de texto indicando el error de validacion encontrado, si
     * hubiere.
     */
    public String validar(AltaConvenioMB.Input input) {
        String tmp;

        // Validar proveedor.
        if (input.todosLosProveedores == false && input.proveedor == null) {
            return "Debe seleccionar un proveedor.";
        }

        // Validar comprador.
        if (input.todosLosCompradores == false && input.comprador == null) {
            return "Debe seleccionar un comprador.";
        }

        // Validar entidad financiera.
        if (input.entidadFinanciera == null) {
            return "Debe seleccionar una entidad financiera.";
        }

        // Validar tipo de intenres.
        if (input.tipoTasaComision == null) {
            return "Debe seleccionar un tipo de porcentaje de comisión";
        }

        // Validar porcentaje de descuento.
        tmp = validarPorcentaje(input.porcentajeImponibleComision, "descuento por comisión", true, true);
        if (tmp != null) {
            return tmp;
        }

        // Validar impuesto por porcentaje de descuento.
        tmp = validarPorcentaje(input.porcentajeImpuestoComision, "impuesto sobre comisión", false, true);
        if (tmp != null) {
            return tmp;
        }

        // Validar distribución de comisión para SEPSA.
        tmp = validarPorcentaje(input.porcentajeParteComisionSepsa, "comisión de SEPSA", false, false);
        if (tmp != null) {
            return tmp;
        }

        // Validar distribución de comision de comprador.
        tmp = validarPorcentaje(input.porcentajeParteComisionComprador, "comisión del Comprador", false, false);
        if (tmp != null) {
            return tmp;
        }

        // Validar distirbución de comision de comprador.
        tmp = validarPorcentaje(input.porcentajeParteComisionEntidadFinanciera, "comisión de Entidad Financiera", false, false);
        if (tmp != null) {
            return tmp;
        }

        // La distribución de comisión debe sumar 100%.
        BigDecimal sum = Numeric.sum(input.porcentajeParteComisionSepsa, input.porcentajeParteComisionComprador, input.porcentajeParteComisionEntidadFinanciera);
        if (Numeric.distinto(sum, Numeric.CIEN)) {
            return "Los porcentajes de distribución de comisión, en conjunto, deben sumar 100%.";
        }

        // Validar gastos operativos.
        tmp = validarPorcentaje(input.porcentajeImponibleGastosOperativos, "gastos administrativos", false, false);
        if (tmp != null) {
            return tmp;
        }

        // Validar gastos operativos.
        tmp = validarPorcentaje(input.porcentajeImpuestoGastosOperativos, "impuesto sobre gastos administrativos", false, true);
        if (tmp != null) {
            return tmp;
        }

        // Verificar que no existan convenios previos.
        Integer idEntidadFinanciera = input.entidadFinanciera.getId();
        Integer idComprador = input.todosLosCompradores ? -1 : input.comprador.getId();
        Integer idProveedor = input.todosLosProveedores ? -1 : input.proveedor.getId();
        List<Convenio> conveniosExistentes = facades.getConvenioFacade().find(idComprador, idProveedor, idEntidadFinanciera);
        if (!conveniosExistentes.isEmpty()) {
            return "Ya existe un convenio previo para esta configuración de participantes.";
        }

        return null;
    }

    /**
     * Persiste los datos cargados para un nuevo convenio.
     */
    public void guardarConvenio() {
        try {
            // Validar datos de entrada.
            String error = validar(input);
            if (error != null) {
                mensajes.addMessageError("Error", error);
                return;
            }

            // Persistir datos.
            Result<?> result = tx.execute(() -> guardarConvenio0());
            if (result.isFailure()) {
                log.error(result.failure().cause(), "Ocurrió un error guardando convenio");
                mensajes.showGlobalDialogMessage("Ocurrió un error registrando convenio. Por favor vuelva a intentar.");
            } else {
                mensajes.showGlobalDialogMessage(ConveniosMB::redireccionarListaConvenios, "Convenio registrado exitosamente");
            }
        } finally {
            updateViewNuevoConvenio();
        }
    }

    private Result<Void> guardarConvenio0() {
        Result<Void> result;

        try {
            // Crear convenio.
            Integer idEntidadFinanciera = input.entidadFinanciera.getId();
            Integer idComprador = input.todosLosCompradores ? -1 : input.comprador.getId();
            Integer idProveedor = input.todosLosProveedores ? -1 : input.proveedor.getId();
            Result<Convenio> resConvenio = beans.getConvenios().registrarConvenio(usuarioLogueado.getUsuario(), idComprador, idProveedor, idEntidadFinanciera, TipoConvenioEnum.NORMAL);
            resConvenio.raise();
            Convenio convenio = resConvenio.value();

            // Agregar configuracion de version de cargos.
            agregarCargo(convenio, Numeric.CERO, "2", TipoCargoEnum.VERSION_CARGOS, TipoValorEnum.TEXTO);

            // Agregar configuracion de porcentaje total imponible.
            agregarCargo(convenio, input.porcentajeImponibleComision, null, TipoCargoEnum.PORCENTAJE_TOTAL_IMPONIBLE_COMISION, TipoValorEnum.PORCENTAJE);

            // Agregar configuracion de porcentaje diario imponible.
            BigDecimal porcentajeDiarioImponibleComision = Numeric.div(input.porcentajeImponibleComision, input.tipoTasaComision.factor());
            agregarCargo(convenio, porcentajeDiarioImponibleComision, null, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION, TipoValorEnum.PORCENTAJE);

            // Agregar configuracion de porcentaje de impuesto sobre comision.
            agregarCargo(convenio, input.porcentajeImpuestoComision, null, TipoCargoEnum.PORCENTAJE_IMPUESTO_COMISION, TipoValorEnum.PORCENTAJE);

            // Agregar configuracion de comision para sepsa.
            BigDecimal porcentajeDiarioImponibleComisionSepsa = Numeric.div(Numeric.mul(porcentajeDiarioImponibleComision, input.porcentajeParteComisionSepsa), Numeric.CIEN);
            agregarCargo(convenio, input.porcentajeParteComisionSepsa, null, TipoCargoEnum.PORCENTAJE_TOTAL_PARTE_COMISION_SEPSA, TipoValorEnum.PORCENTAJE);
            agregarCargo(convenio, porcentajeDiarioImponibleComisionSepsa, null, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_SEPSA, TipoValorEnum.PORCENTAJE);

            // Agregar configuracion de comision para comprador.
            BigDecimal porcentajeDiarioImponibleComisionComprador = Numeric.div(Numeric.mul(porcentajeDiarioImponibleComision, input.porcentajeParteComisionComprador), Numeric.CIEN);
            agregarCargo(convenio, input.porcentajeParteComisionComprador, null, TipoCargoEnum.PORCENTAJE_TOTAL_PARTE_COMISION_COMPRADOR, TipoValorEnum.PORCENTAJE);
            agregarCargo(convenio, porcentajeDiarioImponibleComisionComprador, null, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_COMPRADOR, TipoValorEnum.PORCENTAJE);

            // Agregar configuracion de comision para entidad financiera.
            BigDecimal porcentajeDiarioImponibleComisionEntidadFinanciera = Numeric.div(Numeric.mul(porcentajeDiarioImponibleComision, input.porcentajeParteComisionEntidadFinanciera), Numeric.CIEN);
            agregarCargo(convenio, input.porcentajeParteComisionEntidadFinanciera, null, TipoCargoEnum.PORCENTAJE_TOTAL_PARTE_COMISION_ENTIDAD_FINANCIERA, TipoValorEnum.PORCENTAJE);
            agregarCargo(convenio, porcentajeDiarioImponibleComisionEntidadFinanciera, null, TipoCargoEnum.PORCENTAJE_IMPONIBLE_COMISION_ENTIDAD_FINANCIERA, TipoValorEnum.PORCENTAJE);

            // Agregar configuracion de gastos operativos.
            agregarCargo(convenio, input.porcentajeImponibleGastosOperativos, null, TipoCargoEnum.PORCENTAJE_IMPONIBLE_GASTOS_OPERATIVOS, TipoValorEnum.PORCENTAJE);
            agregarCargo(convenio, input.porcentajeImpuestoGastosOperativos, null, TipoCargoEnum.PORCENTAJE_IMPUESTO_GASTOS_OPERATIVOS, TipoValorEnum.PORCENTAJE);

            // Convenio creado exitosamente.
            result = Results.ok()
                    .nullable(true)
                    .value(null)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error registrando convenio. Por favor vuelva a intentar.")
                    .build();
        }

        return result;
    }

    /**
     * Registra una configuracion de cargo y emite una excepcion en caso de que
     * se genere algun error durante el registro.
     *
     * @param convenio Convenio al que se asocia la configuracion de cargo.
     * @param valor Valor numerico para el cargo.
     * @param valorTexto Valor textual para el cargo.
     * @param tipoCargo Tipo de cargo.
     * @param tipoValor Tipo de valor para el cargo.
     * @throws Throwable Error producido.
     */
    private void agregarCargo(Convenio convenio, BigDecimal valor, String valorTexto, TipoCargoEnum tipoCargo, TipoValorEnum tipoValor) throws Throwable {
        Result<ConfigCargo> resCargo = beans.getCargos().registrarConfigCargo(convenio.getId(), valor, valorTexto, tipoCargo, tipoValor);
        resCargo.raise();
    }

    /**
     * Actualiza el formulario principal.
     */
    private void updateViewNuevoConvenio() {
        Prime.update("nuevo_convenio");
    }

    @Data
    public static class Input implements Serializable {

        private TipoTasasComision.TipoTasaComisionEnum tipoTasaComision;

        private BigDecimal porcentajeImponibleComision;

        private BigDecimal porcentajeImpuestoComision;

        private BigDecimal porcentajeParteComisionSepsa;

        private BigDecimal porcentajeParteComisionComprador;

        private BigDecimal porcentajeParteComisionEntidadFinanciera;

        private BigDecimal porcentajeImponibleGastosOperativos;

        private BigDecimal porcentajeImpuestoGastosOperativos;

        /**
         * Proveedor seleccionado para formar parte de un convenio.
         */
        private Persona proveedor;

        /**
         * Flag que permite aplicar el convenio a todos los proveedores
         * existentes.
         */
        private Boolean todosLosProveedores;

        /**
         * Comprador seleccionado para formar parte de un convenio.
         */
        private Persona comprador;

        /**
         * Flag que permite aplicar el convenio a todos los proveedores
         * existentes.
         */
        private Boolean todosLosCompradores;

        /**
         * Entidad financiera seleccionada para formar parte de un convenio.
         */
        private Persona entidadFinanciera;
        //</editor-fold>

        public Input() {
            comprador = null;
            proveedor = null;
            entidadFinanciera = null;
            tipoTasaComision = null;
            porcentajeImponibleComision = Numeric.CERO;
            porcentajeImpuestoComision = Numeric.DIEZ;
            porcentajeImponibleGastosOperativos = Numeric.CERO;
            porcentajeImpuestoGastosOperativos = Numeric.CERO;
            porcentajeParteComisionSepsa = Numeric.CERO;
            porcentajeParteComisionEntidadFinanciera = Numeric.CERO;
            porcentajeParteComisionComprador = Numeric.CERO;
        }

    }

}
