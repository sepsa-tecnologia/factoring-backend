/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.varios;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.entities.factoring.PreguntaFrecuente;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "faq")
@ViewScoped
public class PreguntasFrecuentesMB extends BasicViewMB implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    private List<PreguntaFrecuente> preguntas;
    // </editor-fold>

    /**
     * Inicializa el managed bean.
     */
    @PostConstruct
    public void init() {
        preguntas = facades.getPreguntasFrecuentesFacade().findAll();
    }

    // <editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public List<PreguntaFrecuente> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<PreguntaFrecuente> preguntas) {
        this.preguntas = preguntas;
    }

    // </editor-fold>
}
