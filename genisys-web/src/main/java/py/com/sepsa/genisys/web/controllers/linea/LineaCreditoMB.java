/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.linea;

import fa.gs.misc.Assertions;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.util.Ajax;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.factoring.Archivo;
import py.com.sepsa.genisys.ejb.entities.factoring.LineaCredito;
import py.com.sepsa.genisys.ejb.entities.factoring.LineaCreditoHistorico;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.enums.TipoArchivoEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Archivos;
import py.com.sepsa.genisys.ejb.logic.impl.LineasCredito;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.archivo.CacheArchivos;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "lineas")
@ViewScoped
public class LineaCreditoMB extends BasicViewMB implements Serializable {

    @Inject
    private Personas personas;

    @Inject
    private TxWrapperWeb tx;

    @Inject
    private py.com.sepsa.genisys.web.controllers.linea.ListaMB lista;

    /**
     * Coleccion de entradas de historico para una linea seleccionada en la
     * vista.
     */
    private Collection<LineaCreditoHistorico> historicoLineaCredito;

    /**
     * Coleccion de clientes disponibles (compradores y/o proveedores) que
     * tienen convenio con una entidad financiera dada para asignar una linea de
     * credito.
     */
    private List<Persona> clientes;

    //<editor-fold defaultstate="collapsed" desc="Registro Lineas de Credito">
    /**
     * Bean encargado de abstraer el registro de lineas de credito.
     */
    @Inject
    private NuevaLineaCreditoMB registroLinea;

    /**
     * Datos de entrada para el registro de una nueva linea de credito.
     */
    private NuevaLineaCreditoMB.Input registroInput;
    //</editor-fold>

    /**
     * Lista de archivos adjuntos para mostrar en vista principal de lineas de
     * credito.
     */
    private List<Archivo> archivosAdjuntos;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        clientes = new LinkedList<>();
    }

    //<editor-fold defaultstate="collapsed" desc="Registro Linea Credito">
    /**
     * Inicializa la vista para el registro de una nueva linea de credito.
     */
    public void initRegistrarLineaCredito() {
        // Inicializar datos.
        clientes = obtenerClientesDisponibles();
        registroInput = new NuevaLineaCreditoMB.Input();

        // Actualizar vista.
        Prime.showSepsaDialog("linea_credito_nuevo_dialog");
        updateViewLineaCreditoNuevoDialog();
    }

    /**
     * Permite realizar la operacion de registro de una linea de credito tomando
     * los valores ingresados en la vista.
     */
    public void registrarLineaCredito() {
        tx.execute(this::registrarLineaCredito0);
    }

    private Result<Void> registrarLineaCredito0() {
        Result<Void> result;

        // Validar datos de entrada.
        String err = registroLinea.validar(registroInput);
        if (err != null) {
            mensajes.addMessageError("Error", err);
            return Results.ko().message(err).build();
        }

        try {
            // Registrar linea de credito.
            Result<LineaCredito> resRegistroLinea = registroLinea.registrar(registroInput);
            resRegistroLinea.raise();

            // Adjuntar archivos.
            LineaCredito linea = resRegistroLinea.value();
            adjuntarArchivosALineaDeCredito(linea, registroInput.getCacheArchivos());

            // Actualizar vista.
            mensajes.showGlobalDialogMessage(LineaCreditoMB.this::recargarListaLineas, "¡Línea de Crédito registrada exitosamente!");
            Prime.closeSepsaDialog("linea_credito_nuevo_dialog");

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            log.error(thr, "Ocurrió un error registrando linea de credito");
            mensajes.showGlobalDialogMessage("Ocurrió un error inesperado durante el registro de la línea de crédito. Por favor vuelva a intentar.");
            result = Results.ko().cause(thr).message("Ocurrió un error registrando linea de credito").build();
        } finally {
            updateViewLineaCreditoNuevoDialog();
        }

        return result;
    }
    //</editor-fold>

    /**
     * Asocia una coleccion de archivos a una linea de credito.
     *
     * @param linea Linea de credito.
     * @param cache Coleccion de archivos en cache.
     * @throws Throwable Error.
     */
    private void adjuntarArchivosALineaDeCredito(LineaCredito linea, CacheArchivos cache) throws Throwable {
        for (Archivos.CacheArchivo cacheArchivo : cache.getCache()) {
            try {
                Result<Archivo> resArchivo = beans.getArchivos().adjuntarArchivo(linea.getId(), Archivos.TipoEntidadArchivoAdjuntoEnum.LINEA_CREADITO, usuarioLogueado.getUsuario(), usuarioLogueado.getPerfil(), cacheArchivo, TipoArchivoEnum.GENERICO);
                resArchivo.raise();
            } catch (Throwable thr) {
                mensajes.addMessageWarning("Advertencia", String.format("El archivo seleccionado '%s' no pudo ser cargado.", cacheArchivo.getNombre()));
                throw thr;
            }
        }
    }

    /**
     * Actualiza la vista para desplegar los archivos adjuntos asociados a una
     * linea de credito.
     *
     * @param idLineaCredito Identificador de linea de credito.
     */
    public void verArchivosAdjuntos(Integer idLineaCredito) {
        // Obetener la lista de archivos adjuntos.
        Result<Collection<Archivo>> resArchivosAdjuntos = beans.getArchivos().buscarArchivosAdjuntos(idLineaCredito, Archivos.TipoEntidadArchivoAdjuntoEnum.LINEA_CREADITO);
        if (!resArchivosAdjuntos.isSuccess()) {
            return;
        }

        // Inicializar coleccion de objetos a utilizar en la vista.
        archivosAdjuntos = new LinkedList<>();
        archivosAdjuntos.addAll(resArchivosAdjuntos.value());

        if (archivosAdjuntos.isEmpty()) {
            mensajes.showGlobalDialogMessage("La línea de crédito seleccionada no posee archivos adjuntos");
        } else {
            // Ordenar entradas en base a la fecha de subida.
            Collections.sort(archivosAdjuntos, (Archivo a, Archivo b) -> {
                Date A = a.getFecha();
                Date B = b.getFecha();
                return A.compareTo(B);
            });

            // Habilitar cuadro de dialogo para visualizacion de archivos.
            Prime.showSepsaDialog("archivos_adjuntos_dialog");
        }

        //Actualizar vista.
        updateViewArchivosAdjuntosDialog();
    }

    /**
     * Inicializa la vista para una operacion de incremento de linea de credito.
     */
    public void initIncrementoLineaCredito() {
        LineaCredito linea = checkSeleccionUnica(lista, "Debe seleccionar una línea de crédito de la lista", "Debe seleccionar una sola línea de crédito de la lista");
        if (linea != null) {
            // TODO: IMPLEMENTAR.
        }
    }

    /**
     * Obtiene la lista de proveedores y compradores que poseen convenio con el
     * usuario logueado. Se espera que este usuario posea el perfil de entidad
     * financiera.
     *
     * @return Lista de clientes ordenados alfabeticamente por razon social.
     */
    private List<Persona> obtenerClientesDisponibles() {
        try {
            // Obtener personas que tienen convenio con esta entidad financiera.
            List<Persona> personas = getCompradoresProveedores();

            // Ordenar alfabeticamente por razon social.
            Collections.sort(personas, (Persona a, Persona b) -> {
                String A = utils.getRazonSocial(a);
                String B = utils.getRazonSocial(b);
                return A.compareTo(B);
            });

            return personas;
        } catch (Throwable thr) {
            thr.printStackTrace(System.err);
            return Collections.emptyList();
        }
    }

    /**
     * Retorna la lista de compradores y proveedores que tienen convenio con el
     * usuario logueado. Este metodo es relevante para los usuarios con rol de
     * entidad financiera.
     *
     * @return Lista de compradores y proveedores conveniados.
     */
    private List<Persona> getCompradoresProveedores() {
        // Obtener personas que tienen convenio con esta entidad financiera.
        Integer idEntidadFinanciera = usuarioLogueado.getPersona().getId();

        // Obtener compradores disponibles.
        Result<Collection<Persona>> resCompradores = personas.obtenerCompradoresDisponibles();
        Collection<Persona> compradores = resCompradores.value(null);

        // Obtener proveedores disponibles.
        Result<Collection<Persona>> resProveedores = personas.obtenerCompradoresDisponibles();
        Collection<Persona> proveedores = resProveedores.value(null);

        // Unificar lista de personas.
        List<Persona> personas = new LinkedList<>();
        if (Assertions.isNullOrEmpty(compradores)) {
            personas.addAll(compradores);
        }
        if (Assertions.isNullOrEmpty(proveedores)) {
            personas.addAll(proveedores);
        }

        // Eliminar registros repetidos.
        personas.stream().distinct().collect(Collectors.toList());
        return personas;
    }

    /**
     * Inicializa la vista para desplegar la lista de movimientos historicos, si
     * hubieren, de una linea de credito.
     *
     * @param idLineaCredito Identificador de linea de credito.
     */
    public void verHistoricoLinea(Integer idLineaCredito) {
        Result<Collection<LineaCreditoHistorico>> result = beans.getLineasCredito().buscarHistoricoLineaCredito(idLineaCredito);
        if (!result.isSuccess()) {
            mensajes.showGlobalDialogMessage("La línea de crédito seleccionada no presenta movimientos históricos");
        } else {
            historicoLineaCredito = result.value();
            if (historicoLineaCredito.isEmpty()) {
                mensajes.showGlobalDialogMessage("La línea de crédito seleccionada no presenta movimientos históricos");
            } else {
                Prime.showSepsaDialog("linea_credito_historico_dialog");
                updateViewLineaCreditoHistoricoDialog();
            }
        }
    }

    /**
     * Aplica los filtros sobre la lista de facturas.
     */
    public void filtrarLista() {
        lista.filter();
        updateViewFiltrosForm();
    }

    /**
     * Limpia los valores establecidos en los campos de filtro.
     */
    public void limpiarFiltroForm() {
        CustomFilter filtro = lista.getFiltro();
        if (!filtro.isEnabled()) {
            filtro.clearFilterValues();
            updateViewFiltrosForm();
        }
    }

    /**
     * Actualiza la lista de facturas en la vista.
     */
    public void recargarListaLineas() {
        lista.clearSelections();
        updateViewListaDeLineas();
        lista.reload();
        updateViewListaDeLineas();
    }

    //<editor-fold defaultstate="collapsed" desc="Actualizaciones de Vista">
    private void updateViewFiltrosForm() {
        Ajax.update("filtro_lineas_panel:filtro_lineas_form");
        Prime.showBlockUi("bui", lista.getFiltro().isEnabled());
        updateViewListaDeLineas();
    }

    private void updateViewListaDeLineas() {
        lista.clearSelections();
        Ajax.update("tabla_lineas_panel:tabla_lineas_form:tabla_lineas");
        Prime.executeJs("$.fn.matchHeight._update();");
    }

    private void updateViewLineaCreditoHistoricoDialog() {
        Ajax.update("linea_credito_historico_dialog:linea_credito_historico_form");
    }

    private void updateViewLineaCreditoNuevoDialog() {
        Ajax.update("linea_credito_nuevo_dialog:linea_credito_nuevo_form");
    }

    private void updateViewArchivosAdjuntosDialog() {
        Prime.update("archivos_adjuntos_dialog:archivos_adjuntos:archivos_adjuntos_form");
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public String getBtnFiltrarText() {
        if (lista.getFiltro().isEnabled()) {
            return "Deshacer Filtro";
        } else {
            return "Filtrar Datos";
        }
    }

    public String getPanelListaClass() {
        if (usuarioLogueado.esEntidadFinanciera()) {
            return "col-md-10";
        } else {
            return "col-md-12";
        }
    }

    public String getHistoricoLineaLabel(LineaCreditoHistorico lineaCreditoHistorico) {
        LineasCredito.TipoOperacionEnum operacion = LineasCredito.TipoOperacionEnum.from(lineaCreditoHistorico.getOperacion());
        if (operacion != null) {
            String type;
            switch (operacion) {
                case CREACION:
                    type = "label-primary";
                    break;
                case DEBITO:
                    type = "label-danger";
                    break;
                case CREDITO:
                    type = "label-success";
                    break;
                default:
                    type = "label-default";
            }

            String html = "\n"
                    + " <span class=\"label " + type + " app_text_truncated\" style=\"position: absolute; min-width: 80px !important;\">\n"
                    + "     " + operacion.descripcion() + "\n"
                    + " </span>";
            return html;
        } else {
            return "N/D";
        }
    }

    public ListaMB getLista() {
        return lista;
    }

    public void setLista(ListaMB lista) {
        this.lista = lista;
    }

    public Collection<LineaCreditoHistorico> getHistoricoLineaCredito() {
        return historicoLineaCredito;
    }

    public void setHistoricoLineaCredito(Collection<LineaCreditoHistorico> historicoLineaCredito) {
        this.historicoLineaCredito = historicoLineaCredito;
    }

    public List<Persona> getClientes() {
        return clientes;
    }

    public void setClientes(List<Persona> clientes) {
        this.clientes = clientes;
    }

    public NuevaLineaCreditoMB.Input getRegistroInput() {
        return registroInput;
    }

    public void setRegistroInput(NuevaLineaCreditoMB.Input registroInput) {
        this.registroInput = registroInput;
    }

    public List<Archivo> getArchivosAdjuntos() {
        return archivosAdjuntos;
    }

    public void setArchivosAdjuntos(List<Archivo> archivosAdjuntos) {
        this.archivosAdjuntos = archivosAdjuntos;
    }
    //</editor-fold>

    public String tmpRazonSocialEntidad(LineaCredito lc) {
        Persona persona = personas.obtenerPorId(lc.getIdEntidadFinanciera()).value(null);
        if (persona != null) {
            return Text.razonSocial(persona);
        } else {
            return Text.nd();
        }
    }

    public String tmpRucEntidad(LineaCredito lc) {
        Persona persona = personas.obtenerPorId(lc.getIdEntidadFinanciera()).value(null);
        if (persona != null) {
            return Text.ruc(persona);
        } else {
            return Text.nd();
        }
    }

    public String tmpRazonSocialCliente(LineaCredito lc) {
        Persona persona = personas.obtenerPorId(lc.getIdPersona()).value(null);
        if (persona != null) {
            return Text.razonSocial(persona);
        } else {
            return Text.nd();
        }
    }

    public String tmpRucCliente(LineaCredito lc) {
        Persona persona = personas.obtenerPorId(lc.getIdPersona()).value(null);
        if (persona != null) {
            return Text.ruc(persona);
        } else {
            return Text.nd();
        }
    }

}
