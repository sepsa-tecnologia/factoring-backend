/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.comprador;

import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Ajax;
import py.com.sepsa.genisys.ejb.entities.info.PersonaInfo;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "compradores")
@ViewScoped
public class CompradorMB extends BasicViewMB implements Serializable {

    @Inject
    @Getter
    @Setter
    private py.com.sepsa.genisys.web.controllers.comprador.ListaMB lista;

    @PostConstruct
    public void init() {
        ;
    }

    public void initAltaComprador() {
        Prime.redirect("app/abm/comprador/alta.xhtml?redirect-true");
    }

    public void initCambiarPersoneria() {
        PersonaInfo persona = checkSeleccionUnica(lista);
        if (persona != null) {
            String id = Ids.mask(persona.getId());
            String path = Strings.format("app/abm/comprador/cambiar_personeria.xhtml?p=%s", id);
            Prime.redirect(path);
        }
    }

    public void initVerContacto() {
        PersonaInfo persona = checkSeleccionUnica(lista);
        if (persona != null) {
            String id = Ids.mask(persona.getId());
            String path = Strings.format("app/abm/comprador/ver_contacto.xhtml?p=%s", id);
            Prime.redirect(path);
        }
    }

    public void filtrarLista() {
        lista.filter();
        updateViewFiltrosForm();
    }

    public void limpiarFiltroForm() {
        CustomFilter filtro = lista.getFiltro();
        if (!filtro.isEnabled()) {
            filtro.clearFilterValues();
            updateViewFiltrosForm();
        }
    }

    public void recargarListaCompradores() {
        lista.clearSelections();
        updateViewListaDeCompradores();
        lista.reload();
        updateViewListaDeCompradores();
    }

    public String htmlPersoneria(PersonaInfo persona) {
        return persona.getTipoPersona().descripcion();
    }

    private void updateViewFiltrosForm() {
        Ajax.update("filtro_compradores_panel:filtro_compradores_form");
        Prime.showBlockUi("bui", lista.getFiltro().isEnabled());
        updateViewListaDeCompradores();
    }

    private void updateViewListaDeCompradores() {
        lista.clearSelections();
        Ajax.update("tabla_compradores_panel:tabla_compradores_form:tabla_compradores");
        Prime.executeJs("$.fn.matchHeight._update();");
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public String getBtnFiltrarText() {
        if (lista.getFiltro().isEnabled()) {
            return "Deshacer Filtro";
        } else {
            return "Filtrar Datos";
        }
    }
    //</editor-fold>

}
