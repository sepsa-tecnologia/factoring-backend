/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.utils;

import fa.gs.utils.misc.numeric.Currency;
import java.math.BigDecimal;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Fabio A. González Sosa
 */
@FacesConverter(forClass = BigDecimal.class, value = "py.com.sepsa.genisys.web.utils.BigDecimalConverter")
public class BigDecimalConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }

        BigDecimal number = Currency.parseCurrency(value);
        return number;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }

        return Currency.formatCurrency((BigDecimal) value);
    }

}
