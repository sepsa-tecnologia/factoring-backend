/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import org.primefaces.component.datatable.DataTable;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.CustomLazyDataModel;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public abstract class ListaMB<T> extends BasicViewMB implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Lista de facturas cargadas dinamicamente desde la BD.
     */
    private CustomLazyDataModel<T> items;

    /**
     * Filtros aplicables a la lista de facturas.
     */
    private CustomFilter filtro;

    /**
     * Lista de facturas seleccionadas desde una tabla con seleccion múltiple
     * habilitada.
     */
    private List<T> multiSelectedItem;

    /**
     * Elemento seleccionado desde una tabla con seleccion simple habilitada.
     */
    private T singleSelectedItem;

    /**
     * Objeto que representa el componente de tabla de facturas en la vista.
     */
    private DataTable tableBind;
    // </editor-fold>

    @PostConstruct
    public void init() {
        items = buildItemsLoader();
        filtro = buildItemsFilter();
        filtro.disable();
        clearSelections();
    }

    /**
     * Metodo que permite a las subclases instanciar el modelo lazy que desean
     * utilizar.
     *
     * @return Modelo lazy.
     */
    protected abstract CustomLazyDataModel<T> buildItemsLoader();

    /**
     * Metodo que permite a las subclases instanciar el objeto contenedor de los
     * valores de filtro de datos.
     *
     * @return Filtro de datos.
     */
    protected abstract CustomFilter buildItemsFilter();

    /**
     * Indica si se han seleccionado elementos de la lista de elementos
     * desplegados.
     *
     * @return True si exsiten elementos seleccionados. False caso contrario.
     */
    public boolean hasSelection() {
        return (singleSelectedItem != null) || (!multiSelectedItem.isEmpty());
    }

    /**
     * Realiza un filtro de los datos desplegados o cancela un filtrado
     * anterior.
     */
    public void filter() {
        if (filtro.isEnabled()) {
            // Deshacer filtro.
            clearFiltering();
            clearSelections();
        } else {
            Map<String, Object> filter = filtro.getFilters();
            if (!filter.isEmpty()) {
                // Filtrar datos.
                filtro.enable();
                tableBind.setFilters(filter);
                clearSelections();
            }
        }
        // Actualizacion de la lista.
        tableBind.loadLazyData();
    }

    /**
     * Realiza una carga completa de los elementos en la lista y vuelve a la
     * primera pagina de elementos. Los datos presentados estarán afectados por
     * cualquier ordenación o filtro activo.
     */
    public void reload() {
        tableBind.setFirst(0);
        tableBind.loadLazyData();
    }

    /**
     * Limpia la seleccion de elementos, si hubiere.
     */
    public void clearSelections() {
        singleSelectedItem = null;
        multiSelectedItem = new LinkedList<>();
    }

    /**
     * Cancela el filtrado de datos.
     */
    private void clearFiltering() {
        filtro.disable();
        tableBind.setFilters(new HashMap<>());
    }

    // <editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public CustomLazyDataModel<T> getItems() {
        return items;
    }

    public void setItems(CustomLazyDataModel<T> items) {
        this.items = items;
    }

    public CustomFilter getFiltro() {
        return filtro;
    }

    public void setFiltro(CustomFilter filtro) {
        this.filtro = filtro;
    }

    public List<T> getMultiSelectedItem() {
        return multiSelectedItem;
    }

    public void setMultiSelectedItem(List<T> multiSelectedItem) {
        this.multiSelectedItem = multiSelectedItem;
    }

    public T getSingleSelectedItem() {
        return singleSelectedItem;
    }

    public void setSingleSelectedItem(T singleSelecteditem) {
        this.singleSelectedItem = singleSelecteditem;
    }

    public DataTable getTableBind() {
        return tableBind;
    }

    public void setTableBind(DataTable tableBind) {
        this.tableBind = tableBind;
    }

    // </editor-fold>
}
