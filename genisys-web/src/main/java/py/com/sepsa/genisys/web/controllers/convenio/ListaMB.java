/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.convenio;

import fa.gs.criteria.query.Order;
import fa.gs.utils.database.query.expressions.literals.DateLiteral;
import fa.gs.utils.database.query.expressions.literals.Literal;
import fa.gs.utils.misc.fechas.Fechas;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.Dependent;
import py.com.sepsa.genisys.ejb.entities.factoring.ConvenioInfo;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Convenios;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.CustomLazyDataModel;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class ListaMB extends py.com.sepsa.genisys.web.controllers.ListaMB<ConvenioInfo> implements Serializable {

    @Override
    protected CustomLazyDataModel<ConvenioInfo> buildItemsLoader() {
        return new ConveniosDefaultLazyList();
    }

    @Override
    protected CustomFilter buildItemsFilter() {
        return new ConvenioListFilter();
    }

    /**
     * Clase que encapsula la carga progresiva de cargos desde la BD.
     */
    private class ConveniosDefaultLazyList extends CustomLazyDataModel<ConvenioInfo> {

        private Convenios convenios;

        /**
         * Constructor.
         *
         * @param facade Facade de facturas.
         */
        public ConveniosDefaultLazyList() {
            super(null);
            this.convenios = Injection.lookup(Convenios.class);
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<ConvenioInfo> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            // Busqueda de registros.
            Integer count = convenios.countConvenios(filter);
            List<ConvenioInfo> data = convenios.selectConvenios(limit, offset, filter, null);

            setRowCount(count);
            return data;
        }

    }

    /**
     * Clase que encapsula los campos y operadores de filtro aplicables a la
     * lista principal de facturas.
     */
    public class ConvenioListFilter extends CustomFilter {

        // <editor-fold defaultstate="collapsed" desc="Atributos">
        private Persona comprador;

        private Persona proveedor;

        private Persona entidadFinanciera;

        private Date fechaAltaDesde;

        private Date fechaAltaHasta;

        // </editor-fold>
        /**
         * Constructor.
         */
        public ConvenioListFilter() {
            clearFilterValues();
        }

        /**
         * Limpia los valores de filtro posiblemente establecidos previamente.
         */
        @Override
        final public void clearFilterValues() {
            comprador = null;
            proveedor = null;
            entidadFinanciera = null;
            fechaAltaDesde = null;
            fechaAltaHasta = null;
        }

        /**
         * Obtiene los valores de filtro correspondientes.
         *
         * @return Filtros organizados en pares {nombre filtro, valor}.
         */
        @Override
        public Map<String, Object> getFilters() {
            Map<String, Object> filters = new HashMap<>();

            // Filtro de comprador.
            if (comprador != null) {
                filters.put("idComprador", String.format("p0.id = %s", comprador.getId()));
            }

            // Filtro de proveedor.
            if (proveedor != null) {
                filters.put("idProveedor", String.format("p1.id = %s", proveedor.getId()));
            }

            // Filtro de entidad financiera.
            if (entidadFinanciera != null) {
                filters.put("idEntidadFinanciera", String.format("p2.id = %s", entidadFinanciera.getId()));
            }

            // Filtro fecha registro desde.
            if (fechaAltaDesde != null) {
                Date fecha = Fechas.inicioDia(fechaAltaDesde);
                Literal literal = new DateLiteral(fecha, DateLiteral.DateType.FECHA_HORA);
                filters.put("fecha$geq", String.format("c.fecha >= %s", literal.stringify(null)));
            }

            // Filtro fecha registro hasta.
            if (fechaAltaHasta != null) {
                Date fecha = Fechas.finDia(fechaAltaHasta);
                Literal literal = new DateLiteral(fecha, DateLiteral.DateType.FECHA_HORA);
                filters.put("fecha$leq", String.format("c.fecha <= %s", literal.stringify(null)));
            }

            return filters;
        }

        // <editor-fold defaultstate="collapsed" desc="Getters y Setters">
        public Persona getComprador() {
            return comprador;
        }

        public void setComprador(Persona comprador) {
            this.comprador = comprador;
        }

        public Persona getProveedor() {
            return proveedor;
        }

        public void setProveedor(Persona proveedor) {
            this.proveedor = proveedor;
        }

        public Persona getEntidadFinanciera() {
            return entidadFinanciera;
        }

        public void setEntidadFinanciera(Persona entidadFinanciera) {
            this.entidadFinanciera = entidadFinanciera;
        }

        public Date getFechaAltaDesde() {
            return fechaAltaDesde;
        }

        public void setFechaAltaDesde(Date fechaAltaDesde) {
            this.fechaAltaDesde = fechaAltaDesde;
        }

        public Date getFechaAltaHasta() {
            return fechaAltaHasta;
        }

        public void setFechaAltaHasta(Date fechaAltaHasta) {
            this.fechaAltaHasta = fechaAltaHasta;
        }
        // </editor-fold>
    }
}
