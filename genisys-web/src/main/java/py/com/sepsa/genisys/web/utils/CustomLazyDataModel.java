/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.utils;

import fa.gs.criteria.facades.AbstractFacade;
import fa.gs.criteria.query.Order;
import fa.gs.criteria.query.QueryCriteria;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;
import py.com.sepsa.genisys.ejb.utils.Criterias;

/**
 * Clase base que implementa las funcionalidades básicas de un modelo de datos
 * para los componentes de tabla de Primefaces.
 *
 * @author Fabio A. González Sosa
 */
public class CustomLazyDataModel<T> extends LazyDataModel<T> {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    protected final AbstractFacade<T> facade;
    private List<T> currentData;
    // </editor-fold>

    /**
     * Constructor.
     *
     * @param facade Facade para realizar operaciones sobre una entidad.
     */
    public CustomLazyDataModel(AbstractFacade<T> facade) {
        this.currentData = new LinkedList<>();
        this.facade = facade;
    }

    /**
     * Realiza una carga lazy de datos desde la BD.
     *
     * @param offset cantidad de registros a omitor.
     * @param limit cantidad máxima de registros a obtener.
     * @param sorts criterios de ordenacion de datos.
     * @param filters criterios de filtrado de datos.
     * @return Lista de entidades recuperadas desde BD.
     */
    protected List<T> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filters) {
        QueryCriteria criteria = Criterias.instance(limit, offset, sorts, filters);
        List<T> data = facade.find(criteria);
        setRowCount(facade.count(criteria));
        return data;
    }

    /**
     * Implementacion del metodo abstracto para utilizar el metodo lazyLoad.
     */
    @Override
    public List<T> load(int offset, int limit, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        // Se convierten los criterios de ordenacion de primefaces a uno propio.
        Map<String, Order> sort = new HashMap<>();
        if (sortField != null && !sortField.isEmpty()) {
            sort.put(sortField, convertSortOrder(sortOrder));
        }
        // Obtenemos la lista de datos desde la BD.
        currentData = lazyLoad(offset, limit, sort, filters);
        setWrappedData(currentData);
        return currentData;
    }

    /**
     * Implementacion del metodo abstracto para utilizar el metodo lazyLoad.
     */
    @Override
    public List<T> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        // Se convierten los criterios de ordenacion de primefaces a uno propio.
        Map<String, Order> sort = new HashMap<>();
        if (multiSortMeta != null) {
            multiSortMeta.stream().forEach((sortMeta) -> {
                sort.put(sortMeta.getSortField(), convertSortOrder(sortMeta.getSortOrder()));
            });
        }
        // Obtenemos la lista de datos desde la BD.
        currentData = lazyLoad(first, pageSize, sort, filters);
        setWrappedData(currentData);
        return currentData;
    }

    /**
     * Dado un identificador, obtiene el objeto que representa a una fila en la
     * tabla.
     *
     * @param rowKey Identificador de la fila.
     * @return Objeto que representa a una fila dentro de la tabla.
     */
    @Override
    public T getRowData(String rowKey) {
        try {
            Integer id = Integer.valueOf(rowKey);
            for (T data : currentData) {
                if (id.equals((Integer) getRowKey(data))) {
                    return data;
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Obtiene el identificador, idealmente unico, de una fila en la tabla.
     *
     * @param object Objeto que representa una fila dentro de la tabla.
     * @return Identificador de la fila.
     */
    @Override
    public Object getRowKey(T object) {
        Class<?> klass = object.getClass();
        try {
            Method getter = klass.getMethod("getId");
            getter.setAccessible(true);
            return getter.invoke(object);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Convierte un objeto SortOrder de primefaces a un tipo enum.
     *
     * @param order Objeto SortOrder generado por primefaces.
     * @return Enumeracion que representa una ordenacion ascendente o
     * descendente.
     */
    private static Order convertSortOrder(SortOrder order) {
        switch (order) {
            case DESCENDING:
                // Orden descendente.
                return Order.DESCENDING;
            default:
                // Orden ascendente.
                return Order.ASCENDING;
        }
    }

}
