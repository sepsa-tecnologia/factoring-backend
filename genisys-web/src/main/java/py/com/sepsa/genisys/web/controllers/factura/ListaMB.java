/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.factura;

import fa.gs.criteria.query.Order;
import fa.gs.misc.fechas.Fechas;
import fa.gs.result.simple.Result;
import fa.gs.utils.database.query.expressions.literals.CollectionLiteral;
import fa.gs.utils.database.query.expressions.literals.SQLStringLiterals;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.Dependent;
import lombok.Data;
import py.com.sepsa.genisys.ejb.entities.factoring.Convenio;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.web.utils.CustomFilter;
import py.com.sepsa.genisys.web.utils.CustomLazyDataModel;

/**
 *
 * @author Fabio A. González Sosa
 */
@Dependent
public class ListaMB extends py.com.sepsa.genisys.web.controllers.ListaMB<FacturaInfo> implements Serializable {

    @Override
    protected CustomLazyDataModel<FacturaInfo> buildItemsLoader() {
        if (usuarioLogueado.esComprador()) {
            return new FacturasDeCompradorLazyList();
        } else if (usuarioLogueado.esProveedor()) {
            return new FacturasDeProveedorLazyList();
        } else if (usuarioLogueado.esEntidadFinanciera()) {
            return new FacturasdeEntidadLazyList();
        } else {
            return new FacturasDefaultLazyList();
        }
    }

    @Override
    protected CustomFilter buildItemsFilter() {
        return new FacturaListFilter();
    }

    /**
     * Clase que encapsula la carga progresiva de facturas desde la BD.
     */
    private class FacturasDefaultLazyList extends CustomLazyDataModel<FacturaInfo> {

        Facturas facturas;

        /**
         * Constructor.
         */
        public FacturasDefaultLazyList() {
            super(null);
            this.facturas = Injection.lookup(Facturas.class);
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<FacturaInfo> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            // Omitir o incluir facturas expiradas.
            if (filter.containsKey("mostrarExpiradas")) {
                filter.remove("fechaPago$geq");
            } else if (!filter.containsKey("fechaPago$geq")) {
                Date fecha = Fechas.inicioDia(new Date());
                filter.put("fechaPago$geq", Strings.format(" f.fecha_pago >= %s ", SQLStringLiterals.fechaHora(fecha)));
            }

            // Ordenar por fechas.
            if (sorts.isEmpty()) {
                // Si no se hace este control, los registros siempre estarán ordenados por fecha y fechaPago de manera descendente.
                sorts.put("f.fecha", Order.DESCENDING);
                sorts.put("f.fecha_pago", Order.DESCENDING);
            }

            // Busqueda de registros.
            List<FacturaInfo> data = facturas.selectFacturas(limit, offset, filter, sorts);
            Integer total = facturas.countFacturas(filter);

            setRowCount(total);
            return data;
        }

    }

    /**
     * Clase que encapsula la carga progresiva de facturas a mostrar a los
     * compradores.
     */
    private class FacturasDeCompradorLazyList extends CustomLazyDataModel<FacturaInfo> {

        Facturas facturas;

        /**
         * Constructor.
         */
        public FacturasDeCompradorLazyList() {
            super(null);
            this.facturas = Injection.lookup(Facturas.class);
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<FacturaInfo> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            // Mostrar facturas donde la persona logueada figure como destino de las facturas.
            {
                Integer idPersona = usuarioLogueado.getPersona().getId();
                filter.put("idPersonaDestino", Strings.format(" f.id_persona_destino = %s ", idPersona));
            }

            // Mostrar facturas en estados validos para el comprador.
            {
                String literal = CollectionLiteral.instance(estados.getEnabledEstadosFactura()).stringify(null);
                filter.put("idEstado$in", Strings.format(" e.id in %s ", literal));
            }

            // Omitir o incluir facturas expiradas.
            if (filter.containsKey("mostrarExpiradas")) {
                filter.remove("fechaPago$geq");
            } else if (!filter.containsKey("fechaPago$geq")) {
                Date fecha = Fechas.inicioDia(new Date());
                filter.put("fechaPago$geq", Strings.format(" f.fecha_pago >= %s ", SQLStringLiterals.fechaHora(fecha)));
            }

            // Ordenar por fechas.
            if (sorts.isEmpty()) {
                // Si no se hace este control, los registros siempre estarán ordenados por fecha y fechaPago de manera descendente.
                sorts.put("f.fecha", Order.DESCENDING);
                sorts.put("f.fecha_pago", Order.DESCENDING);
            }

            // Busqueda de registros.
            List<FacturaInfo> data = facturas.selectFacturas(limit, offset, filter, sorts);
            Integer total = facturas.countFacturas(filter);

            setRowCount(total);
            return data;
        }

    }

    /**
     * Clase que encapsula la carga progresiva de facturas a mostrar a los
     * proveedores.
     */
    private class FacturasDeProveedorLazyList extends CustomLazyDataModel<FacturaInfo> {

        Facturas facturas;

        /**
         * Constructor.
         */
        public FacturasDeProveedorLazyList() {
            super(null);
            this.facturas = Injection.lookup(Facturas.class);
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<FacturaInfo> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            // Mostrar facturas donde la persona logueada figure como origen de facturas.
            {
                Integer idPersona = usuarioLogueado.getPersona().getId();
                filter.put("idPersonaOrigen", Strings.format(" f.id_persona_origen = %s ", idPersona));
            }

            // Mostrar facturas en estados validos para el proveedor.
            {
                String literal = CollectionLiteral.instance(estados.getEnabledEstadosFactura()).stringify(null);
                filter.put("idEstado$in", Strings.format(" e.id in %s ", literal));
            }

            // Omitir o incluir facturas expiradas.
            if (filter.containsKey("mostrarExpiradas")) {
                filter.remove("fechaPago$geq");
            } else if (!filter.containsKey("fechaPago$geq")) {
                Date fecha = Fechas.inicioDia(new Date());
                filter.put("fechaPago$geq", Strings.format(" f.fecha_pago >= %s ", SQLStringLiterals.fechaHora(fecha)));
            }

            // Ordenar por fechas.
            if (sorts.isEmpty()) {
                // Si no se hace este control, los registros siempre estarán ordenados por fecha y fechaPago de manera descendente.
                sorts.put("f.fecha", Order.DESCENDING);
                sorts.put("f.fecha_pago", Order.DESCENDING);
            }

            // Busqueda de registros.
            List<FacturaInfo> data = facturas.selectFacturas(limit, offset, filter, sorts);
            Integer total = facturas.countFacturas(filter);

            setRowCount(total);
            return data;
        }

    }

    /**
     * Clase que encapsula la carga progresiva de facturas a mostrar a las
     * entidades financieras.
     */
    private class FacturasdeEntidadLazyList extends CustomLazyDataModel<FacturaInfo> {

        Facturas facturas;

        /**
         * Constructor.
         */
        public FacturasdeEntidadLazyList() {
            super(null);
            this.facturas = Injection.lookup(Facturas.class);
        }

        /**
         * Realiza una carga lazy de datos desde la BD.
         *
         * @param offset cantidad de registros a omitor.
         * @param limit cantidad máxima de registros a obtener.
         * @param sorts criterios de ordenacion de datos.
         * @param filter criterios de filtrado de datos.
         * @return Lista de entidades recuperadas desde BD.
         */
        @Override
        public List<FacturaInfo> lazyLoad(int offset, int limit, Map<String, Order> sorts, Map<String, Object> filter) {
            // Mostrar facturas que ya han sido conformadas y vendidas bajo convenio con esta entidad.
            {
                Integer[] idConvenios = idConvenios();
                if (!(idConvenios == null || idConvenios.length == 0)) {
                    String literal = CollectionLiteral.instance(idConvenios).stringify(null);
                    filter.put("facturaConformada.idConvenio$in", Strings.format(" c.id in %s ", literal));
                }
            }

            // Mostrar facturas en estado s validos para entidad financiera.
            {
                String literal = CollectionLiteral.instance(estados.getEnabledEstadosFactura()).stringify(null);
                filter.put("idEstado$in", Strings.format(" e.id in %s ", literal));
            }

            // Omitir o incluir facturas expiradas.
            if (filter.containsKey("mostrarExpiradas")) {
                filter.remove("fechaPago$geq");
            } else if (!filter.containsKey("fechaPago$geq")) {
                Date fecha = Fechas.inicioDia(new Date());
                filter.put("fechaPago$geq", Strings.format(" f.fecha_pago >= %s ", SQLStringLiterals.fechaHora(fecha)));
            }

            // Ordenar por fechas.
            if (sorts.isEmpty()) {
                // Si no se hace este control, los registros siempre estarán ordenados por fecha y fechaPago de manera descendente.
                sorts.put("f.fecha", Order.DESCENDING);
                sorts.put("f.fecha_pago", Order.DESCENDING);
            }

            // Busqueda de registros.
            List<FacturaInfo> data = facturas.selectFacturas(limit, offset, filter, sorts);
            Integer total = facturas.countFacturas(filter);

            setRowCount(total);
            return data;
        }

        private Integer[] idConvenios() {
            Integer idPersona = usuarioLogueado.getPersona().getId();
            Result<Collection<Convenio>> resConvenios = beans.getConvenios().obtenerConveniosEntidadFinanciera(idPersona);
            return resConvenios.value(new LinkedList<>()).stream().map(c -> c.getId()).toArray(Integer[]::new);
        }

    }

    /**
     * Clase que encapsula los campos y operadores de filtro aplicables a la
     * lista principal de facturas.
     */
    @Data
    public class FacturaListFilter extends CustomFilter {

        /**
         * Campo de filtro para el dentificador de factura.
         */
        private Integer idFactura;

        /**
         * Campo de filtro para el número de una factura.
         */
        private String nroFactura;

        /**
         * Campo de filtro para el estado de una factura.
         */
        private Integer idEstado;

        /**
         * Campo de filtro para el monto total de una factura.
         */
        private BigDecimal montoTotal;

        /**
         * Campo de filtro para los dias de credito de una factura.
         */
        private Integer diasCredito;

        /**
         * Campo de filtro para la razon social de la persona comprador de la
         * factura.
         */
        private String razonSocialComprador;

        /**
         * Campo de filtro para el RUC de la persona comprador de la factura.
         */
        private String rucComprador;

        /**
         * Campo de filtro para la razon social de la persona proveedor de la
         * factura.
         */
        private String razonSocialProveedor;

        /**
         * Campo de filtro para el RUC de la persona proveedor de la factura.
         */
        private String rucProveedor;

        /**
         * Campo de filtro para la fecha minima de la factura.
         */
        private Date fechaDesde;

        /**
         * Campo de filtro para la fecha maxima de la factura.
         */
        private Date fechaHasta;

        /**
         * Campo de filtro para la fecha minima de pago de la factura.
         */
        private Date fechaPagoDesde;

        /**
         * Campo de filtro para la fecha maxima de pago de la factura.
         */
        private Date fechaPagoHasta;

        /**
         * Campo de filtro para incluir o excluir facturas ya expiradas.
         */
        private Boolean mostrarExpiradas;

        /**
         * Constructor.
         */
        public FacturaListFilter() {
            clearFilterValues();
        }

        /**
         * Limpia los valores de filtro posiblemente establecidos previamente.
         */
        @Override
        final public void clearFilterValues() {
            idFactura = null;
            nroFactura = "";
            idEstado = null;
            montoTotal = null;
            diasCredito = null;
            razonSocialComprador = "";
            rucComprador = "";
            razonSocialProveedor = "";
            rucProveedor = "";
            fechaDesde = null;
            fechaHasta = null;
            fechaPagoDesde = null;
            fechaPagoHasta = null;
            mostrarExpiradas = false;
        }

        /**
         * Obtiene los valores de filtro correspondientes.
         *
         * @return Filtros organizados en pares {nombre filtro, valor}.
         */
        @Override
        public Map<String, Object> getFilters() {
            Map<String, Object> filters = new HashMap<>();

            // Filtro de id factura.
            if (idFactura != null) {
                filters.put("id", Strings.format(" f.id = %s ", idFactura));
            }

            // Filtro de estado.
            if (idEstado != null) {
                filters.put("idEstado.id", Strings.format(" e.id = %s ", idEstado));
            }

            // Filtro de monto total.
            if (montoTotal != null) {
                filters.put("montoTotalFactura", Strings.format(" f.monto_total_factura = %s ", montoTotal));
            }

            // Filtro de monto total.
            if (diasCredito != null) {
                filters.put("diasCredito", Strings.format(" f.dias_credito = %s ", diasCredito));
            }

            // Filtro fecha desde.
            if (fechaDesde != null) {
                Date fecha = Fechas.inicioDia(fechaDesde);
                filters.put("fecha$geq", Strings.format(" f.fecha >= %s ", SQLStringLiterals.fechaHora(fecha)));
            }

            // Filtro fecha hasta.
            if (fechaHasta != null) {
                Date fecha = Fechas.finDia(fechaHasta);
                filters.put("fecha$leq", Strings.format(" f.fecha <= %s ", SQLStringLiterals.fechaHora(fecha)));
            }

            // Filtro fecha pago desde.
            if (fechaPagoDesde != null) {
                Date fecha = Fechas.inicioDia(fechaPagoDesde);
                filters.put("fechaPago$geq", Strings.format(" f.fecha_pago >= %s ", SQLStringLiterals.fechaHora(fecha)));
            }

            // Filtro fecha pago hasta.
            if (fechaPagoHasta != null) {
                Date fecha = Fechas.finDia(fechaPagoHasta);
                filters.put("fechaPago$leq", Strings.format(" f.fecha_pago <= %s ", SQLStringLiterals.fechaHora(fecha)));
            }

            // Filtro de mostrar expiradas.
            if (mostrarExpiradas == true) {
                // Agregar bandera para incluir facturas expiradas.
                filters.put("mostrarExpiradas", " 1 = 1");
            }

            // Filtro de razon social de Comprador.
            if (!Assertions.stringNullOrEmpty(razonSocialComprador)) {
                String literal = SQLStringLiterals.phrasify(razonSocialComprador);
                filters.put("razonSocialComprador", Strings.format(" f.razon_social_comprador ILIKE '%s' ", literal));
            }

            // Filtro de RUC de Comprador.
            if (!Assertions.stringNullOrEmpty(rucComprador)) {
                String literal = SQLStringLiterals.phrasify(rucComprador);
                filters.put("rucComprador", Strings.format(" f.ruc_comprador ILIKE '%s' ", literal));
            }

            // Filtro de razon social de Proveedor.
            if (!Assertions.stringNullOrEmpty(razonSocialProveedor)) {
                String literal = SQLStringLiterals.phrasify(razonSocialProveedor);
                filters.put("razonSocialProveedor", Strings.format(" f.razon_social_proveedor ILIKE '%s' ", literal));
            }

            // Filtro de RUC de Proveedor.
            if (!Assertions.stringNullOrEmpty(rucProveedor)) {
                String literal = SQLStringLiterals.phrasify(rucProveedor);
                filters.put("rucProveedor", Strings.format(" f.ruc_proveedor ILIKE '%s' ", literal));
            }

            // Filtro de Número de Factura.
            if (!Assertions.stringNullOrEmpty(nroFactura)) {
                String literal = SQLStringLiterals.phrasify(nroFactura);
                filters.put("nroFactura", Strings.format(" f.ruc_proveedor ILIKE '%s' ", literal));
            }

            return filters;
        }

    }

}
