/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.sesion;

import fa.gs.utils.authentication.tokens.TokenExtractor;
import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.misc.errors.Errors;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import py.com.sepsa.genisys.ejb.enums.TipoPersonalizacionEnum;
import py.com.sepsa.genisys.ejb.security.UserAuthenticationInfo;
import py.com.sepsa.genisys.web.controllers.varios.PersonalizacionesMB;
import py.com.sepsa.genisys.web.utils.BackingBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@SessionScoped
@Named("bSesion")
public class SesionMB extends BackingBean {

    @Inject
    private PersonalizacionesMB personalizaciones;

    @PostConstruct
    public void init() {
        log.debug("[SESION INICIADA]");
    }

    @PreDestroy
    public void destroy() {
        log.debug("[SESION FINALIZADA]");
    }

    public UserAuthenticationInfo getCurrentUserInfo() {
        // Contexto de sesion de spring.
        SecurityContext ctx = SecurityContextHolder.getContext();
        if (ctx == null) {
            throw Errors.illegalState("Se debe iniciar sesion");
        }

        UserAuthenticationInfo auth = (UserAuthenticationInfo) ctx.getAuthentication().getDetails();
        return auth;
    }

    public void onLogout() {
        try {
            // Invalidar sesion de spring.
            boolean ok1 = invalidateSpringSession();
            log.debug().message("Sesión JSF invalidada: [%s]", ok1 ? "OK" : "KO").log();

            // Invalidar sesion de jsf.
            boolean ok2 = invalidateJsfSession();
            log.debug().message("Sesión Spring invalidada: [%s]", ok2 ? "OK" : "KO").log();

            // Eliminar cookies de sesion.
            boolean ok3 = invalidateCookieSession();
            log.debug().message("Sesión cookie invalidada: [%s]", ok3 ? "OK" : "KO").log();

            // Redireccion a pagina de cierre de sesion.
            redirect();
        } catch (Throwable thr) {
            log.error()
                    .cause(thr)
                    .message("Ocurrió un error terminando sesion")
                    .log();
        }
    }

    private boolean invalidateJsfSession() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            return true;
        } catch (Throwable thr) {
            log.error()
                    .cause(thr)
                    .message("Ocurrió un error invalidando sesion")
                    .tag("session.context", "jsf")
                    .log();
            return false;
        }
    }

    private boolean invalidateSpringSession() {
        try {
            SecurityContextHolder.getContext().setAuthentication(null);
            SecurityContextHolder.clearContext();
            return true;
        } catch (Throwable thr) {
            log.error()
                    .cause(thr)
                    .message("Ocurrió un error invalidando sesión")
                    .tag("session.context", "spring")
                    .log();
            return false;
        }
    }

    private boolean invalidateCookieSession() {
        try {
            TokenExtractor.COOKIE_NAME = "APP_SESSION_TOKEN_GENISYS";
            Jsf.removeCookie(TokenExtractor.COOKIE_NAME, "/");
            return true;
        } catch (Throwable thr) {
            log.error()
                    .cause(thr)
                    .message("Ocurrió un error invalidando sesión")
                    .tag("session.context", "jwt")
                    .log();
            return false;
        }
    }

    private void redirect() {
        // Obtener url personalizada, si hubiere.
        String url = personalizaciones.getUrlLogout().getValor();
        if (Objects.equals(url, TipoPersonalizacionEnum.URL_LOGOUT.fallback())) {
            // Redireccionar a vista de login por defecto.
            Jsf.redirect("/index.xhtml?faces-redirect=true");
        } else {
            // Redireccionar a vista personalizada.
            try {
                ExternalContext context = Jsf.getExternalContext();
                context.redirect(url);
            } catch (Throwable thr) {
                Errors.dump(System.err, thr);
                Jsf.redirect("/index.xhtml?faces-redirect=true");
            }
        }
    }

}
