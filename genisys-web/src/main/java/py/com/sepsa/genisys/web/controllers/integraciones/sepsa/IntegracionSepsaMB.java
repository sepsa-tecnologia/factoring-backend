/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.integraciones.sepsa;

import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.misc.Files;
import java.io.File;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.sepsa.genisys.ejb.logic.impl.Sepsa;
import py.com.sepsa.genisys.ejb.logic.pojos.UsuarioInfo;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named("integracionSepsa")
@ViewScoped
public class IntegracionSepsaMB extends BasicViewMB implements Serializable {

    @Inject
    private Sepsa sepsa;

    public void descargarListaUsuarios() {
        try {
            Collection<UsuarioInfo> infos = sepsa.obtenerUsuariosActivos();
            File file = Files.createTemporary();
            try (PrintWriter writer = Files.getWriter(file, StandardCharsets.UTF_8)) {
                writer.append("SEP=;\n");
                writer.append("razon_social;ruc;dv_ruc;usuario;perfiles\n");
                for (UsuarioInfo info : infos) {
                    writer.append(String.format("%s;%s;%s;%s;%s\n", info.getRazonSocial(), info.getRuc(), info.getDvRuc(), info.getUsuario(), info.getTxtPerfilAgg()));
                }
                writer.flush();
            }
            Jsf.sendFile(file, "usuarios.csv", (int) file.length(), "text/csv");
        } catch (Throwable thr) {
            log.error(thr, "No se pudo obtener la lista de usuarios activos.");
            mensajes.showGlobalDialogMessage("No se pudo obtener la lista de usuarios activos.");
        }
    }

}
