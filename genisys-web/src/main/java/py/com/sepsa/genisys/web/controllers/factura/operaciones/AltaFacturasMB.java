/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.factura.operaciones;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.misc.text.Strings;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Data;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaConformada;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Archivos.CacheArchivo;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.utils.Excel;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.archivo.CacheArchivos;
import py.com.sepsa.genisys.web.controllers.varios.PersonasMB;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "altaFacturas")
@ViewScoped
public class AltaFacturasMB extends BasicViewMB {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    @Inject
    private TxWrapperWeb tx;

    @EJB
    private Notificaciones notificaciones;

    @EJB
    private Facturas facturas;

    @Inject
    private PersonasMB personas;

    /**
     * Contiene los datos de entrada para el alta masiva de facturas.
     */
    private AltaFacturasMB.Input input;

    /**
     * Referencia a tipo de moneda GUARANI.
     */
    private Moneda guarani;

    /**
     * Informe final del proceso inicial de carga/validacion de datos.
     */
    private Informe informe;

    /**
     * Flag que indica si el cuadro de confirmacion debe ser mostrado en
     * pantalla.
     */
    private Boolean showConfirmDialog;
    //</editor-fold>

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        input = new Input();
        guarani = beans.getFacades().getMonedaFacade().getGuarani();
        informe = null;
        showConfirmDialog = false;
    }

    /**
     * Permite descargar el archivo de muestra que define los campos necesarios
     * para el alta masiva de facturas.
     */
    public void descargarTemplate() {
        File file = null;
        try {
            InputStream stream = AltaFacturasMB.class.getResourceAsStream("/py/com/sepsa/genisys/core/templates/alta_masiva_facturas.xlsx");
            Prime.sendFile(stream, "template.xlsx", -1, "application/vnd.ms-excel");
        } catch (Throwable thr) {
            log.error(thr, "Ocurrió un error descargando archivo de muestra para carga masiva de facturas.");
        } finally {
            if (file != null) {
                file.delete();
            }
        }
    }

    /**
     * Permite descarha un archivo que contiene el detalle de cada procesamiento
     * realizado, por filas, al archivo subido.
     */
    public void descargarInforme() {
        try {
            StringBuilder builder = new StringBuilder();
            for (PreProceso preproceso : informe.preprocesos.values()) {
                String text0 = PreProcesoAdapter.adapt(preproceso);
                builder.append(text0);
                builder.append("\r\n");
            }

            byte[] bytes = builder.toString().getBytes(Charset.forName("UTF-8"));
            InputStream stream = new ByteArrayInputStream(bytes);
            Prime.sendFile(stream, "informe.txt", bytes.length, "text/plain");
        } catch (Throwable thr) {
            log.error(thr, "Ocurrió un error descargando archivo de muestra para carga masiva de facturas.");
        }
    }

    /**
     * Procesa un archivo excel para la carga de facturas.
     */
    public void procesar() {
        // Reiniciar estado de vista.
        showConfirmDialog = false;

        // Iniciar procesamiento.
        tx.execute(this::procesar0);
    }

    private Result<Void> procesar0() {
        Result<Void> result;

        try {
            // Deshacer resultados anteriores.
            informe = null;

            // Validar datos de entrada.
            String err = validar(input);
            if (err != null) {
                mensajes.addMessageError("Error", err);
            } else {
                // Procesar cada archivo subido.
                for (CacheArchivo archivo : input.archivos.getCache()) {
                    Result<File> resFile = beans.getArchivos().persistir(archivo.getContenido());
                    if (resFile.isSuccess()) {
                        File file = resFile.value();
                        try {
                            // Procesamiento especifico de archivo.
                            XSSFWorkbook wb = Excel.load(file);
                            if (hasMetaAttribute(wb) && hasSheetFacturas(wb)) {
                                // Procesar los datos presentados y continuar con el siguiente paso.
                                informe = procesar(wb);
                                Prime.update("alta_facturas_confirmar_dialog:form");
                                showConfirmDialog = true;
                            } else {
                                mensajes.addMessageWarning("Advertencia", "El archivo indicado no concuerda con el formato indicado en el archivo de muestra.");
                            }
                        } catch (Throwable thr) {
                            log.error(thr, "Ocurrió un error procesando archivo");
                            mensajes.addMessageWarning("Advertencia", String.format("El archivo '%s' no pudo ser procesado. Asegúrese que el mismo sea un archivo Microsoft Excel® válido.", archivo.getNombre()));
                        } finally {
                            file.delete();
                        }
                    } else {
                        log.error(resFile.failure().cause(), Strings.format("%s", resFile.failure().message()));
                        mensajes.addMessageError("Error", String.format("Ocurrió un error procesando el archivo '%s'. Por favor vuelta a intentar", archivo.getNombre()));
                    }
                }
            }

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            mensajes.addMessageError("Error", "Ocurrió un error procesando carga masiva de facturas");
            result = Results.ko().cause(thr).message("Ocurrió un error procesando carga masiva de facturas").build();
        } finally {
            // Descartar archivo subido porque al volver a dr "procesar" se genera una excepcion.
            input.archivos.limpiar();

            // Actualizar vista.
            updateViewForm();
        }

        return result;
    }

    /**
     * Realiza un proceso de validacio y verificacion de datos sobre un libro
     * excel.
     *
     * @param wb Libro excel.
     * @return Datos de preproceso.
     * @throws Throwable Si ocurre algun error.
     */
    private Informe procesar(XSSFWorkbook wb) throws Throwable {
        Informe informe = new Informe();

        // Procesar cada fila de la hoja "FACTURAS".
        XSSFSheet facturas = Excel.sheet(wb, "FACTURAS");
        int nrows = Excel.rowCount(facturas);
        for (int i = 4; i <= (nrows + 1); i++) {
            XSSFRow fila = Excel.row(facturas, i - 1);
            if (!Excel.isEmpty(fila)) {
                // Obtener los datos de factura y verificar que sean validos.
                Result<DatosFactura> result = obtenerDatosFactura(fila);
                DatosFactura datos = result.value(null);
                if (datos == null) {
                    continue;
                }

                // Validar datos.
                result = validarDatosFactura(informe, datos);

                // Determinar si fila contiene o no datos correctos.
                PreProceso preproceso;
                if (result.isSuccess()) {
                    preproceso = PreProceso.ok(i, datos);
                } else {
                    preproceso = PreProceso.ko(i, datos, result.failure().message());
                }

                informe.agregar(preproceso);
            }
        }

        return informe;
    }

    /**
     * Verifica si un archivo excel cuenta con la hoja oculta "META" cuya celda
     * incial contiene un valor especifico de referencia para identificar el
     * archivo como derivado del archivo de muestra.
     *
     * @param wb Libro excel.
     * @return {@code true} si el archivo excel esta basado en el template, caso
     * contrario {@code false}.
     */
    private boolean hasMetaAttribute(XSSFWorkbook wb) {
        try {
            XSSFSheet meta = Excel.sheet(wb, "META");
            String value = Excel.text(Excel.cell(meta, 0, 0), null);
            return Objects.equals(value, "SEPSA");
        } catch (Throwable thr) {
            return false;
        }
    }

    /**
     * Verifica si un archivo excel cuenta con la hoja "FACTURAS" cuyas filas
     * representan datos de facturas a registrar.
     *
     * @param wb Libro excel.
     * @return {@code true} si el archivo excel cuenta con la hoja "FACTURAS".
     * Caso contrario {@code false}.
     */
    private boolean hasSheetFacturas(XSSFWorkbook wb) {
        try {
            XSSFSheet meta = Excel.sheet(wb, "FACTURAS");
            return meta != null;
        } catch (Throwable thr) {
            return false;
        }
    }

    /**
     * Valida los datos de entrada presentados en la vista.
     *
     * @param input Datos de entrada.
     * @return Mensaje de error indicando el fallo de validacion, si hubiere.
     * Caso contrario {@code null}.
     */
    private String validar(AltaFacturasMB.Input input) {
        if (input.getArchivos().getCache().isEmpty()) {
            return "Debe subir al menos un archivo.";
        }

        return null;
    }

    /**
     * Procesa una fila para obtener datos de factura.
     *
     * @param fila Fila excel.
     * @return Resultado de la operacion.
     */
    private Result<DatosFactura> obtenerDatosFactura(XSSFRow fila) {
        Result<DatosFactura> result;

        if (Excel.isNull(fila)) {
            result = Results.ko().message("Datos inválidos.").build();
            return result;
        }

        try {
            int j = 1;
            DatosFactura datos = new DatosFactura();
            datos.numero = Excel.text(Excel.cell(fila, j++), null);
            datos.razonSocial = Excel.text(Excel.cell(fila, j++), null);
            datos.ruc = Excel.text(Excel.cell(fila, j++), null);
            datos.fechaEmision = Excel.date(Excel.cell(fila, j++), null);
            datos.fechaPago = Excel.date(Excel.cell(fila, j++), null);
            datos.montoTotalIva5 = Excel.number(Excel.cell(fila, j++), Numeric.CERO);
            datos.montoTotalIva10 = Excel.number(Excel.cell(fila, j++), Numeric.CERO);
            datos.montoTotalExentas = Excel.number(Excel.cell(fila, j++), Numeric.CERO);
            datos.montoTotal = Excel.number(Excel.cell(fila, j++), Numeric.CERO);
            datos.montoTotalNotasCredito = Excel.number(Excel.cell(fila, j++), Numeric.CERO);
            datos.montoTotalRetenciones = Excel.number(Excel.cell(fila, j++), Numeric.CERO);
            datos.montoTotalOtros = Excel.number(Excel.cell(fila, j++), Numeric.CERO);
            datos.montoTotalImponible = Excel.number(Excel.cell(fila, j++), Numeric.CERO);
            datos.conformar = Excel.bool(Excel.cell(fila, j++), Boolean.FALSE);
            result = Results.ok().value(datos).build();
        } catch (Throwable thr) {
            result = Results.ko().cause(thr).message("Ocurrió un error procesando datos de factura").build();
        }

        return result;
    }

    /**
     * Verifica que los datos de factura sean validos.
     *
     * @param datos Datos de factura.
     * @return {@code true} si los datos son validos. Caso contrario
     * {@code false}.
     */
    private Result<DatosFactura> validarDatosFactura(Informe informe, DatosFactura datos) {
        Result<DatosFactura> result;

        try {
            // Validar numero de documento.
            String err = AltaFacturaMB.validarNumeroDocumento(datos.numero, "número de comprobante");
            if (err != null) {
                throw new Exception(err);
            }

            // Validar ruc.
            err = AltaFacturaMB.validarRuc(datos.ruc, "ruc");
            if (err != null) {
                throw new Exception(err);
            }

            // Validar origenes de factura.
            Persona comprador = null;
            Persona proveedor = null;

            // Validar proveedor, si hubiere.
            if (usuarioLogueado.esComprador()) {
                comprador = usuarioLogueado.getPersona();
                proveedor = null;
                for (Persona proveedor0 : personas.getProveedores()) {
                    String ruc0 = Text.ruc(proveedor0);
                    if (Objects.equals(datos.ruc, ruc0)) {
                        proveedor = proveedor0;
                        break;
                    }
                }
                if (proveedor == null) {
                    throw new Exception("El campo 'ruc' no corresponde a ningún proveedor habilitado para este comprador.");
                }
            }

            // Validar comprador, si hubiere.
            if (usuarioLogueado.esProveedor()) {
                comprador = null;
                proveedor = usuarioLogueado.getPersona();
                for (Persona comprador0 : personas.getCompradores()) {
                    String ruc0 = Text.ruc(comprador0);
                    if (Objects.equals(datos.ruc, ruc0)) {
                        comprador = comprador0;
                        break;
                    }
                }
                if (comprador == null) {
                    throw new Exception("El campo 'ruc' no corresponde a ningún comprador habilitado para este proveedor.");
                }
            }

            // Validar que documento no se repetido dentro de la lista actual de facturas.
            for (Map.Entry<Integer, PreProceso> entry : informe.preprocesos.entrySet()) {
                String numero0 = entry.getValue().datos.numero;
                String ruc0 = entry.getValue().datos.ruc;
                String numero1 = datos.numero;
                String ruc1 = datos.ruc;
                if (Objects.equals(numero0, numero1) && Objects.equals(ruc0, ruc1)) {
                    throw new Exception("Los campos 'número de comprobante' y 'ruc' están duplicados.");
                }
            }

            // Validar fecha de emision.
            if (datos.fechaEmision == null) {
                throw new Exception("El campo 'fecha de emisión' de factura no puede ser vacío.");
            }

            // Validar fecha de pago.
            if (datos.fechaPago == null) {
                throw new Exception("El campo 'fecha de pago' de factura no puede ser vacío.");
            }

            // Validar relacion entre fecha de pago y fecha de emision.
            if (Fechas.diasDiferencia(datos.fechaEmision, datos.fechaPago) <= 0) {
                throw new Exception("El campo 'fecha de pago' de factura no puede contener un valor inferior al del campo 'fecha de emisión' de factura.");
            }

            // Validar fecha de pago vigente.
            if (Fechas.diasDiferencia(datos.fechaPago) < 0) {
                throw new Exception("El campo 'fecha de pago' de factura no puede ser menor a la fecha actual.");
            }

            // Validar monto total iva 5.
            if (Numeric.menor(datos.montoTotalIva5, Numeric.CERO)) {
                throw new Exception("El campo 'monto total iva 5' de factura no puede ser vacío o menor que cero (0).");
            }

            // Validar monto total iva 10.
            if (Numeric.menor(datos.montoTotalIva10, Numeric.CERO)) {
                throw new Exception("El campo 'monto total iva 10' de factura no puede ser vacío o menor que cero (0).");
            }

            // Validar monto total exentas
            if (Numeric.menor(datos.montoTotalExentas, Numeric.CERO)) {
                throw new Exception("El campo 'monto total exentas' de factura no puede ser vacío o menor que cero (0).");
            }

            // Validar monto total.
            if (Numeric.menorIgual(datos.montoTotal, Numeric.CERO)) {
                throw new Exception("El campo 'monto total' de factura no puede ser vacío, menor o igual que cero (0).");
            }

            // Validar monto notas de credito.
            if (Numeric.menor(datos.montoTotalNotasCredito, Numeric.CERO)) {
                throw new Exception("El campo 'monto total de notas de crédito' de factura no puede ser vacío o menor que cero (0).");
            }

            // Validar monto retenciones
            if (Numeric.menor(datos.montoTotalRetenciones, Numeric.CERO)) {
                throw new Exception("El campo 'monto total de retenciones' de factura no puede ser vacío o menor que cero (0).");
            }

            // Validar monto otros.
            if (Numeric.menor(datos.montoTotalOtros, Numeric.CERO)) {
                throw new Exception("El campo 'monto total otros' de factura no puede ser vacío o menor que cero (0).");
            }

            // Validar monto total imponible.
            if (Numeric.menorIgual(datos.montoTotalImponible, Numeric.CERO)) {
                throw new Exception("El campo 'monto total imponible' de factura no puede ser vacío, menor o igual que cero (0).");
            }

            // Validar integridad de datos para monto imponible.
            // Debe ser igual a la diferencia entre el monto total y la sumatoria de montos descontables (nota credito, retencion, otros).
            BigDecimal tmp0 = Numeric.sub(datos.montoTotal, Numeric.sum(datos.montoTotalNotasCredito, datos.montoTotalRetenciones, datos.montoTotalOtros));
            if (!Numeric.igual(datos.montoTotalImponible, tmp0)) {
                throw new Exception("El campo 'monto total imponible' de factura no coincide con los demás montos declarados.");
            }

            // Validar integridad de datos para monto imponible.
            // Debe ser menor o igual a monto total inicial de factura.
            if (Numeric.mayor(datos.montoTotalImponible, datos.montoTotal)) {
                throw new Exception("El campo 'monto total imponible' de factura no puede ser mayor que el 'monto total' inicial de factura.");
            }

            // Validar que documento ya no haya sido registrada anteriormente.
            boolean existe = facturas.facturaExiste(datos.numero, Text.ruc(proveedor), Text.ruc(comprador), datos.fechaEmision, datos.fechaPago, datos.montoTotal, guarani.getId());
            if (existe) {
                throw new Exception("Los datos de factura ya están registrados.");
            }

            /**
             * Validacion ok. Agregar datos adicionales si es necesario.
             */
            datos.proveedor = proveedor;
            datos.comprador = comprador;
            datos.moneda = guarani;

            result = Results.ok()
                    .value(datos)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message(thr.getMessage())
                    .build();
        }

        return result;
    }

    /**
     * Permite cancelar el registro final de datos de facturas previamente
     * cargados/validados.
     */
    public void cancelarRegistro() {
        Prime.closeSepsaDialog("alta_facturas_confirmar_dialog");
        updateViewForm();
    }

    /**
     * Permite continuar el registro final de datos de facturas previamente
     * cargados/validados.
     *
     * @throws Throwable Si ocurre algun error.
     */
    public void confirmarRegistro() throws Throwable {
        tx.execute(this::confirmarRegistro0);
    }

    private Result<Void> confirmarRegistro0() {
        Result<Void> result;

        try {
            // Coleccion para facturas que fueron conformadas.
            List<FacturaInfo> facturasConformadas = Lists.empty();

            // Consumir datos procesados desde archivos.
            for (PreProceso preproceso : informe.preprocesos.values()) {
                if (preproceso.status == PreProceso.Status.OK) {
                    Integer idUsuario = usuarioLogueado.getUsuario().getId();
                    Persona comprador = preproceso.datos.comprador;
                    Persona proveedor = preproceso.datos.proveedor;
                    String nroFactura = preproceso.datos.numero;
                    Date fechaEmision = preproceso.datos.fechaEmision;
                    Date fechaPago = preproceso.datos.fechaPago;
                    BigDecimal montoTotal = preproceso.datos.montoTotal;
                    BigDecimal montoNotasCredito = preproceso.datos.montoTotalNotasCredito;
                    BigDecimal montoRetenciones = preproceso.datos.montoTotalRetenciones;
                    BigDecimal montoOtros = preproceso.datos.montoTotalOtros;
                    BigDecimal montoTotalImponible = preproceso.datos.montoTotalImponible;

                    // Registrar factura.
                    Result<Factura> resRegistro = facturas.registrarFactura(idUsuario, comprador, proveedor, nroFactura, fechaEmision, fechaPago, montoTotal, guarani);
                    resRegistro.raise();
                    Factura factura = resRegistro.value();

                    // Registrar historico.
                    Result<HistoricoFactura> resHistoricoRegistro = facturas.agregarHistorico(idUsuario, factura, Estados.EstadoFacturaEnum.NO_CONFORMADA, "Factura registrada desde alta masiva.");
                    resHistoricoRegistro.raise();

                    // Conformar factura, si hubiere.
                    if (usuarioLogueado.esComprador() && preproceso.datos.conformar) {
                        // Conformar factura.
                        Result<FacturaConformada> resConformacion = facturas.registrarFacturaConformada(factura.getId(), montoTotal, montoNotasCredito, montoRetenciones, montoOtros, montoTotalImponible);
                        resConformacion.raise();

                        // Cambio de estado.
                        Result<Factura> resCambioEstado = facturas.cambiarEstado(idUsuario, factura.getId(), Estados.EstadoFacturaEnum.CONFORMADA);
                        resCambioEstado.raise();

                        // Registrar historico.
                        Result<HistoricoFactura> resHistoricoConformacion = facturas.agregarHistorico(idUsuario, factura, Estados.EstadoFacturaEnum.CONFORMADA, "Factura conformada desde alta masiva.");
                        resHistoricoConformacion.raise();

                        // Agregar factura a coleccion de facturas conformadas.
                        FacturaInfo factura0 = facturas.buscarFactura(factura.getId()).value(null);
                        if (factura0 != null) {
                            facturasConformadas.add(factura0);
                        }
                    }

                }
            }

            // Notificar conformacion de facturas.
            notificaciones.notificarConformacionDeFacturas(facturasConformadas);

            // Actualizar vista.
            Prime.closeSepsaDialog("alta_facturas_confirmar_dialog");
            mensajes.showGlobalDialogMessage("Facturas registradas exitosamente");

            result = Results.ok()
                    .nullable(true)
                    .value(null)
                    .build();
        } catch (Throwable thr) {
            mensajes.showGlobalDialogMessage("Ocurrió un error registrando las facturas. Por favor vuelta a intentar.");
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error registrando las facturas. Por favor vuelta a intentar.")
                    .build();
        } finally {
            ;
        }

        return result;
    }

    /**
     * Actualiza la vista.
     */
    private void updateViewForm() {
        Prime.update("alta_facturas_form");
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
    }

    public Informe getInforme() {
        return informe;
    }

    public void setInforme(Informe informe) {
        this.informe = informe;
    }

    public Boolean getShowConfirmDialog() {
        return showConfirmDialog;
    }

    public void setShowConfirmDialog(Boolean showConfirmDialog) {
        this.showConfirmDialog = showConfirmDialog;
    }
    //</editor-fold>

    /**
     * Clase que sirve como contenedor de todos los datos involucrados durante
     * la carga masiva de facturas.
     */
    public static class Input {

        /**
         * Archivos subidos.
         */
        private CacheArchivos archivos;

        /**
         * Constructor.
         */
        private Input() {
            archivos = new CacheArchivos();
        }

        public CacheArchivos getArchivos() {
            return archivos;
        }

        public void setArchivos(CacheArchivos archivos) {
            this.archivos = archivos;
        }

    }

    /**
     * Clase que contiene los datos que son obtenidos directamente de cada fila
     * de un archivo excel.
     */
    @Data
    private static class DatosFactura implements Serializable {

        private String numero;
        private String razonSocial;
        private String ruc;
        private Date fechaEmision;
        private Date fechaPago;
        private BigDecimal montoTotalIva5;
        private BigDecimal montoTotalIva10;
        private BigDecimal montoTotalExentas;
        private BigDecimal montoTotal;
        private BigDecimal montoTotalNotasCredito;
        private BigDecimal montoTotalRetenciones;
        private BigDecimal montoTotalOtros;
        private BigDecimal montoTotalImponible;
        private Boolean conformar;
        private Persona comprador;
        private Persona proveedor;
        private Moneda moneda;

    }

    /**
     * Clase que sirve para contener los datos de procesamiento aplicados sobre
     * una fila excel.
     */
    public static class PreProceso {

        /**
         * Numero de fila procesada.
         */
        private Integer fila;

        /**
         * Datos de factura obtenidos.
         */
        private DatosFactura datos;

        /**
         * Mensaje que describe un error producido durante el procesamiento
         * realizado.
         */
        private String error;

        /**
         * Indica el estado final del procesamiento realizado.
         */
        private PreProceso.Status status;

        /**
         * Constructor.
         *
         * @param fila Numero de fila.
         */
        private PreProceso(Integer fila) {
            this.fila = fila;
            this.datos = null;
            this.error = null;
            this.status = PreProceso.Status.KO;
        }

        public static PreProceso ok(Integer fila, DatosFactura datos) {
            PreProceso preproceso = new PreProceso(fila);
            preproceso.status = PreProceso.Status.OK;
            preproceso.datos = datos;
            preproceso.error = null;
            return preproceso;
        }

        public static PreProceso ko(Integer fila, DatosFactura datos, String error) {
            PreProceso preproceso = new PreProceso(fila);
            preproceso.status = PreProceso.Status.KO;
            preproceso.datos = datos;
            preproceso.error = error;
            return preproceso;
        }

        static enum Status {
            OK,
            KO
        }

    }

    /**
     * Clase que permite adaptar una instancia de
     * {@link py.com.sepsa.genisys.web.controllers.factura.operaciones.AltaFacturasMB.PreProceso PreProceso}
     * a una representacion de texto.
     */
    private static class PreProcesoAdapter {

        /**
         * Adapta una instancia de
         * {@link py.com.sepsa.genisys.web.controllers.factura.operaciones.AltaFacturasMB.PreProceso PreProceso}
         * a una representacion de texto.
         *
         * @param builder Constructor de cadenas de texto.
         * @param preproceso Resultado de preproceso.
         */
        public static String adapt(PreProceso preproceso) {
            StringBuilder builder = new StringBuilder();
            setFila(preproceso, builder);
            setStatus(preproceso, builder);
            setError(preproceso, builder);
            return builder.toString().trim();
        }

        /**
         * Agrega un par de elementos [clave, valor] a una cadena en
         * construccion.
         *
         * @param builder Constructor de cadenas de texto.
         * @param name Clave.
         * @param text Valor.
         */
        private static void set(StringBuilder builder, String name, String text) {
            builder.append(" ").append(name).append("=\"").append(text).append("\"");
        }

        /**
         * Agrega el valor de numero de fila.
         *
         * @param preproceso Resultado de preproceso.
         * @param builder Constructor de cadenas de texto.
         */
        private static void setFila(PreProceso preproceso, StringBuilder builder) {
            set(builder, "fila", String.format("%04d", preproceso.fila));
        }

        /**
         * Agrega el valor de estado.
         *
         * @param preproceso Resultado de preproceso.
         * @param builder Constructor de cadenas de texto.
         */
        private static void setStatus(PreProceso preproceso, StringBuilder builder) {
            String texto;
            switch (preproceso.status) {
                case OK:
                    texto = "VÁLIDO";
                    break;
                case KO:
                    texto = "INVÁLIDO";
                    break;
                default:
                    texto = "DESCARTADO";
            }
            set(builder, "estado", texto);
        }

        /**
         * Agrega el valor de mensaje de error durante preproceso.
         *
         * @param preproceso Resultado de preproceso.
         * @param builder Constructor de cadenas de texto.
         */
        private static void setError(PreProceso preproceso, StringBuilder builder) {
            String texto = "";
            if (preproceso.error != null && !preproceso.error.isEmpty()) {
                texto = preproceso.error;
            }
            set(builder, "error", texto);
        }

    }

    /**
     * Clase que contiene una coleccion de resultados de preprocesamiento.
     */
    public static class Informe {

        //<editor-fold defaultstate="collapsed" desc="Atributos">
        /**
         * Coleccion de resultados de preproceso agrupados por numero de fila.
         */
        private final Map<Integer, PreProceso> preprocesos;

        /**
         * Cantidad de resultados de preprocesamiento con estado
         * {@link py.com.sepsa.genisys.web.controllers.factura.operaciones.AltaFacturasMB.PreProceso.Status#OK OK}.
         */
        private Integer totalOk;

        /**
         * Cantidad de resultados de preprocesamiento con estado
         * {@link py.com.sepsa.genisys.web.controllers.factura.operaciones.AltaFacturasMB.PreProceso.Status#KO KO}.
         */
        private Integer totalKo;
        //</editor-fold>

        /**
         * Constructor.
         */
        public Informe() {
            this.preprocesos = new TreeMap<>();
            this.totalOk = 0;
            this.totalKo = 0;
        }

        /**
         * Agrea un resultado de preprocesamiento a la coleccion.
         *
         * @param preproceso Resultado de preprocesamiento.
         */
        void agregar(PreProceso preproceso) {
            preprocesos.put(preproceso.fila, preproceso);
            switch (preproceso.status) {
                case OK:
                    totalOk++;
                    break;
                case KO:
                    totalKo++;
                    break;
            }
        }

        //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
        public Integer getTotalOk() {
            return totalOk;
        }

        public void setTotalOk(Integer totalOk) {
            this.totalOk = totalOk;
        }

        public Integer getTotalKo() {
            return totalKo;
        }

        public void setTotalKo(Integer totalKo) {
            this.totalKo = totalKo;
        }
        //</editor-fold>

    }

}
