/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.usuarios;

import fa.gs.result.simple.Result;
import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.misc.Assertions;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ValidationException;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.logic.impl.Facades;
import py.com.sepsa.genisys.ejb.logic.impl.Usuarios;
import py.com.sepsa.genisys.ejb.utils.Ids;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "usuarioChangepass")
@ViewScoped
public class UsuarioCambiarContrasenaMB extends BasicViewMB implements Serializable {

    @Inject
    private Usuarios usuarios;

    @Inject
    private Facades facades;

    @Inject
    private TxWrapperWeb tx;

    private Usuario usuario;

    @Getter
    @Setter
    private FormInput input;

    @PostConstruct
    public void init() {
        // Obtener datos de usuario.
        Integer idUsuario = Ids.unmask(Jsf.getRequestParameter("u"));
        usuario = usuarios.obtenerPorId(idUsuario).value();

        // Inicializar formulario.
        input = new FormInput();
        input.contrasena = "";
    }

    public void guardar() {
        Result<Void> result = tx.execute(this::guardar0);
        if (result.isFailure()) {
            // Error.
            String msg = result.failure().cause().getMessage();
            mensajes.showGlobalDialogMessage(msg);
        } else {
            // Limpiar campos de entrada.
            mensajes.showGlobalDialogMessage(() -> Prime.redirect("/app/abm/usuarios/lista.xhtml"), "Datos guardados exitosamente");
        }

        updateForm();
    }

    private Void guardar0() throws Throwable {
        // Validar datos de entrada.
        String err = validarInput(input);
        if (err != null) {
            throw new ValidationException(err);
        }

        // Realizar cambio
        facades.getUsuarioFacade().changePass(usuario.getId(), input.contrasena);

        return null;
    }

    private String validarInput(FormInput input) {
        if (Assertions.stringNullOrEmpty(input.contrasena)) {
            return "Se debe especificar una nueva contrseña";
        }

        return null;
    }

    private void updateForm() {
        Prime.update("changepass_form");
    }

    @Data
    public static class FormInput implements Serializable {

        private String contrasena;
    }

}
