/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.archivo;

import java.io.Closeable;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.Part;
import py.com.sepsa.genisys.ejb.logic.impl.Archivos.CacheArchivo;

/**
 *
 * @author Fabio A. González Sosa
 */
public class CacheArchivos implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Coleccion de contenido de archivos en memoria.
     */
    private final List<CacheArchivo> cache;

    private Part file;
    //</editor-fold>

    /**
     * Constructor.
     */
    public CacheArchivos() {
        cache = new LinkedList<>();
    }

    /**
     * Obtiene todos los archivos en cache.
     *
     * @return Coleccion de archivos en cache.
     */
    public Collection<CacheArchivo> getCache() {
        List<CacheArchivo> archivos = new LinkedList<>();
        archivos.addAll(cache);
        return archivos;
    }

    /**
     * Elimina todos los elementos cacheados.
     */
    public void limpiar() {
        cache.clear();
    }

    /**
     * Recibe un archivo desde la vista y almacena temporalmente en memoria.
     */
    public void onSubirArchivo() {
        String nombre = Paths.get(file.getSubmittedFileName()).getFileName().toString();
        long size = file.getSize();

        try (InputStream stream = file.getInputStream()) {
            ByteBuffer buffer = ByteBuffer.allocate((int) size);
            byte[] chunk = new byte[4096];
            int read;
            while ((read = stream.read(chunk)) > -1) {
                buffer.put(chunk, 0, read);
            }
            buffer.flip();

            // Agregar entrada de archivo a cache local.
            CacheArchivo archivo = new CacheArchivo(nombre, buffer);
            cache.add(archivo);
        } catch (Throwable thr) {
            thr.printStackTrace(System.err);
            System.err.printf("Ocurrió un error procesando archivo subido: %s", thr.getMessage());
        }
    }

    /**
     * Cierra un stream abierto.
     *
     * @param closeable Stream abierto que debe ser cerrado.
     * @return {@code true} si el stream fue cerrado exitosamente, {@code false}
     * caso contrario.
     */
    private boolean cerrarStream(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
                return true;
            } catch (Exception e) {
                return false;
            }
        } else {
            return true;
        }
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

}
