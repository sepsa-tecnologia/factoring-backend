/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.varios;

import fa.gs.result.simple.Result;
import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import java8.util.function.Supplier;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "misDatos")
@ViewScoped
public class MisDatosMB extends BasicViewMB implements Serializable {

    @Inject
    private Personas personas;

    @Getter
    @Setter
    private FormInput formInput;

    @PostConstruct
    public void init() {
        // Inicializar contenedor de datos.
        initFormInput();
    }

    private void initFormInput() {
        this.formInput = new FormInput();
        this.formInput.razonSocial = guard(() -> usuarioLogueado.getPersona().getPjRazonSocial(), "");
        this.formInput.nombreFantasia = guard(() -> usuarioLogueado.getPersona().getPjNombreFantasia(), "");
        this.formInput.nombres = guard(() -> usuarioLogueado.getPersona().getPfNombres(), "");
        this.formInput.apellidos = guard(() -> usuarioLogueado.getPersona().getPfApellidos(), "");
        this.formInput.ci = guard(() -> usuarioLogueado.getPersona().getPfCi(), "");
        this.formInput.ruc = guard(() -> Text.ruc(usuarioLogueado.getPersona()), "");
        this.formInput.ciudad = "";
        this.formInput.barrio = guard(() -> usuarioLogueado.getDireccion().getBarrio(), "");
        this.formInput.calle1 = guard(() -> usuarioLogueado.getDireccion().getCalle1(), "");
        this.formInput.calle2 = guard(() -> usuarioLogueado.getDireccion().getCalle2(), "");
        this.formInput.nroCasa = guard(() -> usuarioLogueado.getDireccion().getNroCalle(), "");
        this.formInput.nroTelefono = guard(() -> usuarioLogueado.getContacto().getNroLineaBaja(), "");
        this.formInput.nroCelular = guard(() -> usuarioLogueado.getContacto().getNroCelular(), "");
        this.formInput.email = guard(() -> usuarioLogueado.getContacto().getEmail(), "");
    }

    private <T> T guard(Supplier<T> producer, T fallback) {
        try {
            T val = producer.get();
            return val;
        } catch (Throwable thr) {
            return fallback;
        }
    }

    public void onActualizar() {
        TxWrapperWeb tx = Injection.lookup(TxWrapperWeb.class);
        Result<?> result = tx.execute(() -> {
            Integer idPersona = usuarioLogueado.getPersona().getId();

            // Actualizacion de datos de persona.
            if (usuarioLogueado.esPersonaFisica()) {
                personas.actualizarDatosPersonaFisica(idPersona, formInput.nombres, formInput.apellidos, formInput.ci, formInput.ruc);
            } else if (usuarioLogueado.esPersonaJuridica()) {
                personas.actualizarDatosPersonaJuridica(idPersona, formInput.razonSocial, formInput.nombreFantasia, formInput.ruc);
            } else {
                throw Errors.illegalState();
            }

            // Actualizacion de datos de direccion.
            personas.actualizarBarrio(idPersona, formInput.barrio);
            personas.actualizarCalle1(idPersona, formInput.calle1);
            personas.actualizarCalle2(idPersona, formInput.calle2);
            personas.actualizarNroCasa(idPersona, formInput.nroCasa);

            // Actualizacion de datos de direccion.
            personas.actualizarTelefonoLineaBaja(idPersona, formInput.nroTelefono);
            personas.actualizarTelefonoMovil(idPersona, formInput.nroCelular);
            personas.actualizarEmail(idPersona, formInput.email);

            // Recargar datos de sesion.
            usuarioLogueado.reload();
            return null;
        });

        // Control.
        if (result.isFailure()) {
            log.error().cause(result.failure().cause()).message(result.failure().message()).log();
            mensajes.showGlobalDialogMessage("Ocurrió un error inesperado. Por favor vuelva a intentar.");
        } else {
            mensajes.showGlobalDialogMessage("Datos actualizados correctamente");
        }
    }

    @Data
    public static class FormInput implements Serializable {

        private String razonSocial;
        private String nombreFantasia;
        private String nombres;
        private String apellidos;
        private String ci;
        private String ruc;
        private String ciudad;
        private String barrio;
        private String calle1;
        private String calle2;
        private String nroCasa;
        private String nroTelefono;
        private String nroCelular;
        private String email;
    }

}
