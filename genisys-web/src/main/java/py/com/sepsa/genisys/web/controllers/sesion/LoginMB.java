/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.sesion;

import fa.gs.result.simple.Result;
import fa.gs.utils.authentication.tokens.TokenExtractor;
import fa.gs.utils.jsf.Jsf;
import fa.gs.utils.jsf.JsfMessagesContainer;
import fa.gs.utils.misc.Assertions;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.cdi.ViewScoped;
import py.com.sepsa.genisys.ejb.entities.trans.Usuario;
import py.com.sepsa.genisys.ejb.logic.impl.Sesiones;
import py.com.sepsa.genisys.ejb.security.UserTokenAuthenticator;
import py.com.sepsa.genisys.web.utils.BackingBean;

/**
 *
 * @author Fabio A. González Sosa
 */
@ViewScoped
@Named("vLogin")
public class LoginMB extends BackingBean {

    @Inject
    private Sesiones sesiones;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private JsfMessagesContainer currentInputMessages;

    @PostConstruct
    public void init() {
        this.username = "";
        this.password = "";
        this.currentInputMessages = JsfMessagesContainer.instance();
    }

    public void onLogin() {
        // Reiniciar estado.
        currentInputMessages.reset();

        // Validar datos de entrada.
        String err = validarInput();
        if (err != null) {
            currentInputMessages.pushError(err);
            return;
        }

        // Autenticar usuario.
        Result<Usuario> resUsuario = sesiones.obtenerPorTransUsuario(username, password);
        if (resUsuario.isFailure()) {
            log.debug().cause(resUsuario.failure().cause()).log();
            currentInputMessages.pushError("Credenciales de usuario inválidas.");
        } else {
            // Datos formales de autenticacion.
            Integer idUsuario = resUsuario.value().getId();

            // Generar token con informacion de autenticacion.
            String token = UserTokenAuthenticator.issueToken(idUsuario);

            // Generar cookie con token de autenticacion y redireccionar a home.
            TokenExtractor.COOKIE_NAME = "APP_SESSION_TOKEN_GENISYS";
            Jsf.addCookie(TokenExtractor.COOKIE_NAME, token, "/", -1);
            Jsf.redirect("/app/home.xhtml?faces-redirect=true");
        }
    }

    private String validarInput() {
        if (Assertions.stringNullOrEmpty(username)) {
            return "Se debe especificar un nombre de usuario.";
        }

        if (Assertions.stringNullOrEmpty(password)) {
            return "Se debe especificar una contraseña.";
        }

        return null;
    }

}
