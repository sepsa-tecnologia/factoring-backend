/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.notificaciones.tracking;

import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.web.controllers.varios.UtilsMB;

/**
 *
 * @author Fabio A. González Sosa && Alexander Triana Ríos.
 */
@WebServlet(NotificacionLeidaFilter.URL_MAPPING)
public class NotificacionLeidaFilter extends HttpServlet {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * URL a la que se mapean las acciones del servlet.
     */
    public static final String URL_MAPPING = "/notificacion/track";

    private Notificaciones notificaciones;

    private UtilsMB utils;
    //</editor-fold>

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.notificaciones = Injection.lookup(Notificaciones.class);
        this.utils = Injection.lookupManagedBean(UtilsMB.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String idNotificacion = req.getParameter("id");
            Integer id = utils.unmaskInteger(idNotificacion);
            notificaciones.marcarNotificacionComoLeida(id);
        } finally {
            // Enviar imagen de 1px.
            InputStream stream = NotificacionLeidaFilter.class.getResourceAsStream("/py/com/sepsa/genisys/web/controllers/notificaciones/tracking/1PX.png");
            if (stream != null) {
                resp.setStatus(200);
                resp.addHeader("Content-Type", "image/png");
                resp.addHeader("Content-Length", "95");

                byte[] chunk = new byte[256];
                int read;
                while ((read = stream.read(chunk, 0, 256)) >= 0) {
                    resp.getOutputStream().write(chunk, 0, read);
                    resp.getOutputStream().flush();
                }
            }
        }
    }
}
