/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.factura.operaciones;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.numeric.Currency;
import fa.gs.utils.misc.numeric.Numeric;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ValidationException;
import py.com.generica.epos.web.utils.TxWrapperWeb;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.NotaCredito;
import py.com.sepsa.genisys.ejb.entities.factoring.Retencion;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.facades.info.MonedaFacade;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.Constantes;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;
import py.com.sepsa.genisys.web.controllers.varios.PersonasMB;
import py.com.sepsa.genisys.web.utils.Prime;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "altaFactura")
@ViewScoped
public class AltaFacturaMB extends BasicViewMB {

    //<editor-fold defaultstate="collapsed" desc="Atributos">
    /**
     * Bean para acceso a datos de relaciones con otros tipos de persona
     * (compradores, proveedores, entidades financieras).
     */
    @Inject
    private PersonasMB personas;

    @EJB
    private Facturas facturas;

    @Inject
    private TxWrapperWeb tx;

    /**
     * Contiene los datos que deben ser completados durante el alta de una nueva
     * factura.
     */
    private AltaFacturaMB.Input input;

    /**
     * Parametro de peticion que indica el identificador de factura. Utilizado
     * en la vista de edicion.
     */
    private String idFacturaParam;

    /**
     * Identificador de factura obtenido desde el parametro de peticion en la
     * vista de edicion.
     */
    private Integer idFactura;
    //</editor-fold>

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        this.idFactura = null;
        this.input = nuevoInputAltaFactura(idFactura);
    }

    /**
     * Inicializa el bean, desde la vista de edicion.
     */
    public void init2() {
        // Obtener valor original de identificador de factura.
        idFactura = utils.unmaskInteger(idFacturaParam);
        this.input = nuevoInputAltaFactura(idFactura);
    }

    /**
     * Inicializa un nuevo input para la carga manual de una factura.
     *
     * @return Input.
     */
    private AltaFacturaMB.Input nuevoInputAltaFactura(Integer idFactura) {
        AltaFacturaMB.Input input;

        if (idFactura == null) {
            // Nuevo input en blanco.
            input = new AltaFacturaMB.Input();

            // Inicializar datos de comprador (nulo al comienzo) y proveedor (usuario actual).
            setDatosPersonas(input, null, usuarioLogueado.getPersona());

            // Inicializar moneda por defecto.
            Moneda guarani = facades.getMonedaFacade().getGuarani();
            input.setIdMoneda(guarani);

            // Omitir datos de nota de credito.
            input.setOmitirNotaCredito(true);

            // Omitir datos de retencion.
            input.setOmitirRetencion(true);
        } else {
            // Obtener factura.
            Result<FacturaInfo> resFactura = facturas.buscarFactura(idFactura);
            if (!resFactura.isSuccess()) {
                // No existe la factura, no se puede editar.
                input = null;
            } else {
                // Nuevo input en blanco.
                input = new AltaFacturaMB.Input();

                // Establecer datos de factura.
                FacturaInfo factura = resFactura.value();
                input.setDatosFactura(factura);

                // Establecer datos de nota de credito, si hubiere.
                Result<NotaCredito> resNotaCredito = facturas.buscarNotaCredito(factura.getId());
                if (resNotaCredito.isSuccess()) {
                    input.setDatosNotaCredito(resNotaCredito.value());
                } else {
                    input.setOmitirNotaCredito(true);
                }

                // Establecer datos de retencion, si hubiere.
                Result<Retencion> resRetencion = facturas.buscarRetencion(factura.getId());
                if (resRetencion.isSuccess()) {
                    input.setDatosRetencion(resRetencion.value());
                } else {
                    input.setOmitirRetencion(true);
                }
            }
        }

        return input;
    }

    /**
     * Estblece la razon social y el ruc tanto del comprador como del proveedor,
     * si hubieren, que intervienen en una factura siendo cargada manualmente.
     *
     * @param input Datos de entrada para carga manual de factura.
     * @param comprador Persona comprador.
     * @param proveedor Persona proveedor.
     */
    private void setDatosPersonas(AltaFacturaMB.Input input, Persona comprador, Persona proveedor) {
        if (comprador != null) {
            // Establecer los valores.
            input.setComprador(comprador);
            input.setRazonSocialComprador(utils.getRazonSocial(comprador));
            input.setRucComprador(utils.getRuc(comprador));
        } else {
            // Resetea los valores.
            input.setComprador(null);
            input.setRazonSocialComprador("");
            input.setRucComprador("");
        }

        if (proveedor != null) {
            // Establecer los valores.
            input.setProveedor(proveedor);
            input.setRazonSocialProveedor(utils.getRazonSocial(proveedor));
            input.setRucProveedor(utils.getRuc(proveedor));
        } else {
            // Resetea los valores.
            input.setProveedor(null);
            input.setRazonSocialProveedor("");
            input.setRucProveedor("");
        }
    }

    /**
     * Metodo llamado cuando se selecciona un comprador en la vista de carga
     * manual de facturas. Esta opcion es accesible para los proveedores.
     */
    public void onCompradorChanged() {
        // Actualizar datos de personas.
        setDatosPersonas(input, input.getComprador(), usuarioLogueado.getPersona());
    }

    /**
     * Metodo llamado cuando se indica que los datos de nota de credito no deben
     * ser considerados.
     */
    public void onOmitirNotaCreditoChanged() {
        // Limpiar datos de nota de credito si se estan omitiendo los mismos.
        boolean tmp = input.omitirNotaCredito;
        if (input.omitirNotaCredito) {
            if (input.idNotaCredito != null) {
                mensajes.showGlobalDialogMessage("No se pueden omitir los datos de nota de crédito ya que la misma ya está asociada a la factura");
                input.omitirNotaCredito = false;
            } else {
                input.resetDatosNotaCredito();
                input.setOmitirNotaCredito(tmp);
            }
        }

        // Actualizar la vista.
        updateViewAltaFacturaForm();
    }

    /**
     * Metodo llamado cuando se indica que los datos de retencion no deben ser
     * considerados.
     */
    public void onOmitirRetencionChanged() {
        // Limpiar datos de nota de credito si se estan omitiendo los mismos.
        boolean tmp = input.omitirRetencion;
        if (input.omitirRetencion) {
            if (input.idRetencion != null) {
                mensajes.showGlobalDialogMessage("No se pueden omitir los datos de retención ya que la misma ya está asociada a la factura");
                input.omitirRetencion = false;
            } else {
                input.resetDatosRetencion();
                input.setOmitirRetencion(tmp);
            }
        }

        // Actualizar la vista.
        updateViewAltaFacturaForm();
    }

    /**
     * Permite validar y realizar acciones de persistencia sobre los datos
     * ingresados de manera manual para una factura y/o su nota de credito y
     * retencion asociados.
     */
    public void guardar() {
        tx.execute(this::guardar0);
    }

    private Result<Void> guardar0() {
        Result<Void> result;

        try {
            // Variables a utilizar.
            Factura factura = null;
            NotaCredito notaCredito = null;
            Retencion retencion = null;
            String err;

            // Validar datos de factura.
            err = validarDatosFactura(input);
            if (err != null) {
                mensajes.addMessageError("Error", err);
                throw new ValidationException(err);
            }

            // Generar entidad factura y completar datos adicionales.
            factura = crearFactura(input);

            // Validar datos de nota de credito, si hubiere.
            if (!input.omitirNotaCredito) {
                err = validarDatosNotaCredito(factura, input);
                if (err != null) {
                    mensajes.addMessageError("Error", err);
                    throw new ValidationException(err);
                }
            }

            // Validar datos de retencion, si hubiere.
            if (!input.omitirRetencion) {
                err = validarDatosRetencion(factura, input);
                if (err != null) {
                    mensajes.addMessageError("Error", err);
                    throw new ValidationException(err);
                }
            }

            // Persistir entidad factura.
            if (input.idFactura == null) {
                Result<Factura> resFactura = facturas.registrarFactura(usuarioLogueado.getUsuario().getId(), factura);
                if (!resFactura.isSuccess()) {
                    mensajes.addGlobalDialogMessage("No se pudo guardar la información de factura. Vuelta a intentar.");
                    resFactura.raise();
                }
                factura = resFactura.value();
                log.debug("FACTURA CREADA ID=%d", factura.getId());
            } else {
                beans.getFacades().getFacturaFacade().edit(factura);
                log.debug("FACTURA EDITADA ID=%d", factura.getId());
            }
            input.idFactura = factura.getId();

            // Generar y persistir entidad nota de credito.
            if (!input.omitirNotaCredito) {
                notaCredito = crearNotaCredito(input);
                notaCredito.setIdFactura(factura.getId());
                if (input.idNotaCredito == null) {
                    beans.getFacades().getNotaCreditoFacade().create(notaCredito);
                    log.debug("NOTA CREDITO CREADA ID=%d", notaCredito.getId());
                } else {
                    beans.getFacades().getNotaCreditoFacade().edit(notaCredito);
                    log.debug("NOTA CREDITO EDITADA ID=%d", notaCredito.getId());
                }
                input.idNotaCredito = notaCredito.getId();
            }

            // Generar y persistir entidad retencion.
            if (!input.omitirRetencion) {
                retencion = crearRetencion(factura, input);
                if (input.idRetencion == null) {
                    beans.getFacades().getRetencionFacade().create(retencion);
                    log.debug("RETENCION CREADA ID=%d", retencion.getId());
                } else {
                    beans.getFacades().getRetencionFacade().edit(retencion);
                    log.debug("RETENCION EDITADA ID=%d", retencion.getId());
                }
                input.idRetencion = retencion.getId();
            }

            // Inicializar campos, solo para nueva alta.
            if (idFactura == null) {
                this.input = nuevoInputAltaFactura(null);
            }

            // Mostrar mensaje.
            mensajes.showGlobalDialogMessage(() -> Prime.redirect("/app/abm/factura/lista.xhtml"), "Datos guardados exitosamente");

            result = Results.ok().nullable(true).value(null).build();
        } catch (Throwable thr) {
            String msg = thr.getMessage();
            if (!(thr instanceof ValidationException)) {
                msg = "Ocurrió un error inesperado guardando información de factura y/o documentos asociados";
                mensajes.showGlobalDialogMessage(msg);
            }
            result = Results.ko().cause(thr).message(msg).build();
        } finally {
            // Actualizar vista.
            updateViewAltaFacturaForm();
        }

        return result;
    }

    /**
     * Produce un objeto factura en base a los datos cargados manualmente.
     *
     * @param input Datos ingresados en la vista.
     * @return Objeto factura no persistido en BD.
     */
    public Factura crearFactura(AltaFacturaMB.Input input) {
        // Nuevo objeto Factura
        Factura factura = new Factura();

        // Datos seteados desde el input para una nueva factura.
        factura.setId(input.idFactura);
        factura.setNroFactura(input.nroFactura);
        factura.setIdPersonaOrigen(input.proveedor.getId());
        factura.setRazonSocialProveedor(input.razonSocialProveedor);
        factura.setRucProveedor(input.rucProveedor);
        factura.setIdPersonaDestino(input.comprador.getId());
        factura.setRazonSocialComprador(input.razonSocialComprador);
        factura.setRucComprador(input.rucComprador);
        factura.setFecha(input.fechaEmisionFactura);
        factura.setFechaPago(input.fechaPagoFactura);
        factura.setNroTimbrado(input.nroTimbradoFactura);
        factura.setVtoTimbrado(String.format("%s/%d", input.vtoTimbradoMesFactura, input.vtoTimbradoAnhoFactura));
        factura.setIdMoneda(input.idMoneda.getId());
        factura.setMontoTotal5(input.montoTotalIva5Factura);
        factura.setMontoTotal10(input.montoTotalIva10Factura);
        factura.setMontoTotalExento(input.montoTotalExentoFactura);
        factura.setMontoTotalFactura(input.montoTotalFactura);

        // Estado inicial.
        factura.setIdEstado(beans.getEstados().find(Estados.EstadoFacturaEnum.NO_CONFORMADA));

        // Dias de credito.
        Long diff = factura.getFechaPago().getTime() - factura.getFecha().getTime();
        diff = (diff / 1000 / 60 / 60 / 24);
        factura.setDiasCredito(diff.intValue());

        // Impuesto 5%.
        BigDecimal imponible;
        imponible = Numeric.div(factura.getMontoTotal5(), Numeric.wrap(Constantes.IVA_05_DIVISOR));
        factura.setMontoIva5(factura.getMontoTotal5().subtract(imponible));
        factura.setMontoImponible5(imponible);

        // Impuesto 10%.
        imponible = Numeric.div(factura.getMontoTotal10(), Numeric.wrap(Constantes.IVA_10_DIVISOR));
        factura.setMontoIva10(factura.getMontoTotal10().subtract(imponible));
        factura.setMontoImponible10(imponible);

        // Impuesto total.
        factura.setMontoIvaTotal(factura.getMontoIva5().add(factura.getMontoIva10()));

        // Monto imponible total.
        factura.setMontoImponibleTotal(factura.getMontoImponible5().add(factura.getMontoImponible10()));

        return factura;
    }

    /**
     * Produce un objeto nota de credito en base a los datos cargados
     * manualmente.
     *
     * @param input Datos ingresados en la vista.
     * @return Objeto nota de credito no persistido en BD.
     */
    public NotaCredito crearNotaCredito(AltaFacturaMB.Input input) {
        // Nuevo objeto Nota de Credito
        NotaCredito notaCredito = new NotaCredito();

        // Datos seteados desde el input para una nueva factura.
        notaCredito.setId(input.idNotaCredito);
        notaCredito.setIdFactura(input.idFactura);
        notaCredito.setNroNotaCredito(input.nroNotaCredito);
        notaCredito.setRazonSocialProveedor(input.razonSocialProveedor);
        notaCredito.setRucProveedor(input.rucProveedor);
        notaCredito.setRazonSocialComprador(input.razonSocialComprador);
        notaCredito.setRucComprador(input.rucComprador);
        notaCredito.setFecha(input.fechaEmisionNotaCredito);
        notaCredito.setNroTimbrado(input.nroTimbradoNotaCredito);
        notaCredito.setVtoTimbrado(String.format("%s/%d", input.vtoTimbradoMesNotaCredito, input.vtoTimbradoAnhoNotaCredito));
        notaCredito.setIdMoneda(input.idMoneda.getId());
        notaCredito.setMontoTotal5(input.montoTotalIva5NotaCredito);
        notaCredito.setMontoTotal10(input.montoTotalIva10NotaCredito);
        notaCredito.setMontoTotalExento(input.montoTotalExentoNotaCredito);
        notaCredito.setMontoTotalNotaCredito(input.montoTotalNotaCredito);

        // Impuesto 5%.
        BigDecimal imponible;
        imponible = Numeric.div(notaCredito.getMontoTotal5(), Numeric.wrap(Constantes.IVA_05_DIVISOR));
        notaCredito.setMontoIva5(notaCredito.getMontoTotal5().subtract(imponible));
        notaCredito.setMontoImponible5(imponible);

        // Impuesto 10%.
        imponible = Numeric.div(notaCredito.getMontoTotal10(), Numeric.wrap(Constantes.IVA_10_DIVISOR));
        notaCredito.setMontoIva10(notaCredito.getMontoTotal10().subtract(imponible));
        notaCredito.setMontoImponible10(imponible);

        // Impuesto total.
        notaCredito.setMontoIvaTotal(notaCredito.getMontoIva5().add(notaCredito.getMontoIva10()));

        // Monto imponible total.
        notaCredito.setMontoImponibleTotal(notaCredito.getMontoImponible5().add(notaCredito.getMontoImponible10()));

        return notaCredito;
    }

    /**
     * Produce un objeto retencion en base a los datos cargados manualmente.
     *
     * @param input Datos ingresados en la vista.
     * @return Objeto retencion no persistido en BD.
     */
    public Retencion crearRetencion(Factura factura, AltaFacturaMB.Input input) {
        // Nuevo objeto Nota de Credito
        Retencion retencion = new Retencion();

        // Datos seteados desde el input para una nueva factura.
        retencion.setId(input.idRetencion);
        retencion.setIdFactura(input.idFactura);
        retencion.setNroRetencion(input.nroRetencion);
        retencion.setRazonSocialProveedor(input.razonSocialProveedor);
        retencion.setRucProveedor(input.rucProveedor);
        retencion.setRazonSocialComprador(input.razonSocialComprador);
        retencion.setRucComprador(input.rucComprador);
        retencion.setFecha(input.fechaEmisionRetencion);
        retencion.setNroTimbrado(input.nroTimbradoRetencion);
        retencion.setVtoTimbrado(String.format("%s/%d", input.vtoTimbradoMesRetencion, input.vtoTimbradoAnhoRetencion));
        retencion.setIdMoneda(input.idMoneda.getId());
        retencion.setMontoRetenidoTotal(BigDecimal.ONE);

        /**
         * Montos que hacen referencia a factura, pero que no tienen utilidad.
         * Es decir, el monto imponible total, el monto iva total y el monto
         * total de la retencion deben contener los mismos valores,
         * respectivamente, que figuran en la factura asociada. Sin embargo, la
         * factura asociada ya contiene los valores correspondientes por lo que
         * esto es redundante, pero se completan de igual manera ya que existe
         * una restriccion de no nulidad sobre estos campos.
         */
        retencion.setMontoImponibleTotal(factura.getMontoImponibleTotal());
        retencion.setMontoIvaTotal(factura.getMontoIvaTotal());
        retencion.setMontoTotal(factura.getMontoTotalFactura());

        // Porcentaje de retencion.
        BigDecimal porcentaje = Numeric.div(Numeric.mul(input.montoTotalRetencion, Numeric.CIEN), factura.getMontoIvaTotal());
        retencion.setPorcentajeRetencion(porcentaje);

        return retencion;
    }

    //<editor-fold defaultstate="collapsed" desc="Validacion de Datos">
    /**
     * Valida los datos de factura ingresados en la vista.
     *
     * @param input Datos ingresados en la vista.
     * @return Mensaje de error, si hubiere.
     */
    private String validarDatosFactura(AltaFacturaMB.Input input) {
        String err;
        BigDecimal tmp;

        // Validar numero de documento.
        err = validarNumeroDocumento(input.nroFactura, "número de comprobante de factura");
        if (err != null) {
            return err;
        }

        // Validar fecha de emision.
        if (input.fechaEmisionFactura == null) {
            return "El campo fecha de emisión de factura no puede ser vacío.";
        }

        // Validar fecha de pago.
        if (input.fechaPagoFactura == null) {
            return "El campo fecha de pago de factura no puede ser vacío.";
        }
        if (Fechas.diasDiferencia(input.fechaEmisionFactura, input.fechaPagoFactura) <= 0) {
            return "El campo fecha de pago de factura no puede contener un valor inferior al del campo fecha de emisión de factura.";
        }

        // Validar numero de timbrado.
        if (Assertions.stringNullOrEmpty(input.nroTimbradoFactura)) {
            return "El campo número de timbrado de factura no puede ser vacío.";
        }

        // Validar fecha de vencimiento de timbrado.
        if (Assertions.stringNullOrEmpty(input.vtoTimbradoMesFactura)) {
            return "El campo mes de vencimiento de timbrado de factura no puede ser vacío.";
        }
        if (Assertions.isNull(input.vtoTimbradoAnhoFactura)) {
            return "El campo año de vencimiento de timbrado de factura no puede ser vacío.";
        }

        // Validar datos del comprador.
        if (input.comprador == null) {
            return "Debe seleccionar un comprador de la lista de compradores habilitados.";
        }
        if (Assertions.stringNullOrEmpty(input.rucComprador)) {
            return "El campo RUC de comprador no puede ser vacío.";
        }
        if (Assertions.stringNullOrEmpty(input.razonSocialComprador)) {
            return "El campo razón social de comprador no puede ser vacío.";
        }

        // Validar datos del proveedor.
        if (input.proveedor == null) {
            return "Debe seleccionar un proveedor de la lista de proveedores habilitados.";
        }
        if (Assertions.stringNullOrEmpty(input.rucProveedor)) {
            return "El campo RUC de proveedor no puede ser vacío.";
        }
        if (Assertions.stringNullOrEmpty(input.razonSocialProveedor)) {
            return "El campo razón social de proveedor no puede ser vacío.";
        }

        // Validar moneda.
        if (input.idMoneda == null) {
            return "El campo moneda no puede ser vacío.";
        }

        // Validar monto total IVA 5%.
        if (input.montoTotalIva5Factura == null) {
            return "El campo monto total IVA 5% de factura no puede ser vacío.";
        }
        if (Numeric.menor(input.montoTotalIva5Factura, Numeric.CERO)) {
            return "El campo monto total IVA 5% de factura no puede ser menor que cero (0).";
        }

        // Validar monto total IVA 10%.
        if (input.montoTotalIva10Factura == null) {
            return "El campo monto total IVA 10% de factura no puede ser vacío.";
        }
        if (Numeric.menor(input.montoTotalIva10Factura, Numeric.CERO)) {
            return "El campo monto total IVA 10% de factura no puede ser menor que cero (0).";
        }

        // Validar monto total exento.
        if (input.montoTotalExentoFactura == null) {
            return "El campo monto total exento de factura no puede ser vacío.";
        }
        if (Numeric.menor(input.montoTotalExentoFactura, Numeric.CERO)) {
            return "El campo monto total exento de factura no puede ser menor que cero (0).";
        }

        // Validar monto total.
        if (input.montoTotalFactura == null) {
            return "El campo monto total de factura no puede ser vacío.";
        }
        if (Numeric.menorIgual(input.montoTotalFactura, Numeric.CERO)) {
            return "El campo monto total de factura no puede ser menor o igual que cero (0).";
        }
        tmp = Numeric.sum(input.montoTotalIva5Factura, input.montoTotalIva10Factura, input.montoTotalExentoFactura);
        if (Numeric.menor(input.montoTotalFactura, tmp)) {
            return String.format("El campo monto total de factura no puede ser menor que la suma de los demás campos de monto (%s).", Currency.formatCurrency(tmp, input.idMoneda.getAbreviatura()));
        }
        if (Numeric.distinto(input.montoTotalFactura, tmp)) {
            return String.format("El campo monto total de factura no concuerda con la suma de los demás campos de monto (%s).", Currency.formatCurrency(tmp, input.idMoneda.getAbreviatura()));
        }

        // Validar si la factura ya existe.
        boolean existe = facturas.facturaExiste(input.nroFactura, input.proveedor.getRazonSocial(), input.comprador.getRazonSocial(), input.fechaEmisionFactura, input.fechaPagoFactura, input.montoTotalFactura, input.idMoneda.getId());
        if (existe) {
            return ("Los datos de factura ya están registrados.");
        }

        return null;
    }

    /**
     * Valida los datos de nota de credito ingresados en la vista.
     *
     * @param input Datos ingresados en la vista.
     * @return Mensaje de error, si hubiere.
     */
    private String validarDatosNotaCredito(Factura factura, AltaFacturaMB.Input input) {
        String err;
        BigDecimal tmp;

        // Validar numero de documento.
        err = validarNumeroDocumento(input.nroNotaCredito, "número de nota de crédito");
        if (err != null) {
            return err;
        }

        // Validar fecha de emision.
        if (input.fechaEmisionNotaCredito == null) {
            return "El campo fecha de emisión de nota de crédito no puede ser vacío.";
        }

        // Validar numero de timbrado.
        if (Assertions.stringNullOrEmpty(input.nroTimbradoNotaCredito)) {
            return "El campo número de timbrado de nota de crédito no puede ser vacío.";
        }

        // Validar fecha de vencimiento de timbrado.
        if (Assertions.stringNullOrEmpty(input.vtoTimbradoMesNotaCredito)) {
            return "El campo mes de vencimiento de timbrado de nota de crédito no puede ser vacío.";
        }
        if (Assertions.isNull(input.vtoTimbradoAnhoNotaCredito)) {
            return "El campo año de vencimiento de timbrado de nota de crédito no puede ser vacío.";
        }

        // Validar monto total IVA 5%.
        if (input.montoTotalIva5NotaCredito == null) {
            return "El campo monto total IVA 5% de nota de crédito no puede ser vacío.";
        }
        if (Numeric.menor(input.montoTotalIva5NotaCredito, Numeric.CERO)) {
            return "El campo monto total IVA 5% de nota de crédito no puede ser menor que cero (0).";
        }
        if (Numeric.mayor(input.montoTotalIva5NotaCredito, factura.getMontoTotal5())) {
            return String.format("El campo monto total IVA 5%% de nota de crédito no puede ser superior al monto total IVA 5%% de la factura asociada (%s).", Currency.formatCurrency(factura.getMontoTotal5(), input.idMoneda.getAbreviatura()));
        }

        // Validar monto total IVA 10%.
        if (input.montoTotalIva10NotaCredito == null) {
            return "El campo monto total 10% de nota de crédito no puede ser vacío.";
        }
        if (Numeric.menor(input.montoTotalIva10NotaCredito, Numeric.CERO)) {
            return "El campo monto total 10% de nota de crédito no puede ser menor que cero (0).";
        }
        if (Numeric.mayor(input.montoTotalIva10NotaCredito, factura.getMontoTotal10())) {
            return String.format("El campo monto total IVA 10%% de nota de crédito no puede ser superior al monto total IVA 10%% de la factura asociada (%s).", Currency.formatCurrency(factura.getMontoTotal10(), input.idMoneda.getAbreviatura()));
        }

        // Validar monto total exento.
        if (input.montoTotalExentoNotaCredito == null) {
            return "El campo monto total exento de nota de crédito no puede ser vacío.";
        }
        if (Numeric.menor(input.montoTotalExentoNotaCredito, Numeric.CERO)) {
            return "El campo monto total exento de nota de crédito no puede ser menor que cero (0).";
        }
        if (Numeric.mayor(input.montoTotalExentoNotaCredito, factura.getMontoTotalExento())) {
            return String.format("El campo monto total exento de nota de crédito no puede ser superior al monto total exento de la factura asociada (%s).", Currency.formatCurrency(factura.getMontoTotalExento(), input.idMoneda.getAbreviatura()));
        }

        // Validar monto total.
        if (input.montoTotalNotaCredito == null) {
            return "El campo monto total de nota de crédito no puede ser vacío.";
        }
        if (Numeric.menorIgual(input.montoTotalNotaCredito, Numeric.CERO)) {
            return "El campo monto total de nota de crédito no puede ser menor o igual que cero (0).";
        }
        tmp = Numeric.sum(input.montoTotalIva5NotaCredito, input.montoTotalIva10NotaCredito, input.montoTotalExentoNotaCredito);
        if (Numeric.menor(input.montoTotalNotaCredito, tmp)) {
            return String.format("El campo monto total de nota de crédito no puede ser menor que la suma de los demás campos de monto (%s).", Currency.formatCurrency(tmp, input.idMoneda.getAbreviatura()));
        }
        if (Numeric.distinto(input.montoTotalNotaCredito, tmp)) {
            return String.format("El campo monto total de nota de crédito no concuerda con la suma de los demás campos de monto (%s).", Currency.formatCurrency(tmp, input.idMoneda.getAbreviatura()));
        }
        if (Numeric.mayor(input.montoTotalNotaCredito, factura.getMontoTotalFactura())) {
            return String.format("El campo monto total de nota de crédito no puede ser superior al monto total de la factura asociada (%s).", Currency.formatCurrency(factura.getMontoTotalFactura(), input.idMoneda.getAbreviatura()));
        }

        return null;
    }

    /**
     * Valida los datos de retencion ingresados en la vista.
     *
     * @param input Datos ingresados en la vista.
     * @return Mensaje de error, si hubiere.
     */
    private String validarDatosRetencion(Factura factura, AltaFacturaMB.Input input) {
        String err;

        // Validar numero de documento.
        err = validarNumeroDocumento(input.nroRetencion, "número de retención");
        if (err != null) {
            return err;
        }

        // Validar fecha de emision.
        if (input.fechaEmisionRetencion == null) {
            return "El campo fecha de emisión de retención no puede ser vacío.";
        }

        // Validar numero de timbrado.
        if (Assertions.stringNullOrEmpty(input.nroTimbradoRetencion)) {
            return "El campo número de timbrado de retención no puede ser vacío.";
        }

        // Validar fecha de vencimiento de timbrado.
        if (Assertions.stringNullOrEmpty(input.vtoTimbradoMesRetencion)) {
            return "El campo mes de vencimiento de timbrado de retención no puede ser vacío.";
        }
        if (Assertions.isNull(input.vtoTimbradoAnhoRetencion)) {
            return "El campo año de vencimiento de timbrado de retención no puede ser vacío.";
        }

        // Validar monto total retenido.
        if (input.montoTotalRetencion == null) {
            return "El campo monto total retenido no puede ser vacío.";
        }
        if (Numeric.menor(input.montoTotalRetencion, Numeric.CERO)) {
            return "El campo monto total retenido no puede ser menor que cero (0).";
        }
        if (Numeric.mayor(input.montoTotalRetencion, factura.getMontoIvaTotal())) {
            return String.format("El campo monto total retenido no puede ser superior al monto total de IVA de la factura asociada (%s).", Currency.formatCurrency(factura.getMontoIvaTotal(), input.idMoneda.getAbreviatura()));
        }

        return null;
    }

    /**
     * Valida el valor ingresado como numero de documento en formato
     * xxx-xxx-xxxxxx.
     *
     * @param value Valor ingresado como numero de documento.
     * @param nombreCampo Nombre del campo en la vista para incluir en los
     * mensajes de error.
     * @return Mensaje de error, si hubiere.
     */
    public static String validarNumeroDocumento(String value, String nombreCampo) {
        if (Assertions.stringNullOrEmpty(value) || value.equals("___-___-_______")) {
            return String.format("El campo '%s' no puede estar vacío.", nombreCampo);
        }

        if (!Assertions.isNumeroFactura(value)) {
            return String.format("El campo '%s' debe contener una serie de números enteros en el formato xxx-xxx-xxxxxxx.", nombreCampo);
        }

        return null;
    }

    /**
     * Valida el valor ingresado como numero de registro unico de contribuyente
     * (RUC).
     *
     * @param value Valore ingresado como RUC.
     * @param nombreCampo Nombre del campo en la vista para incluir en los
     * mensajes de error.
     * @return Mensaje de error, si hubiere.
     */
    public static String validarRuc(String value, String nombreCampo) {
        if (fa.gs.utils.misc.Assertions.stringNullOrEmpty(value)) {
            return String.format("El campo '%s' no puede estar vacío.", nombreCampo);
        }

        if (!fa.gs.utils.misc.Assertions.isRuc(value)) {
            return String.format("El campo '%s' contiene un valor de RUC inválido.", nombreCampo);
        }

        return null;
    }
    //</editor-fold>

    private void updateViewAltaFacturaForm() {
        Prime.update("alta_factura_form");
    }

    //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
    public PersonasMB getPersonas() {
        return personas;
    }

    public void setPersonas(PersonasMB personas) {
        this.personas = personas;
    }

    public String getIdFacturaParam() {
        return idFacturaParam;
    }

    public void setIdFacturaParam(String idFacturaParam) {
        this.idFacturaParam = idFacturaParam;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
    }
    //</editor-fold>

    /**
     * Clase que sirve como contenedor de todos los datos involucrados durante
     * la carga manual de una factura.
     */
    public static class Input {

        //<editor-fold defaultstate="collapsed" desc="Atributos">
        //<editor-fold defaultstate="collapsed" desc="Atributos de Factura">
        private Integer idFactura;
        private Moneda idMoneda;
        private String nroFactura;
        private Persona proveedor;
        private String razonSocialProveedor;
        private String rucProveedor;
        private Persona comprador;
        private String razonSocialComprador;
        private String rucComprador;
        private Date fechaEmisionFactura;
        private Date fechaPagoFactura;
        private String nroTimbradoFactura;
        private String vtoTimbradoMesFactura;
        private Integer vtoTimbradoAnhoFactura;
        private BigDecimal montoTotalIva5Factura;
        private BigDecimal montoTotalIva10Factura;
        private BigDecimal montoTotalExentoFactura;
        private BigDecimal montoTotalFactura;
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Atributos de Nota de Credito">
        private Integer idNotaCredito;
        private boolean omitirNotaCredito;
        private String nroNotaCredito;
        private Date fechaEmisionNotaCredito;
        private String nroTimbradoNotaCredito;
        private String vtoTimbradoMesNotaCredito;
        private Integer vtoTimbradoAnhoNotaCredito;
        private BigDecimal montoTotalIva5NotaCredito;
        private BigDecimal montoTotalIva10NotaCredito;
        private BigDecimal montoTotalExentoNotaCredito;
        private BigDecimal montoTotalNotaCredito;
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Atributos de Retencion">
        private Integer idRetencion;
        private boolean omitirRetencion;
        private String nroRetencion;
        private Date fechaEmisionRetencion;
        private String nroTimbradoRetencion;
        private String vtoTimbradoMesRetencion;
        private Integer vtoTimbradoAnhoRetencion;
        private BigDecimal montoTotalRetencion;
        //</editor-fold>
        //</editor-fold>

        /**
         * Constructor.
         */
        private Input() {
            resetDatosFactura();
            resetDatosNotaCredito();
            resetDatosRetencion();
        }

        /**
         * Establece los valores por defecto relacionados a la factura.
         */
        private void resetDatosFactura() {
            idFactura = null;
            idMoneda = null;
            nroFactura = null;
            proveedor = null;
            razonSocialProveedor = null;
            rucProveedor = null;
            comprador = null;
            razonSocialComprador = null;
            rucComprador = null;
            fechaEmisionFactura = null;
            fechaPagoFactura = null;
            nroTimbradoFactura = null;
            vtoTimbradoMesFactura = null;
            vtoTimbradoAnhoFactura = null;
            montoTotalIva5Factura = Numeric.CERO;
            montoTotalIva10Factura = Numeric.CERO;
            montoTotalExentoFactura = Numeric.CERO;
            montoTotalFactura = Numeric.CERO;
        }

        /**
         * Establece los valores relacionados a la factura.
         *
         * @param factura Factura.
         */
        private void setDatosFactura(FacturaInfo factura) {
            // Obtener moneda.
            MonedaFacade monedaFacade = Injection.lookup(MonedaFacade.class);
            Moneda moneda0 = monedaFacade.find(factura.getMonedaId());

            // Obtener comprador;
            Personas personas = Injection.lookup(Personas.class);
            Persona comprador0 = personas.obtenerPorId(factura.getCompradorId()).value(null);

            // Obtener proveedor.
            Persona proveedor0 = personas.obtenerPorId(factura.getProveedorId()).value(null);

            idFactura = factura.getId();
            idMoneda = moneda0;
            nroFactura = factura.getNumero();
            proveedor = proveedor0;
            razonSocialProveedor = factura.getProveedorRazonSocial();
            rucProveedor = factura.getProveedorRuc();
            comprador = comprador0;
            razonSocialComprador = factura.getCompradorRazonSocial();
            rucComprador = factura.getCompradorRuc();
            fechaEmisionFactura = factura.getFechaEmision();
            fechaPagoFactura = factura.getFechaPago();
            nroTimbradoFactura = factura.getTimbradoNumero();
            vtoTimbradoMesFactura = (Assertions.stringNullOrEmpty(factura.getTimbradoVencimiento())) ? null : factura.getTimbradoVencimiento().split("/")[0];
            vtoTimbradoAnhoFactura = (Assertions.stringNullOrEmpty(factura.getTimbradoVencimiento())) ? null : Integer.valueOf(factura.getTimbradoVencimiento().split("/")[1]);
            montoTotalIva5Factura = factura.getMontoTotalIva5();
            montoTotalIva10Factura = factura.getMontoTotalIva10();
            montoTotalExentoFactura = factura.getMontoTotalExento();
            montoTotalFactura = factura.getMontoTotal();
        }

        /**
         * Establece los valores por defecto relacionados a la nota de credito.
         */
        private void resetDatosNotaCredito() {
            idNotaCredito = null;
            omitirNotaCredito = false;
            nroNotaCredito = null;
            fechaEmisionNotaCredito = null;
            nroTimbradoNotaCredito = null;
            vtoTimbradoMesNotaCredito = null;
            vtoTimbradoAnhoNotaCredito = null;
            montoTotalIva5NotaCredito = Numeric.CERO;
            montoTotalIva10NotaCredito = Numeric.CERO;
            montoTotalExentoNotaCredito = Numeric.CERO;
            montoTotalNotaCredito = Numeric.CERO;
        }

        /**
         * Establece los valores relacionados a la nota de credito.
         *
         * @param notaCredito Nota de Credito.
         */
        private void setDatosNotaCredito(NotaCredito notaCredito) {
            idNotaCredito = notaCredito.getId();
            omitirNotaCredito = false;
            nroNotaCredito = notaCredito.getNroNotaCredito();
            fechaEmisionNotaCredito = notaCredito.getFecha();
            nroTimbradoNotaCredito = notaCredito.getNroTimbrado();
            vtoTimbradoMesNotaCredito = notaCredito.getVtoTimbrado().split("/")[0];
            vtoTimbradoAnhoNotaCredito = Integer.valueOf(notaCredito.getVtoTimbrado().split("/")[1]);
            montoTotalIva5NotaCredito = notaCredito.getMontoTotal5();
            montoTotalIva10NotaCredito = notaCredito.getMontoTotal10();
            montoTotalExentoNotaCredito = notaCredito.getMontoTotalExento();
            montoTotalNotaCredito = notaCredito.getMontoTotalNotaCredito();
        }

        /**
         * Establece los valores por defecto relacionados a la retencion.
         */
        private void resetDatosRetencion() {
            idRetencion = null;
            omitirRetencion = false;
            nroRetencion = null;
            fechaEmisionRetencion = null;
            nroTimbradoRetencion = null;
            vtoTimbradoMesRetencion = null;
            vtoTimbradoAnhoRetencion = null;
            montoTotalRetencion = Numeric.CERO;
        }

        /**
         * Establece los valores relacionados a la retencion.
         *
         * @param retencion Retencion.
         */
        private void setDatosRetencion(Retencion retencion) {
            idRetencion = retencion.getId();
            omitirRetencion = false;
            nroRetencion = retencion.getNroRetencion();
            fechaEmisionRetencion = retencion.getFecha();
            nroTimbradoRetencion = retencion.getNroRetencion();
            vtoTimbradoMesRetencion = retencion.getVtoTimbrado().split("/")[0];
            vtoTimbradoAnhoRetencion = Integer.valueOf(retencion.getVtoTimbrado().split("/")[1]);
            montoTotalRetencion = retencion.getMontoRetenidoTotal();
        }

        //<editor-fold defaultstate="collapsed" desc="Getters y Setters">
        public Integer getIdFactura() {
            return idFactura;
        }

        public void setIdFactura(Integer idFactura) {
            this.idFactura = idFactura;
        }

        public Moneda getIdMoneda() {
            return idMoneda;
        }

        public void setIdMoneda(Moneda idMoneda) {
            this.idMoneda = idMoneda;
        }

        public String getNroFactura() {
            return nroFactura;
        }

        public void setNroFactura(String nroFactura) {
            this.nroFactura = nroFactura;
        }

        public Persona getProveedor() {
            return proveedor;
        }

        public void setProveedor(Persona proveedor) {
            this.proveedor = proveedor;
        }

        public String getRazonSocialProveedor() {
            return razonSocialProveedor;
        }

        public void setRazonSocialProveedor(String razonSocialProveedor) {
            this.razonSocialProveedor = razonSocialProveedor;
        }

        public String getRucProveedor() {
            return rucProveedor;
        }

        public void setRucProveedor(String rucProveedor) {
            this.rucProveedor = rucProveedor;
        }

        public Persona getComprador() {
            return comprador;
        }

        public void setComprador(Persona comprador) {
            this.comprador = comprador;
        }

        public String getRazonSocialComprador() {
            return razonSocialComprador;
        }

        public void setRazonSocialComprador(String razonSocialComprador) {
            this.razonSocialComprador = razonSocialComprador;
        }

        public String getRucComprador() {
            return rucComprador;
        }

        public void setRucComprador(String rucComprador) {
            this.rucComprador = rucComprador;
        }

        public Date getFechaEmisionFactura() {
            return fechaEmisionFactura;
        }

        public void setFechaEmisionFactura(Date fechaEmisionFactura) {
            this.fechaEmisionFactura = fechaEmisionFactura;
        }

        public Date getFechaPagoFactura() {
            return fechaPagoFactura;
        }

        public void setFechaPagoFactura(Date fechaPagoFactura) {
            this.fechaPagoFactura = fechaPagoFactura;
        }

        public String getNroTimbradoFactura() {
            return nroTimbradoFactura;
        }

        public void setNroTimbradoFactura(String nroTimbradoFactura) {
            this.nroTimbradoFactura = nroTimbradoFactura;
        }

        public String getVtoTimbradoMesFactura() {
            return vtoTimbradoMesFactura;
        }

        public void setVtoTimbradoMesFactura(String vtoTimbradoMesFactura) {
            this.vtoTimbradoMesFactura = vtoTimbradoMesFactura;
        }

        public Integer getVtoTimbradoAnhoFactura() {
            return vtoTimbradoAnhoFactura;
        }

        public void setVtoTimbradoAnhoFactura(Integer vtoTimbradoAnhoFactura) {
            this.vtoTimbradoAnhoFactura = vtoTimbradoAnhoFactura;
        }

        public BigDecimal getMontoTotalIva5Factura() {
            return montoTotalIva5Factura;
        }

        public void setMontoTotalIva5Factura(BigDecimal montoTotalIva5Factura) {
            this.montoTotalIva5Factura = montoTotalIva5Factura;
        }

        public BigDecimal getMontoTotalIva10Factura() {
            return montoTotalIva10Factura;
        }

        public void setMontoTotalIva10Factura(BigDecimal montoTotalIva10Factura) {
            this.montoTotalIva10Factura = montoTotalIva10Factura;
        }

        public BigDecimal getMontoTotalExentoFactura() {
            return montoTotalExentoFactura;
        }

        public void setMontoTotalExentoFactura(BigDecimal montoTotalExentoFactura) {
            this.montoTotalExentoFactura = montoTotalExentoFactura;
        }

        public BigDecimal getMontoTotalFactura() {
            return montoTotalFactura;
        }

        public void setMontoTotalFactura(BigDecimal montoTotalFactura) {
            this.montoTotalFactura = montoTotalFactura;
        }

        public Integer getIdNotaCredito() {
            return idNotaCredito;
        }

        public void setIdNotaCredito(Integer idNotaCredito) {
            this.idNotaCredito = idNotaCredito;
        }

        public boolean getOmitirNotaCredito() {
            return omitirNotaCredito;
        }

        public void setOmitirNotaCredito(boolean omitirNotaCredito) {
            this.omitirNotaCredito = omitirNotaCredito;
        }

        public String getNroNotaCredito() {
            return nroNotaCredito;
        }

        public void setNroNotaCredito(String nroNotaCredito) {
            this.nroNotaCredito = nroNotaCredito;
        }

        public Date getFechaEmisionNotaCredito() {
            return fechaEmisionNotaCredito;
        }

        public void setFechaEmisionNotaCredito(Date fechaEmisionNotaCredito) {
            this.fechaEmisionNotaCredito = fechaEmisionNotaCredito;
        }

        public String getNroTimbradoNotaCredito() {
            return nroTimbradoNotaCredito;
        }

        public void setNroTimbradoNotaCredito(String nroTimbradoNotaCredito) {
            this.nroTimbradoNotaCredito = nroTimbradoNotaCredito;
        }

        public String getVtoTimbradoMesNotaCredito() {
            return vtoTimbradoMesNotaCredito;
        }

        public void setVtoTimbradoMesNotaCredito(String vtoTimbradoMesNotaCredito) {
            this.vtoTimbradoMesNotaCredito = vtoTimbradoMesNotaCredito;
        }

        public Integer getVtoTimbradoAnhoNotaCredito() {
            return vtoTimbradoAnhoNotaCredito;
        }

        public void setVtoTimbradoAnhoNotaCredito(Integer vtoTimbradoAnhoNotaCredito) {
            this.vtoTimbradoAnhoNotaCredito = vtoTimbradoAnhoNotaCredito;
        }

        public BigDecimal getMontoTotalIva5NotaCredito() {
            return montoTotalIva5NotaCredito;
        }

        public void setMontoTotalIva5NotaCredito(BigDecimal montoTotalIva5NotaCredito) {
            this.montoTotalIva5NotaCredito = montoTotalIva5NotaCredito;
        }

        public BigDecimal getMontoTotalIva10NotaCredito() {
            return montoTotalIva10NotaCredito;
        }

        public void setMontoTotalIva10NotaCredito(BigDecimal montoTotalIva10NotaCredito) {
            this.montoTotalIva10NotaCredito = montoTotalIva10NotaCredito;
        }

        public BigDecimal getMontoTotalExentoNotaCredito() {
            return montoTotalExentoNotaCredito;
        }

        public void setMontoTotalExentoNotaCredito(BigDecimal montoTotalExentoNotaCredito) {
            this.montoTotalExentoNotaCredito = montoTotalExentoNotaCredito;
        }

        public BigDecimal getMontoTotalNotaCredito() {
            return montoTotalNotaCredito;
        }

        public void setMontoTotalNotaCredito(BigDecimal montoTotalNotaCredito) {
            this.montoTotalNotaCredito = montoTotalNotaCredito;
        }

        public Integer getIdRetencion() {
            return idRetencion;
        }

        public void setIdRetencion(Integer idRetencion) {
            this.idRetencion = idRetencion;
        }

        public boolean getOmitirRetencion() {
            return omitirRetencion;
        }

        public void setOmitirRetencion(boolean omitirRetencion) {
            this.omitirRetencion = omitirRetencion;
        }

        public String getNroRetencion() {
            return nroRetencion;
        }

        public void setNroRetencion(String nroRetencion) {
            this.nroRetencion = nroRetencion;
        }

        public Date getFechaEmisionRetencion() {
            return fechaEmisionRetencion;
        }

        public void setFechaEmisionRetencion(Date fechaEmisionRetencion) {
            this.fechaEmisionRetencion = fechaEmisionRetencion;
        }

        public String getNroTimbradoRetencion() {
            return nroTimbradoRetencion;
        }

        public void setNroTimbradoRetencion(String nroTimbradoRetencion) {
            this.nroTimbradoRetencion = nroTimbradoRetencion;
        }

        public String getVtoTimbradoMesRetencion() {
            return vtoTimbradoMesRetencion;
        }

        public void setVtoTimbradoMesRetencion(String vtoTimbradoMesRetencion) {
            this.vtoTimbradoMesRetencion = vtoTimbradoMesRetencion;
        }

        public Integer getVtoTimbradoAnhoRetencion() {
            return vtoTimbradoAnhoRetencion;
        }

        public void setVtoTimbradoAnhoRetencion(Integer vtoTimbradoAnhoRetencion) {
            this.vtoTimbradoAnhoRetencion = vtoTimbradoAnhoRetencion;
        }

        public BigDecimal getMontoTotalRetencion() {
            return montoTotalRetencion;
        }

        public void setMontoTotalRetencion(BigDecimal montoTotalRetencion) {
            this.montoTotalRetencion = montoTotalRetencion;
        }
        //</editor-fold>

    }

}
