/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.web.controllers.varios;

import fa.gs.utils.collections.Lists;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.web.controllers.BasicViewMB;

/**
 *
 * @author Fabio A. González Sosa
 */
@Named(value = "personas")
@SessionScoped
public class PersonasMB extends BasicViewMB {

    @Inject
    private Personas personas;

    /**
     * Lista de proveedores asociados a persona logueada.
     */
    @Getter
    private Collection<Persona> proveedores;

    /**
     * Lista de compradores asociados a persona logueada.
     */
    @Getter
    private Collection<Persona> compradores;

    /**
     * Lista de entidades financieras disponibles a persona logueada.
     */
    @Getter
    private Collection<Persona> entidadesFinancieras;

    /**
     * Inicializa el bean.
     */
    @PostConstruct
    public void init() {
        Integer idPersona = usuarioLogueado.getPersona().getId();
        this.proveedores = personas.obtenerProveedoresDisponibles(idPersona).value(Lists.empty());
        this.compradores = personas.obtenerCompradoresDisponibles(idPersona).value(Lists.empty());
        this.entidadesFinancieras = personas.obtenerEntidadesFinancierasDisponibles(idPersona).value(Lists.empty());
    }

    public Collection<Persona> agruparTodos() {
        // Inicializar conjunto de personas distintas.
        Set<Persona> persona0 = new TreeSet<>((Persona a, Persona b) -> {
            return a.getId().compareTo(b.getId());
        });
        persona0.addAll(compradores);
        persona0.addAll(proveedores);
        persona0.addAll(entidadesFinancieras);

        // Ordenar por razon social.
        List<Persona> persona1 = Lists.empty();
        persona1.addAll(persona0);
        persona1.sort((Persona a, Persona b) -> a.getRazonSocial().compareTo(b.getRazonSocial()));
        return persona1;
    }

}
