/* global PF*/

$(document).ready(function () {
    // Alineacion de texto en cabecera y pie de pagina.
    $('#app_footer >').addClass('text-center');
    $('#app_top_header >').addClass('text-center');

    // Eliminar el estilo ui-commandlink de los elementos que lo utilicen.
    $('.ui-commandlink').removeClass('ui-commandlink');

    // Corrige un error de alineacion del icono de dropdown en los combobox de primefaces.
    $('.ui-selectonemenu-trigger').css('padding', 0);

    // Fuerza a los componentes de entrada de fecha a tomar el 100% de ancho en un formulario.
    $('.ui-calendar > input').css('width', '100% !important');

    // Fuerza al componente de búsqueda dentro de un combobox a tomar el 100% de espacio.
    $('.ui-selectonemenu-filter-container').css('width', '100%');

    // Al igualar las alturas de los componentes dentro del mismo grupo (mismo data-match-height), mantiene el scroll dentro de la vista.
    // Caso contrario, en cada actualizacion de alturas se hace un scroll al tope de la vista.
    $.fn.matchHeight._maintainScroll = true;

});

/**
 * Calcula la dimension visible en la ventana del navegador.
 * @returns {viewportSize.mainAnonym$0} Objeto con el ancho y alto del viewport.
 */
function viewportSize() {
    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    return {w: w, h: h};
}

/**
 * Calcula la dimension de un elemento del DOM.
 * @param {type} el Elemento del DOM.
 * @returns {elementSize.mainAnonym$1} Objeto con el ancho y alto del elemento.
 */
function elementSize(el) {
    el = $(el);
    var w = el.outerWidth();
    var h = el.outerHeight();
    return {w: w, h: h};
}

/**
 * Ajusta un cuadro de dialogo al momento que es desplegado en la vista.
 * @param {type} el Elemento DOM.
 */
function ajustarDialogo(el) {
    try {
        if (el) {
            // Evitar que se pueda hacer scroll sobre el documento mientras el dialogo esta superpuesto.
            $('body').css('position', 'fixed');

            // Eliminar clase de ajuste, si hubiere.
            var dialog = PF(el.widgetVar);
            el = $(dialog.jqId);
            el.removeClass("app_fit_dialog");
            el.scrollTop("0");

            // Calcular tamaños.
            var vSize = viewportSize();
            var eSize = elementSize(el);

            // Aplicar clase de ajuste si es necesario.
            if (eSize.h >= vSize.h) {
                el.addClass("app_fit_dialog");
            }

            dialog.initPosition();
        }
    } catch (e) {
        console.error(e);
    }
}

/**
 * Ajusta un cuadro de dialogo al momento que es ocultado en la vista.
 * @param {type} el Elemento DOM.
 */
function ajustarDialogoCierre(el) {
    try {
        // Permitir que se pueda hacer scroll sobre el documento.
        $('body').css('position', '');
    } catch (e) {
        console.error(e);
    }
}

/**
 * Utilizado cuando se hace foco sobre un elemento de entrada de texto.
 * @param {type} el Elemento DOM.
 */
function inputNumericOnFocus(el) {
    var temp_value = el.value;
    el.value = '';
    el.value = temp_value;
}

/**
 * Permite fijar la posicion de la barra lateral de opciones que normalmente acompaña a las listas de datos.
 * @param {type} el Elemento de barra lateral.
 */
function stick(el) {
    var o = $(el).offset();
    var w = $(el).width();
    $(el).css('position', 'fixed');
    $(el).css('top', 20);
    $(el).css('left', o.left);
    $(el).css('width', w);
}

/**
 * Permite deshacer los cambios realizados por la operacion 'stick'.
 * @param {type} el Elemento de barra lateral.
 */
function unstick(el) {
    $(el).css('position', 'relative');
    $(el).css('top', 'auto');
    $(el).css('left', 'auto');
    $(el).css('width', 'auto');
}

/**
 * Permite iniciar el monitoreo de scroll de la pagina a manera que cuando la lista de datos supere el alto
 * total disponible, la barra lateral de operaciones acompañe al movimiento de scroll para que sea accesible
 * al usuario.
 * @param {type} source Elemento de lista de datos.
 * @param {type} target Elemento de barra lateral.
 */
function addStickyOperacionesBehavior(source, target) {
    window.addEventListener('scroll', function (ev) {
        var distanceToTop = source.getBoundingClientRect().top;
        if (distanceToTop <= -20) {
            stick(target);
        } else {
            unstick(target);
        }
    });
}