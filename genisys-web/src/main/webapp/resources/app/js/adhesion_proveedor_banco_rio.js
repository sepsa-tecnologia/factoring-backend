function getFile(file) {
    var reader = new FileReader();
    return new Promise((resolve, reject) => {
        reader.onerror = function () {
            reader.abort();
            reject(new Error());
        };
        reader.onload = function () {
            resolve({
                fileContent: reader.result.split(',').pop(),
                fileName: file.name,
                fileType: file.type
            });
        };
        reader.readAsDataURL(file);
    });
}

function handleFileInput(fileInputId, fileInputHiddenValueId) {
    var fi = document.getElementById(fileInputId);
    $(fi).on('change', function (e) {
        var file = e.target.files[0];
        if (file !== null && file !== undefined) {
            $(e.target).prop('disabled', true);
            var p = getFile(file);
            p.then(function (r) {
                var fih = document.getElementById(fileInputHiddenValueId);
                $(fih).val(JSON.stringify(r));
            });
            p.finally(function () {
                $(e.target).prop('disabled', false);
            });
        }
    });
}