import os, sys
import urllib

def read_file(filename):
	file = open(filename, 'r')
	lines = [line.strip() for line in file]
	file.close()
	return lines

if __name__ == '__main__':
	FILE = sys.argv[1]
	lines = read_file(FILE)
	for line in lines:
		name = line.split('?')[0].split('/')[6].split('xhtml')[0]
		print 'downloading %s' % (name)
		urllib.urlretrieve (line, name)