
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SimuladorResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SimuladorResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InfoRequest" type="{http://tempuri.org/}SimuladorRequest" minOccurs="0"/&gt;
 *         &lt;element name="SimuladorCalculo" type="{http://tempuri.org/}SimuladorCalculo" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimuladorResponse", propOrder = {
    "infoRequest",
    "simuladorCalculo"
})
public class SimuladorResponse
    extends BaseResponse
{

    @XmlElement(name = "InfoRequest")
    protected SimuladorRequest infoRequest;
    @XmlElement(name = "SimuladorCalculo")
    protected SimuladorCalculo simuladorCalculo;

    /**
     * Obtiene el valor de la propiedad infoRequest.
     * 
     * @return
     *     possible object is
     *     {@link SimuladorRequest }
     *     
     */
    public SimuladorRequest getInfoRequest() {
        return infoRequest;
    }

    /**
     * Define el valor de la propiedad infoRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link SimuladorRequest }
     *     
     */
    public void setInfoRequest(SimuladorRequest value) {
        this.infoRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad simuladorCalculo.
     * 
     * @return
     *     possible object is
     *     {@link SimuladorCalculo }
     *     
     */
    public SimuladorCalculo getSimuladorCalculo() {
        return simuladorCalculo;
    }

    /**
     * Define el valor de la propiedad simuladorCalculo.
     * 
     * @param value
     *     allowed object is
     *     {@link SimuladorCalculo }
     *     
     */
    public void setSimuladorCalculo(SimuladorCalculo value) {
        this.simuladorCalculo = value;
    }

}
