
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AltaCompradorProveedorResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AltaCompradorProveedorResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CodCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InfoPersonaBcp" type="{http://tempuri.org/}InfoCrc" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AltaCompradorProveedorResponse", propOrder = {
    "codCliente",
    "infoPersonaBcp"
})
public class AltaCompradorProveedorResponse
    extends BaseResponse
{

    @XmlElement(name = "CodCliente")
    protected String codCliente;
    @XmlElement(name = "InfoPersonaBcp")
    protected InfoCrc infoPersonaBcp;

    /**
     * Obtiene el valor de la propiedad codCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCliente() {
        return codCliente;
    }

    /**
     * Define el valor de la propiedad codCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCliente(String value) {
        this.codCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad infoPersonaBcp.
     * 
     * @return
     *     possible object is
     *     {@link InfoCrc }
     *     
     */
    public InfoCrc getInfoPersonaBcp() {
        return infoPersonaBcp;
    }

    /**
     * Define el valor de la propiedad infoPersonaBcp.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoCrc }
     *     
     */
    public void setInfoPersonaBcp(InfoCrc value) {
        this.infoPersonaBcp = value;
    }

}
