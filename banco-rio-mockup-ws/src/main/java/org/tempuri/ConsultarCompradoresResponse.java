
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultarCompradoresResult" type="{http://tempuri.org/}ConsultaCompradoresResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarCompradoresResult"
})
@XmlRootElement(name = "ConsultarCompradoresResponse")
public class ConsultarCompradoresResponse {

    @XmlElement(name = "ConsultarCompradoresResult")
    protected ConsultaCompradoresResponse consultarCompradoresResult;

    /**
     * Obtiene el valor de la propiedad consultarCompradoresResult.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaCompradoresResponse }
     *     
     */
    public ConsultaCompradoresResponse getConsultarCompradoresResult() {
        return consultarCompradoresResult;
    }

    /**
     * Define el valor de la propiedad consultarCompradoresResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaCompradoresResponse }
     *     
     */
    public void setConsultarCompradoresResult(ConsultaCompradoresResponse value) {
        this.consultarCompradoresResult = value;
    }

}
