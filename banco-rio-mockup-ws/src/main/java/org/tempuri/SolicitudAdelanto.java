
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SolicitudAdelanto complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SolicitudAdelanto"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdSolicitud" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="Comprador" type="{http://tempuri.org/}Comprador" minOccurs="0"/&gt;
 *         &lt;element name="Proveedor" type="{http://tempuri.org/}Proveedor" minOccurs="0"/&gt;
 *         &lt;element name="InfoCuentaAcreditacion" type="{http://tempuri.org/}InfoCuentaAcreditacion" minOccurs="0"/&gt;
 *         &lt;element name="Tasa" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="MontoLineaDisponible" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="MontoTotalFacturas" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="MontoNetoFacturas" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="PorcentajeGastosAdm" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="PorcentajeUsoPlataforma" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="NroOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaInsercion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaDecision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ComentarioDecision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TasaAnualNominal" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="TasaAnualEfectiva" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="DetalleFacturas" type="{http://tempuri.org/}ArrayOfSolicitudAdelantoDetalle" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SolicitudAdelanto", propOrder = {
    "idSolicitud",
    "comprador",
    "proveedor",
    "infoCuentaAcreditacion",
    "tasa",
    "montoLineaDisponible",
    "montoTotalFacturas",
    "montoNetoFacturas",
    "porcentajeGastosAdm",
    "porcentajeUsoPlataforma",
    "nroOperacion",
    "fechaInsercion",
    "fechaDecision",
    "comentarioDecision",
    "estado",
    "tasaAnualNominal",
    "tasaAnualEfectiva",
    "detalleFacturas"
})
public class SolicitudAdelanto {

    @XmlElement(name = "IdSolicitud")
    protected long idSolicitud;
    @XmlElement(name = "Comprador")
    protected Comprador comprador;
    @XmlElement(name = "Proveedor")
    protected Proveedor proveedor;
    @XmlElement(name = "InfoCuentaAcreditacion")
    protected InfoCuentaAcreditacion infoCuentaAcreditacion;
    @XmlElement(name = "Tasa")
    protected double tasa;
    @XmlElement(name = "MontoLineaDisponible")
    protected double montoLineaDisponible;
    @XmlElement(name = "MontoTotalFacturas")
    protected double montoTotalFacturas;
    @XmlElement(name = "MontoNetoFacturas")
    protected double montoNetoFacturas;
    @XmlElement(name = "PorcentajeGastosAdm")
    protected double porcentajeGastosAdm;
    @XmlElement(name = "PorcentajeUsoPlataforma")
    protected double porcentajeUsoPlataforma;
    @XmlElement(name = "NroOperacion")
    protected String nroOperacion;
    @XmlElement(name = "FechaInsercion")
    protected String fechaInsercion;
    @XmlElement(name = "FechaDecision")
    protected String fechaDecision;
    @XmlElement(name = "ComentarioDecision")
    protected String comentarioDecision;
    @XmlElement(name = "Estado")
    protected String estado;
    @XmlElement(name = "TasaAnualNominal")
    protected double tasaAnualNominal;
    @XmlElement(name = "TasaAnualEfectiva")
    protected double tasaAnualEfectiva;
    @XmlElement(name = "DetalleFacturas")
    protected ArrayOfSolicitudAdelantoDetalle detalleFacturas;

    /**
     * Obtiene el valor de la propiedad idSolicitud.
     * 
     */
    public long getIdSolicitud() {
        return idSolicitud;
    }

    /**
     * Define el valor de la propiedad idSolicitud.
     * 
     */
    public void setIdSolicitud(long value) {
        this.idSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad comprador.
     * 
     * @return
     *     possible object is
     *     {@link Comprador }
     *     
     */
    public Comprador getComprador() {
        return comprador;
    }

    /**
     * Define el valor de la propiedad comprador.
     * 
     * @param value
     *     allowed object is
     *     {@link Comprador }
     *     
     */
    public void setComprador(Comprador value) {
        this.comprador = value;
    }

    /**
     * Obtiene el valor de la propiedad proveedor.
     * 
     * @return
     *     possible object is
     *     {@link Proveedor }
     *     
     */
    public Proveedor getProveedor() {
        return proveedor;
    }

    /**
     * Define el valor de la propiedad proveedor.
     * 
     * @param value
     *     allowed object is
     *     {@link Proveedor }
     *     
     */
    public void setProveedor(Proveedor value) {
        this.proveedor = value;
    }

    /**
     * Obtiene el valor de la propiedad infoCuentaAcreditacion.
     * 
     * @return
     *     possible object is
     *     {@link InfoCuentaAcreditacion }
     *     
     */
    public InfoCuentaAcreditacion getInfoCuentaAcreditacion() {
        return infoCuentaAcreditacion;
    }

    /**
     * Define el valor de la propiedad infoCuentaAcreditacion.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoCuentaAcreditacion }
     *     
     */
    public void setInfoCuentaAcreditacion(InfoCuentaAcreditacion value) {
        this.infoCuentaAcreditacion = value;
    }

    /**
     * Obtiene el valor de la propiedad tasa.
     * 
     */
    public double getTasa() {
        return tasa;
    }

    /**
     * Define el valor de la propiedad tasa.
     * 
     */
    public void setTasa(double value) {
        this.tasa = value;
    }

    /**
     * Obtiene el valor de la propiedad montoLineaDisponible.
     * 
     */
    public double getMontoLineaDisponible() {
        return montoLineaDisponible;
    }

    /**
     * Define el valor de la propiedad montoLineaDisponible.
     * 
     */
    public void setMontoLineaDisponible(double value) {
        this.montoLineaDisponible = value;
    }

    /**
     * Obtiene el valor de la propiedad montoTotalFacturas.
     * 
     */
    public double getMontoTotalFacturas() {
        return montoTotalFacturas;
    }

    /**
     * Define el valor de la propiedad montoTotalFacturas.
     * 
     */
    public void setMontoTotalFacturas(double value) {
        this.montoTotalFacturas = value;
    }

    /**
     * Obtiene el valor de la propiedad montoNetoFacturas.
     * 
     */
    public double getMontoNetoFacturas() {
        return montoNetoFacturas;
    }

    /**
     * Define el valor de la propiedad montoNetoFacturas.
     * 
     */
    public void setMontoNetoFacturas(double value) {
        this.montoNetoFacturas = value;
    }

    /**
     * Obtiene el valor de la propiedad porcentajeGastosAdm.
     * 
     */
    public double getPorcentajeGastosAdm() {
        return porcentajeGastosAdm;
    }

    /**
     * Define el valor de la propiedad porcentajeGastosAdm.
     * 
     */
    public void setPorcentajeGastosAdm(double value) {
        this.porcentajeGastosAdm = value;
    }

    /**
     * Obtiene el valor de la propiedad porcentajeUsoPlataforma.
     * 
     */
    public double getPorcentajeUsoPlataforma() {
        return porcentajeUsoPlataforma;
    }

    /**
     * Define el valor de la propiedad porcentajeUsoPlataforma.
     * 
     */
    public void setPorcentajeUsoPlataforma(double value) {
        this.porcentajeUsoPlataforma = value;
    }

    /**
     * Obtiene el valor de la propiedad nroOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroOperacion() {
        return nroOperacion;
    }

    /**
     * Define el valor de la propiedad nroOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroOperacion(String value) {
        this.nroOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInsercion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaInsercion() {
        return fechaInsercion;
    }

    /**
     * Define el valor de la propiedad fechaInsercion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaInsercion(String value) {
        this.fechaInsercion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaDecision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaDecision() {
        return fechaDecision;
    }

    /**
     * Define el valor de la propiedad fechaDecision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaDecision(String value) {
        this.fechaDecision = value;
    }

    /**
     * Obtiene el valor de la propiedad comentarioDecision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComentarioDecision() {
        return comentarioDecision;
    }

    /**
     * Define el valor de la propiedad comentarioDecision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComentarioDecision(String value) {
        this.comentarioDecision = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad tasaAnualNominal.
     * 
     */
    public double getTasaAnualNominal() {
        return tasaAnualNominal;
    }

    /**
     * Define el valor de la propiedad tasaAnualNominal.
     * 
     */
    public void setTasaAnualNominal(double value) {
        this.tasaAnualNominal = value;
    }

    /**
     * Obtiene el valor de la propiedad tasaAnualEfectiva.
     * 
     */
    public double getTasaAnualEfectiva() {
        return tasaAnualEfectiva;
    }

    /**
     * Define el valor de la propiedad tasaAnualEfectiva.
     * 
     */
    public void setTasaAnualEfectiva(double value) {
        this.tasaAnualEfectiva = value;
    }

    /**
     * Obtiene el valor de la propiedad detalleFacturas.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSolicitudAdelantoDetalle }
     *     
     */
    public ArrayOfSolicitudAdelantoDetalle getDetalleFacturas() {
        return detalleFacturas;
    }

    /**
     * Define el valor de la propiedad detalleFacturas.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSolicitudAdelantoDetalle }
     *     
     */
    public void setDetalleFacturas(ArrayOfSolicitudAdelantoDetalle value) {
        this.detalleFacturas = value;
    }

}
