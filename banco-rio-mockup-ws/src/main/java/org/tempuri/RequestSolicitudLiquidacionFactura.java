
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequestSolicitudLiquidacionFactura complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestSolicitudLiquidacionFactura"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Comprador" type="{http://tempuri.org/}Comprador" minOccurs="0"/&gt;
 *         &lt;element name="InfoComisionComprador" type="{http://tempuri.org/}InfoComisionComprador" minOccurs="0"/&gt;
 *         &lt;element name="InfoComisionBanco" type="{http://tempuri.org/}InfoComisionBanco" minOccurs="0"/&gt;
 *         &lt;element name="InfoComisionSepsa" type="{http://tempuri.org/}InfoComisionSepsa" minOccurs="0"/&gt;
 *         &lt;element name="ListaFacturasALiquidar" type="{http://tempuri.org/}ArrayOfInfoFacturaALiquidar" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestSolicitudLiquidacionFactura", propOrder = {
    "comprador",
    "infoComisionComprador",
    "infoComisionBanco",
    "infoComisionSepsa",
    "listaFacturasALiquidar"
})
public class RequestSolicitudLiquidacionFactura {

    @XmlElement(name = "Comprador")
    protected Comprador comprador;
    @XmlElement(name = "InfoComisionComprador")
    protected InfoComisionComprador infoComisionComprador;
    @XmlElement(name = "InfoComisionBanco")
    protected InfoComisionBanco infoComisionBanco;
    @XmlElement(name = "InfoComisionSepsa")
    protected InfoComisionSepsa infoComisionSepsa;
    @XmlElement(name = "ListaFacturasALiquidar")
    protected ArrayOfInfoFacturaALiquidar listaFacturasALiquidar;

    /**
     * Obtiene el valor de la propiedad comprador.
     * 
     * @return
     *     possible object is
     *     {@link Comprador }
     *     
     */
    public Comprador getComprador() {
        return comprador;
    }

    /**
     * Define el valor de la propiedad comprador.
     * 
     * @param value
     *     allowed object is
     *     {@link Comprador }
     *     
     */
    public void setComprador(Comprador value) {
        this.comprador = value;
    }

    /**
     * Obtiene el valor de la propiedad infoComisionComprador.
     * 
     * @return
     *     possible object is
     *     {@link InfoComisionComprador }
     *     
     */
    public InfoComisionComprador getInfoComisionComprador() {
        return infoComisionComprador;
    }

    /**
     * Define el valor de la propiedad infoComisionComprador.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoComisionComprador }
     *     
     */
    public void setInfoComisionComprador(InfoComisionComprador value) {
        this.infoComisionComprador = value;
    }

    /**
     * Obtiene el valor de la propiedad infoComisionBanco.
     * 
     * @return
     *     possible object is
     *     {@link InfoComisionBanco }
     *     
     */
    public InfoComisionBanco getInfoComisionBanco() {
        return infoComisionBanco;
    }

    /**
     * Define el valor de la propiedad infoComisionBanco.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoComisionBanco }
     *     
     */
    public void setInfoComisionBanco(InfoComisionBanco value) {
        this.infoComisionBanco = value;
    }

    /**
     * Obtiene el valor de la propiedad infoComisionSepsa.
     * 
     * @return
     *     possible object is
     *     {@link InfoComisionSepsa }
     *     
     */
    public InfoComisionSepsa getInfoComisionSepsa() {
        return infoComisionSepsa;
    }

    /**
     * Define el valor de la propiedad infoComisionSepsa.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoComisionSepsa }
     *     
     */
    public void setInfoComisionSepsa(InfoComisionSepsa value) {
        this.infoComisionSepsa = value;
    }

    /**
     * Obtiene el valor de la propiedad listaFacturasALiquidar.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInfoFacturaALiquidar }
     *     
     */
    public ArrayOfInfoFacturaALiquidar getListaFacturasALiquidar() {
        return listaFacturasALiquidar;
    }

    /**
     * Define el valor de la propiedad listaFacturasALiquidar.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInfoFacturaALiquidar }
     *     
     */
    public void setListaFacturasALiquidar(ArrayOfInfoFacturaALiquidar value) {
        this.listaFacturasALiquidar = value;
    }

}
