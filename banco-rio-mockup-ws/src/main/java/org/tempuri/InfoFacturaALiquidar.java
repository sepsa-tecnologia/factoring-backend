
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para InfoFacturaALiquidar complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InfoFacturaALiquidar"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="NroOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IdSolicitud" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="NroFactura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaVto" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="Monto" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoFacturaALiquidar", propOrder = {
    "nroOperacion",
    "idSolicitud",
    "nroFactura",
    "fechaVto",
    "monto"
})
public class InfoFacturaALiquidar {

    @XmlElement(name = "NroOperacion")
    protected String nroOperacion;
    @XmlElement(name = "IdSolicitud")
    protected long idSolicitud;
    @XmlElement(name = "NroFactura")
    protected String nroFactura;
    @XmlElement(name = "FechaVto", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaVto;
    @XmlElement(name = "Monto")
    protected double monto;

    /**
     * Obtiene el valor de la propiedad nroOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroOperacion() {
        return nroOperacion;
    }

    /**
     * Define el valor de la propiedad nroOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroOperacion(String value) {
        this.nroOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idSolicitud.
     * 
     */
    public long getIdSolicitud() {
        return idSolicitud;
    }

    /**
     * Define el valor de la propiedad idSolicitud.
     * 
     */
    public void setIdSolicitud(long value) {
        this.idSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad nroFactura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroFactura() {
        return nroFactura;
    }

    /**
     * Define el valor de la propiedad nroFactura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroFactura(String value) {
        this.nroFactura = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVto.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaVto() {
        return fechaVto;
    }

    /**
     * Define el valor de la propiedad fechaVto.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaVto(XMLGregorianCalendar value) {
        this.fechaVto = value;
    }

    /**
     * Obtiene el valor de la propiedad monto.
     * 
     */
    public double getMonto() {
        return monto;
    }

    /**
     * Define el valor de la propiedad monto.
     * 
     */
    public void setMonto(double value) {
        this.monto = value;
    }

}
