
package org.tempuri;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ReferenciaDireccion.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <pre>
 * &lt;simpleType name="ReferenciaDireccion"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CASI"/&gt;
 *     &lt;enumeration value="ENTRE"/&gt;
 *     &lt;enumeration value="Y"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ReferenciaDireccion")
@XmlEnum
public enum ReferenciaDireccion {

    CASI,
    ENTRE,
    Y;

    public String value() {
        return name();
    }

    public static ReferenciaDireccion fromValue(String v) {
        return valueOf(v);
    }

}
