
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AltaProveedorJuridicoResult" type="{http://tempuri.org/}AltaCompradorProveedorResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "altaProveedorJuridicoResult"
})
@XmlRootElement(name = "AltaProveedorJuridicoResponse")
public class AltaProveedorJuridicoResponse {

    @XmlElement(name = "AltaProveedorJuridicoResult")
    protected AltaCompradorProveedorResponse altaProveedorJuridicoResult;

    /**
     * Obtiene el valor de la propiedad altaProveedorJuridicoResult.
     * 
     * @return
     *     possible object is
     *     {@link AltaCompradorProveedorResponse }
     *     
     */
    public AltaCompradorProveedorResponse getAltaProveedorJuridicoResult() {
        return altaProveedorJuridicoResult;
    }

    /**
     * Define el valor de la propiedad altaProveedorJuridicoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link AltaCompradorProveedorResponse }
     *     
     */
    public void setAltaProveedorJuridicoResult(AltaCompradorProveedorResponse value) {
        this.altaProveedorJuridicoResult = value;
    }

}
