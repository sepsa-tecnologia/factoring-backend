
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SolicitarLiquidacionDeFacturasResult" type="{http://tempuri.org/}ResponseSolicitudLiquidacionFactura" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "solicitarLiquidacionDeFacturasResult"
})
@XmlRootElement(name = "SolicitarLiquidacionDeFacturasResponse")
public class SolicitarLiquidacionDeFacturasResponse {

    @XmlElement(name = "SolicitarLiquidacionDeFacturasResult")
    protected ResponseSolicitudLiquidacionFactura solicitarLiquidacionDeFacturasResult;

    /**
     * Obtiene el valor de la propiedad solicitarLiquidacionDeFacturasResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponseSolicitudLiquidacionFactura }
     *     
     */
    public ResponseSolicitudLiquidacionFactura getSolicitarLiquidacionDeFacturasResult() {
        return solicitarLiquidacionDeFacturasResult;
    }

    /**
     * Define el valor de la propiedad solicitarLiquidacionDeFacturasResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseSolicitudLiquidacionFactura }
     *     
     */
    public void setSolicitarLiquidacionDeFacturasResult(ResponseSolicitudLiquidacionFactura value) {
        this.solicitarLiquidacionDeFacturasResult = value;
    }

}
