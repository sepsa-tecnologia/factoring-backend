
package org.tempuri;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ESTADO.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <pre>
 * &lt;simpleType name="ESTADO"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="PENDIENTE"/&gt;
 *     &lt;enumeration value="APROBADO"/&gt;
 *     &lt;enumeration value="RECHAZADO"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ESTADO")
@XmlEnum
public enum ESTADO {

    PENDIENTE,
    APROBADO,
    RECHAZADO;

    public String value() {
        return name();
    }

    public static ESTADO fromValue(String v) {
        return valueOf(v);
    }

}
