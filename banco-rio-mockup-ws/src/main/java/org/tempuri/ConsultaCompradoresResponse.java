
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultaCompradoresResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultaCompradoresResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Compradores" type="{http://tempuri.org/}ArrayOfComprador" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaCompradoresResponse", propOrder = {
    "compradores"
})
public class ConsultaCompradoresResponse
    extends BaseResponse
{

    @XmlElement(name = "Compradores")
    protected ArrayOfComprador compradores;

    /**
     * Obtiene el valor de la propiedad compradores.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfComprador }
     *     
     */
    public ArrayOfComprador getCompradores() {
        return compradores;
    }

    /**
     * Define el valor de la propiedad compradores.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfComprador }
     *     
     */
    public void setCompradores(ArrayOfComprador value) {
        this.compradores = value;
    }

}
