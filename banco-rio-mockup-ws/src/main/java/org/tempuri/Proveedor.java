
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Proveedor complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Proveedor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}Persona"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ListaCuentas" type="{http://tempuri.org/}ArrayOfProveedorCuentas" minOccurs="0"/&gt;
 *         &lt;element name="InfoCatastroCuentaAcreditacion" type="{http://tempuri.org/}InfoCuentaAcreditacion" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Proveedor", propOrder = {
    "listaCuentas",
    "infoCatastroCuentaAcreditacion"
})
public class Proveedor
    extends Persona
{

    @XmlElement(name = "ListaCuentas")
    protected ArrayOfProveedorCuentas listaCuentas;
    @XmlElement(name = "InfoCatastroCuentaAcreditacion")
    protected InfoCuentaAcreditacion infoCatastroCuentaAcreditacion;

    /**
     * Obtiene el valor de la propiedad listaCuentas.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProveedorCuentas }
     *     
     */
    public ArrayOfProveedorCuentas getListaCuentas() {
        return listaCuentas;
    }

    /**
     * Define el valor de la propiedad listaCuentas.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProveedorCuentas }
     *     
     */
    public void setListaCuentas(ArrayOfProveedorCuentas value) {
        this.listaCuentas = value;
    }

    /**
     * Obtiene el valor de la propiedad infoCatastroCuentaAcreditacion.
     * 
     * @return
     *     possible object is
     *     {@link InfoCuentaAcreditacion }
     *     
     */
    public InfoCuentaAcreditacion getInfoCatastroCuentaAcreditacion() {
        return infoCatastroCuentaAcreditacion;
    }

    /**
     * Define el valor de la propiedad infoCatastroCuentaAcreditacion.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoCuentaAcreditacion }
     *     
     */
    public void setInfoCatastroCuentaAcreditacion(InfoCuentaAcreditacion value) {
        this.infoCatastroCuentaAcreditacion = value;
    }

}
