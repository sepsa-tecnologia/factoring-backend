
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SolicitarAdelantoDeFacturasResult" type="{http://tempuri.org/}SolicitudAdelantoResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "solicitarAdelantoDeFacturasResult"
})
@XmlRootElement(name = "SolicitarAdelantoDeFacturasResponse")
public class SolicitarAdelantoDeFacturasResponse {

    @XmlElement(name = "SolicitarAdelantoDeFacturasResult")
    protected SolicitudAdelantoResponse solicitarAdelantoDeFacturasResult;

    /**
     * Obtiene el valor de la propiedad solicitarAdelantoDeFacturasResult.
     * 
     * @return
     *     possible object is
     *     {@link SolicitudAdelantoResponse }
     *     
     */
    public SolicitudAdelantoResponse getSolicitarAdelantoDeFacturasResult() {
        return solicitarAdelantoDeFacturasResult;
    }

    /**
     * Define el valor de la propiedad solicitarAdelantoDeFacturasResult.
     * 
     * @param value
     *     allowed object is
     *     {@link SolicitudAdelantoResponse }
     *     
     */
    public void setSolicitarAdelantoDeFacturasResult(SolicitudAdelantoResponse value) {
        this.solicitarAdelantoDeFacturasResult = value;
    }

}
