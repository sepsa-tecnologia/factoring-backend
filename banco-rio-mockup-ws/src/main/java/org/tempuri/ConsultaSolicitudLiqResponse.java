
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultaSolicitudLiqResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultaSolicitudLiqResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Request" type="{http://tempuri.org/}ConsultaSolicitudLiqRequest" minOccurs="0"/&gt;
 *         &lt;element name="InfoSolicitud" type="{http://tempuri.org/}InfoSolicitudLiquidacion" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaSolicitudLiqResponse", propOrder = {
    "request",
    "infoSolicitud"
})
public class ConsultaSolicitudLiqResponse
    extends BaseResponse
{

    @XmlElement(name = "Request")
    protected ConsultaSolicitudLiqRequest request;
    @XmlElement(name = "InfoSolicitud")
    protected InfoSolicitudLiquidacion infoSolicitud;

    /**
     * Obtiene el valor de la propiedad request.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaSolicitudLiqRequest }
     *     
     */
    public ConsultaSolicitudLiqRequest getRequest() {
        return request;
    }

    /**
     * Define el valor de la propiedad request.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaSolicitudLiqRequest }
     *     
     */
    public void setRequest(ConsultaSolicitudLiqRequest value) {
        this.request = value;
    }

    /**
     * Obtiene el valor de la propiedad infoSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link InfoSolicitudLiquidacion }
     *     
     */
    public InfoSolicitudLiquidacion getInfoSolicitud() {
        return infoSolicitud;
    }

    /**
     * Define el valor de la propiedad infoSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoSolicitudLiquidacion }
     *     
     */
    public void setInfoSolicitud(InfoSolicitudLiquidacion value) {
        this.infoSolicitud = value;
    }

}
