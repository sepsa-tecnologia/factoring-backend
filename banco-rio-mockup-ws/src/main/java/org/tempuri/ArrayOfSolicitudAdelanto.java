
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfSolicitudAdelanto complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSolicitudAdelanto"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SolicitudAdelanto" type="{http://tempuri.org/}SolicitudAdelanto" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSolicitudAdelanto", propOrder = {
    "solicitudAdelanto"
})
public class ArrayOfSolicitudAdelanto {

    @XmlElement(name = "SolicitudAdelanto", nillable = true)
    protected List<SolicitudAdelanto> solicitudAdelanto;

    /**
     * Gets the value of the solicitudAdelanto property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the solicitudAdelanto property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSolicitudAdelanto().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SolicitudAdelanto }
     * 
     * 
     */
    public List<SolicitudAdelanto> getSolicitudAdelanto() {
        if (solicitudAdelanto == null) {
            solicitudAdelanto = new ArrayList<SolicitudAdelanto>();
        }
        return this.solicitudAdelanto;
    }

}
