
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Parametros complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Parametros"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ListaActividadesEconomicas" type="{http://tempuri.org/}ArrayOfActividadEconomica" minOccurs="0"/&gt;
 *         &lt;element name="ListaTiposSociedad" type="{http://tempuri.org/}ArrayOfTipoSociedad" minOccurs="0"/&gt;
 *         &lt;element name="ListaTipoDocumentos" type="{http://tempuri.org/}ArrayOfTipoDocumento" minOccurs="0"/&gt;
 *         &lt;element name="ListaRegiones" type="{http://tempuri.org/}ArrayOfRegion" minOccurs="0"/&gt;
 *         &lt;element name="ListaCiudades" type="{http://tempuri.org/}ArrayOfCiudad" minOccurs="0"/&gt;
 *         &lt;element name="ListaBancos" type="{http://tempuri.org/}ArrayOfBanco" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Parametros", propOrder = {
    "listaActividadesEconomicas",
    "listaTiposSociedad",
    "listaTipoDocumentos",
    "listaRegiones",
    "listaCiudades",
    "listaBancos"
})
public class Parametros {

    @XmlElement(name = "ListaActividadesEconomicas")
    protected ArrayOfActividadEconomica listaActividadesEconomicas;
    @XmlElement(name = "ListaTiposSociedad")
    protected ArrayOfTipoSociedad listaTiposSociedad;
    @XmlElement(name = "ListaTipoDocumentos")
    protected ArrayOfTipoDocumento listaTipoDocumentos;
    @XmlElement(name = "ListaRegiones")
    protected ArrayOfRegion listaRegiones;
    @XmlElement(name = "ListaCiudades")
    protected ArrayOfCiudad listaCiudades;
    @XmlElement(name = "ListaBancos")
    protected ArrayOfBanco listaBancos;

    /**
     * Obtiene el valor de la propiedad listaActividadesEconomicas.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfActividadEconomica }
     *     
     */
    public ArrayOfActividadEconomica getListaActividadesEconomicas() {
        return listaActividadesEconomicas;
    }

    /**
     * Define el valor de la propiedad listaActividadesEconomicas.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfActividadEconomica }
     *     
     */
    public void setListaActividadesEconomicas(ArrayOfActividadEconomica value) {
        this.listaActividadesEconomicas = value;
    }

    /**
     * Obtiene el valor de la propiedad listaTiposSociedad.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTipoSociedad }
     *     
     */
    public ArrayOfTipoSociedad getListaTiposSociedad() {
        return listaTiposSociedad;
    }

    /**
     * Define el valor de la propiedad listaTiposSociedad.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTipoSociedad }
     *     
     */
    public void setListaTiposSociedad(ArrayOfTipoSociedad value) {
        this.listaTiposSociedad = value;
    }

    /**
     * Obtiene el valor de la propiedad listaTipoDocumentos.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTipoDocumento }
     *     
     */
    public ArrayOfTipoDocumento getListaTipoDocumentos() {
        return listaTipoDocumentos;
    }

    /**
     * Define el valor de la propiedad listaTipoDocumentos.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTipoDocumento }
     *     
     */
    public void setListaTipoDocumentos(ArrayOfTipoDocumento value) {
        this.listaTipoDocumentos = value;
    }

    /**
     * Obtiene el valor de la propiedad listaRegiones.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRegion }
     *     
     */
    public ArrayOfRegion getListaRegiones() {
        return listaRegiones;
    }

    /**
     * Define el valor de la propiedad listaRegiones.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRegion }
     *     
     */
    public void setListaRegiones(ArrayOfRegion value) {
        this.listaRegiones = value;
    }

    /**
     * Obtiene el valor de la propiedad listaCiudades.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCiudad }
     *     
     */
    public ArrayOfCiudad getListaCiudades() {
        return listaCiudades;
    }

    /**
     * Define el valor de la propiedad listaCiudades.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCiudad }
     *     
     */
    public void setListaCiudades(ArrayOfCiudad value) {
        this.listaCiudades = value;
    }

    /**
     * Obtiene el valor de la propiedad listaBancos.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBanco }
     *     
     */
    public ArrayOfBanco getListaBancos() {
        return listaBancos;
    }

    /**
     * Define el valor de la propiedad listaBancos.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBanco }
     *     
     */
    public void setListaBancos(ArrayOfBanco value) {
        this.listaBancos = value;
    }

}
