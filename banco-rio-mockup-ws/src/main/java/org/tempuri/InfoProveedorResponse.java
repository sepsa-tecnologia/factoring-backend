
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para InfoProveedorResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InfoProveedorResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Request" type="{http://tempuri.org/}InfoProveedorRequest" minOccurs="0"/&gt;
 *         &lt;element name="InfoProveedor" type="{http://tempuri.org/}Proveedor" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoProveedorResponse", propOrder = {
    "request",
    "infoProveedor"
})
public class InfoProveedorResponse
    extends BaseResponse
{

    @XmlElement(name = "Request")
    protected InfoProveedorRequest request;
    @XmlElement(name = "InfoProveedor")
    protected Proveedor infoProveedor;

    /**
     * Obtiene el valor de la propiedad request.
     * 
     * @return
     *     possible object is
     *     {@link InfoProveedorRequest }
     *     
     */
    public InfoProveedorRequest getRequest() {
        return request;
    }

    /**
     * Define el valor de la propiedad request.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoProveedorRequest }
     *     
     */
    public void setRequest(InfoProveedorRequest value) {
        this.request = value;
    }

    /**
     * Obtiene el valor de la propiedad infoProveedor.
     * 
     * @return
     *     possible object is
     *     {@link Proveedor }
     *     
     */
    public Proveedor getInfoProveedor() {
        return infoProveedor;
    }

    /**
     * Define el valor de la propiedad infoProveedor.
     * 
     * @param value
     *     allowed object is
     *     {@link Proveedor }
     *     
     */
    public void setInfoProveedor(Proveedor value) {
        this.infoProveedor = value;
    }

}
