package org.tempuri;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

/**
 * This class was generated by Apache CXF 3.5.6
 * 2023-06-27T11:50:03.960-04:00
 * Generated source version: 3.5.6
 *
 */
@WebServiceClient(name = "wsFactoring",
                  wsdlLocation = "https://www.rio.com.py/secure/sepsa/webService.asmx?WSDL",
                  targetNamespace = "http://tempuri.org/")
public class WsFactoring extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://tempuri.org/", "wsFactoring");
    public final static QName WsFactoringSoap = new QName("http://tempuri.org/", "wsFactoringSoap");
    public final static QName WsFactoringSoap12 = new QName("http://tempuri.org/", "wsFactoringSoap12");
    static {
        URL url = null;
        try {
            url = new URL("https://www.rio.com.py/secure/sepsa/webService.asmx?WSDL");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(WsFactoring.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "https://www.rio.com.py/secure/sepsa/webService.asmx?WSDL");
        }
        WSDL_LOCATION = url;
    }

    public WsFactoring(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public WsFactoring(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public WsFactoring() {
        super(WSDL_LOCATION, SERVICE);
    }

    public WsFactoring(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public WsFactoring(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public WsFactoring(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns WsFactoringSoap
     */
    @WebEndpoint(name = "wsFactoringSoap")
    public WsFactoringSoap getWsFactoringSoap() {
        return super.getPort(WsFactoringSoap, WsFactoringSoap.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns WsFactoringSoap
     */
    @WebEndpoint(name = "wsFactoringSoap")
    public WsFactoringSoap getWsFactoringSoap(WebServiceFeature... features) {
        return super.getPort(WsFactoringSoap, WsFactoringSoap.class, features);
    }


    /**
     *
     * @return
     *     returns WsFactoringSoap
     */
    @WebEndpoint(name = "wsFactoringSoap12")
    public WsFactoringSoap getWsFactoringSoap12() {
        return super.getPort(WsFactoringSoap12, WsFactoringSoap.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns WsFactoringSoap
     */
    @WebEndpoint(name = "wsFactoringSoap12")
    public WsFactoringSoap getWsFactoringSoap12(WebServiceFeature... features) {
        return super.getPort(WsFactoringSoap12, WsFactoringSoap.class, features);
    }

}
