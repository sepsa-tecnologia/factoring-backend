
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultarSolicitudResult" type="{http://tempuri.org/}ConsultaInfoSolicitudResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarSolicitudResult"
})
@XmlRootElement(name = "ConsultarSolicitudResponse")
public class ConsultarSolicitudResponse {

    @XmlElement(name = "ConsultarSolicitudResult")
    protected ConsultaInfoSolicitudResponse consultarSolicitudResult;

    /**
     * Obtiene el valor de la propiedad consultarSolicitudResult.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaInfoSolicitudResponse }
     *     
     */
    public ConsultaInfoSolicitudResponse getConsultarSolicitudResult() {
        return consultarSolicitudResult;
    }

    /**
     * Define el valor de la propiedad consultarSolicitudResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaInfoSolicitudResponse }
     *     
     */
    public void setConsultarSolicitudResult(ConsultaInfoSolicitudResponse value) {
        this.consultarSolicitudResult = value;
    }

}
