
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SolicitudAdelantoResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SolicitudAdelantoResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InfoRequest" type="{http://tempuri.org/}SolicitudAdelanteRequest" minOccurs="0"/&gt;
 *         &lt;element name="SolicitudAdelanto" type="{http://tempuri.org/}SolicitudAdelanto" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SolicitudAdelantoResponse", propOrder = {
    "infoRequest",
    "solicitudAdelanto"
})
public class SolicitudAdelantoResponse
    extends BaseResponse
{

    @XmlElement(name = "InfoRequest")
    protected SolicitudAdelanteRequest infoRequest;
    @XmlElement(name = "SolicitudAdelanto")
    protected SolicitudAdelanto solicitudAdelanto;

    /**
     * Obtiene el valor de la propiedad infoRequest.
     * 
     * @return
     *     possible object is
     *     {@link SolicitudAdelanteRequest }
     *     
     */
    public SolicitudAdelanteRequest getInfoRequest() {
        return infoRequest;
    }

    /**
     * Define el valor de la propiedad infoRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link SolicitudAdelanteRequest }
     *     
     */
    public void setInfoRequest(SolicitudAdelanteRequest value) {
        this.infoRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitudAdelanto.
     * 
     * @return
     *     possible object is
     *     {@link SolicitudAdelanto }
     *     
     */
    public SolicitudAdelanto getSolicitudAdelanto() {
        return solicitudAdelanto;
    }

    /**
     * Define el valor de la propiedad solicitudAdelanto.
     * 
     * @param value
     *     allowed object is
     *     {@link SolicitudAdelanto }
     *     
     */
    public void setSolicitudAdelanto(SolicitudAdelanto value) {
        this.solicitudAdelanto = value;
    }

}
