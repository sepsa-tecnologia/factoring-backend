
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfSolicitudAdelantoDetalle complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSolicitudAdelantoDetalle"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SolicitudAdelantoDetalle" type="{http://tempuri.org/}SolicitudAdelantoDetalle" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSolicitudAdelantoDetalle", propOrder = {
    "solicitudAdelantoDetalle"
})
public class ArrayOfSolicitudAdelantoDetalle {

    @XmlElement(name = "SolicitudAdelantoDetalle", nillable = true)
    protected List<SolicitudAdelantoDetalle> solicitudAdelantoDetalle;

    /**
     * Gets the value of the solicitudAdelantoDetalle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the solicitudAdelantoDetalle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSolicitudAdelantoDetalle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SolicitudAdelantoDetalle }
     * 
     * 
     */
    public List<SolicitudAdelantoDetalle> getSolicitudAdelantoDetalle() {
        if (solicitudAdelantoDetalle == null) {
            solicitudAdelantoDetalle = new ArrayList<SolicitudAdelantoDetalle>();
        }
        return this.solicitudAdelantoDetalle;
    }

}
