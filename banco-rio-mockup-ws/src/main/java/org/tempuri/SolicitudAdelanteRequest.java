
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SolicitudAdelanteRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SolicitudAdelanteRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Comprador" type="{http://tempuri.org/}Comprador" minOccurs="0"/&gt;
 *         &lt;element name="Proveedor" type="{http://tempuri.org/}Proveedor" minOccurs="0"/&gt;
 *         &lt;element name="PorcentajeUsoPlataforma" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="InfoCuentaAcreditacion" type="{http://tempuri.org/}InfoCuentaAcreditacion" minOccurs="0"/&gt;
 *         &lt;element name="DetalleFacturas" type="{http://tempuri.org/}ArrayOfSolicitudAdelantoDetalle" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SolicitudAdelanteRequest", propOrder = {
    "comprador",
    "proveedor",
    "porcentajeUsoPlataforma",
    "infoCuentaAcreditacion",
    "detalleFacturas"
})
public class SolicitudAdelanteRequest {

    @XmlElement(name = "Comprador")
    protected Comprador comprador;
    @XmlElement(name = "Proveedor")
    protected Proveedor proveedor;
    @XmlElement(name = "PorcentajeUsoPlataforma")
    protected double porcentajeUsoPlataforma;
    @XmlElement(name = "InfoCuentaAcreditacion")
    protected InfoCuentaAcreditacion infoCuentaAcreditacion;
    @XmlElement(name = "DetalleFacturas")
    protected ArrayOfSolicitudAdelantoDetalle detalleFacturas;

    /**
     * Obtiene el valor de la propiedad comprador.
     * 
     * @return
     *     possible object is
     *     {@link Comprador }
     *     
     */
    public Comprador getComprador() {
        return comprador;
    }

    /**
     * Define el valor de la propiedad comprador.
     * 
     * @param value
     *     allowed object is
     *     {@link Comprador }
     *     
     */
    public void setComprador(Comprador value) {
        this.comprador = value;
    }

    /**
     * Obtiene el valor de la propiedad proveedor.
     * 
     * @return
     *     possible object is
     *     {@link Proveedor }
     *     
     */
    public Proveedor getProveedor() {
        return proveedor;
    }

    /**
     * Define el valor de la propiedad proveedor.
     * 
     * @param value
     *     allowed object is
     *     {@link Proveedor }
     *     
     */
    public void setProveedor(Proveedor value) {
        this.proveedor = value;
    }

    /**
     * Obtiene el valor de la propiedad porcentajeUsoPlataforma.
     * 
     */
    public double getPorcentajeUsoPlataforma() {
        return porcentajeUsoPlataforma;
    }

    /**
     * Define el valor de la propiedad porcentajeUsoPlataforma.
     * 
     */
    public void setPorcentajeUsoPlataforma(double value) {
        this.porcentajeUsoPlataforma = value;
    }

    /**
     * Obtiene el valor de la propiedad infoCuentaAcreditacion.
     * 
     * @return
     *     possible object is
     *     {@link InfoCuentaAcreditacion }
     *     
     */
    public InfoCuentaAcreditacion getInfoCuentaAcreditacion() {
        return infoCuentaAcreditacion;
    }

    /**
     * Define el valor de la propiedad infoCuentaAcreditacion.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoCuentaAcreditacion }
     *     
     */
    public void setInfoCuentaAcreditacion(InfoCuentaAcreditacion value) {
        this.infoCuentaAcreditacion = value;
    }

    /**
     * Obtiene el valor de la propiedad detalleFacturas.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSolicitudAdelantoDetalle }
     *     
     */
    public ArrayOfSolicitudAdelantoDetalle getDetalleFacturas() {
        return detalleFacturas;
    }

    /**
     * Define el valor de la propiedad detalleFacturas.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSolicitudAdelantoDetalle }
     *     
     */
    public void setDetalleFacturas(ArrayOfSolicitudAdelantoDetalle value) {
        this.detalleFacturas = value;
    }

}
