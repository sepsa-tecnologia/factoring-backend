
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultarLiquidacionResult" type="{http://tempuri.org/}ConsultaSolicitudLiqResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarLiquidacionResult"
})
@XmlRootElement(name = "ConsultarLiquidacionResponse")
public class ConsultarLiquidacionResponse {

    @XmlElement(name = "ConsultarLiquidacionResult")
    protected ConsultaSolicitudLiqResponse consultarLiquidacionResult;

    /**
     * Obtiene el valor de la propiedad consultarLiquidacionResult.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaSolicitudLiqResponse }
     *     
     */
    public ConsultaSolicitudLiqResponse getConsultarLiquidacionResult() {
        return consultarLiquidacionResult;
    }

    /**
     * Define el valor de la propiedad consultarLiquidacionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaSolicitudLiqResponse }
     *     
     */
    public void setConsultarLiquidacionResult(ConsultaSolicitudLiqResponse value) {
        this.consultarLiquidacionResult = value;
    }

}
