
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SimuladorCalculo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SimuladorCalculo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdCalculo" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="TipoDocumentoComprador" type="{http://tempuri.org/}TipoDocumento" minOccurs="0"/&gt;
 *         &lt;element name="NroDocumentoComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoDocumentoProveedor" type="{http://tempuri.org/}TipoDocumento" minOccurs="0"/&gt;
 *         &lt;element name="NroDocumentoProveedor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PorcentajeUsoPlataforma" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="TasaAnualNominal" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="TasaAnualEfectiva" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="DetalleDeCalculo" type="{http://tempuri.org/}ArrayOfSimuladorCalculoDetalle" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimuladorCalculo", propOrder = {
    "idCalculo",
    "tipoDocumentoComprador",
    "nroDocumentoComprador",
    "tipoDocumentoProveedor",
    "nroDocumentoProveedor",
    "porcentajeUsoPlataforma",
    "tasaAnualNominal",
    "tasaAnualEfectiva",
    "detalleDeCalculo"
})
public class SimuladorCalculo {

    @XmlElement(name = "IdCalculo")
    protected long idCalculo;
    @XmlElement(name = "TipoDocumentoComprador")
    protected TipoDocumento tipoDocumentoComprador;
    @XmlElement(name = "NroDocumentoComprador")
    protected String nroDocumentoComprador;
    @XmlElement(name = "TipoDocumentoProveedor")
    protected TipoDocumento tipoDocumentoProveedor;
    @XmlElement(name = "NroDocumentoProveedor")
    protected String nroDocumentoProveedor;
    @XmlElement(name = "PorcentajeUsoPlataforma")
    protected double porcentajeUsoPlataforma;
    @XmlElement(name = "TasaAnualNominal")
    protected double tasaAnualNominal;
    @XmlElement(name = "TasaAnualEfectiva")
    protected double tasaAnualEfectiva;
    @XmlElement(name = "DetalleDeCalculo")
    protected ArrayOfSimuladorCalculoDetalle detalleDeCalculo;

    /**
     * Obtiene el valor de la propiedad idCalculo.
     * 
     */
    public long getIdCalculo() {
        return idCalculo;
    }

    /**
     * Define el valor de la propiedad idCalculo.
     * 
     */
    public void setIdCalculo(long value) {
        this.idCalculo = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumentoComprador.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumento }
     *     
     */
    public TipoDocumento getTipoDocumentoComprador() {
        return tipoDocumentoComprador;
    }

    /**
     * Define el valor de la propiedad tipoDocumentoComprador.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumento }
     *     
     */
    public void setTipoDocumentoComprador(TipoDocumento value) {
        this.tipoDocumentoComprador = value;
    }

    /**
     * Obtiene el valor de la propiedad nroDocumentoComprador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroDocumentoComprador() {
        return nroDocumentoComprador;
    }

    /**
     * Define el valor de la propiedad nroDocumentoComprador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroDocumentoComprador(String value) {
        this.nroDocumentoComprador = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumentoProveedor.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumento }
     *     
     */
    public TipoDocumento getTipoDocumentoProveedor() {
        return tipoDocumentoProveedor;
    }

    /**
     * Define el valor de la propiedad tipoDocumentoProveedor.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumento }
     *     
     */
    public void setTipoDocumentoProveedor(TipoDocumento value) {
        this.tipoDocumentoProveedor = value;
    }

    /**
     * Obtiene el valor de la propiedad nroDocumentoProveedor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroDocumentoProveedor() {
        return nroDocumentoProveedor;
    }

    /**
     * Define el valor de la propiedad nroDocumentoProveedor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroDocumentoProveedor(String value) {
        this.nroDocumentoProveedor = value;
    }

    /**
     * Obtiene el valor de la propiedad porcentajeUsoPlataforma.
     * 
     */
    public double getPorcentajeUsoPlataforma() {
        return porcentajeUsoPlataforma;
    }

    /**
     * Define el valor de la propiedad porcentajeUsoPlataforma.
     * 
     */
    public void setPorcentajeUsoPlataforma(double value) {
        this.porcentajeUsoPlataforma = value;
    }

    /**
     * Obtiene el valor de la propiedad tasaAnualNominal.
     * 
     */
    public double getTasaAnualNominal() {
        return tasaAnualNominal;
    }

    /**
     * Define el valor de la propiedad tasaAnualNominal.
     * 
     */
    public void setTasaAnualNominal(double value) {
        this.tasaAnualNominal = value;
    }

    /**
     * Obtiene el valor de la propiedad tasaAnualEfectiva.
     * 
     */
    public double getTasaAnualEfectiva() {
        return tasaAnualEfectiva;
    }

    /**
     * Define el valor de la propiedad tasaAnualEfectiva.
     * 
     */
    public void setTasaAnualEfectiva(double value) {
        this.tasaAnualEfectiva = value;
    }

    /**
     * Obtiene el valor de la propiedad detalleDeCalculo.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSimuladorCalculoDetalle }
     *     
     */
    public ArrayOfSimuladorCalculoDetalle getDetalleDeCalculo() {
        return detalleDeCalculo;
    }

    /**
     * Define el valor de la propiedad detalleDeCalculo.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSimuladorCalculoDetalle }
     *     
     */
    public void setDetalleDeCalculo(ArrayOfSimuladorCalculoDetalle value) {
        this.detalleDeCalculo = value;
    }

}
