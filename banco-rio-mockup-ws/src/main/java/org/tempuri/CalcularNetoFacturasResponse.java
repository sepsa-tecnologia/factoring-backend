
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CalcularNetoFacturasResult" type="{http://tempuri.org/}SimuladorResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "calcularNetoFacturasResult"
})
@XmlRootElement(name = "CalcularNetoFacturasResponse")
public class CalcularNetoFacturasResponse {

    @XmlElement(name = "CalcularNetoFacturasResult")
    protected SimuladorResponse calcularNetoFacturasResult;

    /**
     * Obtiene el valor de la propiedad calcularNetoFacturasResult.
     * 
     * @return
     *     possible object is
     *     {@link SimuladorResponse }
     *     
     */
    public SimuladorResponse getCalcularNetoFacturasResult() {
        return calcularNetoFacturasResult;
    }

    /**
     * Define el valor de la propiedad calcularNetoFacturasResult.
     * 
     * @param value
     *     allowed object is
     *     {@link SimuladorResponse }
     *     
     */
    public void setCalcularNetoFacturasResult(SimuladorResponse value) {
        this.calcularNetoFacturasResult = value;
    }

}
