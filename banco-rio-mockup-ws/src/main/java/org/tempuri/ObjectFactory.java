
package org.tempuri;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObtenerParametros }
     * 
     */
    public ObtenerParametros createObtenerParametros() {
        return new ObtenerParametros();
    }

    /**
     * Create an instance of {@link CredencialAcceso }
     * 
     */
    public CredencialAcceso createCredencialAcceso() {
        return new CredencialAcceso();
    }

    /**
     * Create an instance of {@link ObtenerParametrosResponse }
     * 
     */
    public ObtenerParametrosResponse createObtenerParametrosResponse() {
        return new ObtenerParametrosResponse();
    }

    /**
     * Create an instance of {@link ParametrosResponse }
     * 
     */
    public ParametrosResponse createParametrosResponse() {
        return new ParametrosResponse();
    }

    /**
     * Create an instance of {@link AltaProveedorFisico }
     * 
     */
    public AltaProveedorFisico createAltaProveedorFisico() {
        return new AltaProveedorFisico();
    }

    /**
     * Create an instance of {@link PersonaFisica }
     * 
     */
    public PersonaFisica createPersonaFisica() {
        return new PersonaFisica();
    }

    /**
     * Create an instance of {@link AltaProveedorFisicoResponse }
     * 
     */
    public AltaProveedorFisicoResponse createAltaProveedorFisicoResponse() {
        return new AltaProveedorFisicoResponse();
    }

    /**
     * Create an instance of {@link AltaCompradorProveedorResponse }
     * 
     */
    public AltaCompradorProveedorResponse createAltaCompradorProveedorResponse() {
        return new AltaCompradorProveedorResponse();
    }

    /**
     * Create an instance of {@link AltaProveedorJuridico }
     * 
     */
    public AltaProveedorJuridico createAltaProveedorJuridico() {
        return new AltaProveedorJuridico();
    }

    /**
     * Create an instance of {@link PersonaJuridica }
     * 
     */
    public PersonaJuridica createPersonaJuridica() {
        return new PersonaJuridica();
    }

    /**
     * Create an instance of {@link AltaProveedorJuridicoResponse }
     * 
     */
    public AltaProveedorJuridicoResponse createAltaProveedorJuridicoResponse() {
        return new AltaProveedorJuridicoResponse();
    }

    /**
     * Create an instance of {@link CalcularNetoFacturas }
     * 
     */
    public CalcularNetoFacturas createCalcularNetoFacturas() {
        return new CalcularNetoFacturas();
    }

    /**
     * Create an instance of {@link SimuladorRequest }
     * 
     */
    public SimuladorRequest createSimuladorRequest() {
        return new SimuladorRequest();
    }

    /**
     * Create an instance of {@link CalcularNetoFacturasResponse }
     * 
     */
    public CalcularNetoFacturasResponse createCalcularNetoFacturasResponse() {
        return new CalcularNetoFacturasResponse();
    }

    /**
     * Create an instance of {@link SimuladorResponse }
     * 
     */
    public SimuladorResponse createSimuladorResponse() {
        return new SimuladorResponse();
    }

    /**
     * Create an instance of {@link ObtenerSaldoLineaCredito }
     * 
     */
    public ObtenerSaldoLineaCredito createObtenerSaldoLineaCredito() {
        return new ObtenerSaldoLineaCredito();
    }

    /**
     * Create an instance of {@link SaldoLineaCreditoRequest }
     * 
     */
    public SaldoLineaCreditoRequest createSaldoLineaCreditoRequest() {
        return new SaldoLineaCreditoRequest();
    }

    /**
     * Create an instance of {@link ObtenerSaldoLineaCreditoResponse }
     * 
     */
    public ObtenerSaldoLineaCreditoResponse createObtenerSaldoLineaCreditoResponse() {
        return new ObtenerSaldoLineaCreditoResponse();
    }

    /**
     * Create an instance of {@link SaldoLineaCreditoResponse }
     * 
     */
    public SaldoLineaCreditoResponse createSaldoLineaCreditoResponse() {
        return new SaldoLineaCreditoResponse();
    }

    /**
     * Create an instance of {@link SolicitarAdelantoDeFacturas }
     * 
     */
    public SolicitarAdelantoDeFacturas createSolicitarAdelantoDeFacturas() {
        return new SolicitarAdelantoDeFacturas();
    }

    /**
     * Create an instance of {@link SolicitudAdelanteRequest }
     * 
     */
    public SolicitudAdelanteRequest createSolicitudAdelanteRequest() {
        return new SolicitudAdelanteRequest();
    }

    /**
     * Create an instance of {@link SolicitarAdelantoDeFacturasResponse }
     * 
     */
    public SolicitarAdelantoDeFacturasResponse createSolicitarAdelantoDeFacturasResponse() {
        return new SolicitarAdelantoDeFacturasResponse();
    }

    /**
     * Create an instance of {@link SolicitudAdelantoResponse }
     * 
     */
    public SolicitudAdelantoResponse createSolicitudAdelantoResponse() {
        return new SolicitudAdelantoResponse();
    }

    /**
     * Create an instance of {@link ConsultarEstadoProveedor }
     * 
     */
    public ConsultarEstadoProveedor createConsultarEstadoProveedor() {
        return new ConsultarEstadoProveedor();
    }

    /**
     * Create an instance of {@link ConsultaProveedorRequest }
     * 
     */
    public ConsultaProveedorRequest createConsultaProveedorRequest() {
        return new ConsultaProveedorRequest();
    }

    /**
     * Create an instance of {@link ConsultarEstadoProveedorResponse }
     * 
     */
    public ConsultarEstadoProveedorResponse createConsultarEstadoProveedorResponse() {
        return new ConsultarEstadoProveedorResponse();
    }

    /**
     * Create an instance of {@link ConsultaProveedorResponse }
     * 
     */
    public ConsultaProveedorResponse createConsultaProveedorResponse() {
        return new ConsultaProveedorResponse();
    }

    /**
     * Create an instance of {@link ConsultarCompradores }
     * 
     */
    public ConsultarCompradores createConsultarCompradores() {
        return new ConsultarCompradores();
    }

    /**
     * Create an instance of {@link ConsultarCompradoresResponse }
     * 
     */
    public ConsultarCompradoresResponse createConsultarCompradoresResponse() {
        return new ConsultarCompradoresResponse();
    }

    /**
     * Create an instance of {@link ConsultaCompradoresResponse }
     * 
     */
    public ConsultaCompradoresResponse createConsultaCompradoresResponse() {
        return new ConsultaCompradoresResponse();
    }

    /**
     * Create an instance of {@link ConsultarProveedor }
     * 
     */
    public ConsultarProveedor createConsultarProveedor() {
        return new ConsultarProveedor();
    }

    /**
     * Create an instance of {@link InfoProveedorRequest }
     * 
     */
    public InfoProveedorRequest createInfoProveedorRequest() {
        return new InfoProveedorRequest();
    }

    /**
     * Create an instance of {@link ConsultarProveedorResponse }
     * 
     */
    public ConsultarProveedorResponse createConsultarProveedorResponse() {
        return new ConsultarProveedorResponse();
    }

    /**
     * Create an instance of {@link InfoProveedorResponse }
     * 
     */
    public InfoProveedorResponse createInfoProveedorResponse() {
        return new InfoProveedorResponse();
    }

    /**
     * Create an instance of {@link ConsultarSolicitud }
     * 
     */
    public ConsultarSolicitud createConsultarSolicitud() {
        return new ConsultarSolicitud();
    }

    /**
     * Create an instance of {@link ConsultaInfoSolicitudRequest }
     * 
     */
    public ConsultaInfoSolicitudRequest createConsultaInfoSolicitudRequest() {
        return new ConsultaInfoSolicitudRequest();
    }

    /**
     * Create an instance of {@link ConsultarSolicitudResponse }
     * 
     */
    public ConsultarSolicitudResponse createConsultarSolicitudResponse() {
        return new ConsultarSolicitudResponse();
    }

    /**
     * Create an instance of {@link ConsultaInfoSolicitudResponse }
     * 
     */
    public ConsultaInfoSolicitudResponse createConsultaInfoSolicitudResponse() {
        return new ConsultaInfoSolicitudResponse();
    }

    /**
     * Create an instance of {@link ConsultarFacturasALiquidar }
     * 
     */
    public ConsultarFacturasALiquidar createConsultarFacturasALiquidar() {
        return new ConsultarFacturasALiquidar();
    }

    /**
     * Create an instance of {@link RequestFacturasALiquidar }
     * 
     */
    public RequestFacturasALiquidar createRequestFacturasALiquidar() {
        return new RequestFacturasALiquidar();
    }

    /**
     * Create an instance of {@link ConsultarFacturasALiquidarResponse }
     * 
     */
    public ConsultarFacturasALiquidarResponse createConsultarFacturasALiquidarResponse() {
        return new ConsultarFacturasALiquidarResponse();
    }

    /**
     * Create an instance of {@link ResponseFacturasALiquidar }
     * 
     */
    public ResponseFacturasALiquidar createResponseFacturasALiquidar() {
        return new ResponseFacturasALiquidar();
    }

    /**
     * Create an instance of {@link SolicitarLiquidacionDeFacturas }
     * 
     */
    public SolicitarLiquidacionDeFacturas createSolicitarLiquidacionDeFacturas() {
        return new SolicitarLiquidacionDeFacturas();
    }

    /**
     * Create an instance of {@link RequestSolicitudLiquidacionFactura }
     * 
     */
    public RequestSolicitudLiquidacionFactura createRequestSolicitudLiquidacionFactura() {
        return new RequestSolicitudLiquidacionFactura();
    }

    /**
     * Create an instance of {@link SolicitarLiquidacionDeFacturasResponse }
     * 
     */
    public SolicitarLiquidacionDeFacturasResponse createSolicitarLiquidacionDeFacturasResponse() {
        return new SolicitarLiquidacionDeFacturasResponse();
    }

    /**
     * Create an instance of {@link ResponseSolicitudLiquidacionFactura }
     * 
     */
    public ResponseSolicitudLiquidacionFactura createResponseSolicitudLiquidacionFactura() {
        return new ResponseSolicitudLiquidacionFactura();
    }

    /**
     * Create an instance of {@link ConsultarLiquidacion }
     * 
     */
    public ConsultarLiquidacion createConsultarLiquidacion() {
        return new ConsultarLiquidacion();
    }

    /**
     * Create an instance of {@link ConsultaSolicitudLiqRequest }
     * 
     */
    public ConsultaSolicitudLiqRequest createConsultaSolicitudLiqRequest() {
        return new ConsultaSolicitudLiqRequest();
    }

    /**
     * Create an instance of {@link ConsultarLiquidacionResponse }
     * 
     */
    public ConsultarLiquidacionResponse createConsultarLiquidacionResponse() {
        return new ConsultarLiquidacionResponse();
    }

    /**
     * Create an instance of {@link ConsultaSolicitudLiqResponse }
     * 
     */
    public ConsultaSolicitudLiqResponse createConsultaSolicitudLiqResponse() {
        return new ConsultaSolicitudLiqResponse();
    }

    /**
     * Create an instance of {@link BaseResponse }
     * 
     */
    public BaseResponse createBaseResponse() {
        return new BaseResponse();
    }

    /**
     * Create an instance of {@link Parametros }
     * 
     */
    public Parametros createParametros() {
        return new Parametros();
    }

    /**
     * Create an instance of {@link ArrayOfActividadEconomica }
     * 
     */
    public ArrayOfActividadEconomica createArrayOfActividadEconomica() {
        return new ArrayOfActividadEconomica();
    }

    /**
     * Create an instance of {@link ActividadEconomica }
     * 
     */
    public ActividadEconomica createActividadEconomica() {
        return new ActividadEconomica();
    }

    /**
     * Create an instance of {@link Banco }
     * 
     */
    public Banco createBanco() {
        return new Banco();
    }

    /**
     * Create an instance of {@link Ciudad }
     * 
     */
    public Ciudad createCiudad() {
        return new Ciudad();
    }

    /**
     * Create an instance of {@link Region }
     * 
     */
    public Region createRegion() {
        return new Region();
    }

    /**
     * Create an instance of {@link TipoDocumento }
     * 
     */
    public TipoDocumento createTipoDocumento() {
        return new TipoDocumento();
    }

    /**
     * Create an instance of {@link TipoSociedad }
     * 
     */
    public TipoSociedad createTipoSociedad() {
        return new TipoSociedad();
    }

    /**
     * Create an instance of {@link ArrayOfTipoSociedad }
     * 
     */
    public ArrayOfTipoSociedad createArrayOfTipoSociedad() {
        return new ArrayOfTipoSociedad();
    }

    /**
     * Create an instance of {@link ArrayOfTipoDocumento }
     * 
     */
    public ArrayOfTipoDocumento createArrayOfTipoDocumento() {
        return new ArrayOfTipoDocumento();
    }

    /**
     * Create an instance of {@link ArrayOfRegion }
     * 
     */
    public ArrayOfRegion createArrayOfRegion() {
        return new ArrayOfRegion();
    }

    /**
     * Create an instance of {@link ArrayOfCiudad }
     * 
     */
    public ArrayOfCiudad createArrayOfCiudad() {
        return new ArrayOfCiudad();
    }

    /**
     * Create an instance of {@link ArrayOfBanco }
     * 
     */
    public ArrayOfBanco createArrayOfBanco() {
        return new ArrayOfBanco();
    }

    /**
     * Create an instance of {@link Comprador }
     * 
     */
    public Comprador createComprador() {
        return new Comprador();
    }

    /**
     * Create an instance of {@link PersonaDireccion }
     * 
     */
    public PersonaDireccion createPersonaDireccion() {
        return new PersonaDireccion();
    }

    /**
     * Create an instance of {@link InfoCuentaAcreditacion }
     * 
     */
    public InfoCuentaAcreditacion createInfoCuentaAcreditacion() {
        return new InfoCuentaAcreditacion();
    }

    /**
     * Create an instance of {@link InfoCrc }
     * 
     */
    public InfoCrc createInfoCrc() {
        return new InfoCrc();
    }

    /**
     * Create an instance of {@link SimuladorCalculo }
     * 
     */
    public SimuladorCalculo createSimuladorCalculo() {
        return new SimuladorCalculo();
    }

    /**
     * Create an instance of {@link ArrayOfSimuladorCalculoDetalle }
     * 
     */
    public ArrayOfSimuladorCalculoDetalle createArrayOfSimuladorCalculoDetalle() {
        return new ArrayOfSimuladorCalculoDetalle();
    }

    /**
     * Create an instance of {@link SimuladorCalculoDetalle }
     * 
     */
    public SimuladorCalculoDetalle createSimuladorCalculoDetalle() {
        return new SimuladorCalculoDetalle();
    }

    /**
     * Create an instance of {@link Proveedor }
     * 
     */
    public Proveedor createProveedor() {
        return new Proveedor();
    }

    /**
     * Create an instance of {@link ArrayOfProveedorCuentas }
     * 
     */
    public ArrayOfProveedorCuentas createArrayOfProveedorCuentas() {
        return new ArrayOfProveedorCuentas();
    }

    /**
     * Create an instance of {@link ProveedorCuentas }
     * 
     */
    public ProveedorCuentas createProveedorCuentas() {
        return new ProveedorCuentas();
    }

    /**
     * Create an instance of {@link ArrayOfSolicitudAdelantoDetalle }
     * 
     */
    public ArrayOfSolicitudAdelantoDetalle createArrayOfSolicitudAdelantoDetalle() {
        return new ArrayOfSolicitudAdelantoDetalle();
    }

    /**
     * Create an instance of {@link SolicitudAdelantoDetalle }
     * 
     */
    public SolicitudAdelantoDetalle createSolicitudAdelantoDetalle() {
        return new SolicitudAdelantoDetalle();
    }

    /**
     * Create an instance of {@link SolicitudAdelanto }
     * 
     */
    public SolicitudAdelanto createSolicitudAdelanto() {
        return new SolicitudAdelanto();
    }

    /**
     * Create an instance of {@link ArrayOfInfoProveedor }
     * 
     */
    public ArrayOfInfoProveedor createArrayOfInfoProveedor() {
        return new ArrayOfInfoProveedor();
    }

    /**
     * Create an instance of {@link InfoProveedor }
     * 
     */
    public InfoProveedor createInfoProveedor() {
        return new InfoProveedor();
    }

    /**
     * Create an instance of {@link ArrayOfComprador }
     * 
     */
    public ArrayOfComprador createArrayOfComprador() {
        return new ArrayOfComprador();
    }

    /**
     * Create an instance of {@link ArrayOfDateTime }
     * 
     */
    public ArrayOfDateTime createArrayOfDateTime() {
        return new ArrayOfDateTime();
    }

    /**
     * Create an instance of {@link ArrayOfSolicitudAdelanto }
     * 
     */
    public ArrayOfSolicitudAdelanto createArrayOfSolicitudAdelanto() {
        return new ArrayOfSolicitudAdelanto();
    }

    /**
     * Create an instance of {@link ArrayOfInfoFacturaALiquidar }
     * 
     */
    public ArrayOfInfoFacturaALiquidar createArrayOfInfoFacturaALiquidar() {
        return new ArrayOfInfoFacturaALiquidar();
    }

    /**
     * Create an instance of {@link InfoFacturaALiquidar }
     * 
     */
    public InfoFacturaALiquidar createInfoFacturaALiquidar() {
        return new InfoFacturaALiquidar();
    }

    /**
     * Create an instance of {@link InfoComisionComprador }
     * 
     */
    public InfoComisionComprador createInfoComisionComprador() {
        return new InfoComisionComprador();
    }

    /**
     * Create an instance of {@link InfoComisionBanco }
     * 
     */
    public InfoComisionBanco createInfoComisionBanco() {
        return new InfoComisionBanco();
    }

    /**
     * Create an instance of {@link InfoComisionSepsa }
     * 
     */
    public InfoComisionSepsa createInfoComisionSepsa() {
        return new InfoComisionSepsa();
    }

    /**
     * Create an instance of {@link InfoSolicitudLiquidacion }
     * 
     */
    public InfoSolicitudLiquidacion createInfoSolicitudLiquidacion() {
        return new InfoSolicitudLiquidacion();
    }

    /**
     * Create an instance of {@link InfoComprador }
     * 
     */
    public InfoComprador createInfoComprador() {
        return new InfoComprador();
    }

    /**
     * Create an instance of {@link InfoBanco }
     * 
     */
    public InfoBanco createInfoBanco() {
        return new InfoBanco();
    }

    /**
     * Create an instance of {@link InfoSepsa }
     * 
     */
    public InfoSepsa createInfoSepsa() {
        return new InfoSepsa();
    }

    /**
     * Create an instance of {@link ArrayOfFactura }
     * 
     */
    public ArrayOfFactura createArrayOfFactura() {
        return new ArrayOfFactura();
    }

    /**
     * Create an instance of {@link Factura }
     * 
     */
    public Factura createFactura() {
        return new Factura();
    }

}
