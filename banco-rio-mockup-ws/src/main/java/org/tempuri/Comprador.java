package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para Comprador complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="Comprador"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}Persona"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PersonaJuridica" type="{http://tempuri.org/}PersonaJuridica" minOccurs="0"/&gt;
 *         &lt;element name="CodAplica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Tasa" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="Moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CobrarSeguro" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="PorcentajeGastosAdm" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="CuentaCredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CuentaDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Comprador", propOrder = {
    "personaJuridica",
    "codAplica",
    "tasa",
    "moneda",
    "cobrarSeguro",
    "porcentajeGastosAdm",
    "cuentaCredito",
    "cuentaDebito"
})
public class Comprador
        extends Persona {

    @XmlElement(name = "PersonaJuridica")
    protected PersonaJuridica personaJuridica;
    @XmlElement(name = "CodAplica")
    protected String codAplica;
    @XmlElement(name = "Tasa")
    protected double tasa;
    @XmlElement(name = "Moneda")
    protected String moneda;
    @XmlElement(name = "CobrarSeguro")
    protected boolean cobrarSeguro;
    @XmlElement(name = "PorcentajeGastosAdm")
    protected double porcentajeGastosAdm;
    @XmlElement(name = "CuentaCredito")
    protected String cuentaCredito;
    @XmlElement(name = "CuentaDebito")
    protected String cuentaDebito;

    /**
     * Obtiene el valor de la propiedad personaJuridica.
     *
     * @return possible object is {@link PersonaJuridica }
     *
     */
    public PersonaJuridica getPersonaJuridica() {
        return personaJuridica;
    }

    /**
     * Define el valor de la propiedad personaJuridica.
     *
     * @param value allowed object is {@link PersonaJuridica }
     *
     */
    public void setPersonaJuridica(PersonaJuridica value) {
        this.personaJuridica = value;
    }

    /**
     * Obtiene el valor de la propiedad codAplica.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodAplica() {
        return codAplica;
    }

    /**
     * Define el valor de la propiedad codAplica.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCodAplica(String value) {
        this.codAplica = value;
    }

    /**
     * Obtiene el valor de la propiedad tasa.
     *
     */
    public double getTasa() {
        return tasa;
    }

    /**
     * Define el valor de la propiedad tasa.
     *
     */
    public void setTasa(double value) {
        this.tasa = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad cobrarSeguro.
     *
     */
    public boolean isCobrarSeguro() {
        return cobrarSeguro;
    }

    /**
     * Define el valor de la propiedad cobrarSeguro.
     *
     */
    public void setCobrarSeguro(boolean value) {
        this.cobrarSeguro = value;
    }

    /**
     * Obtiene el valor de la propiedad porcentajeGastosAdm.
     *
     */
    public double getPorcentajeGastosAdm() {
        return porcentajeGastosAdm;
    }

    /**
     * Define el valor de la propiedad porcentajeGastosAdm.
     *
     */
    public void setPorcentajeGastosAdm(double value) {
        this.porcentajeGastosAdm = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCredito.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCuentaCredito() {
        return cuentaCredito;
    }

    /**
     * Define el valor de la propiedad cuentaCredito.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCuentaCredito(String value) {
        this.cuentaCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaDebito.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCuentaDebito() {
        return cuentaDebito;
    }

    /**
     * Define el valor de la propiedad cuentaDebito.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCuentaDebito(String value) {
        this.cuentaDebito = value;
    }

}
