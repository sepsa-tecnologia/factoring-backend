
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfInfoProveedor complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfInfoProveedor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InfoProveedor" type="{http://tempuri.org/}InfoProveedor" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfInfoProveedor", propOrder = {
    "infoProveedor"
})
public class ArrayOfInfoProveedor {

    @XmlElement(name = "InfoProveedor", nillable = true)
    protected List<InfoProveedor> infoProveedor;

    /**
     * Gets the value of the infoProveedor property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infoProveedor property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfoProveedor().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InfoProveedor }
     * 
     * 
     */
    public List<InfoProveedor> getInfoProveedor() {
        if (infoProveedor == null) {
            infoProveedor = new ArrayList<InfoProveedor>();
        }
        return this.infoProveedor;
    }

}
