
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Acceso" type="{http://tempuri.org/}CredencialAcceso" minOccurs="0"/&gt;
 *         &lt;element name="request" type="{http://tempuri.org/}ConsultaInfoSolicitudRequest" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "acceso",
    "request"
})
@XmlRootElement(name = "ConsultarSolicitud")
public class ConsultarSolicitud {

    @XmlElement(name = "Acceso")
    protected CredencialAcceso acceso;
    protected ConsultaInfoSolicitudRequest request;

    /**
     * Obtiene el valor de la propiedad acceso.
     * 
     * @return
     *     possible object is
     *     {@link CredencialAcceso }
     *     
     */
    public CredencialAcceso getAcceso() {
        return acceso;
    }

    /**
     * Define el valor de la propiedad acceso.
     * 
     * @param value
     *     allowed object is
     *     {@link CredencialAcceso }
     *     
     */
    public void setAcceso(CredencialAcceso value) {
        this.acceso = value;
    }

    /**
     * Obtiene el valor de la propiedad request.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaInfoSolicitudRequest }
     *     
     */
    public ConsultaInfoSolicitudRequest getRequest() {
        return request;
    }

    /**
     * Define el valor de la propiedad request.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaInfoSolicitudRequest }
     *     
     */
    public void setRequest(ConsultaInfoSolicitudRequest value) {
        this.request = value;
    }

}
