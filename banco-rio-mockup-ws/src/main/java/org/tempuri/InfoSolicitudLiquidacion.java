
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para InfoSolicitudLiquidacion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InfoSolicitudLiquidacion"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdLiquidacion" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="Comprador" type="{http://tempuri.org/}Comprador" minOccurs="0"/&gt;
 *         &lt;element name="InfoComprador" type="{http://tempuri.org/}InfoComprador" minOccurs="0"/&gt;
 *         &lt;element name="InfoBanco" type="{http://tempuri.org/}InfoBanco" minOccurs="0"/&gt;
 *         &lt;element name="InfoSepsa" type="{http://tempuri.org/}InfoSepsa" minOccurs="0"/&gt;
 *         &lt;element name="FechaSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Estado" type="{http://tempuri.org/}ESTADO_LIQUIDACION"/&gt;
 *         &lt;element name="FechaDecision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Facturas" type="{http://tempuri.org/}ArrayOfFactura" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoSolicitudLiquidacion", propOrder = {
    "idLiquidacion",
    "comprador",
    "infoComprador",
    "infoBanco",
    "infoSepsa",
    "fechaSolicitud",
    "estado",
    "fechaDecision",
    "facturas"
})
public class InfoSolicitudLiquidacion {

    @XmlElement(name = "IdLiquidacion")
    protected long idLiquidacion;
    @XmlElement(name = "Comprador")
    protected Comprador comprador;
    @XmlElement(name = "InfoComprador")
    protected InfoComprador infoComprador;
    @XmlElement(name = "InfoBanco")
    protected InfoBanco infoBanco;
    @XmlElement(name = "InfoSepsa")
    protected InfoSepsa infoSepsa;
    @XmlElement(name = "FechaSolicitud")
    protected String fechaSolicitud;
    @XmlElement(name = "Estado", required = true)
    @XmlSchemaType(name = "string")
    protected ESTADOLIQUIDACION estado;
    @XmlElement(name = "FechaDecision")
    protected String fechaDecision;
    @XmlElement(name = "Facturas")
    protected ArrayOfFactura facturas;

    /**
     * Obtiene el valor de la propiedad idLiquidacion.
     * 
     */
    public long getIdLiquidacion() {
        return idLiquidacion;
    }

    /**
     * Define el valor de la propiedad idLiquidacion.
     * 
     */
    public void setIdLiquidacion(long value) {
        this.idLiquidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad comprador.
     * 
     * @return
     *     possible object is
     *     {@link Comprador }
     *     
     */
    public Comprador getComprador() {
        return comprador;
    }

    /**
     * Define el valor de la propiedad comprador.
     * 
     * @param value
     *     allowed object is
     *     {@link Comprador }
     *     
     */
    public void setComprador(Comprador value) {
        this.comprador = value;
    }

    /**
     * Obtiene el valor de la propiedad infoComprador.
     * 
     * @return
     *     possible object is
     *     {@link InfoComprador }
     *     
     */
    public InfoComprador getInfoComprador() {
        return infoComprador;
    }

    /**
     * Define el valor de la propiedad infoComprador.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoComprador }
     *     
     */
    public void setInfoComprador(InfoComprador value) {
        this.infoComprador = value;
    }

    /**
     * Obtiene el valor de la propiedad infoBanco.
     * 
     * @return
     *     possible object is
     *     {@link InfoBanco }
     *     
     */
    public InfoBanco getInfoBanco() {
        return infoBanco;
    }

    /**
     * Define el valor de la propiedad infoBanco.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoBanco }
     *     
     */
    public void setInfoBanco(InfoBanco value) {
        this.infoBanco = value;
    }

    /**
     * Obtiene el valor de la propiedad infoSepsa.
     * 
     * @return
     *     possible object is
     *     {@link InfoSepsa }
     *     
     */
    public InfoSepsa getInfoSepsa() {
        return infoSepsa;
    }

    /**
     * Define el valor de la propiedad infoSepsa.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoSepsa }
     *     
     */
    public void setInfoSepsa(InfoSepsa value) {
        this.infoSepsa = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    /**
     * Define el valor de la propiedad fechaSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaSolicitud(String value) {
        this.fechaSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link ESTADOLIQUIDACION }
     *     
     */
    public ESTADOLIQUIDACION getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link ESTADOLIQUIDACION }
     *     
     */
    public void setEstado(ESTADOLIQUIDACION value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaDecision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaDecision() {
        return fechaDecision;
    }

    /**
     * Define el valor de la propiedad fechaDecision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaDecision(String value) {
        this.fechaDecision = value;
    }

    /**
     * Obtiene el valor de la propiedad facturas.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfFactura }
     *     
     */
    public ArrayOfFactura getFacturas() {
        return facturas;
    }

    /**
     * Define el valor de la propiedad facturas.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfFactura }
     *     
     */
    public void setFacturas(ArrayOfFactura value) {
        this.facturas = value;
    }

}
