
package org.tempuri;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ESTADO_LIQUIDACION.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <pre>
 * &lt;simpleType name="ESTADO_LIQUIDACION"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SOLICITADO"/&gt;
 *     &lt;enumeration value="APROBADO"/&gt;
 *     &lt;enumeration value="RECHAZADO"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ESTADO_LIQUIDACION")
@XmlEnum
public enum ESTADOLIQUIDACION {

    SOLICITADO,
    APROBADO,
    RECHAZADO;

    public String value() {
        return name();
    }

    public static ESTADOLIQUIDACION fromValue(String v) {
        return valueOf(v);
    }

}
