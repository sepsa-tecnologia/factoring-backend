
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Ciudad complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Ciudad"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseParametro"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CodRegion" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Ciudad", propOrder = {
    "codRegion"
})
public class Ciudad
    extends BaseParametro
{

    @XmlElement(name = "CodRegion")
    protected int codRegion;

    /**
     * Obtiene el valor de la propiedad codRegion.
     * 
     */
    public int getCodRegion() {
        return codRegion;
    }

    /**
     * Define el valor de la propiedad codRegion.
     * 
     */
    public void setCodRegion(int value) {
        this.codRegion = value;
    }

}
