
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SimuladorRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SimuladorRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Simulador" type="{http://tempuri.org/}SimuladorCalculo" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimuladorRequest", propOrder = {
    "simulador"
})
public class SimuladorRequest {

    @XmlElement(name = "Simulador")
    protected SimuladorCalculo simulador;

    /**
     * Obtiene el valor de la propiedad simulador.
     * 
     * @return
     *     possible object is
     *     {@link SimuladorCalculo }
     *     
     */
    public SimuladorCalculo getSimulador() {
        return simulador;
    }

    /**
     * Define el valor de la propiedad simulador.
     * 
     * @param value
     *     allowed object is
     *     {@link SimuladorCalculo }
     *     
     */
    public void setSimulador(SimuladorCalculo value) {
        this.simulador = value;
    }

}
