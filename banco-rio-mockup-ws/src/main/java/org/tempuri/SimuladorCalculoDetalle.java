
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para SimuladorCalculoDetalle complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SimuladorCalculoDetalle"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Simulador" type="{http://tempuri.org/}SimuladorCalculo" minOccurs="0"/&gt;
 *         &lt;element name="NroFactura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Monto" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="FechaVto" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="UsoPlataforma" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="DiasDeInteres" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Interes" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="Seguro" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="IvaSeguro" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="GastosAdm" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="IvaGastosAdm" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="Neto" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="Estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Mensaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimuladorCalculoDetalle", propOrder = {
    "simulador",
    "nroFactura",
    "monto",
    "fechaVto",
    "usoPlataforma",
    "diasDeInteres",
    "interes",
    "seguro",
    "ivaSeguro",
    "gastosAdm",
    "ivaGastosAdm",
    "neto",
    "estado",
    "mensaje"
})
public class SimuladorCalculoDetalle {

    @XmlElement(name = "Simulador")
    protected SimuladorCalculo simulador;
    @XmlElement(name = "NroFactura")
    protected String nroFactura;
    @XmlElement(name = "Monto")
    protected double monto;
    @XmlElement(name = "FechaVto", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaVto;
    @XmlElement(name = "UsoPlataforma")
    protected double usoPlataforma;
    @XmlElement(name = "DiasDeInteres")
    protected int diasDeInteres;
    @XmlElement(name = "Interes")
    protected double interes;
    @XmlElement(name = "Seguro")
    protected double seguro;
    @XmlElement(name = "IvaSeguro")
    protected double ivaSeguro;
    @XmlElement(name = "GastosAdm")
    protected double gastosAdm;
    @XmlElement(name = "IvaGastosAdm")
    protected double ivaGastosAdm;
    @XmlElement(name = "Neto")
    protected double neto;
    @XmlElement(name = "Estado")
    protected String estado;
    @XmlElement(name = "Mensaje")
    protected String mensaje;

    /**
     * Obtiene el valor de la propiedad simulador.
     * 
     * @return
     *     possible object is
     *     {@link SimuladorCalculo }
     *     
     */
    public SimuladorCalculo getSimulador() {
        return simulador;
    }

    /**
     * Define el valor de la propiedad simulador.
     * 
     * @param value
     *     allowed object is
     *     {@link SimuladorCalculo }
     *     
     */
    public void setSimulador(SimuladorCalculo value) {
        this.simulador = value;
    }

    /**
     * Obtiene el valor de la propiedad nroFactura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroFactura() {
        return nroFactura;
    }

    /**
     * Define el valor de la propiedad nroFactura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroFactura(String value) {
        this.nroFactura = value;
    }

    /**
     * Obtiene el valor de la propiedad monto.
     * 
     */
    public double getMonto() {
        return monto;
    }

    /**
     * Define el valor de la propiedad monto.
     * 
     */
    public void setMonto(double value) {
        this.monto = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVto.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaVto() {
        return fechaVto;
    }

    /**
     * Define el valor de la propiedad fechaVto.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaVto(XMLGregorianCalendar value) {
        this.fechaVto = value;
    }

    /**
     * Obtiene el valor de la propiedad usoPlataforma.
     * 
     */
    public double getUsoPlataforma() {
        return usoPlataforma;
    }

    /**
     * Define el valor de la propiedad usoPlataforma.
     * 
     */
    public void setUsoPlataforma(double value) {
        this.usoPlataforma = value;
    }

    /**
     * Obtiene el valor de la propiedad diasDeInteres.
     * 
     */
    public int getDiasDeInteres() {
        return diasDeInteres;
    }

    /**
     * Define el valor de la propiedad diasDeInteres.
     * 
     */
    public void setDiasDeInteres(int value) {
        this.diasDeInteres = value;
    }

    /**
     * Obtiene el valor de la propiedad interes.
     * 
     */
    public double getInteres() {
        return interes;
    }

    /**
     * Define el valor de la propiedad interes.
     * 
     */
    public void setInteres(double value) {
        this.interes = value;
    }

    /**
     * Obtiene el valor de la propiedad seguro.
     * 
     */
    public double getSeguro() {
        return seguro;
    }

    /**
     * Define el valor de la propiedad seguro.
     * 
     */
    public void setSeguro(double value) {
        this.seguro = value;
    }

    /**
     * Obtiene el valor de la propiedad ivaSeguro.
     * 
     */
    public double getIvaSeguro() {
        return ivaSeguro;
    }

    /**
     * Define el valor de la propiedad ivaSeguro.
     * 
     */
    public void setIvaSeguro(double value) {
        this.ivaSeguro = value;
    }

    /**
     * Obtiene el valor de la propiedad gastosAdm.
     * 
     */
    public double getGastosAdm() {
        return gastosAdm;
    }

    /**
     * Define el valor de la propiedad gastosAdm.
     * 
     */
    public void setGastosAdm(double value) {
        this.gastosAdm = value;
    }

    /**
     * Obtiene el valor de la propiedad ivaGastosAdm.
     * 
     */
    public double getIvaGastosAdm() {
        return ivaGastosAdm;
    }

    /**
     * Define el valor de la propiedad ivaGastosAdm.
     * 
     */
    public void setIvaGastosAdm(double value) {
        this.ivaGastosAdm = value;
    }

    /**
     * Obtiene el valor de la propiedad neto.
     * 
     */
    public double getNeto() {
        return neto;
    }

    /**
     * Define el valor de la propiedad neto.
     * 
     */
    public void setNeto(double value) {
        this.neto = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

}
