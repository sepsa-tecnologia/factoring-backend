
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultaProveedorResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultaProveedorResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultaRequest" type="{http://tempuri.org/}ConsultaProveedorRequest" minOccurs="0"/&gt;
 *         &lt;element name="InfoProveedor" type="{http://tempuri.org/}ArrayOfInfoProveedor" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaProveedorResponse", propOrder = {
    "consultaRequest",
    "infoProveedor"
})
public class ConsultaProveedorResponse
    extends BaseResponse
{

    @XmlElement(name = "ConsultaRequest")
    protected ConsultaProveedorRequest consultaRequest;
    @XmlElement(name = "InfoProveedor")
    protected ArrayOfInfoProveedor infoProveedor;

    /**
     * Obtiene el valor de la propiedad consultaRequest.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaProveedorRequest }
     *     
     */
    public ConsultaProveedorRequest getConsultaRequest() {
        return consultaRequest;
    }

    /**
     * Define el valor de la propiedad consultaRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaProveedorRequest }
     *     
     */
    public void setConsultaRequest(ConsultaProveedorRequest value) {
        this.consultaRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad infoProveedor.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInfoProveedor }
     *     
     */
    public ArrayOfInfoProveedor getInfoProveedor() {
        return infoProveedor;
    }

    /**
     * Define el valor de la propiedad infoProveedor.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInfoProveedor }
     *     
     */
    public void setInfoProveedor(ArrayOfInfoProveedor value) {
        this.infoProveedor = value;
    }

}
