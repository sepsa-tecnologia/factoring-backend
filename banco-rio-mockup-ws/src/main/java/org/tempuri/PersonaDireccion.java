
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PersonaDireccion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PersonaDireccion"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Calle1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Calle2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Referencia" type="{http://tempuri.org/}ReferenciaDireccion"/&gt;
 *         &lt;element name="NroCalle" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Calle3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Barrio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Ciudad" type="{http://tempuri.org/}Ciudad" minOccurs="0"/&gt;
 *         &lt;element name="Region" type="{http://tempuri.org/}Region" minOccurs="0"/&gt;
 *         &lt;element name="NroLineaBaja" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NroCelular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DireccionCompleta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonaDireccion", propOrder = {
    "calle1",
    "calle2",
    "referencia",
    "nroCalle",
    "calle3",
    "barrio",
    "ciudad",
    "region",
    "nroLineaBaja",
    "nroCelular",
    "email",
    "direccionCompleta"
})
public class PersonaDireccion {

    @XmlElement(name = "Calle1")
    protected String calle1;
    @XmlElement(name = "Calle2")
    protected String calle2;
    @XmlElement(name = "Referencia", required = true)
    @XmlSchemaType(name = "string")
    protected ReferenciaDireccion referencia;
    @XmlElement(name = "NroCalle")
    protected int nroCalle;
    @XmlElement(name = "Calle3")
    protected String calle3;
    @XmlElement(name = "Barrio")
    protected String barrio;
    @XmlElement(name = "Ciudad")
    protected Ciudad ciudad;
    @XmlElement(name = "Region")
    protected Region region;
    @XmlElement(name = "NroLineaBaja")
    protected String nroLineaBaja;
    @XmlElement(name = "NroCelular")
    protected String nroCelular;
    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "DireccionCompleta")
    protected String direccionCompleta;

    /**
     * Obtiene el valor de la propiedad calle1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalle1() {
        return calle1;
    }

    /**
     * Define el valor de la propiedad calle1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalle1(String value) {
        this.calle1 = value;
    }

    /**
     * Obtiene el valor de la propiedad calle2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalle2() {
        return calle2;
    }

    /**
     * Define el valor de la propiedad calle2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalle2(String value) {
        this.calle2 = value;
    }

    /**
     * Obtiene el valor de la propiedad referencia.
     * 
     * @return
     *     possible object is
     *     {@link ReferenciaDireccion }
     *     
     */
    public ReferenciaDireccion getReferencia() {
        return referencia;
    }

    /**
     * Define el valor de la propiedad referencia.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenciaDireccion }
     *     
     */
    public void setReferencia(ReferenciaDireccion value) {
        this.referencia = value;
    }

    /**
     * Obtiene el valor de la propiedad nroCalle.
     * 
     */
    public int getNroCalle() {
        return nroCalle;
    }

    /**
     * Define el valor de la propiedad nroCalle.
     * 
     */
    public void setNroCalle(int value) {
        this.nroCalle = value;
    }

    /**
     * Obtiene el valor de la propiedad calle3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalle3() {
        return calle3;
    }

    /**
     * Define el valor de la propiedad calle3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalle3(String value) {
        this.calle3 = value;
    }

    /**
     * Obtiene el valor de la propiedad barrio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarrio() {
        return barrio;
    }

    /**
     * Define el valor de la propiedad barrio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarrio(String value) {
        this.barrio = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudad.
     * 
     * @return
     *     possible object is
     *     {@link Ciudad }
     *     
     */
    public Ciudad getCiudad() {
        return ciudad;
    }

    /**
     * Define el valor de la propiedad ciudad.
     * 
     * @param value
     *     allowed object is
     *     {@link Ciudad }
     *     
     */
    public void setCiudad(Ciudad value) {
        this.ciudad = value;
    }

    /**
     * Obtiene el valor de la propiedad region.
     * 
     * @return
     *     possible object is
     *     {@link Region }
     *     
     */
    public Region getRegion() {
        return region;
    }

    /**
     * Define el valor de la propiedad region.
     * 
     * @param value
     *     allowed object is
     *     {@link Region }
     *     
     */
    public void setRegion(Region value) {
        this.region = value;
    }

    /**
     * Obtiene el valor de la propiedad nroLineaBaja.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroLineaBaja() {
        return nroLineaBaja;
    }

    /**
     * Define el valor de la propiedad nroLineaBaja.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroLineaBaja(String value) {
        this.nroLineaBaja = value;
    }

    /**
     * Obtiene el valor de la propiedad nroCelular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroCelular() {
        return nroCelular;
    }

    /**
     * Define el valor de la propiedad nroCelular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroCelular(String value) {
        this.nroCelular = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad direccionCompleta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionCompleta() {
        return direccionCompleta;
    }

    /**
     * Define el valor de la propiedad direccionCompleta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionCompleta(String value) {
        this.direccionCompleta = value;
    }

}
