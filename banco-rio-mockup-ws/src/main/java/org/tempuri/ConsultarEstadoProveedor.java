
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Acceso" type="{http://tempuri.org/}CredencialAcceso" minOccurs="0"/&gt;
 *         &lt;element name="consultaProveedorRequest" type="{http://tempuri.org/}ConsultaProveedorRequest" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "acceso",
    "consultaProveedorRequest"
})
@XmlRootElement(name = "ConsultarEstadoProveedor")
public class ConsultarEstadoProveedor {

    @XmlElement(name = "Acceso")
    protected CredencialAcceso acceso;
    protected ConsultaProveedorRequest consultaProveedorRequest;

    /**
     * Obtiene el valor de la propiedad acceso.
     * 
     * @return
     *     possible object is
     *     {@link CredencialAcceso }
     *     
     */
    public CredencialAcceso getAcceso() {
        return acceso;
    }

    /**
     * Define el valor de la propiedad acceso.
     * 
     * @param value
     *     allowed object is
     *     {@link CredencialAcceso }
     *     
     */
    public void setAcceso(CredencialAcceso value) {
        this.acceso = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaProveedorRequest.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaProveedorRequest }
     *     
     */
    public ConsultaProveedorRequest getConsultaProveedorRequest() {
        return consultaProveedorRequest;
    }

    /**
     * Define el valor de la propiedad consultaProveedorRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaProveedorRequest }
     *     
     */
    public void setConsultaProveedorRequest(ConsultaProveedorRequest value) {
        this.consultaProveedorRequest = value;
    }

}
