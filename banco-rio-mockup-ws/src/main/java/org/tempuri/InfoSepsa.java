
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para InfoSepsa complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InfoSepsa"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Monto" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="CuentaDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoSepsa", propOrder = {
    "monto",
    "cuentaDebito"
})
public class InfoSepsa {

    @XmlElement(name = "Monto")
    protected double monto;
    @XmlElement(name = "CuentaDebito")
    protected String cuentaDebito;

    /**
     * Obtiene el valor de la propiedad monto.
     * 
     */
    public double getMonto() {
        return monto;
    }

    /**
     * Define el valor de la propiedad monto.
     * 
     */
    public void setMonto(double value) {
        this.monto = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaDebito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaDebito() {
        return cuentaDebito;
    }

    /**
     * Define el valor de la propiedad cuentaDebito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaDebito(String value) {
        this.cuentaDebito = value;
    }

}
