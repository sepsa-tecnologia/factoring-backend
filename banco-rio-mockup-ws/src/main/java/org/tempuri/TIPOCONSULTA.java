
package org.tempuri;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TIPO_CONSULTA.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <pre>
 * &lt;simpleType name="TIPO_CONSULTA"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="POR_ID_SOLICITUD"/&gt;
 *     &lt;enumeration value="POR_RANGO_FECHA_INGRESO"/&gt;
 *     &lt;enumeration value="POR_COMPRADOR"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TIPO_CONSULTA")
@XmlEnum
public enum TIPOCONSULTA {

    POR_ID_SOLICITUD,
    POR_RANGO_FECHA_INGRESO,
    POR_COMPRADOR;

    public String value() {
        return name();
    }

    public static TIPOCONSULTA fromValue(String v) {
        return valueOf(v);
    }

}
