
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para PersonaJuridica complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PersonaJuridica"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}Persona"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FechaConstitucion" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RazonSocial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoSociedad" type="{http://tempuri.org/}TipoSociedad" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonaJuridica", propOrder = {
    "fechaConstitucion",
    "razonSocial",
    "tipoSociedad"
})
public class PersonaJuridica
    extends Persona
{

    @XmlElement(name = "FechaConstitucion", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaConstitucion;
    @XmlElement(name = "RazonSocial")
    protected String razonSocial;
    @XmlElement(name = "TipoSociedad")
    protected TipoSociedad tipoSociedad;

    /**
     * Obtiene el valor de la propiedad fechaConstitucion.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaConstitucion() {
        return fechaConstitucion;
    }

    /**
     * Define el valor de la propiedad fechaConstitucion.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaConstitucion(XMLGregorianCalendar value) {
        this.fechaConstitucion = value;
    }

    /**
     * Obtiene el valor de la propiedad razonSocial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * Define el valor de la propiedad razonSocial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazonSocial(String value) {
        this.razonSocial = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoSociedad.
     * 
     * @return
     *     possible object is
     *     {@link TipoSociedad }
     *     
     */
    public TipoSociedad getTipoSociedad() {
        return tipoSociedad;
    }

    /**
     * Define el valor de la propiedad tipoSociedad.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoSociedad }
     *     
     */
    public void setTipoSociedad(TipoSociedad value) {
        this.tipoSociedad = value;
    }

}
