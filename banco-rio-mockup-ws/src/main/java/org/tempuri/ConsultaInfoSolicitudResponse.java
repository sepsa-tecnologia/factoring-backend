
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultaInfoSolicitudResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultaInfoSolicitudResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Request" type="{http://tempuri.org/}ConsultaInfoSolicitudRequest" minOccurs="0"/&gt;
 *         &lt;element name="ListaSolicitud" type="{http://tempuri.org/}ArrayOfSolicitudAdelanto" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaInfoSolicitudResponse", propOrder = {
    "request",
    "listaSolicitud"
})
public class ConsultaInfoSolicitudResponse
    extends BaseResponse
{

    @XmlElement(name = "Request")
    protected ConsultaInfoSolicitudRequest request;
    @XmlElement(name = "ListaSolicitud")
    protected ArrayOfSolicitudAdelanto listaSolicitud;

    /**
     * Obtiene el valor de la propiedad request.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaInfoSolicitudRequest }
     *     
     */
    public ConsultaInfoSolicitudRequest getRequest() {
        return request;
    }

    /**
     * Define el valor de la propiedad request.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaInfoSolicitudRequest }
     *     
     */
    public void setRequest(ConsultaInfoSolicitudRequest value) {
        this.request = value;
    }

    /**
     * Obtiene el valor de la propiedad listaSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSolicitudAdelanto }
     *     
     */
    public ArrayOfSolicitudAdelanto getListaSolicitud() {
        return listaSolicitud;
    }

    /**
     * Define el valor de la propiedad listaSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSolicitudAdelanto }
     *     
     */
    public void setListaSolicitud(ArrayOfSolicitudAdelanto value) {
        this.listaSolicitud = value;
    }

}
