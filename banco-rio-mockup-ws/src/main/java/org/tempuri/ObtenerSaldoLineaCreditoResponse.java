
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ObtenerSaldoLineaCreditoResult" type="{http://tempuri.org/}SaldoLineaCreditoResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "obtenerSaldoLineaCreditoResult"
})
@XmlRootElement(name = "ObtenerSaldoLineaCreditoResponse")
public class ObtenerSaldoLineaCreditoResponse {

    @XmlElement(name = "ObtenerSaldoLineaCreditoResult")
    protected SaldoLineaCreditoResponse obtenerSaldoLineaCreditoResult;

    /**
     * Obtiene el valor de la propiedad obtenerSaldoLineaCreditoResult.
     * 
     * @return
     *     possible object is
     *     {@link SaldoLineaCreditoResponse }
     *     
     */
    public SaldoLineaCreditoResponse getObtenerSaldoLineaCreditoResult() {
        return obtenerSaldoLineaCreditoResult;
    }

    /**
     * Define el valor de la propiedad obtenerSaldoLineaCreditoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link SaldoLineaCreditoResponse }
     *     
     */
    public void setObtenerSaldoLineaCreditoResult(SaldoLineaCreditoResponse value) {
        this.obtenerSaldoLineaCreditoResult = value;
    }

}
