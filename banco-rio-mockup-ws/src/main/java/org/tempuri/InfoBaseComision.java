
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para InfoBaseComision complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InfoBaseComision"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Porcentaje" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="Monto" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="CuentaCredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoBaseComision", propOrder = {
    "porcentaje",
    "monto",
    "cuentaCredito"
})
@XmlSeeAlso({
    InfoComprador.class,
    InfoBanco.class
})
public abstract class InfoBaseComision {

    @XmlElement(name = "Porcentaje")
    protected double porcentaje;
    @XmlElement(name = "Monto")
    protected double monto;
    @XmlElement(name = "CuentaCredito")
    protected String cuentaCredito;

    /**
     * Obtiene el valor de la propiedad porcentaje.
     * 
     */
    public double getPorcentaje() {
        return porcentaje;
    }

    /**
     * Define el valor de la propiedad porcentaje.
     * 
     */
    public void setPorcentaje(double value) {
        this.porcentaje = value;
    }

    /**
     * Obtiene el valor de la propiedad monto.
     * 
     */
    public double getMonto() {
        return monto;
    }

    /**
     * Define el valor de la propiedad monto.
     * 
     */
    public void setMonto(double value) {
        this.monto = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCredito() {
        return cuentaCredito;
    }

    /**
     * Define el valor de la propiedad cuentaCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCredito(String value) {
        this.cuentaCredito = value;
    }

}
