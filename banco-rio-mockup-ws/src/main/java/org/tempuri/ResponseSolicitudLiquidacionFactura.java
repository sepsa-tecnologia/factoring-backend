
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseSolicitudLiquidacionFactura complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseSolicitudLiquidacionFactura"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Request" type="{http://tempuri.org/}RequestSolicitudLiquidacionFactura" minOccurs="0"/&gt;
 *         &lt;element name="IdLiquidacion" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseSolicitudLiquidacionFactura", propOrder = {
    "request",
    "idLiquidacion"
})
public class ResponseSolicitudLiquidacionFactura
    extends BaseResponse
{

    @XmlElement(name = "Request")
    protected RequestSolicitudLiquidacionFactura request;
    @XmlElement(name = "IdLiquidacion")
    protected long idLiquidacion;

    /**
     * Obtiene el valor de la propiedad request.
     * 
     * @return
     *     possible object is
     *     {@link RequestSolicitudLiquidacionFactura }
     *     
     */
    public RequestSolicitudLiquidacionFactura getRequest() {
        return request;
    }

    /**
     * Define el valor de la propiedad request.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestSolicitudLiquidacionFactura }
     *     
     */
    public void setRequest(RequestSolicitudLiquidacionFactura value) {
        this.request = value;
    }

    /**
     * Obtiene el valor de la propiedad idLiquidacion.
     * 
     */
    public long getIdLiquidacion() {
        return idLiquidacion;
    }

    /**
     * Define el valor de la propiedad idLiquidacion.
     * 
     */
    public void setIdLiquidacion(long value) {
        this.idLiquidacion = value;
    }

}
