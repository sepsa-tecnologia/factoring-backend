
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultaInfoSolicitudRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultaInfoSolicitudRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Comprador" type="{http://tempuri.org/}Comprador" minOccurs="0"/&gt;
 *         &lt;element name="TipoConsulta" type="{http://tempuri.org/}TIPO_CONSULTA"/&gt;
 *         &lt;element name="IdSolicitud" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="RangoDeFecha" type="{http://tempuri.org/}ArrayOfDateTime" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaInfoSolicitudRequest", propOrder = {
    "comprador",
    "tipoConsulta",
    "idSolicitud",
    "rangoDeFecha"
})
public class ConsultaInfoSolicitudRequest {

    @XmlElement(name = "Comprador")
    protected Comprador comprador;
    @XmlElement(name = "TipoConsulta", required = true)
    @XmlSchemaType(name = "string")
    protected TIPOCONSULTA tipoConsulta;
    @XmlElement(name = "IdSolicitud")
    protected long idSolicitud;
    @XmlElement(name = "RangoDeFecha")
    protected ArrayOfDateTime rangoDeFecha;

    /**
     * Obtiene el valor de la propiedad comprador.
     * 
     * @return
     *     possible object is
     *     {@link Comprador }
     *     
     */
    public Comprador getComprador() {
        return comprador;
    }

    /**
     * Define el valor de la propiedad comprador.
     * 
     * @param value
     *     allowed object is
     *     {@link Comprador }
     *     
     */
    public void setComprador(Comprador value) {
        this.comprador = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoConsulta.
     * 
     * @return
     *     possible object is
     *     {@link TIPOCONSULTA }
     *     
     */
    public TIPOCONSULTA getTipoConsulta() {
        return tipoConsulta;
    }

    /**
     * Define el valor de la propiedad tipoConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link TIPOCONSULTA }
     *     
     */
    public void setTipoConsulta(TIPOCONSULTA value) {
        this.tipoConsulta = value;
    }

    /**
     * Obtiene el valor de la propiedad idSolicitud.
     * 
     */
    public long getIdSolicitud() {
        return idSolicitud;
    }

    /**
     * Define el valor de la propiedad idSolicitud.
     * 
     */
    public void setIdSolicitud(long value) {
        this.idSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad rangoDeFecha.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDateTime }
     *     
     */
    public ArrayOfDateTime getRangoDeFecha() {
        return rangoDeFecha;
    }

    /**
     * Define el valor de la propiedad rangoDeFecha.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDateTime }
     *     
     */
    public void setRangoDeFecha(ArrayOfDateTime value) {
        this.rangoDeFecha = value;
    }

}
