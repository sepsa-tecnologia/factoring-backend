
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para InfoComisionComprador complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InfoComisionComprador"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PorcentajeComision" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="MontoComision" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="NroCuentaCredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoComisionComprador", propOrder = {
    "porcentajeComision",
    "montoComision",
    "nroCuentaCredito"
})
public class InfoComisionComprador {

    @XmlElement(name = "PorcentajeComision")
    protected double porcentajeComision;
    @XmlElement(name = "MontoComision")
    protected double montoComision;
    @XmlElement(name = "NroCuentaCredito")
    protected String nroCuentaCredito;

    /**
     * Obtiene el valor de la propiedad porcentajeComision.
     * 
     */
    public double getPorcentajeComision() {
        return porcentajeComision;
    }

    /**
     * Define el valor de la propiedad porcentajeComision.
     * 
     */
    public void setPorcentajeComision(double value) {
        this.porcentajeComision = value;
    }

    /**
     * Obtiene el valor de la propiedad montoComision.
     * 
     */
    public double getMontoComision() {
        return montoComision;
    }

    /**
     * Define el valor de la propiedad montoComision.
     * 
     */
    public void setMontoComision(double value) {
        this.montoComision = value;
    }

    /**
     * Obtiene el valor de la propiedad nroCuentaCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroCuentaCredito() {
        return nroCuentaCredito;
    }

    /**
     * Define el valor de la propiedad nroCuentaCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroCuentaCredito(String value) {
        this.nroCuentaCredito = value;
    }

}
