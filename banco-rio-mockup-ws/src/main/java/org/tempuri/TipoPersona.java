
package org.tempuri;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TipoPersona.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <pre>
 * &lt;simpleType name="TipoPersona"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="FISICA"/&gt;
 *     &lt;enumeration value="JURIDICA"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TipoPersona")
@XmlEnum
public enum TipoPersona {

    FISICA,
    JURIDICA;

    public String value() {
        return name();
    }

    public static TipoPersona fromValue(String v) {
        return valueOf(v);
    }

}
