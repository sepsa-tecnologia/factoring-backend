
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para InfoComisionBanco complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InfoComisionBanco"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PorcentajeComision" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="MontoComision" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoComisionBanco", propOrder = {
    "porcentajeComision",
    "montoComision"
})
public class InfoComisionBanco {

    @XmlElement(name = "PorcentajeComision")
    protected double porcentajeComision;
    @XmlElement(name = "MontoComision")
    protected double montoComision;

    /**
     * Obtiene el valor de la propiedad porcentajeComision.
     * 
     */
    public double getPorcentajeComision() {
        return porcentajeComision;
    }

    /**
     * Define el valor de la propiedad porcentajeComision.
     * 
     */
    public void setPorcentajeComision(double value) {
        this.porcentajeComision = value;
    }

    /**
     * Obtiene el valor de la propiedad montoComision.
     * 
     */
    public double getMontoComision() {
        return montoComision;
    }

    /**
     * Define el valor de la propiedad montoComision.
     * 
     */
    public void setMontoComision(double value) {
        this.montoComision = value;
    }

}
