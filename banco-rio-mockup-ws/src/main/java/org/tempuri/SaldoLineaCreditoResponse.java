
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SaldoLineaCreditoResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SaldoLineaCreditoResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InfoRequest" type="{http://tempuri.org/}SaldoLineaCreditoRequest" minOccurs="0"/&gt;
 *         &lt;element name="MontoLineaCredito" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="MontoUtilizado" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="MontoPendienteAutorizacion" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="MontoEstaOperacion" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="MontoDisponible" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaldoLineaCreditoResponse", propOrder = {
    "infoRequest",
    "montoLineaCredito",
    "montoUtilizado",
    "montoPendienteAutorizacion",
    "montoEstaOperacion",
    "montoDisponible"
})
public class SaldoLineaCreditoResponse
    extends BaseResponse
{

    @XmlElement(name = "InfoRequest")
    protected SaldoLineaCreditoRequest infoRequest;
    @XmlElement(name = "MontoLineaCredito")
    protected double montoLineaCredito;
    @XmlElement(name = "MontoUtilizado")
    protected double montoUtilizado;
    @XmlElement(name = "MontoPendienteAutorizacion")
    protected double montoPendienteAutorizacion;
    @XmlElement(name = "MontoEstaOperacion")
    protected double montoEstaOperacion;
    @XmlElement(name = "MontoDisponible")
    protected double montoDisponible;

    /**
     * Obtiene el valor de la propiedad infoRequest.
     * 
     * @return
     *     possible object is
     *     {@link SaldoLineaCreditoRequest }
     *     
     */
    public SaldoLineaCreditoRequest getInfoRequest() {
        return infoRequest;
    }

    /**
     * Define el valor de la propiedad infoRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link SaldoLineaCreditoRequest }
     *     
     */
    public void setInfoRequest(SaldoLineaCreditoRequest value) {
        this.infoRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad montoLineaCredito.
     * 
     */
    public double getMontoLineaCredito() {
        return montoLineaCredito;
    }

    /**
     * Define el valor de la propiedad montoLineaCredito.
     * 
     */
    public void setMontoLineaCredito(double value) {
        this.montoLineaCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad montoUtilizado.
     * 
     */
    public double getMontoUtilizado() {
        return montoUtilizado;
    }

    /**
     * Define el valor de la propiedad montoUtilizado.
     * 
     */
    public void setMontoUtilizado(double value) {
        this.montoUtilizado = value;
    }

    /**
     * Obtiene el valor de la propiedad montoPendienteAutorizacion.
     * 
     */
    public double getMontoPendienteAutorizacion() {
        return montoPendienteAutorizacion;
    }

    /**
     * Define el valor de la propiedad montoPendienteAutorizacion.
     * 
     */
    public void setMontoPendienteAutorizacion(double value) {
        this.montoPendienteAutorizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad montoEstaOperacion.
     * 
     */
    public double getMontoEstaOperacion() {
        return montoEstaOperacion;
    }

    /**
     * Define el valor de la propiedad montoEstaOperacion.
     * 
     */
    public void setMontoEstaOperacion(double value) {
        this.montoEstaOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad montoDisponible.
     * 
     */
    public double getMontoDisponible() {
        return montoDisponible;
    }

    /**
     * Define el valor de la propiedad montoDisponible.
     * 
     */
    public void setMontoDisponible(double value) {
        this.montoDisponible = value;
    }

}
