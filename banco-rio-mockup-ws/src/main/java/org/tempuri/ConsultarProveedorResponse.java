
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsultarProveedorResult" type="{http://tempuri.org/}InfoProveedorResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarProveedorResult"
})
@XmlRootElement(name = "ConsultarProveedorResponse")
public class ConsultarProveedorResponse {

    @XmlElement(name = "ConsultarProveedorResult")
    protected InfoProveedorResponse consultarProveedorResult;

    /**
     * Obtiene el valor de la propiedad consultarProveedorResult.
     * 
     * @return
     *     possible object is
     *     {@link InfoProveedorResponse }
     *     
     */
    public InfoProveedorResponse getConsultarProveedorResult() {
        return consultarProveedorResult;
    }

    /**
     * Define el valor de la propiedad consultarProveedorResult.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoProveedorResponse }
     *     
     */
    public void setConsultarProveedorResult(InfoProveedorResponse value) {
        this.consultarProveedorResult = value;
    }

}
