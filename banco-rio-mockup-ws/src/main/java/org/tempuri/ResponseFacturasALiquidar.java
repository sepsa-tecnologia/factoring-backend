
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseFacturasALiquidar complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseFacturasALiquidar"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://tempuri.org/}BaseResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RequestFacturasALiquidar" type="{http://tempuri.org/}RequestFacturasALiquidar" minOccurs="0"/&gt;
 *         &lt;element name="ListaFacturas" type="{http://tempuri.org/}ArrayOfInfoFacturaALiquidar" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseFacturasALiquidar", propOrder = {
    "requestFacturasALiquidar",
    "listaFacturas"
})
public class ResponseFacturasALiquidar
    extends BaseResponse
{

    @XmlElement(name = "RequestFacturasALiquidar")
    protected RequestFacturasALiquidar requestFacturasALiquidar;
    @XmlElement(name = "ListaFacturas")
    protected ArrayOfInfoFacturaALiquidar listaFacturas;

    /**
     * Obtiene el valor de la propiedad requestFacturasALiquidar.
     * 
     * @return
     *     possible object is
     *     {@link RequestFacturasALiquidar }
     *     
     */
    public RequestFacturasALiquidar getRequestFacturasALiquidar() {
        return requestFacturasALiquidar;
    }

    /**
     * Define el valor de la propiedad requestFacturasALiquidar.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestFacturasALiquidar }
     *     
     */
    public void setRequestFacturasALiquidar(RequestFacturasALiquidar value) {
        this.requestFacturasALiquidar = value;
    }

    /**
     * Obtiene el valor de la propiedad listaFacturas.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInfoFacturaALiquidar }
     *     
     */
    public ArrayOfInfoFacturaALiquidar getListaFacturas() {
        return listaFacturas;
    }

    /**
     * Define el valor de la propiedad listaFacturas.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInfoFacturaALiquidar }
     *     
     */
    public void setListaFacturas(ArrayOfInfoFacturaALiquidar value) {
        this.listaFacturas = value;
    }

}
