
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Persona complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Persona"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Comprador" type="{http://tempuri.org/}Comprador" minOccurs="0"/&gt;
 *         &lt;element name="CodCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoPersona" type="{http://tempuri.org/}TipoPersona"/&gt;
 *         &lt;element name="TipoDocumento" type="{http://tempuri.org/}TipoDocumento" minOccurs="0"/&gt;
 *         &lt;element name="NroDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RazonSocialNombres" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Direccion" type="{http://tempuri.org/}PersonaDireccion" minOccurs="0"/&gt;
 *         &lt;element name="ActividadEconomica" type="{http://tempuri.org/}ActividadEconomica" minOccurs="0"/&gt;
 *         &lt;element name="PromedioMensual" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="DescripcionRubro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DocumentoDeIdentidad" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="EmailNotificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InfoCuentaAcreditacion" type="{http://tempuri.org/}InfoCuentaAcreditacion" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Persona", propOrder = {
    "comprador",
    "codCliente",
    "tipoPersona",
    "tipoDocumento",
    "nroDocumento",
    "razonSocialNombres",
    "direccion",
    "actividadEconomica",
    "promedioMensual",
    "descripcionRubro",
    "documentoDeIdentidad",
    "emailNotificacion",
    "infoCuentaAcreditacion"
})
@XmlSeeAlso({
    PersonaFisica.class,
    PersonaJuridica.class,
    Comprador.class,
    Proveedor.class
})
public abstract class Persona {

    @XmlElement(name = "Comprador")
    protected Comprador comprador;
    @XmlElement(name = "CodCliente")
    protected String codCliente;
    @XmlElement(name = "TipoPersona", required = true)
    @XmlSchemaType(name = "string")
    protected TipoPersona tipoPersona;
    @XmlElement(name = "TipoDocumento")
    protected TipoDocumento tipoDocumento;
    @XmlElement(name = "NroDocumento")
    protected String nroDocumento;
    @XmlElement(name = "RazonSocialNombres")
    protected String razonSocialNombres;
    @XmlElement(name = "Direccion")
    protected PersonaDireccion direccion;
    @XmlElement(name = "ActividadEconomica")
    protected ActividadEconomica actividadEconomica;
    @XmlElement(name = "PromedioMensual")
    protected double promedioMensual;
    @XmlElement(name = "DescripcionRubro")
    protected String descripcionRubro;
    @XmlElement(name = "DocumentoDeIdentidad")
    protected byte[] documentoDeIdentidad;
    @XmlElement(name = "EmailNotificacion")
    protected String emailNotificacion;
    @XmlElement(name = "InfoCuentaAcreditacion")
    protected InfoCuentaAcreditacion infoCuentaAcreditacion;

    /**
     * Obtiene el valor de la propiedad comprador.
     * 
     * @return
     *     possible object is
     *     {@link Comprador }
     *     
     */
    public Comprador getComprador() {
        return comprador;
    }

    /**
     * Define el valor de la propiedad comprador.
     * 
     * @param value
     *     allowed object is
     *     {@link Comprador }
     *     
     */
    public void setComprador(Comprador value) {
        this.comprador = value;
    }

    /**
     * Obtiene el valor de la propiedad codCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCliente() {
        return codCliente;
    }

    /**
     * Define el valor de la propiedad codCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCliente(String value) {
        this.codCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoPersona.
     * 
     * @return
     *     possible object is
     *     {@link TipoPersona }
     *     
     */
    public TipoPersona getTipoPersona() {
        return tipoPersona;
    }

    /**
     * Define el valor de la propiedad tipoPersona.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPersona }
     *     
     */
    public void setTipoPersona(TipoPersona value) {
        this.tipoPersona = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumento }
     *     
     */
    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Define el valor de la propiedad tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumento }
     *     
     */
    public void setTipoDocumento(TipoDocumento value) {
        this.tipoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad nroDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroDocumento() {
        return nroDocumento;
    }

    /**
     * Define el valor de la propiedad nroDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroDocumento(String value) {
        this.nroDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad razonSocialNombres.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazonSocialNombres() {
        return razonSocialNombres;
    }

    /**
     * Define el valor de la propiedad razonSocialNombres.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazonSocialNombres(String value) {
        this.razonSocialNombres = value;
    }

    /**
     * Obtiene el valor de la propiedad direccion.
     * 
     * @return
     *     possible object is
     *     {@link PersonaDireccion }
     *     
     */
    public PersonaDireccion getDireccion() {
        return direccion;
    }

    /**
     * Define el valor de la propiedad direccion.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaDireccion }
     *     
     */
    public void setDireccion(PersonaDireccion value) {
        this.direccion = value;
    }

    /**
     * Obtiene el valor de la propiedad actividadEconomica.
     * 
     * @return
     *     possible object is
     *     {@link ActividadEconomica }
     *     
     */
    public ActividadEconomica getActividadEconomica() {
        return actividadEconomica;
    }

    /**
     * Define el valor de la propiedad actividadEconomica.
     * 
     * @param value
     *     allowed object is
     *     {@link ActividadEconomica }
     *     
     */
    public void setActividadEconomica(ActividadEconomica value) {
        this.actividadEconomica = value;
    }

    /**
     * Obtiene el valor de la propiedad promedioMensual.
     * 
     */
    public double getPromedioMensual() {
        return promedioMensual;
    }

    /**
     * Define el valor de la propiedad promedioMensual.
     * 
     */
    public void setPromedioMensual(double value) {
        this.promedioMensual = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionRubro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionRubro() {
        return descripcionRubro;
    }

    /**
     * Define el valor de la propiedad descripcionRubro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionRubro(String value) {
        this.descripcionRubro = value;
    }

    /**
     * Obtiene el valor de la propiedad documentoDeIdentidad.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getDocumentoDeIdentidad() {
        return documentoDeIdentidad;
    }

    /**
     * Define el valor de la propiedad documentoDeIdentidad.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setDocumentoDeIdentidad(byte[] value) {
        this.documentoDeIdentidad = value;
    }

    /**
     * Obtiene el valor de la propiedad emailNotificacion.
     *
     * @return possible object is {@link String }
     *
     */
    public String getEmailNotificacion() {
        return emailNotificacion;
    }

    /**
     * Define el valor de la propiedad emailNotificacion.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setEmailNotificacion(String value) {
        this.emailNotificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad infoCuentaAcreditacion.
     * 
     * @return
     *     possible object is
     *     {@link InfoCuentaAcreditacion }
     *     
     */
    public InfoCuentaAcreditacion getInfoCuentaAcreditacion() {
        return infoCuentaAcreditacion;
    }

    /**
     * Define el valor de la propiedad infoCuentaAcreditacion.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoCuentaAcreditacion }
     *     
     */
    public void setInfoCuentaAcreditacion(InfoCuentaAcreditacion value) {
        this.infoCuentaAcreditacion = value;
    }

}
