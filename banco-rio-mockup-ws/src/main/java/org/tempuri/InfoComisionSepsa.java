
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para InfoComisionSepsa complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InfoComisionSepsa"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MontoDebito" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="NroCuentaDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoComisionSepsa", propOrder = {
    "montoDebito",
    "nroCuentaDebito"
})
public class InfoComisionSepsa {

    @XmlElement(name = "MontoDebito")
    protected double montoDebito;
    @XmlElement(name = "NroCuentaDebito")
    protected String nroCuentaDebito;

    /**
     * Obtiene el valor de la propiedad montoDebito.
     * 
     */
    public double getMontoDebito() {
        return montoDebito;
    }

    /**
     * Define el valor de la propiedad montoDebito.
     * 
     */
    public void setMontoDebito(double value) {
        this.montoDebito = value;
    }

    /**
     * Obtiene el valor de la propiedad nroCuentaDebito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroCuentaDebito() {
        return nroCuentaDebito;
    }

    /**
     * Define el valor de la propiedad nroCuentaDebito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroCuentaDebito(String value) {
        this.nroCuentaDebito = value;
    }

}
