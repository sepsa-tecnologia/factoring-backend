/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.impl;

import fa.gs.utils.collections.Lists;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.fechas.Fechas;
import fa.gs.utils.misc.fechas.Formats;
import java.io.File;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import javax.xml.datatype.DatatypeFactory;
import org.tempuri.ArrayOfComprador;
import org.tempuri.ArrayOfInfoFacturaALiquidar;
import org.tempuri.ArrayOfInfoProveedor;
import org.tempuri.ArrayOfSolicitudAdelanto;
import org.tempuri.ESTADO;
import org.tempuri.ESTADOLIQUIDACION;
import org.tempuri.InfoFacturaALiquidar;
import org.tempuri.InfoProveedor;
import org.tempuri.InfoSolicitudLiquidacion;
import org.tempuri.SimuladorCalculoDetalle;
import org.tempuri.SolicitudAdelanto;

/**
 *
 * @author Fabio A. González Sosa
 */
public class WsFactoringImpl {

    private static long stamp() {
        String stamp = Fechas.toString(Fechas.now(), Formats.YYYYMMDDHHMMSS);
        return Long.parseLong(stamp);
    }

    public static org.tempuri.ResponseSolicitudLiquidacionFactura solicitarLiquidacionDeFacturas(org.tempuri.CredencialAcceso acceso, org.tempuri.RequestSolicitudLiquidacionFactura request) {
        org.tempuri.ResponseSolicitudLiquidacionFactura response = new org.tempuri.ResponseSolicitudLiquidacionFactura();
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setIdLiquidacion(stamp());
        return response;
    }

    public static org.tempuri.ConsultaSolicitudLiqResponse consultarLiquidacion(org.tempuri.CredencialAcceso acceso, org.tempuri.ConsultaSolicitudLiqRequest request) {
        org.tempuri.ConsultaSolicitudLiqResponse response = new org.tempuri.ConsultaSolicitudLiqResponse();
        response.setCodigoError(0);
        response.setMensaje("LIQUIDACION APROBADA");
        response.setInfoSolicitud(new InfoSolicitudLiquidacion());
        response.getInfoSolicitud().setEstado(ESTADOLIQUIDACION.APROBADO);
        return response;
    }

    public static org.tempuri.ConsultaInfoSolicitudResponse consultarSolicitud(org.tempuri.CredencialAcceso acceso, org.tempuri.ConsultaInfoSolicitudRequest request) {
        SolicitudAdelanto adelanto = new SolicitudAdelanto();
        adelanto.setEstado("APROBADA");
        adelanto.setIdSolicitud(request.getIdSolicitud());
        adelanto.setComentarioDecision("DESEMBOLSO APROBADO");

        org.tempuri.ConsultaInfoSolicitudResponse response = new org.tempuri.ConsultaInfoSolicitudResponse();
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setListaSolicitud(new ArrayOfSolicitudAdelanto());
        response.getListaSolicitud().getSolicitudAdelanto().add(adelanto);
        return response;
    }

    public static org.tempuri.AltaCompradorProveedorResponse altaProveedorFisico(org.tempuri.CredencialAcceso acceso, org.tempuri.PersonaFisica personaFisica) {
        org.tempuri.AltaCompradorProveedorResponse response = new org.tempuri.AltaCompradorProveedorResponse();
        response.setCodigoError(0);
        response.setMensaje("SOLICITUD RECIBIDA");
        response.setCodCliente("C" + personaFisica.getNroDocumento());
        return response;
    }

    public static org.tempuri.SolicitudAdelantoResponse solicitarAdelantoDeFacturas(org.tempuri.CredencialAcceso acceso, org.tempuri.SolicitudAdelanteRequest solicitudRequest) {
        org.tempuri.SolicitudAdelantoResponse response = new org.tempuri.SolicitudAdelantoResponse();
        response.setCodigoError(0);
        response.setMensaje("SOLICITUD DE ADELANTO RECIBIDO");
        response.setSolicitudAdelanto(new SolicitudAdelanto());
        response.getSolicitudAdelanto().setIdSolicitud(stamp());
        response.getSolicitudAdelanto().setNroOperacion(Fechas.toCompactString(Fechas.now()));
        return response;
    }

    public static org.tempuri.ParametrosResponse obtenerParametros(org.tempuri.CredencialAcceso acceso) {
        throw Errors.unsupported();
    }

    public static org.tempuri.ResponseFacturasALiquidar consultarFacturasALiquidar(org.tempuri.CredencialAcceso acceso, org.tempuri.RequestFacturasALiquidar request) {
        org.tempuri.ResponseFacturasALiquidar response = new org.tempuri.ResponseFacturasALiquidar();
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setListaFacturas(new ArrayOfInfoFacturaALiquidar());
        for (InfoFacturaALiquidar factura : loadFacturasCobradas()) {
            response.getListaFacturas().getInfoFacturaALiquidar().add(factura);
        }
        return response;
    }

    private static List<InfoFacturaALiquidar> loadFacturasCobradas() {
        List<InfoFacturaALiquidar> facturas = Lists.empty();
        try {
            File input = new File("D:/Fabio/Proyectos/SEPSA/Factoring/web/banco-rio-mockup-ws/facturas_cobradas.txt");
            List<String> lines = Files.readAllLines(input.toPath());
            for (int i = 1; i < lines.size(); i++) {
                String line = lines.get(i).trim();
                if (Assertions.stringNullOrEmpty(line)) {
                    continue;
                }

                String[] parts = line.split(";");
                InfoFacturaALiquidar factura = new InfoFacturaALiquidar();
                factura.setNroFactura(parts[0]);
                factura.setFechaVto(DatatypeFactory.newInstance().newXMLGregorianCalendar(parts[1] + "T00:00:00"));
                factura.setMonto(Double.valueOf(parts[2]));
                factura.setIdSolicitud(Long.valueOf(parts[3]));
                factura.setNroOperacion(parts[4]);
                facturas.add(factura);
            }
        } catch (Throwable thr) {
            thr.printStackTrace(System.err);
        }
        return facturas;
    }

    public static org.tempuri.SimuladorResponse calcularNetoFacturas(org.tempuri.CredencialAcceso acceso, org.tempuri.SimuladorRequest simuladorRequest) {
        org.tempuri.SimuladorResponse response = new org.tempuri.SimuladorResponse();
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setSimuladorCalculo(simuladorRequest.getSimulador());
        if (!Assertions.isNullOrEmpty(simuladorRequest.getSimulador().getDetalleDeCalculo().getSimuladorCalculoDetalle())) {
            for (SimuladorCalculoDetalle solicitudCalculo : simuladorRequest.getSimulador().getDetalleDeCalculo().getSimuladorCalculoDetalle()) {
                double interes = Math.ceil(2.0 + (15.0 - 2.0) * ThreadLocalRandom.current().nextDouble());
                double comision = Math.ceil((solicitudCalculo.getMonto() * interes) / 100.0);
                double neto = solicitudCalculo.getMonto() - comision;
                long diasAdelanto = TimeUnit.DAYS.convert(Fechas.toEpoch(solicitudCalculo.getFechaVto().toGregorianCalendar().getTime()) - Fechas.toEpoch(Fechas.now()), TimeUnit.MILLISECONDS);

                solicitudCalculo.setEstado("0");
                solicitudCalculo.setMensaje("OK");
                solicitudCalculo.setDiasDeInteres((int) diasAdelanto);
                solicitudCalculo.setInteres(comision);
                solicitudCalculo.setNeto(neto);
                solicitudCalculo.setSeguro(0);
                solicitudCalculo.setIvaSeguro(0);
                solicitudCalculo.setGastosAdm(0);
                solicitudCalculo.setIvaGastosAdm(0);
            }
        }
        return response;
    }

    public static org.tempuri.AltaCompradorProveedorResponse altaProveedorJuridico(org.tempuri.CredencialAcceso acceso, org.tempuri.PersonaJuridica personaJuridica) {
        org.tempuri.AltaCompradorProveedorResponse response = new org.tempuri.AltaCompradorProveedorResponse();
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setCodCliente("C" + personaJuridica.getNroDocumento());
        return response;
    }

    public static org.tempuri.SaldoLineaCreditoResponse obtenerSaldoLineaCredito(org.tempuri.CredencialAcceso acceso, org.tempuri.SaldoLineaCreditoRequest saldoLineaRequest) {
        org.tempuri.SaldoLineaCreditoResponse response = new org.tempuri.SaldoLineaCreditoResponse();
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setMontoLineaCredito(150000000);
        response.setMontoDisponible(150000000);
        response.setMontoEstaOperacion(0);
        response.setMontoUtilizado(0);
        response.setMontoPendienteAutorizacion(0);
        return response;
    }

    public static org.tempuri.ConsultaCompradoresResponse consultarCompradores(org.tempuri.CredencialAcceso acceso) {
        org.tempuri.ConsultaCompradoresResponse response = new org.tempuri.ConsultaCompradoresResponse();
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setCompradores(new ArrayOfComprador());
        return response;
    }

    public static org.tempuri.ConsultaProveedorResponse consultarEstadoProveedor(org.tempuri.CredencialAcceso acceso, org.tempuri.ConsultaProveedorRequest consultaProveedorRequest) {
        InfoProveedor estadoProveedor = new InfoProveedor();
        estadoProveedor.setFechaInsercion(Fechas.toString(Fechas.now()));
        estadoProveedor.setEstado(ESTADO.APROBADO);
        estadoProveedor.setComentarioDecision("ADHESION APROBADA");
        estadoProveedor.setCodigoDeProveedor("C" + consultaProveedorRequest.getProveedor().getNroDocumento());

        org.tempuri.ConsultaProveedorResponse response = new org.tempuri.ConsultaProveedorResponse();
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setInfoProveedor(new ArrayOfInfoProveedor());
        response.getInfoProveedor().getInfoProveedor().add(estadoProveedor);
        return response;
    }

    public static org.tempuri.InfoProveedorResponse consultarProveedor(org.tempuri.CredencialAcceso acceso, org.tempuri.InfoProveedorRequest request) {
        throw Errors.unsupported();
    }
}
