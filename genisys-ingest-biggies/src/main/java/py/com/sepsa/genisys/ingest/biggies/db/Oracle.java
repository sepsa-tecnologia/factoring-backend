/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.biggies.db;

import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.text.Strings;
import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Oracle {

    private static final String DB_HOST = "190.128.234.10";
    private static final String DB_PORT = "1524";
    private static final String DB_NAME = "biggie_pro";
    private static final String DB_USER = "SEPSA";
    private static final String DB_PASS = "S3pSa147258";

    static {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrió un error inicializando driver para db Oracle.");
        }
    }

    public static Connection getConnection() throws Throwable {
        String url = Strings.format("jdbc:oracle:thin:%s/%s@%s:%s/%s", DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME);
        return DriverManager.getConnection(url);
    }

}
