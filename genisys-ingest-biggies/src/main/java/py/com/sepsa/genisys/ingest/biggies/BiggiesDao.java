/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.biggies;

import fa.gs.misc.collections.Lists;
import fa.gs.utils.collections.Maps;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.errors.Errors;
import fa.gs.utils.misc.fechas.Fechas;
import fa.gs.utils.misc.text.StringBuilder2;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ingest.biggies.db.DbUtils;
import py.com.sepsa.genisys.ingest.biggies.db.Oracle;
import py.com.sepsa.genisys.ingest.biggies.pojos.FacturaAPagar;
import py.com.sepsa.genisys.ingest.biggies.pojos.OrdenPago;
import py.com.sepsa.genisys.ingest.biggies.pojos.Proveedor;

/**
 *
 * @author Fabio A. González Sosa
 */
public class BiggiesDao implements Serializable {

    private AppLogger log;

    private BiggiesDao() {
        this.log = AppLoggerFactory.ingest("biggies");
    }

    public static BiggiesDao instance() {
        return new BiggiesDao();
    }

    public Collection<Proveedor> obtenerProveedores() {
        Collection<Proveedor> proveedores = Lists.empty();

        // Obtener proveedores de Biggie.
        try (Connection conn = Oracle.getConnection()) {
            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" SELECT ");
            builder.append("   P.ID AS \"ID\", ");
            builder.append("   P.DOCUMENTO AS \"RUC\", ");
            builder.append("   P.DV_DOCUMENTO AS \"DV_RUC\", ");
            builder.append("   P.NOMBRE_LEGAL AS \"RAZON_SOCIAL\", ");
            builder.append("   P.NRO_TIMBRADO AS \"NUMERO_TIMBRADO\", ");
            builder.append("   P.FECHA_VENC_TIMBRADO AS \"VENCIMIENTO_TIMBRADO\", ");
            builder.append("   P.DIAS_PAGO AS \"DIAS_PLAZO\" ");
            builder.append(" FROM TRC.ORDEN_PAGO_PROVEEDORES_VIEW P ");
            builder.append(" WHERE ");
            builder.append("     1 = 1 ");
            builder.append("     AND P.ACTIVO = 'S' ");

            Collection<Map<String, Object>> rows = DbUtils.select(conn, builder.toString());
            for (Map<String, Object> row : rows) {
                // Extraer datos.
                Integer id = Maps.bigdecimal(row, "ID").intValue();
                String ruc = Maps.string(row, "RUC");
                String dvRuc = Maps.string(row, "DV_RUC");
                String razonSocial = Maps.string(row, "RAZON_SOCIAL");
                BigDecimal nroTimbrado = Maps.bigdecimal(row, "NUMERO_TIMBRADO");
                Date vtoTimbrado = Maps.date(row, "VENCIMIENTO_TIMBRADO");
                BigDecimal diasPlazo = Maps.bigdecimal(row, "DIAS_PLAZO");

                // Control.
                if (Assertions.stringNullOrEmpty(ruc) || Assertions.stringNullOrEmpty(dvRuc)) {
                    log.warning("OMITIENDO PROVEEDOR %s: RUC o DV_RUC invalido.", id);
                    continue;
                }

                // Control.
                if (Assertions.stringNullOrEmpty(razonSocial)) {
                    log.warning("OMITIENDO PROVEEDOR %s: RAZON_SOCIAL invalido.", id);
                    continue;
                }

                // Ok.
                Proveedor proveedor = new Proveedor();
                proveedor.setId(id);
                proveedor.setRuc(Strings.format("%s-%s", ruc, dvRuc));
                proveedor.setRazonSocial(razonSocial == null ? "--" : razonSocial);
                proveedor.setNroTimbrado(nroTimbrado);
                proveedor.setVtoTimbrado(vtoTimbrado);
                proveedor.setDiasPlazo(diasPlazo);

                proveedores.add(proveedor);
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr);
        }

        return proveedores;
    }

    public Collection<OrdenPago> obtenerOrdenesPagoAprobadas(Proveedor proveedor) {
        return obtenerOrdenesPago(proveedor, OrdenPago.Estado.APROBADA);
    }

    public Collection<OrdenPago> obtenerOrdenesPagoPendientes(Proveedor proveedor) {
        return obtenerOrdenesPago(proveedor, OrdenPago.Estado.PENDIENTE);
    }

    public Collection<OrdenPago> obtenerOrdenesPagoAnuladas(Proveedor proveedor) {
        return obtenerOrdenesPago(proveedor, OrdenPago.Estado.ANULADA);
    }

    private Collection<OrdenPago> obtenerOrdenesPago(Proveedor proveedor, OrdenPago.Estado estado) {
        Collection<OrdenPago> ordenesPago = Lists.empty();

        // Parametros de fecha.
        Date now = Fechas.now();
        int y = Fechas.getFieldValue(now, Calendar.YEAR);
        int m = Fechas.getFieldValue(now, Calendar.MONTH);

        try (Connection conn = Oracle.getConnection()) {
            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" SELECT ");
            builder.append("     OPC.ID AS \"ID\", ");
            builder.append("     OPC.CHEQUE_PAGO AS \"FECHA_PAGO\" ");
            builder.append(" FROM TRC.ORDEN_PAGO_CABECERA_VIEW OPC ");
            builder.append(" WHERE ");
            builder.append("     1 = 1 ");
            builder.append("     AND OPC.ESTADO = '%s' ", estado.bdValue());
            builder.append("     AND OPC.MONEDA = 'GUARANIES' ");
            builder.append("     AND EXTRACT(YEAR FROM OPC.FECHA) = %s ", y);
            builder.append("     AND EXTRACT(MONTH FROM OPC.FECHA) = %s ", m);
            builder.append("     AND OPC.ID_PROVEEDOR = %s ", proveedor.getId());

            // Ejecutar query.
            Collection<Map<String, Object>> rows = DbUtils.select(conn, builder.toString());
            for (Map<String, Object> row : rows) {
                Integer idOrdenPago = Maps.bigdecimal(row, "ID").intValue();

                OrdenPago ordenPago = new OrdenPago();
                ordenPago.setId(idOrdenPago);
                ordenesPago.add(ordenPago);
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr);
        }

        return ordenesPago;
    }

    public Collection<FacturaAPagar> obtenerFacturas(OrdenPago ordenPago) {
        Collection<FacturaAPagar> facturas = Lists.empty();

        try (Connection conn = Oracle.getConnection()) {
            // Construir query.
            StringBuilder2 builder = new StringBuilder2();
            builder.append(" SELECT ");
            builder.append("     OPD.ID AS \"ID\", ");
            builder.append("     OPD.NRO_COMPROBANTE AS \"NUMERO\", ");
            builder.append("     F.FECHA_COMPROBANTE AS \"FECHA_EMISION\", ");
            builder.append("     F.FECHA_VENCIMIENTO AS \"FECHA_VENCIMIENTO\", ");
            builder.append("     OPD.IMPORTE AS \"MONTO\", ");
            builder.append("     OPD.OBSERVACION AS \"OBSERVACIONES\" ");
            builder.append(" FROM TRC.ORDEN_PAGO_DETALLE_VIEW OPD ");
            builder.append(" LEFT JOIN TRC.ORDEN_PAGO_COMPROBANTES_VIEW F ON OPD.NRO_COMPROBANTE = F.NRO_COMPROBANTE ");
            builder.append(" WHERE ");
            builder.append("     1 = 1 ");
            builder.append("     AND OPD.ID_ORDEN_PAGO = %s ", ordenPago.getId());
            builder.append("     AND (F.TIPO = 'FACTURA DE GASTO' OR F.TIPO = 'FACTURA DE COMPRA') ");

            // Ejecutar query.
            Collection<Map<String, Object>> rows = DbUtils.select(conn, builder.toString());
            for (Map<String, Object> row : rows) {
                Integer id = Maps.bigdecimal(row, "ID").intValue();
                String numero = Maps.string(row, "NUMERO");
                Date fechaEmision = Maps.date(row, "FECHA_EMISION");
                Date fechaPago = Maps.date(row, "FECHA_VENCIMIENTO");
                BigDecimal monto = Maps.bigdecimal(row, "MONTO");
                String observaciones = Maps.string(row, "OBSERVACIONES");

                FacturaAPagar factura = new FacturaAPagar();
                factura.setId(id);
                factura.setNumero(numero);
                factura.setFechaEmision(fechaEmision);
                factura.setFechaPago(fechaPago);
                factura.setMonto(monto);
                factura.setObservaciones(observaciones);
                facturas.add(factura);
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr);
        }

        return facturas;
    }

}
