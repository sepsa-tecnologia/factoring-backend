/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.biggies;

import fa.gs.misc.Assertions;
import fa.gs.result.simple.Result;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.logging.app.AppLogger;
import java.io.Serializable;
import java.util.Collection;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.entities.trans.PersonaRol;
import py.com.sepsa.genisys.ejb.enums.PerfilEnum;
import py.com.sepsa.genisys.ejb.logic.impl.Configuraciones;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Injection;

/**
 *
 * @author Fabio A. González Sosa
 */
public class SepsaDao implements Serializable {

    private AppLogger log;

    private Personas personas;

    private Configuraciones configuraciones;

    private SepsaDao() {
        this.log = AppLoggerFactory.ingest("biggies");
        this.personas = Injection.lookup(Personas.class);
        this.configuraciones = Injection.lookup(Configuraciones.class);
    }

    public static SepsaDao instance() {
        return new SepsaDao();
    }

    public Integer obtenerIdPersonaBiggie() {
        Result<String> result = configuraciones.buscarValorParametro(Configuraciones.Param.INTEGRACION_BIGGIE_ID_PERSONA);
        if (result.isFailure()) {
            return null;
        } else {
            return Integer.valueOf(result.value("-1"), 10);
        }
    }

    public Integer obtenerIdUsuarioBiggie() {
        Result<String> result = configuraciones.buscarValorParametro(Configuraciones.Param.INTEGRACION_BIGGIE_ID_USUARIO);
        if (result.isFailure()) {
            return null;
        } else {
            return Integer.valueOf(result.value("-1"), 10);
        }
    }

    public Collection<Persona> obtenerProveedores(Integer idPersona) throws Throwable {
        Collection<Persona> proveedores = Lists.empty();

        try {
            // Obtener proveedores disponibles.
            Result<Collection<Persona>> resProveedores = personas.obtenerProveedoresDisponibles(idPersona);
            resProveedores.raise();

            // Obtener proveedores.
            Collection<Persona> proveedores0 = resProveedores.value(Lists.empty());
            if (!Assertions.isNullOrEmpty(proveedores0)) {
                proveedores.addAll(proveedores0);
            }
        } catch (Throwable thr) {
            log.debug()
                    .cause(thr)
                    .message("Ocurrió un error obteniendo proveedores de cliente")
                    .log();
        }

        return proveedores;
    }

    public Persona obtenerOCrearProveedor(String razonSocial, String nombreFantasia, String ruc) throws Throwable {
        // Obtener persona.
        Result<Persona> resPersona = personas.obtenerPorRuc(ruc);
        Persona persona = resPersona.value(null);

        // Registrar persona.
        if (persona == null) {
            // Registrar si no existe.
            resPersona = personas.registrarPersonaJuridica(razonSocial, nombreFantasia, ruc, PerfilEnum.PROVEEDOR);
            resPersona.raise(true);
            persona = resPersona.value();
        } else {
            // Agregar rol si ya existe.
            Result<PersonaRol> resRol = personas.agregarRol(persona.getId(), PerfilEnum.PROVEEDOR);
            resRol.raise();
        }

        return persona;
    }

    public void agregarOCrearRelacionComercial(Integer idProveedor, Integer idComprador) throws Throwable {
        Result<Void> result = personas.agregarRelacion(idProveedor, idComprador);
        result.raise();
    }

}
