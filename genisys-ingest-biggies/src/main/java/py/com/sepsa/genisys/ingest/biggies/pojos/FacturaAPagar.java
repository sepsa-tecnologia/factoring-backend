/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.biggies.pojos;

import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class FacturaAPagar implements Serializable {

    private Integer id;
    private String numero;
    private Date fechaEmision;
    private Date fechaPago;
    private BigDecimal monto;
    private String observaciones;

    public String getNumeroComprobanteFormatoSepsa() {
        String a = numero.substring(0, 3);
        String b = numero.substring(3, 6);
        String c = numero.substring(6);
        return Strings.format("%s-%s-%s", a, b, c);
    }

}
