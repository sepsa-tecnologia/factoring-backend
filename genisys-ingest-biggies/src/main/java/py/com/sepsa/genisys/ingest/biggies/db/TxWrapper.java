/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.biggies.db;

import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.logging.app.AppLogger;
import java.io.Serializable;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;

/**
 *
 * @author Fabio A. González Sosa
 */
@Stateless(name = "TxWrapper", mappedName = "TxWrapper")
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class TxWrapper implements Serializable {

    public static final AppLogger log = AppLoggerFactory.core();

    @Resource
    private UserTransaction tx;

    /**
     * Ejecuta un bloque de ejecución arbitrario.
     *
     * @param <T> Tipo de resultado primario manejado dentro del bloque de
     * ejecución.
     * @param executable Abstracción del bloque a ejecutar.
     * @return Resultado de la operacion.
     */
    public <T> Result<T> execute(TxWrapper.Executable<T> executable) {
        // Control de seguridad.
        if (executable == null) {
            throw new IllegalArgumentException("El codigo de ejecucion no puede ser nulo");
        }

        Result<T> result;

        try {
            // Iniciar transaccion.
            log.debug("iniciando transaccion");
            tx.begin();
            try {
                // Ejectura bloque de codigo.
                log.debug("ejecutando transaccion");
                result = executable.execute();
                result.raise();

                // Confirmar (commitear) cambios realizados.
                log.debug("finalizando transaccion");
                tx.commit();
            } catch (Throwable thr) {
                // Desechar (rollback) cambios realizados.
                log.error("desechando transaccion");
                tx.rollback();
                result = Results.ko()
                        .cause(thr)
                        .message("Ocurrió un error durante ejecucion de bloque de código")
                        .build();
            }
        } catch (Throwable thr) {
            // No se pudo iniciar la transaccion.
            log.fatal(thr, "desechando ejecucion");
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error durante ejecucion de transacción")
                    .build();
        }

        return result;
    }

    /**
     * Interface que define el comportamiento esperado de un bloque de código a
     * ser ejecutado.
     *
     * @param <T> Tipo de resultado primario manejado dentro del bloque de
     * ejecución.
     */
    public interface Executable<T> {

        /**
         * Metodo que implementa la lógica de ejecución del bloque.
         *
         * @return Resultado de la ejecución, si hubiere.
         */
        Result<T> execute();
    }

}
