/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.biggies.db;

import fa.gs.misc.collections.Lists;
import fa.gs.misc.collections.Maps;
import fa.gs.utils.misc.Closer;
import fa.gs.utils.misc.errors.Errors;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Collection;
import java.util.Map;

/**
 *
 * @author Fabio A. González Sosa
 */
public class DbUtils {

    public static Collection<Map<String, Object>> select(Connection conn, String query) {
        Collection<Map<String, Object>> rows;

        Closer closer = Closer.instance();
        try {
            Statement stm = closer.add(conn.createStatement());
            stm.execute(query);
            ResultSet rs = closer.add(stm.getResultSet());
            rows = adapt(rs);
        } catch (Throwable thr) {
            Errors.dump(System.err, thr);
            rows = Lists.empty();
        } finally {
            closer.close();
        }

        return rows;
    }

    public static Collection<Map<String, Object>> adapt(ResultSet rs) {
        Collection<Map<String, Object>> rows = Lists.empty();

        try {
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            while (rs.next()) {
                Map<String, Object> row = Maps.empty();
                for (int i = 1; i <= columns; ++i) {
                    row.put(md.getColumnName(i), rs.getObject(i));
                }
                rows.add(row);
            }
        } catch (Throwable thr) {
            Errors.dump(System.err, thr);
        }

        return rows;
    }

}
