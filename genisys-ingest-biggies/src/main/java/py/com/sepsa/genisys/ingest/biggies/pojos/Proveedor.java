/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.biggies.pojos;

import fa.gs.utils.misc.fechas.Fechas;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class Proveedor implements Serializable {

    private Integer id;
    private String ruc;
    private String razonSocial;
    private BigDecimal nroTimbrado;
    private Date vtoTimbrado;
    private BigDecimal diasPlazo;

    public String getNumeroTimbradoFormatoSepsa() {
        return String.valueOf(nroTimbrado);
    }

    public String getVencimientoTimbradoFormatoSepsa() {
        String mes = Fechas.getNombreMes(Fechas.getFieldValue(vtoTimbrado, Calendar.MONTH) + 1);
        Integer anho = Fechas.getFieldValue(vtoTimbrado, Calendar.YEAR);
        return Strings.format("%s/%s", mes.toUpperCase(), anho).trim();
    }

}
