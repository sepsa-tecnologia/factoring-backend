/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.biggies;

import fa.gs.utils.misc.errors.Errors;
import java.io.Serializable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;

/**
 *
 * @author Fabio A. González Sosa
 */
@Singleton(name = "ServiceEntry", mappedName = "ServiceEntry")
@Startup
@LocalBean
public class ServiceEntry implements Serializable {

    @Resource
    private ManagedScheduledExecutorService executor;

    private ScheduledFuture<?> workerHandle;

    @PostConstruct
    public void init() {
        workerHandle = executor.schedule(new Worker(), 30, TimeUnit.SECONDS);
    }

    @PreDestroy
    public void finish() {
        try {
            workerHandle.cancel(true);
        } catch (Throwable thr) {
            Errors.dump(System.err, thr, "Ocurrio un error deteniendo trabajador en segundo plano.");
        }
    }

}
