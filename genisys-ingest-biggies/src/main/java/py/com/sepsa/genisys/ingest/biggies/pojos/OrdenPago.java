/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.biggies.pojos;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Fabio A. González Sosa
 */
@Data
public class OrdenPago implements Serializable {

    private Integer id;

    /**
     * Estados posibles para una orden de pago. Los mismos se obtienen
     * empiricamente al ejecutar la consulta
     * <code>SELECT DISTINCT(ESTADO) FROM TRC.ORDEN_PAGO_CABECERA_VIEW ;</code>.
     * <br/>
     * Los valores considerados corresponden a resultados obtenidos el
     * <b>2021-08-25</b>.
     */
    public static enum Estado {
        PENDIENTE("PEND"),
        ANULADA("ANULADO"),
        APROBADA("APROBADO");
        private final String bdValue;

        Estado(String bdValue) {
            this.bdValue = bdValue;
        }

        public String bdValue() {
            return bdValue;
        }

    }
}
