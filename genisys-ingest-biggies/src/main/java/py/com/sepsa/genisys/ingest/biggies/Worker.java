/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.genisys.ingest.biggies;

import fa.gs.misc.Ids;
import fa.gs.result.simple.Result;
import fa.gs.result.simple.Results;
import fa.gs.utils.collections.Lists;
import fa.gs.utils.logging.app.AppLogger;
import fa.gs.utils.misc.Assertions;
import fa.gs.utils.misc.numeric.Numeric;
import fa.gs.utils.misc.text.Strings;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import py.com.sepsa.genisys.ejb.entities.factoring.Estado;
import py.com.sepsa.genisys.ejb.entities.factoring.Factura;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaConformada;
import py.com.sepsa.genisys.ejb.entities.factoring.FacturaInfo;
import py.com.sepsa.genisys.ejb.entities.factoring.HistoricoFactura;
import py.com.sepsa.genisys.ejb.entities.info.Moneda;
import py.com.sepsa.genisys.ejb.entities.info.Persona;
import py.com.sepsa.genisys.ejb.facades.factoring.FacturaFacade;
import py.com.sepsa.genisys.ejb.facades.info.MonedaFacade;
import py.com.sepsa.genisys.ejb.logic.impl.Configuraciones;
import py.com.sepsa.genisys.ejb.logic.impl.Estados;
import py.com.sepsa.genisys.ejb.logic.impl.Facturas;
import py.com.sepsa.genisys.ejb.logic.impl.Notificaciones;
import py.com.sepsa.genisys.ejb.logic.impl.Personas;
import py.com.sepsa.genisys.ejb.utils.AbstractWorker;
import py.com.sepsa.genisys.ejb.utils.AppLoggerFactory;
import py.com.sepsa.genisys.ejb.utils.Fechas;
import py.com.sepsa.genisys.ejb.utils.Injection;
import py.com.sepsa.genisys.ejb.utils.Text;
import py.com.sepsa.genisys.ingest.biggies.db.TxWrapper;
import py.com.sepsa.genisys.ingest.biggies.pojos.FacturaAPagar;
import py.com.sepsa.genisys.ingest.biggies.pojos.OrdenPago;
import py.com.sepsa.genisys.ingest.biggies.pojos.Proveedor;

/**
 *
 * @author Fabio A. González Sosa
 */
public class Worker extends AbstractWorker {

    private final AppLogger log;
    private final String id;

    Worker() {
        this.log = AppLoggerFactory.ingest("biggie");
        this.id = Strings.format("worker-ingestion-biggie@%s", Ids.randomUuid());
    }

    @Override
    protected AppLogger getLog() {
        return log;
    }

    @Override
    protected String getId() {
        return id;
    }

    @Override
    protected long obtenerIntervaloWork() {
        Long defaultMillis = TimeUnit.MINUTES.toMillis(5);

        try {
            Configuraciones configuraciones = Injection.lookup(Configuraciones.class);
            Result<String> resConfig = configuraciones.buscarValorParametro(Configuraciones.Param.INTEGRACION_BIGGIE_INGESTION_INTERVALO);
            resConfig.raise();
            String value = resConfig.value();
            return Long.valueOf(value);
        } catch (Throwable thr) {
            getLog().error(thr, "Ocurrió un error obteniendo intervalo de ejecucion de trabajo");
            return defaultMillis;
        }
    }

    @Override
    protected void work() throws Throwable {
        // Inyeccion de beans.
        SepsaDao sepsa = SepsaDao.instance();
        BiggiesDao biggies = BiggiesDao.instance();
        Personas personas = Injection.lookup(Personas.class);

        try {
            // Obtener datos de persona Biggie.
            Integer idPersonaBiggie = sepsa.obtenerIdPersonaBiggie();
            if (idPersonaBiggie == null || idPersonaBiggie <= 0) {
                log.warning("Parámetro de identificador de persona biggie no identificado.");
                return;
            }

            // Obtener datos de usuario Biggie.
            Integer idUsuarioBiggie = sepsa.obtenerIdUsuarioBiggie();
            if (idUsuarioBiggie == null || idPersonaBiggie <= 0) {
                log.warning("Parámetro de identificador de persona biggie no identificado.");
                return;
            }

            // Obtener persona Biggie.
            Result<Persona> resPersonaBiggie = personas.obtenerPorId(idPersonaBiggie);
            Persona personaBiggie = resPersonaBiggie.value(null);
            if (personaBiggie == null) {
                log.warning("Cliente no identificado.");
                return;
            }

            // Obtener proveedores habilitados de biggie.
            Collection<Proveedor> proveedoresBiggie = biggies.obtenerProveedores();

            // Completar con informacion sepsa.
            Collection<ProveedorInfo> proveedoresBiggieSepsa = completarProveedores(idPersonaBiggie, proveedoresBiggie);

            // Procesar proveedores disponibles.
            for (ProveedorInfo proveedor : proveedoresBiggieSepsa) {
                // Procesar ordenes de pago aprobadas.
                ingestOrdenesDePagoAprobadas(personaBiggie, proveedor, idUsuarioBiggie);

                // Procesar ordenes de pago pendientes.
                ingestOrdenesDePagoPendientes(personaBiggie, proveedor, idUsuarioBiggie);
            }
        } catch (Throwable thr) {
            log.error(thr, "Ocurrió un error importando ordenes de pago");
        }
    }

    private Collection<ProveedorInfo> completarProveedores(Integer idPersonaBiggie, Collection<Proveedor> proveedores) throws Throwable {
        SepsaDao sepsa = SepsaDao.instance();

        Collection<ProveedorInfo> infos = Lists.empty();
        if (!Assertions.isNullOrEmpty(proveedores)) {
            for (Proveedor proveedor : proveedores) {
                // Generar proveedor SEPSA.
                Persona proveedorSepsa = sepsa.obtenerOCrearProveedor(proveedor.getRazonSocial(), proveedor.getRazonSocial(), proveedor.getRuc());
                if (proveedorSepsa == null) {
                    continue;
                }

                // Generar relacion comercial SEPSA.
                sepsa.agregarOCrearRelacionComercial(proveedorSepsa.getId(), idPersonaBiggie);

                // Ok.
                ProveedorInfo info = new ProveedorInfo();
                info.proveedorBiggie = proveedor;
                info.proveedorSepsa = proveedorSepsa;
                infos.add(info);
            }
        }

        return infos;
    }

    private Result<Void> inTx(Runnable r) {
        TxWrapper tx = Injection.lookup(TxWrapper.class);
        return tx.execute(() -> {
            try {
                r.run();

                return Results.ok()
                        .nullable(true)
                        .build();
            } catch (Throwable thr) {
                return Results.ko()
                        .cause(thr)
                        .build();
            }
        });
    }

    private void ingestOrdenesDePagoAprobadas(Persona personaBiggie, ProveedorInfo proveedor, Integer idUsuarioBiggie) throws Throwable {
        // Inyeccion de beans.
        BiggiesDao biggies = BiggiesDao.instance();

        Collection<OrdenPago> ordenesPago = biggies.obtenerOrdenesPagoAprobadas(proveedor.proveedorBiggie);
        if (!Assertions.isNullOrEmpty(ordenesPago)) {
            for (OrdenPago ordenPago : ordenesPago) {
                // Obtener facturas en cada orden de pago.
                Collection<FacturaAPagar> facturas0 = biggies.obtenerFacturas(ordenPago);

                // Ingestion de facturas.
                Result<Void> result = inTx(() -> {
                    if (!Assertions.isNullOrEmpty(facturas0)) {
                        // Importar facturas de proveedor biggies a sepsa factoring.
                        ingest(personaBiggie, proveedor, ordenPago, facturas0, idUsuarioBiggie, true);
                    }
                });

                result.raise();
            }
        }
    }

    private void ingestOrdenesDePagoPendientes(Persona personaBiggie, ProveedorInfo proveedor, Integer idUsuarioBiggie) throws Throwable {
        // Inyeccion de beans.
        BiggiesDao biggies = BiggiesDao.instance();

        Collection<OrdenPago> ordenesPago = biggies.obtenerOrdenesPagoPendientes(proveedor.proveedorBiggie);
        if (!Assertions.isNullOrEmpty(ordenesPago)) {
            for (OrdenPago ordenPago : ordenesPago) {
                // Obtener facturas en cada orden de pago.
                Collection<FacturaAPagar> facturas0 = biggies.obtenerFacturas(ordenPago);

                // Ingestion de facturas.
                Result<Void> result = inTx(() -> {
                    if (!Assertions.isNullOrEmpty(facturas0)) {
                        // Importar facturas de proveedor biggies a sepsa factoring.
                        ingest(personaBiggie, proveedor, ordenPago, facturas0, idUsuarioBiggie, true);
                    }
                });

                result.raise();
            }
        }
    }

    private Result<Void> ingest(Persona biggie, ProveedorInfo proveedor, OrdenPago ordenPago, Collection<FacturaAPagar> facturas, Integer idUsuario, boolean conformar) {
        Result<Void> result;

        // Obtener moneda base.
        MonedaFacade monedas = Injection.lookup(MonedaFacade.class);
        Moneda guarani = monedas.getGuarani();

        // Beans.
        Facturas bFacturas = Injection.lookup(Facturas.class);
        Notificaciones bNotificaciones = Injection.lookup(Notificaciones.class);

        try {
            List<FacturaInfo> facturasInfo = Lists.empty();

            for (FacturaAPagar factura : facturas) {
                // Verificar si factura existe.
                boolean ok = bFacturas.facturaExiste(factura.getNumeroComprobanteFormatoSepsa(), proveedor.proveedorBiggie.getRuc(), Text.ruc(biggie), factura.getFechaEmision(), factura.getFechaPago(), factura.getMonto(), guarani.getId());
                if (ok) {
                    continue;
                }

                try {
                    // Registrar factura.
                    Factura factura0 = registrarFactura(biggie, proveedor, factura);

                    // Registrar historico.
                    Result<HistoricoFactura> resHistoricoRegistro = bFacturas.agregarHistorico(idUsuario, factura0, Estados.EstadoFacturaEnum.NO_CONFORMADA, "Factura registrada desde alta masiva.");
                    resHistoricoRegistro.raise();

                    // Conformacion automatica.
                    if (conformar) {
                        // Conformar factura.
                        Result<FacturaConformada> resConformacion = bFacturas.registrarFacturaConformada(factura0.getId(), factura.getMonto(), Numeric.CERO, Numeric.CERO, Numeric.CERO, factura.getMonto());
                        resConformacion.raise();

                        // Cambio de estado.
                        Result<Factura> resCambioEstado = bFacturas.cambiarEstado(idUsuario, factura0.getId(), Estados.EstadoFacturaEnum.CONFORMADA);
                        resCambioEstado.raise();

                        // Registrar historico.
                        Result<HistoricoFactura> resHistoricoConformacion = bFacturas.agregarHistorico(idUsuario, factura0, Estados.EstadoFacturaEnum.CONFORMADA, "Factura conformada desde alta masiva.");
                        resHistoricoConformacion.raise();
                    }

                    // Obtener datos finales.
                    Result<FacturaInfo> resFacturaInfo = bFacturas.buscarFactura(factura0.getId());
                    resFacturaInfo.raise();
                    FacturaInfo facturaInfo = resFacturaInfo.value();

                    // Agregar a lote de facturas procesadas correctamente.
                    facturasInfo.add(facturaInfo);
                } catch (Throwable thr) {
                    log.warning()
                            .cause(thr)
                            .message("No se pudo importar la factura")
                            .tag("factura.nro", factura.getNumeroComprobanteFormatoSepsa())
                            .tag("factura.fecha", Fechas.format(factura.getFechaEmision(), Fechas.FORMAT_DD_MM_YYYY))
                            .tag("factura.monto", factura.getMonto())
                            .tag("orden_pago.id", ordenPago.getId())
                            .tag("proveedor.id.biggie", proveedor.proveedorBiggie.getId())
                            .tag("proveedor.id.sepsa", proveedor.proveedorSepsa.getId())
                            .log();
                }
            }

            // Notificar procesamiento de facturas.
            if (conformar) {
                bNotificaciones.notificarConformacionDeFacturas(facturasInfo);
            }

            result = Results.ok()
                    .nullable(true)
                    .build();
        } catch (Throwable thr) {
            result = Results.ko()
                    .cause(thr)
                    .message("Ocurrió un error registrando facturas")
                    .build();
        }

        return result;
    }

    private Factura registrarFactura(Persona biggie, ProveedorInfo proveedor, FacturaAPagar factura) throws Throwable {
        // Obtener estados.
        Estados estados = Injection.lookup(Estados.class);
        Estado estadoNoConformada = estados.find(Estados.EstadoFacturaEnum.NO_CONFORMADA);

        // Obtener moneda base.
        MonedaFacade monedas = Injection.lookup(MonedaFacade.class);
        Moneda guarani = monedas.getGuarani();

        // Nuevo objeto Factura
        Factura factura0 = new Factura();
        factura0.setIdEstado(estadoNoConformada);
        factura0.setNroFactura(factura.getNumeroComprobanteFormatoSepsa());
        factura0.setIdPersonaOrigen(proveedor.proveedorSepsa.getId());
        factura0.setRazonSocialProveedor(proveedor.proveedorBiggie.getRazonSocial());
        factura0.setRucProveedor(proveedor.proveedorBiggie.getRuc());
        factura0.setIdPersonaDestino(biggie.getId());
        factura0.setRazonSocialComprador(Text.razonSocial(biggie));
        factura0.setRucComprador(Text.ruc(biggie));
        factura0.setFecha(factura.getFechaEmision());
        factura0.setFechaPago(factura.getFechaPago());
        factura0.setDiasCredito(Fechas.diasDiferencia(factura.getFechaEmision(), factura.getFechaPago()));
        factura0.setNroTimbrado(proveedor.proveedorBiggie.getNumeroTimbradoFormatoSepsa());
        factura0.setVtoTimbrado(proveedor.proveedorBiggie.getVencimientoTimbradoFormatoSepsa());
        factura0.setIdMoneda(guarani.getId());
        factura0.setMontoTotal5(Numeric.CERO);
        factura0.setMontoTotal10(Numeric.CERO);
        factura0.setMontoTotalExento(Numeric.CERO);
        factura0.setMontoIva5(Numeric.CERO);
        factura0.setMontoImponible5(Numeric.CERO);
        factura0.setMontoIva10(Numeric.CERO);
        factura0.setMontoImponible10(Numeric.CERO);
        factura0.setMontoIvaTotal(Numeric.CERO);
        factura0.setMontoImponibleTotal(Numeric.CERO);
        factura0.setMontoTotalFactura(factura.getMonto());

        FacturaFacade facturaFacade = Injection.lookup(FacturaFacade.class);
        facturaFacade.create(factura0);
        return factura0;
    }

    private static class ProveedorInfo implements Serializable {

        Proveedor proveedorBiggie;
        Persona proveedorSepsa;
    }

}
